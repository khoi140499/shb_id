﻿Imports System.Data
Imports System.Diagnostics
Imports System.Xml
Imports System.Xml.XmlDocument
Imports Business_HQ
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common.mdlSystemVariables
Imports VBOracleLib
Imports Business.ChungTu
Imports Business.NewChungTu
Imports Business.BaoCao
Imports System.Collections.Generic
Imports System.Xml.Linq
Imports ConcertCurToText
Imports System.IO
Imports System.Linq

Partial Class pages_ChungTu_frmChiTietCT_HQ_EBANK
    Inherits System.Web.UI.Page
    Private mv_objUser As New CTuUser
    Private illegalChars As String = "[&]"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LogApp.AddDebug("Chon chung tu xem chi tiet", "TRA CUU CHUNG TU: Xem chi tiet chung tu")
        If Not IsPostBack Then
            If Not Request.QueryString.Item("soct") Is Nothing Then
                Dim so_ct As String = Request.QueryString.Item("soct").ToString
                GetChungTu(so_ct)
            End If
        End If
    End Sub

    Protected Sub GetChungTu(ByVal soct As String)
        Dim SHKB As String = String.Empty
        Dim NGAY_KB As String = String.Empty
        Dim SO_CT As String = String.Empty
        Dim KYHIEU_CT As String = String.Empty
        Dim SO_BT As String = String.Empty
        Dim MA_DTHU As String = String.Empty
        Dim MA_NV As String = String.Empty
        Dim TRANG_THAI As String = String.Empty
        Dim MA_CN_USER As String = String.Empty
        Dim v_strLOAI_CTU As String = String.Empty
        Dim key As New Business_HQ.NewChungTu.KeyCTu

        Dim strSQL As String = ""
        strSQL = "SELECT SHKB,NGAY_KB,SO_CT,KYHIEU_CT,SO_BT,MA_DTHU,MA_NV,TRANG_THAI,MA_CN,NGAY_CT,loai_ctu FROM TCS_CTU_HDR WHERE SO_CT='" & soct & "'"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSQL)

        If Not dt Is Nothing Then
            SHKB = dt.Rows(0)("SHKB").ToString
            NGAY_KB = dt.Rows(0)("NGAY_KB").ToString
            SO_CT = dt.Rows(0)("SO_CT").ToString
            KYHIEU_CT = dt.Rows(0)("KYHIEU_CT").ToString
            SO_BT = dt.Rows(0)("SO_BT").ToString
            MA_DTHU = dt.Rows(0)("MA_DTHU").ToString
            MA_NV = dt.Rows(0)("MA_NV").ToString
            TRANG_THAI = dt.Rows(0)("TRANG_THAI").ToString
            MA_CN_USER = dt.Rows(0)("MA_CN").ToString
            v_strLOAI_CTU = dt.Rows(0)("loai_ctu").ToString
        End If

        If ((Not SHKB Is Nothing) And (Not MA_NV Is Nothing) And (Not SO_BT Is Nothing) And (Not SO_CT Is Nothing)) Then
            'Dim key As New KeyCTu
            key.Ma_Dthu = MA_DTHU
            key.Kyhieu_CT = KYHIEU_CT
            key.Ma_NV = MA_NV
            key.Ngay_KB = NGAY_KB
            key.SHKB = SHKB
            key.So_BT = SO_BT
            key.So_CT = SO_CT
            'key.MA_CN = MA_CN_USER
            hdnMA_NV.Value = MA_NV
            Dim d_NgayLV As Date = ConvertNumberToDate(NGAY_KB)
            hdnNGAYLV.Value = d_NgayLV.ToString("dd/MM/yyyy")
            hdnSHKB.Value = SHKB
            hdnSO_BT.Value = SO_BT
            hdnSO_CTU.Value = SO_CT
            hdfKyHieu_CT.Value = KYHIEU_CT
            hdfTrangThai_CT.Value = TRANG_THAI
            hdnNgayKB.Value = NGAY_KB
            hdnSO_BT.Value = SO_BT
            hdfLoai_CT.Value = v_strLOAI_CTU
            LoadCTChiTietAll(key)
        End If

        lbSoCT.Text = "Số CT: " & SO_CT
        lbSoCT.Visible = True

        If (TRANG_THAI = "01" And MA_CN_USER = Session.Item("MA_CN_USER_LOGON")) Then
            btnInCTU.Enabled = True
        ElseIf (TRANG_THAI = "05" And MA_CN_USER = Session.Item("MA_CN_USER_LOGON")) Then
            btnInCTU.Enabled = True
        ElseIf (TRANG_THAI = "08" And MA_CN_USER = Session.Item("MA_CN_USER_LOGON")) Then
            btnInCTU.Enabled = True
        ElseIf (TRANG_THAI = "07" And MA_CN_USER = Session.Item("MA_CN_USER_LOGON")) Then
            btnInCTU.Enabled = True
        ElseIf (TRANG_THAI = "03" And MA_CN_USER = Session.Item("MA_CN_USER_LOGON")) Then
            btnInCTU.Enabled = True
        Else
            btnInCTU.Enabled = False
        End If
    End Sub

    Private Sub LoadCTChiTietAll(ByVal key As Business_HQ.NewChungTu.KeyCTu)
        Try
            Dim v_strTT As String = String.Empty
            Dim v_strTTThue As String = String.Empty
            Dim v_strLoaiThue As String = String.Empty
            Dim v_strPhuongThucTT As String = String.Empty
            Dim v_str_Ma_NNT As String = String.Empty

            'Load chứng từ header
            Dim dsCT As New DataSet

            dsCT = ChungTu.buChungTu.CTU_Header_Set(key)

            If (dsCT.Tables(0).Rows.Count > 0) Then
                ShowHideCTRow(True)
                FillDataToCTHeader(dsCT)
                v_strTT = dsCT.Tables(0).Rows(0)("TRANG_THAI").ToString
                v_strLoaiThue = dsCT.Tables(0).Rows(0)("MA_LTHUE").ToString
                v_strPhuongThucTT = dsCT.Tables(0).Rows(0)("PT_TT").ToString
                v_str_Ma_NNT = dsCT.Tables(0).Rows(0)("Ma_NNThue").ToString
                v_strTTThue = dsCT.Tables(0).Rows(0)("TT_CTHUE").ToString
            Else
                lblStatus.Text = "Không tìm thấy chứng từ cần tìm.Hãy thử lại"
                Exit Sub
            End If

            'Load thông tin chi tiết
            Dim dsCTChiTiet As New DataSet

            'dsCTChiTiet = ChungTu.buChungTu.CTU_ChiTiet_Set(key)
            dsCTChiTiet = GetChiTietHQBy(key)

            If dsCTChiTiet.Tables(0).Rows.Count > 0 Then
                FillDataToCTDetail(dsCTChiTiet)
            Else
                lblStatus.Text = "Không lấy được thông tin chi tiết của chứng từ.Hãy kiểm tra lại"
                Exit Sub
            End If
            If v_strTTThue.Equals("0") And (v_strTT.Equals("01") Or v_strTT.Equals("07")) Then
                btnChuyenThue.Visible = True
            End If
            ShowHideControlByPT_TT(v_strPhuongThucTT, v_strTT)
            'Me.txtMatKhau.Focus()
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

            If Not ex.InnerException Is Nothing Then
                sbErrMsg = New StringBuilder()

                sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ ")
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.Message)
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.StackTrace)

                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)
            End If

        Finally
        End Try
    End Sub

    Private Sub FillDataToCTHeader(ByVal dsCT As DataSet)
        Try
            Dim strTenVT = String.Empty
            Dim strTenLH As String = String.Empty
            Dim strMaLH As String = String.Empty
            Dim dr As DataRow = dsCT.Tables(0).Rows(0)
            Dim v_strTrangThai As String = dr("TRANG_THAI").ToString()
            Dim v_strHTTT As String = GetString(dr("PT_TT")).ToString
            txtSHKB.Text = dr("SHKB").ToString()
            txtTenKB.Text = dr("TEN_KB").ToString()
            txtNGAY_KH_NH.Text = ConvertNumbertoString(dr("NGAY_KB").ToString())
            txtTKCo.Text = dr("TK_Co").ToString()
            txtTKNo.Text = dr("TK_No").ToString()
            txtMaDBHC.Text = dr("MA_XA").ToString()
            txtTenDBHC.Text = Get_TenTinh(dr("MA_XA").ToString())
            txtMaNNT.Text = dr("MA_NNTHUE").ToString()
            txtTenNNT.Text = dr("TEN_NNTHUE").ToString()
            txtDChiNNT.Text = dr("DC_NNTHUE").ToString()
            txtMaNNTien.Text = dr("Ma_NNTien").ToString()
            txtTenNNTien.Text = dr("TEN_NNTIEN").ToString()
            txtDChiNNTien.Text = dr("DC_NNTIEN").ToString()
            txtMaCQThu.Text = dr("MA_CQTHU").ToString()
            txtTenCQThu.Text = dr("TEN_CQTHU").ToString()
            Dim dt As New DataTable
            dt.Columns.Add("ID_HQ", Type.GetType("System.String"))
            dt.Columns.Add("Name", Type.GetType("System.String"))
            dt.Columns.Add("Value", Type.GetType("System.String"))
            dt.Columns(0).AutoIncrement = True
            Dim R As DataRow = dt.NewRow
            R("ID_HQ") = dr("MA_LTHUE").ToString()
            R("Name") = GetTenLoaiThue(dr("MA_LTHUE").ToString())
            R("Value") = dr("MA_LTHUE").ToString()
            dt.Rows.Add(R)

            strMaLH = GetString(dr("LH_XNK").ToString())
            Get_LHXNK_ALL(strMaLH, strTenLH, strTenVT)

            ddlHTTT.SelectedValue = v_strHTTT
            ddlHTTT.SelectedItem.Text = Get_TenPTTT(v_strHTTT)
            rowTKGL.Visible = False
            If dr("ma_ntk").ToString = "1" Then
                txtTenTKCo.Text = "Tài khoản thu NSNN"
            ElseIf dr("ma_ntk").ToString = "2" Then
                txtTenTKCo.Text = "Tài khoản tạm thu"
            Else
                txtTenTKCo.Text = "Thu hoàn thuế GTGT"
            End If

            txtTK_KH_NH.Text = GetString(dr("TK_KH_NH"))
            txtTenTK_KH_NH.Text = dr("TEN_KH_NH").ToString
            txtNGAY_KH_NH.Text = GetString(dr("NGAY_KH_NH"))
            txtMA_NH_A.Text = GetString(dr("MA_NH_A"))
            txtTen_NH_A.Text = GetString(dr("TEN_NH_A"))
            txtMA_NH_B.Text = GetString(dr("MA_NH_B"))
            txtTen_NH_B.Text = GetString(dr("TEN_NH_B"))
            txtDienGiaiNH.Text = GetString(dr("NH_HUONG"))
            txtDienGiaiHQ.Text = GetString(dr("DIENGIAI_HQ"))
            txtMA_NH_TT.Text = GetString(dr("MA_NH_TT"))
            txtTEN_NH_TT.Text = GetString(dr("TEN_NH_TT"))
            txtMaNT.Text = GetString(dr("Ma_NT"))
            txtTenNT.Text = GetString(dr("Ten_NT"))
            txtTimeLap.Text = GetString(dr("ngay_ht"))

            Dim strPTTP As String = ""
            strPTTP = dr("pt_tinhphi").ToString()
            If strPTTP = "O" Then
                rdTypePN.Checked = True
                rdTypeMP.Checked = False
            ElseIf strPTTP = "W" Then
                rdTypeMP.Checked = True
                rdTypePN.Checked = False
            End If
            Dim strTT_CITAD As String = ""
            strTT_CITAD = dr("TT_CITAD").ToString()

            txtRM_REF_NO.Text = dr("RM_REF_NO").ToString()
            txtREF_NO.Text = dr("REF_NO").ToString()
            txtSoCT_NH.Text = dr("So_CT_NH").ToString()
            txtQuan_HuyenNNTien.Text = dr("QUAN_HUYENNNTIEN").ToString()
            txtTinh_TphoNNTien.Text = dr("TINH_TPNNTIEN").ToString()
            txtKHCT.Text = dr("KyHieu_CT").ToString()
            txtSoCT.Text = dr("So_CT").ToString()
            If dr("LY_DO_HUY").ToString().Trim <> "" Then
                txtLyDoHuy.Text = dr("LY_DO_HUY").ToString().Trim
            Else
                txtLyDoHuy.Text = ""
            End If
            Dim strTenTinhTP As String = ""
            If dr("Quan_huyennntien").ToString().Trim <> "" Then
                txtQuanNNThue.Text = dr("Quan_huyennntien").ToString().Trim
                txtTenQuanNNThue.Text = clsCTU.Get_TenQuanHuyen(txtQuanNNThue.Text.ToString())
                strTenTinhTP = clsCTU.Get_TenTinhTP(txtQuanNNThue.Text.ToString())
                txtTinhNNThue.Text = strTenTinhTP.Split(";")(0).ToString()
                txtTenTinhNNThue.Text = strTenTinhTP.Split(";")(1).ToString()
            Else
                txtQuanNNThue.Text = dr("Quan_huyennntien").ToString().Trim
                txtTenQuanNNThue.Text = ""
                txtTinhNNThue.Text = ""
                txtTenTinhNNThue.Text = ""
            End If
            If dr("ma_tinh").ToString().Trim <> "" Then
                txtQuanNNTien.Text = dr("ma_tinh").ToString().Trim
                txtTenQuanNNTien.Text = clsCTU.Get_TenQuanHuyen(txtQuanNNTien.Text.ToString())
                strTenTinhTP = clsCTU.Get_TenTinhTP(txtQuanNNTien.Text.ToString())
                txtTinhNNTien.Text = strTenTinhTP.Split(";")(0).ToString()
                txtTenTinhNNTien.Text = strTenTinhTP.Split(";")(1).ToString()
            Else
                txtQuanNNTien.Text = ""
                txtTenQuanNNTien.Text = ""
                txtTinhNNTien.Text = ""
                txtTenTinhNNTien.Text = ""
            End If

            Dim strLoaiPhi As String = dr("ma_sanpham").ToString()
            Dim strPhiGD As String = ""
            Dim strPhiKD As String = ""
            Try
                If strLoaiPhi.LastIndexOf("|") Then
                    strPhiGD = strLoaiPhi.Split("|")(1).ToString()
                    strPhiKD = strLoaiPhi.Split("|")(0).ToString()
                    Dim strSQL As String = "SELECT NAME from tcs_dm_phi where tinh_trang='1' and CODE='" & strPhiGD & "'"
                    dt = DataAccess.ExecuteToTable(strSQL)
                    If dt.Rows.Count > 0 Then
                        txtTenLoaiPhi.Text = dt.Rows(0)(0).ToString()
                    End If
                    strSQL = "SELECT NAME from tcs_dm_phi where tinh_trang='1' and CODE='" & strPhiKD & "'"
                    dt = DataAccess.ExecuteToTable(strSQL)
                    If dt.Rows.Count > 0 Then
                        txtTenLoaiPhiKD.Text = dt.Rows(0)(0).ToString()
                    End If
                Else
                    Dim strSQL As String = "SELECT NAME from tcs_dm_phi where tinh_trang='1' and CODE='" & strLoaiPhi & "'"
                    dt = DataAccess.ExecuteToTable(strSQL)
                    If dt.Rows.Count > 0 Then
                        txtTenLoaiPhi.Text = dt.Rows(0)(0).ToString()
                    End If
                End If
            Catch ex As Exception
            End Try

            txtPhi.Text = VBOracleLib.Globals.Format_Number(dr("phi_gd").ToString(), ",")
            txtPhiVAT.Text = VBOracleLib.Globals.Format_Number(dr("phi_vat").ToString(), ",")
            txtPhiKD.Text = VBOracleLib.Globals.Format_Number(dr("phi_gd2").ToString(), ",")
            txtPhiKDVAT.Text = VBOracleLib.Globals.Format_Number(dr("phi_vat2").ToString(), ",")

            'txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number(dr("tt_tthu").ToString().Trim, ".")

            If dr("SEQ_NO").ToString() <> "" Then
                lbSoFT.Text = "Số FT:" & dr("SEQ_NO").ToString()
                If dr("so_xcard").ToString() <> "" Then
                    lbSoFT.Text &= " Số XCARD :" & dr("so_xcard").ToString()
                End If
            Else
                lbSoFT.Text = ""
            End If
            txtCK_SoCMND.Text = dr("SO_CMND").ToString()
            txtCK_SoDT.Text = dr("SO_FONE").ToString()

            txtMaCuc.Text = dr("MA_CUC").ToString()
            txtTenCuc.Text = dr("TEN_CUC").ToString()
            txtMaHQ.Text = dr("MA_HQ").ToString()
            txtTenHQ.Text = dr("TEN_HQ").ToString()
            txtMaHQPH.Text = dr("MA_HQ_PH").ToString()
            txtTenHQPH.Text = dr("TEN_HQ_PH").ToString()
            txtTyGia.Text = VBOracleLib.Globals.Format_Number(dr("TY_GIA").ToString(), ",")


            Dim dTTien As Double = dr("TTIEN")
            Dim dPhiGD As Double = dr("PHI_GD")
            Dim dVat As Double = dr("PHI_VAT")
            Dim dPhiGD2 As Double = dr("PHI_GD2")
            Dim dVat2 As Double = dr("PHI_VAT2")

            txtTTIEN.Text = VBOracleLib.Globals.Format_Number(dTTien.ToString(), ",")

            lblTTien_Chu.Text = ConcertCurToText.ConvertCurr.So_chu(dTTien)

            txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number((dTTien + dPhiGD + dVat + dPhiGD2 + dVat2).ToString(), ",")

            txtDienGiai.Text = dr("REMARKS").ToString()

        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình FillDataToCTHeader")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

            If Not ex.InnerException Is Nothing Then
                sbErrMsg = New StringBuilder()

                sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ ")
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.Message)
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.StackTrace)

                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)
            End If
        Finally
        End Try
    End Sub

    Private Sub FillDataToCTDetail(ByVal pv_dsCT As DataSet)
        Try
            Dim v_dt As DataTable = pv_dsCT.Tables(0)
            grdChiTiet.DataSource = v_dt
            grdChiTiet.DataBind()

            For i As Integer = 0 To v_dt.Rows.Count - 1
                CType(grdChiTiet.Items(i).Controls(8).Controls(1), TextBox).Text = VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(8).Controls(1), TextBox).Text, ",")
                CType(grdChiTiet.Items(i).Controls(9).Controls(1), TextBox).Text = VBOracleLib.Globals.Format_Number2(CType(grdChiTiet.Items(i).Controls(8).Controls(1), TextBox).Text)
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Function GetTenLoaiThue(ByVal pv_strMaLoaiThue As String) As String
        Select Case pv_strMaLoaiThue
            Case "01"
                Return "Thuế nội địa"
            Case "02"
                Return "Thuế thu nhập cá nhân"
            Case "03"
                Return "Thuế chước bạ"
            Case "04"
                Return "Thuế Hải Quan"
            Case "05"
                Return "Thu khác"
            Case "06"
                Return "Thu tài chính"
            Case Else '05'
                Return "Phạt vi phạm hành chính"
        End Select
    End Function

    Private Sub ShowHideCTRow(ByVal pv_blnDisplayRow As Boolean)
        rowDChiNNT.Visible = pv_blnDisplayRow
        rowNNTien.Visible = pv_blnDisplayRow
        rowDChiNNTien.Visible = False
        rowQuan_HuyenNNTien.Visible = False
        rowTinhNNTien.Visible = pv_blnDisplayRow
        rowDBHC.Visible = pv_blnDisplayRow
        rowCQThu.Visible = pv_blnDisplayRow
        rowTKNo.Visible = False
        rowTKCo.Visible = pv_blnDisplayRow
        rowHTTT.Visible = pv_blnDisplayRow
        rowTK_KH_NH.Visible = pv_blnDisplayRow
        rowSoDuNH.Visible = False
        rowNgay_KH_NH.Visible = pv_blnDisplayRow
        rowMaNT.Visible = pv_blnDisplayRow

        rowNganHangChuyen.Visible = pv_blnDisplayRow
        rowNganHangNhan.Visible = pv_blnDisplayRow
        rowTKGL.Visible = pv_blnDisplayRow
        rowNNTien.Visible = pv_blnDisplayRow
        rowSoCMND.Visible = pv_blnDisplayRow
        rowDChiNNTien.Visible = pv_blnDisplayRow
        rowSoDT.Visible = pv_blnDisplayRow
        rowTinhNNThue.Visible = pv_blnDisplayRow
        rowTinhNNTien.Visible = pv_blnDisplayRow
        rowQuanNNThue.Visible = pv_blnDisplayRow
        rowQuanNNTien.Visible = pv_blnDisplayRow

    End Sub

    Private Sub ShowHideControlByPT_TT(ByVal pv_strPT_TT As String, ByVal pv_strTT As String)
        If pv_strPT_TT = "01" Or pv_strPT_TT = "03" Then 'Chuyển khoản
            'If pv_strPT_TT = "03" Then ' Chỉ GL mới lấy ra số dư vì chuyển khoản đã trừ tiền trước rồi
            rowTK_KH_NH.Visible = True
            rowNganHangNhan.Visible = True
            rowNganHangChuyen.Visible = True
            rowNNTien.Visible = True
            rowSoCMND.Visible = False
            rowDChiNNTien.Visible = True
            rowSoDT.Visible = False
            rowTKGL.Visible = False
            If pv_strTT.Equals("05") Then
                rowSoDuNH.Visible = True
                rowTK_KH_NH.Visible = True
                Dim v_strSoTKKH As String = txtTK_KH_NH.Text
                Dim v_strLoaiTien As String = txtMaNT.Text
                If v_strSoTKKH.Length > 0 Then
                    ' Dim v_objCoreBank As New clsCoreBankUtilABB
                    Dim v_strDescTKKH As String = String.Empty
                    Dim v_strSoDuNH As String = String.Empty
                    Dim v_strErrorCode As String = String.Empty
                    Dim v_strErrorMsg As String = String.Empty

                    If (txtTK_KH_NH.Text.Trim.Length > 0) Then
                        Dim xmlDoc As New XmlDocument

                        Dim v_strReturn As String = ""
                        v_strReturn = Regex.Replace(v_strReturn, illegalChars, "")
                        xmlDoc.LoadXml(v_strReturn)
                        Dim ds As New DataSet
                        ds.ReadXml(New XmlTextReader(New StringReader(xmlDoc.InnerXml)))
                        If ds.Tables.Count > 0 Then
                            txtTenTK_KH_NH.Text = ds.Tables(0).Rows(0)("CIFNO") & "|" & ds.Tables(0).Rows(0)("BRANCH_CODE") & "|" & ds.Tables(0).Rows(0)("ACCOUNTNAME") & "|" & ds.Tables(0).Rows(0)("CUSTOMER_TYPE") & "|" & ds.Tables(0).Rows(0)("CUSTOMER_CATEGORY")
                            txtSoDu_KH_NH.Text = VBOracleLib.Globals.Format_Number(ds.Tables(0).Rows(0)("ACY_AVL_BAL"), ".")
                        Else
                            lblStatus.Text = "Không truy vấn được thông tin tài khoản.Mã lỗi:" & v_strErrorCode & ",mô tả:" & clsCTU.MappingErrorCode(v_strErrorCode)
                        End If
                    End If
                End If
            Else
                rowSoDuNH.Visible = False
            End If
        ElseIf pv_strPT_TT = "00" Then  'Tiền mặt
            rowTK_KH_NH.Visible = False
            'rowNganHangNhan.Visible = False
            'rowNganHangChuyen.Visible = False
            rowSoDuNH.Visible = False
            rowTKGL.Visible = False

            rowNNTien.Visible = True
            rowSoCMND.Visible = True
            rowDChiNNTien.Visible = True
            rowSoDT.Visible = True
        Else 'Báo có
            rowTK_KH_NH.Visible = False
            rowNganHangNhan.Visible = False
            rowNganHangChuyen.Visible = False
            rowSoDuNH.Visible = False
            rowTKGL.Visible = True
            rowNNTien.Visible = False
            rowSoCMND.Visible = False
            rowDChiNNTien.Visible = False
            rowSoDT.Visible = False
        End If
    End Sub

   

    Private Function GetChiTietHQBy(ByVal _key As Business_HQ.NewChungTu.KeyCTu) As DataSet
        Dim ds As DataSet = Nothing
        Try

            Dim sSql As String = "SELECT SO_TK, TO_CHAR(NGAY_TK,'dd/MM/yyyy') NGAY_TK, MA_LT, MA_LHXNK, SAC_THUE, MA_CHUONG, MA_TMUC, NOI_DUNG, SOTIEN, SOTIEN_NT " & _
                                 "FROM TCS_CTU_DTL " & _
                                 "WHERE (SHKB = '" & _key.SHKB & "') AND (SO_CT = " & _key.So_CT & ") " & _
                                    " AND (SO_BT = " & _key.So_BT & ") AND (MA_NV = " & _key.Ma_NV & ")"
            ds = DataAccess.ExecuteReturnDataSet(sSql, CommandType.Text)

        Catch ex As Exception

        End Try
        Return ds
    End Function

    Protected Sub btnChuyenThue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChuyenThue.Click
        LogApp.AddDebug("cmdChuyenThue_Click", "KIEM SOAT CHUNG TU HQ: Chuyen thue")
        Dim v_blnReturn As Boolean = False
        'Dim strNguyenTe As String = ""
        Try
            Dim strUser = mv_objUser.Ten_DN.ToString

            btnChuyenThue.Enabled = False

            'Dat lai gia tri
            Dim v_strSHKB As String = hdnSHKB.Value
            Dim v_strNgay_KB As String = hdnNgayKB.Value
            Dim v_strSo_CT As String = hdnSO_CTU.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdnSO_BT.Value
            'Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdnMA_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            'Neu ma trang thai 19 thi chuyen thanh trang thai 01 de sau do chuyen sang hai quan

            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""
            Dim v_strTransaction_id As String = ""
            Dim pv_so_tn_ct_ych As String = ""
            Dim pv_so_tn_ct As String = ""
            Dim pv_ngay_tn_ct As String = ""
            Dim hdr As New infDoiChieuNHHQ_HDR
            Dim dtls() As infDoiChieuNHHQ_DTL
            hdr.ten_kb = Globals.RemoveSign4VietnameseString(txtTenKB.Text)
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New Business_HQ.NewChungTu.KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                'key.Ma_Dthu = v_strMa_Dthu

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT
                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    btnChuyenThue.Enabled = True
                    Exit Sub
                End If
                Dim blnSuccess As Boolean = True
                Dim MSGType As String = ""
                Dim desMSG_HQ As String = ""
                Try
                    Dim loai_ct As String = hdfLoai_CT.Value

                    Dim strTN_CT As String = ChungTu.buChungTu.CTU_TN_CT_NHHQ(v_strSo_CT, TransactionHQ_Type.M47.ToString())
                    If strTN_CT <> "" Then
                        pv_so_tn_ct = strTN_CT.Split(";")(0)
                        pv_ngay_tn_ct = strTN_CT.Split(";")(1)
                    End If

                    If loai_ct.Equals("P") Then
                        'Le phi quan
                        CustomsServiceV3.ProcessMSG.guiCTLePhiHQ(txtMaNNT.Text, v_strSo_CT, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
                        MSGType = TransactionHQ_Type.M41.ToString()
                    Else
                        'Thue hai quan
                        CustomsServiceV3.ProcessMSG.guiCTThueHQ(txtMaNNT.Text, v_strSo_CT, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
                        MSGType = TransactionHQ_Type.M47.ToString()
                    End If

                    ''Neu gui thong tin thanh cong
                    ' strErrorNum = "0"
                    Dim str_TTThue As String = String.Empty
                    If strErrorNum = "0" Then

                        If v_strTrangThai_CT = "01" And v_strTrangThai_CT = "19" Then
                            If blnSuccess = True Then
                                str_TTThue = "1"
                            Else
                                str_TTThue = "0"
                            End If
                            v_strTrangThai_CT = "01"
                        End If

                        If v_strTrangThai_CT = "04" Then
                            If blnSuccess = True Then
                                str_TTThue = "0"
                            Else
                                str_TTThue = "1"
                            End If
                        End If

                        ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT)
                        'Insert du lieu vao bang DOICHIEU_NHHQ
                        clsCTU.SetObjectToDoiChieuNHHQ(v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, "CTU", hdr, dtls, "", strErrorNum, MSGType, "", pv_so_tn_ct, pv_ngay_tn_ct)
                        hdr.transaction_id = v_strTransaction_id
                        hdr.ten_nh_th = txtTen_NH_B.Text.Trim.ToString()
                        ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls, str_TTThue)
                        lblStatus.Text = "Chứng từ đã chuyển số liệu sang Cơ quan Hải quan thành công"
                    Else

                        blnSuccess = False
                        lblStatus.Text = "Có lỗi khi gửi số liệu sang Hải Quan. Mã lỗi " & strErrorNum & " Mô tả lỗi: " & strErrorMes
                        'ShowHideCTRow(False)

                        If v_strTrangThai_CT = "01" And v_strTrangThai_CT = "19" Then
                            If blnSuccess = True Then
                                str_TTThue = "1"
                            Else
                                str_TTThue = "0"
                            End If
                            v_strTrangThai_CT = "01"
                        End If
                        ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT)
                        btnChuyenThue.Enabled = True
                    End If
                    desMSG_HQ = lblStatus.Text.Trim
                    If desMSG_HQ.Length > 400 Then
                        desMSG_HQ = desMSG_HQ.Substring(0, 399)
                    End If
                    ChungTu.buChungTu.InsertMSGLoi(hdr, dtls, IIf(blnSuccess, "1", "0"), desMSG_HQ)
                Catch ex As Exception
                    blnSuccess = False
                    lblStatus.Text = "Chứng từ chưa chuyển được số liệu sang Cơ quan Hải quan. " & ex.Message
                    Exit Sub
                End Try



            End If

        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình chuyển sang hải quan")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

            If Not ex.InnerException Is Nothing Then
                sbErrMsg = New StringBuilder()

                sbErrMsg.Append("Lỗi trong quá trình chuyển sang hải quan")
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.Message)
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.StackTrace)

                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)
            End If

            ' Throw ex
        Finally
        End Try
    End Sub

End Class
