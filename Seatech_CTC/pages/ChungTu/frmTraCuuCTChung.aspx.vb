﻿Imports System.Data
Imports Business.NTDT.NNTDT
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao

Partial Class pages_ChungTu_frmTraCuuCTChung
    Inherits System.Web.UI.Page
    Dim total As Decimal = 0
    Dim totalRow As Integer = 0
    Dim totalUSD As Decimal = 0
    Dim strNguyeTe As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            DM_LoaiThue()
            DM_DiemThu()
            DM_TrangThai()
            hdfMA_CN.Value = Session.Item("MA_CN_USER_LOGON").ToString
        End If
    End Sub
    Protected Function GetMoTa_TrangThai(ByVal strTrang_Thai As String) As String
        Try
            Dim result As String = ""
            Dim sql As String = "select mota from tcs_dm_trangthai_ntdt where trang_thai='" & strTrang_Thai & "' and GHICHU='CT'"
            Dim dr As IDataReader = DataAccess.ExecuteDataReader(sql, CommandType.Text)
            If dr.Read Then
                result = dr("MOTA").ToString
            End If
            Return result
        Catch ex As Exception
            Return strTrang_Thai
        End Try
    End Function
    Private Sub load_dataGrid()
        total = 0
        totalRow = 0
        Dim objNNTDT As New buNNTDT
        Dim WhereClause As String = ""
        Try
            If txtTuNgay.Value <> "" Then
                WhereClause += " AND (TO_CHAR(A.NGAY_NHANG,'yyyymmdd') >= " & ConvertDateToNumber(txtTuNgay.Value) & ") "
            End If
            If txtDenNgay.Value <> "" Then
                WhereClause += " AND (TO_CHAR(A.NGAY_NHANG,'yyyymmdd') <= " & ConvertDateToNumber(txtDenNgay.Value) & ") "
            End If
            If txtTuSo_CT.Text <> "" Then
                WhereClause += " AND A.SO_CTU >= " & txtTuSo_CT.Text.Trim
            End If
            If txtDenSo_CT.Text <> "" Then
                WhereClause += " AND A.SO_CTU <= " & txtDenSo_CT.Text.Trim
            End If
            If txtMST.Text <> "" Then
                WhereClause += " AND A.MST_NNOP like '%" & txtMST.Text.Trim & "%' "
            End If
            If txtTenNNT.Text <> "" Then
                WhereClause += " AND A.TEN_NNOP like '%" + txtTenNNT.Text.Trim + "%' "
            End If
            If drpLoaiThue.SelectedIndex <> 0 Then
                'Thuế hải quan
                WhereClause += " AND A.Ma_LThue = '" & drpLoaiThue.SelectedValue & "' "
            End If
            If txtTKNH.Text <> "" Then
                WhereClause += " AND A.STK_NHANG_NOP like '%" + txtTKNH.Text + "%' "
            End If
            If drpTrangThai.SelectedIndex <> 0 Then
                'Thành công
                WhereClause += " AND A.TTHAI_CTU = '" & drpTrangThai.SelectedValue & "' "
            End If

            ' tuanda
            ' tìm kiếm theo chi nhánh
            ' thêm mã tham chiếu (hdr) 
            If txtMaTC.Text <> "" Then
                WhereClause += " AND A.MA_THAMCHIEU like '%" & txtMaTC.Text.Trim & "%' "
            End If
            If txtMaCN.SelectedValue.Trim.Length > 0 Then
                WhereClause += " AND a.ma_nhang_unt = '" & txtMaCN.SelectedValue & "' "
            End If

            Dim strSQL As String = ""

            'strSQL = "SELECT A.MA_THAMCHIEU_KNOP as MA_THAMCHIEU, A.ID_CTU,A.SO_CTU,A.MST_NNOP,A.TEN_NNOP,A.STK_NHANG_NOP,A.MA_LTHUE,A.TEN_TTHAI_CTU,A.TONG_TIEN,LT.TEN AS TEN_LTHUE, A.SO_CT_NH AS REF_NO " & _
            '         " ,NVL(A.MA_NGUYENTE,'VND') MA_NGUYENTE FROM TCS_CTU_NTDT_HDR A, TCS_DM_LTHUE LT WHERE A.MA_LTHUE=LT.MA_LTHUE(+) " & _
            '         " " & WhereClause & _
            '         " ORDER BY  A.ID_CTU DESC "

            ' tuanda
            ' tìm kiếm theo chi nhánh
            ' thêm mã tham chiếu (hdr) 
            strSQL = "SELECT a.ma_nhang_unt as ID, a.ten_nhang_unt as NAME, A.MA_THAMCHIEU as SO_CTU_CORE, a.ma_thamchieu ,A.ID_CTU,A.SO_CTU,A.MST_NNOP,A.TEN_NNOP,A.STK_NHANG_NOP, " &
                     " A.MA_LTHUE,A.TEN_TTHAI_CTU,A.TONG_TIEN,LT.TEN AS TEN_LTHUE, A.SO_CT_NH AS REF_NO " &
                     " ,NVL(A.MA_NGUYENTE,'VND') MA_NGUYENTE " &
                     " FROM TCS_CTU_NTDT_HDR A, TCS_DM_LTHUE LT " &
                     " WHERE A.MA_LTHUE=LT.MA_LTHUE(+) " &
                     " " & WhereClause &
                     " ORDER BY  A.ID_CTU DESC "

            Dim dt = DatabaseHelp.ExecuteToTable(strSQL)
            If dt.Rows.Count > 0 Then
                totalRow = dt.Rows.Count
                For Each item In dt.Rows
                    If item("MA_NGUYENTE").ToString().Equals("USD") Then
                        totalUSD += Decimal.Parse(item("TONG_TIEN").ToString)
                    Else
                        total += Decimal.Parse(item("TONG_TIEN").ToString)
                    End If

                    '' nếu ct_core bằng null -> gán giá trị FT+năm+tháng+ngày+giờ+phút+giây
                    'If String.IsNullOrEmpty(item("SO_CTU_CORE").ToString()) Then
                    '    item("SO_CTU_CORE") = "FT" + DateTime.Now.ToString("yyyyMMddHHmmss")
                    'End If
                Next
                dtgrd.DataSource = dt
                dtgrd.DataBind()
                grdExport.DataSource = dt
                grdExport.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub

    Protected Sub dtgrd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dtgrd.PageIndexChanging
        load_dataGrid()
        dtgrd.PageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub

    Protected Sub dtgrd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dtgrd.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lblTotal As Label = DirectCast(e.Row.FindControl("totalAmount"), Label)
            'lblTotal.Text = Replace(total.ToString("##,##"), ",", ".")
            Dim strLBVND As String = ""
            Dim strLBUSD As String = ""
            'lblTotal.Text = total.ToString("##,##") & " / " & totalUSD.ToString("##,##")
            If total = 0 Then
                strLBVND = "0"
            ElseIf total > 0 Then
                strLBVND = total.ToString("##,##")
            End If

            If totalUSD = 0 Then
                strLBUSD = "0.00"
            ElseIf totalUSD > 0 Then
                strLBUSD = String.Format("{0:n2}", totalUSD)
            End If

            lblTotal.Text = strLBVND & "/" & strLBUSD
            Dim lblTotalCT As Label = DirectCast(e.Row.FindControl("totalCT"), Label)
            lblTotalCT.Text = totalRow.ToString()
        End If
    End Sub

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        load_dataGrid()
        dtgrd.PageIndex = 0
        dtgrd.DataBind()
    End Sub
    Private Sub DM_TrangThai()

        Dim dsTrangThai As DataSet
        Dim strSQL As String = ""
        strSQL = "SELECT a.TRANG_THAI, a.MOTA  FROM tcs_dm_trangthai_ntdt a where a.GHICHU='CT' "
        Try
            dsTrangThai = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(drpTrangThai, dsTrangThai, 1, 0, "Tất cả")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình lấy thông tin trạng thái chứng từ", Session.Item("TEN_DN"))
            Throw ex
        Finally

        End Try
    End Sub
    Private Sub DM_LoaiThue()

        Dim dsLoaiThue As DataSet
        Dim strSQL As String = ""
        strSQL = "SELECT a.MA_LTHUE, a.TEN  FROM tcs_dm_lthue a"
        Try
            dsLoaiThue = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(drpLoaiThue, dsLoaiThue, 1, 0, "Tất cả")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Có lỗi trong quá trình lấy thông tin loại thuế", Session.Item("TEN_DN"))
            Throw ex
        Finally

        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        grdExport.Visible = True
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=ExportTraCuu.xls")
        Response.Charset = "utf-8"
        Response.ContentType = "application/vnd.ms-excel"
        Using sw1 As New StringWriter()
            Dim hw1 As New HtmlTextWriter(sw1)
            grdExport.RenderControl(hw1)
            Response.Output.Write(sw1.ToString)
            Response.Flush()
            Response.End()
        End Using
        grdExport.Visible = False
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    ' tuanda
    ' tìm kiếm theo chi nhánh
    Private Sub DM_DiemThu()
        Dim dsDiemThu As DataSet
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        ' Dim item As ListItem
        'strcheckHSC = checkHSC()
        'If strcheckHSC = "1" Then
        '    strSQL = "SELECT a.id, a.id ||'-'||a.name nam FROM tcs_dm_chinhanh a  "
        'Else
        '    strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        'End If
        strSQL = "SELECT a.MA_GIANTIEP id, a.MA_GIANTIEP ||'-'||a.TEN_GIANTIEP name FROM tcs_dm_nh_giantiep_tructiep a"
        Try
            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(txtMaCN, dsDiemThu, 1, 0, "Tất cả")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình lấy thông tin trạng thái chứng từ", Session.Item("TEN_DN"))
            Throw ex
        Finally

        End Try

    End Sub

    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub grdExport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdExport.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lblTotal As Label = DirectCast(e.Row.FindControl("totalAmount"), Label)
            lblTotal.Text = total.ToString("##,##") & " / " & String.Format("{0:n2}", totalUSD)
            Dim lblTotalCT As Label = DirectCast(e.Row.FindControl("totalCT"), Label)
            lblTotalCT.Text = totalRow.ToString()
        End If
    End Sub
End Class
