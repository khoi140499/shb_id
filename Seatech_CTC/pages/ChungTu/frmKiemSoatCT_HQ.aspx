﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmKiemSoatCT_HQ.aspx.vb" Inherits="pages_ChungTu_frmKiemSoatCT_HQ"
    Title="Kiểm soát chứng từ hải quan-Hệ thống thu thuế tập trung" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../../javascript/jquery/jquery-1.3.2.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
    
        $(document).ready(function(){
            
        });
        function jsShowCustomerSignature()
            {        
                if  ($get('<%=txtTK_KH_NH.clientID%>').value !='' )
                {
                    window.showModalDialog("../../Find_DM/ShowSignature.aspx?account=" + $get('<%=txtTK_KH_NH.clientID%>').value, "", "dialogWidth:700px;dialogHeight:500px;help:0;status:0;","_new");
                }
                else{
                     alert('Phải truy vấn được thông tin khách hàng trước khi xem chữ ký');}
            }
        function DG_changeBackColor(row, highlight) {
            if (highlight) {
                row.style.cursor = "hand";
                lastColor = row.style.backgroundColor;
                row.style.backgroundColor = '#C6D6FF';
            }
            else
                row.style.backgroundColor = lastColor;
        };


        function jsFormatNumber(txtCtl) {
            document.getElementById(txtCtl).value = localeNumberFormat(document.getElementById(txtCtl).value.replaceAll('.', ''), '.');
        }
        
        function pageLoad() {
        window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        }
        function EndRequestHandler()
        {
            var v_inCTU = $get('<%=hdfInCTU.clientID%>').value; 
        
        if (v_inCTU == "1"){
            $get('<%=hdfInCTU.clientID%>').value = "0";
        
            var v_soCT,v_shKB,v_soBT,v_ngayKB,v_maNV;
            v_soCT = $get('<%=txtSoCT.clientID%>').value;
            v_shKB = $get('<%=txtSHKB.clientID%>').value;
            v_soBT = $get('<%=hdfSo_BT.clientID%>').value;
            v_ngayKB = $get('<%=txtNGAY_KH_NH.clientID%>').value;
            v_maNV = $get('<%=hdfMa_NV.clientID%>').value;
            
            
              //  window.open ("../../pages/Baocao/frmInGNT.aspx?SoCT=" + v_soCT +"&SHKB=" + v_shKB + "&SoBT="+ v_soBT +"&NgayKB=" + v_ngayKB + "&MaNV=" + v_maNV + "&BS=1");
            window.open("../../pages/Baocao/InGNTpdf.ashx?SoCT=" + v_soCT + "&SHKB=" + v_shKB + "&SoBT=" + v_soBT + "&NgayKB=" + v_ngayKB + "&MaNV=" + v_maNV + "&BS=1");
        }
        }
        function XacNhan() {
            var confirm_value = document.createElement("INPUT");
            if(document.getElementById('<%=cmdKS.clientID%>').value != '0' && document.getElementById('<%=cmdKS.clientID%>').value != ''){
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Tờ khai này đã tồn tại trong hệ thống. Bạn có muốn tiếp tục ?")) {
                confirm_value.value = "Có";
            } else {
                confirm_value.value = "Không";
            }
            document.forms[0].appendChild(confirm_value);
            }else{
                confirm_value.value = "Có";
            }
        }
        function localeNumberFormat(amount, delimiter) {
            try {

                amount = parseFloat(amount);
            } catch (e) {
                throw ('localeNumberFormat caused INVALID FLOAT with value ' + amount)
                return null;
            }
            if (delimiter == null || delimiter == undefined) { delimiter = '.'; }

            // convert to string
            if (amount.match != 'function') { amount = amount.toString(); }

            // validate as numeric
            var regIsNumeric = /[^\d,\.-]+/igm;
            var results = amount.match(regIsNumeric);

            if (results != null) {
                outputText('INVALID NUMBER', eOutput)
                return null;
            }
        }
         function disableButton() {
                document.getElementById('<%=cmdKS.clientID%>').disabled=true;
                document.getElementById('<%=cmdChuyenTra.clientID%>').disabled=true;
                document.getElementById('<%=cmdChuyenThue.clientID%>').disabled=true;
                document.getElementById('<%=cmdHuy.clientID%>').disabled=true;
            }
            
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">KIỂM SOÁT CHỨNG TỪ HẢI QUAN</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr height="8px">
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td width='100%' align="center" valign="top" style="padding-left: 3; padding-right: 3">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width='900' align="left" valign="top" class="frame">
                                        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                                            <ContentTemplate>
                                                <asp:HiddenField ID="hdfMaNNT" runat="server" Value="" />
                                                <asp:HiddenField ID="hdfSHKB" runat="server" Value="" />
                                                <asp:HiddenField ID="hdfNgay_KB" runat="server" Value="" />
                                                <asp:HiddenField ID="hdfSo_CT" runat="server" Value="" />
                                                <asp:HiddenField ID="hdfKyHieu_CT" runat="server" Value="" />
                                                <asp:HiddenField ID="hdfSo_BT" runat="server" Value="" />
                                                <asp:HiddenField ID="hdfMa_Dthu" runat="server" Value="" />
                                                <asp:HiddenField ID="hdfMa_NV" runat="server" Value="" />
                                                <asp:HiddenField ID="hdfTrangThai_CT" runat="server" Value="" />
                                                 <asp:HiddenField ID="hdfTrangThai_CThue" runat="server" Value="" />
                                                 <asp:HiddenField ID="hdfExistCT" runat="server" Value="" />
                                                 <asp:HiddenField ID="hdfInCTU" runat="server" Value="" />
                                                <table width="100%" align="center">
                                                    <tr>
                                                        <td valign="top" width="210px" height="300px" align="left">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr align="left">
                                                                    <td>
                                                                        <asp:GridView ID="grdDSCT" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                                                            ShowHeader="True" ShowFooter="True" BorderColor="#989898" CssClass="grid_data"
                                                                            PageSize="15" Width="100%">
                                                                            <PagerStyle CssClass="cssPager" />
                                                                            <AlternatingRowStyle CssClass="grid_item_alter" />
                                                                            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header" HorizontalAlign="Left"></HeaderStyle>
                                                                            <RowStyle CssClass="grid_item" />
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="TT">
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                    <ItemTemplate>
                                                                                        <img src=" <%#Business.CTuCommon.Get_ImgTrangThai(Eval("TRANG_THAI").ToString(),Eval("TT_CTHUE").ToString(),Eval("Ma_KS").ToString())%>"
                                                                                            alt=" <%#Business.CTuCommon.Get_TrangThai(Eval("TRANG_THAI").ToString())%>" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Số CT" FooterText="Tổng số CT:">
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <FooterStyle HorizontalAlign="Left"></FooterStyle>
                                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="btnSelectCT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "so_ct") %>'
                                                                                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SHKB") & "," & DataBinder.Eval(Container.DataItem, "NGAY_KB")& "," & DataBinder.Eval(Container.DataItem, "SO_CT") & "," & DataBinder.Eval(Container.DataItem, "KYHIEU_CT") & "," & DataBinder.Eval(Container.DataItem, "SO_BT") & "," & DataBinder.Eval(Container.DataItem, "MA_DTHU") & "," & DataBinder.Eval(Container.DataItem, "MA_NV") & "," & Eval("TRANG_THAI").ToString() & "," & Eval("MA_NNTHUE").ToString() %>'
                                                                                            OnClick="btnSelectCT_Click" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Tên ĐN/Số BT">
                                                                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                                    <FooterStyle Font-Bold="true" />
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblMaNV" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TEN_DN") %>' />
                                                                                        <asp:Label ID="lblDelimiter" runat="server" Text='/' />
                                                                                        <asp:Label ID="lblSoBT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SO_BT") %>' />
                                                                                    </ItemTemplate>
                                                                                    <FooterTemplate>
                                                                                        <%#grdDSCT.Rows.Count.ToString%>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <PagerStyle HorizontalAlign="Left" BackColor="#E4E5D7"></PagerStyle>
                                                                        </asp:GridView>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" width='100%' class="nav">
                                                                        Trạng thái chứng từ:
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" valign="top" width='100%'>
                                                                        <asp:DropDownList ID="ddlTrangThai" runat="server" CssClass="inputflat" Width="100%"
                                                                            AutoPostBack="true">
                                                                            <asp:ListItem Value="99" Text="Tất cả"></asp:ListItem>
                                                                           <%-- <asp:ListItem Value="00" Text="Chưa kiểm soát"></asp:ListItem>--%>
                                                                            <asp:ListItem Value="05" Text="Chờ kiểm soát"></asp:ListItem>
                                                                            <asp:ListItem Value="01" Text="Đã kiểm soát"></asp:ListItem>
                                                                            <asp:ListItem Value="03" Text="Chuyển trả"></asp:ListItem>
                                                                            <asp:ListItem Value="04" Text="Đã hủy bởi GDV"></asp:ListItem>
                                                                            <asp:ListItem Value="02" Text="Hủy bởi GDV chờ duyệt"></asp:ListItem>
                                                                            <asp:ListItem Value="08" Text="Hủy chuyển thuế lỗi"></asp:ListItem>                                                                            
                                                                            <asp:ListItem Value="07" Text="Đã hủy bởi KSV"></asp:ListItem>
                                                                            <asp:ListItem Value="06" Text="Chuyển thuế lỗi"></asp:ListItem>
                                                                                                                                                                                                                                 
                                                                            <%--<asp:ListItem Value="08" Text="Hủy chuyển HQ lỗi"></asp:ListItem>--%>
                                                                        </asp:DropDownList>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr class="img">
                                                                    <td align="left" style="width: 100%" colspan="3">
                                                                         <hr style="width:50%" />
                                                                    </td>
                                                                </tr>
                                                                <tr class="img">
                                                                    <td style="width: 100%" colspan="3">
                                                                        <table>
                                                                            <tr style="display:none">
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/ChuaKS.png" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    Chưa Kiểm soát
                                                                                </td>
                                                                            </tr>
                                                                            
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/ChuyenKS.png" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    Chờ Kiểm soát
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/DaKS.png" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    Đã Kiểm soát
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/KSLoi.png" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    Chuyển trả
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/Huy.png" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    CT bị Hủy bởi GDV
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/HuyHS.gif" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    CT Hủy bởi GDV - chờ duyệt
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/Huy_CT_Loi.png" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    Duyệt hủy chuyển thuế lỗi
                                                                                </td>
                                                                            </tr>
                                                                             <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/Huy_KS.png" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    CT bị Hủy bởi KSV
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/TT_CThueLoi.gif" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    Chuyển thuế/HQ lỗi
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="display:none">
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/Huy_CT_Loi.png" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    Hủy chuyển thuế/HQ lỗi
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr class="img">
                                                                    <td align="left" style="width: 100%" colspan="3">
                                                                         <hr style="width:50%" />
                                                                    </td>
                                                                </tr>
                                                                <tr class="img">
                                                                    <td style="width: 100%" colspan="3">
                                                                        Alt + K : Kiểm soát<br />
                                                                        Alt + C : Chuyển trả
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td valign="top" width="680px">
                                                            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                                                <tr>
                                                                    <td colspan="2" width='100%' align="left">
                                                                        <table width="100%">
                                                                            <tr class="grid_header">
                                                                                <td height="15px" align="right">
                                                                                    Trường <span style="color:#FF0000;font-weight:bold">(*)</span> là bắt buộc nhập
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div id="Panel1" style="height: 380px; width: 100%; overflow-y: scroll;">
                                                                            <table runat="server" id="grdHeader" cellspacing="0" cellpadding="1" rules="all"
                                                                                border="1" style="border-color: #989898; border-style: solid; font-family: Verdana;
                                                                                font-size: 8pt; width: 99%; border-collapse: collapse;margin-left:2px;">
                                                                                <tr id="rowKHCT">
                                                                                    <td width="24%">
                                                                                        <b>Mã hiệu CT/Số CT </b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td width="25%">
                                                                                        <asp:TextBox runat="server" ID="txtKHCT" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                    <td width="51%">
                                                                                        <asp:TextBox runat="server" ID="txtSoCT" CssClass="inputflat" ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowMaNNT">
                                                                                    <td width="24%">
                                                                                        <b>Mã số thuế/Tên </b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td width="25%">
                                                                                        <asp:TextBox runat="server" ID="txtMaNNT" CssClass="inputflat3" ReadOnly="true" BackColor="Aqua"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                    <td width="51%">
                                                                                        <asp:TextBox runat="server" ID="txtTenNNT" CssClass="inputflat" ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowToKhaiSo">
                                                                                    <td>
                                                                                        <b>Tờ khai số</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtToKhaiSo" CssClass="inputflat3" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtDescToKhaiSo" runat="server" Width="99%" CssClass="inputflat">
                                                                                        </asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowNgayDK">
                                                                                    <td>
                                                                                        <b>Ngày đăng ký</b>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox runat="server" ID="txtNgayDK" CssClass="inputflat3" ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowLHXNK">
                                                                                    <td>
                                                                                        <b>Loại hình XNK</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtLHXNK" CssClass="inputflat"  ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtDescLHXNK" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowMaHQ">
                                                                                    <td>
                                                                                        <b>Mã Hải Quan</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtMaHQ" CssClass="inputflat"   ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtDescMaHQ" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowDChiNNT">
                                                                                    <td>
                                                                                        <b>Địa chỉ người nộp thuế</b>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox runat="server" ID="txtDChiNNT" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowQuanNNThue">
                                                                                    <td>
                                                                                        <b>Quận(Huyện)</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtQuanNNThue" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTenQuanNNThue" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowTinhNNThue">
                                                                                    <td>
                                                                                        <b>Thành phố(Tỉnh)</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTinhNNThue" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTenTinhNNThue" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowNNTien">
                                                                                    <td>
                                                                                        <b>MST /Tên người nộp tiền</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtMaNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTenNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowDChiNNTien">
                                                                                    <td>
                                                                                        <b>Địa chỉ người nộp tiền</b>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox runat="server" ID="txtDChiNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                  <tr id="rowQuan_HuyenNNTien">
                                                                                    <td>
                                                                                        <b>Quận(huyện)</b>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox runat="server" ID="txtQuan_HuyenNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowTinhNNTien">
                                                                                    <td>
                                                                                        <b>Thành phố(Tỉnh)</b>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox runat="server" ID="txtTinh_TphoNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowCQThu">
                                                                                    <td>
                                                                                        <b>CQ Thu </b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtMaCQThu" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTenCQThu" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowMaHQPH">
                                                                                    <td>
                                                                                        <b>Mã HQ PH</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtMaHQPH" CssClass="inputflat" ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTenMaHQPH" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>   
                                                                                <tr id="rowDBHC" style="display:none">
                                                                                    <td style="display:none">
                                                                                        <b>ĐBHC </b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td style="display:none">
                                                                                        <asp:TextBox runat="server" ID="txtMaDBHC" CssClass="inputflat" ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                    <td style="display:none">
                                                                                        <asp:TextBox runat="server" ID="txtTenDBHC" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>                                                                             
                                                                                <tr id="rowSHKB">
                                                                                    <td width="24%">
                                                                                        <b>Số hiệu KB </b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td width="25%">
                                                                                        <asp:TextBox runat="server" ID="txtSHKB" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                    <td width="51%">
                                                                                        <asp:TextBox runat="server" ID="txtTenKB" CssClass="inputflat" ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                
                                                                                <tr id="rowTKCo">
                                                                                    <td style="background-color: Pink">
                                                                                        <b>TK Có NSNN </b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTenTKCo" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTKCo" CssClass="inputflat" ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                    
                                                                                </tr>
                                                                                <tr id="rowHTTT">
                                                                                    <td>
                                                                                        <b>Hình thức thanh toán</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList runat="server" ID="ddlHTTT" CssClass="inputflat" Enabled="false"
                                                                                            Width="100%">
                                                                                            <asp:ListItem Value="00" Text="Nộp tiền mặt" Selected="True"></asp:ListItem>
                                                                                            <asp:ListItem Value="01" Text="Chuyển khoản"></asp:ListItem>
                                                                                            <asp:ListItem Value="02" Text="Điện chuyển tiền"></asp:ListItem>
                                                                                            <asp:ListItem Value="03" Text="Chuyển khoản nội bộ"></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTenHTTT" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowTKGL">
                                                                                    <td>
                                                                                        <b>TK Chuyển </b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTKGL" CssClass="inputflat" ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTenTKGL" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                            
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowTK_KH_NH">
                                                                                    <td>
                                                                                        <b>TK Chuyển </b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTK_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTenTK_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="90%" />
                                                                                            <%--<img id="img1" alt="Chọn từ danh mục đặc biệt" src="../../images/icons/MaDB.png"
                                                                                    onclick="jsShowCustomerSignature();" style="width: 16px; height: 16px" />--%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowSoDuNH">
                                                                                    <td>
                                                                                        <b>Số dư NH</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtSoDu_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtDesc_SoDu_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowNgay_KH_NH">
                                                                                    <td>
                                                                                        <b>Ngày chuyển</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtNGAY_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtDesNGAY_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowNganHangChuyen">
                                                                                    <td>
                                                                                        <b>Mã NH Chuyển </b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtMA_NH_A" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTen_NH_A" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr> 
                                                                                <tr id="rowNhanHangTT">
                                                                                    <td>
                                                                                        <b>Mã NH TT </b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtMA_NH_TT" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTEN_NH_TT" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>                                                                               
                                                                                <tr id="rowNganHangNhan">
                                                                                    <td>
                                                                                        <b>Mã NH TH </b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtMA_NH_B" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTen_NH_B" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowMaNT">
                                                                                    <td>
                                                                                        <b>Loại tiền tệ</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtMaNT" CssClass="inputflat" ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTenNT" CssClass="inputflat" ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowLoaiTienThue">
                                                                                    <td>
                                                                                        <b>Loại tiền thuế</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="ddlMaLoaiTienThue" runat="server" CssClass="inputflat" Enabled="false">
                                                                                            <asp:ListItem Text="Thuế" Value="1"></asp:ListItem>
                                                                                            <asp:ListItem Text="Truy thu Thuế" Value="2"></asp:ListItem>
                                                                                            <asp:ListItem Text="Phạt chậm nộp Thuế" Value="3"></asp:ListItem>
                                                                                            <asp:ListItem Text="Lệ Phí HQ" Value="4"></asp:ListItem>
                                                                                            <asp:ListItem Text="Phạt Vi phạm hành chính" Value="5"></asp:ListItem>
                                                                                        </asp:DropDownList>                                                                                        
                                                                                    </td>
                                                                                    <td>
                                                                                        <%--<input type="text" id="txtTenLoaiThue" class="inputflat" style="width: 100%;" disabled="true"
                                                                                    readonly="true" value="Thuế Hải Quan" />--%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="Tr1">
                                                                                    <td>
                                                                                        <b>Diễn giải Hải Quan</b>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox runat="server" ID="txtDienGiai" CssClass="inputflat" ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                    
                                                                                </tr>
                                                                                <tr id="rowLoaiThue" style="display: none;">
                                                                                    <td>
                                                                                        <b>Loại thuế</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList runat="server" ID="ddlLoaiThue" CssClass="inputflat" Enabled="false"
                                                                                            Width="99%">
                                                                                            <%--<asp:ListItem Value="01" Text="01" Selected="True"></asp:ListItem>
                                                                                            <asp:ListItem Value="03" Text="03"></asp:ListItem>
                                                                                            <asp:ListItem Value="04" Text="04"></asp:ListItem>
                                                                                            <asp:ListItem Value="05" Text="05"></asp:ListItem>--%>
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtDescLoaiThue" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowSoBK" style="display:none">
                                                                                    <td>
                                                                                        <b>Số bảng kê</b>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox runat="server" ID="txtSo_BK" CssClass="inputflat" ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowNgayBK" style="display:none">
                                                                                    <td>
                                                                                        <b>Ngày bảng kê</b>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox runat="server" ID="txtNgay_BK" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                               
                                                                               
                                                                                <tr id="rowTKNo" style="display: none">
                                                                                    <td style="background-color: Aqua">
                                                                                        <b>TK Nợ NSNN </b><span class="requiredField">(*)</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTKNo" CssClass="inputflat" ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtTenTKNo" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                
                                                                                <tr id="rowSoKhung">
                                                                                    <td>
                                                                                        <b>Số khung</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtSoKhung" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="100%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtDescSoKhung" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowSoMay">
                                                                                    <td>
                                                                                        <b>Số máy</b>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtSoMay" CssClass="inputflat" ReadOnly="true" Width="99%" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtDescSoMay" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr style="visibility: hidden">
                                                                    <td width="25%" style="height: 0px">
                                                                        <asp:TextBox runat="server" ID="txtSo_BT" CssClass="inputflat" ReadOnly="true" Visible="false"
                                                                            Width="99%" />
                                                                        <asp:TextBox runat="server" ID="txtSoCT1" CssClass="inputflat" ReadOnly="true" Visible="false"
                                                                            Width="99%" />
                                                                             <asp:TextBox runat="server" ID="txtSoCT_NH" CssClass="inputflat" ReadOnly="true" Visible="false"
                                                                            Width="99%" />
                                                                    </td>
                                                                    <td width="51%" style="height: 0px">
                                                                        <asp:TextBox runat="server" ID="txtTRANG_THAI" CssClass="inputflat" ReadOnly="true"
                                                                            Visible="false" Width="99%" />
                                                                        <asp:TextBox runat="server" ID="txtMA_NV" CssClass="inputflat" ReadOnly="true" Visible="false"
                                                                            Width="99%" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan='2' align="left" class="errorMessage">
                                                                        <table width="100%">
                                                                            <tr>
                                                                               
                                                                                <td width="100%" align="right">
                                                                                     <asp:Label ID="lbSoFT" runat="server" style="font-weight: bold;"  ></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" width='99%' colspan="2">
                                                                                    <asp:UpdateProgress ID="updProgress" runat="server">
                                                                                        <ProgressTemplate>
                                                                                            <div style="background-color: Aqua;">
                                                                                                <b style="font-size: 15pt">Đang lấy dữ liệu tại server.Xin chờ một lát...</b>
                                                                                            </div>
                                                                                        </ProgressTemplate>
                                                                                    </asp:UpdateProgress>
                                                                                    <b>
                                                                                        <asp:Literal ID="lblStatus" runat="server"></asp:Literal></b>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" width="100%">
                                                                        <asp:DataGrid ID="grdChiTiet" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                            ShowHeader="true" Width="100%" CssClass="grid_data">
                                                                            <HeaderStyle CssClass="grid_header"></HeaderStyle>
                                                                            <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                                                                            <ItemStyle CssClass="grid_item" />
                                                                            <Columns>
                                                                                <asp:TemplateColumn>
                                                                                    <HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkRemove" runat="server" Enabled="false" Checked="true" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Mã quỹ" Visible="false">
                                                                                    <HeaderStyle HorizontalAlign="Center" Width="0px"></HeaderStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="MA_CAP" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                            ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MAQUY") %>'
                                                                                            Font-Bold="true" MaxLength="2" ToolTip="Ấn phím Enter để chọn mã cấp"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Chương" ItemStyle-HorizontalAlign="Center">
                                                                                    <HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="MA_CHUONG" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                            Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_CHUONG") %>'
                                                                                            Font-Bold="true" MaxLength="3" ToolTip="Ấn phím Enter để chọn mã chương" onkeyup="return valInteger(this)"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Khoản" ItemStyle-HorizontalAlign="Center" Visible="false">
                                                                                    <HeaderStyle HorizontalAlign="Center" Width="0px"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="MA_KHOAN" runat="server" Width="0%" BorderColor="white" CssClass="inputflat"
                                                                                            Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_KHOAN") %>'
                                                                                            Font-Bold="true" MaxLength="3" ToolTip="Ấn phím Enter để chọn mã khoản" onkeyup="return valInteger(this)"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Tiểu Mục" ItemStyle-HorizontalAlign="Center">
                                                                                    <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="MA_MUC" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                            Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_TMUC") %>'
                                                                                            Font-Bold="true" MaxLength="4" ToolTip="Ấn phím Enter để chọn mã tiểu mục" onkeyup="return valInteger(this)"
                                                                                            AutoPostBack="true"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Nội dung" ItemStyle-HorizontalAlign="Left">
                                                                                    <HeaderStyle HorizontalAlign="Center" Width="220px"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="NOI_DUNG" runat="server" Width="99%" BorderColor="white" CssClass="inputflat"
                                                                                            ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "NOI_DUNG") %>'
                                                                                            MaxLength="200" Font-Bold="true"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Số tiền" ItemStyle-HorizontalAlign="Right">
                                                                                    <HeaderStyle HorizontalAlign="Center" Width="70px"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="right" VerticalAlign="Middle"></ItemStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="SOTIEN" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                            Style="text-align: right" ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "TTIEN") %>'
                                                                                            AutoPostBack="true" onfocus="this.select()" Font-Bold="true">
                                                                                        </asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Số ĐT" >
                                                                                    <HeaderStyle HorizontalAlign="Center" Width="70px"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="SODATHU" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                            Text='<%# DataBinder.Eval(Container.DataItem, "SODATHUTK") %>' ReadOnly="true"
                                                                                            Font-Bold="true"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Kỳ thuế" ItemStyle-HorizontalAlign="Right" Visible="false">
                                                                                    <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                                                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:TextBox ID="KY_THUE" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                            Style="text-align: right" ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "KY_THUE") %>'
                                                                                            Font-Bold="true"></asp:TextBox>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan='2' height='5'>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" colspan="2">
                                                                        <table width="500px">
                                                                            <tr align="right">
                                                                                <td>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="txtRM_REF_NO" CssClass="inputflat" Visible="false"
                                                                                        ReadOnly="true" Text="" />
                                                                                    <asp:TextBox runat="server" ID="txtREF_NO" CssClass="inputflat" Visible="false" ReadOnly="true"
                                                                                        Text="" />
                                                                                        <asp:TextBox runat="server" ID="txtTimeLap" CssClass="inputflat"  Visible="false" ReadOnly="true" Text="" />
                                                                                </td>
                                                                                 <td  align="left"  style="display:none;">PT tính phí
                                                                                  <asp:RadioButton id="rdTypeMP" Text="Miễn phí"   GroupName="RadioGroup" runat="server" Enabled=false  />
                                                                                  <asp:RadioButton id="rdTypePT" Text="Phí trong" Visible="false"  GroupName="RadioGroup" runat="server" Enabled=false/>
                                                                                  <asp:RadioButton id="rdTypePN" Text="Phí ngoài"  GroupName="RadioGroup" runat="server" Enabled=false/>
                                                                             </td>
                                                                                <td>
                                                                                    Tổng Tiền:
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="txtTTIEN" CssClass="inputflat" ReadOnly="true" Text="0" style="text-align:right" />
                                                                                </td>
                                                                            </tr>
                                                                            <%--<tr align="right">
                                                                                <td align="right" valign="top">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td align="right" valign="top">
                                                                                    Số đã thu: &nbsp;
                                                                                    <asp:TextBox runat="server" ID="txtTT_TTHU" CssClass="inputflat" ReadOnly="true"
                                                                                        Text="0" />
                                                                                </td>
                                                                            </tr>--%>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr >
                                                                    <td align="right" colspan="2">
                                                                        <table width="100%">
                                                                            <tr  style="display:none;">
                                                                                
                                                                                   
                                                                                <td align="right"> Phí
                                                                                    <asp:TextBox runat="server" ID="txtCharge" Width="100px" CssClass="inputflat" ReadOnly="true" Style="text-align: right" Text="0" />
                                                                                </td>
                                                                                    
                                                                                <td align="right">VAT
                                                                                    <asp:TextBox runat="server" ID="txtVAT" Width="100px" CssClass="inputflat" ReadOnly="true" Style="text-align: right"  Text="0" />
                                                                                </td>
                                                                                 <td align="right" > Sản phẩm
                                                                                    <asp:TextBox ID="txtSanPham" Text="" runat="server" Width="80px" CssClass="inputflat" ReadOnly="true"></asp:TextBox>
                                                                                
                                                                                <td align="right">Tổng trích nợ
                                                                                    <asp:TextBox runat="server" ID="txtTongTrichNo" Width="120px" CssClass="inputflat" Style="text-align: right" 
                                                                                        ReadOnly="true" Text="0" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr align="right" style="display:none">
                                                                               <td  align="left" colspan="3">Đi Citad
                                                                              <asp:RadioButton id="RadioButton1" Text="Kiểu mới"   GroupName="RGCitad" runat="server" Enabled=false  />
                                                                              <asp:RadioButton id="RadioButton2" Text="Kiểu cũ" GroupName="RGCitad" runat="server" Enabled=false/>
                                                                             </td>
                                                                            </tr>
                                                                            <tr align="right" >
                                                                            <td>
                                                                                <b>Ghi chú:</b>
                                                                            </td>
                                                                            <td align="right" valign="top" colspan="4">
                                                                                <asp:TextBox runat="server" ID="txtGhiChu" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="99%" />
                                                                                    <%--Lý do hủy: &nbsp;--%>
                                                                                    <asp:DropDownList ID="drpLyDoHuy" Visible="false" runat="server" Style="width: 60%" CssClass="inputflat">
                                                                                        <asp:ListItem Value="0">Không có trong dữ liệu HQ/NH </asp:ListItem>
                                                                                        <asp:ListItem Value="2">Điện thừa</asp:ListItem>
                                                                                        <asp:ListItem Value="3">Lỗi dữ liệu</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                             <tr id="rowDienGiai">
                                                                                    <td>
                                                                                        
                                                                                    </td>
                                                                                    <td colspan="3">
                                                                                        
                                                                                    </td>
                                                                                </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan='2' height='5'>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                                                <tr align="left">
                                                                     <td>
                                                                        <table width ="200">
                                                                             <tr style="display:none;">
                                                                           <td align="left" style="height:30;width:130px">
                                                                             Mật khẩu Kiểm soát <span class="requiredField">(*)</span>:
                                                                             </td><td valign="top">
                                                                            <asp:TextBox runat="server" ID="txtMatKhau"  Width="70px"  Text="" TextMode="Password"  style="height:30;background-color:Silver " />
                                                                             </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td align="left" valign="top" class='text'>
                                                                        <asp:Button ID="cmdKS" runat="server" Text="[(K)iểm soát]" AccessKey="K" CssClass="ButtonCommand" UseSubmitBehavior="false" OnClientClick="disableButton();XacNhan();" />
                                                                        &nbsp;
                                                                        <asp:Button ID="cmdChuyenTra" runat="server" Text="[(C)huyển trả]" AccessKey="C"
                                                                            CssClass="ButtonCommand" UseSubmitBehavior="false" OnClientClick="disableButton();"/>
                                                                        &nbsp;
                                                                        <asp:Button ID="cmdHuy" runat="server" Text="[Duyệt CT(H)ủy]" AccessKey="H" CssClass="ButtonCommand" UseSubmitBehavior="false" OnClientClick="disableButton();" />&nbsp;
                                                                        <asp:Button ID="cmdChuyenThue" runat="server" Text="[(G)ửi lại]" AccessKey="G" CssClass="ButtonCommand" UseSubmitBehavior="false" OnClientClick="disableButton();"/>&nbsp;
                                                                        <asp:Button ID="cmdINCT" runat="server" Text="[In CT]" CssClass="ButtonCommand"
                                                                            UseSubmitBehavior="false" OnClientClick="disableButton();" />&nbsp;
                                                                        <asp:Button ID="cmdChuyenBDS" runat="server" Text="[Chuyển (B)DS]" AccessKey="B" CssClass="ButtonCommand" style="display:none" UseSubmitBehavior="false" OnClientClick="disableButton();"/>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr height="5">
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
