﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    EnableEventValidation="true" CodeFile="frmLapCT.aspx.vb" Inherits="pages_ChungTu_frmLapCT"
    Title="Lập chứng từ thuế" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>

    <script language="javascript" src="../../images/javascript/jquery/script/default/jquery-1.4.3.min.js"
        type="text/javascript"></script>

    <script src="../../javascript/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>

    <script src="../../javascript/jquery/1.11.2/jquery-ui.min.js" type="text/javascript"></script>

    <script src="../../javascript/json/json2.js" type="text/javascript"></script>

    <script src="../../javascript/accounting.min.js" type="text/javascript"></script>

    <script src="../../javascript/object/chungTu.js" type="text/javascript"></script>

    <script src="../../javascript/table/paging.js" type="text/javascript"></script>

    <link href="../../javascript/table/paging.css" rel="stylesheet" type="text/css" />

     <script type="text/javascript" language="javascript">
        var charNumberLimit = 210;
        var feeRate = 2;
        var minFee = 10000;
        var charNumber = 210;//200;
        var charRemark = 0;
        var invalidCharacter = '';
        var temp_arrDBHC;
        var temp_arrSHKB;
        var temp_arrCQThu;
        var temp_arrTKNo;
        var temp_arrTKCo;
        var arrCQThu_SHKB;
        var flag_CQThu = 0;
        var ma_nnthue_loi = '';
        //PG BANK

        var _SHKB;
        var _TKNH;
        var _TTIEN = 0;
        var _PPTP;


        function pageLoad() {
            Sys.Net.WebRequestManager.add_invokingRequest(onInvoke);
            Sys.Net.WebRequestManager.add_completedRequest(onComplete);
        }
        function onInvoke(sender, asHgs) {
            document.getElementById('divProgress').style.display = '';
        }

        function onComplete(sender, args) {
            document.getElementById('divProgress').style.display = 'none';
        }
        function pageUnload() {
            Sys.Net.WebRequestManager.remove_invokingRequest(onInvoke);
            Sys.Net.WebRequestManager.remove_completedRequest(onComplete);
        }

        function DG_changeBackColor(row, highlight) {
            if (highlight) {
                row.style.cursor = "hand";
                lastColor = row.style.backgroundColor;
            }
            else
                row.style.backgroundColor = lastColor;
        };

        function IsNumeric(strString)//  check for valid numeric strings	     
        {
            var strValidChars = "0123456789.-";
            var strChar;
            var blnResult = true;

            if (strString.length == 0) return false;

            //  test strString consists of valid characters listed above
            for (i = 0; i < strString.length && blnResult == true; i++) {
                strChar = strString.charAt(i);
                if (strValidChars.indexOf(strChar) == -1) {
                    blnResult = false;
                }
            }
            return blnResult;
        }

        function isValidCitadCharacter(strString) {
            var iChars = "#\\!@#$%^*+=[]\\\'{}|\"<>?~_";
            for (var i = 0; i < strString.length; i++) {
                if (iChars.indexOf(strString.charAt(i)) != -1) {
                    return false;
                }
            }
            return true;
        }
        function isValidCitadCharacterNoiDung(strString) {
            var iChars = "";
            for (var i = 0; i < strString.length; i++) {
                if (iChars.indexOf(strString.charAt(i)) != -1) {
                    return false;
                }
            }
            return true;
        }
        function jsCalCharacter_V20() {
            var tempCharater = '';
            charRemark = 0;
            if (document.getElementById("ddlMaLoaiThue").value == '04') {
                charRemark = 24;
                tempCharater += $get('txtToKhaiSo').value;
                tempCharater += $get('txtNgayDK').value;
                tempCharater += $get('txtLHXNK').value;
            }
            if (document.getElementById("ddlMaLoaiThue").value == '03') {
                charRemark += 8;
                tempCharater += $get('txtSoKhung').value.trim();
                tempCharater += $get('txtSoMay').value.trim();
            }
            if (document.getElementById("ddlMaLoaiThue").value == '07') {
                charRemark += 20;
                tempCharater += $get('txtCQQD').value.trim();
                tempCharater += $get('txtSOQD').value.trim();
                tempCharater += $get('txtNgayQD').value.trim();
            }


            charRemark = charRemark + 6;//.DNCT
            tempCharater += $get('txtGhiChu').value;
            $get('txtRemarks').value = tempCharater;
            document.getElementById('leftCharacter').visible = false;
            charNumber = charNumberLimit - charRemark - tempCharater.length + 1;
            document.getElementById('leftCharacter').innerHTML = charNumber;
            if (charNumber < 0) {
                return false;
            }
            else {
                return true;
            }

        }
        function jsCalCharacter() {
            //        alert ("jsCalcharacter");
            if (v_coreVersion == "2") {
                return jsCalCharacter_V20();
            } else {
                var tempCharater = '';

                //Cộng phần head MST:..
                //Cộng kỳ thuế
                charRemark = 17;
                $get('txtMaNNT').value = $get('txtMaNNT').value;
                $get('txtTenNNT').value = $get('txtTenNNT').value;
                tempCharater += $get('txtMaNNT').value.trim();
                tempCharater += $get('txtTenNNT').value.trim();
                tempCharater += dtmNgayCore;
                //tempCharater += $get('txtNGAY_KH_NH').value;
                tempCharater += $get('ddlMaLoaiThue').value;

                //alert($get('txtMaNNT').value + $get('txtTenNNT').value + $get('txtNGAY_KH_NH').value + $get('ddlMaLoaiThue').value);

                if (document.getElementById("ddlMaLoaiThue").value == '04') {
                    charRemark = 24;
                    tempCharater += $get('txtToKhaiSo').value;
                    tempCharater += $get('txtNgayDK').value;
                    tempCharater += $get('txtLHXNK').value;
                }
                if (document.getElementById("ddlMaLoaiThue").value == '03') {
                    charRemark += 8;
                    tempCharater += $get('txtSoKhung').value.trim();
                    tempCharater += $get('txtSoMay').value.trim();
                }
                if (document.getElementById("ddlMaLoaiThue").value == '07') {
                    charRemark += 20;
                    tempCharater += $get('txtCQQD').value.trim();
                    tempCharater += $get('txtSOQD').value.trim();
                    tempCharater += $get('txtNgayQD').value.trim();
                }
                // tempCharater+=$get('txtMaNNT').value;

                if (document.getElementById("chkRemove1").checked && document.getElementById('SOTIEN_1').value.replaceAll('.', '') != 0) {
                    charRemark = charRemark + 14;
                    tempCharater += $get('MA_CHUONG_1').value;
                    tempCharater += $get('MA_MUC_1').value;
                    tempCharater += $get('NOI_DUNG_1').value;
                    tempCharater += $get('SOTIEN_1').value.replaceAll('.', '');
                    tempCharater += $get('KY_THUE_1').value;

                    //alert($get('MA_CHUONG_1').value + $get('MA_MUC_1').value + $get('NOI_DUNG_1').value + $get('SOTIEN_1').value + $get('KY_THUE_1').value);
                }
                if (document.getElementById("chkRemove2").checked && document.getElementById('SOTIEN_2').value.replaceAll('.', '') != 0) {
                    charRemark = charRemark + 14;
                    tempCharater += $get('MA_CHUONG_2').value;
                    tempCharater += $get('MA_MUC_2').value;
                    tempCharater += $get('NOI_DUNG_2').value;
                    tempCharater += $get('SOTIEN_2').value.replaceAll('.', '');
                    tempCharater += $get('KY_THUE_2').value;

                    // alert($get('MA_CHUONG_2').value + $get('MA_MUC_2').value + $get('NOI_DUNG_2').value + $get('SOTIEN_2').value + $get('KY_THUE_2').value);
                }
                if (document.getElementById("chkRemove3").checked && document.getElementById('SOTIEN_3').value.replaceAll('.', '') != 0) {
                    charRemark = charRemark + 14;
                    tempCharater += $get('MA_CHUONG_3').value;
                    tempCharater += $get('MA_MUC_3').value;
                    tempCharater += $get('NOI_DUNG_3').value;
                    tempCharater += $get('SOTIEN_3').value.replaceAll('.', '');
                    tempCharater += $get('KY_THUE_3').value;
                }
                if (document.getElementById("chkRemove4").checked && document.getElementById('SOTIEN_4').value.replaceAll('.', '') != 0) {
                    charRemark = charRemark + 14;
                    tempCharater += $get('MA_CHUONG_4').value;
                    tempCharater += $get('MA_MUC_4').value;
                    tempCharater += $get('NOI_DUNG_4').value;
                    tempCharater += $get('SOTIEN_4').value.replaceAll('.', '');
                    tempCharater += $get('KY_THUE_4').value;
                }
                tempCharater += $get('txtGhiChu').value;
                $get('txtRemarks').value = tempCharater;
                document.getElementById('leftCharacter').visible = false;
                charNumber = charNumberLimit - charRemark - tempCharater.length + 1;
                document.getElementById('leftCharacter').innerHTML = charNumber;
                if (charNumber < 0) {
                    return false;
                }
                else {
                    return true;
                }
            }
        }

        function IsValidCitadLength() {
            return true;
        }

        function checkMaNNT(obj) {
            ////        if((obj.value.trim()).length!=0 && (obj.value.trim()).length!=10 && (obj.value.trim()).length!=14)
            ////        {
            ////            obj.value = "";
            ////		    obj.focus();
            ////		    alert("Mã số thuế không hợp lệ!");
            ////		    return;
            ////        }
        }
        function valInteger(obj) {
            var i, strVal, blnChange;
            blnChange = false
            strVal = "";

            for (i = 0; i < (obj.value).length; i++) {
                switch (obj.value.charAt(i)) {
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9": strVal = strVal + obj.value.charAt(i);
                        break;
                    default: blnChange = true;
                        break;
                }
            }
            if (blnChange) {
                obj.value = strVal;
            }
        }

        function jsFormatNumber(txtCtl) {
            document.getElementById(txtCtl).value = localeNumberFormat(document.getElementById(txtCtl).value.replaceAll('.', ''), '.');
        }

        function localeNumberFormat(amount, delimiter) {
            try {

                amount = parseFloat(amount);
            } catch (e) {
                throw ('localeNumberFormat caused INVALID FLOAT with value ' + amount)
                return null;
            }
            if (delimiter == null || delimiter == undefined) { delimiter = '.'; }

            // convert to string
            if (amount.match != 'function') { amount = amount.toString(); }

            // validate as numeric
            var regIsNumeric = /[^\d,\.-]+/igm;
            var results = amount.match(regIsNumeric);

            if (results != null) {
                outputText('INVALID NUMBER', eOutput)
                return null;
            }

            var minus = amount.indexOf('-') >= 0 ? '-' : '';
            amount = amount.replace('-', '');
            var amtLen = amount.length;
            var decPoint = amount.indexOf(',');
            var wholeNumberEnd = decPoint > 0 ? amtLen - (amtLen - decPoint) : amtLen;

            var wholeNumber = amount.substr(0, wholeNumberEnd);

            var numberEnd = amount.substr(decPoint, amtLen);
            var fraction = amount.substr(wholeNumberEnd, amtLen - wholeNumberEnd);

            var segments = (wholeNumberEnd - (wholeNumberEnd % 3)) / 3;
            var rvsNumber = wholeNumber.split('').reverse().join('');
            var output = '';

            for (i = 0; i < wholeNumberEnd; i++) {
                if (i % 3 == 0 && i != 0 && i != wholeNumberEnd) { output += delimiter; }
                output += rvsNumber.charAt(i);
            }
            output = minus + output.split('').reverse().join('') + fraction;
            return output;
        }

        String.prototype.replaceAll = function (strTarget, strSubString) {
            var strText = this;
            var intIndexOfMatch = strText.indexOf(strTarget);
            while (intIndexOfMatch != -1) {
                strText = strText.replace(strTarget, strSubString)
                intIndexOfMatch = strText.indexOf(strTarget);
            }
            return (strText);
        }

        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }
        //    
        //    function jsNhapBDS() {
        ////        var TTBDS= curBDS_Status;
        ////        if (TTBDS=="F")
        ////        {
        ////            alert("BDS đang ở trạng thái offline!");
        ////            return;
        ////        }
        ////        if (document.getElementById('divDescCTU').innerHTML.length>0)
        ////        {
        ////            var desCTU= document.getElementById('divDescCTU').innerHTML.split(':')[1]; 
        ////            mangTS = desCTU.split('/');            
        ////        }else {
        ////            mangTS = "///".split('/');
        ////        }   
        ////        if (mangTS[3]=="F")
        ////        {
        ////            alert("Chứng từ này lập ở chế độ BDS offline!");
        ////            return;
        ////        }
        ////        if (mangTS[4]=="N")
        ////        {
        ////            //var returnValue = window.showModalDialog("../../pages/KhoQuy/frmNhapSoThucThu.aspx?SoCT=" + mangTS[1] +"&SHKB=" + mangTS[0]+ "&SoBT="+ mangTS[2], "","dialogWidth:810px;dialogHeight:650px;help:0;status:0;");               
        ////        }
        ////        else
        ////        {
        ////            //var returnValue = window.showModalDialog("../../pages/KhoQuy/frmNhapSoThucThu.aspx?SoCT=''", "","dialogWidth:810px;dialogHeight:650px;help:0;status:0;");
        ////        }
        ////        if( returnValue =="1"){ 
        ////        }else{
        ////        }                         
        //        //lay lai thong tin ve chung tu  
        //        jsLoadCTList();
        //        if (document.getElementById('divDescCTU').innerHTML.length>0)
        //        {                
        //            var strSHKB =document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[0];
        //            var strSoCT =document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[1];
        //            var strSo_BT =document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[2];
        //            jsGetCTU(strSHKB,strSoCT,strSo_BT);
        //        }
        //    }

        function FindNNT_NEW(txtID, txtTitle) {
            var returnValue = window.showModalDialog("../../Find_DM/Find_NNT_NEW.aspx?initParam=" + $get('txtMaNNT').value + "&SHKB=" + $get('txtSHKB').value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;
            }
            NextFocus();
        }
        function jsGetSignature() {
            if (document.getElementById('txtSoDu_KH_NH').value != '') {
                PageMethods.GetURLSignature($get('txtTK_KH_NH').value, FSuccess, FError);
            } else {
                alert('Không tìm được chữ ký')
            }
        }
        function FSuccess(result) {
            var url = result.split("#");
            if (url.length >= 2) {
                var str = new Array(url);
                for (var i = 0; i < url.length - 2; i++) {
                    window.showModalDialog(url[0] + url[i + 1], "Chữ ký mẫu", "dialogWidth:810px;dialogHeight:650px;help:0;status:0;");
                }
            }
            else {
                alert(result);
            }

        }

        function NextFocus(event) {
            if (event.keyCode == 13) {
                event.preventDefault;
                event.keyCode = 9;
                // return false;
            }

        }
        function NextControlFocus(IDControl) {
            //        if (event.keyCode == 13) {
            //            document.getElementById(IDControl).focus();
            //        }
        }

        function jsRemoveDumpRow() {
            if (($get('SOTIEN_1').value.length == 0) || ($get('SOTIEN_1').value == "0") || (document.getElementById("chkRemove1").checked == false)) {
                $get('MA_CHUONG_1').value = '';
                $get('MA_KHOAN_1').value = '';
                $get('MA_MUC_1').value = '';
                $get('NOI_DUNG_1').value = '';
                $get('SODATHU_1').value = '';
                $get('KY_THUE_1').value = '';
                document.getElementById("rowGridDetail1").style.display = 'none';
            }
            if (($get('SOTIEN_2').value.length == 0) || ($get('SOTIEN_2').value == "0") || (document.getElementById("chkRemove2").checked == false)) {
                $get('MA_CHUONG_2').value = '';
                $get('MA_KHOAN_2').value = '';
                $get('MA_MUC_2').value = '';
                $get('NOI_DUNG_2').value = '';
                $get('SODATHU_2').value = '';
                $get('KY_THUE_2').value = '';
                document.getElementById("rowGridDetail2").style.display = 'none';
            }
            if (($get('SOTIEN_3').value.length == 0) || ($get('SOTIEN_3').value == "0") || (document.getElementById("chkRemove3").checked == false)) {
                $get('MA_CHUONG_3').value = '';
                $get('MA_KHOAN_3').value = '';
                $get('MA_MUC_3').value = '';
                $get('NOI_DUNG_3').value = '';
                $get('SODATHU_3').value = '';
                $get('KY_THUE_3').value = '';
                document.getElementById("rowGridDetail3").style.display = 'none';
            }
            if (($get('SOTIEN_4').value.length == 0) || ($get('SOTIEN_4').value == "0") || (document.getElementById("chkRemove4").checked == false)) {
                $get('MA_CHUONG_4').value = '';
                $get('MA_KHOAN_4').value = '';
                $get('MA_MUC_4').value = '';
                $get('NOI_DUNG_4').value = '';
                $get('SODATHU_4').value = '';
                $get('KY_THUE_4').value = '';
                document.getElementById("rowGridDetail4").style.display = 'none';
            }
        }
        function jsShowHideRowProgress(blnShow) {
            if (blnShow) {
                document.getElementById("rowProgress").style.display = '';
            }
            else {
                document.getElementById("rowProgress").style.display = 'none';
            }
        }
        function jsGetIndexByValue(ddlObject, cmpVal) {
            var obj = document.getElementById(ddlObject);
            for (var i = 0; i < obj.length; i++) {
                if (obj.options[i].value == cmpVal) {
                    return i;
                }
            }
            return 0;
        }

        function jsGetTenQuanHuyenNNT(ma_xa) {
            if (ma_xa != "") {
                PageMethods.GetTenQuanHuyen(ma_xa, GetTenQuanHuyenNNT_Complete, GetTenQuanHuyenNNT_Error);
                jsGetTenTinhNNT(ma_xa);
            } else {
                document.getElementById('txtHuyen_NNT').value = "";
                document.getElementById('txtTenHuyen_NNT').value = "";
                document.getElementById('txtTinh_NNT').value = "";
                document.getElementById('txtTenTinh_NNT').value = "";
            }
        }
        function GetTenQuanHuyenNNT_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Quan = result;
                document.getElementById('txtTenHuyen_NNT').value = ten_Quan;
            } else {
                alert('Mã Quận người nộp tiền không chính xác. Vui lòng nhập lại.');
                document.getElementById('txtHuyen_NNT').focus();
            }
        }
        function GetTenQuanHuyenNNT_Error(error, userContext, methodName) {
            alert('Mã Quận người nộp tiền không chính xác. Vui lòng nhập lại.');
            document.getElementById('txtHuyen_NNT').focus();
            document.getElementById('txtHuyen_NNT').value = "";
            document.getElementById('txtTenHuyen_NNT').value = "";
            document.getElementById('txtTinh_NNT').value = "";
            document.getElementById('txtTenTinh_NNT').value = "";
        }
        function jsGetTenTinhNNT(ma_xa) {
            PageMethods.GetTenTinh(ma_xa, GetTenTinhNNT_Complete, GetTenTinhNNT_Error);
        }
        function GetTenTinhNNT_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Tinh = result.split(';');
                document.getElementById('txtTinh_NNT').value = ten_Tinh[0];
                document.getElementById('txtTenTinh_NNT').value = ten_Tinh[1];
            }
        }
        function GetTenTinhNNT_Error(error, userContext, methodName) {
            alert('Mã Quận người nộp tiền không chính xác. Vui lòng nhập lại.');
            document.getElementById('txtHuyen_NNT').focus();
        }

        function jsGet_TenCQThu() {
            if (arrCQThu.length > 0) {
                //alert(arrCQThu.length.toString());
                $get('txtTenCQThu').value = '';
                for (var i = 0; i < arrCQThu.length; i++) {
                    var arr = arrCQThu[i].split(';');
                    if (arr[1] == $get('txtMaCQThu').value.trim()) {
                        $get('txtTenCQThu').value = arr[2];
                        //alert(flag_CQThu.toString());
                        break;
                    }
                }
            }
        }
        function jsGet_TenDBHC() {
            //console.log("XXXXXX");
            if (arrDBHC.length > 0) {
                $get('txtTenDBHC').value = '';
                for (var i = 0; i < arrDBHC.length; i++) {
                    var arr = arrDBHC[i].split(';');
                    if (arr[0] == $get('txtMaDBHC').value.trim()) {
                        $get('txtTenDBHC').value = arr[1];
                        break;
                    }
                }
            }
        }
        function jsGet_TenLHSX() {
            if (arrLHSX.length > 0) {
                $get('txtDescLHXNK').value = '';
                for (var i = 0; i < arrLHSX.length; i++) {
                    var arr = arrLHSX[i].split(';');
                    if (arr[0] == $get('txtLHXNK').value) {
                        $get('txtDescLHXNK').value = arr[1];
                        break;
                    }
                }
            }
        }
        function jsGet_TenKB() {
            if (arrSHKB.length > 0) {
                for (var i = 0; i < arrSHKB.length; i++) {
                    var arr = arrSHKB[i].split(';');
                    if (arr[0] == $get('txtSHKB').value.trim()) {
                        $get('txtTenKB').value = arr[1];
                        break;
                    }
                }
            }
        }
        function jsGet_TenTKGL() {
            if (arrTKGL.length > 0) {
                for (var i = 0; i < arrTKGL.length; i++) {
                    var arr = arrTKGL[i].split(';');
                    if (arr[0] == $get('txtTKGL').value) {
                        $get('txtTenTKGL').value = arr[1];
                        break;
                    }
                }
            }
        }
        function jsCheckMapTK() {
            var strMaDBHC = document.getElementById('txtMaDBHC').value;
            var strMaCQThu = document.getElementById('txtMaCQThu').value;

            if (strMaCQThu.length == 0) return;
            if (strMaDBHC.length == 0) return;
            if (arrTKCoNSNN == null) return;
            if (arrTKCoNSNN == null) return;
            for (i = 0; i < arrTKCoNSNN.length; i++) {
                var arr = arrTKCoNSNN[i].split(';');
                var strMaTKCo = document.getElementById('ddlMaTKCo').value;
                if (arr[2] == strMaTKCo) {
                    if (strMaDBHC + strMaCQThu == arr[0] + arr[1]) {
                        return true;
                    }
                }
            }
            return false;
        }

        function jsGetMaDB() {
            var oldSHKB = $get('txtSHKB').value;
            jsClearGridHeader(true, true);
            $get('txtSHKB').value = oldSHKB;
            jsGet_TenKB();
            jsSHKB_lostFocus(oldSHKB);
            jsClearGridDetail();
            $get('txtMaNNT').value = defMaVangLai;
            document.getElementById('txtNGAY_KH_NH').value = dtmNgayNH;//dtmNgayLV; 
            jsSetDefaultValues();
            document.getElementById('ddlMaTKCo').value = "7111";
            jsShowGridAfterHasMaNNT();
            // jsGet_TKNo();
            // jsGet_TKCo();  
            jsEnableButtons(1); //Hien thi nut ghi,ghi va chuyen ks               
            $get('txtTenNNT').disabled = false;
            $get('txtTenNNT').focus();
            //document.getElementById('divStatus').innerHTML= "Tìm thấy danh mục NNThuế trong danh mục vãng lai!!!";
            ShowHideControlByPT_TT();
        }
        function FindDanhMuc(strPage, txtID, txtTitle, txtFocus) {
            var strSHKB;
            var returnValue;
            if (document.getElementById('txtSHKB').value.length > 0) {
                strSHKB = document.getElementById('txtSHKB').value;
            }
            else {
                strSHKB = defSHKB.split(';')[0];
            }

            //returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&SHKB=" + strSHKB + "&initParam=" + $get(txtID).value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
            returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&SHKB=" + strSHKB + "&initParam=" + $get(txtID).value);
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                if (strPage == "KhoBac") {
                    jsSHKB_lostFocus(returnValue.ID);
                }
                if (strPage == "CQThu_Thue") {
                    jsMACQT_lostFocus(returnValue.ID);
                }
                if (txtTitle != null) {
                    document.getElementById(txtTitle).value = returnValue.Title;

                }
                if (txtFocus != null) {
                    document.getElementById(txtFocus).focus();
                }
            }

        }

        function FindNNT_NEW(txtID, txtTitle) {
            var strSHKB;
            if (document.getElementById('txtSHKB').value.length > 0) {
                strSHKB = document.getElementById('txtSHKB').value;
            }
            else {
                strSHKB = defSHKB.split(';')[0];
            }
            var returnValue = window.showModalDialog("../../Find_DM/Find_NNT_NEW.aspx?SHKB=" + strSHKB + "&initParam=" + $get('txtMaNNT').value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;
                //document.getElementById(txtFocus).focus();
            }
            NextFocus();
        }

        //Kienvt: them ham hien thi chu ky
        function jsShowCustomerSignature() {
            if ($get('txtTenTK_KH_NH').value != '') {
                window.showModalDialog("../../Find_DM/ShowSignature.aspx?account=" + $get('txtTK_KH_NH').value, "", "dialogWidth:700px;dialogHeight:500px;help:0;status:0;", "_new");
            }
            else {
                alert('Phải truy vấn được thông tin khách hàng trước khi xem chữ ký');
            }
        }

        function ShowLov(strType) {
            if (strType == "NNT") return FindNNT_NEW('txtMaNNT', 'txtTenNNT');
            //if (strType == "SHKB") return FindDanhMuc('KhoBac', 'txtSHKB', 'txtTenKB', 'txtSHKB');
            //if (strType == "DBHC") return FindDanhMuc('DBHC', 'txtMaDBHC', 'txtTenDBHC', 'txtMaCQThu');
            //if (strType == "CQT") return FindDanhMuc('CQThu_Thue', 'txtMaCQThu', 'txtTenCQThu', 'ddlMaTKNo');
            //if (strType == "LHXNK") return FindDanhMuc('LHXNK', 'txtLHXNK', 'txtDescLHXNK', 'txtSo_BK');
            //if (strType == "DMNH_TT") return FindDanhMuc('DMNH_TT', 'txtMA_NH_TT', 'txtTEN_NH_TT', 'txtTEN_NH_TT');
            //if (strType == "DMNH_GT") return FindDanhMuc('DMNH_GT', 'txtMA_NH_B', 'txtTenMA_NH_B', 'txtTenMA_NH_B');

            if (strType == "SHKB") return ShowDMKB('KhoBac', 'txtSHKB', 'txtTenKB', 'txtSHKB');
            if (strType == "DBHC") return ShowDMKB('DBHC', 'txtMaDBHC', 'txtTenDBHC', 'txtMaCQThu');
            if (strType == "CQT") return ShowDMKB('CQThu_Thue', 'txtMaCQThu', 'txtTenCQThu', 'ddlMaTKNo');
            if (strType == "LHXNK") return ShowDMKB('LHXNK', 'txtLHXNK', 'txtDescLHXNK', 'txtSo_BK');
            if (strType == "DMNH_TT") return ShowDMKB('DMNH_TT', 'txtMA_NH_TT', 'txtTEN_NH_TT', 'txtTEN_NH_TT');
            if (strType == "DMNH_GT") return ShowDMKB('DMNH_GT', 'txtMA_NH_B', 'txtTenMA_NH_B', 'txtTenMA_NH_B');

            //sonmt
            // if (strType == "LOAI_NNTHUE") return FindDanhMuc('LOAI_NNTHUE', 'txtMaLoaiNNT', 'txtTenLoaiNNT', 'txtMaLoaiNNT');
            if (strType == "LOAI_NNTHUE") return ShowDMKB('LOAI_NNTHUE', 'txtMaLoaiNNT', 'txtTenLoaiNNT', 'txtMaLoaiNNT');
        }


        function ShowMLNS(strType) {
            //if (strType == "MC_01") return FindDanhMuc('CapChuong', 'MA_CHUONG_1', null, 'MA_MUC_1');
            //if (strType == "MC_02") return FindDanhMuc('CapChuong', 'MA_CHUONG_2', null, 'MA_MUC_2');
            //if (strType == "MC_03") return FindDanhMuc('CapChuong', 'MA_CHUONG_3', null, 'MA_MUC_3');
            //if (strType == "MC_04") return FindDanhMuc('CapChuong', 'MA_CHUONG_4', null, 'MA_MUC_4');

            //if (strType == "MM_01") return FindDanhMuc('MucTMuc', 'MA_MUC_1', 'NOI_DUNG_1', 'NOI_DUNG_1');
            //if (strType == "MM_02") return FindDanhMuc('MucTMuc', 'MA_MUC_2', 'NOI_DUNG_2', 'NOI_DUNG_2');
            //if (strType == "MM_03") return FindDanhMuc('MucTMuc', 'MA_MUC_3', 'NOI_DUNG_3', 'NOI_DUNG_3');
            //if (strType == "MM_04") return FindDanhMuc('MucTMuc', 'MA_MUC_4', 'NOI_DUNG_4', 'NOI_DUNG_4');

            if (strType == "MC_01") return ShowDMKB('CapChuong', 'MA_CHUONG_1', null, 'MA_MUC_1');
            if (strType == "MC_02") return ShowDMKB('CapChuong', 'MA_CHUONG_2', null, 'MA_MUC_2');
            if (strType == "MC_03") return ShowDMKB('CapChuong', 'MA_CHUONG_3', null, 'MA_MUC_3');
            if (strType == "MC_04") return ShowDMKB('CapChuong', 'MA_CHUONG_4', null, 'MA_MUC_4');

            if (strType == "MM_01") return ShowDMKB('MucTMuc', 'MA_MUC_1', 'NOI_DUNG_1', 'NOI_DUNG_1');
            if (strType == "MM_02") return ShowDMKB('MucTMuc', 'MA_MUC_2', 'NOI_DUNG_2', 'NOI_DUNG_2');
            if (strType == "MM_03") return ShowDMKB('MucTMuc', 'MA_MUC_3', 'NOI_DUNG_3', 'NOI_DUNG_3');
            if (strType == "MM_04") return ShowDMKB('MucTMuc', 'MA_MUC_4', 'NOI_DUNG_4', 'NOI_DUNG_4');
        }


        function getSoTK_TruyVanCore() {
            //jsGetTK_KH_NH(); jsGetTenTK_KH_NH();
            var pttt = $("#ddlMaHTTT option:selected").val();
            //console.log("PT_TT(CORE): " + pttt);
            if (pttt == "05") {
                PageMethods.getSoTK_TruyVanCore(document.getElementById("ddlMaHTTT").value, getSoTK_TruyVanCore_Complete, getSoTK_TruyVanCore_Error);
            }
            else {
                $get('txtTK_KH_NH').disabled = false;
            }
        }

        function getSoTK_TruyVanCore_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            if (result.length > 0) {
                $get('txtTK_KH_NH').value = result;
                $get('txtTK_KH_NH').disabled = true;
                $get('txtTenTK_KH_NH').value = '';
                $get('txtSoDu_KH_NH').value = '';

            } else {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản tiền mặt ngân hàng" + error.get_message();
            }
        }
        function getSoTK_TruyVanCore_Error(error, userContext, methodName) {
            if (error !== null) {
                jsShowHideRowProgress(false);
                $get('txtTenTK_KH_NH').value = '';
                $get('txtSoDu_KH_NH').value = '';
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản tiền mặt ngân hàng" + error.get_message();
            }
        }
        //
        function ShowHideControlByPT_TT() {
            var intPP_TT = document.getElementById("ddlMaHTTT").value;
            var rTK_KH_NH = document.getElementById("rowTK_KH_NH");
            var rSoDuNH = document.getElementById("rowSoDuNH");
            var rTKGL = document.getElementById("rowTKGL");
            //07/06/2011-Kienvt:Them thong tin ngan hang nhan,chuyen
            var rNHChuyen = document.getElementById("rowNHChuyen");
            var rNHNhan = document.getElementById("rowNHNhan");
            var rNHTT = document.getElementById("rowNHTT");
            //jsCalTotal();
            //alert ('a');
            //        if (intPP_TT == 0 ){//"NH_TKTM"){ :tien mat
            //            rTK_KH_NH.style.display = 'none';
            //            rSoDuNH.style.display = 'none'; 
            //            //07/06/2011-Kienvt:Them thong tin ngan hang nhan,chuyen
            //            rNHChuyen.style.display = 'none'; 
            //            rNHNhan.style.display = 'none';             
            //            //$get('txtNGAY_KH_NH').focus();      
            //        }  
            if (intPP_TT == '01') {//"NH_TKTH_KB"){ :chuyen khoan
                rTK_KH_NH.style.display = '';
                rSoDuNH.style.display = '';
                rTKGL.style.display = 'none';
                //07/06/2011-Kienvt:Them thong tin ngan hang nhan,chuyen
                rNHChuyen.style.display = '';
                rNHNhan.style.display = '';
                rNHTT.style.display = '';
                //$get('txtTK_KH_NH').focus();             
            }
            if (intPP_TT == '05') {//"NH_TKTH_KB"){ :chuyen khoan
                rTK_KH_NH.style.display = '';
                rSoDuNH.style.display = '';
                rTKGL.style.display = 'none';
                //07/06/2011-Kienvt:Them thong tin ngan hang nhan,chuyen
                rNHChuyen.style.display = '';
                rNHNhan.style.display = '';
                rNHTT.style.display = '';
                //$get('txtTK_KH_NH').focus();             
            }
            //        if (intPP_TT == 2 ){//"NH_TKCT"){ :dien chuyen tien
            //            rTK_KH_NH.style.display = 'none';
            //            rSoDuNH.style.display = 'none';
            //            //07/06/2011-Kienvt:Them thong tin ngan hang nhan,chuyen
            //            rNHChuyen.style.display = 'none'; 
            //            rNHNhan.style.display = 'none';
            //            //$get('txtNGAY_KH_NH').focus();
            //        } 
            if (intPP_TT == '03') {//"NH_TKTG"){ :trung gian
                rTK_KH_NH.style.display = 'none';
                rSoDuNH.style.display = 'none';
                rTKGL.style.display = '';
                rNHChuyen.style.display = '';
                rNHNhan.style.display = '';
                rNHTT.style.display = '';
                var arr = arrTKGL[i].split(';');
                $get('txtTKGL').value = arr[0];

            }
            jsEnabledButtonByBDS();
        }
        function ShowLoaiThue() {
            var intLoaiThue = document.getElementById("ddlMaLoaiThue").selectedIndex;

            if (intLoaiThue == 0)//"01"
            {
                $get('txtTenLoaiThue').value = 'Thuế công thương nghiệp';

            }
            else if (intLoaiThue == 1)//"03"
            {

                $get('txtTenLoaiThue').value = 'Thuế Nhà đất,xe máy';

            }
            else //"05
            {
                $get('txtTenLoaiThue').value = 'Các loại thuế khác';
            }
        }
        function ShowHideControlByLoaiThue() {
            var intLoaiThue = document.getElementById("ddlMaLoaiThue").value;
            var rSoKhung = document.getElementById("rowSoKhung");
            var rSoMay = document.getElementById("rowSoMay");
            var rCQQD = document.getElementById("rowCQQD");
            var rSO_CQQD = document.getElementById("rowSO_CQQD");
            var rToKhaiSo = document.getElementById("rowToKhaiSo");
            var rNgayDK = document.getElementById("rowNgayDK");
            var rLHXNK = document.getElementById("rowLHXNK");

            if (intLoaiThue == '01') {
                rSoKhung.style.display = 'none';
                rSoMay.style.display = 'none';
                rToKhaiSo.style.display = 'none';
                rNgayDK.style.display = 'none';
                rLHXNK.style.display = 'none';
                rCQQD.style.display = 'none';
                rowSO_CQQD.style.display = 'none';
                rowNGAY_CQQD.style.display = 'none';
            }
            else if (intLoaiThue == '02') {
                rSoKhung.style.display = 'none';
                rSoMay.style.display = 'none';
                rToKhaiSo.style.display = 'none';
                rNgayDK.style.display = 'none';
                rLHXNK.style.display = 'none';
                rCQQD.style.display = 'none';
                rowSO_CQQD.style.display = 'none';
                rowNGAY_CQQD.style.display = 'none';
            }
            else if (intLoaiThue == '05') {
                rSoKhung.style.display = 'none';
                rSoMay.style.display = 'none';
                rToKhaiSo.style.display = 'none';
                rNgayDK.style.display = 'none';
                rLHXNK.style.display = 'none';
                rCQQD.style.display = 'none';
                rowSO_CQQD.style.display = 'none';
                rowNGAY_CQQD.style.display = 'none';
            }
            else if (intLoaiThue == '06') {
                rSoKhung.style.display = 'none';
                rSoMay.style.display = 'none';
                rToKhaiSo.style.display = 'none';
                rNgayDK.style.display = 'none';
                rLHXNK.style.display = 'none';
                rCQQD.style.display = 'none';
                rowSO_CQQD.style.display = 'none';
                rowNGAY_CQQD.style.display = 'none';
            }
            else if (intLoaiThue == '03')//"03"
            {
                rSoKhung.style.display = '';
                rSoMay.style.display = '';

                rToKhaiSo.style.display = 'none';
                rNgayDK.style.display = 'none';
                rLHXNK.style.display = 'none';
                rCQQD.style.display = 'none';
                rowSO_CQQD.style.display = 'none';
                rowNGAY_CQQD.style.display = 'none';
            }
            else {
                rSoKhung.style.display = 'none';
                rSoMay.style.display = 'none';
                rToKhaiSo.style.display = 'none';
                rNgayDK.style.display = 'none';
                rLHXNK.style.display = 'none';
                rCQQD.style.display = '';
                rowSO_CQQD.style.display = '';
                rowNGAY_CQQD.style.display = '';
            }
        }
        function jsClearGridHeader(clearDefVal, clearMa_NNT) {
            if (clearDefVal == true) $get('txtSHKB').value = '';
            if (clearDefVal == true) $get('txtTenKB').value = '';
            if (clearMa_NNT == true) $get('txtMaNNT').value = '';
            curSoCT = '';
            curSo_BT = '';
            $get('txtDC_NNTIEN').value = '';
            $get('txtTenNNT').value = '';
            $get('txtDChiNNT').value = '';
            $get('txtMaNNTien').value = '';
            $get('txtTenNNTien').value = '';
            $get('txtDChiNNTien').value = '';
            $get('hdnSoCT_NH').value = '';
            $get('hdnSO_CT').value = '';
            $get('hdnTongTienCu').value = '';
            $get('hdnSoLan_LapCT').value = '';

            //sonmt
            $get('txtHuyen_NNT').value = '';
            $get('txtTenHuyen_NNT').value = '';
            $get('txtTinh_NNT').value = '';
            $get('txtTenTinh_NNT').value = '';
            $get('txtHuyen_NNTHAY').value = '';
            $get('txtTinh_NNTHAY').value = '';
            $get('txtGhiChu').value = '';

            //sonmt
            $get('txtMaLoaiNNT').value = '';
            $get('txtTenLoaiNNT').value = '';

            if (clearDefVal == true) $get('txtMaDBHC').value = '';
            if (clearDefVal == true) $get('txtTenDBHC').value = '';
            if (clearDefVal == true) $get('txtMaCQThu').value = '';
            if (clearDefVal == true) $get('txtTenCQThu').value = '';

            //07/06/2011-Kienvt:Them thong tin ngan hang nhan,chuyen
            //if(clearDefVal==true) $get('txtMA_NH_A').value = '';
            //if(clearDefVal==true) $get('txtTenMA_NH_A').value = '';
            //if(clearDefVal==true) $get('txtMA_NH_B').value = '';
            //if(clearDefVal==true) $get('txtTenMA_NH_B').value = '';
            //end
            $get('txtCQQD').value = '';
            $get('txtTenHTTT').value = '';
            $get('txtTK_KH_NH').value = '';
            $get('txtTKGL').value = '';
            $get('txtTenTKGL').value = '';
            $get('txtTenTK_KH_NH').value = '';
            $get('txtSoDu_KH_NH').value = '';
            $get('txtDesc_SoDu_KH_NH').value = '';
            $get('txtNGAY_KH_NH').value = '';
            $get('txtDesNGAY_KH_NH').value = '';
            $get('txtTenLoaiThue').value = '';
            $get('txtToKhaiSo').value = '';
            $get('txtNgayDK').value = '';
            $get('txtLHXNK').value = '';
            $get('txtDescLHXNK').value = '';
            $get('txtSoKhung').value = '';
            $get('txtDescSoKhung').value = '';
            $get('txtSoMay').value = '';
            $get('txtDescSoMay').value = '';
            $get('txtSo_BK').value = '';
            $get('txtNgayBK').value = '';
            $get('txtQuan_HuyenNNTien').value = '';
            $get('txtTinh_NNTien').value = '';
            $get('txtKyHieuCT').value = '';

            $get('txtCQQD').value = '';
            $get('txtSOQD').value = '';
            $get('txtNgayQD').value = '';

            document.getElementById('ddlMaHTTT').selectedIndex = 0;
            document.getElementById('ddlMaLoaiThue').selectedIndex = 0;


            document.getElementById("rowSHKB").style.display = 'none';
            document.getElementById("rowKHCT").style.display = 'none';
            document.getElementById("rowMaNNT").style.display = '';

            document.getElementById("rowNNTien").style.display = 'none';
            document.getElementById("rowDChiNNT").style.display = 'none';
            document.getElementById("rowDChiNNTien").style.display = 'none';
            document.getElementById("rowDBHC").style.display = 'none';
            document.getElementById("rowCQThu").style.display = 'none';
            document.getElementById("rowTKNo").style.display = 'none';
            document.getElementById("rowTKCo").style.display = 'none';
            document.getElementById("rowHTTT").style.display = 'none';
            document.getElementById("rowTK_KH_NH").style.display = 'none';
            document.getElementById("rowSoDuNH").style.display = 'none';
            document.getElementById("rowNgay_KH_NH").style.display = 'none';
            //07/06/2011-Kienvt:Them thong tin ngan hang nhan,chuyen
            document.getElementById("rowNHChuyen").style.display = 'none';
            document.getElementById("rowNHNhan").style.display = 'none';
            document.getElementById("rowNHTT").style.display = 'none';
            //end
            document.getElementById("rowMaNT").style.display = 'none';
            document.getElementById("rowLoaiThue").style.display = 'none';
            document.getElementById("rowToKhaiSo").style.display = 'none';
            document.getElementById("rowNgayDK").style.display = 'none';
            document.getElementById("rowLHXNK").style.display = 'none';
            document.getElementById("rowSoKhung").style.display = 'none';
            document.getElementById("rowSoMay").style.display = 'none';
            document.getElementById("rowSoBK").style.display = 'none';
            document.getElementById("rowNgayBK").style.display = 'none';
            document.getElementById("rowHuyen").style.display = 'none';
            document.getElementById("rowTinh").style.display = 'none';
            document.getElementById("rowTKGL").style.display = 'none';
        }
        function jsCheckEmptyVal(cmpCtrl, divCtrl, strMessage) {
            if (document.getElementById(cmpCtrl).value == '') {
                document.getElementById(divCtrl).innerHTML = strMessage;
                return false;
            }
            return true;
        }
        function jsValidSoDuKH() {
            var dblTongTrichNo = parseFloat($get('txtTongTrichNo').value.replaceAll('.', ''));
            var dblTongTrichCu = parseFloat($get('hdnTongTienCu').value.replaceAll('.', ''));
            if ($get('hdnTongTienCu').value == null || $get('hdnTongTienCu').value == "" || $get('txtTK_KH_NH').value.substring(0, 3) == 'VND') {
                dblTongTrichCu = 0;
            } else dblTongTrichCu = parseFloat($get('hdnTongTienCu').value.replaceAll('.', ''));
            var dblSoDuKH = parseFloat($get('txtSoDu_KH_NH').value.replaceAll('.', ''));
            if (dblTongTrichNo - dblTongTrichCu <= dblSoDuKH) {
                return false;
            } else {
                return true;
            }
        }
        function jsValidForm() {
            if (IsNumeric(document.getElementById('txtTenNNT').value.trim())) {
                alert('Tên NNT không chấp nhận chỉ có số. Vui lòng kiểm tra lại.');
                document.getElementById('txtTenNNT').focus();
                return false;
            }
            //txtDChiNNT
            if (jsCheckEmptyVal('txtSHKB', document.getElementById('divStatus').id, "Số hiệu kho bạc không được để trống.Hãy nhập lại") == false) return false;
            if (jsCheckEmptyVal('txtMaNNT', document.getElementById('divStatus').id, "Mã số thuế không được để trống.Hãy nhập lại") == false) return false;
            if (jsCheckEmptyVal('txtTenNNT', document.getElementById('divStatus').id, "Tên người nộp thuế không được để trống.Hãy nhập lại") == false) return false;
            if (jsCheckEmptyVal('txtMaDBHC', document.getElementById('divStatus').id, "Mã ĐBHC không được để trống.Hãy nhập lại") == false) return false;
            if (jsCheckEmptyVal('txtMaCQThu', document.getElementById('divStatus').id, "Mã Cơ quan thu không được để trống.Hãy nhập lại") == false) return false;
            if (jsCheckEmptyVal('txtNGAY_KH_NH', document.getElementById('divStatus').id, "Ngày ngân hàng không được để trống.Hãy nhập lại") == false) return false;
            if (jsCheckEmptyVal('txtDChiNNT', document.getElementById('divStatus').id, "Địa chỉ người nộp thuế không được để trống.Hãy nhập lại") == false) return false;
            //Valid ky tu la
            if (isValidCitadCharacter($get('txtTenNNT').value) == false) {
                alert("Tên NNT không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            } if (isValidCitadCharacter($get('txtMaNNTien').value) == false) {
                alert("Tên người nôp thay không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            }
            if (isValidCitadCharacter($get('txtDChiNNTien').value) == false) {
                alert("Địa chỉ người nộp thay không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            }
            if (isValidCitadCharacter($get('txtDChiNNT').value) == false) {
                alert("Địa chỉ NNT không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            }
            if (isValidCitadCharacterNoiDung($get('NOI_DUNG_1').value) == false) {
                alert("Nội dung nhập không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            }
            if (isValidCitadCharacterNoiDung($get('NOI_DUNG_2').value) == false) {
                alert("Nội dung nhập không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            }
            if (isValidCitadCharacterNoiDung($get('NOI_DUNG_3').value) == false) {
                alert("Nội dung nhập không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            }
            if (isValidCitadCharacterNoiDung($get('NOI_DUNG_4').value) == false) {
                alert("Nội dung nhập không được bao gồm các ký tự lạ.Hãy kiểm tra lại");
                return false;
            }
            //alert(flag_CQThu.toString());
            if ($get('txtTenCQThu').value == '') {
                alert("Mã cơ quan thu không tồn tại. Vui lòng kiểm tra lại");
                return false;
            }
            //        if (checkCQT()==false) 
            //        {
            //            alert("Mã cơ quan thu và số hiệu kho bạc không cùng tỉnh");
            //            return false;
            //        }
            //        if (checkDBHC()==false) 
            //        {
            //            alert("Mã địa bàn hành chính không có trong danh sách");
            //            return false;
            //        }
            if (checkLHSX() == false) {
                alert("Mã loại hình sản xuất không có trong hệ thống");
                return false;
            }
            if (document.getElementById('txtTenNNT').value.length > 140) {
                alert("Tên NNT không được lớn hơn 140 ký tự");
                document.getElementById('txtTenNNT').focus();
                return false;
            }
            if (document.getElementById('txtMaNNTien').value.length > 15) {
                alert("Mã người nộp tiền không được quá 14 ký tự.");
                document.getElementById('txtMaNNTien').focus();
                return false;
            }
            if (jsCheckMapTK() == false) {
                // if (confirm ('Mã ĐBHC và Tài khoản Có không tương ứng với nhau.Bạn có muốn tiếp tục thực hiện???')==false) return false;

                //Kienvt : khong cho nhap neu dbhc,cquthu va tk co khong tuong ung voi nhau
                //           alert("Mã ĐBHC và Tài khoản Có không tương ứng với nhau.Hãy chọn lại!!!");
                //           return false;
            }
            //        if (document.getElementById("ddlMa_NTK").value=="1"){
            //            document.getElementById('ddlMaTKCo').value="7111";
            //        }else if ((document.getElementById("ddlMa_NTK").value=="2")){
            //            document.getElementById('ddlMaTKCo').value="3942";
            //        }
            if (jsCheckEmptyVal('ddlMaTKCo', document.getElementById('divStatus').id, "Tài khoản có không được để trống") == false) return false;
            //kienvt check them tai khoan khach hang khong duoc de trongddlMaTKCo
            if (document.getElementById("ddlMaHTTT").value == "01") {
                if (jsCheckEmptyVal('txtTK_KH_NH', document.getElementById('divStatus').id, "Tài khoản chuyển không được để trống") == false) return false;
                if (jsValidSoDuKH()) {
                    alert("TK của quý khách không đủ số dư để thực hiện giao dịch");
                    return false;
                }
            }
            if (document.getElementById("ddlMaHTTT").value == "05") {
                if (jsCheckEmptyVal('txtTK_KH_NH', document.getElementById('divStatus').id, "Tài khoản chuyển không được để trống") == false) return false;
                //if (jsValidSoDuKH()) {
                //    alert("TK của quý khách không đủ số dư để thực hiện giao dịch");
                //    return false;
                //}
            }
            if (document.getElementById("ddlMaHTTT").value == "03") {
                if (jsCheckEmptyVal('txtTKGL', document.getElementById('divStatus').id, "Tài khoản chuyển không được để trống") == false) return false;
                if (!checkTKGL()) {
                    document.getElementById('divStatus').innerHTML = "Tài khoản GL không đúng";
                    return false;
                }
            }

            if ($get('txtToKhaiSo').value == '' && document.getElementById('ddlMaLoaiThue').value == '04') {
                document.getElementById('divStatus').innerHTML = "Số tờ khai không được để trống";
                return false;
            }
            if ($get('txtNgayDK').value == '' && document.getElementById('ddlMaLoaiThue').value == '04') {
                document.getElementById('divStatus').innerHTML = "Ngày đăng ký không được để trống";
                return false;
            }
            if (document.getElementById("chkRemove1").checked) {
                if ($get('SOTIEN_1').value != '') {
                    if ($get('MA_CHUONG_1').value == '') {
                        document.getElementById('divStatus').innerHTML = "Mã chương (01) không được để trống.Hãy nhập lại";
                        return false;
                    }
                        //            else if ($get('MA_KHOAN_1').value=='')
                        //            {
                        //                document.getElementById('divStatus').innerHTML ="Mã khoản (01) không được để trống.Hãy nhập lại";
                        //                return false;
                        //            }  
                    else if ($get('MA_MUC_1').value == '') {
                        document.getElementById('divStatus').innerHTML = "Mã mục (01) không được để trống.Hãy nhập lại";
                        return false;
                    }
                    else if ($get('KY_THUE_1').value == '') {
                        document.getElementById('divStatus').innerHTML = "Kỳ thuế (01) không được để trống.Hãy nhập lại";
                        return false;
                    }

                    if (jsCheckFormatKyThue($get('KY_THUE_1').value) == false) {
                        document.getElementById('divStatus').innerHTML = "Định dạng Kỳ thuế (01) không đúng.Hãy nhập lại";
                        return false;
                    }

                }
            }
            if (document.getElementById("chkRemove2").checked) {
                if ($get('SOTIEN_2').value != '') {

                    if ($get('MA_CHUONG_2').value == '') {
                        document.getElementById('divStatus').innerHTML = "Mã chương (02) không được để trống.Hãy nhập lại";
                        return false;
                    }
                        //            else if ($get('MA_KHOAN_2').value=='')
                        //            {
                        //                document.getElementById('divStatus').innerHTML ="Mã khoản (02) không được để trống.Hãy nhập lại";
                        //                return false;
                        //            }  
                    else if ($get('MA_MUC_2').value == '') {
                        document.getElementById('divStatus').innerHTML = "Mã mục (02) không được để trống.Hãy nhập lại";
                        return false;
                    }
                    else if ($get('KY_THUE_2').value == '') {
                        document.getElementById('divStatus').innerHTML = "Kỳ thuế (02) không được để trống.Hãy nhập lại";
                        return false;
                    }

                    if (jsCheckFormatKyThue($get('KY_THUE_2').value) == false) {
                        document.getElementById('divStatus').innerHTML = "Định dạng Kỳ thuế (02) không đúng.Hãy nhập lại";
                        return false;
                    }

                }
            }
            if (document.getElementById("chkRemove3").checked) {
                if ($get('SOTIEN_3').value != '') {
                    if ($get('MA_CHUONG_3').value == '') {
                        document.getElementById('divStatus').innerHTML = "Mã chương (03) không được để trống.Hãy nhập lại";
                        return false;
                    }
                        //            else if ($get('MA_KHOAN_3').value=='')
                        //            {
                        //                document.getElementById('divStatus').innerHTML ="Mã khoản (03) không được để trống.Hãy nhập lại";
                        //                return false;
                        //            }  
                    else if ($get('MA_MUC_3').value == '') {
                        document.getElementById('divStatus').innerHTML = "Mã mục (03) không được để trống.Hãy nhập lại";
                        return false;
                    }
                    else if ($get('KY_THUE_3').value == '') {
                        document.getElementById('divStatus').innerHTML = "Kỳ thuế (03) không được để trống.Hãy nhập lại";
                        return false;
                    }

                    if (jsCheckFormatKyThue($get('KY_THUE_3').value) == false) {
                        document.getElementById('divStatus').innerHTML = "Định dạng Kỳ thuế (03) không đúng.Hãy nhập lại";
                        return false;
                    }
                }
            }
            if (document.getElementById("chkRemove4").checked) {
                if ($get('SOTIEN_4').value != '') {
                    if ($get('MA_CHUONG_4').value == '') {
                        document.getElementById('divStatus').innerHTML = "Mã chương (04) không được để trống.Hãy nhập lại";
                        return false;
                    }
                        //            else if ($get('MA_KHOAN_4').value=='')
                        //            {
                        //                document.getElementById('divStatus').innerHTML ="Mã khoản (04) không được để trống.Hãy nhập lại";
                        //                return false;
                        //            }  
                    else if ($get('MA_MUC_4').value == '') {
                        document.getElementById('divStatus').innerHTML = "Mã mục (04) không được để trống.Hãy nhập lại";
                        return false;
                    }
                    else if ($get('KY_THUE_4').value == '') {
                        document.getElementById('divStatus').innerHTML = "Kỳ thuế (04) không được để trống.Hãy nhập lại";
                        return false;
                    }

                    if (jsCheckFormatKyThue($get('KY_THUE_4').value) == false) {
                        document.getElementById('divStatus').innerHTML = "Định dạng Kỳ thuế (04) không đúng.Hãy nhập lại";
                        return false;
                    }
                }
            }
            //Kiem tra dinh dang ky thue       

            //Kiem tra thong tin tai khoan...

            return true;
        }
        function checkCQT() {
            for (var i = 0; i < arrCQThu_SHKB.length; i++) {
                var arr = arrCQThu_SHKB[i].split(';');
                if (arr[1] == $get('txtMaCQThu').value) {
                    return true;
                }
            }
            return false;
        }
        function checkTKGL() {
            for (var i = 0; i < arrTKGL.length; i++) {
                var arr = arrTKGL[i].split(';');
                if (arr[0] == $get('txtTKGL').value) {
                    return true;
                }
            }
            return false;
        }
        function checkDBHC() {
            for (var i = 0; i < arrDBHC.length; i++) {
                var arr = arrDBHC[i].split(';');
                if (arr[0] == $get('txtMaDBHC').value) {
                    return true;
                }
            }
            return false;
        }
        function checkLHSX() {
            if (document.getElementById('ddlMaLoaiThue').value == '04') {
                for (var i = 0; i < arrLHSX.length; i++) {
                    var arr = arrLHSX[i].split(';');
                    if (arr[0] == $get('txtLHXNK').value) {
                        return true;
                    }
                }
                return false;
            } else {
                return true;
            }
        }
        //1)Lay thong tin nnthue
        function jsGet_NNThue() {
            var strMaNNT = document.getElementById('txtMaNNT').value.trim();
            ma_nnthue_loi = strMaNNT;
            if (strMaNNT.length == 0) return;
            if (strMaNNT != curMaNNT) {//Chi cho phep querry lai db trong truong hop them moi chung tu
                if (strMaNNT == defMaVangLai) {
                    jsGetMaDB();
                }
                else {
                    jsShowHideRowProgress(true);
                    //$get('txtTenNNT').disabled = true;
                    PageMethods.Get_NNThue(strMaNNT, dtmNgayLV, Get_NNThue_Complete, Get_NNThue_Error);
                }
                //Kienvt them chuyen khoan
                ShowHideControlByPT_TT();
            }
        }
        function jsGet_Citad_Type() {

            PageMethods.GET_CITAD_TYPE(Get_Citad_Type_Complete, Get_Citad_Type_Error);
        }
        function Get_Citad_Type_Complete(result) {

            if (result.length > 0) {
                if (result == "0") {
                    document.getElementById('Radio3').checked = true;
                } else {
                    document.getElementById('Radio1').checked = true;
                }
            }
        }
        function Get_Citad_Type_Error(result) { }
        function jsOnChange_SoTK() {
            //alert ('a');
            var strMaNNT = document.getElementById('txtMaNNT').value;
            var strSoTK = document.getElementById('ddlToKhaiSo').value;
            if (strMaNNT.length == 0) return;

            if (strMaNNT != curMaNNT) {//Chi cho phep querry lai db trong truong hop them moi chung tu
                if (strMaNNT == defMaVangLai) {

                    jsGetMaDB();
                }
                else {
                    jsShowHideRowProgress(true);
                    //$get('txtTenNNT').disabled = true;
                    PageMethods.Get_NNThue_HQ(strMaNNT, strSoTK, Get_NNThue_Complete, Get_NNThue_Error);
                }
            }
        }
        function Get_NNThue_Complete(result, methodName) {
            checkSS(result);
            if (result.length > 0) {
                //check Session
                var arrKQ = result.split(';');
                if (arrKQ[0] == "-1") {
                    document.getElementById('divStatus').innerHTML = arrKQ[1];
                    jsEnableButtons(0);
                    //window.open("../../pages/frmLogin.aspx","_self");
                    return;
                }
                if (arrKQ[0] == "-99") {
                    document.getElementById('divStatus').innerHTML = arrKQ[1];
                    jsEnableButtons(0);
                    //window.open("../../pages/frmLogin.aspx","_self");
                    return;
                }
                //end check Session                         
                jsShowHideRowProgress(false);

                var xmlstring = result;
                var xmlDoc = jsGetXMLDoc(xmlstring);
                // alert(xmlstring);                                          
                jsClearGridHeader(false, false);
                jsClearGridDetail();
                document.getElementById('txtTenDBHC').value = '';
                XMLToBean(xmlDoc);
                jsEnableButtons(1); //Hien thi nut ghi,ghi va chuyen ks               
                $get('txtNGAY_KH_NH').value = dtmNgayNH;//dtmNgayLV; 
                // $get('txtDChiNNT').focus();
                jsSHKB_lostFocus(document.getElementById('txtSHKB').value);
                jsGetTTNganHangNhan();
                jsGetLoaiNNT();
                //jsGet_TKCo();
                //jsGet_TKNo();
                document.getElementById('divStatus').innerHTML = "Tìm thấy NNThuế trong danh mục!!!";
            }
            else {
                jsThem_Moi();
                document.getElementById('divStatus').innerHTML = "Mã NNT này không có trong CSDL.Hãy nhập lại!!!";
                document.getElementById('txtMaNNT').value = ma_nnthue_loi;
                document.getElementById($get('txtMaNNT')).focus();
            }
            _SHKB = $("#txtSHKB").val();
            jsCalCharacter();
        }
        function Get_NNThue_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin người nộp thuế";
                document.getElementById($get('txtMaNNT')).focus();
            }
            jsShowHideRowProgress(false);
        }

        function jsGetLoaiNNT() {
            var pLoai = $('#txtMaLoaiNNT').val();
            if (pLoai == null)
                return;
            if (pLoai.trim().length <= 0)
                return;

            PageMethods.jsGetLoaiNNT(pLoai, jsGetLoaiNNT_Complete, jsGetLoaiNNT_Error);
        }
        function jsGetLoaiNNT_Complete(pData) {

            if (!jsCheckUserSession(pData))
                return;
            if (pData.length <= 0) {

                return;
            }
            $('#txtTenLoaiNNT').val(pData);

        }
        function jsGetLoaiNNT_Error() {

        }

        function jsCheckUserSession(param) {
            var bRet = true;
            if (param) {
                var arrKQ = param.split(';');
                if (arrKQ[0] == "errUserSession") {
                    window.open("../../pages/Warning.html", "_self");
                    bRet = false;
                }
            }
            return bRet;
        }

        function loadXMLString(txt) {
            if (window.DOMParser) {
                parser = new DOMParser();
                xmlDoc = parser.parseFromString(txt, "text/xml");
            }
            else // code for IE
            {
                xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = false;
                xmlDoc.loadXML(txt);
            }
            return xmlDoc;
        }

        //2)Lay thong tin tai khoan ngan hang
        function jsGetTK_KH_NH() {
            if ($get('txtTK_KH_NH').value.length > 0)//kienvt : tai khoan fix la 14 + 4'-'
            {
                document.getElementById('divStatus').innerHTML = '';
                var strAccType = '';
                var intPP_TT = document.getElementById("ddlMaHTTT").selectedIndex;
                var ma_NT = document.getElementById("txtTenNT").value;
                if (intPP_TT == 3) {
                    strAccType = 'TG';
                } else {
                    strAccType = 'CK';
                }
                jsShowHideRowProgress(true);
                //console.log("GetTK_KH_NH.PT_TT: " + $("#ddlMaHTTT option:selected").val());
                var pttt = $("#ddlMaHTTT option:selected").val();
                if (pttt == "01") {
                    PageMethods.GetTK_KH_NH($get('txtTK_KH_NH').value.trim(), GetTK_NH_Complete, GetTK_NH_Error);
                } else if (pttt == "05") {
                    $get('txtTK_KH_NH').disabled = true;
                }
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Tài khoản khách hàng không được để trống';
            }
        }

        function GetTK_NH_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            if (result.length > 0) {
                var xmlDoc = loadXMLString(result);
                var rootCTU_HDR = xmlDoc.getElementsByTagName('PARAMETER')[0];
                if (rootCTU_HDR != null) {

                    if (xmlDoc.getElementsByTagName("RETCODE")[0].childNodes[0].nodeValue == "00000") {
                        if (xmlDoc.getElementsByTagName("BANKACCOUNT")[0].childNodes != null) $get('txtTK_KH_NH').value = xmlDoc.getElementsByTagName("BANKACCOUNT")[0].childNodes[0].nodeValue;
                        if (xmlDoc.getElementsByTagName("ACY_AVL_BAL")[0].childNodes != null) $get('txtSoDu_KH_NH').value = xmlDoc.getElementsByTagName("ACY_AVL_BAL")[0].childNodes[0].nodeValue;
                        jsFormatNumber('txtSoDu_KH_NH');

                    } else {
                        $get('txtTK_KH_NH').value = '';
                        $get('txtSoDu_KH_NH').value = '';
                        document.getElementById('divStatus').innerHTML = "Không lấy được thông tin tài khoản. Mã lỗi " + xmlDoc.getElementsByTagName("RETCODE")[0].childNodes[0].nodeValue;
                    }
                } else {
                    document.getElementById('divStatus').innerHTML = 'Có lỗi trong quá trình truy vấn số dư tài khoản.';
                    $get('txtTenTK_KH_NH').value = '';
                    $get('txtSoDu_KH_NH').value = '';

                }
            }
        }
        function GetTK_NH_Error(error, userContext, methodName) {
            if (error !== null) {
                jsShowHideRowProgress(false);
                $get('txtTenTK_KH_NH').value = '';
                $get('txtSoDu_KH_NH').value = '';
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản ngân hàng" + error.get_message();
            }
        }
        //2')Lay tên khách hàng
        function jsGetTenTK_KH_NH() {
            if ($get('txtTK_KH_NH').value.length > 0) {
                document.getElementById('divStatus').innerHTML = '';
                var strAccType = '';
                var intPP_TT = document.getElementById("ddlMaHTTT").selectedIndex;
                var ma_NT = document.getElementById("txtTenNT").value;
                if (intPP_TT == 3) {
                    strAccType = 'TG';
                } else {
                    strAccType = 'CK';
                }
                jsShowHideRowProgress(true);
                PageMethods.GetTEN_KH_NH($get('txtTK_KH_NH').value.trim(), GetTEN_KH_NH_Complete, GetTEN_KH_NH_Error);
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Tài khoản khách hàng không được để trống';
            }
        }

        function GetTEN_KH_NH_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            if (result.length > 0) {
                $get('txtTenTK_KH_NH').value = result;
            } else {
                document.getElementById('divStatus').innerHTML = 'Không lấy được tên tài khoản khách hàng';
                $get('txtTenTK_KH_NH').value = '';
            }
        }
        function GetTEN_KH_NH_Error(error, userContext, methodName) {
            if (error !== null) {
                $get('txtTenTK_KH_NH').value = '';
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản ngân hàng" + error.get_message();
            }
        }
        //3)Lay danh sach chung tu
        function jsLoadCTList() {
            PageMethods.LoadCTList(curMa_NV, LoadCTList_Complete, LoadCTList_Error);
        }
        function LoadCTList_Complete(result, methodName) {
            if (result.length > 0) {
                checkSS(result);
                document.getElementById('divDSCT').innerHTML = result;
            }
            try {
                $('#grdDSCT').paging({ limit: 15 });
            } catch (err) {
                paginationList(15);
            }
        }
        function LoadCTList_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy danh sách chứng từ" + error.get_message();
            }
        }

        function jsGetXMLDoc(xmlString) {
            var xmlDoc;
            //if (xmlString==null||xmlString.length==0) return;
            try //Internet Explorer
            {
                //alert (xmlString);
                xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = "false";
                xmlDoc.loadXML(xmlString);
                return xmlDoc;
            }
            catch (e) {
                parser = new DOMParser();
                xmlDoc = parser.parseFromString(xmlString, "text/xml");
                return xmlDoc;
            }
        }

        //4)Lay mot chung tu cu the
        function jsGetCTU(strSHKB, strSoCT, strSoBT) {
            jsCalCharacter();
            jsLoadDM('DBHC', strSHKB);
            jsLoadDM('CQTHU', strSHKB);
            jsLoadDM('CQTHU_SHKB', strSHKB);

            //07/06 Kienvt: Load dm
            PageMethods.GetCTU(curMa_NV, strSHKB, strSoCT, strSoBT, GetCTU_Complete, GetCTU_Error);
        }

        function GetCTU_Complete(result, methodName) {
            checkSS(result);
            var xmlstring = result;
            //alert(result);
            var xmlDoc = jsGetXMLDoc(xmlstring);
            jsClearGridHeader(true, true);

            jsClearGridDetail();
            XMLToBean(xmlDoc);
            var blnEnableEdit = false;

            if (curCtuStatus == "00") {//Chua ks
                blnEnableEdit = false;
                document.getElementById('hddAction').value = 2;
                jsEnableButtons(2);
            }
            else if (curCtuStatus == "01") {//Da ks
                blnEnableEdit = true;
                jsEnableButtons(6);
            }
            else if (curCtuStatus == "06") {//Da ks
                blnEnableEdit = true;
                jsEnableButtons(6);
            }
            else if (curCtuStatus == "03") {//Ks Loi - chuyen tra
                blnEnableEdit = false;
                document.getElementById('hddAction').value = 2;
                jsEnableButtons(7);
            }
            else if (curCtuStatus == "04") {//Huy
                blnEnableEdit = true;
                jsEnableButtons(4);
            }
            else if (curCtuStatus == "05") {//Chuyen KS
                blnEnableEdit = true;
                jsEnableButtons(5);
            }
            else if (curCtuStatus == "02") {//Chuyen KS
                blnEnableEdit = true;
                jsEnableButtons(8);
            }

            //Lay trang thai cua chung tu cho biet
            jsEnableGridHeader(blnEnableEdit);
            jsEnableGridDetail(blnEnableEdit);
            jsLoadCTList();//load lai danh sach chung tu
            jsGetTTNganHangNhan();
            document.getElementById('txtMaNNT').disabled = true;
            document.getElementById('txtSHKB').disabled = true;
        }
        function GetCTU_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin chứng từ" + error.get_message();
            }
        }

        function jsShowGridAfterHasMaNNT() {
            document.getElementById("rowSHKB").style.display = '';
            document.getElementById("rowKHCT").style.display = '';
            document.getElementById("rowNNTien").style.display = '';
            document.getElementById("rowDChiNNT").style.display = '';
            document.getElementById("rowDChiNNTien").style.display = 'none';
            document.getElementById("rowDBHC").style.display = '';
            document.getElementById("rowCQThu").style.display = '';
            document.getElementById("rowTKNo").style.display = 'none';
            document.getElementById("rowTKCo").style.display = '';
            document.getElementById("rowHTTT").style.display = '';
            document.getElementById("rowNgay_KH_NH").style.display = '';
            document.getElementById("rowMaNT").style.display = '';
            document.getElementById("rowLoaiThue").style.display = '';
            document.getElementById("rowSoBK").style.display = 'none';
            document.getElementById("rowNgayBK").style.display = 'none';
            document.getElementById("rowHuyen").style.display = 'none';
            document.getElementById("rowTinh").style.display = '';
        }

        function XMLToBean(xmlDoc) {

            var strLoaiThue = "";
            var strHTTT = "";

            jsShowGridAfterHasMaNNT();
            var rootCTU_HDR = xmlDoc.getElementsByTagName('CTU_HDR')[0];

            try {
                if (rootCTU_HDR != null) {

                    if (rootCTU_HDR.getElementsByTagName("NgayLV")[0].firstChild != null && rootCTU_HDR.getElementsByTagName("SoCT")[0].firstChild != null) {
                        curSoCT = rootCTU_HDR.getElementsByTagName("SoCT")[0].firstChild.nodeValue.trim();
                        $get('hdnSO_CT').value = curSoCT;
                        curSo_BT = rootCTU_HDR.getElementsByTagName("So_BT")[0].firstChild.nodeValue;
                        document.getElementById('divDescCTU').innerHTML = 'SHKB/SoCT/SoBT:' + rootCTU_HDR.getElementsByTagName("SHKB")[0].firstChild.nodeValue + "/" + curSoCT + "/" + curSo_BT;
                    }
                    if (rootCTU_HDR.getElementsByTagName("SHKB")[0].firstChild != null) $get('txtSHKB').value = rootCTU_HDR.getElementsByTagName("SHKB")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("TenKB")[0].firstChild != null) $get('txtTenKB').value = rootCTU_HDR.getElementsByTagName("TenKB")[0].firstChild.nodeValue;

                    if (rootCTU_HDR.getElementsByTagName("MaNNT")[0].firstChild != null) {
                        $get('txtMaNNT').value = rootCTU_HDR.getElementsByTagName("MaNNT")[0].firstChild.nodeValue;
                        //Luu mannt hien tai

                    }
                    curMaNNT = $get('txtMaNNT').value;

                    if (rootCTU_HDR.getElementsByTagName("TenNNT")[0].firstChild != null) $get('txtTenNNT').value = rootCTU_HDR.getElementsByTagName("TenNNT")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("DChiNNT")[0].firstChild != null) $get('txtDChiNNT').value = rootCTU_HDR.getElementsByTagName("DChiNNT")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("MaNNTien")[0].firstChild != null) $get('txtMaNNTien').value = rootCTU_HDR.getElementsByTagName("MaNNTien")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("TenNNTien")[0].firstChild != null) $get('txtTenNNTien').value = rootCTU_HDR.getElementsByTagName("TenNNTien")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("DChiNNTien")[0].firstChild != null) $get('txtDC_NNTIEN').value = rootCTU_HDR.getElementsByTagName("DChiNNTien")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("Huyen_NNTIEN")[0].firstChild != null) $get('txtQuan_HuyenNNTien').value = rootCTU_HDR.getElementsByTagName("Huyen_NNTIEN")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("Tinh_NNTIEN")[0].firstChild != null) $get('txtTinh_NNTien').value = rootCTU_HDR.getElementsByTagName("Tinh_NNTIEN")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("KHCT")[0].firstChild != null) $get('txtKyHieuCT').value = rootCTU_HDR.getElementsByTagName("KHCT")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("SoCTNH")[0].firstChild != null) $get('hdnSoCT_NH').value = rootCTU_HDR.getElementsByTagName("SoCTNH")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("MaCQThu")[0].firstChild != null) $get('txtMaCQThu').value = rootCTU_HDR.getElementsByTagName("MaCQThu")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("TenCQThu")[0].firstChild != null) $get('txtTenCQThu').value = rootCTU_HDR.getElementsByTagName("TenCQThu")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("MaDBHC")[0].firstChild != null) $get('txtMaDBHC').value = rootCTU_HDR.getElementsByTagName("MaDBHC")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("TenDBHC")[0].firstChild != null) { $get('txtTenDBHC').value = rootCTU_HDR.getElementsByTagName("TenDBHC")[0].firstChild.nodeValue; }
                    if (rootCTU_HDR.getElementsByTagName("MA_NTK")[0].firstChild != null) $get('ddlMa_NTK').value = rootCTU_HDR.getElementsByTagName("MA_NTK")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("TKNo")[0].firstChild != null) {
                        document.getElementById('ddlMaTKNo').value = rootCTU_HDR.getElementsByTagName("TKNo")[0].firstChild.nodeValue;
                    }
                    if (rootCTU_HDR.getElementsByTagName("TenTKNo")[0].firstChild != null) { $get('txtTKNo').value = rootCTU_HDR.getElementsByTagName("TenTKNo")[0].firstChild.nodeValue; }
                    if (rootCTU_HDR.getElementsByTagName("TKCo")[0].firstChild != null) {

                        document.getElementById('ddlMaTKCo').value = rootCTU_HDR.getElementsByTagName("TKCo")[0].firstChild.nodeValue;
                    }
                    if (rootCTU_HDR.getElementsByTagName("TenTKCo")[0].firstChild != null) { $get('txtTKCo').value = rootCTU_HDR.getElementsByTagName("TenTKCo")[0].firstChild.nodeValue; }
                    if (rootCTU_HDR.getElementsByTagName("HTTT")[0].firstChild != null) {
                        strHTTT = rootCTU_HDR.getElementsByTagName("HTTT")[0].firstChild.nodeValue;
                    }

                    //sonmt: them loai hinh nguoi nop thue
                    if (rootCTU_HDR.getElementsByTagName("LOAI_NNT")[0].firstChild != null) $get('txtMaLoaiNNT').value = rootCTU_HDR.getElementsByTagName("LOAI_NNT")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("TEN_LOAI_NNT")[0].firstChild != null) $get('txtTenLoaiNNT').value = rootCTU_HDR.getElementsByTagName("TEN_LOAI_NNT")[0].firstChild.nodeValue;


                    //console.log(strHTTT);
                    $('#ddlMaHTTT').val(strHTTT);

                    if (strHTTT == "01") {
                        if (rootCTU_HDR.getElementsByTagName("TK_KH_NH")[0].firstChild != null) $get('txtTK_KH_NH').value = rootCTU_HDR.getElementsByTagName("TK_KH_NH")[0].firstChild.nodeValue;
                        if (rootCTU_HDR.getElementsByTagName("TenTK_KH_NH")[0].firstChild != null) $get('txtTenTK_KH_NH').value = rootCTU_HDR.getElementsByTagName("TenTK_KH_NH")[0].firstChild.nodeValue;
                        if (rootCTU_HDR.getElementsByTagName("SoDu_KH_NH")[0].firstChild != null) $get('txtSoDu_KH_NH').value = rootCTU_HDR.getElementsByTagName("SoDu_KH_NH")[0].firstChild.nodeValue;
                    }

                    if (strHTTT == "03") {
                        if (rootCTU_HDR.getElementsByTagName("TK_KH_NH")[0].firstChild != null) $get('txtTKGL').value = rootCTU_HDR.getElementsByTagName("TK_KH_NH")[0].firstChild.nodeValue;
                        if (rootCTU_HDR.getElementsByTagName("TenTK_KH_NH")[0].firstChild != null) $get('txtTenTKGL').value = rootCTU_HDR.getElementsByTagName("TenTK_KH_NH")[0].firstChild.nodeValue;
                    }
                    if (strHTTT == "05") {

                        if (rootCTU_HDR.getElementsByTagName("TK_KH_NH")[0].firstChild != null) $get('txtTK_KH_NH').value = rootCTU_HDR.getElementsByTagName("TK_KH_NH")[0].firstChild.nodeValue;
                        $get('txtTK_KH_NH').disabled = true;
                        //if (rootCTU_HDR.getElementsByTagName("TenTK_KH_NH")[0].firstChild != null) $get('txtTenTK_KH_NH').value = rootCTU_HDR.getElementsByTagName("TenTK_KH_NH")[0].firstChild.nodeValue;
                        //if (rootCTU_HDR.getElementsByTagName("SoDu_KH_NH")[0].firstChild != null) $get('txtSoDu_KH_NH').value = rootCTU_HDR.getElementsByTagName("SoDu_KH_NH")[0].firstChild.nodeValue;
                    }
                    //if (rootCTU_HDR.getElementsByTagName("TenTK_KH_NH")[0].firstChild.nodeValue!=null) $get('txtTenTK_KH_NH').value = rootCTU_HDR.getElementsByTagName("TenTK_KH_NH")[0].firstChild.nodeValue;        

                    //KIENVT : THEM THONG TIN NGAN HANG NHAN/CHUYEN
                    if (rootCTU_HDR.getElementsByTagName("MA_NH_A")[0].firstChild != null) {
                        if (rootCTU_HDR.getElementsByTagName("MA_NH_A")[0].firstChild.nodeValue != null) {
                            //$get('txtMA_NH_A').value = rootCTU_HDR.getElementsByTagName("MA_NH_A")[0].firstChild.nodeValue;
                            var intPos = jsGetIndexByValue('ddlMA_NH_A', rootCTU_HDR.getElementsByTagName("MA_NH_A")[0].firstChild.nodeValue)
                            document.getElementById('ddlMA_NH_A').selectedIndex = intPos;
                        }
                    }
                    if (rootCTU_HDR.getElementsByTagName("MA_NH_B")[0].firstChild != null) {
                        if (rootCTU_HDR.getElementsByTagName("MA_NH_B")[0].firstChild.nodeValue != null) {
                            $get('txtMA_NH_B').value = rootCTU_HDR.getElementsByTagName("MA_NH_B")[0].firstChild.nodeValue;
                        }
                    }

                    if (rootCTU_HDR.getElementsByTagName("Ten_NH_B")[0].firstChild != null) {
                        if (rootCTU_HDR.getElementsByTagName("Ten_NH_B")[0].firstChild.nodeValue != null) {
                            $get('txtTenMA_NH_B').value = rootCTU_HDR.getElementsByTagName("Ten_NH_B")[0].firstChild.nodeValue;
                        }
                    }
                    if (rootCTU_HDR.getElementsByTagName("MA_NH_TT")[0].firstChild != null) {
                        if (rootCTU_HDR.getElementsByTagName("MA_NH_TT")[0].firstChild.nodeValue != null) {
                            $get('txtMA_NH_TT').value = rootCTU_HDR.getElementsByTagName("MA_NH_TT")[0].firstChild.nodeValue;
                        }
                    }

                    if (rootCTU_HDR.getElementsByTagName("TEN_NH_TT")[0].firstChild != null) {
                        if (rootCTU_HDR.getElementsByTagName("TEN_NH_TT")[0].firstChild.nodeValue != null) {
                            $get('txtTEN_NH_TT').value = rootCTU_HDR.getElementsByTagName("TEN_NH_TT")[0].firstChild.nodeValue;
                        }
                    }
                    if (rootCTU_HDR.getElementsByTagName("PHI_GD")[0].firstChild != null) {
                        if (rootCTU_HDR.getElementsByTagName("PHI_GD")[0].firstChild.nodeValue != null) {
                            $get('txtCharge').value = rootCTU_HDR.getElementsByTagName("PHI_GD")[0].firstChild.nodeValue;
                        }
                    }
                    if (rootCTU_HDR.getElementsByTagName("PHI_VAT")[0].firstChild != null) {
                        if (rootCTU_HDR.getElementsByTagName("PHI_VAT")[0].firstChild.nodeValue != null) {
                            $get('txtVAT').value = rootCTU_HDR.getElementsByTagName("PHI_VAT")[0].firstChild.nodeValue;
                        }
                    }
                    if (rootCTU_HDR.getElementsByTagName("SO_QD")[0].firstChild != null) {
                        if (rootCTU_HDR.getElementsByTagName("SO_QD")[0].firstChild.nodeValue != null) {
                            $get('txtSOQD').value = rootCTU_HDR.getElementsByTagName("SO_QD")[0].firstChild.nodeValue;
                        }
                    }
                    if (rootCTU_HDR.getElementsByTagName("NGAY_QD")[0].firstChild != null) {
                        if (rootCTU_HDR.getElementsByTagName("NGAY_QD")[0].firstChild.nodeValue != null) {
                            $get('txtNgayQD').value = rootCTU_HDR.getElementsByTagName("NGAY_QD")[0].firstChild.nodeValue;
                        }
                    }
                    if (rootCTU_HDR.getElementsByTagName("CQ_QD")[0].firstChild != null) {
                        if (rootCTU_HDR.getElementsByTagName("CQ_QD")[0].firstChild.nodeValue != null) {
                            $get('txtCQQD').value = rootCTU_HDR.getElementsByTagName("CQ_QD")[0].firstChild.nodeValue;
                        }
                    }
                    if (rootCTU_HDR.getElementsByTagName("PT_TINHPHI")[0].firstChild != null) {
                        if (rootCTU_HDR.getElementsByTagName("PT_TINHPHI")[0].firstChild.nodeValue != null) {
                            if (rootCTU_HDR.getElementsByTagName("PT_TINHPHI")[0].firstChild.nodeValue == "W") {
                                document.getElementById('chkChargeTypeMP').checked = true;//tu dong
                            } else if (rootCTU_HDR.getElementsByTagName("PT_TINHPHI")[0].firstChild.nodeValue == "O") {
                                document.getElementById('chkChargeTypePN').checked = true;//bang tay
                            } else {
                                document.getElementById('chkChargeTypePT').checked = true;
                            }
                        }
                    }
                    if (rootCTU_HDR.getElementsByTagName("TT_CITAD")[0].firstChild != null) {
                        if (rootCTU_HDR.getElementsByTagName("TT_CITAD")[0].firstChild.nodeValue != null) {
                            if (rootCTU_HDR.getElementsByTagName("TT_CITAD")[0].firstChild.nodeValue == "1") {
                                document.getElementById('Radio1').checked = true;//tu dong
                            } else {
                                document.getElementById('Radio3').checked = true;
                            }
                        }
                    }
                    //if (rootCTU_HDR.getElementsByTagName("Desc_SoDu_KH_NH")[0].firstChild.nodeValue!=null) $get('txtDesc_SoDu_KH_NH').value = rootCTU_HDR.getElementsByTagName("Desc_SoDu_KH_NH")[0].firstChild.nodeValue;        
                    if (rootCTU_HDR.getElementsByTagName("NGAY_KH_NH")[0].firstChild != null) $get('txtNGAY_KH_NH').value = rootCTU_HDR.getElementsByTagName("NGAY_KH_NH")[0].firstChild.nodeValue;
                    //if (rootCTU_HDR.getElementsByTagName("DesNGAY_KH_NH")[0].firstChild.nodeValue!=null) $get('txtDesNGAY_KH_NH').value = rootCTU_HDR.getElementsByTagName("DesNGAY_KH_NH")[0].firstChild.nodeValue;                       
                    if (rootCTU_HDR.getElementsByTagName("LoaiThue")[0].firstChild != null) {
                        strLoaiThue = rootCTU_HDR.getElementsByTagName("LoaiThue")[0].firstChild.nodeValue;
                        document.getElementById("ddlMaLoaiThue").selectedValue = rootCTU_HDR.getElementsByTagName("LoaiThue")[0].firstChild.nodeValue;

                    }

                    //if (rootCTU_HDR.getElementsByTagName("DescLoaiThue")[0].firstChild!=null) $get('txtTenLoaiThue').value = rootCTU_HDR.getElementsByTagName("DescLoaiThue")[0].firstChild.nodeValue;                 
                    if (rootCTU_HDR.getElementsByTagName("MaNT")[0].firstChild != null) $get('cboNguyenTe').value = rootCTU_HDR.getElementsByTagName("MaNT")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("MaNT")[0].firstChild != null) $get('txtTenNT').value = rootCTU_HDR.getElementsByTagName("MaNT")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("ToKhaiSo")[0].firstChild != null) $get('txtToKhaiSo').value = rootCTU_HDR.getElementsByTagName("ToKhaiSo")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("NgayDK")[0].firstChild != null) $get('txtNgayDK').value = rootCTU_HDR.getElementsByTagName("NgayDK")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("LHXNK")[0].firstChild != null) $get('txtLHXNK').value = rootCTU_HDR.getElementsByTagName("LHXNK")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("DescLHXNK")[0].firstChild != null) $get('txtDescLHXNK').value = rootCTU_HDR.getElementsByTagName("DescLHXNK")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("SoKhung")[0].firstChild != null) $get('txtSoKhung').value = rootCTU_HDR.getElementsByTagName("SoKhung")[0].firstChild.nodeValue;
                    //if (rootCTU_HDR.getElementsByTagName("DescSoKhung")[0].firstChild.nodeValue!=null) $get('txtDescSoKhung').value = rootCTU_HDR.getElementsByTagName("DescSoKhung")[0].firstChild.nodeValue;                 
                    if (rootCTU_HDR.getElementsByTagName("SoMay")[0].firstChild != null) $get('txtSoMay').value = rootCTU_HDR.getElementsByTagName("SoMay")[0].firstChild.nodeValue;
                    //if (rootCTU_HDR.getElementsByTagName("DescSoMay")[0].firstChild.nodeValue!=null) $get('txtDescSoMay').value = rootCTU_HDR.getElementsByTagName("DescSoMay")[0].firstChild.nodeValue;                 
                    if (rootCTU_HDR.getElementsByTagName("So_BK")[0].firstChild != null) $get('txtSo_BK').value = rootCTU_HDR.getElementsByTagName("So_BK")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("Ngay_BK")[0].firstChild != null) $get('txtNgayBK').value = rootCTU_HDR.getElementsByTagName("Ngay_BK")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("TT_TTHU")[0].firstChild != null) $get('hdnSoLan_LapCT').value = rootCTU_HDR.getElementsByTagName("TT_TTHU")[0].firstChild.nodeValue;

                    //sonmt

                    if (rootCTU_HDR.getElementsByTagName("GHI_CHU")[0].firstChild != null) $get('txtGhiChu').value = rootCTU_HDR.getElementsByTagName("GHI_CHU")[0].firstChild.nodeValue;

                    if (rootCTU_HDR.getElementsByTagName("MA_HUYEN")[0].firstChild != null) $get('txtHuyen_NNT').value = rootCTU_HDR.getElementsByTagName("MA_HUYEN")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("TEN_HUYEN")[0].firstChild != null) $get('txtTenHuyen_NNT').value = rootCTU_HDR.getElementsByTagName("TEN_HUYEN")[0].firstChild.nodeValue;

                    if (rootCTU_HDR.getElementsByTagName("MA_TINH")[0].firstChild != null) $get('txtTinh_NNT').value = rootCTU_HDR.getElementsByTagName("MA_TINH")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("TEN_TINH")[0].firstChild != null) $get('txtTenTinh_NNT').value = rootCTU_HDR.getElementsByTagName("TEN_TINH")[0].firstChild.nodeValue;

                    if (rootCTU_HDR.getElementsByTagName("HUYEN_NNTHAY")[0].firstChild != null) $get('txtHuyen_NNTHAY').value = rootCTU_HDR.getElementsByTagName("HUYEN_NNTHAY")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("TINH_NNTHAY")[0].firstChild != null) $get('txtTinh_NNTHAY').value = rootCTU_HDR.getElementsByTagName("TINH_NNTHAY")[0].firstChild.nodeValue;


                    if (rootCTU_HDR.getElementsByTagName("TTIEN")[0].firstChild != null) {
                        var strSoTien = rootCTU_HDR.getElementsByTagName("TTIEN")[0].firstChild.nodeValue;
                        $get('hdnTongTienCu').value = strSoTien;
                        _TTIEN = strSoTien.replaceAll('.', '');
                    }
                    if (rootCTU_HDR.getElementsByTagName("TTIEN_TN")[0].firstChild != null) {
                        $get('txtTongTrichNo').value = rootCTU_HDR.getElementsByTagName("TTIEN_TN")[0].firstChild.nodeValue;
                        jsFormatNumber('txtTongTrichNo');
                    }
                    //Lay thong tin ma_ks
                    if (rootCTU_HDR.getElementsByTagName("MA_KS")[0].firstChild != null)
                        $get('hdnMA_KS').value = rootCTU_HDR.getElementsByTagName("MA_KS")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("MA_SANPHAM")[0].firstChild != null)
                        $get('txtSanPham').value = rootCTU_HDR.getElementsByTagName("MA_SANPHAM")[0].firstChild.nodeValue;
                    if (rootCTU_HDR.getElementsByTagName("TRANG_THAI")[0].firstChild != null) {
                        curCtuStatus = rootCTU_HDR.getElementsByTagName("TRANG_THAI")[0].firstChild.nodeValue;
                        var strTTCT = "";
                        if (curCtuStatus == "00") {
                            jsGetTK_KH_NH();
                            strTTCT = "Chứng từ chưa chuyển kiểm soát";
                        }
                        if (curCtuStatus == "01")
                        { strTTCT = "Chứng từ đã kiểm soát"; }
                        if (curCtuStatus == "03") {
                            jsGetTK_KH_NH();
                            strTTCT = "Chứng từ ở trạng thái chuyển trả";
                        }
                        if (curCtuStatus == "04")
                        { strTTCT = "Chứng từ ở trạng thái hủy"; }
                        if (curCtuStatus == "05")
                        { strTTCT = "Chứng từ ở trạng chờ kiểm soát"; }
                        if (curCtuStatus == "06")
                        { strTTCT = "Chứng từ chưa nhập BDS"; }
                        document.getElementById('divStatus').innerHTML = strTTCT;
                    }

                }
                if (rootCTU_HDR.getElementsByTagName("DSToKhai")[0].firstChild != null) {
                    jsFill2Combobox(rootCTU_HDR.getElementsByTagName("DSToKhai")[0].firstChild.nodeValue);
                }

                var intPos = jsGetIndexByValue('ddlMaLoaiThue', strLoaiThue)
                document.getElementById('ddlMaLoaiThue').selectedIndex = intPos;
                ShowHideControlByLoaiThue();
              
                intPos = jsGetIndexByValue('ddlMaHTTT', strHTTT);
                document.getElementById('ddlMaHTTT').selectedIndex = intPos;
                ShowHideControlByPT_TT();

                var rootCTU_DTL = xmlDoc.getElementsByTagName('CTU_DTL').item(0);
                var node01 = rootCTU_DTL.childNodes.item(0);
                if (node01 != null) {
                    if (node01.childNodes[1].firstChild != null) $get('MA_CAP_1').value = node01.childNodes[1].firstChild.nodeValue;
                    if (node01.childNodes[2].firstChild != null) $get('MA_CHUONG_1').value = node01.childNodes[2].firstChild.nodeValue;
                    // if (node01.childNodes[3].firstChild!=null)  $get('MA_KHOAN_1').value = node01.childNodes[3].firstChild.nodeValue;
                    if (node01.childNodes[4].firstChild != null) $get('MA_MUC_1').value = node01.childNodes[4].firstChild.nodeValue;
                    if (node01.childNodes[5].firstChild != null) $get('NOI_DUNG_1').value = node01.childNodes[5].firstChild.nodeValue;
                    if (node01.childNodes[6].firstChild != null) $get('SOTIEN_1').value = node01.childNodes[6].firstChild.nodeValue;
                    if (node01.childNodes[7].firstChild != null) $get('SODATHU_1').value = node01.childNodes[7].firstChild.nodeValue;
                    if (node01.childNodes[8].firstChild != null) $get('KY_THUE_1').value = node01.childNodes[8].firstChild.nodeValue;
                    document.getElementById("rowGridDetail1").style.display = '';
                }

                var node02 = rootCTU_DTL.childNodes.item(1);
                if (node02 != null && node02.childNodes[1].firstChild != null) {
                    if (node02.childNodes[1].firstChild != null) $get('MA_CAP_2').value = node02.childNodes[1].firstChild.nodeValue;
                    if (node02.childNodes[2].firstChild != null) $get('MA_CHUONG_2').value = node02.childNodes[2].firstChild.nodeValue;
                    // if (node02.childNodes[3].firstChild!=null) $get('MA_KHOAN_2').value = node02.childNodes[3].firstChild.nodeValue;
                    if (node02.childNodes[4].firstChild != null) $get('MA_MUC_2').value = node02.childNodes[4].firstChild.nodeValue;
                    if (node02.childNodes[5].firstChild != null) $get('NOI_DUNG_2').value = node02.childNodes[5].firstChild.nodeValue;
                    if (node02.childNodes[6].firstChild != null) $get('SOTIEN_2').value = node02.childNodes[6].firstChild.nodeValue;
                    if (node02.childNodes[7].firstChild != null) $get('SODATHU_2').value = node02.childNodes[7].firstChild.nodeValue;
                    if (node02.childNodes[8].firstChild != null) $get('KY_THUE_2').value = node02.childNodes[8].firstChild.nodeValue;
                    document.getElementById("rowGridDetail2").style.display = '';
                }

                var node03 = rootCTU_DTL.childNodes.item(2);
                if (node03 != null && node03.childNodes[1].firstChild != null) {
                    if (node03.childNodes[1].firstChild != null) $get('MA_CAP_3').value = node03.childNodes[1].firstChild.nodeValue;
                    if (node03.childNodes[2].firstChild != null) $get('MA_CHUONG_3').value = node03.childNodes[2].firstChild.nodeValue;
                    //if (node03.childNodes[3].firstChild!=null)  $get('MA_KHOAN_3').value = node03.childNodes[3].firstChild.nodeValue;
                    if (node03.childNodes[4].firstChild != null) $get('MA_MUC_3').value = node03.childNodes[4].firstChild.nodeValue;
                    if (node03.childNodes[5].firstChild != null) $get('NOI_DUNG_3').value = node03.childNodes[5].firstChild.nodeValue;
                    if (node03.childNodes[6].firstChild != null) $get('SOTIEN_3').value = node03.childNodes[6].firstChild.nodeValue;
                    if (node03.childNodes[7].firstChild != null) $get('SODATHU_3').value = node03.childNodes[7].firstChild.nodeValue;
                    if (node03.childNodes[8].firstChild != null) $get('KY_THUE_3').value = node03.childNodes[8].firstChild.nodeValue;
                    document.getElementById("rowGridDetail3").style.display = '';
                }
                var node04 = rootCTU_DTL.childNodes.item(3);
                if (node04 != null && node04.childNodes[1].firstChild != null) {
                    if (node04.childNodes[1].firstChild != null) $get('MA_CAP_4').value = node04.childNodes[1].firstChild.nodeValue;
                    if (node04.childNodes[2].firstChild != null) $get('MA_CHUONG_4').value = node04.childNodes[2].firstChild.nodeValue;
                    // if (node04.childNodes[3].firstChild!=null)  $get('MA_KHOAN_4').value = node04.childNodes[3].firstChild.nodeValue;
                    if (node04.childNodes[4].firstChild != null) $get('MA_MUC_4').value = node04.childNodes[4].firstChild.nodeValue;
                    if (node04.childNodes[5].firstChild != null) $get('NOI_DUNG_4').value = node04.childNodes[5].firstChild.nodeValue;
                    if (node04.childNodes[6].firstChild != null) $get('SOTIEN_4').value = node04.childNodes[6].firstChild.nodeValue;
                    if (node04.childNodes[7].firstChild != null) $get('SODATHU_4').value = node04.childNodes[7].firstChild.nodeValue;
                    if (node04.childNodes[8].firstChild != null) $get('KY_THUE_4').value = node04.childNodes[8].firstChild.nodeValue;
                    document.getElementById("rowGridDetail4").style.display = '';
                }

                jsCalTotal();//Tinh tong tien cua chung tu
            }
            catch (e) {
                alert(e);
            }
        }

        //5)Hủy,Chuyển KS,Khôi phục chứng từ
        function jsUpdate_CTU_Status(strCTUAction) {
            // jsGet_Session();
            //        if(check_ss=='0'){
            //            window.open("../../pages/frmLogin.aspx","_self");
            //            return;
            //        }
            var blnEnableEdit = false;
            var strSo_FT = document.getElementById('hdnSO_CT').value;
            if (document.getElementById('divDescCTU').innerHTML.length > 0) {
                var arrDesCTU = document.getElementById('divDescCTU').innerHTML.split(":")[1].split("/");
                if (strCTUAction == '2') {//Chuyen kiem soat
                    curCtuStatus = '05';
                    jsEnableButtons(5);//disable tat ca cac nut                 
                    blnEnableEdit = true;
                } else if (strCTUAction == '4') {//Huy   
                    if (document.getElementById('hdnMA_KS').value.toString() != '' && curCtuStatus == '01') {
                        curCtuStatus = '02';
                        jsEnableButtons(3);//disable tat ca cac nut tru nut khoi phuc                
                        blnEnableEdit = true;
                    }
                    else {
                        curCtuStatus = '04';
                        jsEnableButtons(3);//disable tat ca cac nut tru nut khoi phuc                
                        blnEnableEdit = true;
                    }
                } else if (strCTUAction == '5') {//Khoi phuc
                    curCtuStatus = '99';//de trang thai 99 chu khong phai 00
                    jsEnableButtons(2);//enable tat ca cac nut tru nut in                                
                    blnEnableEdit = false;
                }
                //Lay trang thai cua chung tu cho biet
                jsEnableGridHeader(blnEnableEdit);
                jsEnableGridDetail(blnEnableEdit);
                document.getElementById('cmdThemMoi').disabled = true;
                document.getElementById('cmdGhiKS').disabled = true;
                document.getElementById('cmdGhiCT').disabled = true;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = true;
                document.getElementById('cmdInBS').disabled = true;
                document.getElementById('cmdKhoiPhuc').disabled = true;
                document.getElementById('cmdHuyCT').disabled = true;
                //alert(curCtuStatus);
                //PageMethods.Update_CTU_Status(curMa_NV,arrDesCTU[0],arrDesCTU[1],arrDesCTU[2],arrDesCTU[3],arrDesCTU[4],strCTUAction,Update_CTU_Status_Complete,Update_CTU_Status_Error);
                PageMethods.Update_CTU_Status(curMa_NV, arrDesCTU[0], arrDesCTU[1], arrDesCTU[2], strCTUAction, strSo_FT, curCtuStatus, "", Update_CTU_Status_Complete, Update_CTU_Status_Error);
            }
        }
        function Update_CTU_Status_Complete(result, userContext, methodName) {
            checkSS(result);
            if (result.length > 0) {
                var arrKQ = result.split(';');
                if (arrKQ[0] == "1") {
                    document.getElementById('divStatus').innerHTML = arrKQ[1];
                    //                alert('Hủy chứng từ');
                    //                document.getElementById('cmdInCT').disabled = true ;
                }
                else if (arrKQ[0] == "05") {
                    document.getElementById('divStatus').innerHTML = arrKQ[1];
                    jsLoadCTList();
                    document.getElementById('divDescCTU').innerHTML = arrKQ[2];
                    //alert('Đổi trạng thái 05');
                    document.getElementById('cmdInCT').disabled = false;
                }
                else if (arrKQ[0] == "04") {
                    document.getElementById('divStatus').innerHTML = arrKQ[1];
                    jsLoadCTList();
                    document.getElementById('divDescCTU').innerHTML = arrKQ[2];
                    //alert('Hủy');
                    document.getElementById('cmdInCT').disabled = true;
                }
                document.getElementById('cmdThemMoi').disabled = false;
            }
            else {
                document.getElementById('divStatus').innerHTML = "Thao tác không thành công.Hãy thực hiện lại!!!"
                //phuc hoi nguyen trang thai cac nut truoc thuc hien
                document.getElementById('cmdThemMoi').disabled = false;
                document.getElementById('cmdGhiKS').disabled = false;
                document.getElementById('cmdGhiCT').disabled = false;
                //document.getElementById('cmdChuyenKS').disabled = false;
                document.getElementById('cmdInCT').disabled = true;
                document.getElementById('cmdInBS').disabled = false;
                document.getElementById('cmdKhoiPhuc').disabled = false;
                document.getElementById('cmdHuyCT').disabled = false;
            }
        }
        function Update_CTU_Status_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình cập nhật trạng thái chứng từ" + error.get_message();
                //phuc hoi nguyen trang thai cac nut truoc thuc hien cmdThemMoi
                document.getElementById('cmdGhiKS').disabled = false;
                document.getElementById('cmdThemMoi').disabled = false;
                document.getElementById('cmdGhiCT').disabled = false;
                //document.getElementById('cmdChuyenKS').disabled = false;
                document.getElementById('cmdInCT').disabled = true;
                document.getElementById('cmdInBS').disabled = false;
                document.getElementById('cmdKhoiPhuc').disabled = false;
                document.getElementById('cmdHuyCT').disabled = false;
            }
        }
        //        //tynk_Session
        //        function jsGet_Session(){
        //        //alert('bug');
        //        PageMethods.GetSession(GetSession_Complete,GetSession_Error);
        //    }

        //    function GetSession_Complete(result){
        //        if(result=='0'){
        //            check_ss='0';
        //        }else{
        //            check_ss='1';
        //        }
        //    }
        //    function GetSession_Error(result){
        //       //alert('Xảy ra lỗi.' );
        //    }
        function jsLog_out() {
            window.open("../../pages/frmLogin.aspx", "_self");
            return;
        }
        //xxx)Lay thong tin tai ngan hang song phuong
        function jsValidNHSHB_SP(strSHKB) {
            if ($get('txtSHKB').value.length > 0) {
                PageMethods.ValidNHSHB_SP($get('txtSHKB').value.trim(), ValidNHSHB_SP_Complete, ValidNHSHB_SP_Error);
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Không tìm thấy SHKB';
            }
        }

        function ValidNHSHB_SP_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            if (result.length < 8) {
                document.getElementById("hndNHSHB").value = '';
            } else {
                document.getElementById("hndNHSHB").value = result;
                if (result.length > 0) {
                    $get('txtMA_NH_B').value = result;
                    jsGet_TenNH_B();
                }
               

            }
        }
        function ValidNHSHB_SP_Error(error, userContext, methodName) {
            if (error !== null) {

                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin ngân hàng hưởng" + error.get_message();
            }
        }

        //xxx)Lay thong tin tai ngan hang song phuong jsValidNHSHB_SP_GhiCtu
        function jsValidNHSHB_SP_GhiCtu(strSHKB) {
            if ($get('txtSHKB').value.length > 0) {
                PageMethods.ValidNHSHB_SP($get('txtSHKB').value.trim(), ValidNHSHB_SP_GhiCtu_Complete, ValidNHSHB_SP_GhiCtu_Error);
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Không tìm thấy SHKB';
            }
        }

        function ValidNHSHB_SP_GhiCtu_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            if (result.length < 8) {
                document.getElementById("hndNHSHB").value = '';
            } else {
                document.getElementById("hndNHSHB").value = result;
            }
        }
        function ValidNHSHB_SP_GhiCtu_Error(error, userContext, methodName) {
            if (error !== null) {

                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin ngân hàng hưởng" + error.get_message();
            }
        }

        function checkSS(param) {

            var arrKQ = param.split(';');
            if (arrKQ[0] == "ssF") {
                window.open("../../pages/Warning.html", "_self");
                return;
            }
        }
        //tynk
        //6)Ghi,Ghi và chuyển ks
        function jsGhi_CTU(updType) {
            //updType=1 : Ghi,=2 Ghi va chuyen ks
            //editMode=1 :Them moi,=2 cap nhat
            //        if(document.getElementById('Radio1').checked==true && document.getElementById('txtMA_NH_TT').value.substring(2,5) != '101' && $("#ddlMaTKCo").val() == '7111'){
            //        }else{
            //            if( ! confirm("Dữ liệu TK Có và Mã NH không hợp lệ để đi Citad mới. Bạn có tiếp tục không?"))
            //           {
            //            jsEnableButtons(1);
            //           return;
            //           }
            //        }

            //Neu la mien phi thi ko can txtSanPham
            // var _radioF = $("input[type='radio']:checked").val();
            //        if ($("#txtSanPham").val() == ''){
            //            alert("Hãy tính phí cho chứng từ");
            //            return;
            //        }

            //Kiem tra han muc

            jsValidMin();
            var _hanMuc = parseFloat($("#txtDesc_SoDu_KH_NH").val().replaceAll(".", ""));
            var _checkHM = parseFloat($("#hdLimitAmout").val().replaceAll(".", ""));
            var _tongTN = parseFloat($("#txtTongTrichNo").val().replaceAll(".", ""));
            if (document.getElementById("ddlMaHTTT").value == "05") {
                //nếu dùng tài khoản tiền mặt thì tạm thời không làm gì
            } else {
                if (_hanMuc < _tongTN && _checkHM != 0) {
                    alert("Tổng trích nợ khách hàng là: " + $("#txtTongTrichNo").val() + " > Hạn mức hiện tại của khách hàng: " + $("#txtDesc_SoDu_KH_NH").val());
                    return;
                }
            }
            if (document.getElementById("txtTongTien").value <= 0) {
                alert("Không lập chứng từ với số tiền bằng 0");
                return;
            }
            if (!jsCalCharacter()) {
                alert("Số ký tự vượt quá số lượng cho phép");
                return;
            }
            var editMode = '1';
            if (jsValidForm() == false) return;
            jsRemoveDumpRow(); //Xoa cac row co so tien =0 truoc khi ghi
            var strXML = jsFormToBean();
            //        if (updType=='1'){
            //            jsEnableButtons(2); 
            //        }else if (updType=='2'){//Ghi va chuyen ks
            //            curCtuStatus="00";
            //            jsEnableButtons(5);  
            //        }
            var vAction = $('#hddAction').val();
            //alert(vAction);
            if (vAction == 2) {//Cap nhat chung tu
                editMode = '2';
            } else {//Them moi chung tu
                editMode = '1';
                //jsCheck_Exit_NNT()
                if (document.getElementById('hdnSoLan_LapCT').value != '0' && document.getElementById('hdnSoLan_LapCT').value != '') {
                    if (!confirm("Mã số thuế này đã có giao dịch. Bạn có tiếp tục")) {
                        jsEnableButtons(1);
                        return;
                    }

                }
            }
            jsValidNHSHB_SP_GhiCtu($get('txtSHKB').value);
            if (document.getElementById('hndNHSHB').value.length >= 8 && document.getElementById('hndNHSHB').value != $get('txtMA_NH_B').value) {
                if (!confirm("Kho bạc trên có mở tài khoản tại SHB. Bạn có muốn tiếp tục lưu chứng từ với ngân hàng hưởng đã chọn?")) {
                    jsEnableButtons(1);
                    return;
                }

            }
            var tk_chuyen = document.getElementById('txtTK_KH_NH').value;
            var ht_tt = "00";
            if (document.getElementById("ddlMaHTTT").value == "01") {
                ht_tt = "01";
            }

            //for (var i = 1; i <= 4; i++) {
            //    if (document.getElementById("chkRemove" + i).checked) {
            //        if (document.getElementById("MA_CHUONG_" + i).value.length < 3) {
            //            alert("Mã chương " + i + " cần tối thiểu 3 ký tự");
            //            document.getElementById("MA_CHUONG_" + i).value = "";
            //            document.getElementById("MA_CHUONG_" + i).focus();
            //            return;
            //        } else if (document.getElementById("MA_MUC_" + i).value.length < 4) {
            //            alert("Mã NDKT " + i + " cần tối thiểu 4 ký tự");
            //            document.getElementById("MA_MUC_" + i).value = "";
            //            document.getElementById("MA_MUC_" + i).focus();
            //            return;
            //        } else if (document.getElementById("NOI_DUNG_" + i).value.length < 1) {
            //            alert("Nội dung " + i + " cần tối thiểu 4 ký tự");
            //            document.getElementById("NOI_DUNG_" + i).value = "";
            //            document.getElementById("NOI_DUNG_" + i).focus();
            //            return;
            //        }
            //    }
            //}

            //document.getElementById('cmdThemMoi').disabled = true;
            document.getElementById('cmdThemMoi').disabled = true;
            document.getElementById('cmdGhiKS').disabled = true;
            document.getElementById('cmdGhiCT').disabled = true;
            //document.getElementById('cmdChuyenKS').disabled = true;
            document.getElementById('cmdInCT').disabled = true;
            document.getElementById('cmdInBS').disabled = true;
            document.getElementById('cmdKhoiPhuc').disabled = true;
            document.getElementById('cmdHuyCT').disabled = true;
            $get('hdnTongTienCu').value = $get('txtTongTrichNo').value;
            PageMethods.Ghi_CTU(curMa_NV, strXML, updType, editMode,ht_tt,tk_chuyen, Ghi_CTU_Complete, Ghi_CTU_Error);
        }

        function Ghi_CTU_Complete(result, userContext, methodName) {
            checkSS(result);
            if (result.length > 0) {
                var arrKQ = result.split(';');
                if (arrKQ[0] != "0") {
                    document.getElementById('divStatus').innerHTML = arrKQ[1];
                    //phuc hoi nguyen trang thai cac nut truoc thuc hien
                    document.getElementById('cmdThemMoi').disabled = false;
                    document.getElementById('cmdGhiKS').disabled = false;
                    document.getElementById('cmdGhiCT').disabled = false;
                }
                else {
                    document.getElementById('divStatus').innerHTML = arrKQ[1];//'="Ghi mới chứng từ thành công";
                    document.getElementById('txtMaNNT').disabled = true;
                    document.getElementById('txtSHKB').disabled = true;
                    document.getElementById('cmdThemMoi').disabled = false;
                    jsLoadCTList();

                    document.getElementById('divDescCTU').innerHTML = arrKQ[2];
                    curSo_BT = arrKQ[2].split(':')[1].split('/')[2];
                    var curSoFT = arrKQ[2].split(':')[1].split('/')[1];
                    curSoCT = arrKQ[2].split(':')[1].split('/')[1];
                    var TT = arrKQ[2].split(':')[1].split('/')[3];
                    document.getElementById("hdnSoCT_NH").value = curSoFT;
                    document.getElementById("hdnSO_CT").value = curSoCT;
                    document.getElementById('hddAction').value = 2;
                    jsGetTK_KH_NH();

                    if (TT != '00') {
                        jsEnableButtons(5);
                    } else {
                        //curCtuStatus="00";
                        jsEnableButtons(2);
                    }
                }
            } else {
                document.getElementById('divStatus').innerHTML = "Thao tác không thành công.Hãy thực hiện lại!!!"
                //phuc hoi nguyen trang thai cac nut truoc thuc hien
                document.getElementById('cmdThemMoi').disabled = false;
                document.getElementById('cmdGhiKS').disabled = false;
                document.getElementById('cmdGhiCT').disabled = false;
            }
        }
        function Ghi_CTU_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình cập nhật chứng từ" + error.get_message();
                //phuc hoi nguyen trang thai cac nut truoc thuc hien
                document.getElementById('cmdThemMoi').disabled = false;
                document.getElementById('cmdGhiKS').disabled = false;
                document.getElementById('cmdGhiCT').disabled = false;
            }
        }


        //7)Lay thong tin noi dung kinh te
        function jsNoiDungKT(txtMaMuc, txtNoiDung) {
            var userContext = {};
            userContext.ControlID = txtNoiDung;

            var strMaNDKT = document.getElementById(txtMaMuc).value;
            if (strMaNDKT.length > 0) {
                PageMethods.Get_TenTieuMuc(strMaNDKT, Get_NoiDungKT_Complete, Get_NoiDungKT_Error, userContext);
            }
        }

        function Get_NoiDungKT_Complete(result, userContext, methodName) {
            checkSS(result);
            if (result.length > 0) {
                document.getElementById(userContext.ControlID).value = result;
                document.getElementById('divStatus').innerHTML = "Tìm thấy nội dung kinh tế phù hợp";
            } else {
                document.getElementById('divStatus').innerHTML = "Không tìm thấy nội dung kinh tế phù hợp.Hãy tìm lại";
            }
        }
        function Get_NoiDungKT_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy nội dung kinh tế" + error.get_message();
            }
        }
        function jsFormToBean() {       //alert($get('hdnSO_CT').value);
            var xmlResult = "<?xml version='1.0'?>";
            xmlResult += "<CTC_CTU>";
            xmlResult += "<CTU_HDR>";
            xmlResult += "<NgayLV></NgayLV>";
            xmlResult += "<SoCT>" + $get('hdnSO_CT').value + "</SoCT>";
            xmlResult += "<So_BT>" + curSo_BT + "</So_BT>";
            xmlResult += "<SHKB>" + $get('txtSHKB').value + "</SHKB>";
            xmlResult += "<TenKB></TenKB>";
            xmlResult += "<MaNNT>" + $get('txtMaNNT').value + "</MaNNT>";
            xmlResult += "<TenNNT>" + $get('txtTenNNT').value + "</TenNNT>";
            xmlResult += "<DChiNNT>" + $get('txtDChiNNT').value + "</DChiNNT>";
            xmlResult += "<MaNNTien>" + $get('txtMaNNTien').value + "</MaNNTien>";
            xmlResult += "<TenNNTien>" + $get('txtTenNNTien').value + "</TenNNTien>";
            xmlResult += "<DChiNNTien>" + $get('txtDC_NNTIEN').value + "</DChiNNTien>";
            xmlResult += "<MaCQThu>" + $get('txtMaCQThu').value + "</MaCQThu>";
            xmlResult += "<TenCQThu></TenCQThu>";
            xmlResult += "<MaDBHC>" + $get('txtMaDBHC').value + "</MaDBHC>";

            xmlResult += "<TenDBHC></TenDBHC>";
            xmlResult += "<TKNo>" + document.getElementById('ddlMaTKNo').value.replaceAll('-', '') + "</TKNo>";
            xmlResult += "<TenTKNo>" + $get('txtTKNo').value + "</TenTKNo>";
            xmlResult += "<TKCo>" + document.getElementById('ddlMaTKCo').value.replaceAll('-', '') + "</TKCo>";
            xmlResult += "<TenTKCo>" + $get('txtTKCo').value + "</TenTKCo>";
            xmlResult += "<HTTT>" + document.getElementById('ddlMaHTTT').value + "</HTTT>";
            xmlResult += "<SoCTNH>" + document.getElementById('hdnSoCT_NH').value + " </SoCTNH>";
            if (document.getElementById('ddlMaHTTT').value == "01") {
                xmlResult += "<TK_KH_NH>" + $get('txtTK_KH_NH').value + "</TK_KH_NH>";
                xmlResult += "<TenTK_KH_NH>" + $get('txtTenTK_KH_NH').value + "</TenTK_KH_NH>";
            }
            if (document.getElementById('ddlMaHTTT').value == "05") {
                xmlResult += "<TK_KH_NH>" + $get('txtTK_KH_NH').value + "</TK_KH_NH>";
                xmlResult += "<TenTK_KH_NH>" + $get('txtTenTK_KH_NH').value + "</TenTK_KH_NH>";
            }
            if (document.getElementById('ddlMaHTTT').value == "03") {
                xmlResult += "<TK_KH_NH>" + $get('txtTKGL').value + "</TK_KH_NH>";
                xmlResult += "<TenTK_KH_NH>" + $get('txtTenTKGL').value + "</TenTK_KH_NH>";
            }
            xmlResult += "<SoDu_KH_NH></SoDu_KH_NH>";
            xmlResult += "<NGAY_KH_NH>" + $get('txtNGAY_KH_NH').value + "</NGAY_KH_NH>";
            //07/06/2011 Kienvt:Them ma ngan hang A,B
            //xmlResult +="<MA_NH_A>" + $get('txtMA_NH_A').value + "</MA_NH_A>";                 
            xmlResult += "<MA_NH_A>" + document.getElementById('ddlMA_NH_A').value + "</MA_NH_A>";
            xmlResult += "<Ten_NH_A>" + $get('txtTEN_NH_TT').value + "</Ten_NH_A>";
            xmlResult += "<MA_NH_B>" + $get('txtMA_NH_B').value + "</MA_NH_B>";
            xmlResult += "<Ten_NH_B>" + $get('txtTenMA_NH_B').value + "</Ten_NH_B>";
            //end
            xmlResult += "<LoaiThue>" + document.getElementById('ddlMaLoaiThue').value + "</LoaiThue>";
            xmlResult += "<DescLoaiThue></DescLoaiThue>";
            xmlResult += "<MaNT>" + $get('txtTenNT').value + "</MaNT>";
            xmlResult += "<TenNT></TenNT>";
            xmlResult += "<ToKhaiSo>" + $get('txtToKhaiSo').value + "</ToKhaiSo>";
            xmlResult += "<NgayDK>" + $get('txtNgayDK').value + "</NgayDK>";
            xmlResult += "<LHXNK>" + $get('txtLHXNK').value + "</LHXNK>";
            xmlResult += "<DescLHXNK>" + $get('txtDescLHXNK').value + "</DescLHXNK>";
            xmlResult += "<SoKhung>" + $get('txtSoKhung').value + "</SoKhung>";
            xmlResult += "<SoMay>" + $get('txtSoMay').value + "</SoMay>";
            xmlResult += "<So_BK>" + $get('txtSo_BK').value + "</So_BK>";
            xmlResult += "<NgayBK>" + $get('txtNgayBK').value + "</NgayBK>";
            xmlResult += "<TRANG_THAI></TRANG_THAI>";
            xmlResult += "<TT_BDS>" + curBDS_Status + "</TT_BDS>";
            xmlResult += "<TT_NSTT>N</TT_NSTT>";
            xmlResult += "<PHI_GD>" + $get('txtCharge').value + "</PHI_GD>";
            xmlResult += "<PHI_VAT>" + $get('txtVAT').value + "</PHI_VAT>";


            if (document.getElementById('chkChargeTypeMP').checked) {
                xmlResult += "<PT_TINHPHI>" + document.getElementById('chkChargeTypeMP').value + "</PT_TINHPHI>";//tu dong
            } else if (document.getElementById('chkChargeTypePT').checked) {
                xmlResult += "<PT_TINHPHI>" + document.getElementById('chkChargeTypePT').value + "</PT_TINHPHI>";//tu dong
            } else {
                xmlResult += "<PT_TINHPHI>" + document.getElementById('chkChargeTypePN').value + "</PT_TINHPHI>";//tu dong
            }
            xmlResult += "<Huyen_NNTIEN>" + $get('txtHuyen_NNT').value + "</Huyen_NNTIEN>";
            xmlResult += "<Tinh_NNTIEN>" + $get('txtTinh_NNT').value + "</Tinh_NNTIEN>";
            xmlResult += "<TTIEN> </TTIEN>";
            xmlResult += "<TT_TTHU> </TT_TTHU>";
            xmlResult += "<MA_NV> </MA_NV>";
            xmlResult += "<DSToKhai> </DSToKhai>";
            /****************************BEGIN********************************/
            xmlResult += "<MA_HQ> </MA_HQ>";
            xmlResult += "<LOAI_TT> </LOAI_TT>"; //ASDF
            xmlResult += "<TEN_HQ> </TEN_HQ>";
            xmlResult += "<MA_HQ_PH> </MA_HQ_PH>";
            xmlResult += "<TEN_HQ_PH> </TEN_HQ_PH>";

            /****************************END**********************************/

            xmlResult += "<MA_NH_TT>" + $get('txtMA_NH_TT').value + "</MA_NH_TT>";
            xmlResult += "<TEN_NH_TT>" + $get('txtTEN_NH_TT').value + "</TEN_NH_TT>";
            xmlResult += "<MA_KS></MA_KS>";
            xmlResult += "<KHCT>" + $get('txtKyHieuCT').value + " </KHCT>";
            xmlResult += "<TT_CTHUE></TT_CTHUE>";
            xmlResult += "<MA_SANPHAM>" + $get('txtSanPham').value + "</MA_SANPHAM>";
            xmlResult += "<TTIEN_TN> " + $get('txtTongTrichNo').value.replaceAll(".", "") + " </TTIEN_TN>";
            if (document.getElementById('Radio1').checked) {
                xmlResult += "<TT_CITAD>" + document.getElementById('Radio1').value + "</TT_CITAD>";
            } else {
                xmlResult += "<TT_CITAD>" + document.getElementById('Radio3').value + "</TT_CITAD>";
            }
            xmlResult += "<MA_NTK>" + document.getElementById('ddlMa_NTK').value + "</MA_NTK>";

            //sonmt
            xmlResult += "<DIEN_GIAI>" + document.getElementById('txtRemarks').value + "</DIEN_GIAI>";
            xmlResult += "<MA_HUYEN>" + document.getElementById('txtHuyen_NNT').value + "</MA_HUYEN>";
            xmlResult += "<TEN_HUYEN>" + document.getElementById('txtTenHuyen_NNT').value + "</TEN_HUYEN>";
            xmlResult += "<MA_TINH>" + document.getElementById('txtTinh_NNT').value + "</MA_TINH>";
            xmlResult += "<TEN_TINH>" + document.getElementById('txtTenTinh_NNT').value + "</TEN_TINH>";

            xmlResult += "<HUYEN_NNTHAY>" + document.getElementById('txtHuyen_NNTHAY').value + "</HUYEN_NNTHAY>";
            xmlResult += "<TINH_NNTHAY>" + document.getElementById('txtTinh_NNTHAY').value + "</TINH_NNTHAY>";
            xmlResult += "<SO_QD>" + document.getElementById('txtSOQD').value + "</SO_QD>";

            xmlResult += "<CQ_QD>" + document.getElementById('txtCQQD').value + "</CQ_QD>";
            xmlResult += "<NGAY_QD>" + document.getElementById('txtNgayQD').value + "</NGAY_QD>";
            xmlResult += "<GHI_CHU>" + document.getElementById('txtGhiChu').value + "</GHI_CHU>";


            //sonmt
            xmlResult += "<LOAI_NNT>" + $get('txtMaLoaiNNT').value.trim() + "</LOAI_NNT>";
            xmlResult += "<TEN_LOAI_NNT>" + $get('txtTenLoaiNNT').value.trim() + "</TEN_LOAI_NNT>";

            xmlResult += "</CTU_HDR>";
            xmlResult += "<CTU_DTL>";

            if (document.getElementById("chkRemove1").checked) {
                if ($get('SOTIEN_1').value != '') {
                    xmlResult += "<ITEMS>";
                    xmlResult += "<ChkRemove></ChkRemove>";
                    xmlResult += "<MA_CAP>" + $get('MA_CAP_1').value + "</MA_CAP>";
                    xmlResult += "<MA_CHUONG>" + $get('MA_CHUONG_1').value + "</MA_CHUONG>";
                    xmlResult += "<MA_KHOAN>000</MA_KHOAN>";
                    xmlResult += "<MA_MUC>" + $get('MA_MUC_1').value + "</MA_MUC>";
                    xmlResult += "<NOI_DUNG>" + $get('NOI_DUNG_1').value + "</NOI_DUNG>";
                    xmlResult += "<SOTIEN>" + $get('SOTIEN_1').value + "</SOTIEN>";
                    xmlResult += "<SoDT></SoDT>";
                    xmlResult += "<KY_THUE>" + $get('KY_THUE_1').value + "</KY_THUE>";
                    xmlResult += "</ITEMS>";
                }
            }
            if (document.getElementById("chkRemove2").checked) {
                if ($get('SOTIEN_2').value != '') {
                    xmlResult += "<ITEMS>";
                    xmlResult += "<ChkRemove></ChkRemove>";
                    xmlResult += "<MA_CAP>" + $get('MA_CAP_2').value + "</MA_CAP>";
                    xmlResult += "<MA_CHUONG>" + $get('MA_CHUONG_2').value + "</MA_CHUONG>";
                    xmlResult += "<MA_KHOAN>000</MA_KHOAN>";
                    xmlResult += "<MA_MUC>" + $get('MA_MUC_2').value + "</MA_MUC>";
                    xmlResult += "<NOI_DUNG>" + $get('NOI_DUNG_2').value + "</NOI_DUNG>";
                    xmlResult += "<SOTIEN>" + $get('SOTIEN_2').value + "</SOTIEN>";
                    xmlResult += "<SoDT></SoDT>";
                    xmlResult += "<KY_THUE>" + $get('KY_THUE_2').value + "</KY_THUE>";
                    xmlResult += "</ITEMS>";
                }
            }
            if (document.getElementById("chkRemove3").checked) {
                if ($get('SOTIEN_3').value != '') {
                    xmlResult += "<ITEMS>";
                    xmlResult += "<ChkRemove></ChkRemove>";
                    xmlResult += "<MA_CAP>" + $get('MA_CAP_3').value + "</MA_CAP>";
                    xmlResult += "<MA_CHUONG>" + $get('MA_CHUONG_3').value + "</MA_CHUONG>";
                    xmlResult += "<MA_KHOAN>000</MA_KHOAN>";
                    xmlResult += "<MA_MUC>" + $get('MA_MUC_3').value + "</MA_MUC>";
                    xmlResult += "<NOI_DUNG>" + $get('NOI_DUNG_3').value + "</NOI_DUNG>";
                    xmlResult += "<SOTIEN>" + $get('SOTIEN_3').value + "</SOTIEN>";
                    xmlResult += "<SoDT></SoDT>";
                    xmlResult += "<KY_THUE>" + $get('KY_THUE_3').value + "</KY_THUE>";
                    xmlResult += "</ITEMS>";
                }
            }
            if (document.getElementById("chkRemove4").checked) {
                if ($get('SOTIEN_4').value != '') {
                    xmlResult += "<ITEMS>";
                    xmlResult += "<ChkRemove></ChkRemove>";
                    xmlResult += "<MA_CAP>" + $get('MA_CAP_4').value + "</MA_CAP>";
                    xmlResult += "<MA_CHUONG>" + $get('MA_CHUONG_4').value + "</MA_CHUONG>";
                    xmlResult += "<MA_KHOAN>000</MA_KHOAN>";
                    xmlResult += "<MA_MUC>" + $get('MA_MUC_4').value + "</MA_MUC>";
                    xmlResult += "<NOI_DUNG>" + $get('NOI_DUNG_4').value + "</NOI_DUNG>";
                    xmlResult += "<SOTIEN>" + $get('SOTIEN_4').value + "</SOTIEN>";
                    xmlResult += "<SoDT></SoDT>";
                    xmlResult += "<KY_THUE>" + $get('KY_THUE_4').value + "</KY_THUE>";
                    xmlResult += "</ITEMS>";
                }
            }




            xmlResult += "</CTU_DTL>";
            xmlResult += "</CTC_CTU>";
            //Lay thong tin cua phan chi tiet
            return xmlResult;
        }

        function jsIn_CT(isBS) {
            // jsGet_Session();
            //        if(check_ss=='0'){
            //            window.open("../../pages/frmLogin.aspx","_self");
            //            return;
            //        }
            var width = screen.availWidth - 100;
            var height = screen.availHeight - 10;
            var left = 0;
            var top = 0;
            var params = 'width=' + width + ', height=' + height;
            params += ', top=' + top + ', left=' + left;
            params += ', directories=no';
            params += ', location=no';
            params += ', menubar=yes';
            params += ', resizable=no';
            params += ', scrollbars=yes';
            params += ', status=no';
            params += ', toolbar=no';
            if (document.getElementById('divDescCTU').innerHTML.length > 0) {
                var strSHKB = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[0];
                var strSoCT = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[1];
                var strSo_BT = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[2];
                var MaNV = curMa_NV;
                var ngayLV = dtmNgayLV;
            }
            //window.open("../../pages/Baocao/frmInGNT.aspx?SoCT=" + strSoCT + "&SHKB=" + strSHKB + "&SoBT=" + strSo_BT + "&NgayKB=" + ngayLV + "&MaNV=" + MaNV + "&BS=" + isBS, "", params);
            //
            window.open("../../pages/Baocao/InGNTpdf.ashx?SoCT=" + strSoCT + "&SHKB=" + strSHKB + "&SoBT=" + strSo_BT + "&NgayKB=" + ngayLV + "&MaNV=" + MaNV + "&BS=" + isBS, "", params);
        }

        function jsFillMA_NH_A() {
            var cb = document.getElementById('ddlMA_NH_A');
            cb.options.length = 0;
            for (var i = 0; i < arrMA_NHA.length; i++) {
                var arr = arrMA_NHA[i].split(';');
                var value = arr[0];
                var label = arr[1];
                cb.options[cb.options.length] = new Option(label, value);
            }
        }
        function jsFillPTTT() {
            var cb = document.getElementById('ddlMaHTTT');
            cb.options.length = 0;
            for (var i = 0; i < arrPTTT.length; i++) {
                var arr = arrPTTT[i].split(';');
                var value = arr[0];
                var label = arr[1];
                cb.options[cb.options.length] = new Option(label, value);
            }
        }
        function jsThem_Moi() {
            document.getElementById('hddAction').value = 1;
            flag_CQThu = 0;
            //1)Xoa grid header
            jsClearGridHeader(true, true);//khong xoa gt mac dinh                                 
            jsClearGridDetail();
            jsSetDefaultValues();
            jsFillNguyenTe();
            jsFillLTHUE();
            jsFillMA_NH_A();
            jsFillPTTT();
            jsGet_Citad_Type();
            jsLoadCTList();
            jsEnableGridHeader(false);
            jsEnableGridDetail(false);

            document.getElementById('divStatus').innerHTML = "Chế độ làm việc: Lập chứng từ";
            document.getElementById('divDescCTU').innerHTML = "";
            jsEnableButtons(0);
            jsShowHideRowProgress(true);

            curCtuStatus = '99';
            curMaNNT = "";
            document.getElementById('divTTBDS').innerHTML = (curBDS_Status == 'O' ? 'Trạng thái BDS : Online' : 'Trạng thái BDS : Offline');

            if (curBDS_Status == 'F') {
                document.getElementById('ddlMaHTTT').selectedIndex = 0;
                document.getElementById('ddlMaHTTT').disabled = true;
            }
            //Cap nhat lai so ky tu
            document.getElementById('leftCharacter').innerHTML = charNumberLimit;

            //focus ban dau
            $get('txtMaNNT').focus();
        }
        function jsFillNguyenTe() {
            var cb = document.getElementById('cboNguyenTe');
            var arr = arrMaNT;
            cb.options.length = 0;
            for (var i = 0; i < arrMaNT.length; i++) {
                var arr = arrMaNT[i].split(';');
                var value = arr[0];
                var label = arr[1];
                cb.options[cb.options.length] = new Option(label, value);
            }
        }
        function jsFillLTHUE() {
            var cb = document.getElementById('ddlMaLoaiThue');
            var arr = arrMaLTHUE;
            cb.options.length = 0;
            for (var i = 0; i < arrMaLTHUE.length; i++) {
                var arr = arrMaLTHUE[i].split(';');
                var value = arr[0];
                var label = arr[1];
                cb.options[cb.options.length] = new Option(label, value);
            }
        }
        function showNT() {
            var cb = document.getElementById('cboNguyenTe').value;
            $get('txtTenNT').value = cb;
        }
        function jsEnableButtons(intButtonType) {
            //alert (intButtonType);
            if (intButtonType == 0) //trang thai ban dau : ko cho hien thi nut nao ca
            {
                document.getElementById('cmdGhiKS').disabled = true;
                document.getElementById('cmdGhiCT').disabled = true;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = true;
                document.getElementById('cmdInBS').disabled = true;
                document.getElementById('cmdKhoiPhuc').disabled = true;
                document.getElementById('cmdHuyCT').disabled = true;
                document.getElementById('cmdTinhPhi').disabled = true;
            }
            else if (intButtonType == 1) //sau khi da co ma so thue ->hien thi 2 nut ghi
            {
                document.getElementById('cmdGhiKS').disabled = false;
                document.getElementById('cmdGhiCT').disabled = false;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = true;
                document.getElementById('cmdInBS').disabled = true;
                document.getElementById('cmdKhoiPhuc').disabled = true;
                document.getElementById('cmdHuyCT').disabled = true;
                document.getElementById('cmdTinhPhi').disabled = false;
            }
            else if (intButtonType == 2) //sau khi an nut ghi thanh cong 
            {
                document.getElementById('cmdGhiKS').disabled = false;
                document.getElementById('cmdGhiCT').disabled = false;
                //document.getElementById('cmdChuyenKS').disabled = false;
                document.getElementById('cmdInCT').disabled = true;
                document.getElementById('cmdInBS').disabled = true;
                document.getElementById('cmdKhoiPhuc').disabled = true;
                document.getElementById('cmdHuyCT').disabled = false;
                document.getElementById('cmdTinhPhi').disabled = false;
            }
            else if (intButtonType == 3) // sau khi an nut huy thanh cong
            {
                document.getElementById('cmdGhiKS').disabled = true;
                document.getElementById('cmdGhiCT').disabled = true;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = true;
                document.getElementById('cmdInBS').disabled = true;
                document.getElementById('cmdKhoiPhuc').disabled = false;
                document.getElementById('cmdHuyCT').disabled = true;
                document.getElementById('cmdTinhPhi').disabled = true;
            }
            else if (intButtonType == 4) // chung tu da huy cho phep khoi phuc chung tu
            {
                document.getElementById('cmdGhiKS').disabled = true;
                document.getElementById('cmdGhiCT').disabled = true;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = true;
                document.getElementById('cmdInBS').disabled = true;
                document.getElementById('cmdKhoiPhuc').disabled = false;
                document.getElementById('cmdHuyCT').disabled = true;
                document.getElementById('cmdTinhPhi').disabled = true;
            }
            else if (intButtonType == 5) // chung tu da chuyen sang trang thai cho kiem soat 
            {
                document.getElementById('cmdGhiKS').disabled = true;
                document.getElementById('cmdGhiCT').disabled = true;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = false;
                document.getElementById('cmdInBS').disabled = true;
                document.getElementById('cmdKhoiPhuc').disabled = true;
                document.getElementById('cmdHuyCT').disabled = true;
                document.getElementById('cmdTinhPhi').disabled = true;
            }
            else if (intButtonType == 6) // chung tu  da duoc kiem soat
            {
                document.getElementById('cmdGhiKS').disabled = true;
                document.getElementById('cmdGhiCT').disabled = true;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = false;
                document.getElementById('cmdInBS').disabled = true;
                document.getElementById('cmdKhoiPhuc').disabled = true;
                document.getElementById('cmdHuyCT').disabled = true;
                document.getElementById('cmdTinhPhi').disabled = true;

            }
            else if (intButtonType == 7) // chung tu o trang thai Chuyen tra
            {
                document.getElementById('cmdGhiKS').disabled = false;
                document.getElementById('cmdGhiCT').disabled = false;
                //document.getElementById('cmdChuyenKS').disabled = false;
                document.getElementById('cmdInCT').disabled = true;
                document.getElementById('cmdInBS').disabled = true;
                document.getElementById('cmdKhoiPhuc').disabled = true;
                document.getElementById('cmdHuyCT').disabled = false;
            } else if (intButtonType == 8) // sau khi an nut huy chung tu da kiem soat thanh cong
            {
                document.getElementById('cmdGhiKS').disabled = true;
                document.getElementById('cmdGhiCT').disabled = true;
                //document.getElementById('cmdChuyenKS').disabled = true;
                document.getElementById('cmdInCT').disabled = true;
                document.getElementById('cmdInBS').disabled = true;
                document.getElementById('cmdKhoiPhuc').disabled = false;
                document.getElementById('cmdHuyCT').disabled = true;
                document.getElementById('cmdTinhPhi').disabled = true;
            }
            if (document.getElementById('ddlMaHTTT').value == '00') {
                jsEnabledButtonByBDS();
            }
        }
        function jsEnabledButtonByBDS() {
            //00,01,03,04,05,99
            if (document.getElementById('ddlMaHTTT').value == '00') {
                if (curCtuStatus == '99') {
                    if (curBDS_Status == 'O') {
                        document.getElementById('cmdGhiKS').disabled = true;
                        //document.getElementById('cmdChuyenKS').disabled = true;
                    } else {
                        document.getElementById('cmdGhiKS').disabled = false;
                        //document.getElementById('cmdNhapBDS') .disabled=true;
                        //document.getElementById('cmdChuyenKS').disabled = false;
                    }
                }
                else if (curCtuStatus == '00') {
                    if (document.getElementById('divDescCTU').innerHTML.length > 0) {
                        if (curBDS_Status == 'O') {

                            if (document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[4] == 'N' && document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[3] == 'O') {

                                document.getElementById('cmdGhiKS').disabled = true;
                                //document.getElementById('cmdChuyenKS').disabled = true;
                                document.getElementById('cmdHuyCT').disabled = false;

                            }
                            else if (document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[4] == 'Y' && document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[3] == 'O') {
                                document.getElementById('cmdGhiKS').disabled = true;
                                //document.getElementById('cmdChuyenKS').disabled = false;
                                document.getElementById('cmdHuyCT').disabled = false;
                            }
                            else {
                                document.getElementById('cmdGhiKS').disabled = false;
                                //document.getElementById('cmdChuyenKS').disabled = false;
                                document.getElementById('cmdHuyCT').disabled = true;//khong cho huy ctu da nhap sott

                            }
                        }
                        else
                            if (document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[3] == 'O') {
                                document.getElementById('cmdGhiKS').disabled = true;
                                document.getElementById('cmdGhi').disabled = true;
                                //document.getElementById('cmdChuyenKS').disabled = true;
                                document.getElementById('cmdHuyCT').disabled = true;//khong cho huy ctu da nhap sott
                                document.getElementById('divStatus').innerHTML = 'Chứng từ này lập ở trạng thái BDS Online.'
                            }

                    }
                }
                else if (curCtuStatus == '03') {
                    if (curBDS_Status == 'O' && document.getElementById('divDescCTU').innerHTML.length > 0) {
                        document.getElementById('cmdGhiKS').disabled = true;
                    }
                    if (curBDS_Status == 'F' && document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[3] == 'O') {
                        document.getElementById('cmdGhiKS').disabled = true;
                        document.getElementById('cmdGhi').disabled = true;
                        //document.getElementById('cmdChuyenKS').disabled = true;
                        document.getElementById('cmdHuyCT').disabled = true;//khong cho huy ctu da nhap sott
                        document.getElementById('divStatus').innerHTML = 'Chứng từ này lập ở trạng thái BDS Online.'
                    }
                }
            }
            else {
                if (curCtuStatus == '00' || curCtuStatus == '03' || curCtuStatus == '99') {
                    document.getElementById('cmdGhiKS').disabled = false;
                    //document.getElementById('cmdChuyenKS').disabled = true;
                }
            }
        }
        function jsAddNewDetailRow(rowID) {
            if (rowID == '1' && $get('SOTIEN_1').value.length > 0 && $get('KY_THUE_1').value.length > 0) {
                $get('MA_CAP_2').value = $get('MA_CAP_1').value;
                $get('MA_CHUONG_2').value = $get('MA_CHUONG_1').value;
                $get('MA_KHOAN_2').value = $get('MA_KHOAN_1').value;
                $get('KY_THUE_2').value = $get('KY_THUE_1').value;
                document.getElementById('rowGridDetail2').style.display = '';
                document.getElementById('MA_KHOAN_2').focus();
            }

            else if (rowID == '2' && $get('SOTIEN_2').value.length > 0 && $get('KY_THUE_2').value.length > 0) {
                $get('MA_CAP_3').value = $get('MA_CAP_1').value;
                $get('MA_CHUONG_3').value = $get('MA_CHUONG_1').value;
                $get('MA_KHOAN_3').value = $get('MA_KHOAN_1').value;
                $get('KY_THUE_3').value = $get('KY_THUE_1').value;
                document.getElementById('rowGridDetail3').style.display = '';
                document.getElementById('MA_KHOAN_3').focus();
            }
            else if (rowID == '3' && $get('SOTIEN_3').value.length > 0 && $get('KY_THUE_3').value.length > 0) {
                $get('MA_CAP_4').value = $get('MA_CAP_1').value;
                $get('MA_CHUONG_4').value = $get('MA_CHUONG_1').value;
                // $get('MA_KHOAN_4').value = $get('MA_KHOAN_1').value;                 
                $get('KY_THUE_4').value = $get('KY_THUE_1').value;
                document.getElementById('rowGridDetail4').style.display = '';
                //document.getElementById('MA_KHOAN_4').focus();
            }
        }

        function jsClearGridDetail() {


            $get('MA_CHUONG_1').value = '';
            $get('MA_MUC_1').value = '';
            $get('NOI_DUNG_1').value = '';
            $get('SOTIEN_1').value = '';
            $get('SODATHU_1').value = '';
            $get('KY_THUE_1').value = '';//curKyThue;     


            $get('MA_CHUONG_2').value = '';
            //$get('MA_KHOAN_2').value = '';     
            $get('MA_MUC_2').value = '';
            $get('NOI_DUNG_2').value = '';
            $get('SOTIEN_2').value = '';
            $get('SODATHU_2').value = '';
            $get('KY_THUE_2').value = '';


            $get('MA_CHUONG_3').value = '';
            //  $get('MA_KHOAN_3').value = '';     
            $get('MA_MUC_3').value = '';
            $get('NOI_DUNG_3').value = '';
            $get('SOTIEN_3').value = '';
            $get('SODATHU_3').value = '';
            $get('KY_THUE_3').value = '';


            $get('MA_CHUONG_4').value = '';
            // $get('MA_KHOAN_4').value = '';     
            $get('MA_MUC_4').value = '';
            $get('NOI_DUNG_4').value = '';
            $get('SOTIEN_4').value = '';
            $get('SODATHU_4').value = '';
            $get('KY_THUE_4').value = '';

            document.getElementById('rowGridDetail1').style.display = '';
            document.getElementById('rowGridDetail2').style.display = 'none';
            document.getElementById('rowGridDetail3').style.display = 'none';
            document.getElementById('rowGridDetail4').style.display = 'none';

            $get('txtTongTien').value = '0';
            $get('txtCharge').value = '0';
            $get('txtVAT').value = '0';
            $get('txtTongTrichNo').value = '0';
            // document.getElementById('chkChargeType').checked=true;

            document.getElementById('chkRemove1').checked = true;
            document.getElementById('chkRemove2').checked = true;
            document.getElementById('chkRemove3').checked = true;
            document.getElementById('chkRemove4').checked = true;

        }
        function jsSetDefaultValues() {
            document.getElementById('txtNGAY_KH_NH').value = dtmNgayNH;//dtmNgayLV;  
            document.getElementById('txtTenNT').value = 'VND';
            document.getElementById('cboNguyenTe').value = 'VND';
            if (defSHKB.length > 0) {
                var arr1 = defSHKB.split(';');
                if ($get('txtSHKB').value.length == 0) {
                    $get('txtSHKB').value = arr1[0];
                    $get('txtTenKB').value = arr1[1];
                    jsLoadDM('TAIKHOANNO', arr1[0]);
                    jsLoadDM('TAIKHOANCO', arr1[0]);

                }
            }
            if (defCQThu.length > 0) {
                var arr2 = defCQThu.split(';');
                if ($get('txtMaCQThu').value.length == 0) {
                    $get('txtMaCQThu').value = arr2[0];
                    $get('txtTenCQThu').value = arr2[1];
                }
            }
            if (defKHCT.length > 0) {
                var arr2 = defKHCT.split(';');
                if ($get('txtKyHieuCT').value.length == 0) {
                    $get('txtKyHieuCT').value = arr2[0];

                }
            }
            if (defDBHC.length > 0) {
                var arr3 = defDBHC.split(';');
                if ($get('txtMaDBHC').value.length == 0) {

                    $get('txtMaDBHC').value = arr3[0];
                    $get('txtTenDBHC').value = arr3[1];
                    //console.log("1.MADBHC: " + $get('txtMaDBHC').value);
                }
            }
            //07/06/11 Them default cho defMaNHNhan
            if (defMaNHNhan.length > 0) {
                var arr4 = defMaNHNhan.split(';');
                if ($get('txtMA_NH_B').value.length == 0) {
                    $get('txtMA_NH_B').value = arr4[0];
                    $get('txtTenMA_NH_B').value = arr4[1];
                    $get('txtMA_NH_TT').value = arr4[2];
                    $get('txtTEN_NH_TT').value = arr4[3];
                }
            }
        }
        //    function getTenNHChuyen()
        //    {
        //    
        //    }    
        function jsEnableGridHeader(blnEnable) {
            //if( document.getElementById('txtMaNNT').value==defMaVangLai)
            //{
            document.getElementById('txtTenNNT').disabled = blnEnable;
            //}
            document.getElementById('cboNguyenTe').disabled = blnEnable;
            document.getElementById('ddlMaHTTT').disabled = blnEnable;
            document.getElementById('ddlMaLoaiThue').disabled = blnEnable;
            document.getElementById('ddlMaTKCo').disabled = blnEnable;
            document.getElementById('ddlMaTKNo').disabled = blnEnable;
            document.getElementById('ddlMa_NTK').disabled = blnEnable;
            document.getElementById('txtTKNo').disabled = blnEnable;
            document.getElementById('txtTKCo').disabled = 'none';
            //document.getElementById('grdHeader').disabled=blnEnable;                         
            //kienvt : enabled control in disabled grdheader


            $get('txtSHKB').disabled = blnEnable;
            $get('txtMaNNT').disabled = blnEnable;
            $get('txtTenNNT').disabled = blnEnable;
            $get('txtDChiNNT').disabled = blnEnable;
            $get('txtMaNNTien').disabled = blnEnable;
            $get('txtTenNNTien').disabled = blnEnable;
            $get('txtDChiNNTien').disabled = blnEnable;
            //$get('txtQuan_HuyenNNTien').disabled = blnEnable;
            // $get('txtTinh_NNTien').disabled = blnEnable;  
            $get('txtMA_NH_TT').disabled = blnEnable;
            $get('txtMA_NH_B').disabled = blnEnable;
            $get('txtMaDBHC').disabled = blnEnable;
            $get('txtMaCQThu').disabled = blnEnable;
            //$get('txtTK_KH_NH').disabled = blnEnable;
            $get('txtTKGL').disabled = blnEnable;
            // $get('txtNGAY_KH_NH').disabled = blnEnable;
            $get('txtDesNGAY_KH_NH').disabled = blnEnable;
            $get('txtToKhaiSo').disabled = blnEnable;
            $get('txtNgayDK').disabled = blnEnable;
            $get('txtLHXNK').disabled = blnEnable;
            $get('txtSoKhung').disabled = blnEnable;
            $get('txtSoMay').disabled = blnEnable;

            $get('txtSOQD').disabled = blnEnable;
            $get('txtNgayQD').disabled = blnEnable;
            $get('txtCQQD').disabled = blnEnable;


            $get('txtSo_BK').disabled = blnEnable;
            $get('txtNgayBK').disabled = blnEnable;
            $get('chkChargeTypeMP').disabled = blnEnable;
            $get('chkChargeTypePT').disabled = blnEnable;
            $get('chkChargeTypePN').disabled = blnEnable;
            document.getElementById('ddlMaHTTT').disabled = blnEnable;
            document.getElementById('ddlMaLoaiThue').disabled = blnEnable;
            document.getElementById('ddlMA_NH_A').disabled = blnEnable;

            //         if (blnEnable)
            //         {
            //            document.getElementById("rowSoDuNH").visible='none';
            //         }else
            //         {  
            //            document.getElementById("rowSoDuNH").visible='';       
            //            
            //         }
            //$get('txtCharge').disabled = blnEnable; 
            // document.getElementById('chkChargeType').disabled = blnEnable;   
        }

        function jsEnableGridDetail(blnEnable) {
            //document.getElementById('grdChiTiet').disabled=blnEnable;
            //Kienvt:Sua lai enabled grid thi an tung dieu kihen
            $get('chkRemove1').disabled = blnEnable;
            $get('MA_CHUONG_1').disabled = blnEnable;
            $get('MA_KHOAN_1').disabled = blnEnable;
            $get('MA_MUC_1').disabled = blnEnable;
            $get('NOI_DUNG_1').disabled = blnEnable;
            $get('SOTIEN_1').disabled = blnEnable;
            $get('KY_THUE_1').disabled = blnEnable;

            $get('chkRemove2').disabled = blnEnable;
            $get('MA_CHUONG_2').disabled = blnEnable;
            $get('MA_KHOAN_2').disabled = blnEnable;
            $get('MA_MUC_2').disabled = blnEnable;
            $get('NOI_DUNG_2').disabled = blnEnable;
            $get('SOTIEN_2').disabled = blnEnable;
            $get('KY_THUE_2').disabled = blnEnable;

            $get('chkRemove3').disabled = blnEnable;
            $get('MA_CHUONG_3').disabled = blnEnable;
            $get('MA_KHOAN_3').disabled = blnEnable;
            $get('MA_MUC_3').disabled = blnEnable;
            $get('NOI_DUNG_3').disabled = blnEnable;
            $get('SOTIEN_3').disabled = blnEnable;
            $get('KY_THUE_3').disabled = blnEnable;

            $get('chkRemove4').disabled = blnEnable;
            $get('MA_CHUONG_4').disabled = blnEnable;
            $get('MA_KHOAN_4').disabled = blnEnable;
            $get('MA_MUC_4').disabled = blnEnable;
            $get('NOI_DUNG_4').disabled = blnEnable;
            $get('SOTIEN_4').disabled = blnEnable;
            $get('KY_THUE_4').disabled = blnEnable;

        }
        function jsCalTotal() {
            var dblTongTien = 0;
            var dblPhi = 0;
            var dblVAT = 0;
            var dblTongTrichNo = 0;

            if (!document.getElementById("chkRemove1").checked) {
                //$get('SOTIEN_1').value="";
            }
            if (!document.getElementById("chkRemove2").checked) {
                //$get('SOTIEN_2').value="";
            }
            if (!document.getElementById("chkRemove3").checked) {
                //$get('SOTIEN_3').value="";
            }
            if (!document.getElementById("chkRemove4").checked) {
                //$get('SOTIEN_4').value="";
            }

            if ($get('SOTIEN_1').value.length > 0) {
                if (document.getElementById("chkRemove1").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_1').value.replaceAll('.', ''));
                    jsFormatNumber('SOTIEN_1');
                    //$get('SOTIEN_1').value="";
                }
            }
            if ($get('SOTIEN_2').value.length > 0) {
                if (document.getElementById("chkRemove2").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_2').value.replaceAll('.', ''));
                    jsFormatNumber('SOTIEN_2');
                    //$get('SOTIEN_2').value="";
                }
            }

            if ($get('SOTIEN_3').value.length > 0) {
                if (document.getElementById("chkRemove3").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_3').value.replaceAll('.', ''));
                    jsFormatNumber('SOTIEN_3');
                    //$get('SOTIEN_3').value="";
                }
            }

            if ($get('SOTIEN_4').value.length > 0) {
                if (document.getElementById("chkRemove4").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_4').value.replaceAll('.', ''));
                    jsFormatNumber('SOTIEN_4');
                    //$get('SOTIEN_4').value="";
                }
            }
            if (document.getElementById("ddlMaHTTT").value == "01" || document.getElementById("ddlMaHTTT").value == "05") {
                //$get('txtVAT').value = dblVAT.toString().replaceAll('.','');
                dblVAT = $get('txtVAT').value.replaceAll('.', '');
                dblPhi = $get('txtCharge').value.replaceAll('.', '');
                $get('txtTongTien').value = dblTongTien;
                //dblPhi=dblPhi.toString().replaceAll('.','')

                dblTongTrichNo = parseFloat(dblPhi) + parseFloat(dblVAT) + parseFloat(dblTongTien);
                // $get('txtTongTrichNo').value = dblTongTrichNo.toString().replaceAll('.','');

                //localeNumberFormat(dblPhi,'.');
                //$get('txtCharge').value = dblPhi;
                jsFormatNumber('txtTongTien');
                jsFormatNumber('txtCharge');
                jsFormatNumber('txtVAT');
                jsFormatNumber('txtTongTrichNo');
            }
            else {

                $get('txtCharge').value = 0;
                $get('txtVAT').value = 0;
                $get('txtTongTien').value = dblTongTien;
                dblTongTrichNo = dblTongTien;
                //$get('txtTongTrichNo').value = dblTongTrichNo.toString().replaceAll('.','');
                jsFormatNumber('txtTongTien');
                jsFormatNumber('txtCharge');
                jsFormatNumber('txtVAT');
                jsFormatNumber('txtTongTrichNo');

            }

            if ($("#txtTongTien").val().replaceAll('.', '') != _TTIEN) {
                _TTIEN = $("#txtTongTien").val().replaceAll('.', '');
                ResetTinhPhi();
            }

        }
        function jsTinhPhi() {
            $get('txtTongTrichNo').value = 0;
            if (document.getElementById("ddlMaHTTT").value == "03") {
                $get('txtCharge').value = 0;
                $get('txtVAT').value = 0;
                LoaiCK = "G";
                return;
            }
            var strAmount = $get('txtTongTien').value.replaceAll('.', '');
            if (strAmount == "0") {
                alert("Không tính phí với số tiền bằng 0");
                return;
            }
            var strAcountNo = document.getElementById('txtTK_KH_NH').value;
            if (strAcountNo == "" || document.getElementById('txtTenTK_KH_NH').value == "") {
                alert("Chưa có tài khoản trích nợ");
                return;
            }
            var pv_strFeeType = "";
            if (document.getElementById('chkChargeTypeMP').checked) {
                pv_strFeeType = document.getElementById('chkChargeTypeMP').value;
            } else if (document.getElementById('chkChargeTypePT').checked) {
                pv_strFeeType = document.getElementById('chkChargeTypePT').value;
            } else pv_strFeeType = document.getElementById('chkChargeTypePN').value;

            var LoaiCK = "A";

            var pv_strLoaiTien = $get('cboNguyenTe').value;
            var pv_strProvine = $get('txtTinh_NNTien').value;
            PageMethods.GetFee(strAcountNo, LoaiCK, strAmount, pv_strLoaiTien, pv_strFeeType, pv_strProvine, TinhPhi_Complete, TinhPhi_Error);
        }
        function TinhPhi_Complete(result, methodName) {
            checkSS(result);
            if (result.length > 0) {
                var xmlDoc = loadXMLString(result);
                var rootCTU_HDR = xmlDoc.getElementsByTagName('PARAMETER')[0];
                if (rootCTU_HDR != null) {
                    if (xmlDoc.getElementsByTagName("RETCODE")[0].childNodes[0].nodeValue == "0") {
                        if (xmlDoc.getElementsByTagName("FEE")[0].childNodes != null) {
                            $get('txtCharge').value = xmlDoc.getElementsByTagName("FEE")[0].childNodes[0].nodeValue;
                        } else $get('txtCharge').value = 0;
                        if (xmlDoc.getElementsByTagName("VAT")[0].childNodes != null) $get('txtVAT').value = xmlDoc.getElementsByTagName("VAT")[0].childNodes[0].nodeValue
                        else $get('txtVAT').value = 0;
                        if (xmlDoc.getElementsByTagName("PRODUCT")[0].childNodes != null) $get('txtSanPham').value = xmlDoc.getElementsByTagName("PRODUCT")[0].childNodes[0].nodeValue;
                        else $get('txtSanPham').value = "";
                        if (xmlDoc.getElementsByTagName("TOTALAMOUNT")[0].childNodes != null) $get('txtTongTrichNo').value = xmlDoc.getElementsByTagName("TOTALAMOUNT")[0].childNodes[0].nodeValue;
                        else $get('txtTongTrichNo').value = 0;
                        jsFormatNumber('txtCharge');
                        jsFormatNumber('txtVAT');
                        jsFormatNumber('txtTongTrichNo');
                    } else
                        document.getElementById('divStatus').innerHTML = rootCTU_HDR.getElementsByTagName("ERRCODE")[0].firstChild.nodeValue;
                }
            } else {
                document.getElementById('divStatus').innerHTML = 'Có lỗi trong quá trình lấy phí của khách hàng.';
            }
        }
        function TinhPhi_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình tính phí" + error.get_message();
            }
        }

        function jsCheckFormatKyThue(strKyThue) {
            //console.log(strKyThue);

            strKyThue = strKyThue.trim().toUpperCase();
            if (strKyThue.length < 10 || strKyThue.length > 10) {
                alert("Định dạng trường kỳ thuế không hợp lệ!");
                return false;
            }
            if (Validate_Format(strKyThue) == true) {
                if (validateTime(strKyThue) == true) {

                } else {
                    var arrKTthue = strKyThue.split("/");
                    var formatNQ = arrKTthue[0].toString() + "/" + arrKTthue[1].toString();
                    var formatNam = arrKTthue[2].toString();

                    var A = ['00/01', '00/02', '00/03', '00/04', '00/05', '00/06', '00/07', '00/08', '00/09', '00/10', '00/11', '00/12', '00/Q1', '00/Q2', '00/Q3', '00/Q4', '00/K1', '00/K2', '00/K3', '00/K4', '00/K5', '00/K6', '00/K7', '00/K8', '00/K9', '00/CN', '00/QT'];
                    var i;
                    var checkmatch = 0;
                    for (i = 0; i < A.length; i++) {
                        if (formatNQ == A[i].toString()) {
                            checkmatch += 1;
                        }
                    }
                    // console.log(checkmatch);
                    if (checkmatch > 0) {
                        if (typeof formatNam === 'number') {
                            // yes it is numeric

                            if (formatNam % 1 === 0) {
                                // yes it's an integer.
                            }
                            else {
                                alert("Định dạng trường kỳ thuế không hợp lệ!");
                                return false;
                            }
                        }
                    } else {

                        alert("Định dạng trường kỳ thuế không hợp lệ!");
                        return false;
                    }

                }

            } else {

                alert("Định dạng trường kỳ thuế không hợp lệ!");
                return false;
            }

            return true;

        }
        function validateTime(strKyThue) {
            var pattern = /^((0[1-9]|[12][0-9]|3[01])(\/)(0[13578]|1[02]))|((0[1-9]|[12][0-9])(\/)(02))|((0[1-9]|[12][0-9]|3[0])(\/)(0[469]|11))(\/)\d{4}$/;
            if (pattern.test(strKyThue)) {
                return true;
            }
            else {
                return false;
            }
        }
        function Validate_Format(strKyThue) {
            var countSlashes = strKyThue.split("/").length - 1;
            if (parseInt(countSlashes) == 2) {
                var arrKT = strKyThue.split("/");
                if (arrKT[0].toString().length == 2) {
                    return true;
                } else {
                    return false;
                }
                if (arrKT[1].toString().length == 2) {
                    return true;
                } else {
                    return false;
                }
                if (arrKT[2].toString().length == 4) {
                    return true;
                } else {
                    return false;
                }

                //return true;
            } else {
                return false;
            }

        }

        function jsEnabledInputCharge() {
            var dblTongTien = parseFloat($get('txtTongTien').value.replaceAll('.', ''));
            var dblPhi = 0;
            var dblVAT = 0;
            var dblTongTrichNo = 0;
            if (document.getElementById('chkChargeTypeMP').checked) {
                // $get('txtCharge').disabled  = true;
                //neu ma checked thi tinh lai fee
                dblPhi = 0;
                dblVAT = 0;

                //            $get('txtCharge').value = dblPhi;                                        
                //            $get('txtVAT').value = dblVAT.toString().replace('.',',');                            
                //            $get('txtTongTien').value = dblTongTien;                    
                //            dblTongTrichNo = dblPhi + dblVAT + dblTongTien;            
                //            $get('txtTongTrichNo').value = dblTongTrichNo.toString().replace('.',',');
                //            jsFormatNumber('txtVAT');  
                //            jsFormatNumber('txtTongTien');  
                //            jsFormatNumber('txtCharge');
                //            jsFormatNumber('txtTongTrichNo');                          
            } else {
                //            var strMaDBHC = document.getElementById ('txtMaDBHC').value;                              
                //            var strMaCQThu = document.getElementById ('txtMaCQThu').value;    
                //            PageMethods.GetFee(strLoaiDM,strSHKB,LoadDM_Complete,LoadDM_Error); 
                //            $get('txtCharge').disabled  = false;
                //            $get('txtCharge').value = "0";                                        
                //            $get('txtVAT').value = "0";
                //            $get('txtTongTrichNo').value = $get('txtTongTien').value;
            }
        }
        //    function jsCalCharge()
        //    {
        //    
        ////        var dblPhi=0;
        ////        var dblTongTien = parseFloat ($get('txtTongTien').value.replaceAll('.',''));  
        ////        if (document.getElementById('chkChargeType').checked)
        ////        {
        ////            dblPhi = dblTongTien * 0.02;            
        ////        }else
        ////        {
        ////            dblPhi = parseFloat ($get('txtCharge').value.replaceAll('.',''));           
        ////        }
        ////        $get('txtCharge').value = dblPhi;
        ////        jsFormatNumber('txtCharge');
        ////                
        ////        //dblPhi += parseFloat ($get('txtCharge').value.replaceAll('.','')); 
        ////        var dblVAT = dblPhi/10;
        ////                    
        ////        $get('txtVAT').value = dblVAT;
        ////        jsFormatNumber('txtVAT');  
        ////                
        ////        $get('txtTongTien').value = dblTongTien;
        ////        jsFormatNumber('txtTongTien');  
        ////        
        ////        var dblTongTrichNo = dblPhi + dblVAT + dblTongTien;
        ////        $get('txtTongTrichNo').value = dblTongTrichNo;
        ////        jsFormatNumber('txtTongTrichNo'); 
        //       if( $get('txtCharge').value=="") $get('txtCharge').value="0";
        //       var dblPhi = 0;             
        //       var dblVAT = 0;
        //       $get('txtVAT').value = dblVAT.toString().replace('.',',');               
        //       var dblTongTien = parseFloat ($get('txtTongTien').value.replaceAll('.',''));          
        //       var dblTongTrichNo = dblPhi + dblVAT + dblTongTien;
        //      
        //       $get('txtTongTrichNo').value = dblTongTrichNo.toString().replace('.',',');;
        //       jsFormatNumber('txtVAT'); 
        //       jsFormatNumber('txtTongTrichNo');  
        //       jsFormatNumber('txtCharge');
        //    }

        function jsSHKB_lostFocus(strSHKB) {
            // jsLoadDM('SHKB',strSHKB);
            jsLoadDM('KHCT', strSHKB);
            jsLoadDM('DBHC', strSHKB);
            jsLoadDM('CQTHU', strSHKB);
            jsLoadDM('CQTHU_DEF', strSHKB);
            jsLoadDM('DBHC_DEF', strSHKB);
            jsLoadDM('TAIKHOANNO', strSHKB);
            jsLoadDM('TAIKHOANCO', strSHKB);
            jsLoadDM('CQTHU_SHKB', strSHKB);

            jsLoadDM('TINH_SHKB', strSHKB);
            jsGetTTNganHangNhan();
        }

        //sonmt
        function jsMACQT_lostFocus(strMACQT) {
            if (strMACQT.length > 0) {
                PageMethods.GetSHKB(strMACQT, jsGetSHKB_Complete, jsGetSHKB_Error);
            }
        }
        function jsGetSHKB_Complete(result) {
            if (result.length == 0) {
                document.getElementById("txtSHKB").value = "";
            }
            if (result.length > 0) {
                document.getElementById("txtSHKB").value = result;
            }

            jsSHKB_lostFocus(document.getElementById('txtSHKB').value);
            jsGet_TenKB();
            jsGetTTNganHangNhan();
        }
        function jsGetSHKB_Error(result) {
            document.getElementById("txtSHKB").value = "";
            jsSHKB_lostFocus(document.getElementById('txtSHKB').value);
            jsGet_TenKB();
            //do nothing
        }

        function jsLoadDM(strLoaiDM, strSHKB) {
            //alert(strSHKB);
            if (strSHKB.length == 0) {
                strSHKB = document.getElementById('txtSHKB').value;
            }
            if (strSHKB.length > 0) {
                PageMethods.GetData_SHKB(strLoaiDM, strSHKB, LoadDM_Complete, LoadDM_Error);
            }
            else {
                jsClearGridHeader(true, true);//khong xoa gt mac dinh                                 
                jsClearGridDetail();
                jsSetDefaultValues();
            }
        }
        function LoadDM_Complete(result, userContext, methodName) {
            checkSS(result);
            var mangKQ = new Array;
            mangKQ = result.toString().split('|');
            $get('txtGhiChu').value = $get('txtTenKB').value;
            if (mangKQ[0] == 'TAIKHOANNO') {
                arrTKNoNSNN = new Array(mangKQ.length - 2);
                for (i = 1; i < mangKQ.length - 1; i++) {
                    var strTemp = mangKQ[i].toString();
                    arrTKNoNSNN[i - 1] = strTemp;
                }
                // jsFillTKNo();
            }
            else if (mangKQ[0] == 'TAIKHOANCO') {
                arrTKCoNSNN = new Array(mangKQ.length - 2);
                for (i = 1; i < mangKQ.length - 1; i++) {
                    var strTemp = mangKQ[i].toString();
                    arrTKCoNSNN[i - 1] = strTemp;
                }
                //jsFillTKCo();
            }
            else if (mangKQ[0] == 'CQTHU') {
                arrCQThu = new Array(mangKQ.length - 2);
                for (i = 1; i < mangKQ.length - 1; i++) {
                    var strTemp = mangKQ[i].toString();
                    arrCQThu[i - 1] = strTemp;
                }
            }
                //       else if (mangKQ[0]=='CQTHU_SHKB')
                //       {
                //           arrCQThu_SHKB = new Array(mangKQ.length-2);
                //           for(i=1;i<mangKQ.length-1;i++){
                //                var strTemp = mangKQ[i].toString();
                //                arrCQThu_SHKB[i-1] = strTemp;
                //                }
                //       }
            else if (mangKQ[0] == 'KHCT') {

                if (mangKQ[1].length != 0) {

                    document.getElementById('txtKyHieuCT').value = mangKQ[1].split(';')[0];
                }
                else {
                    document.getElementById('txtKyHieuCT').value = '';
                }
            }
            else if (mangKQ[0] == 'DBHC_DEF') {
                if (mangKQ[1].length != 0) {
                    //document.getElementById('txtMaDBHC').value = mangKQ[1].split(';')[0];
                    //document.getElementById('txtTenDBHC').value = mangKQ[1].split(';')[1];
                }
                else {
                    document.getElementById('txtMaDBHC').value = '';
                    document.getElementById('txtTenDBHC').value = '';
                }
                //console.log("1.DBHC_DEF.MADBHC: " + document.getElementById('txtMaDBHC').value);
            }
            else if (mangKQ[0] == 'TINH_SHKB') {
                if (mangKQ[1].length != 0) {
                    document.getElementById('txtTinh_NNTien').value = mangKQ[1].split(';')[0];
                    document.getElementById('txtQuan_HuyenNNTien').value = mangKQ[1].split(';')[1];
                }
                else {
                    document.getElementById('txtTinh_NNTien').value = '';
                    document.getElementById('txtQuan_HuyenNNTien').value = '';
                }
                //console.log("1.TINH_SHKB.MA_TINH: " + document.getElementById('txtTinh_NNTien').value);
            }
            else if (mangKQ[0] == 'CQTHU_SHKB') {
                if (mangKQ[1].length != 0) {
                    //document.getElementById('txtMaCQThu').value = mangKQ[1].split(';')[1];
                   // document.getElementById('txtTenCQThu').value = mangKQ[1].split(';')[2];
                }
                else {
                    document.getElementById('txtMaCQThu').value = '';
                    document.getElementById('txtTenCQThu').value = '';
                    document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin Cơ quan thu với SHKB này.';
                }

            }

        }
        function LoadDM_Error(error, userContext, methochedName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = 'Không lấy được dữ liệu liên quan đến kho bạc này';
                //phuc hoi nguyen trang thao cac nut truoc thuc hien
            }
        }
        //     function jsGet_TKCo()
        //    {        
        //        var strMaDBHC = document.getElementById ('txtMaDBHC').value;                              
        //        var strMaCQThu = document.getElementById ('txtMaCQThu').value;                      
        //        //Kiem tra xem neu chua co trong arrTKNo thi moi querry ve db                        

        //        if (strMaCQThu.length ==0) return;
        //        if (strMaDBHC.length ==0) return;
        //        if (arrTKCoNSNN==null) return;
        //        for (i=0;i<arrTKCoNSNN.length;i++)
        //        {            
        //            var arr = arrTKCoNSNN[i].split(';');            
        //            if (strMaDBHC + strMaCQThu == arr[0] + arr[1])
        //            {                                
        //                document.getElementById('ddlMaTKCo').value = arr[2];
        //                document.getElementById('txtTKCo').value = arr[3];
        //                break;
        //            }
        //        } 
        //        //PageMethods.Get_TKCo(txtMaDBHC,strMaCQThu,Get_TKCo_Complete,Get_TKCo_Error);                       
        //    }
        function jsGetTen_TKCo() {
            var txtTKCoValue = document.getElementById('ddlMaTKCo').value.replaceAll('-', '');
            if (arrTKCoNSNN == null) return;
            for (i = 0; i < arrTKCoNSNN.length; i++) {
                var arr = arrTKCoNSNN[i].split(';');
                if (txtTKCoValue == arr[2]) {
                    document.getElementById('txtTKCo').value = arr[3];
                    break;
                }
            }
            //PageMethods.Get_TKCo(txtMaDBHC,strMaCQThu,Get_TKCo_Complete,Get_TKCo_Error);                       
        }
        function jsGetTen_TKNo() {
            var txtTKNoValue = document.getElementById('ddlMaTKNo').value.replaceAll('-', '');
            if (arrTKNoNSNN == null) return;
            for (i = 0; i < arrTKNoNSNN.length; i++) {
                var arr = arrTKNoNSNN[i].split(';');
                if (txtTKNoValue == arr[1]) {
                    document.getElementById('txtTKNo').value = arr[2];
                    break;
                }
            }
            //PageMethods.Get_TKCo(txtMaDBHC,strMaCQThu,Get_TKCo_Complete,Get_TKCo_Error);                       
        }
        function jsGet_TKNo() {
            var strSHKB = document.getElementById('txtSHKB').value;
            var strMaDBHC = document.getElementById('txtMaDBHC').value;
            //Kiem tra xem neu chua co trong arrTKNo thi moi querry ve db  
            if (arrTKNoNSNN == null) return;
            for (i = 0; i < arrTKNoNSNN.length; i++) {
                var arr = arrTKNoNSNN[i].split(';');
                if (strSHKB == arr[0]) {
                    document.getElementById('ddlMaTKNo').value = arr[1];
                    document.getElementById('txtTKNo').value = arr[2];
                    break;
                }
            }
        }
        //ManhNV sua lay NH truc tiep - gian tiep 01/10/2012
        function jsGetTTNganHangNhan() {
            //07/06/2011: Kienvt lay thong tin ngan hang nhan
            var strSHKB = $get('txtSHKB').value;
            var strDBHC = '';//$get('txtMaDBHC').value;
            var strCQThu = '';//$get('txtMaCQThu').value;   
            //alert(curCtuStatus)
            if (curCtuStatus == "99" || curCtuStatus == "03") {
                PageMethods.GetTT_NH_NHAN(strSHKB, GetTT_NganHangNhan_Complete, GetTT_NganHangNhan_Error);
            }
        }
        //Kienvt: them phan get thong tin ngan hang nhan
        function GetTT_NganHangNhan_Complete(result, methodName) {
            checkSS(result);
            //alert(result.toString());
            if (result.length > 0) {
                var arr2 = result.split(';');
                //alert(arr2[0]);
                $get('txtMA_NH_B').value = arr2[0];
                $get('txtTenMA_NH_B').value = arr2[1];
                $get('txtMA_NH_TT').value = arr2[2];
                $get('txtTEN_NH_TT').value = arr2[3];
                jsValidNHSHB_SP($get('txtSHKB').value);
                document.getElementById('divStatus').innerHTML = '';
            }
            else {
                $get('txtMA_NH_B').value = '';
                $get('txtTenMA_NH_B').value = '';
                $get('txtMA_NH_TT').value = '';
                $get('txtTEN_NH_TT').value = '';
                //07/06/2011 : Kienvt focus vao truong tai khoan khi so nhap bi sai
                //$get('txtTK_KH_NH').value='';
                //$get('txtTK_KH_NH').focus();
                document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin ngân hàng nhận.Hãy kiểm tra lại';
            }
        }
        //END-ManhNV sua lay NH truc tiep - gian tiep 01/10/2012 
        //Kienvt: them phan get thong tin ngan hang nhan
        function GetTT_NganHangNhan_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin ngân hàng nhận" + error.get_message();
            }
        }
        function jsCallSo_DaThu(txtMaChuong, txtMa_TMuc, txtKyThue, txtSo_DThu) {
            var strMaChuong = document.getElementById(txtMaChuong).value;
            var strMaTMuc = document.getElementById(txtMa_TMuc).value;
            var strKyThue = document.getElementById(txtKyThue).value;
            var strMa_NNT = document.getElementById('txtMaNNT').value;
            if (strMa_NNT.trim == "" || strKyThue == "" || strMaChuong == "" || strMaTMuc == "") { return; }
            PageMethods.CallSo_DT(strMa_NNT, "", strMaChuong, "", strMaTMuc, strKyThue, txtSo_DThu, CallSo_DT_Complete, CallSo_DT_Error);
        }
        function jsGet_TenNH_B() {
            if (arrTT_NH_B.length > 0) {
                $get('txtTenMA_NH_B').value = '';
                for (var i = 0; i < arrTT_NH_B.length; i++) {
                    var arr = arrTT_NH_B[i].split(';');
                    if (arr[0] == $get('txtMA_NH_B').value.trim() && arr[4] == $get('txtSHKB').value.trim()) {
                        $get('txtTenMA_NH_B').value = arr[1];
                        $get('txtMA_NH_TT').value = arr[2];
                        $get('txtTEN_NH_TT').value = arr[3];
                        break;
                    }
                }
            }
            if ($get('txtTenMA_NH_B').value == '') {
                alert('Không tìm thấy thông tin NH thụ hưởng');
            }
        }
        function jsGet_TenNH_TT(MA_NH_B) {
            if (arrTT_NH_TT.length > 0) {
                $get('txtTEN_NH_TT').value = '';
                for (var i = 0; i < arrTT_NH_TT.length; i++) {
                    var arr = arrTT_NH_TT[i].split(';');
                    if (arr[2] == MA_NH_B.value.trim()) {
                        $get('txtMA_NH_TT').value = arr[0];
                        $get('txtTEN_NH_TT').value = arr[1];
                        break;
                    }
                }
            }
            if ($get('txtMA_NH_TT').value == '') {
                alert('Không tìm thấy thông tin NH thanh toán');
            }
        }
        function CallSo_DT_Complete(result, userContext, methodName) {
            checkSS(result);
            if (result.length > 0) {
                var arrresult = result.split(';');
                document.getElementById(arrresult[0]).value = arrresult[1];
            }
        }
        function CallSo_DT_Error(error, userContext, methodName) {

        }
        function jsCheck_Exit_NNT() {
            var strMa_NNT = document.getElementById('txtMaNNT').value;
            var strNgay_LV = curNgay_LV;
            if (strMa_NNT.trim == "") { return true; }

            PageMethods.CTU_Check_Exist_NNT(strMa_NNT, strNgay_LV, check_NNT_Exits_Complete, check_NNT_Exits_Error);
        }

        function check_NNT_Exits_Complete(result, methodName) {
            checkSS(result);
            //alert(result)   ;              
            if (result.toString.length > 0) {
                document.getElementById('hdnSoLan_LapCT').value = result;
            }
        }
        function check_NNT_Exits_Error(error, userContext, methodName) {
            document.getElementById('hdnSoLan_LapCT').value = '0';
        }

        $(document).ready(function () {
            $("#txtSHKB").blur(function () {
                if ($("#txtSHKB").val() != _SHKB) {
                    _SHKB = $("#txtSHKB").val();
                    ResetTinhPhi();
                }
            });
            $("#txtTK_KH_NH").blur(function () {
                if ($("#txtTK_KH_NH").val() != _TKNH) {
                    _TKNH = $("#txtTK_KH_NH").val();
                    ResetTinhPhi();
                }
            });

            try {
                $('#grdDSCT').paging({ limit: 15 });
            } catch (err) {
                paginationList(15);
            }

        });

        function paginationList(listPaginationCount) {
            $('table#grdDSCT').each(function () {
                var currentPage = 0;
                var numPerPage = listPaginationCount;
                var $pager = $('.pager').remove();
                var $paging = $("#paging").remove();
                var $table = $(this);
                $table.bind('repaginate', function () {
                    $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager = $('<div id="pager" style="text-align: center;"></div>');

                for (var page = 0; page < numPages; page++) {
                    $('<span class="page-number"></span>').text(page + 1).bind('click', {
                        newPage: page
                    }, function (event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');

                }

                $pager.insertAfter($table).find('span.page-number:first').addClass('active');
            });
        }

        function ResetTinhPhi() {
            $("#txtCharge").val("0");
            $("#txtVAT").val("0");
            $("#txtSanPham").val("");
            $get('txtTongTrichNo').value = 0;
            document.getElementById('cmdTinhPhi').disabled = false;
        }

        function jsValidMin() {
            var TKCo = document.getElementById('ddlMaTKCo').value;
            if (TKCo.length < 4) {

                alert("TK Có NSNN cần tối thiểu 4 ký tự");
            }
        };

        // valid length

        function jsValidLength(id, value) {
            if (id == 'txtMaLoaiNNT' && value.length < 4) {
                alert("Mã/Tên Loại NNT cần tối thiểu 4 ký tự");
                document.getElementById("txtMaLoaiNNT").value = "";
                document.getElementById("txtMaLoaiNNT").focus();
            }
            else if (id == 'txtHuyen_NNT' || id == 'txtTinh_NNT') {
                if (value.length < 5) {
                    alert("Mã Quận(Huyện)/Tỉnh(Thành phố) cần tối thiểu 5 ký tự");
                    document.getElementById("txtHuyen_NNT").value = "";
                    document.getElementById("txtTinh_NNT").value = "";
                    document.getElementById("txtHuyen_NNT").focus();
                }
            }
            else if (id == 'txtSHKB' && value.length < 4) {
                alert("Số hiệu KB cần tối thiểu 4 ký tự");
                document.getElementById("txtSHKB").value = "";
                document.getElementById("txtSHKB").focus();
            }
            else if (id == 'txtMaCQThu' && value.length < 7) {
                alert("Mã CQ Thu cần tối thiểu 7 ký tự");
                document.getElementById("txtMaCQThu").value = "";
                document.getElementById("txtMaCQThu").focus();
            }
            else if (id == 'ddlMaTKCo' && value.length < 4) {
                alert("Mã TK Có NSNN cần tối thiểu 4 ký tự");
                document.getElementById("ddlMaTKCo").value = "";
                document.getElementById("ddlMaTKCo").focus();
            }
            else if (id == 'txtMaDBHC' && value.length < 5) {
                alert("Số ĐBHC cần tối thiểu 5 ký tự");
                document.getElementById("txtMaDBHC").value = "";
                document.getElementById("txtMaDBHC").focus();
            }
            else if (id == 'txtMA_NH_TT' && value.length < 8) {
                alert("Mã NH trực tiếp cần tối thiểu 8 ký tự");
                document.getElementById("txtMA_NH_TT").value = "";
                document.getElementById("txtMA_NH_TT").focus();
            }
            else if (id == 'txtMA_NH_B' && value.length < 8) {
                alert("Mã NH gián tiếp cần tối thiểu 8 ký tự");
                document.getElementById("txtMA_NH_B").value = "";
                document.getElementById("txtMA_NH_TT").focus();
            }
        };
        //

    </script>

    <style type="text/css">
        .style1 {
            height: 24px;
        }

        .style2 {
            width: 30%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">LẬP CHỨNG TỪ THUẾ</asp:Label>
            </td>
        </tr>
        <tr valign="top">
            <td align="center" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr height="12px">
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td width='100%' align="center" valign="top" style="padding-left: 3; padding-right: 3">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width='900' align="left" valign="top">
                                        <table width="100%" align="left">
                                            <tr>
                                                <td valign="top" width="190px" height="480px">
                                                    <asp:Panel ID="pnlDSCT" runat="server" Height="450px" Width="98%" Style="overflow: auto;">
                                                        <table class="grid_data" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr class='grid_header'>
                                                                <td style="width: 20%">
                                                                    TT
                                                                </td>
                                                                <td class="style2">
                                                                    Số CT
                                                                </td>
                                                                <td style="width: 50%">
                                                                    Tên ĐN/Số BT
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <div id='divDSCT' />
                                                                </td>
                                                            </tr>
                                                            <%-- <tr class="grid_data">
                                                                <td style="width: 50%" colspan="2">
                                                                    <b>Tổng Số CT:</b>
                                                                </td>
                                                                <td style="width: 50%">
                                                                </td>
                                                            </tr>
                                                            <tr class="img">
                                                                <td style="width: 100%" colspan="3">
                                                                </td>
                                                            </tr>--%>
                                                        </table>
                                                    </asp:Panel>
                                                    <table>
                                                                            <tr style="display:none">
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/ChuaKS.png" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    Chưa Kiểm soát
                                                                                </td>
                                                                            </tr>
                                                                            
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/ChuyenKS.png" style="width: 16px" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    Chờ Kiểm soát
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/DaKS.png" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    Đã Kiểm soát
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/KSLoi.png" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    Chuyển trả
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/Huy.png" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    CT bị Hủy bởi GDV
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/HuyHS.gif" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    CT Hủy bởi GDV - chờ duyệt
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/Huy_CT_Loi.png" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    Duyệt hủy chuyển thuế lỗi
                                                                                </td>
                                                                            </tr>
                                                                             <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/Huy_KS.png" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    CT bị Hủy bởi KSV
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/TT_CThueLoi.gif" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    Chuyển thuế/HQ lỗi
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="display:none">
                                                                                <td style="width: 10%">
                                                                                    <img src="../../images/icons/Huy_CT_Loi.png" />
                                                                                </td>
                                                                                <td style="width: 90%" colspan="2">
                                                                                    Hủy chuyển thuế/HQ lỗi
                                                                                </td>
                                                                            </tr>
                                                        <tr class="img">
                                                            <td align="left" style="width: 100%" colspan="3">
                                                                <hr style="width: 50%" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    Alt + M : Thêm mới<br />
                                                    <%--Alt + G : Ghi<br />--%>Alt + K : Ghi và chuyển KS<br />
                                                   <%-- Alt + C : Chuyển KS<br />--%>Alt + I&nbsp;&nbsp; : In chứng từ<br />
                                                    Alt + H : Hủy chứng từ<br />
                                                </td>
                                                <td valign="top" style="height:auto; width:670px" >
                                                    <table border='0' cellpadding='0' cellspacing='0' width='700px'>
                                                        <tr>
                                                            <td colspan="2" width='100%' align="left">
                                                                <table width="100%">
                                                                    <tr class="grid_header">
                                                                        <td align="left">
                                                                            <div style="display: none" id="divTTBDS">
                                                                            </div>
                                                                        </td>
                                                                        <td height='15' align="right">
                                                                            Trường <span style="color: #FF0000; font-weight: bold">(*)</span> là bắt buộc nhập
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <div id="Panel1" style="height: 370px; width: 100%; overflow-y: scroll;">
                                                                    <table id="grdHeader" cellspacing="0" cellpadding="1" rules="all" border="1" style="border-width: 1px;
                                                                        border-style: solid; font-family: Verdana; font-size: 8pt; width: 97%; border-collapse: collapse;">
                                                                        <tr id="rowMaNNT">
                                                                            <td width="24%">
                                                                                <b>Mã số thuế/Tên </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td width="25%">
                                                                                <input type="text" id="txtMaNNT" class="inputflat" maxlength="14" style="width: 99%; "
                                                                                    onkeydown="if (event.keyCode==13){checkMaNNT(this);jsGet_NNThue();}" onblur="checkMaNNT(this);jsGet_NNThue();" />
                                                                            </td>
                                                                            <td width="51%">
                                                                                <input type="text" id="txtTenNNT" class="inputflat" style="width: 92%;" maxlength="68"
                                                                                    disabled />
                                                                                <img id="imgMaDB" alt="Chọn từ danh mục đặc biệt" src="../../images/icons/MaDB.png"
                                                                                    onclick="jsGetMaDB();" style="width: 16px; height: 16px" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowLoaiNNT">
                                                                            <td>
                                                                                <b>Mã/Tên Loại NNT</b>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtMaLoaiNNT" maxlength="4" class="inputflat" style="width: 98%; background: Aqua" onkeypress="if (event.keyCode==13){ShowLov('LOAI_NNTHUE');}" 
                                                                                    onchange="javascript:jsValidLength(this.id,this.value);jsGetLoaiNNT();" onblur="javascript:jsGetLoaiNNT();" />
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtTenLoaiNNT" class="inputflat" style="width: 98%;" readonly="readonly"/>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowDChiNNT">
                                                                            <td>
                                                                                <b>Địa chỉ người nộp thuế</b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <input type="text" id="txtDChiNNT" class="inputflat" maxlength="200" style="width: 99%;" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowHuyenNNT">
                                                                            <td with="24%">
                                                                                <b>Huyện người nộp thuế</b>
                                                                            </td>
                                                                            <td with="25%">
                                                                                <input type="text" id="txtHuyen_NNT" maxlength="5" class="inputflat" style="width: 99%;" onchange="javascript:jsValidLength(this.id,this.value);jsGetTenQuanHuyenNNT(this.value);" onblur="javascript:jsGetTenQuanHuyenNNT(this.value);" />
                                                                            </td>
                                                                            <td with="51%">
                                                                                <input type="text" id="txtTenHuyen_NNT" class="inputflat" maxlength="200" style="width: 99%;"  />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTinhNNT">
                                                                            <td with="24%">
                                                                                <b>Tỉnh người nộp thuế</b>
                                                                            </td>
                                                                            <td with="25%">
                                                                                <input type="text" id="txtTinh_NNT" class="inputflat" maxlength="5" style="width: 99%;" onchange="javascript:jsValidLength(this.id,this.value);" />
                                                                            </td>
                                                                             <td with="51%">
                                                                                <input type="text" id="txtTenTinh_NNT" class="inputflat" maxlength="200" style="width: 99%;" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNNTien">
                                                                            <td>
                                                                                <b>MST/Tên người nộp thay</b>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtMaNNTien" maxlength="14" class="inputflat" style="width: 99%;" />
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtTenNNTien" maxlength="100" class="inputflat" style="width: 99%;" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowDCNNTIEN">
                                                                            <td>
                                                                                <b>Địa chỉ người nộp thay</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <input type="text" id="txtDC_NNTIEN" maxlength="200" class="inputflat" style="width: 99%;" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowHuyenNNTIEN">
                                                                            <td>
                                                                                <b>Huyện người nộp thay</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <input type="text" id="txtHuyen_NNTHAY" maxlength="100" class="inputflat" style="width: 99%;" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTinhNNTIEN">
                                                                            <td>
                                                                                <b>Tỉnh người nộp thay</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <input type="text" id="txtTinh_NNTHAY" maxlength="100" class="inputflat" style="width: 99%;" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowDChiNNTien">
                                                                            <td>
                                                                                <b>Địa chỉ người nộp tiền</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <input type="text" id="txtDChiNNTien" class="inputflat" style="width: 99%;" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowHuyen">
                                                                            <td>
                                                                                <b>Quận(Huyện)</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <input type="text" id="txtQuan" class="inputflat" style="width: 99%;" />
                                                                            </td>
                                                                        </tr>
                                                                       <tr id="rowCQThu">
                                                                            <td class="style1">
                                                                                <b>CQ Thu </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td class="style1">
                                                                                 <input type="text" id="txtMaCQThu" maxlength="7" style="width: 98%; background:#0072BC" onkeypress="if (event.keyCode==13){ShowLov('CQT');jsMACQT_lostFocus(this.value);}"
                                                                                  onchange="javascript:jsValidLength(this.id,this.value);jsGet_TenCQThu();jsMACQT_lostFocus(this.value);" class="inputflat" />
                                                                            </td>
                                                                            <td class="style1">
                                                                                <input type="text" id="txtTenCQThu" class="inputflat" style="width: 99%;" disabled />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSHKB">
                                                                            <td width="24%">
                                                                                <b>Số hiệu KB </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td width="25%">
                                                                                <input type="text" id="txtSHKB" class="inputflat" maxlength="4" style="width: 99%; background: Aqua"
                                                                                    onkeypress="if (event.keyCode==13){ShowLov('SHKB');}"
                                                                                    onchange="javascript:jsValidLength(this.id,this.value);jsGet_TenKB();jsSHKB_lostFocus(this.value);"  onblur="jsGetTTNganHangNhan();" />
                                                                                <%-- <script type="text/javascript">
                                                                                                new actb(document.getElementById('txtSHKB'), temp_arrSHKB, 'txtTenKB');					
                                                                                </script>--%>
                                                                            </td>
                                                                            <td width="51%">
                                                                                <input type="text" id="txtTenKB" class="inputflat" style="width: 99%;" disabled maxlength="60" />
                                                                            </td>
                                                                        </tr>
                                                                         <tr id="rowTinh">
                                                                            <td class="style1">
                                                                                <b>Tỉnh/Thành phố</b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td  class="style1">
                                                                                <input type="text" id="txtTinh_NNTien" class="inputflat" style="width: 99%;"disabled />
                                                                            </td>
                                                                            <td  class="style1">
                                                                                <input type="text" id="txtQuan_HuyenNNTien" class="inputflat" style="width: 99%;" disabled />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowKHCT">
                                                                            <td width="24%">
                                                                                <b>Mã hiệu CT </b>
                                                                            </td>
                                                                            <td width="25%">
                                                                                <input type="text" id="txtKyHieuCT" class="inputflat" style="width: 99%;" disabled
                                                                                    maxlength="60" />
                                                                            </td>
                                                                            <td width="51%">
                                                                                <input type="text" id="Text1" class="inputflat" style="width: 99%; display:none" disabled maxlength="60" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowDBHC">
                                                                            <td>
                                                                                <b>ĐBHC </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtMaDBHC" maxlength="5" class="inputflat" style="width: 99%; background: Aqua"
                                                                                  onchange="javascript:jsValidLength(this.id,this.value);jsGet_TenDBHC();"  onkeypress="if (event.keyCode==13){ShowLov('DBHC');}" />
                                                                                <%-- <script type="text/javascript">
                                                                                                new actb(document.getElementById('txtMaDBHC'), temp_arrDBHC, 'txtTenDBHC');					
                                                                                            </script>--%>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtTenDBHC" class="inputflat" style="width: 99%;" disabled />
                                                                            </td>
                                                                        </tr>
                                                                        
                                                                        <tr id="rowTKNo" style="display: none">
                                                                            <td style="background-color: Aqua">
                                                                                <b>TK Nợ NSNN </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="ddlMaTKNo" class="inputflat" style="width: 99%;" onblur="jsGetTen_TKNo();"
                                                                                    onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'3,6,9,16','-');}"
                                                                                    maxlength="18" />
                                                                            </td>
                                                                            <td class="style1" style="display: none">
                                                                                <input type="text" id="txtTKNo" class="inputflat" style="width: 99%; display: none"
                                                                                    visible="false" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTKCo">
                                                                            <td >
                                                                                <b>TK Có NSNN </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <select id="ddlMa_NTK" style="width: 100%" class="inputflat">
                                                                                    <option value="1" selected="true">Tài khoản thu NSNN 7111</option>
                                                                                    <option value="2">Tài khoản Thu hoàn thuế GTGT</option>
                                                                                    <%--<option value="02">Điện chuyển tiền</option>--%>
                                                                                    <%--<option value="03">Nộp qua GL</option>--%>
                                                                                </select>
                                                                            </td>
                                                                            <td style="">
                                                                                <input type="text" id="ddlMaTKCo" maxlength="4" class="inputflat" style="width: 99%;" onchange="javascript:jsValidLength(this.id, this.value);" />
                                                                            </td>
                                                                            <td visible="false">
                                                                                <input type="text" id="txtTKCo" class="inputflat" style="width: 10%; display: none;
                                                                                    height: 0px" visible="false" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowHTTT">
                                                                            <td>
                                                                                <b>Hình thức thanh toán</b>
                                                                            </td>
                                                                            <td>
                                                                                    <%--<option value="00" >Nộp tiền mặt</option>--%>
                                                                                    <%--<option value="01" selected="true">Chuyển khoản</option>--%>
                                                                                    <%--<option value="02">Điện chuyển tiền</option>--%>
                                                                                    <%--<option value="03">Nộp qua GL</option>--%>
                                                                                <%--</select>--%>
                                                                                <select id="ddlMaHTTT" style="width: 100%" class="inputflat" onchange="ShowHideControlByPT_TT();getSoTK_TruyVanCore();">
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtTenHTTT" class="inputflat" style="width: 99%; display:none" disabled="true"
                                                                                    readonly="true" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowMaNT">
                                                                            <td>
                                                                                <b>Nguyên tệ</b>
                                                                            </td>
                                                                            <td>
                                                                                <select id="cboNguyenTe" style="width: 100%" class="inputflat" onchange="showNT();">
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtTenNT" class="inputflat" style="width: 99%;" disabled="true"
                                                                                    readonly="true" value="VND" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTK_KH_NH">
                                                                            <td>
                                                                                <b>TK Chuyển </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtTK_KH_NH" class="inputflat" style="width: 99%;" onblur="jsGetTK_KH_NH();jsGetTenTK_KH_NH();"
                                                                                    maxlength="18" />
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtTenTK_KH_NH" class="inputflat" style="width: 99%;" disabled="true"
                                                                                    readonly="true" />
                                                                                    <img id="img1" alt="Chọn từ danh mục đặc biệt" src="../../images/icons/MaDB.png" style="display: none;" visible="false"
                                                                                    onclick="jsShowCustomerSignature();" style="width: 0px; height: 16px" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSoDuNH">
                                                                            <td>
                                                                                <b>Số dư NH</b>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtSoDu_KH_NH" class="inputflat" style="width: 99%;" disabled="true"
                                                                                    readonly="true" />
                                                                            </td>
                                                                            <td style="display:none ">
                                                                                <input type="text" id="txtDesc_SoDu_KH_NH" class="inputflat" style="width: 99%; visibility:visible"
                                                                                    disabled="true" readonly="true" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTKGL">
                                                                            <td>
                                                                                <b>TK chuyển </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtTKGL" maxlength="12" class="inputflat" style="width: 99%;"
                                                                                    onblur="jsGet_TenTKGL();" />
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtTenTKGL" class="inputflat" style="width: 99%;" disabled="true"
                                                                                    readonly="true" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNgay_KH_NH">
                                                                            <td>
                                                                                <b>Ngày chuyển</b>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtNGAY_KH_NH" class="inputflat" style="width: 99%;" maxlength="10"
                                                                                    onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                    onblur="javascript:CheckDate(this)" onfocus="this.select()" disabled="disabled" />
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtDesNGAY_KH_NH" class="inputflat" style="width: 99%; display:none" visible="false" disabled="true"
                                                                                    readonly="true" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNHChuyen">
                                                                            <td>
                                                                                <b>Mã NH Chuyển </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <select id="ddlMA_NH_A" style="width: 100%" class="inputflat">
                                                                                    <%--<option value="30319001" selected="true">30319001-NGAN HANG TMCP ĐẠI DƯƠNG</option>--%>
                                                                                </select>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNHTT">
                                                                            <td>
                                                                                <b>Mã NH TT </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtMA_NH_TT" class="inputflat" style="width: 99%;" maxlength="8" readonly="readonly"  onkeypress="if (event.keyCode==13){ShowLov('DMNH_TT');}" onkeyup="valInteger(this)"
                                                                                   onchange="javascript:jsValidLength(this.id, this.value);" />
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtTEN_NH_TT" class="inputflat" style="width: 99%;" disabled="true"
                                                                                    readonly="true" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNHNhan">
                                                                            <td>
                                                                                <b>Mã NH TH </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtMA_NH_B" class="inputflat" style="width: 99%;"  maxlength="8" onblur="jsGet_TenNH_B()"  onkeypress="if (event.keyCode==13){ShowLov('DMNH_GT');jsGet_TenNH_B();}" onkeyup="valInteger(this)"
                                                                                    onchange="javascript:jsValidLength(this.id, this.value);" />
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtTenMA_NH_B" class="inputflat" style="width: 99%;" disabled="true"
                                                                                    readonly="true" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowLoaiThue">
                                                                            <td>
                                                                                <b>Loại thuế</b>
                                                                            </td>
                                                                            <td>
                                                                                <select id="ddlMaLoaiThue" style="width: 100%" class="inputflat" onchange="ShowHideControlByLoaiThue();">
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtTenLoaiThue" class="inputflat" style="width: 99%; display:none" disabled="true"
                                                                                    readonly="true" value="Thuế công thương nghiệp" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowToKhaiSo">
                                                                            <td>
                                                                                <b>Tờ khai số </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtToKhaiSo" class="inputflat" style="width: 99%;" />
                                                                            </td>
                                                                            <td>
                                                                                <select id="ddlToKhaiSo" style="width: 99%" class="inputflat" onchange="jsOnChange_SoTK();">
                                                                                </select>
                                                                                <%--  <asp:DropDownList ID="ddlToKhaiSo" runat="server" Width="100%" CssClass="inputflat">
                                                                                        </asp:DropDownList>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNgayDK">
                                                                            <td>
                                                                                <b>Ngày đăng ký</b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <input type="text" id="txtNgayDK" class="inputflat" style="width: 99%;" maxlength="10"
                                                                                    onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                    onblur="javascript:CheckDate(this)" onfocus="this.select()" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowLHXNK">
                                                                            <td>
                                                                                <b>Loại hình XNK </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtLHXNK" style="width: 99%; background: Aqua" class="inputflat"
                                                                                    onkeypress="if (event.keyCode==13){ShowLov('LHXNK');}" onblur="jsGet_TenLHSX();" />
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtDescLHXNK" class="inputflat" style="width: 99%;" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSoKhung">
                                                                            <td>
                                                                                <b>Số khung</b>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtSoKhung" class="inputflat" style="width: 99%;" />
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtDescSoKhung" class="inputflat" style="width: 99%; display:none;" disabled="true"
                                                                                    readonly="true" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSoMay">
                                                                            <td>
                                                                                <b>Số máy</b>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtSoMay" class="inputflat" style="width: 99%;" />
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" id="txtDescSoMay" class="inputflat" style="width: 99%; display:none" disabled="true"
                                                                                    readonly="true" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSoBK" >
                                                                            <td>
                                                                                <b>Số bảng kê</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <input type="text" id="txtSo_BK" class="inputflat" style="width: 99%;" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNgayBK">
                                                                            <td>
                                                                                <b>Ngày bảng kê</b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <input type="text" id="txtNgayBK" class="inputflat" style="width: 99%;" maxlength="10"
                                                                                    onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                    onblur="javascript:CheckDate(this)" />
                                                                            </td>
                                                                        </tr>
																		                           <tr id="rowCQQD">
                                                                            <td>
                                                                                <b>Cơ quan quyết định</b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <input type="text" id="txtCQQD" class="inputflat" style="width: 99%;" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSO_CQQD" >
                                                                            <td>
                                                                                <b>Số quyết định</b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <input type="text" id="txtSOQD" class="inputflat" style="width: 98%;" />
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr id="rowNGAY_CQQD">
                                                                            <td>
                                                                                <b>Ngày quyết định</b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <input type="text" id='txtNgayQD' maxlength='10' class="inputflat" style="width: 40%"
                                                                                onkeyup="javascript:return mask(this.value,this,'2,5','/');" onblur="CheckDate(this);"
                                                                                onfocus="this.select()" />
                                                                                <img src="../../images/cal.gif"   onclick='jsCalendar(document.getElementById("txtNgayQD"))'; />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr><td colspan='2' style="height:2px"></td></tr>
                                                       
                                                        <tr>
                                                            <td colspan='2' align="left" class="errorMessage">
                                                                <table style="width:100%">
                                                                    <tr id="rowProgress">
                                                                        <td  align="left">
                                                                            <div style="background-color: Aqua; display: none" id="divProgress">
                                                                                <b style="font-weight: bold; font-size: 15pt">Đang lấy dữ liệu tại server.Xin chờ một lát...</b>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" style="width:99%" >
                                                                            <div id='divStatus' style="font-weight: bold;" />
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                    <tr align="right" >
                                                                   <td align="right" style="width:99%;">
                                                                    <div id='divDescCTU' style="font-weight: bold;">
                                                                    </div>
                                                                    </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" width="100%">
                                                                <table class="grid_data" cellspacing="0" rules="all" border="1" id="grdChiTiet" style="width: 100%;
                                                                    border-collapse: collapse;">
                                                                    <tr class="grid_header">
                                                                        <td style="width: 30px;">
                                                                        </td>
                                                                        <td align="center" style="display: none; width: 0%" visible="false">
                                                                        </td>
                                                                        <td align="center" style="width: 70px;">
                                                                            Mã chương
                                                                        </td>
                                                                        <td align="center" style="display: none" visible="false">
                                                                        </td>
                                                                        <td align="center" style="width: 50px;">
                                                                            NDKT
                                                                        </td>
                                                                        <td align="center" style="width: 250px;">
                                                                            Nội dung
                                                                        </td>
                                                                        <td align="center" style="width: 90px;">
                                                                            Số tiền
                                                                        </td>
                                                                        <td align="center" style="width: 90px;">
                                                                            Số đã thu</td>
                                                                        <td align="center" style="width: 80px;">
                                                                            Kỳ thuế
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="rowGridDetail1" class="grid_item">
                                                                        <td>
                                                                            <input id="chkRemove1" type="checkbox" checked="checked" onclick="jsCalCharacter(); jsCalTotal();" />
                                                                        </td>
                                                                        <td style="display: none">
                                                                            <input type="text" id="MA_CAP_1" visible="false" style="width: 0%; display: none;"
                                                                                class="inputflat" readonly="readonly" disabled="disabled" onkeypress="if (event.keyCode==13){ShowMLNS('MQ_01');}"
                                                                                value="01" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="MA_CHUONG_1" style="width: 90%; border-color: White; background: Aqua;
                                                                                text-align: center; font-weight: bold" maxlength="3" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MC_01');}" />
                                                                        </td>
                                                                        <td style="display: none">
                                                                            <input type="text" id="MA_KHOAN_1" style="width: 0; display: none; border-color: White;
                                                                                background-color: Yellow; font-weight: bold" class="inputflat" visible="false" disabled="disabled"
                                                                                onkeypress="if (event.keyCode==13){ShowMLNS('MK_01');}" onkeyup="if (event.keyCode!=191){return valInteger(this);}" />
                                                                            <%-- onkeyup="if (event.keyCode!=191){return valInteger(this);}"--%>
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="MA_MUC_1" style="width: 90%; border-color: White; background: Aqua;
                                                                                text-align: center; font-weight: bold" maxlength="4" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MM_01');}"
                                                                                onkeyup="return valInteger(this)" onblur="jsNoiDungKT('MA_MUC_1','NOI_DUNG_1');jsCallSo_DaThu('MA_CHUONG_1','MA_MUC_1','KY_THUE_1','SODATHU_1');" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="NOI_DUNG_1" style="width: 95%; border-color: White; font-weight: bold"
                                                                                maxlength="200" class="inputflat" onblur="jsCalCharacter();" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="SOTIEN_1" style="width: 90%; border-color: White; font-weight: bold;
                                                                                text-align: right;" class="inputflat" onfocus="this.select()" onblur="jsCalTotal();jsCalCharacter();"
                                                                                onkeyup="valInteger(this)" maxlength="17" />
                                                                        </td>
                                                                        <td >
                                                                            <input type="text" id="SODATHU_1" style="width: 90%; border-color: White; font-weight: bold;
                                                                                text-align: right" readonly="readonly" class="inputflat" disabled="disabled" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="KY_THUE_1" style="width: 95%; border-color: White; font-weight: bold;
                                                                                text-align: center" class="inputflat" maxlength="10"  onblur="javascript:jsCheckFormatKyThue(this.value);jsCalCharacter();"
                                                                                onkeypress="if (event.keyCode==13){jsAddNewDetailRow('1');}" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="rowGridDetail2" class="grid_item_alter">
                                                                        <td>
                                                                            <input id="chkRemove2" type="checkbox" checked="checked" onclick="jsCalCharacter(); jsCalTotal();" />
                                                                        </td>
                                                                        <td style="display: none">
                                                                            <input type="text" id="MA_CAP_2" visible="false" style="width: 0%; display: none;"
                                                                                class="inputflat" readonly="readonly" disabled="disabled" onkeypress="if (event.keyCode==13){ShowMLNS('MQ_01');NextFocus();}"
                                                                                value="01" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="MA_CHUONG_2" style="width: 90%; border-color: White; background: Aqua;
                                                                                text-align: center; font-weight: bold" maxlength="3" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MC_02');NextFocus();}"
                                                                                onkeyup="return valInteger(this)" />
                                                                        </td>
                                                                        <td style="display: none">
                                                                            <input type="text" id="MA_KHOAN_2" style="width: 0%; display: none; border-color: White;
                                                                                background-color: Yellow; font-weight: bold" disabled="disabled" class="inputflat"
                                                                                visible="false" onkeypress="if (event.keyCode==13){ShowMLNS('MK_02');NextFocus();}"
                                                                                onkeyup="return valInteger(this)" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="MA_MUC_2" style="width: 90%; border-color: White; background: Aqua;
                                                                                text-align: center; font-weight: bold" maxlength="4" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MM_02');NextFocus();}"
                                                                                onkeyup="return valInteger(this)" onblur="jsNoiDungKT('MA_MUC_2','NOI_DUNG_2');jsCallSo_DaThu('MA_CHUONG_2','MA_MUC_2','KY_THUE_2','SODATHU_2');" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="NOI_DUNG_2" style="width: 95%; border-color: White; font-weight: bold"
                                                                                maxlength="200" class="inputflat" onblur="jsCalCharacter();" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="SOTIEN_2" style="width: 90%; border-color: White; font-weight: bold;
                                                                                text-align: right" class="inputflat" onfocus="this.select()" onblur="jsCalTotal();jsCalCharacter();"
                                                                                onkeyup="valInteger(this)" maxlength="17" />
                                                                        </td>
                                                                        <td >
                                                                            <input type="text" id="SODATHU_2" style="width: 90%; border-color: White; font-weight: bold;
                                                                                text-align: right" readonly="readonly" class="inputflat" disabled="disabled" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="KY_THUE_2" style="width: 95%; border-color: White; font-weight: bold;
                                                                                text-align: center" class="inputflat" maxlength="10" onblur="javascript:jsCheckFormatKyThue(this.value);jsCalCharacter();"
                                                                                onkeypress="if (event.keyCode==13){jsAddNewDetailRow('2');}" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="rowGridDetail3" class="grid_item">
                                                                        <td>
                                                                            <input id="chkRemove3" type="checkbox" checked="checked" onclick="jsCalCharacter(); jsCalTotal();" />
                                                                        </td>
                                                                        <td style="display: none">
                                                                            <input type="text" id="MA_CAP_3" visible="false" style="width: 0%; display: none;"
                                                                                class="inputflat" readonly="readonly" disabled="disabled" onkeypress="if (event.keyCode==13){ShowMLNS('MQ_01');NextFocus();}"
                                                                                value="01" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="MA_CHUONG_3" style="width: 90%; border-color: White; background: Aqua;
                                                                                text-align: center; font-weight: bold" maxlength="3" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MC_03');NextFocus();}"
                                                                                onkeyup="return valInteger(this)" />
                                                                        </td>
                                                                        <td style="display: none">
                                                                            <input type="text" id="MA_KHOAN_3" style="width: 0%; display: none; border-color: White;
                                                                                background-color: Yellow; font-weight: bold" disabled="disabled" class="inputflat"
                                                                                onkeypress="if (event.keyCode==13){ShowMLNS('MK_03');NextFocus();}" onkeyup="return valInteger(this)"
                                                                                visible="false" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="MA_MUC_3" style="width: 90%; border-color: White; background: Aqua;
                                                                                text-align: center; font-weight: bold" maxlength="4" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MM_03');NextFocus();}"
                                                                                onkeyup="return valInteger(this)" onblur="jsNoiDungKT('MA_MUC_3','NOI_DUNG_3');jsCallSo_DaThu('MA_CHUONG_3','MA_MUC_3','KY_THUE_3','SODATHU_3');" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="NOI_DUNG_3" style="width: 95%; border-color: White; font-weight: bold"
                                                                                maxlength="200" class="inputflat" onblur="jsCalCharacter();" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="SOTIEN_3" style="width: 90%; border-color: White; font-weight: bold;
                                                                                text-align: right" class="inputflat" onfocus="this.select()" onblur="jsCalTotal();jsCalCharacter();"
                                                                                onkeyup="valInteger(this)" maxlength="17" />
                                                                        </td>
                                                                        <td >
                                                                            <input type="text" id="SODATHU_3" style="width: 90%; border-color: White; font-weight: bold;
                                                                                text-align: right" readonly="readonly"  disabled="disabled" class="inputflat" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="KY_THUE_3" style="width: 95%; border-color: White; font-weight: bold;
                                                                                text-align: center" class="inputflat" maxlength="10" onblur="javascript:jsCheckFormatKyThue(this.value);jsCalCharacter();"
                                                                                onkeypress="if (event.keyCode==13){jsAddNewDetailRow('3');}" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="rowGridDetail4" class="grid_item_alter">
                                                                        <td>
                                                                            <input id="chkRemove4" type="checkbox" checked="checked" onclick="jsCalCharacter(); jsCalTotal();" />
                                                                        </td>
                                                                        <td style="display: none">
                                                                            <input type="text" id="MA_CAP_4" visible="false" style="width: 0%; display: none;"
                                                                                class="inputflat" readonly="readonly" disabled="disabled" onkeypress="if (event.keyCode==13){ShowMLNS('MQ_01');NextFocus();}"
                                                                                value="01" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="MA_CHUONG_4" style="width: 90%; border-color: White; background: Aqua;
                                                                                text-align: center; font-weight: bold" maxlength="3" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MC_04');NextFocus();}"
                                                                                onkeyup="return valInteger(this)" />
                                                                        </td>
                                                                        <td style="display: none">
                                                                            <input type="text" id="MA_KHOAN_4" style="width: 0%; display: none; border-color: White;
                                                                                background-color: Yellow; font-weight: bold" disabled="disabled" class="inputflat"
                                                                                onkeypress="if (event.keyCode==13){ShowMLNS('MK_04');NextFocus();}" onkeyup="return valInteger(this)"
                                                                                visible="false" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="MA_MUC_4" style="width: 90%; border-color: White; background: Aqua;
                                                                                text-align: center; font-weight: bold" maxlength="4" class="inputflat" onkeypress="if (event.keyCode==13){ShowMLNS('MM_04');NextFocus();}"
                                                                                onkeyup="return valInteger(this)" onblur="jsNoiDungKT('MA_MUC_4','NOI_DUNG_4');jsCallSo_DaThu('MA_CHUONG_4','MA_MUC_4','KY_THUE_4','SODATHU_4');" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="NOI_DUNG_4" style="width: 95%; border-color: White; font-weight: bold"
                                                                                maxlength="200" class="inputflat" onblur="jsCalCharacter();" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="SOTIEN_4" style="width: 90%; border-color: White; font-weight: bold;
                                                                                text-align: right" class="inputflat" onfocus="this.select()" onblur="jsCalTotal();jsCalCharacter();"
                                                                                onkeyup="valInteger(this)" maxlength="17" />
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="SODATHU_4" style="width: 90%; border-color: White; font-weight: bold;
                                                                                text-align: right" readonly="readonly" class="inputflat" disabled="disabled"/>
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="KY_THUE_4" style="width: 95%; border-color: White; font-weight: bold;
                                                                                text-align: center" class="inputflat" maxlength="10" onblur="javascript:jsCheckFormatKyThue(this.value);jsCalCharacter();"
                                                                                 />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='2' height='5'>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" class='text' style="width: 300px">
                                                                Số kt còn lại:<label id="leftCharacter" style="width: 150px; font-weight: bold" visible="false"></label>
                                                            </td>
                                                            <td align="right">
                                                                <table width="300px">
                                                                    <tr align="right">
                                                                        <td align="right" valign="top">
                                                                            Tổng Tiền &nbsp;
                                                                            <input type="text" id="txtTongTien" style="width: 150px; background-color: Yellow;
                                                                                text-align: right;" class="inputflat" value="0" readonly disabled />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                         <tr style="height:50">
                                                            <td  align="left" style="width:15%" >
                                                               <b> Ghi chú: </b>
                                                            </td>
                                                            <td>
                                                                <input type="text"  id="txtGhiChu" class="inputflat" style="width:99%;" maxlength="210" onblur="jsCalCharacter();" onchange="jsCalCharacter();"/>
                                                                  <input type="text"  id="txtRemarks" class="inputflat" style="width:99%; display:none;" maxlength="210" onblur="jsCalCharacter();" onchange="jsCalCharacter();"/>
                                                            </td>
                                                        </tr>
                                                        <tr >
                                                            <td align="right" colspan="2">
                                                                <table width="100%">
                                                                <tr>
                                                                    <td colspan="6">
                                                                    <input type="hidden" id="hdnSoCT_NH" />
                                                                      <input type="hidden" id="hdnSoLan_LapCT" />
                                                                      <input type="hidden" id="hdnTongTienCu" value="0" />
                                                                      <input type="hidden" id="hdnMA_KS" />
                                                                      <input type="hidden" id="hdnSO_CT" />
                                                                       <input type="hidden" id="hddAction" value="1" />
                                                                       <input type="hidden" id="hdLimitAmout" value="0" />
                                                                        <input type="hidden" id="hndNHSHB" />
                                                                    </td>
                                                                </tr>
                                                                <tr  style="display:none;" >
                                                                <td align="right"> PT tính phí</td >
                                                                 <td colspan="6" align="left">
                                                                        <input type="radio" name="chkChargeType" id="chkChargeTypeMP" value="W"  onclick="ResetTinhPhi();"  />Miễn phí
                                                                        <input type="radio" size="0" name="chkChargeType" id="chkChargeTypePT" value="B" onclick="ResetTinhPhi();" style="visibility:hidden"  visible="false"/><%--Phí trong--%>
                                                                        <input type="radio" name="chkChargeType" id="chkChargeTypePN" value="O" checked="checked" onclick="ResetTinhPhi();"/>Phí ngoài
                                                                        <input type="button" id="cmdTinhPhi" class="ButtonCommand" value="Tính phí" onclick="jsTinhPhi(); jsCalCharacter();" />
                                                                  </td>
                                                                </tr>
                                                                    <tr  style="display:none;" >
                                                                        <td>
                                                                            Phí
                                                                        </td>
                                                                        <td align="right">
                                                                            <input type="text" id="txtCharge" style="width: 100px; background-color: Yellow; text-align:right;
                                                                                font-weight: bold" class="inputflat" value="0" disabled="disabled" onfocus="this.select()"
                                                                                onkeyup="valInteger(this)"  />
                                                                        </td>
                                                                        <td align="right">
                                                                            VAT
                                                                        </td>
                                                                        <td align="right">
                                                                            <input type="text" id="txtVAT" style="width: 100px; background-color: Yellow; font-weight: bold; text-align:right"
                                                                                class="inputflat" value="0" readonly disabled="disabled" />
                                                                        </td>
                                                                        <td>Sản phẩm
                                                                           <input type="text" id="txtSanPham" style="width: 100px; background-color: Yellow; font-weight: bold; text-align:right"
                                                                                class="inputflat" value="" readonly disabled="disabled" /> 
                                                                        </td>
                                                                        <td align="right">Tổng trích nợ
                                                                            <input type="text" id="txtTongTrichNo" style="width: 150px; background-color: Yellow;text-align:right;
                                                                                font-weight: bold" class="inputflat" value="0" readonly disabled="disabled" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr  style="display:none;" >
                                                                <td align="right" >Đi Citad</td >
                                                                 <td colspan="6" align="left">
                                                                        <input type="radio" name="chkChekCitad" id="Radio1" value="1" disabled="disabled"/>Kiểu mới
                                                                        <input type="radio" name="chkChekCitad" id="Radio3" value="0" disabled="disabled"/>Kiểu cũ
                                                                  </td>
                                                                </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='2' height='5'>
                                                            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                            <tr>
                                                <td width='100%' align="center" valign="top" class='text' colspan='2' align="right">
                                                    <input type="button" id="cmdThemMoi" class="ButtonCommand" onclick="jsThem_Moi();"
                                                        value="[Lập (M)ới]" accesskey="M" />&nbsp;
                                                    <input type="button" id="cmdGhiKS" class="ButtonCommand" value="[Ghi & (K)S]" onclick="jsGhi_CTU('2');"
                                                        accesskey="K"  />
                                                    &nbsp;
                                                    <input style="display:none" type="button" id="cmdGhiCT" class="ButtonCommand" value="[(G)hi]" onclick="jsGhi_CTU('1');"
                                                        accesskey="G" />
                                                    <!-- <input type="button" id="cmdChuyenKS" class="ButtonCommand" value="[(C)huyển KS]"
                                                        onclick="jsUpdate_CTU_Status('2');" accesskey="C" />-->
                                                    <%-- <input type="button" id="cmdNhapBDS" onclick="jsNhapBDS();" value="[Nhập BDS]" class="ButtonCommand"
                                                                onclick="return cmdNhapBDS_onclick()" Visible="false"  />--%>
                                                    &nbsp;
                                                    <input type="button" id="cmdInCT" value="[(I)n CT]" class="ButtonCommand" onclick="jsIn_CT('1');"
                                                        accesskey="I" />
                                                    &nbsp;
                                                    <input type="button" id="cmdHuyCT" class="ButtonCommand" value="[(H)ủy]" onclick="jsUpdate_CTU_Status('4');"
                                                        accesskey="H" />
                                                    &nbsp;
                                                    <input style="display:none " type="button" id="cmdKhoiPhuc" class="ButtonCommand" value="[Khôi (P)hục]"
                                                        onclick="jsUpdate_CTU_Status('5');" accesskey="P" />
                                                    &nbsp;
                                                    <input type="button" id="cmdInBS" value="[In B(S)]" class="ButtonCommand" onclick="jsIn_CT('2');"
                                                        accesskey="S" visible="false" style="display: none" />
                                                    &nbsp;<asp:Button ID="cmdGhiDe" Text="[Ghi đè]" runat="server" Width='100' CssClass="ButtonCommand"
                                                        Visible="false" />
                                                </td>
                                            </tr>
                                        </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr height="5">
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
      <div id="dialog-popup" style="display: none;">
        </div>
    <div id="wapaler" style="display: none;">
        </div>
    <div class="msgBox" id="msgBox_SHKB" style=" display:none; opacity: 1; min-height: 100px;width:730px;">
            <div class="msgBoxTitle">DANH MỤC</div>
            <div>
                <div>
                    <div class="msgBoxContent" id="msgBox_SHKB_CONTENT">
                     
                      <table border="0" cellpadding="2" cellspacing="1" class="form_input" style="width:100%">
        <tr class="grid-heading">
            <td align="center" class="style2" style ="width:30%" >
                <b>Mã danh mục</b> 
            </td>
            <td  align="center" class="style3" >
                <b>Tên danh mục</b>
            </td>
        </tr>
        <tr >
            
            <td>
            <input type="text" id="msgBox_txtMaDanhMuc"  onkeydown="if(event.keyCode==13) {msgBox_jsSearch(); event.keyCode=9;}" class="inputflat" />&nbsp;
            </td>
         
          
            <td align="center">
             <input type="text" id="msgBox_txtTenDanhMuc" onkeydown="if(event.keyCode==13) {msgBox_jsSearch(); event.keyCode=9;}"  class="inputflat" />&nbsp;
             <%-- <input type="button" id="msgBox_btnSearch" value=" ... " class="inputflat" onclick="msgBox_jsSearch();" />--%>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="style1">
                <div id='msgBox_divDanhMuc' style="max-height:300px;overflow:auto">
                </div>
                <div id='msgBox_divStatus'>
                </div>
                
            </td>
        </tr>
    </table>           
                    </div>
                     
                </div>
                   <div class="msgBoxButtons" id="msgBox_SHKB_ConfirmDivButton">
                       <input type="hidden" id="msgBox_result" value="" />
                        <input id="msgBox-btn-close" class="btn btn-primary" onclick="jsMsgBoxClose();" type="button" name="Ok" value="Đóng" />
                  </div>
                
            </div>
        </div>
     <link href="../../css/dialog.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        function jsDiaglog() {
            $('#msgBox_result').val('');
            document.getElementById('msgBox_txtMaDanhMuc').value = "";
            document.getElementById('msgBox_txtTenDanhMuc').value = "";
            // console.log('Start');
            $('#wapaler').css("display", "block");
            $('#msgBox_SHKB').css("display", "block");

            jsDivAutoCenter('#msgBox_SHKB');
            msgBox_jsSearch();
        }


        function jsMsgBoxClose() {

            $('#wapaler').css("display", "none");
            $('#msgBox_SHKB').css("display", "none");


        }
        var msgBox_type = "";
        var msgBox_SHKB = "";
        var msgBox_txtID = "";
        var msgBox_txtTitle = "";
        var msgBox_txtFocus = "";
        function ShowDMKB(strPage, txtID, txtTitle, txtFocus) {
            document.getElementById('msgBox_result').value = "";
            msgBox_type = strPage;

            msgBox_txtID = txtID;
            msgBox_txtTitle = txtTitle;
            msgBox_txtFocus = txtFocus;
            jsDiaglog();


        }
        function jsDivAutoCenter(divName) {
            //      console.log('em chay phat' + divName);
            $(divName).css({
                position: 'fixed',
                left: ($(window).width() - $(divName).outerWidth()) / 2,
                top: ($(window).height() - $(divName).outerHeight()) / 2,
                height: 'auto'
            });
        }
        //pop-up
        $(window).resize(function () {

            jsDivAutoCenter('.msgBox');
        });
        function msgBox_jsSearch() {
            var strKey = document.getElementById('msgBox_txtMaDanhMuc').value;
            var strTenDM = document.getElementById('msgBox_txtTenDanhMuc').value;
            var strPage = msgBox_type;
            var strSHKB = "";
            if (document.getElementById('txtSHKB').value.length > 0) {
                strSHKB = document.getElementById('txtSHKB').value;
            }
            var curMa_Dthu = '';
            var curMa_DBHC = document.getElementById('txtMaDBHC').value;   //txtMaDBHC
            PageMethods.SearchDanhMuc(curMa_Dthu, curMa_DBHC, strPage, strKey, strTenDM, strSHKB, SearchDanhMuc_Complete, SearchDanhMuc_Error);
        }
        function SearchDanhMuc_Complete(result, methodName) {
            //document.getElementById('divDanhMuc').innerHTML = result;          
            document.getElementById('msgBox_divDanhMuc').innerHTML = jsBuildResultTable(result);

        }
        function SearchDanhMuc_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('msgBox_divStatus').innerHTML = "Lỗi trong quá trình lấy danh sách chứng từ" + error.get_message();
            }
        }
        function SelectDanhMuc(strID, strTitle) {
            //var arr = new Array();
            //arr["ID"] = strID;
            //arr["Title"] = strTitle;
            //document.getElementById('msgBox_result').value = arr;
            jsMsgBoxClose();
            if (strID.length > 0) {
                document.getElementById(msgBox_txtID).value = strID;
                if (msgBox_type == "KhoBac") {

                    jsSHKB_lostFocus(strID);
                }
                if (strTitle != null && msgBox_txtTitle!=null) {
                    if (msgBox_txtTitle.length>0)
                        document.getElementById(''+msgBox_txtTitle).value = strTitle;

                }
                if (msgBox_txtFocus != null) {
                    console.log(msgBox_txtFocus);
                    document.getElementById(msgBox_txtFocus).focus();
                }
            }
            //window.parent.returnValue = arr;
            //window.parent.close();
        }
        function getParam(param) {
            var query = window.location.search.substring(1);
            var parms = query.split('&');
            for (var i = 0; i < parms.length; i++) {
                var pos = parms[i].indexOf('=');
                if (pos > 0) {
                    var key = parms[i].substring(0, pos).toLowerCase();
                    var val = parms[i].substring(pos + 1);
                    if (key == param.toLowerCase())
                        return val;
                }
            }
            jsDivAutoCenter('.msgBox');
            return '';
        }

        function jsBuildResultTable(strInput) {
            var strRet = '';
            if (strInput.length > 0) {
                strRet += " <table border='0' cellpadding='0' cellspacing='0' width='100%'>";
                var arr = strInput.split("|");
                for (var i = 0; i < arr.length - 1; i++) {
                    var strID = arr[i].split(";")[0];
                    var strTitle = arr[i].split(";")[1];
                    strRet += "<tr onclick=\"SelectDanhMuc('" + strID + "','" + strTitle + "');\" class='PH'>" + "<td width='150' align='left'><b>" + strID + "</b></td><td  align='left'>" + strTitle + "</td></tr><tr><td colspan='2' height='8' ></td></tr> <tr><td colspan='2' class='line'></td></tr>";
                }
                strRet += "</table>";
            } else {
                strRet += " <table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td width='100%' align='left' class='nav'>Không tồn tại danh mục này !</td></tr></table>";
            }
            return strRet;
        }
    </script>
</asp:Content>
