﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmSuaLoiNhapBDS.aspx.vb" Inherits="pages_ChungTu_frmSuaLoiNhapBDS"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script language="javascript" type="text/javascript">
    function jsKhoiTao()
    {
        document.getElementById ('cmdNhapBDS').disabled = true;
        jsLoadCTList();        
    }    
    function jsNhapBDS()
    {                   
        if (document.getElementById('divDescCTU').innerHTML.length>0){       
            var strSHKB = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[0];                  
            var strMaDBHC = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[1];                  
            var strSoCT = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[2];                              
            var strSoBT = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[3];                  
            var strMaNV = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[4];                  
            var strSoTien = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[5];                  
            var strSoTT = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[6];                              
            PageMethods.NhapBDS(strSHKB,strSoCT,strSoBT,strMaNV,strMaDBHC,strSoTien,strSoTT,NhapBDS_Complete,NhapBDS_Error);                                                  
        }else{
            alert('Bạn phải chọn 1 dòng để nhập số thực thu!!!'); 
        }                    
    }
    function NhapBDS_Complete(result,methodName)
    {          
        document.getElementById('divTienMat').innerHTML=result;        
        var strAppletMsg=document.tstapp.get();
        if (strAppletMsg=='SUCCESS')
        {
            var strSHKB = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[0];                  
            var strSoCT = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[2];                  
            var strSoBT = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[3];                  
            var strMaNV = document.getElementById('divDescCTU').innerHTML.split(':')[1].split('/')[4];    
            
            PageMethods.Chuyen_TT_CT(strSHKB,strSoCT,strSoBT,strMaNV,"01",ChuyenTT_Complete,ChuyenTT_Error);            
        }else{
            alert('Hach toán BDS không thành công'); 
            return;
        }                                           
    }
    function NhapBDS_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {            
            document.getElementById('divStatus').innerHTML="Lỗi trong quá trình kiểm soát chứng từ" + error.get_message();            
        }
    }        
    function ChuyenTT_Complete(result,userContext,methodName)
    {
        document.getElementById('divStatus').innerHTML=result;
        document.getElementById('divDescCTU').innerHTML='';
        jsKhoiTao();
     }
     function ChuyenTT_Error(error,userContext,methodName)
     {
        document.getElementById('divStatus').innerHTML=error.get_message();
     }               
    
    function jsGetCTU(strSHKB,strDBHC,strSoCT,strSoBT,strSoTien,strSoTT,strMaNV)
    {
        document.getElementById('divDescCTU').innerHTML ='SHKB/DBHC/SoCT/SoBT/MaNV/SoTien/SoThucThu:' + strSHKB + '/' + strDBHC + '/' + strSoCT + '/' + strSoBT + '/' + strMaNV + '/' + strSoTien + '/' + strSoTT;   
        document.getElementById ('cmdNhapBDS').disabled = false;
    }
    
    function jsLoadCTList()
    {       
        var TT = '06';
        PageMethods.LoadCTList(TT,curMa_NV,LoadCTList_Complete,LoadCTList_Error);      
    }
    function LoadCTList_Complete(result,methodName)
    {                             
        document.getElementById('divDSCT').innerHTML = result;          
    }
    function LoadCTList_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {          
        }
    }          
                        
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">SỬA LỖI CHỨNG TỪ CHƯA NHẬP TIỀN Ở BDS</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" class="errorMessage">
                <table>
                    <tr>
                        <td style="height: 10px;" align="center">
                            <input type="button" value="Refresh" id="cmdTim" class="ButtonCommand" onclick="jsLoadCTList();" />&nbsp;
                            <input type="button" id="cmdNhapBDS" class="ButtonCommand" disabled value="Nhập lại BDS" onclick="jsNhapBDS();" />
                        </td>
                        <td align="left">
                            <div id='divStatus' style="font-weight: bold">
                            </div>
                        </td>
                        <td align="right">
                            <div id='divDescCTU' style="font-weight: bold">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px;" align="center">
            </td>
        </tr>
        <tr>
            <td align="center">
                <div id='divDSCT'>
                </div>
            </td>
        </tr>
    </table>
    <div id='divTienMat' style="width: 0px; height: 0px;">
    </div>
</asp:Content>
