﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false"
    CodeFile="frmKTDuLieu.aspx.vb" Inherits="pages_ChungTu_frmKTDuLieu" Title="Kiểm tra dữ liệu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" runat="Server">
    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">KIỂM TRA DỮ LIỆU</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
                <table id="Table2" width="100%" border="0" cellpadding="2" cellspacing="1" class="form_input">
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblTuNgay" runat="server" CssClass="label">Từ ngày</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox runat="server" ID="txtTuNgay" Width="120px" CssClass="inputflat"></asp:TextBox>&nbsp;
                        </td>
                        <td class="form_label">
                            <asp:Label ID="lblDenNgay" runat="server" CssClass="label">Đến ngày</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox runat="server" ID="txtDenNgay" Width="120px" CssClass="inputflat"></asp:TextBox>&nbsp;
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblMaDV" runat="server" CssClass="label">Tài khoản nợ</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtTKNo" runat="server" CssClass="inputflat" Width="70%"> </asp:TextBox>
                        </td>
                        <td class="form_label">
                            <asp:Label ID="lblKhoGiay" runat="server" CssClass="label">Tài khoản có</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtTKCo" runat="server" CssClass="inputflat" Width="70%"> </asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label" colspan="2">
                            <asp:CheckBox ID="chkDBTK" runat="server" Text="Kiểm tra địa bàn-TK Có-CQ Thu" />
                        </td>
                        <td class="form_label" colspan="2">
                            <asp:CheckBox ID="chkMLNS" runat="server" Text="Kiểm tra MLNS-TLPC" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="middle" align="center" style="height: 40px">
                &nbsp;
                <asp:Button ID="cmdKiemTra" runat="server" CssClass="ButtonCommand" Text="[Kiểm tra]"
                    TabIndex="5"></asp:Button>&nbsp;<asp:Button ID="cmdExit" runat="server" CssClass="ButtonCommand"
                        Text="[Thoát]" TabIndex="5"></asp:Button>&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <div style="vertical-align: top; height: 200px; overflow: auto; width: 100%;">
                    <asp:DataGrid ID="dtg" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                        Width="100%" CssClass="grid_data">
                        <HeaderStyle CssClass="grid_header"></HeaderStyle>
                        <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                        <ItemStyle CssClass="grid_item" />
                        <Columns>
                            <asp:BoundColumn DataField="Ngay_KB" HeaderText="Ngày">
                                <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="KyHieu_CT" HeaderText="Ký hiệu CT" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="So_CT" HeaderText="Số CT" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="So_BT" HeaderText="Mã NV/ Số BT" ItemStyle-HorizontalAlign="Left">
                                <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
