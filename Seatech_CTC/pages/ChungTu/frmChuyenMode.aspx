﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmChuyenMode.aspx.vb" Inherits="pages_ChungTu_frmChuyenMode" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <input type="hidden" runat="server" id="Hidden1" />
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">CHUYỂN TRẠNG THÁI BDS CHỨNG TỪ</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            
        </tr>
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%"
                    class="form_input">
                    <tr align="left">
                        <td class="form_label" width='10%'>
                            <asp:Label runat="server" CssClass="label" ID="Label1"> Số CT
                            </asp:Label>
                        </td>
                        <td class="form_control" width='25%'>
                            <asp:TextBox ID="txtSoCT" runat="server" MaxLength="20" Width="50%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>
                        <td class="form_label" width='10%'>
                            <asp:Label runat="server" CssClass="label" ID="Label2"> Trạng thái BDS
                            </asp:Label>
                        </td>
                        <td class="form_control" width='25%'>
                            <asp:DropDownList ID="cboBDS" runat="server" Width='100%' CssClass='inputflat' EnableViewState="true">
                                <asp:ListItem Value="0" Text="Offline" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="form_control" width='30%'>
                            <asp:Button ID="cmdTim" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm">
                            </asp:Button>&nbsp;
                            <asp:Button ID="cmdChuyen" runat="server" CssClass="ButtonCommand" Text="Chuyển TT BDS"
                                TabIndex="5" Width="100"></asp:Button>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label6" runat="server" CssClass="label">Mã NV</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMaNV" runat="server" MaxLength="20" Width="50%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>
                        <td class="form_label">
                            <asp:Label runat="server" CssClass="label" ID="lbtrangthai"> Trạng thái CT
                            </asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTrangThaiCT" runat="server" Width='100%' CssClass='inputflat'
                                EnableViewState="true">
                                <asp:ListItem Value="99" Text=" Tất cả " Selected="True"></asp:ListItem>
                                <asp:ListItem Value="00" Text=" Chưa kiểm soát "></asp:ListItem>
                                <asp:ListItem Value="03" Text=" Kiểm soát lỗi "></asp:ListItem>
                                <asp:ListItem Value="05" Text=" Chờ kiểm soát "></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="form_control">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 30px;" align="center">
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <div id="dgdata" style="vertical-align: top; height: 300px; overflow: auto; width: 100%;">
                    <asp:DataGrid ID="gridData" runat="server" AutoGenerateColumns="False" TabIndex="9"
                        Width="99%" BorderColor="#989898" CssClass="grid_data">
                        <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                        <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
                        <ItemStyle CssClass="grid_item" />
                        <Columns>
                            <asp:BoundColumn DataField="So_CT" HeaderText="Số CT">
                                <HeaderStyle Width="90"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="90"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="KYHIEU_CT" HeaderText="Ký hiệu CT">
                                <HeaderStyle Width="90"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="80"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SO_BT" HeaderText="Số BT">
                                <HeaderStyle Width="90"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="90"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TEN_DN" HeaderText="GD viên">
                                <HeaderStyle Width="100px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="100"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BDS" HeaderText="Tình trạng BDS">
                                <HeaderStyle Width="120px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="120px"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MA_NV" Visible="false" HeaderText="MA NV">
                                <HeaderStyle Width="0"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Chọn">
                                <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                            Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                            Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                        </PagerStyle>
                    </asp:DataGrid>
                </div>
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
