﻿Imports Business
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports System.Diagnostics
Imports System.Xml.XmlDocument
Imports Business.NewChungTu
Imports System.Xml
Imports Business.CTuCommon

Partial Class pages_ChungTu_frmHuyDuyet
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Load_Datagrid()
            DM_NV()
        End If
    End Sub

    Private Sub DM_NV()

        Dim dsNV As DataSet
        Dim dk As String = ""
        dk = "And Ban_LV=" & clsCTU.TCS_GetMaDT(CType(Session("User"), Integer))

        Dim strSQL As String = "SELECT a.Ma_NV,a.ten_DN FROM tcs_dm_NhanVien a Where a.ma_nhom='02' " & dk
        Try
            dsNV = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(ddlMaNV, dsNV, 1, 0, "Tất cả")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            Throw ex
        Finally

        End Try
    End Sub

    Private Sub Load_Datagrid(Optional ByVal pv_strSoCT As String = "", Optional ByVal pv_strMaNV As String = "", Optional ByVal pv_strPTTT As String = "")
        Dim ds As New DataSet
        Dim strWhereSTT As String = ""
        Dim strSQL As String
        If pv_strMaNV <> "" Then
            strWhereSTT = strWhereSTT & " AND MA_NV=" & pv_strMaNV & ""
        End If
        If pv_strSoCT <> "" Then
            strWhereSTT = strWhereSTT & " AND SO_CT='" & pv_strSoCT & "'"
        End If
        If pv_strPTTT <> "" Then
            strWhereSTT = strWhereSTT & " AND PT_TT='" & pv_strPTTT & "'"
        End If
        Try
            ' AND MA_DTHU ='" & clsCTU.TCS_GetMaDT(CType(Session("User"), Integer)) & "' 
            strSQL = "SELECT SHKB,KYHIEU_CT, SO_CT,Ma_NV, SO_BT,TRANG_THAI, So_BThu, TTIEN, TT_TTHU, NGAY_KB," & _
              " TK_KH_NH,TK_KB_NH,MA_XA DBHC,PT_TT" & _
              " FROM TCS_CTU_HDR  " & _
              " WHERE NGAY_KB = " & clsCTU.TCS_GetNgayLV(Session.Item("User")) & _
               " AND MA_NV IN(SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" & CType(Session("User"), Integer) & "'))" & _
              " AND so_bt_ktkb <> 1 AND TRANG_THAI='01'" & strWhereSTT & _
              " ORDER BY so_bt DESC "

            ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If Not ds Is Nothing Then
                dgChungTu.DataSource = ds
                dgChungTu.DataBind()
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
        End Try
    End Sub

    Protected Sub cmdHuy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdHuy.Click
        Dim strMaNV As String
        Dim strPT_TT As String
        Dim v_strMa_HuyKS As String
        Dim strSHKB As String
        Dim v_strOriRefNo As String
        Dim v_strREFNo As String = ""
        Dim strTongTien As String
        If Session("User") Is Nothing Then
            lblError.Text = " Hãy ra ngoài và đăng nhập lại"
            Exit Sub
        End If
        v_strMa_HuyKS = Session("User").ToString
        For i As Integer = 0 To dgChungTu.Items.Count() - 1
            Dim chkSelect As CheckBox
            chkSelect = dgChungTu.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then

                strMaNV = Trim(dgChungTu.Items(i).Cells(6).Text)
                strPT_TT = Trim(dgChungTu.Items(i).Cells(4).Text)
                strSHKB = Trim(dgChungTu.Items(i).Cells(0).Text)

                Dim key As New KeyCTu
                key.Kyhieu_CT = clsCTU.TCS_GetKHCT(strSHKB) 'clsCTU.TCS_GetKHCT(CInt(strMaNV))
                key.So_CT = Trim(dgChungTu.Items(i).Cells(2).Text)
                key.SHKB = strSHKB
                key.Ngay_KB = clsCTU.TCS_GetNgayLV(Session.Item("User"))
                key.Ma_NV = CInt(strMaNV)
                key.So_BT = Trim(dgChungTu.Items(i).Cells(3).Text)
                key.Ma_Dthu = clsCTU.TCS_GetMaDT_KB(strSHKB)
                If strPT_TT = "01" Then 'Chuyen khoan
                    'Thanh toan corebank
                    strTongTien = Trim(dgChungTu.Items(i).Cells(5).Text)
                    v_strOriRefNo = Get_RefNo(key.So_CT, key.So_BT)
                    Dim v_objCoreBank As New clsCoreBankUtil
                    Dim v_strRemark As String = "Huy nop thue qua tai khoan khach hang ngay " & ConvertNumberToDate(clsCTU.TCS_GetNgayLV(Session.Item("User")))

                    '1)Tai khoan cua kho bac tai nh                
                    Dim v_strTKNo As String = Trim(dgChungTu.Items(i).Cells(8).Text).ToString.Trim 'TK_NH_KB
                    Dim v_strTKco As String = Trim(dgChungTu.Items(i).Cells(7).Text).ToString.Trim 'TK_KH_NH
                    '2)Chu y ghi no tai khoan kho bac,ghi co tai khoan khach hang
                    'If Not v_objCoreBank.CancelPayment(v_strTKco, v_strOriRefNo, v_strTKNo, strTongTien, "VND", v_strRemark, v_strREFNo) Then
                    'Exit Sub
                    'End If
                ElseIf strPT_TT = "03" Then 'Trung gian
                    'Thanh toan corebank
                    'strMaDBHC = Trim(dgChungTu.Items(i).Cells(1).Text)
                    strTongTien = Trim(dgChungTu.Items(i).Cells(5).Text)
                    v_strOriRefNo = Get_RefNo(key.So_CT, key.So_BT)
                    Dim v_objCoreBank As New clsCoreBankUtil
                    Dim v_strRemark As String = "Huy nop thue qua tai khoan trung gian ngay " & ConvertNumberToDate(clsCTU.TCS_GetNgayLV(Session.Item("User")))

                    '1)Tai khoan cua kho bac tai nh  

                    Dim v_strTKNo As String = Trim(dgChungTu.Items(i).Cells(8).Text).ToString.Trim 'TK_NH_KB
                    Dim v_strTKco As String = Trim(dgChungTu.Items(i).Cells(7).Text).ToString.Trim 'TK_KH_NH
                    '2)Chu y ghi no tai khoan kho bac,ghi co tai khoan khach hang
                    'If Not v_objCoreBank.CancelPayment(v_strTKco, v_strOriRefNo, v_strTKNo, strTongTien, "VND", v_strRemark, v_strREFNo) Then
                    'Exit Sub
                    'End If
                End If
                Try
                    'Voi cac truong hop khac thi chi can chuyen trang thai
                    ChungTu.buChungTu.CTU_SetTrangThai(key, "04", "", "", "", v_strMa_HuyKS)
                    Exit For
                Catch ex As Exception
                    log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
                    lblError.Text = "Lỗi hủy kiểm soát!"
                End Try
            End If
        Next
        Load_Datagrid()
    End Sub

    Protected Sub cmdTim_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTim.Click
        Load_Datagrid(txtSoCT.Text.Trim, ddlMaNV.SelectedValue.Trim, ddlPTTT.SelectedValue.Trim)
    End Sub

    Protected Sub dgChungTu_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgChungTu.PageIndexChanged
        Load_Datagrid(txtSoCT.Text.Trim, ddlMaNV.SelectedValue.Trim, ddlPTTT.SelectedValue.Trim)
        dgChungTu.CurrentPageIndex = e.NewPageIndex
        dgChungTu.DataBind()
    End Sub
End Class
