﻿Imports System.Data
Imports System.Diagnostics
Imports System.Xml
Imports System.Xml.XmlDocument
Imports Business
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports Business.ChungTu
Imports Business.NewChungTu
Imports Business.BaoCao
Imports System.IO


Partial Class pages_ChungTu_frmLapCT_HQ
    Inherits System.Web.UI.Page

#Region "Lap chung tu"

    Public Enum eCTUAction
        Them_Moi = 0
        Ghi = 1
        Chuyen_KS = 2
        Ghi_ChuyeKS = 3
        Huy = 4
        KhoiPhuc = 5
        InCT = 6
        KiemSoat = 7
        ChuyenTra = 8
        Huy_KS = 9
    End Enum

    Private Shared DefSHKB As String = ""
    Private Shared DefCQThu As String = ""
    Private Shared DefDBHC As String = ""

    Private Shared mblnEdit As Boolean = False
    Private Shared mNguonNNT As Integer = 0
    Private Shared url As String = ""

    Private Const ERR_SYSTEM_OK As String = "0"

    Private Const gc_MAX_SOTIEN_LENGTH As Integer = 15
    Private Const gc_MAX_KYTHUE_LENGTH As Integer = 7
    Private mv_objUser As New CTuUser
    Private Ma_CN As String = ""
    Private Shared mv_SesssionID As String = ""
    Private Shared cls_corebank As New CorebankServiceESB.clsCoreBank
    Private Shared CORE_ON_OFF As String = ConfigurationManager.AppSettings.Get("CORE.ON").ToString()

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'BEGIN-TYNK CHECK NGAY NGHI
        'If Not Business.CTuCommon.GetNgayNghi() Then
        '    ' clsCommon.ShowMessageBox(Me, "Hôm nay là ngày nghỉ. Bạn không được vào chức năng này!!!")
        '    Response.Redirect("~/pages/frmHomeNV.aspx?CheckNN=1", False)
        '    Exit Sub
        'End If
        'END TYNK CHECK NGAY NGHI
        'DIVLoaiTien.InnerHtml = Get_Detail()
        Dim v_strSHKB As String = String.Empty '01/10/2012
        Dim v_strMaDiemThu As String = String.Empty
        'Dim v_strMaHQ As String = String.Empty
        Dim strNgay_LV As String = ""
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not Session.Item("User") Is Nothing Then
            v_strSHKB = clsCTU.TCS_GetKHKB(CType(Session("User"), Integer))
            v_strMaDiemThu = clsCTU.TCS_GetMaDT(CType(Session("User"), Integer))
            'v_strMaHQ = clsCTU.TCS_GetMaHQ(v_strMaDiemThu) '01/10/2012
            mv_objUser = New CTuUser(Session.Item("User"))
            Ma_CN = mv_objUser.MA_CN
        End If
        mv_SesssionID = Session.Item("User").ToString()
        If Not IsPostBack Then
            If (Not ClientScript.IsStartupScriptRegistered(Me.GetType(), "InitDefValues")) Then
                ClientScript.RegisterStartupScript(Me.GetType(), "InitDefValues", PopulateDefaultValues(v_strMaDiemThu), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrSHKB", PopulateArrSHKB(v_strMaDiemThu), True) '01/10/2012
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrDBHC", PopulateArrDBHC(v_strSHKB), True) '01/10/2012
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrCQThu", PopulateArrCQThu(v_strMaDiemThu), True) '01/10/2012
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTKGL", PopulateArrTKGL(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrNT", DM_NT(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrLTHUE", DM_LTHUE(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrLTIEN", DM_LTIEN(), True)
                'ClientScript.RegisterStartupScript(Me.GetType(), "InitArrMaSanPham", DM_SANPHAM(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTKNoNSNN", PopulateArrTKNoNSNN(v_strSHKB), True) '01/10/2012
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTKCoNSNN", PopulateArrTKCoNSNN(v_strSHKB), True) '01/10/2012
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrLHSX", PopulateArrLHSX(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTT_NH_B", PopulateArrTT_NH_B(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTT_NH_TT", PopulateArrTT_NH_TT(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrMANHA", DM_MA_NH_A(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrPTTT", DM_PTTT(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitPage", "jsThem_Moi();", True)
            End If
            If Not Request.Params("SoCT") Is Nothing Then
                Dim soCT As String = Request.Params("SoCT").ToString
                If soCT <> "" Then
                    Dim dtCT As DataTable
                    dtCT = dtlCTU_Load(Session.Item("User").ToString, clsCTU.TCS_GetNgayLV(Session.Item("User").ToString), "01", soCT)
                    If dtCT.Rows.Count > 0 Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "PageLoad", "HiddenCancelButton('" & dtCT.Rows(0)("trang_thai").ToString & "')", True)
                        ClientScript.RegisterStartupScript(Me.GetType(), "PageLoad", "jsSTT_GetCTU('" & dtCT.Rows(0)("SHKB").ToString & "','" & dtCT.Rows(0)("SO_CT").ToString & "','" & dtCT.Rows(0)("SO_BT").ToString & "','" & dtCT.Rows(0)("TTien").ToString & "','" & dtCT.Rows(0)("TT_TThu").ToString & "');", True)
                    End If
                End If
            End If
            'Dim v_objCoreBank As New clsCoreBankUtil
            'If Not Session.Item("User") Is Nothing Then
            '    'divApplet.InnerHtml = v_objCoreBank.GetAppletAvailBDSNote(clsCTU.Get_TellerID(Session.Item("User")).ToString)
            'End If
        End If
    End Sub

    Private Function PopulateDefaultValues(ByVal pv_strMaDiemThu As String) As String
        Dim myJavaScript As StringBuilder = New StringBuilder()
        Dim strSHKB = GetSHKB(pv_strMaDiemThu)
        myJavaScript.Append(" var dtmNgayLV ='" & ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString)) & "';")
        myJavaScript.Append(" var dtmNgayNH ='" & ConvertNumbertoString(clsCTU.TCS_GetNgayKH_NH(Session.Item("User").ToString)) & "';")
        myJavaScript.Append(" var defSHKB ='" & GetDefaultSHKB(pv_strMaDiemThu) & "';")
        myJavaScript.Append(" var defDBHC ='" & GetDefaultDBHC(pv_strMaDiemThu) & "';")
        'Begin remove default value for CQThu
        'myJavaScript.Append(" var defCQThu ='" & GetDefaultCQThu(pv_strMaDiemThu) & "';")
        myJavaScript.Append(" var defCQThu ='" & "" & "';")
        'End 
        myJavaScript.Append(" var defMaVangLai ='" & GetDefaultMaVangLai() & "';")
        '07/06/2011 Kienvt: them ma ngan hang nhan theo kho bac
        myJavaScript.Append(" var defMaNHNhan ='" & GetDefaultMaNHTH(strSHKB) & "';")
        'end
        '01/10/2012 ManhNV
        myJavaScript.Append(" var defMaNHTT ='" & GetDefaultMaNHTT(strSHKB) & "';")
        'end
        '13/09/2012 Anhld: them ma hai quan
        myJavaScript.Append(" var defMaHQ ='" & GetDefaultMaHQ(pv_strMaDiemThu) & "';")
        'end        
        myJavaScript.Append(" var curCtuStatus ='99';")
        myJavaScript.Append(" var curBDS_Status ='" & GetBdsStatus() & "';")
        myJavaScript.Append(" var curMaNNT ='';")
        myJavaScript.Append(" var curKyThue ='" & GetKyThue(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString)) & "';")
        myJavaScript.Append(" var curSoCT ='';")
        myJavaScript.Append(" var curSo_BT ='';")
        myJavaScript.Append(" var curMa_NV ='" & CType(Session("User"), Integer).ToString & "';")
        myJavaScript.Append(" var curTeller_ID ='" & clsCTU.Get_TellerID(CType(Session("User"), Integer)) & "';")
        myJavaScript.Append(" var saiSo =' " & CTuCommon.Get_ParmValues("SAI_THUCTHU", pv_strMaDiemThu).ToString() & "';")
        myJavaScript.Append(" var sodong ='" & GetSoDong() & "';")
        Return myJavaScript.ToString
    End Function

    Private Function PopulateArrSHKB(ByVal pv_strMaDiemThu As String) As String
        Dim strSQL As String = "SELECT b.shkb,b.ten from  TCS_DM_KHOBAC b WHERE   b.tinh_trang=1"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrSHKB = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrSHKB[" & i & "] ='" & dt.Rows(i)("SHKB") & ";" & dt.Rows(i)("TEN") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function

    Private Function PopulateArrDBHC(ByVal strSHKB As String) As String
        Dim strSQL As String = "SELECT b.shkb ma_kb, A.MA_XA, A.TEN FROM TCS_DM_XA A, TCS_DM_KHOBAC B  WHERE  B.ma_db= A.MA_XA AND B.SHKB='" & strSHKB & "' " '"SELECT a.ma_kb,b.ma_xa, b.ten FROM tcs_map_tk_nh_kb a, tcs_dm_xa b WHERE a.dbhc = b.ma_xa and a.ma_kb='" & strSHKB & "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrDBHC = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrDBHC[" & i & "] ='" & dt.Rows(i)("ma_kb") & ";" & dt.Rows(i)("ma_xa") & ";" & dt.Rows(i)("ten") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    Private Function PopulateArrLHSX() As String
        Dim strSQL As String = "SELECT MA_LH,  TEN_LH,TEN_VT  FROM TCS_DM_LHINH  ORDER BY TEN_VT"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrLHSX = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrLHSX[" & i & "] ='" & dt.Rows(i)("MA_LH") & ";" & dt.Rows(i)("TEN_LH") & ";" & dt.Rows(i)("TEN_VT") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    Private Function PopulateArrTT_NH_B() As String
        Dim strSQL As String = "SELECT DISTINCT( MA_GIANTIEP) as MA_NH_B,  TEN_GIANTIEP as TEN_NH_B, MA_TRUCTIEP as MA_NH_TT,  TEN_TRUCTIEP AS TEN_NH_TT  FROM tcs_dm_nh_giantiep_tructiep  ORDER BY MA_GIANTIEP"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrTT_NH_B = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrTT_NH_B[" & i & "] ='" & dt.Rows(i)("MA_NH_B") & ";" & dt.Rows(i)("TEN_NH_B") & ";" & dt.Rows(i)("MA_NH_TT") & ";" & dt.Rows(i)("TEN_NH_TT") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    Private Function PopulateArrTT_NH_TT() As String
        Dim strSQL As String = "SELECT DISTINCT( MA_TRUCTIEP) as MA_NH_TT,  TEN_TRUCTIEP as TEN_NH_TT  FROM tcs_dm_nh_giantiep_tructiep  ORDER BY MA_TRUCTIEP"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrTT_NH_TT = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrTT_NH_TT[" & i & "] ='" & dt.Rows(i)("MA_NH_TT") & ";" & dt.Rows(i)("TEN_NH_TT") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    Private Function PopulateArrCQThu(ByVal pv_strMaDiemThu As String) As String
        Dim strSQL As String = "SELECT MA_DTHU,MA_CQTHU,TEN FROM TCS_DM_CQTHU WHERE MA_DTHU='" & pv_strMaDiemThu & "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrCQThu = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrCQThu[" & i & "] ='" & dt.Rows(i)("MA_DTHU") & ";" & dt.Rows(i)("MA_CQTHU") & ";" & dt.Rows(i)("TEN") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    Private Function PopulateArrTKGL() As String
        Dim strSQL As String = "SELECT SO_TK, TEN_TK FROM TCS_DM_TK_TG"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        Dim noOfItems As Integer = dt.Rows.Count
        Dim myJavaScript As StringBuilder = New StringBuilder()
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            myJavaScript.Append(" var arrTKGL = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrTKGL[" & i & "] ='" & dt.Rows(i)("SO_TK") & ";" & dt.Rows(i)("TEN_TK") & "';")
            Next
            Return myJavaScript.ToString()
        Else
            myJavaScript.Append(" var arrTKGL = new Array(0);")
            Return myJavaScript.ToString()
        End If

    End Function

    Private Function PopulateArrTKNoNSNN(ByVal strSHKB As String) As String
        'Dim strSQL As String = "SELECT distinct a.tk_kb,a.ma_kb, b.ten_tk FROM tcs_map_tk_nh_kb a, tcs_dm_taikhoan b WHERE a.tk_kb = b.tk  AND a.ma_kb = '" & strSHKB & "'"
        Dim strSQL As String = "SELECT distinct a.tk,a.ten_tk,a.shkb FROM tcs_dm_taikhoan A WHERE a.shkb = '" & strSHKB & "' AND a.TK_KB_NH='1'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrTKNoNSNN = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrTKNoNSNN[" & i & "] ='" & dt.Rows(i)("shkb") & ";" & dt.Rows(i)("tk") & ";" & dt.Rows(i)("ten_tk") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function

    Private Function PopulateArrTKCoNSNN(ByVal strSHKB As String) As String
        Dim strSQL As String = "SELECT tk,ten_tk,dbhc,MA_CQTHU FROM TCS_DM_TAIKHOAN WHERE TRIM(MA_CQTHU) IS NOT NULL AND DBHC IN (SELECT DBHC FROM TCS_MAP_TK_NH_KB WHERE MA_KB='" & strSHKB & "') AND TINH_TRANG='1'  AND TK_KB_NH='0'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrTKCoNSNN = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrTKCoNSNN[" & i & "] ='" & dt.Rows(i)("dbhc") & ";" & dt.Rows(i)("MA_CQTHU") & ";" & dt.Rows(i)("tk") & ";" & dt.Rows(i)("ten_tk") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function

    Private Function GetDefaultDBHC(ByVal pv_strMaDiemThu As String) As String
        Dim strSQL As String = " SELECT A.MA_XA, A.TEN FROM TCS_DM_XA A, TCS_THAMSO B WHERE A.MA_XA = B.GIATRI_TS AND B.TEN_TS = 'MA_DBHC' AND B.MA_DTHU = '" & pv_strMaDiemThu & "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Return dt.Rows(0)(0).ToString & ";" & dt.Rows(0)(1).ToString
        End If
        Return String.Empty
    End Function

    Private Function GetDefaultSHKB(ByVal pv_strMaDiemThu As String) As String
        Dim strSQL As String = "SELECT b.shkb,b.ten FROM TCS_THAMSO a,TCS_DM_KHOBAC b WHERE a.giatri_ts=b.shkb And a.ten_ts='SHKB' and a.ma_dthu='" & pv_strMaDiemThu & "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Return dt.Rows(0)(0).ToString & ";" & dt.Rows(0)(1).ToString
        End If
        Return String.Empty
    End Function
    Private Function GetSHKB(ByVal pv_strMaDiemThu As String) As String
        Dim strSQL As String = "SELECT b.shkb FROM TCS_THAMSO a,TCS_DM_KHOBAC b WHERE a.giatri_ts=b.shkb And a.ten_ts='SHKB' and a.ma_dthu='" & pv_strMaDiemThu & "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Return dt.Rows(0)(0).ToString
        End If
        Return String.Empty
    End Function

    Private Function GetDefaultCQThu(ByVal pv_strMaDiemThu As String) As String
        Dim strSQL As String = "SELECT A.MA_CQTHU, A.TEN FROM TCS_DM_CQTHU A, TCS_THAMSO B WHERE A.MA_CQTHU = B.GIATRI_TS AND A.MA_DTHU = B.MA_DTHU AND B.TEN_TS = 'CQ_THU' AND A.MA_DTHU='" & pv_strMaDiemThu & "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Return dt.Rows(0)(0).ToString() & ";" & dt.Rows(0)(1).ToString
        End If
        Return String.Empty
    End Function

    Private Function GetDefaultMaNHNhan(ByVal pv_strMaDiemThu As String) As String
        Dim strSQL As String = "SELECT b.shkb,b.ten FROM TCS_THAMSO a,TCS_DM_KHOBAC b WHERE a.giatri_ts=b.shkb And a.ten_ts='SHKB' and a.ma_dthu='" & pv_strMaDiemThu & "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        Dim v_strSHKB As String = String.Empty
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            v_strSHKB = dt.Rows(0)(0).ToString
        End If
        strSQL = "SELECT A.MA, A.TEN FROM TCS_DM_NGANHANG A WHERE A.SHKB='" + v_strSHKB + "'"
        dt = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Return dt.Rows(0)(0).ToString() & ";" & dt.Rows(0)(1).ToString
        End If
        Return String.Empty

    End Function
    Private Function GetDefaultMaNHTT(ByVal pv_strSHKB As String) As String
        Dim strSQL As String = "SELECT A.MA_TRUCTIEP MA, A.TEN_TRUCTIEP TEN FROM TCS_DM_NH_GIANTIEP_TRUCTIEP A WHERE A.SHKB='" + pv_strSHKB + "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Return dt.Rows(0)(0).ToString() & ";" & dt.Rows(0)(1).ToString
        End If
        Return String.Empty

    End Function
    Private Function GetDefaultMaNHTH(ByVal pv_strSHKB As String) As String
        Dim strSQL As String = "SELECT A.MA_GIANTIEP MA, A.TEN_GIANTIEP TEN FROM TCS_DM_NH_GIANTIEP_TRUCTIEP A WHERE A.SHKB='" + pv_strSHKB + "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Return dt.Rows(0)(0).ToString() & ";" & dt.Rows(0)(1).ToString
        End If
        Return String.Empty

    End Function
    Private Function GetDefaultMaHQ(ByVal pv_strMaDiemThu As String) As String
        Dim strSQL As String = clsCTU.TCS_GetMaHQ(pv_strMaDiemThu)        
        Return strSQL

    End Function

    Private Function GetDefaultMaVangLai() As String
        If Load_DM_NNT_DB().Tables.Count > 0 Then
            Return Load_DM_NNT_DB().Tables(0).Rows(0)(0)
        Else
            Return String.Empty
        End If
    End Function

    Private Function Load_DM_NNT_DB() As DataSet
        Dim strSQL As String
        Try
            strSQL = "Select distinct MA_NNT as ID, TEN_NNT as Title " & _
                " From TCS_DM_NNT_KB " & _
                " Order By MA_NNT"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function DM_NT() As String
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable

        Dim strSQL As String = "SELECT * from Tcs_dm_nguyente "
        dt = DataAccess.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrMaNT= new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrMaNT[" & i & "] ='" & dt.Rows(i)("Ma_NT") & ";" & dt.Rows(i)("TEN") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    Private Function DM_LTHUE() As String
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable

        Dim strSQL As String = "SELECT * from TCS_DM_LTHUE WHERE MA_LTHUE = '04'"
        dt = DataAccess.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrMaLTHUE= new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrMaLTHUE[" & i & "] ='" & dt.Rows(i)("Ma_LTHUE") & ";" & dt.Rows(i)("TEN") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    Private Function DM_LTIEN() As String
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable

        Dim strSQL As String = "SELECT MA_LT, MA_LT ||'-'||ten_lt ten_lt from tcs_dm_loaitien_thue"
        dt = DataAccess.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrMaLTIEN= new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrMaLTIEN[" & i & "] ='" & dt.Rows(i)("MA_LT") & ";" & dt.Rows(i)("ten_lt") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
#End Region

#Region "AjaxFunction"
    <System.Web.Services.WebMethod()> _
  Public Shared Function GetSession() As String
        If Not clsCommon.GetSessionByID(mv_SesssionID) Then
            'Response.Redirect("~/pages/frmLogin.aspx", False)
            Return "0"
        Else
            Return "1"
        End If
    End Function
    <System.Web.Services.WebMethod()> _
   Public Shared Function GetTK_KH_NH(ByVal pv_strTKNH As String) As String
        Dim strReturn As String = ""
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Dim ma_loi As String = ""
        Dim so_du As String = ""
        Dim branch_tk As String = ""
        'If CORE_ON_OFF = "1" Then
        strReturn = cls_corebank.GetTK_KH_NH(pv_strTKNH.Replace("-", ""), ma_loi, so_du, branch_tk)
        'Else
        '    strReturn = "<PARAMETER><PARAMS><BANKACCOUNT>111000222</BANKACCOUNT><ACY_AVL_BAL>1000000000</ACY_AVL_BAL><CRR_CY_CODE>VND</CRR_CY_CODE><RETCODE>00000</RETCODE><ERRCODE></ERRCODE></PARAMS></PARAMETER>"
        'End If
        Return strReturn
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function Check_TaiKhoanTM(ByVal pvTK_chuyen As String) As String
        Dim dt As New DataTable
        Dim ret As String = "NOTOK"
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If
        Try
            Dim strSql As String = "Select 1 from TCS_DM_CHINHANH Where taikhoan_tienmat='" & pvTK_chuyen.Trim & "'"
            dt = DataAccess.ExecuteToTable(strSql)
            If VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                ret = "OK"
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Loi check tai khoan tien mat", HttpContext.Current.Session("TEN_DN"))
        Finally
            If Not dt Is Nothing Then
                dt.Dispose()
            End If
        End Try
        Return ret
    End Function
    <System.Web.Services.WebMethod()> _
  Public Shared Function GetTK_KH_NH_MST(ByVal pv_strMST As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        'Return clsCoreBankUtilPGB.GetMAPMST_TKKH(pv_strMST.Trim)
        ' Return "123"
    End Function
    <System.Web.Services.WebMethod()> _
  Public Shared Function GetTEN_KH_NH(ByVal pv_strTKNH As String) As String
        Dim strReturn As String = ""
        Dim ma_loi As String = ""
        Dim so_du As String = ""
        'If CORE_ON_OFF = "1" Then
        strReturn = cls_corebank.GetTen_TK_KH_NH(pv_strTKNH.Replace("-", ""))
        'Else
        '    strReturn = "Tài khoản test"
        'End If
        Return strReturn
    End Function
    <System.Web.Services.WebMethod()> _
   Public Shared Function GetURLSignature(ByVal pv_strTK As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        'Dim v_objCoreBank As New clsCoreBankUtilPGB
        ' url = v_objCoreBank.GetSignature(pv_strTK)
        Return url
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetFee(ByVal pv_strAcountNo As String, ByVal pv_LoaiCK As String, ByVal pv_strAmount As String, _
                             ByVal pv_strLoaiTien As String, ByVal pv_strFeeType As String, ByVal pv_strProvine As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Dim strMaCN As String = HttpContext.Current.Session("MA_CN_USER_LOGON")
        Dim strMaNV As String = HttpContext.Current.Session("UserName")
      Return "123"
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTT_NganHangNhan(ByVal strSHKB As String, ByVal strDBHC As String, ByVal strCQThu As String) As String
        Dim strResult As String = "null"
        Dim dt As New DataTable
        Dim strSql As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Try
            'strSql = "Select MA_NH_B, TEN_NH_B  From tcs_dm_nganhang" & _
            '" Where Ma_Xa ='" & strDBHC & "' and SHKB='" & strSHKB & "'"
            strSql = "Select MA, TEN  From tcs_dm_nganhang" & _
                " Where SHKB='" & strSHKB.Trim & "'"
            dt = DataAccess.ExecuteToTable(strSql)
            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                Return dt.Rows(0)(0).ToString() & ";" & dt.Rows(0)(1).ToString
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi get trạng thái Ngân hàng nhận", HttpContext.Current.Session("TEN_DN"))
        Finally
            If Not dt Is Nothing Then
                dt.Dispose()
            End If
        End Try
        Return ""
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTT_NH(ByVal strSHKB As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Dim strResult As String = "null"
        Dim dt As New DataTable
        Dim strSql As String
        Try
            strSql = "Select MA_GIANTIEP, TEN_GIANTIEP, MA_TRUCTIEP, TEN_TRUCTIEP  From TCS_DM_NH_GIANTIEP_TRUCTIEP" & _
                " Where SHKB='" & strSHKB.Trim & "'"
            dt = DataAccess.ExecuteToTable(strSql)
            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                Return dt.Rows(0)(0).ToString() & ";" & dt.Rows(0)(1).ToString & ";" & dt.Rows(0)(2).ToString & ";" & dt.Rows(0)(3).ToString
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi GetTT NH", HttpContext.Current.Session("TEN_DN"))
        Finally
            If Not dt Is Nothing Then
                dt.Dispose()
            End If
        End Try
        Return ""
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_NNThue(ByVal pv_strMaNNT As String, ByVal strNgay_LV As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Return clsCTU.TCS_Check_NNThue(pv_strMaNNT, strNgay_LV)
    End Function
    <System.Web.Services.WebMethod()> _
   Public Shared Function CTU_Check_Exist_NNT(ByVal pv_strMaNNT As String, ByVal pv_SoTK As String) As Integer
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Return clsCTU.CTU_Check_Exist_NNT_HQ(pv_strMaNNT, pv_SoTK)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_NNThue_HQ(ByVal pv_strMaNNT As String, ByVal pv_strTenNNT As String, ByVal pv_strCQT As String, ByVal pv_strTenCQT As String, ByVal pv_strSoTK As String, ByVal pv_strLHXNK As String, ByVal pv_strNamDK As String, ByVal pv_strLoaitt As String, ByVal pv_ngayLV As String) As String
        Dim strMaNNT As String = pv_strMaNNT.Trim
        Dim strTenDTNT As String = pv_strTenNNT
        Dim strMaHQ As String = pv_strCQT.Trim
        Dim strTenHQ As String = pv_strTenCQT
        Dim strMaLH As String = pv_strLHXNK.Trim
        Dim strNamDK As String = ""
        If pv_strNamDK.Length > 0 Then
            strNamDK = pv_strNamDK.Substring(6, 4).Trim
        End If
        Dim strSoTK As String = pv_strSoTK.Trim
        Dim strErrCode As String = ""
        Dim strErrMSG As String = ""
        Dim strResult As String = ""
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        'Get thong tin to khai ben Hai quan, cap nhat thong tin lay duoc vao database
        '        CustomsService.ProcessMSG.getMSG12(strMaNNT, strNamDK, strSoTK, strErrCode, strErrMSG)
        If strSoTK.Length > 0 Then
            strResult = strSoTK & ";"
            Return strResult
        End If
        Dim cnDSToKhai As New DataAccess
        Dim dt As DataTable

        Dim strSQL As String = "SELECT DISTINCT(SO_TK) from TCS_DS_TOKHAI WHERE MA_NNT = '" & strMaNNT & "' "
        dt = DataAccess.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()

            For i As Integer = 0 To dt.Rows.Count - 1
                strResult &= dt.Rows(i)("SO_TK") & ";"
            Next
            Return strResult

        End If
        'End If
        Return strResult
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function LoadTT_ToKhai(ByVal pv_strMaNNT As String, ByVal pv_strSoTK As String, ByVal pv_strLoaiTT As String) As String
        Dim strMaNNT As String = pv_strMaNNT.Trim
        Dim strSoTK As String = pv_strSoTK.Trim
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Return clsCTU.TCS_CheckToKhai(strMaNNT, strSoTK, pv_strLoaiTT)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GET_CITAD_TYPE() As String
        Dim strSQL As String = "SELECT GIATRI_TS  FROM TCS_THAMSO_HT WHERE TEN_TS='CITAD_TYPE'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim strCITAD_TYPE As String = ""
            strCITAD_TYPE = dt.Rows(0)("GIATRI_TS")
            Return strCITAD_TYPE.ToString()
        End If
        Return String.Empty
    End Function

    ''' ******************************************************'
    ''' Name: Get_s the NN thue_ H q_ LTT.
    ''' Author:ANHLD
    ''' Goals: Lay to khai theo loai tien thue
    ''' Date: 21.09.2012
    ''' Event: Add
    ''' ************************BEGIN******************************'
    ''' <param name="pv_strMaNNT">The PV_STR ma NNT.</param>
    ''' <param name="pv_strTenNNT">The PV_STR ten NNT.</param>
    ''' <param name="pv_strCQT">The PV_STR CQT.</param>
    ''' <param name="pv_strTenCQT">The PV_STR ten CQT.</param>
    ''' <param name="pv_strSoTK">The PV_STR so TK.</param>
    ''' <param name="pv_strLHXNK">The PV_STR LHXNK.</param>
    ''' <param name="pv_strNamDK">The PV_STR nam DK.</param>
    ''' <param name="pv_strLoaitt">The PV_STR loaitt.</param>
    ''' <returns>System.String.</returns>
    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_NNThue_HQ_LTT(ByVal pv_strMaNNT As String, ByVal pv_strTenNNT As String, ByVal pv_strCQT As String, ByVal pv_strTenCQT As String, ByVal pv_strSoTK As String, ByVal pv_strLHXNK As String, ByVal pv_strNamDK As String, ByVal pv_strLoaitt As String, ByVal pv_NgayLV As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Dim strMaNNT As String = pv_strMaNNT.Trim
        Dim strTenDTNT As String = pv_strTenNNT.Trim
        Dim strMaHQ As String = pv_strCQT.Trim
        Dim strTenHQ As String = pv_strTenCQT.Trim
        Dim strMaLH As String = pv_strLHXNK.Trim
        Dim strNamDK As String = pv_strNamDK.Substring(6, 4)
        Dim strSoTK As String = pv_strSoTK

        'Get thong tin to khai ben Hai quan, cap nhat thong tin lay duoc vao database
        'CustomsService.ProcessMSG.getMSG12(strMaNNT, strTenDTNT, strMaHQ, strTenHQ, strMaLH, strNamDK, strSoTK)

        Return clsCTU.TCS_CheckToKhai(strMaNNT, strSoTK, pv_strLoaitt)
    End Function

    ''' ************************END********************************'
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetCTU(ByVal pv_strMaNV As String, ByVal pv_strSHKB As String, ByVal pv_strSoCT As String, ByVal pv_strSoBT As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Return clsCTU.TCS_GetCTU(pv_strSHKB, pv_strSoCT, pv_strSoBT, CInt(pv_strMaNV), "")
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function KiemTraBDS() As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Dim v_strReturnMsg As String = ""
        If GetBdsStatus() <> gblnTTBDS Then
            'trạng thai bds thay doi
            v_strReturnMsg = "1"
        Else
            'khong thay doi
            v_strReturnMsg = "0"
        End If
        Return v_strReturnMsg
    End Function

    <System.Web.Services.WebMethod()> _
   Public Shared Function Update_CTU_Status(ByVal pv_strMaNV As String, ByVal pv_strSHKB As String, ByVal pv_strSoCT As String, _
                                            ByVal pv_strSoBT As String, ByVal pv_strCTUAction As String, ByVal pv_strSoFT As String, ByVal pv_strCurStatus As String, ByVal pv_strHoldDebit As String) As String

        Try
            Dim v_strReturnMsg As String = String.Empty
            'If GetBdsStatus() <> gblnTTBDS Then
            '    v_strReturnMsg = "1;Trạng thái BDS đã thay đổi. Hãy thoát khỏi chương trình và đăng nhập lại"
            '    Return v_strReturnMsg
            'End If
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
                ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
            End If
            Dim key As New KeyCTu
            key.Kyhieu_CT = clsCTU.TCS_GetKHCT(pv_strSHKB)
            key.So_CT = pv_strSoCT
            key.SHKB = pv_strSHKB
            key.Ngay_KB = clsCTU.TCS_GetNgayLV(pv_strMaNV)
            key.Ma_NV = CInt(pv_strMaNV)
            key.So_BT = CInt(pv_strSoBT)
            key.Ma_Dthu = pv_strSHKB 'clsCTU.TCS_GetMaDT_KB(pv_strSHKB)
            Dim strMaCN As String = clsCTU.TCS_GetMaChiNhanh(pv_strMaNV)
            Dim strTenDN As String = clsCTU.TCS_GetTenDN(pv_strMaNV)
            Dim strFunction As String = "D"
            Dim strErrNum As String = ""
            Dim strErrMsg As String = ""
            Dim strTT_KS As String = "0"
            'Load tham so gui huy sang corebank
            Dim str_TS_HUY() As String = pv_strHoldDebit.Split(";")
            '
            If CInt(pv_strMaNV) <> 0 Then
                If (pv_strCTUAction = eCTUAction.Chuyen_KS) Then
                    ChungTu.buChungTu.CTU_ChuyenKS(key, False)
                    v_strReturnMsg = "Chứng từ đã được chuyển sang trạng thái chờ kiểm soát."
                    strTT_KS = "1"
                ElseIf (pv_strCTUAction = eCTUAction.Huy) Then
                    If pv_strCurStatus = "02" Then
                        ChungTu.buChungTu.CTU_Huy_Da_KS(key, False)
                        v_strReturnMsg = "Hủy chứng từ thành công."
                    Else
                        ChungTu.buChungTu.CTU_Huy(key, False)
                        v_strReturnMsg = "Hủy chứng từ thành công."
                    End If
                ElseIf (pv_strCTUAction = eCTUAction.KhoiPhuc) Then
                    ChungTu.buChungTu.CTU_KhoiPhuc(key, False)
                    v_strReturnMsg = "Khôi phục chứng từ thành công."
                End If
            Else
                Return String.Empty
            End If
            'them trang thai strTT_KS de tao tham so hien thi nut in khi chuyen kiem soat thanh cong
            Return ERR_SYSTEM_OK & ";" & v_strReturnMsg & ";" & "SHKB/SoCT/SoBT" & ":" & key.SHKB & "/" & key.So_CT & "/" & key.So_BT & ";" & strTT_KS
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi cập nhật trạng thái chứng từ", HttpContext.Current.Session("TEN_DN"))
            Return String.Empty
        End Try
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenTieuMuc(ByVal pv_strMaTMuc As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Return clsCTU.Get_TenTieuMuc(pv_strMaTMuc)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function LoadCTList(ByVal pv_strMaNV As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Try
            Dim dsCT As DataSet = ChungTu.buChungTu.CTU_GetListCT(clsCTU.TCS_GetNgayLV(pv_strMaNV), CInt(pv_strMaNV), clsCTU.TCS_GetMaDT(CInt(pv_strMaNV)), "04")

            Dim dtCT As DataTable = dsCT.Tables(0)
            Dim v_strBuildTable As String = ""

            v_strBuildTable &= "<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdDSCT' style='width:100%;border-collapse:collapse;'>"

            Dim i As Integer = 0
            For Each dr As DataRow In dtCT.Rows
                v_strBuildTable &= "<tr onmouseover = DG_changeBackColor(this,true); onmouseout = DG_changeBackColor(this,false); onclick=jsGetCTU('" & dr("SHKB").ToString & "','" & dr("SO_CT").ToString & "','" & dr("SO_BT").ToString & "') " & "class='" & IIf(i Mod 2 = 0, "grid_item", "grid_item_alter") & "'>"
                v_strBuildTable &= "<td align='center' style='width: 20%'><img src='" & Business.CTuCommon.Get_ImgTrangThai(dr("TRANG_THAI").ToString, dr("tt_cthue").ToString, dr("ma_ks").ToString) & "' alt='" & Business.CTuCommon.Get_TrangThai(dr("TRANG_THAI").ToString) & "' /></td>"
                v_strBuildTable &= "<td align='center' style='width: 30%'><a href='#'>" & dr("SO_CT").ToString & "</a></td>"
                v_strBuildTable &= "<td align='center' style='width: 50%'>" & dr("TEN_DN").ToString & "/" & dr("SO_BT").ToString & "</td>"
                v_strBuildTable &= "</tr>"
            Next

            v_strBuildTable &= "</table>"
            Return v_strBuildTable
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi dữ liệu chi tiết chứng từ", HttpContext.Current.Session("TEN_DN"))
            Return String.Empty
        End Try
    End Function

    Private Shared Function Check_Mode() As String
        Dim strSQL As String
        Dim ds As DataSet
        Try
            strSQL = "Select TEN_TS,GIATRI_TS From TCS_THAMSO WHERE MA_DTHU='00' AND TEN_TS='MODE'"
            ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If Not ds Is Nothing Then
                Return ds.Tables(0).Rows(0)("GIATRI_TS").ToString
            Else
                Return ""
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi kiểm tra Giá trị tham số", HttpContext.Current.Session("TEN_DN"))
            Return ""
        End Try
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Ghi_CTU(ByVal pv_strMa_NV As String, ByVal pv_strXML As String, ByVal pv_stAction As String, ByVal pv_strEditMode As String) As String
        Try

            'sonmt
            Dim objUser As CTuUser = New CTuUser(HttpContext.Current.Session("User"))
            Dim macn As String = objUser.MA_CN

            Dim v_strReturn As String
            If HttpContext.Current.Session("User") Is Nothing Then
                v_strReturn = "-1;Bạn hãy thoát khỏi chương trình và đăng nhập lại"
                Return v_strReturn
            End If
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
                ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
            End If

            If pv_strEditMode = "1" Then 'Them moi
                mblnEdit = False
            Else 'Cap nhat chung tu
                mblnEdit = True
            End If

            If pv_stAction = "1" Then 'Ghi CTU
                v_strReturn = GhiChungTu(pv_strXML, CInt(pv_strMa_NV), CInt(macn))
            Else '=2 Ghi va kiem soat
                v_strReturn = GhiChuyenKS(pv_strXML, CInt(pv_strMa_NV), CInt(macn))
            End If

            If v_strReturn.Length > 1 And v_strReturn.IndexOf("/") > 0 Then
                Return ERR_SYSTEM_OK & ";" & "Lập mới chứng từ thành công" & ";" & "SHKB/SoCT/SoBT/TT:" & v_strReturn
            End If
            If v_strReturn.Length > 1 Then
                Return "-1" & ";" & v_strReturn
            End If
            If v_strReturn = "1" Then
                Return "-1" & ";" & "Mã quỹ không đúng!!"
            End If
            If v_strReturn = "2" Then
                Return "-1" & ";" & "Mã chương không đúng!!"
            End If
            If v_strReturn = "4" Then
                Return "-1" & ";" & "Mã tiểu mục không đúng!!"
            End If
            Return String.Empty
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi valid dữ liệu dtl", HttpContext.Current.Session("TEN_DN"))
            Throw ex
        End Try
    End Function

    Private Shared Function GhiChuyenKS(ByVal pv_strXML As String, Optional ByVal pv_intMaNV As Integer = 0, Optional ByVal pv_intMaCN As Integer = 0) As String
        Dim v_strTK_KH_NH As String = ""
        Dim v_strPT_TINHPHI As String = ""
        Dim v_strTTien As String = ""
        Dim v_strPHI_GD As String = ""
        Dim v_strPHI_VAT As String = ""
        Try
            Dim objHDR As New Business.NewChungTu.infChungTuHDR
            Dim objDTL() As Business.NewChungTu.infChungTuDTL

            Dim v_strReturn As String
            If GetBdsStatus() <> gblnTTBDS Then
                v_strReturn = ";Trạng thái BDS đã thay đổi. Hãy thoát khỏi chương trình và đăng nhập lại"
                Return v_strReturn
            End If

            ' Kiểm tra các điều kiện bắt buộc trước khi ghi
            'If Not Check_Info() Then
            '    Return False
            'End If

            ' Lấy thông tin từ form đưa vào bean
            Dim strNgay_DK As String = ""
            XMLToBean(pv_strXML, objHDR, objDTL, strNgay_DK, pv_intMaNV)
            Dim objUser = New CTuUser(pv_intMaNV)

            Dim key As New KeyCTu
            key.Ma_Dthu = objHDR.Ma_DThu
            key.Ma_NV = objHDR.Ma_NV
            key.Ngay_KB = objHDR.Ngay_KB
            key.SHKB = objHDR.SHKB
            key.So_BT = objHDR.So_BT
            key.Kyhieu_CT = objHDR.KyHieu_CT
            key.So_CT = objHDR.So_CT
            key.TT_BDS = objHDR.TT_BDS
            key.TT_NSTT = objHDR.TT_NSTT
            v_strTK_KH_NH = objHDR.TK_KH_NH
            v_strPT_TINHPHI = objHDR.PT_TINHPHI
            v_strTTien = objHDR.TTien
            v_strPHI_GD = objHDR.PHI_GD
            v_strPHI_VAT = objHDR.PHI_VAT
            If key.Ngay_KB() <> GetDate(key.SHKB) Then 'And key.SHKB <> clsCTU.TCS_GetKHKB(pv_intMaNV) Then ' manhnv-02/10/2012
                v_strReturn = "Ngày của kho bạc " & key.SHKB & " đã thay đổi. Không lập được chứng từ."
                Return v_strReturn
            End If
            If insertTK(objHDR.TK_Co, objHDR.TEN_TK_CO, objHDR.Ma_Xa, objHDR.Ma_CQThu, "0", objHDR.SHKB) Then
            Else
                v_strReturn = "Tài khoản có  không có trong danh mục."
                Return v_strReturn
            End If
            'Nếu thông tin chi tiết không hợp lệ : --> Thoát
            objHDR.Trang_Thai = "05"
            Dim blnVND As Boolean = True
            If (objHDR.Ma_NT <> "VND") Then blnVND = False
            Dim intVitriLoi() As Integer = Valid_DTL(objDTL, True, blnVND, True)
            If (Not IsNothing(intVitriLoi)) Then
                Return intVitriLoi(1).ToString
            End If

            If (Not mblnEdit) Then   'Lập chứng từ mới

                If Not ChungTu.buChungTu.Insert_New(objHDR, objDTL, "0", , , , , pv_intMaCN) Then
                    v_strReturn = "Lỗi ghi và kiểm soát chứng từ"
                    Return v_strReturn
                End If
            Else            'Sửa chứng từ    

                objHDR.SHKB = key.SHKB
                objHDR.Ngay_KB = key.Ngay_KB
                objHDR.Ma_NV = key.Ma_NV
                objHDR.So_BT = key.So_BT
                objHDR.Ma_DThu = key.Ma_Dthu

                If (Not Business.buChungTu.Update_New(objHDR, objDTL, clsCTU.mblnNhapThucThu)) Then
                    Return String.Empty
                End If

            End If
            '2)Chuyen sang trang thai cho kiem soat
            'ChungTu.buChungTu.CTU_ChuyenKS(key, False)
            'Neu thanh cong tra ve so bt
            Return key.SHKB & "/" & key.So_CT & "/" & key.So_BT & "/05"
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi trong quá trình ghi chuyển kiểm soát", HttpContext.Current.Session("TEN_DN"))
            Return ex.ToString()
        End Try
    End Function

    Private Shared Function GhiChungTu(ByVal pv_strXML As String, Optional ByVal pv_intMaNV As Integer = 0, Optional ByVal pv_intMaCN As Integer = 0) As String
        Try
            Dim objHDR As New NewChungTu.infChungTuHDR
            Dim objDTL() As NewChungTu.infChungTuDTL

            Dim v_strDesCTU As String = ""

            Dim v_strReturn As String = ""
            If GetBdsStatus() <> gblnTTBDS Then
                v_strReturn = "Trạng thái BDS đã thay đổi. Hãy thoát khỏi chương trình và đăng nhập lại"
                Return v_strReturn
            End If

            ' Kiểm tra các điều kiện bắt buộc trước khi ghi
            'If Not Check_Info() Then
            '    Return False
            'End If

            ' Lấy thông tin từ form đưa vào bean
            Dim strNgay_DK As String = ""
            XMLToBean(pv_strXML, objHDR, objDTL, strNgay_DK, pv_intMaNV)
            Dim objUser = New CTuUser(pv_intMaNV)
            Dim key As New KeyCTu
            key.Ma_Dthu = objHDR.Ma_DThu
            key.Ma_NV = objHDR.Ma_NV
            key.Ngay_KB = objHDR.Ngay_KB
            key.SHKB = objHDR.SHKB
            key.So_BT = objHDR.So_BT
            key.Kyhieu_CT = objHDR.KyHieu_CT
            key.So_CT = objHDR.So_CT
            key.TT_BDS = objHDR.TT_BDS
            key.TT_NSTT = objHDR.TT_NSTT
            'If key.Ngay_KB() <> GetDate(key.SHKB) Then 'And key.SHKB <> clsCTU.TCS_GetKHKB(pv_intMaNV) Then'manhnv-02/10/2012
            '    v_strReturn = "Ngày của kho bạc " & key.SHKB & " đã thay đổi. Không lập được chứng từ."
            '    Return v_strReturn
            'End If
            Dim blnVND As Boolean = True
            If (objHDR.Ma_NT <> "VND") Then blnVND = False
            Dim intVitriLoi() As Integer = Valid_DTL(objDTL, True, blnVND, True)
            If (Not IsNothing(intVitriLoi)) Then
                Return intVitriLoi(1).ToString
            End If
            If insertTK(objHDR.TK_Co, objHDR.TEN_TK_CO, objHDR.Ma_Xa, objHDR.Ma_CQThu, "0", objHDR.SHKB) Then
            Else
                v_strReturn = "Tài khoản có  không có trong danh mục."
                Return v_strReturn
            End If
            If (Not mblnEdit) Then

                If Not ChungTu.buChungTu.Insert_New(objHDR, objDTL, "0", , , , , pv_intMaCN) Then
                    v_strReturn = "Lỗi trong quá trình ghi chứng từ"
                    Return v_strReturn
                End If

            Else            'Sửa chứng từ

                objHDR.SHKB = key.SHKB
                objHDR.Ngay_KB = key.Ngay_KB
                objHDR.Ma_NV = key.Ma_NV
                objHDR.So_BT = key.So_BT
                objHDR.Ma_DThu = key.Ma_Dthu

                If (Not Business.buChungTu.Update_New(objHDR, objDTL, clsCTU.mblnNhapThucThu)) Then
                    v_strReturn = "Cập nhật chứng từ không thành công"
                    Return v_strReturn
                End If

            End If
            Return key.SHKB & "/" & key.So_CT & "/" & key.So_BT & "/00"
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi ghi chứng từ", HttpContext.Current.Session("TEN_DN"))
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return ex.ToString()
        End Try
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetData_SHKB(ByVal strLoaiDM As String, ByVal strSHKB As String) As String
        Dim v_strReturnMsg As String = strLoaiDM & "|"
        Dim strSql As String = ""
        Dim strMaDBHC As String = ""
        strSHKB = strSHKB.Trim
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Select Case strLoaiDM
            Case "DBHC_DEF".ToUpper
                strSql = " SELECT B.MA_TINH,c.ten FROM TCS_DM_KHOBAC A, TCS_DM_cqthu B,tcs_dm_xa c WHERE  A.shkb= B.shkb and b.dbhc_tinh=c.ma_xa AND A.SHKB='" & strSHKB & "' and ma_hq is null" 'strSql = "SELECT B.MA_TINH,B.TEN FROM TCS_DM_KHOBAC A, TCS_DM_cqthu B WHERE  A.shkb= B.shkb AND A.SHKB='" & strSHKB & "' and ma_hq is null"
                'Case "DBHC_DEF".ToUpper
                '    strSql = " SELECT A.MA_XA, A.TEN FROM TCS_DM_XA A, TCS_DM_KHOBAC B  WHERE  B.ma_db= A.MA_XA AND B.SHKB='" & strSHKB & "' "
            Case "DBHC".ToUpper
                strMaDBHC = GetMaDBHC_KB(strSHKB)
                strSql = " SELECT b.ma_xa, b.ten FROM tcs_dm_xa b WHERE b.ma_cha ='" & strMaDBHC & "' union SELECT b.ma_xa, b.ten FROM tcs_dm_xa b WHERE b.ma_xa ='" & strMaDBHC & "' "
            Case "TaiKhoanNo".ToUpper

                strSql = "SELECT distinct a.shkb,a.tk,a.ten_tk FROM tcs_dm_taikhoan A WHERE a.shkb = '" & strSHKB & "' AND a.TK_KB_NH='1'"
            Case "TaiKhoanCo".ToUpper
                strSql = " SELECT dbhc,MA_CQTHU,tk,ten_tk FROM TCS_DM_TAIKHOAN WHERE TRIM(MA_CQTHU) IS NOT NULL AND SHKB='" & strSHKB & "'AND TK_KB_NH='0'" ' DBHC IN (SELECT DBHC FROM TCS_MAP_TK_NH_KB WHERE MA_KB='" & strSHKB & "') AND TINH_TRANG='1' and tk_kb_nh='0'"
            Case "CQTHU_SHKB".ToUpper
                strSql = " SELECT MA_DTHU,MA_CQTHU,TEN FROM TCS_DM_CQTHU WHERE SHKB='" & strSHKB & "'"
            Case "CQTHU".ToUpper
                strSql = " SELECT MA_DTHU,MA_CQTHU,TEN FROM TCS_DM_CQTHU " 'WHERE MA_DTHU in(SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS='" & strSHKB & "')"
            Case "LHSX".ToUpper
                strSql = "SELECT MA_LH as MA_DM, TEN_LH as TEN_DM, TEN_VT FROM TCS_DM_LHINH  ORDER BY TEN_VT"
            Case "SHKB".ToUpper
            Case "KHCT".ToUpper
                strSql = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT' AND ROWNUM=1 " 'AND MA_DTHU=(SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS='" & strSHKB & "')"
            Case "TINH_SHKB".ToUpper
                strSql = "SELECT B.MA_TINH,B.TEN FROM TCS_DM_KHOBAC A, TCS_DM_XA B WHERE  A.MA_TINH= B.MA_XA AND A.SHKB='" & strSHKB & "' "
        End Select
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then

            Dim noOfItems As Integer = dt.Rows.Count
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim v_strTemp As String = String.Empty
                For j As Integer = 0 To dt.Columns.Count - 1
                    v_strTemp &= dt.Rows(i)(j).ToString & ";"
                Next
                v_strReturnMsg = v_strReturnMsg & v_strTemp.Substring(0, v_strTemp.LastIndexOf(";")) & "|"
            Next
            Return v_strReturnMsg
        End If
        Return v_strReturnMsg
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetData_MaHQ(ByVal strMaCQThu As String) As String
        Dim v_strReturnMsg As String = ""
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        v_strReturnMsg = clsCTU.TCS_GetMaHQ(strMaCQThu)
             
        Return v_strReturnMsg
    End Function
    ''' ******************************************************'
    ''' Name: Gets the data_ ma HQ.
    ''' Author: ANHLD
    ''' Goals: Lay thong tin chi tiet ma_hq, ma_hq_ph, shkb theo ma co quan thu
    ''' Date:
    ''' Event:
    ''' ************************BEGIN******************************'
    ''' <param name="strMaCQThu">The STR ma CQ thu.</param>
    ''' <returns>System.String.</returns>
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetData_ByCQT(ByVal strMaCQThu As String, ByVal strMa_HQ As String) As String
        Dim v_strReturnMsg As String = ""
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        v_strReturnMsg = clsCTU.TCS_GetByCQT(strMaCQThu, strMa_HQ)

        Return v_strReturnMsg
    End Function
    ''' ************************END********************************'
    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenMaHQ(ByVal pv_strMaHQ As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Return clsCTU.Get_TenMaHQ(pv_strMaHQ.Trim)
    End Function
    <System.Web.Services.WebMethod()> _
   Public Shared Function Get_TenNNT(ByVal pv_strMaNNT As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Return clsCTU.Get_TenNNT(pv_strMaNNT.Trim)
    End Function

    ''' ******************************************************'
    ''' Name: Get_s the ten SHKB.
    ''' Author: ANHLD
    ''' Goals: Lay ten so hieu kho bac
    ''' Date: 24.09.2012
    ''' Event:
    ''' ************************BEGIN******************************'
    ''' <param name="pv_strSHKB">The PV_STR SHKB.</param>
    ''' <returns>System.String.</returns>
    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenSHKB(ByVal pv_strSHKB As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If
        Return clsCTU.Get_TenSHKB(pv_strSHKB)
    End Function
    ''' ************************END********************************'
#End Region

#Region "Private Util"

    Private Function GetKyThue(ByVal strNgayLV As String) As String
        Try
            If (strNgayLV.Length <> 8) Then Return "" 'Không đúng định dạng đầu vào. 

            Dim strThang, strNam As String
            strNam = strNgayLV.Substring(0, 4)
            strThang = strNgayLV.Substring(4, 2)

            Return (strThang & "/" & strNam)
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin kỳ thuế", HttpContext.Current.Session("TEN_DN"))
            Throw ex
        End Try
    End Function

    Private Function GetSoDong() As String
        Dim strSQL As String = "Select count(menh_gia) from tcs_dm_loaitien "
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        Return dt.Rows(0)(0).ToString
    End Function

    Private Shared Function GetBdsStatus() As String
        Dim strSQL As String
        Dim TTBDS As String
        Try
            strSQL = "Select TEN_TS,GIATRI_TS From TCS_THAMSO WHERE MA_DTHU='00' AND TEN_TS='MODE'"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If dt.Rows.Count > 0 Then
                TTBDS = dt.Rows(0)("GIATRI_TS").ToString.ToUpper
            Else
                TTBDS = ""
            End If
            Return TTBDS
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi lấy thông tin BDS", HttpContext.Current.Session("TEN_DN"))
            Return String.Empty
        End Try
    End Function

    Private Function Check_NhapsoTT(ByVal soCT As String) As Boolean
        Dim strSql As String = "SELECT So_CT FROM TCS_KHOQUY_DTL WHERE SO_CT='" & soCT & "'"
        Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSql, CommandType.Text)
        If Not ds Is Nothing And ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function Check_TTCTu(ByVal soCT As String) As String
        Dim strSql As String = "SELECT So_CT, isactive FROM TCS_ctu_hdr WHERE   SO_CT='" & soCT & "'"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ISACTIVE").ToString
        Else
            Return ""
        End If
    End Function

    Private Shared Function Get_MaNH(ByVal strMaXa As String, ByVal strMaDThu As String, ByVal strSHKB As String) As DataTable
        Dim strResult As String = "null"
        Dim dt As DataTable
        Dim strSql As String
        Try
            strSql = "Select MA_NH_A, MA_NH_B  From tcs_dm_nganhang" & _
                        " Where Ma_Xa ='" & strMaXa & "' and ma_dthu='" & strMaDThu & "' and SHKB='" & strSHKB & "'"
            dt = DataAccess.ExecuteToTable(strSql)
            If Not dt Is Nothing Then
                Return dt
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin Mã ngân hàng", HttpContext.Current.Session("TEN_DN"))
        Finally
            If Not dt Is Nothing Then
                dt.Dispose()
            End If
        End Try
    End Function

    Private Shared Sub XMLToBean(ByVal pv_strXML As String, ByRef hdr As NewChungTu.infChungTuHDR, _
                                 ByRef dtls As NewChungTu.infChungTuDTL(), ByRef strNgay_DK As String, Optional ByVal pv_intMaNV As Integer = 0)

        Try
            Dim strValue As String
            Dim strXmlRoot As String
            Dim dtl As NewChungTu.infChungTuDTL
            Dim dblTTien As Double = 0.0

            Dim v_xmlDocument As New XmlDocument
            Dim v_nodeCTU_HDR As XmlNodeList
            Dim v_nodeCTU_DTL As XmlNodeList

            strXmlRoot = "/CTC_CTU/CTU_HDR/"
            v_xmlDocument.LoadXml(pv_strXML.Replace("&", "&amp;"))

            v_nodeCTU_HDR = v_xmlDocument.SelectNodes("/CTC_CTU/CTU_HDR")

            ' ****************************************************************************
            ' Người sửa: Anhld
            ' Ngày 14/09/2012
            ' Mục đích: Them ma hai quan va loai tien thue
            ''
            '' Người sửa: Anhld
            '' Ngày sửa: 22.09.2012
            '' Mục đích: Đéo lấy theo vị trí nữa, code lởm bỏ sừ. Lấy theo tên cơ.
            '*****************************************************************************            
            'hdr.MA_HQ = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_HQ).InnerText
            hdr.MA_HQ = v_nodeCTU_HDR.Item(0).SelectSingleNode(strXmlRoot + clsCTU.ColHDR.MA_HQ.ToString()).InnerText
            'hdr.LOAI_TT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.LOAI_TT).InnerText
            hdr.LOAI_TT = v_nodeCTU_HDR.Item(0).SelectSingleNode(strXmlRoot + clsCTU.ColHDR.LOAI_TT.ToString()).InnerText
            'hdr.TEN_HQ = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TEN_HQ).InnerText
            hdr.TEN_HQ = v_nodeCTU_HDR.Item(0).SelectSingleNode(strXmlRoot + clsCTU.ColHDR.TEN_HQ.ToString()).InnerText
            'hdr.Ma_hq_ph = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_HQ_PH).InnerText
            hdr.Ma_hq_ph = v_nodeCTU_HDR.Item(0).SelectSingleNode(strXmlRoot + clsCTU.ColHDR.MA_HQ_PH.ToString()).InnerText
            'hdr.TEN_HQ_PH = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TEN_HQ_PH).InnerText
            hdr.TEN_HQ_PH = v_nodeCTU_HDR.Item(0).SelectSingleNode(strXmlRoot + clsCTU.ColHDR.TEN_HQ_PH.ToString()).InnerText

            '1)Neu o trang thai edit
            If (mblnEdit) Then  'Nếu sửa chứng từ, lấy bộ key cũ
                ' Ngay KB
                hdr.Ngay_KB = clsCTU.TCS_GetNgayLV(pv_intMaNV)
                ' Mã NV
                hdr.Ma_NV = pv_intMaNV
                ' Số bút toán
                hdr.So_BT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.So_BT).InnerText
                ' Mã điểm thu 02/10/2012-manhnv
                'If Not pv_intMaNV = 0 Then
                hdr.Ma_DThu = hdr.SHKB
                'End If
                ' Số CT
                hdr.So_CT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SoCT).InnerText
                ' Ky hieu CT
                'hdr.KyHieu_CT = clsCTU.TCS_GetKHCT(pv_intMaNV)

                hdr.TT_BDS = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TT_BDS).InnerText

                hdr.TT_NSTT = CTuCommon.GetNSTT(hdr.Ngay_KB, hdr.Ma_NV, hdr.So_CT, hdr.So_BT, hdr.Ma_DThu) 'v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TT_BDS).InnerText

                '2)Neu o trang thai insert moi
            Else
                'Nếu nhập mới chứng từ, tạo bộ key mới
                hdr.Ngay_KB = clsCTU.TCS_GetNgayLV(pv_intMaNV)
                Dim str_nam_kb As String
                Dim str_thang_kb As String
                str_nam_kb = Mid(hdr.Ngay_KB, 3, 2)
                str_thang_kb = Mid(hdr.Ngay_KB, 5, 2)
                ' Ngay KB


                ' Mã NV
                hdr.Ma_NV = pv_intMaNV
                ' Số bút toán
                hdr.So_BT = CTuCommon.Get_SoBT(clsCTU.TCS_GetNgayLV(pv_intMaNV), hdr.Ma_NV)

                ' Mã điểm thu 02/10/2012-manhnv
                'If Not pv_intMaNV = 0 Then
                '    hdr.Ma_DThu = clsCTU.TCS_GetMaDT_KB(pv_intMaNV)
                'End If
                ' Số CT
                hdr.So_CT = str_nam_kb + str_thang_kb + CTuCommon.Get_SoCT()
                ' Ky hieu CT
                '  hdr.KyHieu_CT = clsCTU.TCS_GetKHCT(pv_intMaNV)
                'Trang thai bds hien tai
                hdr.TT_BDS = gblnTTBDS

                hdr.TT_NSTT = "N"
            End If


            'Khoi tao phan HDR
            If Not v_nodeCTU_HDR Is Nothing Then

                'sonmt
                hdr.MA_HUYEN_NNT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_HUYEN).InnerText
                hdr.TEN_HUYEN_NNT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TEN_HUYEN).InnerText
                hdr.MA_TINH_NNT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_TINH).InnerText
                hdr.TEN_TINH_NNT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TEN_TINH).InnerText

                hdr.HUYEN_NNTHAY = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.HUYEN_NNTHAY).InnerText
                hdr.TINH_NNTHAY = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TINH_NNTHAY).InnerText

                'SHKB
                hdr.SHKB = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SHKB).InnerText
                ' Mã điểm thu
                hdr.Ma_DThu = clsCTU.TCS_GetMaDT(pv_intMaNV)
                ' Mã NNTien
                hdr.KyHieu_CT = clsCTU.TCS_GetKHCT(hdr.SHKB, pv_intMaNV, "04")
                hdr.Ma_NNTien = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MaNNTien).InnerText

                ' Tên NNTien
                hdr.Ten_NNTien = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TenNNTien).InnerText

                ' Địa chỉ NNTien
                hdr.DC_NNTien = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.DChiNNTien).InnerText

                ' Mã NNT
                hdr.Ma_NNThue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MaNNT).InnerText

                ' Tên NNT
                hdr.Ten_NNThue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TenNNT).InnerText
                ' Địa chỉ NNT
                hdr.DC_NNThue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.DChiNNT).InnerText

                If (hdr.DC_NNThue = "") Then hdr.DC_NNThue = ""

                ' Lý do
                hdr.Ly_Do = ""
                ' Mã TQ
                hdr.Ma_TQ = 0

                hdr.So_QD = ""
                hdr.Ngay_QD = "null"
                hdr.CQ_QD = ""

                ' Ngay CT
                hdr.Ngay_CT = clsCTU.TCS_GetNgayLV(pv_intMaNV)
                ' Ngay HT
                hdr.Ngay_HT = ToValue(Now.ToString("dd/MM/yyyy HH:mm:ss"), "DATE") 'GetDBSysDate() '

                hdr.Ma_CQThu = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MaCQThu).InnerText

                ' DBHC
                hdr.Ma_Xa = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MaDBHC).InnerText
                hdr.XA_ID = Get_XaID(hdr.Ma_Xa)
                hdr.Ma_Huyen = ""
                hdr.Ma_Tinh = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Tinh_NNTIEN).InnerText

                ' TK No
                hdr.TK_No = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TKNo).InnerText.Replace(".", "").Replace(" ", "")
                hdr.TEN_TK_NO = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TenTKNo).InnerText
                Dim v_strSHKB As String = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SHKB).InnerText
                Dim v_strMaDBHC As String = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MaDBHC).InnerText


                Dim arrTMP As ArrayList = CTuCommon.MapTK_Load(v_strMaDBHC, v_strSHKB, clsCTU.TCS_GetDBThu(pv_intMaNV), clsCTU.TCS_GetMaNHKB(pv_intMaNV))
                Dim v_strMaHTTT As String = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.HTTT).InnerText

                If v_strMaHTTT = "01" Then 'Chuyen khoan
                    hdr.TK_GL_NH = "NULL"
                Else
                    If arrTMP.Count > 0 Then
                        hdr.TK_GL_NH = arrTMP(3).ToString().Trim()
                    End If
                End If

                If arrTMP.Count > 0 Then
                    hdr.TK_KB_NH = arrTMP(4).ToString().Trim()
                Else
                    hdr.TK_KB_NH = ""
                End If
                hdr.Ghi_chu = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.GHI_CHU).InnerText
                ' TK Co
                hdr.TK_Co = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TKCo).InnerText.Replace(".", "").Replace(" ", "")
                hdr.TEN_TK_CO = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TenTKCo).InnerText
                ' So TKhai
                hdr.So_TK = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.ToKhaiSo).InnerText
                hdr.TEN_NH_B = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Ten_NH_B).InnerText
                hdr.TEN_NH_TT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TEN_NH_TT).InnerText
                ' Ngay TK
                strValue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.NgayDK).InnerText
                strNgay_DK = strValue
                If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
                    hdr.Ngay_TK = ToValue(strValue, "DATE")
                Else
                    hdr.Ngay_TK = "null"
                End If

                ' LHXNK
                If (v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.LHXNK).InnerText.Length > 0) Then
                    Dim strTenVT, strTenLH, strMaLH, strTen As String
                    strTenVT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.LHXNK).InnerText
                    strTen = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.DescLHXNK).InnerText

                    If (strTenVT = "" Or strTen = "") Then
                        hdr.LH_XNK = ""
                    Else
                        'Dim s As String()
                        's = strTen.Split("-")
                        strMaLH = strTenVT
                        strTenLH = strTen

                        If (Valid_LHXNK(strMaLH)) Then
                            hdr.LH_XNK = strMaLH
                        Else
                            CTuCommon.Get_LHXNK_byTenVT(strTenVT, strMaLH, strTenLH)
                            hdr.LH_XNK = strMaLH
                        End If

                        If (hdr.LH_XNK.Length > 20) Then
                            hdr.LH_XNK = hdr.LH_XNK.Substring(0, 20)
                        End If
                    End If
                Else
                    hdr.LH_XNK = ""
                End If

                ' DVSDNS
                hdr.DVSDNS = ""
                hdr.Ten_DVSDNS = ""

                ' Ma_NT
                'hdr.Ma_NT = "VND"
                ' Ti_gia
                'strValue = 'Trim(CType(grdHeader.Rows(RowHeader.TiGia).Cells.Item(1).Controls(0), TextBox).Text)
                ' TG_ID
                'hdr.TG_ID = "1062"

                'hdr.Ty_Gia = "1"

                hdr.Ma_NT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MaNT).InnerText
                hdr.Ty_Gia = ChungTu.buChungTu.get_tygia(hdr.Ma_NT, hdr.TG_ID)
                ' Mã Loại Thuế
                hdr.Ma_LThue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.LoaiThue).InnerText

                ' So Khung - So May
                hdr.So_Khung = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SoKhung).InnerText
                hdr.So_May = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SoMay).InnerText


                ' So_CT_NH
                hdr.So_CT_NH = ""  'v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.SoCTNH).InnerText



                If Not gblnTTBDS Is Nothing Then
                    If gblnTTBDS.ToUpper = "O" Then
                        hdr.Mode = "1"
                    Else 'If gblnTTBDS.ToUpper = "F" Then
                        hdr.Mode = "0"
                    End If
                Else
                    hdr.Mode = "0"
                End If


                ' TK_KH_KB
                'If Get_MaNH(hdr.Ma_Xa, clsCTU.TCS_GetMaDT_KB(hdr.SHKB), clsCTU.TCS_GetKHKB(pv_intMaNV)).Rows.Count > 0 Then
                '    hdr.MA_NH_A = Get_MaNH(hdr.Ma_Xa, clsCTU.TCS_GetMaDT_KB(hdr.SHKB), clsCTU.TCS_GetKHKB(pv_intMaNV)).Rows(0).Item("ma_nh_a").ToString
                '    hdr.MA_NH_B = Get_MaNH(hdr.Ma_Xa, clsCTU.TCS_GetMaDT_KB(hdr.SHKB), clsCTU.TCS_GetKHKB(pv_intMaNV)).Rows(0).Item("ma_nh_b").ToString
                'Else
                '    hdr.MA_NH_A = ""
                '    hdr.MA_NH_B = ""
                'End If
                'KIENVT : THEM THONG TIN VE NGAN HANG
                hdr.MA_NH_A = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_A).InnerText
                'hdr.MA_NH_B = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NH_B).InnerText
                ''MANHNV-01/10/12 Them NH TT
                'hdr.MA_NH_TT = v_nodeCTU_HDR.Item(0).SelectSingleNode(strXmlRoot + clsCTU.ColHDR.MA_NH_TT.ToString()).InnerText
                ' hoapt sua ma ngan hang truc tiep gian tiep
                Dim strMaNH As String = GetTT_NH(v_strSHKB)
                hdr.MA_NH_B = strMaNH.Split(";")(0).ToString
                hdr.MA_NH_TT = strMaNH.Split(";")(2).ToString
                'END
                ' TK_KH_NH 
                hdr.TK_KH_NH = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TK_KH_NH).InnerText.Replace("-", "")
                hdr.TEN_KH_NH = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TenTK_KH_NH).InnerText
                'If (IsNumeric(hdr.TK_KH_NH) = False) Then
                '    hdr.PT_TT = "03"
                'Else
                '    hdr.PT_TT = "01"
                'End If
                ''Phuong thuc thanh toan
                hdr.PT_TT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.HTTT).InnerText
                'Ngay_KH_NH
                strValue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.NGAY_KH_NH).InnerText
                hdr.NGAY_KH_NH = ToValue(strValue, "DATE")

                hdr.TK_KH_Nhan = ""
                hdr.Ten_KH_Nhan = ""
                hdr.Diachi_KH_Nhan = ""

                'THEM PHI GIAO DỊCH,VAT
                hdr.PHI_GD = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_GD).InnerText.Replace(".", "")
                hdr.PHI_VAT = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PHI_VAT).InnerText.Replace(".", "")
                hdr.PT_TINHPHI = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.PT_TINHPHI).InnerText.Replace(".", "")
                hdr.SAN_PHAM = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_SANPHAM).InnerText
                hdr.TT_CITAD = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TT_CITAD).InnerText
                ' quận_huyên NNTien
                hdr.QUAN_HUYEN_NNTIEN = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Huyen_NNTIEN).InnerText
                ' Tinh _NNTien
                hdr.TINH_TPHO_NNTIEN = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Tinh_NNTIEN).InnerText
                hdr.MA_NTK = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.MA_NTK).InnerText
                hdr.Dien_giai = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.DIEN_GIAI).InnerText
                Dim ref_no As String = String.Empty

                hdr.RM_REF_NO = hdr.So_CT
                hdr.REF_NO = hdr.So_CT
                hdr.TT_TThu = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.TTIEN_TN).InnerText
                ' So BK
                hdr.So_BK = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.So_BK).InnerText

                ' Ngay BK
                If (v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Ngay_BK).InnerText.Length > 0) Then
                    strValue = v_nodeCTU_HDR.Item(0).ChildNodes(clsCTU.ColHDR.Ngay_BK).InnerText
                    If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
                        hdr.Ngay_BK = ToValue(strValue, "DATE")
                    Else
                        hdr.Ngay_BK = "null"
                    End If
                Else
                    hdr.Ngay_BK = "null"
                End If

                hdr.Lan_In = "0" '?????

                hdr.Ma_KS = 0

                hdr.Trang_Thai = "00" '????
            End If

            v_nodeCTU_DTL = v_xmlDocument.SelectNodes("/CTC_CTU/CTU_DTL/ITEMS")
            Dim k As Integer = 0

            'Khoi tao phan DTL
            If Not v_nodeCTU_DTL Is Nothing Then
                ReDim Preserve dtls(v_nodeCTU_DTL.Count - 1)
                For i As Integer = 0 To v_nodeCTU_DTL.Count - 1 'v_nodeCTU_DTL.Item(0).ChildNodes.Count - 1
                    dtl = New NewChungTu.infChungTuDTL
                    'For j As Integer = 0 To v_nodeCTU_DTL.Item(i).ChildNodes.Count

                    'So tu tang
                    dtl.ID = getDataKey("TCS_CTU_DTL_SEQ.NEXTVAL")

                    ' SHKB
                    dtl.SHKB = hdr.SHKB

                    ' Ngày KB
                    dtl.Ngay_KB = hdr.Ngay_KB

                    ' Mã NV
                    dtl.Ma_NV = hdr.Ma_NV

                    ' Số Bút toán
                    dtl.So_BT = hdr.So_BT

                    ' Mã điểm thu
                    dtl.Ma_DThu = hdr.Ma_DThu

                    'Mã quỹ
                    dtl.MaQuy = v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.MaQuy).InnerText

                    ' Mã Cấp
                    dtl.Ma_Cap = ""

                    ' Mã Chương
                    dtl.Ma_Chuong = v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.CChuong).InnerText

                    ' CCH_ID
                    dtl.CCH_ID = Get_CCHID(dtl.Ma_Chuong)

                    ' Mã Loại
                    dtl.Ma_Loai = ""

                    ' Mã Khoản
                    dtl.Ma_Khoan = v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.LKhoan).InnerText

                    ' CCH_ID
                    dtl.LKH_ID = Get_LKHID(dtl.Ma_Khoan)

                    ' Mã Mục
                    dtl.Ma_Muc = ""

                    ' Mã TMục
                    dtl.Ma_TMuc = v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.MTieuMuc).InnerText

                    ' MTM_ID
                    dtl.MTM_ID = Get_MTMucID(dtl.Ma_TMuc)

                    ' Mã TLDT
                    dtl.Ma_TLDT = "null"
                    ' DT_ID
                    dtl.DT_ID = "null"

                    '  Nội dung
                    dtl.Noi_Dung = v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.NoiDung).InnerText

                    ' Tiền nguyên tệ
                    strValue = ""
                    If "".Equals(strValue) Then
                        strValue = "0"
                    End If
                    dtl.SoTien_NT = CDbl(strValue.Replace(" ", ""))
                    ' Tiền 
                    dtl.SoTien = StoN(v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.Tien).InnerText.Replace(".", ""))
                    dblTTien += dtl.SoTien

                    ' Kỳ thuế
                    dtl.Ky_Thue = RemoveApostrophe(v_nodeCTU_DTL.Item(i).ChildNodes(clsCTU.ColDTL.KyThue).InnerText)

                    ' Mã dự phòng
                    dtl.Ma_DP = "null"

                    'Next
                    If dtl.SoTien <> 0 Then
                        dtls(k) = dtl 'Kienvt : Chi insert nhung ban ghi co so tien <> 0
                    End If
                    k += 1
                Next
            End If

            ' Tiền thực thu
            hdr.TTien = dblTTien

            If (hdr.Ma_NT.ToUpper() <> "VND") Then
                hdr.TTien_NT = hdr.TTien / hdr.Ty_Gia
            Else
                hdr.TTien_NT = hdr.TTien
            End If

            ''Nếu không nhập số thực thu (chuyển khoản)
            'If (Not clsCTU.mblnNhapThucThu) Then
            '    hdr.Ma_TQ = hdr.Ma_NV

            'End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi XMLToBean", HttpContext.Current.Session("TEN_DN"))
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Throw ex
        End Try
    End Sub

    Private Shared Function GetDBSysDate() As String
        Dim strSQL As String = String.Empty
        strSQL = "SELECT SYSDATE FROM DUAL"
        Return DatabaseHelp.ExecuteToTable(strSQL).Rows(0)(0).ToString
    End Function

#End Region

#Region "Valid _DTL"

    Private Shared Function Valid_DTL(ByVal dtl As NewChungTu.infChungTuDTL, _
                ByVal fblnBatBuocMLNS As Boolean, _
                ByVal fblnVND As Boolean, _
                ByVal fblnBatBuocTLDT As Boolean) As Integer
        Try
            If (fblnBatBuocMLNS = True) Then
                If (Not Valid_MaQuy(dtl.MaQuy, True)) Then
                    'lblStatus.Text = "Thông tin 'Mã quỹ' không hợp lệ"
                    Return clsCTU.ColDTL.MaQuy
                End If
                'If (Not Valid_Chuong(dtl.Ma_Chuong, True)) Then
                'lblStatus.Text = "Thông tin 'Chương' không hợp lệ"
                'Return clsCTU.ColDTL.CChuong
                'End If
                If (Not Valid_Khoan(dtl.Ma_Khoan, True)) Then
                    'lblStatus.Text = "Thông tin 'Ngành kinh tế' không hợp lệ"
                    Return clsCTU.ColDTL.LKhoan
                End If
                If (Not Valid_TMuc(dtl.Ma_TMuc, True)) Then
                    'lblStatus.Text = "Thông tin 'Nội dung kinh tế' không hợp lệ"
                    Return clsCTU.ColDTL.MTieuMuc
                End If
                'If ((Not Valid_TLDT(dtl.Ma_TLDT, True)) And (fblnBatBuocTLDT = True)) Then
                '    lblStatus.Text = "Thông tin Tỷ lệ phân chia không hợp lệ"
                '    Return ColDTL.TLDT
                'End If
            End If

            If (dtl.SoTien = 0) Then
                'lblStatus.Text = "Số tiền không được để 0"
                If (fblnVND) Then
                    Return clsCTU.ColDTL.Tien
                    'Else
                    'Return ColDTL.TienNT
                End If
            End If
            'If (Not Valid_KyThue(dtl.Ky_Thue)) Then
            '    'lblStatus.Text = "Thông tin Kỳ thuế không hợp lệ (MM/yyyy)"
            '    Return clsCTU.ColDTL.KyThue
            'End If
            Return -1
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi valid chứng từ DTL", HttpContext.Current.Session("TEN_DN"))
            Return -1
        End Try
    End Function

    Private Shared Function Valid_DTL(ByVal dtl() As NewChungTu.infChungTuDTL, _
            ByVal fblnBatBuocMLNS As Boolean, _
            ByVal fblnVND As Boolean, _
            ByVal fblnBatBuocTLDT As Boolean) As Integer()
        Dim intReturn(1) As Integer
        Try
            Dim strMaQuy As String = ""
            Dim i, iCol As Integer
            For i = 0 To dtl.Length - 1
                iCol = Valid_DTL(dtl(i), fblnBatBuocMLNS, fblnVND, fblnBatBuocTLDT)
                If (iCol <> -1) Then
                    intReturn(0) = i
                    intReturn(1) = iCol
                    Return intReturn
                End If

                If (strMaQuy <> "") And (strMaQuy <> dtl(i).MaQuy) Then
                    'lblStatus.Text = "Chỉ cho phép 1 mã quỹ trên 1 chứng từ"
                    intReturn(0) = i
                    intReturn(1) = clsCTU.ColDTL.MaQuy
                    Return intReturn
                End If

                strMaQuy = dtl(i).MaQuy
            Next
            Return Nothing
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi valid dữ liệu dtl", HttpContext.Current.Session("TEN_DN"))
            Return Nothing
        End Try
    End Function
#End Region


#End Region

    'Lay danh sach chung tu Hai quan: trang thai 04
    Private Shared Function dtlCTU_Load(ByVal strUser As String, ByVal lNgayKB As Long, ByVal pv_strTrangThai As String, ByVal SoCT As String) As DataTable
        Dim dt As New DataTable
        Dim strSQL As String
        Dim strWhereSTT As String = ""
        Try
            Select Case pv_strTrangThai
                Case "02"
                    strWhereSTT = " AND TT_TTHU<>0 AND MA_TQ<>0"
                Case "01"
                    strWhereSTT = " AND TT_TTHU=0 "
            End Select

            If SoCT <> "" Then
                strWhereSTT = strWhereSTT & " AND So_CT='" & SoCT & "'"
            End If

            strSQL = "SELECT SHKB, KYHIEU_CT, SO_CT,Ma_NV, SO_BT,TRANG_THAI, So_BThu,TTien, TT_TTHU, NGAY_KB, MA_XA " & _
                    " FROM TCS_CTU_HDR  " & _
                    " WHERE NGAY_KB = " & lNgayKB & _
                        " AND TRANG_THAI='00' " & strWhereSTT & _
                        " AND ISACTIVE='1'" & _
                        " AND MA_LTHUE = '04'" & _
                        " AND Ma_NV=" & CInt(strUser) & _
                        " ORDER BY so_bt DESC "
            '  " AND MA_DTHU='" & clsCTU.TCS_GetMaDT(strUser) & "'" & _
            dt = DatabaseHelp.ExecuteToTable(strSQL)
            Return dt
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi load dữ liệu chứng từ", HttpContext.Current.Session("TEN_DN"))
            Return Nothing
        End Try
    End Function

    '    <System.Web.Services.WebMethod()> _
    '    Public Shared Function Update_CTU(ByVal pv_strMa_NV As String, ByVal pv_strSHKB As String, ByVal pv_strSoCT As String, _
    '                                          ByVal pv_strSoBT As String, ByVal tongtien As String, ByVal strChiTiet As String, _
    '                                          ByVal CheckEdit As Boolean) As String
    '        Try
    '            Dim strLoi As String = ""
    '            Dim strSQL As String
    '            'If HttpContext.Current.Session("User") Is Nothing Then
    '            '    Return "Bạn hãy thoát khỏi chương trình và đăng nhập lại"
    '            'End If
    '            'If CInt(HttpContext.Current.Session("User")) = 0 Then
    '            '    Return "Bạn hãy thoát khỏi chương trình và đăng nhập lại"
    '            'End If

    '            Dim kq As Integer
    '            Dim key As New KeyCTu
    '            key.Ma_Dthu = clsCTU.TCS_GetMaDT_KB(pv_strSHKB)
    '            key.Kyhieu_CT = clsCTU.TCS_GetKHCT(pv_strSHKB)
    '            key.Ma_NV = CInt(pv_strMa_NV)
    '            key.Ngay_KB = clsCTU.TCS_GetNgayLV(pv_strMa_NV)
    '            key.SHKB = pv_strSHKB
    '            key.So_BT = pv_strSoBT
    '            key.So_CT = pv_strSoCT

    '            If strChiTiet <> "" Then
    '                If CheckEdit = False Then

    '                    strSQL = "UPDATE tcs_ctu_hdr SET ma_tq=" & CInt(key.Ma_NV) & ",tt_tthu=" & Double.Parse(tongtien) & _
    '                           " WHERE shkb=" & Globals.EscapeQuote(key.SHKB) & _
    '                           " AND ngay_kb=" & key.Ngay_KB & _
    '                           " AND ma_nv=" & key.Ma_NV & _
    '                           " AND so_bt=" & key.So_BT & _
    '                           " AND ma_dthu=" & Globals.EscapeQuote(key.Ma_Dthu)
    '                    DatabaseHelp.Execute(strSQL)

    '                    strSQL = "INSERT INTO TCS_KHOQUY_HDR(SHKB,Ngay_KB,Ma_NV,So_BT,Ma_Dthu) values ('" & key.SHKB & "'," & key.Ngay_KB & "," & key.Ma_NV & _
    '                        "," & key.So_BT & ",'" & key.Ma_Dthu & "')"
    '                    kq = DatabaseHelp.Execute(strSQL)

    '                    If kq <= 0 Then
    '                        strLoi = "Nhập vào kho quỹ không thành công!"
    '                        Return strLoi
    '                    End If
    '                Else
    '                    strSQL = "DELETE FROM TCS_KHOQUY_DTL WHERE SO_CT='" & key.So_CT & "'" & _
    '                        " AND SHKB='" & clsCTU.TCS_GetKHKB(pv_strMa_NV) & "'AND NGAY_KB=" & key.Ngay_KB & " AND SO_BT='" & key.So_BT & "'"
    '                    kq = DatabaseHelp.Execute(strSQL)

    '                    If kq <= 0 Then
    '                        strLoi = "Nhập vào kho quỹ không thành công!"
    '                        Return strLoi
    '                    End If
    '                End If
    '                ' Insert vao bang detail
    '                Dim mangStr As String()
    '                Dim loaiGD, soTo As Integer
    '                Dim loaiTien As String
    '                mangStr = strChiTiet.Split("|")
    '                Dim mangTien As String()
    '                For i As Integer = 0 To mangStr.Length - 2
    '                    mangTien = mangStr(i).Split(":")
    '                    loaiTien = mangTien(0)
    '                    soTo = CInt(mangTien(1))
    '                    If soTo > 0 Then loaiGD = 1
    '                    If soTo < 0 Then
    '                        loaiGD = 2
    '                        soTo = Math.Abs(soTo)
    '                    End If
    '                    strSQL = "Insert Into TCS_KHOQUY_DTL(SHKB,Ngay_KB,Ma_NV,So_BT,Ma_Dthu,Kyhieu_CT,So_CT,Ma_LoaiTien,So_to,Loai_GD) values('" & key.SHKB & "'," & key.Ngay_KB & _
    '                                                "," & key.Ma_NV & "," & key.So_BT & ",'" & key.Ma_Dthu & _
    '                                                "','" & key.Kyhieu_CT & "','" & pv_strSoCT.ToString & "','" & loaiTien & _
    '                                                "'," & soTo & "," & loaiGD & ")"

    '                    kq = DatabaseHelp.Execute(strSQL)

    '                    If kq <= 0 Then
    '                        strLoi = "Nhập số thực thu không thành công!"
    '                        Return strLoi
    '                    End If
    '                Next
    '            End If

    '            'Thuc hien thanh cong thi goi lai script cap nhat so to
    '            strLoi = "Thực hiện thành công"
    '            Return strLoi
    '        Catch ex As Exception
    '            Throw ex
    '            Return "Nhập vào kho quỹ không thành công!"
    '        End Try
    '    End Function

    '    <System.Web.Services.WebMethod()> _
    '    Public Shared Function LoadSTT_CT(ByVal pv_strMa_NV As String, ByVal SoCT As String, ByVal SoBT As String) As String
    '        Dim strSQL As String
    '        Dim result As String = ""
    '        Dim dt As DataTable
    '        strSQL = "SELECT MA_LOAITIEN, SO_TO, LOAI_GD FROM TCS_KHOQUY_DTL WHERE SO_CT='" & SoCT & "'" & _
    '                 " AND SHKB='" & clsCTU.TCS_GetKHKB(pv_strMa_NV) & "'AND NGAY_KB=" & clsCTU.TCS_GetNgayLV(pv_strMa_NV) & " AND SO_BT='" & SoBT & "'"
    '        dt = DataAccess.ExecuteToTable(strSQL)
    '        If Not dt Is Nothing Then
    '            For i As Integer = 0 To dt.Rows.Count - 1
    '                result = result & dt.Rows(i).Item("MA_LOAITIEN").ToString & ":" & dt.Rows(i).Item("SO_TO").ToString & ":" & dt.Rows(i).Item("LOAI_GD").ToString & "/"
    '            Next
    '        End If
    '        Return result
    '    End Function

    '    '<System.Web.Services.WebMethod()> _
    '    'Public Shared Function Delete_STT(ByVal pv_strMa_NV As String, ByVal SoCT As String, ByVal SoBT As String) As String
    '    '    Dim strSQL As String
    '    '    Dim result As String = ""
    '    '    Dim dt As DataTable
    '    '    strSQL = "DELETE FROM TCS_KHOQUY_DTL WHERE SO_CT='" & SoCT & "'" & _
    '    '             " AND SHKB='" & clsCTU.TCS_GetKHKB(pv_strMa_NV) & "'AND NGAY_KB=" & gdtmNgayLV & " AND SO_BT='" & SoBT & "'"
    '    '    dt = DataAccess.ExecuteToTable(strSQL)
    '    '    If Not dt Is Nothing Then
    '    '        For i As Integer = 0 To dt.Rows.Count - 1
    '    '            result = result & dt.Rows(i).Item("MA_LOAITIEN").ToString & ":" & dt.Rows(i).Item("SO_TO").ToString & ":" & dt.Rows(i).Item("LOAI_GD").ToString & "/"
    '    '        Next
    '    '    End If
    '    '    Return result
    '    'End Function
    '#End Region

    Private Shared Function GetDate(ByVal strSHKB As String) As Long
        Try
            Dim strSQL As String
            Dim dt As DataTable
            strSQL = " SELECT TO_NUMBER (TO_CHAR (TO_DATE (giatri_ts, 'DD/MM/YYYY'), 'YYYYMMDD')) FROM tcs_thamso "
            strSQL += " WHERE ten_ts = 'NGAY_LV' AND ma_dthu = (SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS='" & strSHKB & "')"
            dt = DataAccess.ExecuteToTable(strSQL)
            If Not dt Is Nothing Then
                Return dt.Rows(0)(0)
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '    clsCommon.WriteLog(ex, "Lỗi lấy thông tin dữ liệu ngày", HttpContext.Current.Session("TEN_DN"))
            Return Long.Parse(DateTime.Now.ToString("yyyyMMdd"))
        End Try
    End Function
    Private Shared Function insertTK(ByVal strTK As String, ByVal strTenTK As String, ByVal strMaDBHC As String, ByVal strMaCQT As String, ByVal strTK_KB_NH As String, ByVal strSHKB As String) As Boolean
        Dim objTaiKhoan As New DanhMuc.TaiKhoan.buTaiKhoan()
        Dim infTK As New DanhMuc.TaiKhoan.infTaiKhoan()
        Dim daTK As New DanhMuc.TaiKhoan.daTaiKhoan
        infTK.TK = strTK
        infTK.TEN_TK = "Tài khoản thu kho bạc"
        infTK.MA_CQTHU = strMaCQT
        infTK.MA_DBHC = strMaDBHC
        infTK.TINH_TRANG = 1
        infTK.TK_KB_NH = strTK_KB_NH
        infTK.SHKB = strSHKB
        If daTK.CheckExistTK(infTK) = True Then
            Return True
        End If

        If daTK.Insert(infTK) Then
            Return True
        Else
            Return False
        End If

    End Function
    Private Function DM_MA_NH_A() As String
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable

        Dim strSQL As String = "SELECT ma_nh_chuyen,ten_nh_chuyen FROM tcs_nganhang_chuyen"
        dt = DataAccess.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrMA_NHA= new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrMA_NHA[" & i & "] ='" & dt.Rows(i)("ma_nh_chuyen") & ";" & dt.Rows(i)("ten_nh_chuyen") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    Private Function DM_PTTT() As String
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable

        Dim strSQL As String = "SELECT ma_pttt,ten_pttt FROM tcs_dm_pttt where status=1"
        dt = DataAccess.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrPTTT= new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrPTTT[" & i & "] ='" & dt.Rows(i)("ma_pttt") & ";" & dt.Rows(i)("ten_pttt") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty
    End Function
    'Protected Sub cmdInCT_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdInCT.Click

    '    Dim mv_strLoaiBaoCao As String = Report_Type.TCS_CTU_LASER
    '    Dim key As New KeyCTu
    '    Dim objHDR As New NewChungTu.infChungTuHDR
    '    key.So_CT = hdfDescCTU.Value.Split(":")(1).ToString
    '    key.SHKB = hdfDescCTU.Value.Split(":")(0).ToString
    '    key.Ngay_KB = ConvertDateToNumber(hdfDescCTU.Value.Split(":")(3).ToString)
    '    key.Ma_NV = mv_objUser.Ma_NV.ToString
    '    key.So_BT = hdfDescCTU.Value.Split(":")(2).ToString

    '    Dim hdr As New NewChungTu.infChungTuHDR
    '    hdr = Business.buChungTu.CTU_Header(key)
    '    Dim ds As DataSet = Business.buChungTu.CTU_IN_LASER(key)
    '    Dim frm As New infBaoCao
    '    frm.TCS_DS = ds

    '    frm.KHCT = hdr.KyHieu_CT
    '    frm.SoCT = hdr.So_CT
    '    frm.Ten_NNThue = hdr.Ten_NNThue

    '    ' Mã số thuế NNT: Mã NNT
    '    frm.Ma_NNThue = hdr.Ma_NNThue

    '    frm.DC_NNThue = hdr.DC_NNThue
    '    frm.Huyen_NNThue = GetQuanHuyenByXaID(hdr.Ma_DThu)
    '    frm.Tinh_NNThue = GetTinhThanhByMaXa(hdr.Ma_DThu)

    '    frm.Ten_NNTien = hdr.Ten_NNTien
    '    frm.Ma_NNTien = hdr.Ma_NNTien
    '    frm.DC_NNTien = hdr.DC_NNTien
    '    frm.Huyen_NNTien = ""
    '    frm.Tinh_NNTien = ""

    '    frm.NHA = "NH HÀNG HẢI - " & hdr.MA_NH_A.ToString
    '    '  frm.NHB = Get_TenNganHang(hdr.MA_NH_B)
    '    ' frm.TKNo = strFormatTK(hdr.TK_No)
    '    ' frm.TKCo = strFormatTK(hdr.TK_Co)

    '    frm.Ten_KB = Get_TenKhoBac(key.SHKB)
    '    frm.Ma_CQThu = hdr.Ma_CQThu
    '    frm.Ten_CQThu = Get_TenCQThu(hdr.Ma_CQThu)
    '    frm.TK_KH_NH = strFormatTK(hdr.TK_KH_NH)

    '    frm.So_TK = hdr.So_TK
    '    frm.Ngay_TK = hdr.Ngay_TK

    '    Dim strTenLH, strTenVT As String
    '    Get_LHXNK_ALL(hdr.LH_XNK, strTenLH, strTenVT)
    '    frm.LHXNK = strTenVT

    '    frm.So_BK = ""
    '    frm.Ngay_BK = ""
    '    frm.Tien_Bang_Chu = TCS_Dich_So(hdr.TTien, "đồng")
    '    frm.SHKB = hdr.SHKB
    '    frm.DBHC = hdr.Ma_Xa

    '    frm.Ten_ThuQuy = Get_HoTenNV(hdr.Ma_KS)
    '    frm.Ten_KeToan = Get_HoTenNV(hdr.Ma_NV)
    '    ' frm.Ten_KTT = gstrTen_KTT
    '    Dim strNgay() As String
    '    strNgay = ConvertNumbertoString(hdr.Ngay_CT).Split("/")

    '    frm.Ngay = GetString(strNgay(0))
    '    frm.Thang = GetString(strNgay(1))
    '    frm.Nam = GetString(strNgay(2))

    '    If (hdr.Ma_NT.ToUpper() = "VND") Then
    '        frm.IsTienNT = False
    '    Else
    '        frm.IsTienNT = True
    '    End If
    '    Session("objBaoCao") = frm 'Gọi form show báo cáo
    '    'clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx", "ManPower")
    '    ClientScript.RegisterStartupScript(Me.GetType(), "InitPage", "jsThem_Moi();", True)
    '    clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao, "ManPower")
    'End Sub
    'Private Function GetQuanHuyenByXaID(ByVal strMa_Dthu As String) As String

    '    Dim strSql As String = "SELECT ten FROM tcs_dm_xa where ma_xa= (select Giatri_ts from tcs_thamso where upper(ten_ts)='MA_DBHC' and ma_dthu ='" & strMa_Dthu & "')"
    '    Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
    '    If Not dt Is Nothing And dt.Rows.Count > 0 Then
    '        Return dt.Rows(0)("ten").ToString
    '    Else
    '        Return ""
    '    End If
    'End Function
    'Private Function GetTinhThanhByMaXa(ByVal strMa_Dthu As String) As String
    '    Dim strSql As String = "SELECT ten FROM tcs_dm_xa where ma_xa=(select Giatri_ts from tcs_thamso where upper(ten_ts)='MA_TTP' and ma_dthu ='" & strMa_Dthu & "')"
    '    Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
    '    If Not dt Is Nothing And dt.Rows.Count > 0 Then
    '        Return dt.Rows(0)("ten").ToString
    '    Else
    '        Return ""
    '    End If
    'End Function
End Class


