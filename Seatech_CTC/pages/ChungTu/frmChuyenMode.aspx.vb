﻿Imports System.Data
Imports System.Data.SqlClient
Imports VBOracleLib
Imports Business
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Partial Class pages_ChungTu_frmChuyenMode
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            cboBDS.SelectedValue = 1
            cboBDS.Enabled = False
            FillDatagrid()
            cmdChuyen.Attributes.Add("onClick", "return confirm('Bạn có thực sự muốn chuyển kiểu lập CT này không?')")
        End If
    End Sub

    Private Sub FillDatagrid()
        Dim strWhere As String = ""
        If txtSoCT.Text.Trim <> "" Then strWhere = strWhere + " AND c.SO_CT='" & txtSoCT.Text.Trim & "'"
        If txtMaNV.Text.Trim <> "" Then strWhere = strWhere + " AND n.ten_dn='" & txtMaNV.Text.Trim & "'"
        If ddlTrangThaiCT.SelectedValue.ToString <> "99" Then strWhere = strWhere + " AND c.TRANG_THAI=" & ddlTrangThaiCT.SelectedValue
        If cboBDS.SelectedValue.ToString <> "" Then strWhere = strWhere + " AND c.ISACTIVE=" & cboBDS.SelectedValue
        Dim dt As DataTable
        Dim strSQL As String
        strSQL = "SELECT C.SHKB,C.KYHIEU_CT, C.SO_CT,C.Ma_NV, C.SO_BT,C.TRANG_THAI, CASE C.ISACTIVE WHEN 0 THEN 'Offline' else 'Online' end BDS, C.TTien, C.NGAY_KB,N.TEN_DN" & _
             " FROM TCS_CTU_HDR C, tcs_DM_NHANVIEN N " & _
             " WHERE N.BAN_LV= C.MA_DTHU  AND N.MA_NV=C.MA_NV AND C.TRANG_THAI <>'01' AND C.TRANG_THAI<>'04' AND  C.MA_DTHU = " & clsCTU.TCS_GetMaDT(CType(Session("User"), Integer)) & strWhere & _
             " ORDER BY C.SO_CT "
        dt = DatabaseHelp.ExecuteToTable(strSQL)
        If Not dt Is Nothing Then
            gridData.DataSource = dt
            gridData.DataBind()
        End If

    End Sub

    Protected Sub cmdTim_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTim.Click
        FillDatagrid()
    End Sub

    Protected Sub cmdChuyen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyen.Click
        Dim SoCT As String
        Dim str As String = ""
        Dim TT As Integer
        Dim maNV As String
        Dim SOBT As String
        For i As Integer = 0 To gridData.Items.Count() - 1
            Dim chkSelect As CheckBox
            chkSelect = gridData.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                SoCT = Trim(gridData.Items(i).Cells(0).Text)
                SOBT = Trim(gridData.Items(i).Cells(2).Text)
                maNV = Trim(gridData.Items(i).Cells(5).Text)
                Try
                    If Trim(gridData.Items(i).Cells(4).Text).ToUpper = "OFFLINE" Then
                        TT = 1

                    ElseIf Trim(gridData.Items(i).Cells(4).Text).ToUpper = "ONLINE" Then
                        TT = 0
                        'Neu chuyen tu trang thai on sang off thi xoa het so thuc thu
                        str = " DELETE FROM TCS_KHOQUY_DTL WHERE SO_CT='" & SoCT & "' AND MA_NV='" & maNV & "' AND MA_DTHU='" & clsCTU.TCS_GetMaDT(CType(Session("User"), Integer)) & "'"
                        DataAccess.ExecuteNonQuery(str, CommandType.Text)
                        str = " DELETE FROM TCS_KHOQUY_HDR WHERE So_BT='" & SOBT & "' AND MA_NV='" & maNV & "' AND MA_DTHU='" & clsCTU.TCS_GetMaDT(CType(Session("User"), Integer)) & "'"
                        DataAccess.ExecuteNonQuery(str, CommandType.Text)
                    End If

                    ' Thiết lập trạng thái của lập chứng từ
                    str = " Update TCS_CTU_HDR SET ISACTIVE=" & TT & " WHERE SO_CT='" & SoCT & "'"


                    DataAccess.ExecuteNonQuery(str, CommandType.Text)
                Catch ex As Exception

                    'Lỗi xoá dữ liệu không thành công
                    clsCommon.ShowMessageBox(Me, "Không chuyển được kiểu lập chứng từ")                    
                    Return
                End Try

            End If

        Next
        clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
        FillDatagrid()
    End Sub

End Class
