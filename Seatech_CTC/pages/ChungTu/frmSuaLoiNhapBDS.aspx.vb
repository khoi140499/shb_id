﻿Imports System.Data
Imports System.Data.SqlClient
Imports VBOracleLib
Imports Business
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Diagnostics

Partial Class pages_ChungTu_frmSuaLoiNhapBDS
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            If (Not ClientScript.IsStartupScriptRegistered(Me.GetType(), "InitDefValues")) Then
                ClientScript.RegisterStartupScript(Me.GetType(), "InitDefValues", PopulateDefaultValues(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitPage", "jsKhoiTao();", True)
            End If
        End If
    End Sub

    Private Function PopulateDefaultValues() As String
        Dim myJavaScript As StringBuilder = New StringBuilder()
        myJavaScript.Append(" var curMa_NV ='" & CType(Session("User"), Integer).ToString & "';")
        Return myJavaScript.ToString
    End Function

    <System.Web.Services.WebMethod()> _
   Public Shared Function NhapBDS(ByVal pv_strSHKB As String, ByVal pv_strSoCT As String, _
                                     ByVal pv_strSoBT As String, ByVal strMaNV As String, _
                                     ByVal strMaDBHC As String, ByVal strTongTien As String, ByVal strTienTT As String) As String
        Dim v_blnReturn As Boolean = False

        Try
            Dim strLoi As String = ""
            Dim strTrangThai As String = "00"
            Dim v_strAppletContent As String
            If clsCTU.TCS_GetNgayLV(strMaNV) <> clsCTU.GetWorkingDate() Then
                strLoi = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới."
                Return strLoi
            End If

            Dim key As New Business.NewChungTu.KeyCTu
            key.Kyhieu_CT = clsCTU.TCS_GetKHCT(pv_strSHKB) 'clsCTU.TCS_GetKHCT(strMaNV)
            key.So_CT = pv_strSoCT
            key.SHKB = pv_strSHKB
            key.Ngay_KB = clsCTU.TCS_GetNgayLV(strMaNV)
            key.Ma_NV = CInt(strMaNV)
            key.So_BT = pv_strSoBT
            key.Ma_Dthu = clsCTU.TCS_GetMaDT(strMaNV)

            Dim v_strSeqNo As String = GetSeqNoBySoCT(pv_strSoCT, clsCTU.TCS_GetNgayLV(strMaNV))

            Dim glBranch As String = clsCTU.Get_GL_BranchCode(key.Ma_NV)
            Dim tellerId As String = clsCTU.Get_TellerID(key.Ma_NV)
            Dim amount As String = strTongTien
            Dim payAmount As String = strTienTT
            Dim cashDeno As String = ChungTu.buChungTu.CTU_GetCashNote(key)
            cashDeno = cashDeno.Replace("&&", "&")

            '1)Tai khoan cua kho bac tai nh                
            Dim arrTMP As ArrayList = CTuCommon.MapTK_Load(strMaDBHC, pv_strSHKB, clsCTU.TCS_GetDBThu(strMaNV), clsCTU.TCS_GetMaNHKB(strMaNV))
            Dim v_strTKCo As String = arrTMP(4).ToString().Trim()

            'Goi Webservice hach toan len host
            Dim v_objCoreBank As New clsCoreBankUtil
            Dim v_strRemark As String = "Nop thue bang tien mat ngay " & ConvertNumberToDate(clsCTU.TCS_GetNgayLV(strMaNV))

            'v_strAppletContent = v_objCoreBank.GetAppletPaymentMsg(tellerId, v_strSeqNo, payAmount, cashDeno, v_strTKCo, amount, v_strRemark)
            strLoi = v_strAppletContent
            Return strLoi

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi nhập thông tin BDS", HttpContext.Current.Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function

    <System.Web.Services.WebMethod()> _
  Public Shared Function LoadCTList(ByVal mv_strTrang_Thai As String, ByVal pv_strMaNV As String) As String
        Try
            Dim dtCT As DataTable
            dtCT = dtlCTU_Load(clsCTU.TCS_GetNgayLV(pv_strMaNV), mv_strTrang_Thai, pv_strMaNV)
            If Not dtCT Is Nothing Then
                Dim v_strBuildTable As String = ""
                v_strBuildTable &= "<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdDSCT' style='border-color:#989898;width:99%;border-collapse:collapse;'>"
                v_strBuildTable &= "<tr class='grid_header'><td style='width:90px;'>SHKB</td><td style='width:90px;'>ĐBHC</td><td style='width:90px;'>Số CT</td><td style='width:90px;'>Số BT</td><td style='width:100px;'>Mã NV</td><td style='width:100px;'>Mã NNT</td><td style='width:100px;'>Tên NNT</td></tr>"
                Dim i As Integer = 0
                For Each dr As DataRow In dtCT.Rows
                    v_strBuildTable &= "<tr onclick=jsGetCTU('" & dr("SHKB").ToString & "','" & dr("DBHC").ToString & "','" & _
                            dr("SO_CT").ToString & "','" & dr("SO_BT").ToString & "','" & dr("TTien").ToString & "','" & _
                            dr("TT_TTHU").ToString & "','" & dr("MA_NV").ToString & "') " & _
                            "class='" & IIf(i Mod 2 = 0, "grid_item", "grid_item_alter") & "'>"
                    v_strBuildTable &= "<td align='center'>" & dr("SHKB").ToString & "</td>"
                    v_strBuildTable &= "<td align='center'>" & dr("DBHC").ToString & "</td>"
                    v_strBuildTable &= "<td align='center'>" & dr("SO_CT").ToString & "</td>"
                    v_strBuildTable &= "<td align='center'>" & dr("SO_BT").ToString & "</td>"
                    v_strBuildTable &= "<td align='center'>" & dr("Ma_NV").ToString & "</td>"
                    v_strBuildTable &= "<td align='center'>" & dr("MA_NNTHUE").ToString & "</td>"
                    v_strBuildTable &= "<td align='center'>" & dr("TEN_NNTHUE").ToString & "</td>"
                    v_strBuildTable &= "</tr>"
                Next
                v_strBuildTable &= "</table>"
                Return v_strBuildTable
            Else
                Return String.Empty
            End If
            Return String.Empty
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '    clsCommon.WriteLog(ex, "Lỗi load thông tin chi tiết chứng từ", HttpContext.Current.Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function

    <System.Web.Services.WebMethod()> _
   Public Shared Function Chuyen_TT_CT(ByVal pv_strSHKB As String, ByVal pv_strSoCT As String, _
                                       ByVal pv_strSoBT As String, ByVal strMaNV As String, ByVal TT As String) As String
        Try
            Dim strLoi As String
            Dim key As New Business.NewChungTu.KeyCTu
            key.Kyhieu_CT = clsCTU.TCS_GetKHCT(pv_strSHKB) 'clsCTU.TCS_GetKHCT(strMaNV)
            key.So_CT = pv_strSoCT
            key.SHKB = pv_strSHKB
            key.Ngay_KB = clsCTU.TCS_GetNgayLV(strMaNV) 'gdtmNgayLV
            key.Ma_NV = CInt(strMaNV)
            key.So_BT = pv_strSoBT
            key.Ma_Dthu = clsCTU.TCS_GetMaDT(strMaNV)
            Dim v_strREFNo As String = ""
            Dim v_strSeqNo As String = ""

            Try
                ChungTu.buChungTu.CTU_SetTrangThai(key, TT)
                strLoi = "Thực hiện thành công!"
            Catch ex As Exception
                strLoi = "Thực hiện không thành công!"
            End Try
            Return strLoi
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '      clsCommon.WriteLog(ex, "Lỗi chuyển trạng thái chứng từ", HttpContext.Current.Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '    & "Error code: System error!" & vbNewLine _
            '    & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function

    Private Shared Function dtlCTU_Load(ByVal lNgayKB As Long, ByVal pv_strTrangThai As String, ByVal pv_strMaNV As String) As DataTable
        Dim dt As New DataTable
        Dim strWhereSTT As String = ""
        Dim strSQL As String
        Select Case pv_strTrangThai
            Case "06" '
                strWhereSTT = " AND TRANG_THAI='06'"
        End Select

        Try
            strSQL = "SELECT SHKB,KYHIEU_CT, SO_CT,MA_NNTHUE,TEN_NNTHUE,Ma_NV, SO_BT,TRANG_THAI, So_BThu, TTIEN, TT_TTHU, NGAY_KB,MA_XA DBHC" & _
              " FROM TCS_CTU_HDR  " & _
              " WHERE NGAY_KB = " & lNgayKB & _
              " AND MA_DTHU =" & clsCTU.TCS_GetMaDT(pv_strMaNV) & strWhereSTT & _
              " AND MA_NV IN(SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" & pv_strMaNV & "'))" & _
              " ORDER BY so_bt DESC "
            dt = DatabaseHelp.ExecuteToTable(strSQL)
            Return dt
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi load thông tin chi tiết chứng từ", HttpContext.Current.Session.Item("TEN_DN"))
            Return Nothing
        End Try
    End Function

    Private Shared Function GetSeqNoBySoCT(ByVal pv_strSoCT As String, ByVal strNgayLV As String) As String
        Dim dt As New DataTable
        Dim strSQL As String = String.Empty
        Try
            strSQL = "SELECT SEQ_NO FROM TCS_CTU_HDR WHERE SO_CT='" & pv_strSoCT & "' AND NGAY_KB=" & strNgayLV
            dt = DatabaseHelp.ExecuteToTable(strSQL)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0)(0).ToString
            End If
            Return String.Empty
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '     clsCommon.WriteLog(ex, "Lỗi thông tin số chứng từ", HttpContext.Current.Session.Item("TEN_DN"))
            Return String.Empty
        End Try
    End Function
End Class
