﻿Imports Business.XLCuoiNgay
Imports Business.ChungTu
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data

Partial Class pages_ChungTu_frmKTDuLieu
    Inherits System.Web.UI.Page

    Dim strDK As String

    Private Sub frmKTDuLieu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Try
                txtTuNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User")))
                txtDenNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User")))
            Catch ex As Exception
                lblError.Text = ex.Message
            End Try
        End If
    End Sub

    Protected Sub cmdExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        Response.Redirect("../frmHomeNV.aspx", False)
    End Sub

    Private Function strWheres() As String
        Dim strResult As String = ""
        Dim strTK() As String
        Dim strValue As String = ""
        strResult = " And (Hdr.ngay_kb>=" & ConvertDateToNumber(txtTuNgay.Text.Replace("-", "/")) & ") " & _
                    " And (Hdr.ngay_kb<=" & ConvertDateToNumber(txtDenNgay.Text.Replace("-", "/")) & ") "
        If Not "".Equals(txtTKNo.Text.Trim) Then
            txtTKNo.Text = txtTKNo.Text.Replace("'", "")
            txtTKNo.Text = txtTKNo.Text.Replace(",", ";")
            strTK = txtTKNo.Text.Split(";")
            strResult &= " And ((1<>1) "
            For Each strValue In strTK
                strResult &= " Or (Hdr.Tk_No like '" & strValue & "%') "
            Next
            strResult &= ") "
        End If
        If Not "".Equals(txtTKCo.Text.Trim) Then
            txtTKCo.Text = txtTKCo.Text.Replace("'", "")
            txtTKCo.Text = txtTKCo.Text.Replace(",", ";")
            strTK = txtTKCo.Text.Split(";")
            strResult &= " And ((1<>1) "
            For Each strValue In strTK
                strResult &= " Or (Hdr.Tk_Co like '" & strValue & "%') "
            Next
            strResult &= ") "
        End If
        If chkDBTK.Checked Then
            strDK = "DBHC" 'Kiểm tra cho DBHC-TK_Co-CQThu
        End If
        If chkMLNS.Checked Then
            strDK = "MLNS" 'Kiểm tra cho MLNS
        End If
        If chkDBTK.Checked And chkMLNS.Checked Then
            strDK = "ALL" 'Kiểm tra cho cả 2 trường hợp
        End If
        Return strResult
    End Function

    Private Sub cmdKiemTra_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKiemTra.Click
        Try
            Dim ds As New DataSet

            'CheckNgayThang            
            If Not clsCommon.CompareDate(txtTuNgay.Text, txtDenNgay.Text) Then
                clsCommon.ShowMessageBox(Me, "Từ ngày phải nhỏ hơn đến ngày.")
                txtTuNgay.Focus()
                Exit Sub
            End If

            If Not chkDBTK.Checked And Not chkMLNS.Checked Then
                clsCommon.ShowMessageBox(Me, "Bạn phải  chọn ít nhất một kiểu kiểm tra.")
                chkDBTK.Focus()
                Exit Sub
            End If
            'ds = buKiemTraCN.lstHeaderCT(strWheres(), strDK)

            ds = buKiemTraCN.lstHeaderCT(strWheres(), strDK)
            If IsEmptyDataSet(ds) Then
                clsCommon.ShowMessageBox(Me, "Không tìm thấy chứng từ sai với các điều kiện trên.")
                txtTuNgay.Focus()
                Exit Sub
            Else
                'Bind vao grid cac du lieu sai
                Me.dtg.DataSource = ds
                Me.dtg.DataBind()
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình kiểm tra chứng từ.")
        End Try
    End Sub


End Class
