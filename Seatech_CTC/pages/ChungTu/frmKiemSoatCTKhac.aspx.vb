﻿Imports Business
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports System.Diagnostics
Imports System.Xml.XmlDocument
Imports Business.NewChungTu
Imports System.Xml
Imports Business.CTuCommon
Imports log4net
Imports Business.BaoCao
Imports System.IO
Imports ProcMegService
Imports CorebankServiceESB


Partial Class pages_ChungTu_frmKiemSoatCTKhac
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private mv_objUser As New CTuUser
    Private check_TTTT As String = "1"
    Private illegalChars As String = "[&]"
    Private Shared cls_corebank As New CorebankServiceESB.clsCoreBank
    Private Shared CORE_ON_OFF As String = ConfigurationManager.AppSettings.Get("CORE.ON").ToString()
    Private Shared CORE_VERSION As String = ConfigurationManager.AppSettings.Get("CORE.VERSION").ToString()



    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            CreateDumpChiTietCTGrid() 'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu
            ClearCTHeader() 'Xóa thông tin ở grid header
            ShowHideCTRow(False)
            lblStatus.Text = "Hãy chọn một chứng từ cần kiểm soát"
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'BEGIN-TYNK CHECK NGAY NGHI
        'If Not Business.CTuCommon.GetNgayNghi() Then
        '    ' clsCommon.ShowMessageBox(Me, "Hôm nay là ngày nghỉ. Bạn không được vào chức năng này!!!")
        '    Response.Redirect("~/pages/frmHomeNV.aspx?CheckNN=1", False)
        '    Exit Sub
        'End If
        'END TYNK CHECK NGAY NGHI
        'ClientScript.RegisterStartupScript(Me.GetType(), "jsProcessKS", "jsProcessKS('OK');", True)
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            'RemoveHandler cmdKS.Click, AddressOf cmdKS_Click
            RemoveHandler cmdChuyenThue.Click, AddressOf cmdChuyenThue_Click
            RemoveHandler cmdHuy.Click, AddressOf cmdHuy_Click
            RemoveHandler cmdChuyenTra.Click, AddressOf cmdChuyenTra_Click
            Exit Sub
        End If
        'Kiểm tra trạng thái session
        If Not Session.Item("User") Is Nothing Then
            mv_objUser = New CTuUser(Session.Item("User"))
        Else
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        'Check các quyền của user

        If Not IsPostBack Then
            'Load danh sách chứng từ thành lập bởi điểm thu
            LoadDSCT("99")
        End If
        ' OnClientClick = "disableButton();"
        'cmdKS.Attributes.Add("onclick", "javascript:disableButton() ;" + Page.ClientScript.GetPostBackEventReference(cmdKS, "").ToString())
        'cmdChuyenTra.Attributes.Add("onclick", "javascript:disableButton() ;" + Page.ClientScript.GetPostBackEventReference(cmdChuyenTra, "").ToString())
        'cmdHuy.Attributes.Add("onclick", "this.disabled=true;" + Page.ClientScript.GetPostBackEventReference(cmdHuy, "").ToString())
    End Sub

    Private Sub CreateDumpChiTietCTGrid()
        'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu
        Dim v_dt As New DataTable()
        v_dt.Columns.Add(New DataColumn("MAQUY"))
        v_dt.Columns.Add(New DataColumn("MA_CHUONG"))
        v_dt.Columns.Add(New DataColumn("MA_KHOAN"))
        v_dt.Columns.Add(New DataColumn("MA_TMUC"))
        v_dt.Columns.Add(New DataColumn("NOI_DUNG"))
        v_dt.Columns.Add(New DataColumn("TTIEN"))
        v_dt.Columns.Add(New DataColumn("SODATHUTK"))
        v_dt.Columns.Add(New DataColumn("KY_THUE"))


        grdChiTiet.Controls.Clear()
        Dim v_dumpRow As DataRow = v_dt.NewRow
        v_dt.Rows.Add(v_dumpRow)
        grdChiTiet.DataSource = v_dt
        grdChiTiet.DataBind()
    End Sub

    Private Sub CreateDumpDSCTGrid()
        'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu
        Dim v_dt As New DataTable()
        v_dt.Columns.Add(New DataColumn("SHKB"))
        v_dt.Columns.Add(New DataColumn("NGAY_KB"))
        v_dt.Columns.Add(New DataColumn("SO_CT"))
        v_dt.Columns.Add(New DataColumn("KYHIEU_CT"))
        v_dt.Columns.Add(New DataColumn("SO_BT"))
        v_dt.Columns.Add(New DataColumn("MA_DTHU"))
        v_dt.Columns.Add(New DataColumn("MA_NV"))
        v_dt.Columns.Add(New DataColumn("TRANG_THAI"))
        v_dt.Columns.Add(New DataColumn("TT_CTHUE"))
        v_dt.Columns.Add(New DataColumn("TEN_DN"))
        v_dt.Columns.Add(New DataColumn("so_ct_nh"))
        v_dt.Columns.Add(New DataColumn("Ma_KS"))
        v_dt.Columns.Add(New DataColumn("SAVE_TYPE"))
        grdDSCT.Controls.Clear()
        'Dim v_dumpRow As DataRow = v_dt.NewRow
        'v_dt.Rows.Add(v_dumpRow)
        grdDSCT.DataSource = v_dt
        grdDSCT.DataBind()
    End Sub

    Private Sub ClearCTHeader()
        txtTenTKCo.Text = ""
        txtSHKB.Text = ""
        txtTenKB.Text = ""
        txtNGAY_KH_NH.Text = ""
        txtTKCo.Text = ""
        txtTKNo.Text = ""
        txtMaDBHC.Text = ""
        txtTenDBHC.Text = ""
        txtMaNNT.Text = ""
        txtTenNNT.Text = ""
        txtDChiNNT.Text = ""
        txtMaNNTien.Text = ""
        txtTenNNTien.Text = ""
        txtDChiNNT.Text = ""
        txtMaCQThu.Text = ""
        txtTenCQThu.Text = ""
        'ddlLoaiThue.SelectedIndex = 0
        txtDescLoaiThue.Text = ""
        txtLHXNK.Text = ""
        txtDescLHXNK.Text = ""
        txtSoKhung.Text = ""
        txtSoMay.Text = ""
        txtToKhaiSo.Text = ""
        txtMaNT.Text = ""
        txtTenNT.Text = ""
        txtNgayDK.Text = ""
        'txtSo_BK.Text = ""
        'txtNgay_BK.Text = ""
        ddlHTTT.SelectedIndex = 0
        txtTK_KH_NH.Text = ""
        txtTenTK_KH_NH.Text = ""
        txtNGAY_KH_NH.Text = ""
        txtMA_NH_A.Text = ""
        txtTen_NH_A.Text = ""
        txtMA_NH_B.Text = ""
        txtTen_NH_B.Text = ""
        txtMA_NH_TT.Text = ""
        txtTEN_NH_TT.Text = ""
        txtSoCT_NH.Text = ""
        hdfTrangThai_CThue.Value = ""
        hdfKyHieu_CT.Value = ""
        hdfMa_Dthu.Value = ""
        hdfSHKB.Value = ""
        hdfTrangThai_CT.Value = ""
        hdfTrangThai_CThue.Value = ""
    End Sub

    Private Sub LoadDSCT(ByVal pv_strTrangThaiCT As String)
        Try
            Dim dsCT As DataSet = GetDSCT(pv_strTrangThaiCT)
            If (Not dsCT Is Nothing) And (dsCT.Tables(0).Rows.Count > 0) Then
                Me.grdDSCT.DataSource = dsCT
                Me.grdDSCT.DataBind()

            Else
                CreateDumpDSCTGrid()
            End If
        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub

    'Lay danh sach chung tu THUE
    Private Function GetDSCT(ByVal pv_strTrangThaiCT As String) As DataSet
        Try
            If pv_strTrangThaiCT.Equals("99") Then pv_strTrangThaiCT = ""
            'Load danh sách chứng từ thành lập bởi điểm thu
            'Trường hợp thu hộ thì chứng từ sẽ có maDT <> maDT ban đầu của giao dịch viên
            'trong trường hợp này mv_strUserKS<>"" có nghĩa là lấy tất cả chưng từ mà giao dịch viên
            'thực hiện không phân biệt theo mã DT nữa
            'If (Not mv_objUser.Ngay_LV Is Nothing) And (mv_objUser.Ngay_LV.Length > 0) Then
            ''Dim dsCT As DataSet = ChungTu.buChungTu.CTU_GetListKiemSoat_Set(mv_objUser.Ngay_LV, pv_strTrangThaiCT, _
            ''                      mv_objUser.Ban_LV, "", mv_objUser.Ma_NV, "")

            'mv_objUser.Ban_LV="" cho phep lay chung tu thu ho
            Dim dsCT As DataSet = ChungTu.buChungTu.CTU_GetListKiemSoat(mv_objUser.Ngay_LV, pv_strTrangThaiCT, _
                                  "", "01", "", mv_objUser.Ma_NV, "")
            Return dsCT
        Catch ex As Exception
            logger.Error(ex.StackTrace)
            Return Nothing
        End Try
    End Function

    Private Sub LoadCTChiTietAll(ByVal key As KeyCTu)
        Try
            Dim v_strTT As String = String.Empty
            Dim v_strTTThue As String = String.Empty
            Dim v_strLoaiThue As String = String.Empty
            Dim v_strPhuongThucTT As String = String.Empty
            Dim v_str_Ma_NNT As String = String.Empty

            'Load chứng từ header
            Dim dsCT As New DataSet
            dsCT = ChungTu.buChungTu.CTU_Header_Set(key)
            If (dsCT.Tables(0).Rows.Count > 0) Then
                ShowHideCTRow(True)
                FillDataToCTHeader(dsCT)
                v_strTT = dsCT.Tables(0).Rows(0)("TRANG_THAI").ToString
                v_strLoaiThue = dsCT.Tables(0).Rows(0)("MA_LTHUE").ToString
                v_strPhuongThucTT = dsCT.Tables(0).Rows(0)("PT_TT").ToString
                v_str_Ma_NNT = dsCT.Tables(0).Rows(0)("Ma_NNThue").ToString
                v_strTTThue = dsCT.Tables(0).Rows(0)("TT_CTHUE").ToString
            Else
                lblStatus.Text = "Không tìm thấy chứng từ cần tìm.Hãy thử lại"
                Exit Sub
            End If

            'Load thông tin chi tiết
            Dim dsCTChiTiet As New DataSet
            dsCTChiTiet = ChungTu.buChungTu.CTU_ChiTiet_Set(key)
            If dsCTChiTiet.Tables(0).Rows.Count > 0 Then
                FillDataToCTDetail(dsCTChiTiet, v_str_Ma_NNT)
                CaculateTongTien(dsCTChiTiet.Tables(0))
            Else
                lblStatus.Text = "Không lấy được thông tin chi tiết của chứng từ.Hãy kiểm tra lại"
                Exit Sub
            End If
            hdfTrangThai_CT.Value = v_strTT
            ShowHideButtons(v_strTT, v_strTTThue)
            ShowHideControlByLoaiThue(v_strLoaiThue)
            ShowHideControlByPT_TT(v_strPhuongThucTT, v_strTT)
            'Me.txtMatKhau.Focus()
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())


            'Throw ex
        Finally
            'Catch ex As Exception
            '    logger.Error(ex.StackTrace)
        End Try
    End Sub

    Private Sub FillDataToCTHeader(ByVal dsCT As DataSet)
        Try
            Dim strTenVT = String.Empty
            Dim strTenLH As String = String.Empty
            Dim strMaLH As String = String.Empty
            Dim dr As DataRow = dsCT.Tables(0).Rows(0)
            Dim v_strTrangThai As String = dr("TRANG_THAI").ToString()
            Dim v_strHTTT As String = GetString(dr("PT_TT")).ToString
            txtSHKB.Text = dr("SHKB").ToString()
            txtTenKB.Text = dr("TEN_KB").ToString()
            txtNGAY_KH_NH.Text = ConvertNumbertoString(dr("NGAY_KB").ToString())
            txtTKCo.Text = dr("TK_Co").ToString()
            txtTKNo.Text = dr("TK_No").ToString()
            txtMaDBHC.Text = dr("MA_XA").ToString()
            txtTenDBHC.Text = dr("TENXA").ToString()
            txtMaNNT.Text = dr("MA_NNTHUE").ToString()
            txtTenNNT.Text = dr("TEN_NNTHUE").ToString()
            txtDChiNNT.Text = dr("DC_NNTHUE").ToString()
            txtMaNNTien.Text = dr("Ma_NNTien").ToString()
            txtTenNNTien.Text = dr("TEN_NNTIEN").ToString()
            txtDChiNNTien.Text = dr("DC_NNTIEN").ToString()
            txtMaCQThu.Text = dr("MA_CQTHU").ToString()
            txtTenCQThu.Text = dr("TEN_CQTHU").ToString()
            Dim dt As New DataTable
            dt.Columns.Add("ID_HQ", Type.GetType("System.String"))
            dt.Columns.Add("Name", Type.GetType("System.String"))
            dt.Columns.Add("Value", Type.GetType("System.String"))
            dt.Columns(0).AutoIncrement = True
            Dim R As DataRow = dt.NewRow
            R("ID_HQ") = dr("MA_LTHUE").ToString()
            R("Name") = GetTenLoaiThue(dr("MA_LTHUE").ToString()) 'dr("MA_LTHUE").ToString() & "-" & GetTenLoaiThue(dr("MA_LTHUE").ToString())
            R("Value") = dr("MA_LTHUE").ToString()
            dt.Rows.Add(R)
            ddlLoaiThue.DataSource = dt
            ddlLoaiThue.DataTextField = "Name"
            ddlLoaiThue.DataValueField = "Value"
            ddlLoaiThue.DataBind()

            'txtDescLoaiThue.Text = GetTenLoaiThue(dr("MA_LTHUE").ToString())
            strMaLH = GetString(dr("LH_XNK").ToString())
            Get_LHXNK_ALL(strMaLH, strTenLH, strTenVT)
            txtLHXNK.Text = strMaLH
            txtDescLHXNK.Text = strTenVT & "-" & strTenLH
            txtSoKhung.Text = GetString(dr("SO_KHUNG").ToString())
            txtSoMay.Text = GetString(dr("SO_MAY").ToString())
            txtToKhaiSo.Text = GetString(dr("SO_TK").ToString())
            txtNgayDK.Text = GetString(dr("NGAY_TK"))
            'txtSo_BK.Text = GetString(dr("So_BK"))
            'txtNgay_BK.Text = GetString(dr("Ngay_BK"))
            ddlHTTT.SelectedValue = v_strHTTT
            ddlHTTT.SelectedItem.Text = Get_TenPTTT(v_strHTTT)
            rowTKGL.Visible = False
            'If v_strHTTT = "03" Then
            '    rowTKGL.Visible = True
            '    rowSoDuNH.Visible = False
            '    rowTK_KH_NH.Visible = False
            '    txtTKGL.Text = GetString(dr("TK_KH_NH"))
            '    txtTenTKGL.Text = dr("TEN_KH_NH").ToString
            'End If
            If dr("ma_ntk").ToString = "1" Then
                txtTenTKCo.Text = "Tài khoản thu NSNN"
            ElseIf dr("ma_ntk").ToString = "2" Then
                txtTenTKCo.Text = "Tài khoản tạm thu"
            Else
                txtTenTKCo.Text = "Thu hoàn thuế GTGT"
            End If

            rowLHXNK.Visible = False

            txtTK_KH_NH.Text = GetString(dr("TK_KH_NH"))
            txtTenTK_KH_NH.Text = dr("TEN_KH_NH").ToString
            txtNGAY_KH_NH.Text = GetString(dr("NGAY_KH_NH"))
            txtMA_NH_A.Text = GetString(dr("MA_NH_A"))
            txtTen_NH_A.Text = GetString(dr("TEN_NH_A"))
            txtMA_NH_B.Text = GetString(dr("MA_NH_B"))
            txtTen_NH_B.Text = GetString(dr("TEN_NH_B"))
            txtMA_NH_TT.Text = GetString(dr("MA_NH_TT"))
            txtTEN_NH_TT.Text = GetString(dr("TEN_NH_TT"))
            txtMaNT.Text = GetString(dr("Ma_NT"))
            txtTenNT.Text = GetString(dr("Ten_NT"))
            txtTimeLap.Text = GetString(dr("ngay_ht"))
            txtSO_QD.Text = GetString(dr("so_qd"))
            txtCQ_QD.Text = GetString(dr("cq_qd"))
            txtNGAY_QD.Text = GetString(dr("ngay_qd"))
            ' txtTenTKNo.Text = dr("TEN_TKNO").ToString()
            ' txtTenTKCo.Text = ""
            'tynk-comment phí và VAT - 08-04-2013*********
            Dim strPTTP As String = ""
            strPTTP = dr("pt_tinhphi").ToString()

            txtCharge.Text = VBOracleLib.Globals.Format_Number(dr("PHI_GD").ToString(), ".")
            txtVAT.Text = VBOracleLib.Globals.Format_Number(dr("PHI_VAT").ToString(), ".")
            '*********************************************
            txtRM_REF_NO.Text = dr("RM_REF_NO").ToString()
            txtREF_NO.Text = dr("REF_NO").ToString()
            txtSoCT_NH.Text = dr("So_CT_NH").ToString()
            txtTenTinhKB.Text = dr("ten_tinh").ToString()
            txtTinhKB.Text = dr("ma_tinh").ToString()
            txtKHCT.Text = dr("KyHieu_CT").ToString()
            txtSoCT.Text = dr("So_CT").ToString()
            If dr("LY_DO_HUY").ToString().Trim <> "" Then
                drpLyDoHuy.SelectedValue = dr("LY_DO_HUY").ToString().Trim
            End If
            hdfTrangThai_CT.Value = dr("trang_thai").ToString().Trim
            hdfTrangThai_CThue.Value = dr("tt_cthue").ToString().Trim
            hdfMACN.Value = dr("ma_cn").ToString().Trim
            txtSanPham.Text = dr("ma_sanpham").ToString()
            txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number(dr("tt_tthu").ToString().Trim, ".")
            If dr("so_ct_nh").ToString() <> "" Then
                lbSoFT.Text = "Số FT:" & dr("so_ct_nh").ToString()
            Else
                lbSoFT.Text = ""
            End If
            txtGhiChu.Text = dr("dien_giai").ToString().Trim
            txtQuanNNThue.Text = dr("MAHUYEN_NNT").ToString().Trim
            txtTenQuanNNThue.Text = dr("TENHUYEN_NNT").ToString().Trim
            txtTinhNNThue.Text = dr("MATINH_NNT").ToString().Trim
            txtTenTinhNNThue.Text = dr("TENTINH_NNT").ToString().Trim

            txtQuan_HuyenNNTien.Text = dr("tenhuyen_nnthay").ToString().Trim
            'txtQuan_HuyenNNTien.Text = dr("TENHUYEN_NNT").ToString().Trim
            txtTinh_TphoNNTien.Text = dr("tentinh_nnthay").ToString().Trim
            ' txtTenTinhNNThue.Text = dr("TENTINH_NNT").ToString().Trim

            txtSO_QD.Text = dr("So_QD").ToString().Trim
            txtCQ_QD.Text = dr("CQ_QD").ToString().Trim
            txtNGAY_QD.Text = dr("Ngay_QD").ToString().Trim
            txtSoKhung.Text = dr("So_Khung").ToString().Trim
            txtSoMay.Text = dr("So_May").ToString().Trim

            'sonmt
            txtMaLoaiNNT.Text = dr("LOAI_NNT").ToString()
            txtTenLoaiNNT.Text = dr("TEN_LOAI_NNT").ToString()

        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình FillDataToCTHeader")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())


            ' Throw ex
        Finally
        End Try
    End Sub
    Private Sub FillDataToCTDetail(ByVal pv_dsCT As DataSet, ByVal v_strMa_NNT As String)
        Try
            Dim v_dt As DataTable = pv_dsCT.Tables(0)
            Dim dr As DataRow
            Dim sodathu As String
            grdChiTiet.DataSource = v_dt
            grdChiTiet.DataBind()
            For i As Integer = 0 To v_dt.Rows.Count - 1
                dr = v_dt.Rows(i)
                sodathu = clsCTU.TCS_CalSoDaThu(v_strMa_NNT, dr("MaQuy").ToString(), dr("Ma_Chuong").ToString(), dr("Ma_Khoan").ToString(), dr("Ma_TMuc").ToString(), dr("Ky_Thue").ToString())
                grdChiTiet.Items(i).Cells(7).Text = sodathu
                grdChiTiet.Items(i).Cells(7).Enabled = False
                CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text = VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text, ".")
            Next
        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function ValidNHSHB_SP(ByVal strSHKB As String, ByVal strMa_NH_B As String, ByVal strLanKS As String, ByVal strSo_ct As String) As String
        Dim strResult As String = ""
        Dim dt As New DataTable
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If
        Dim cls_songphuong As New CorebankServiceESB.clsSongPhuong

        strResult = cls_songphuong.ValiadSHB(strSHKB)

        Dim strNHB As String = ""
        If strResult.Length > 8 Then
            Dim strarr() As String
            strarr = strResult.Split(";")
            strNHB = strarr(0).ToString
        End If


        Dim strClick As String = "0"
        strClick = ChungTu.buChungTu.CTU_Get_Valid_LanKS(strSo_ct)

        If strClick.Length > 0 Then

        Else
            strClick = "0"
        End If

        If strNHB.Length >= 8 Then
            If strNHB.Equals(strMa_NH_B) Then
                Return "TRUE;" & strClick
            Else
                Return "FALSE;" & strClick
            End If
        Else
            Return "TRUE;" & strClick
        End If

        'Return "FALSE"
    End Function

    Private Sub CaculateTongTien(ByVal pv_dt As DataTable)
        'Tính toán và cập nhật lại trường tổng tiền
        Dim dblTongTien As Double = 0
        Dim dblTongTrichNo As Double = 0
        For i As Integer = 0 To pv_dt.Rows.Count - 1
            dblTongTien += pv_dt.Rows(i)("TTIEN")
        Next
        'tynk-commennt phí và VAT-08-04-2013*******
        Dim dblCharge As Double = 0 'CDbl(IIf((txtCharge.Text.Replace(".", "").Length > 0), txtCharge.Text.Replace(".", ""), "0"))
        Dim dblVAT As Double = 0 'CDbl(IIf((txtVAT.Text.Replace(".", "").Length > 0), txtVAT.Text.Replace(".", ""), "0"))
        '******************************************
        txtTTIEN.Text = VBOracleLib.Globals.Format_Number(dblTongTien, ".")
        dblTongTrichNo = dblTongTien + dblCharge + dblVAT
        ' txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number(dblTongTrichNo, ".")
    End Sub

    Private Function GetTenLoaiThue(ByVal pv_strMaLoaiThue As String) As String
        Select Case pv_strMaLoaiThue
            Case "01"
                Return "Thuế nội địa"
            Case "02"
                Return "Thuế thu nhập cá nhân"
            Case "03"
                Return "Thuế trước bạ"
            Case "04"
                Return "Thuế Hải Quan"
            Case "05"
                Return "Thu khác"
            Case "06"
                Return "Thu tài chính"
            Case Else '05'
                Return "Thu ngân sách nhà nước"
        End Select
    End Function

    Private Sub GetThongTinTKKH(ByVal v_strHTTT As String, ByVal v_strTrangThaiCT As String, _
                    ByVal pv_strSoTKKH As String, ByRef pv_strTenKH As String, ByRef pv_strTTKH As String)
        If (pv_strSoTKKH.Length > 0) Then 'Lay theo sotk
            If v_strHTTT.Equals("01") Then 'Chuyen Khoan
                If v_strTrangThaiCT.Equals("05") Then 'Cho KS lay thong tin tu corebank

                Else 'Cac truong hop khac thi lay thong tin tu db

                End If
            End If
        End If
    End Sub

    Private Sub ShowHideButtons(ByVal strTT As String, ByVal strTTThue As String)
        If strTT <> "01" Then
            lbSoFT.Text = ""
        End If
        Select Case strTT
            Case "00"
                lblStatus.Text = "Chứng từ chưa kiểm soát"
                cmdHuy.Enabled = False
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdChuyenBDS.Enabled = False
                cmdINCT.Enabled = False
            Case "01"
                lblStatus.Text = "Chứng từ đã kiểm soát"
                cmdHuy.Enabled = False
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdChuyenBDS.Enabled = False
                cmdINCT.Enabled = True
            Case "02"
                lblStatus.Text = "Chứng từ đã hủy bởi GDV - đang chờ duyệt"
                cmdHuy.Enabled = True
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = True
                cmdChuyenBDS.Enabled = False
                cmdINCT.Enabled = False
            Case "03"
                lblStatus.Text = "Chứng từ bị chuyển trả"
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdHuy.Enabled = False
                cmdChuyenBDS.Enabled = False
                cmdINCT.Enabled = False
            Case "04"
                lblStatus.Text = "Chứng từ đã bị hủy"
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdHuy.Enabled = False
                cmdChuyenBDS.Enabled = False
                cmdINCT.Enabled = False
            Case "05"
                lblStatus.Text = "Chứng từ chờ kiểm soát"
                'cmdKS.Enabled = True
                cmdChuyenTra.Enabled = True
                cmdHuy.Enabled = False
                cmdChuyenBDS.Enabled = False
                cmdINCT.Enabled = False
            Case "06"
                lblStatus.Text = "Chứng từ hạch toán lỗi ở BDS"
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdHuy.Enabled = False
                cmdChuyenBDS.Enabled = True
                cmdINCT.Enabled = False
        End Select
        cmdChuyenThue.Enabled = False
        If strTTThue = "0" Then
            If strTT = "01" Then
                lblStatus.Text = "Chứng từ đã duyệt nhưng chưa chuyển sang thuế"
                cmdChuyenThue.Enabled = False
                ' cmdHuy.Enabled = False
            End If
        End If
        If strTTThue = "1" Then
            If strTT = "04" Then
                lblStatus.Text = "Chứng từ đã hủy nhưng chưa chuyển sang thuế"
                cmdChuyenThue.Enabled = False
            End If
        End If
    End Sub

    Private Sub ShowHideCTRow(ByVal pv_blnDisplayRow As Boolean)
        rowDChiNNT.Visible = pv_blnDisplayRow
        rowNNTien.Visible = pv_blnDisplayRow
        rowDChiNNTien.Visible = pv_blnDisplayRow
        rowQuanNNThue.Visible = pv_blnDisplayRow
        rowTinhKB.Visible = pv_blnDisplayRow
        rowTinhNNTien.Visible = pv_blnDisplayRow
        rowDBHC.Visible = pv_blnDisplayRow
        rowCQThu.Visible = pv_blnDisplayRow
        rowTKNo.Visible = False
        rowTKCo.Visible = pv_blnDisplayRow
        rowHTTT.Visible = pv_blnDisplayRow
        rowTK_KH_NH.Visible = pv_blnDisplayRow
        rowSoDuNH.Visible = False
        rowNgay_KH_NH.Visible = pv_blnDisplayRow
        rowMaNT.Visible = pv_blnDisplayRow
        rowLoaiThue.Visible = pv_blnDisplayRow
        rowToKhaiSo.Visible = pv_blnDisplayRow
        rowNgayDK.Visible = pv_blnDisplayRow
        rowLHXNK.Visible = pv_blnDisplayRow
        rowSoKhung.Visible = pv_blnDisplayRow
        rowSoMay.Visible = pv_blnDisplayRow
        'rowSoBK.Visible = pv_blnDisplayRow
        'rowNgayBK.Visible = pv_blnDisplayRow
        rowNganHangChuyen.Visible = pv_blnDisplayRow
        rowNganHangNhan.Visible = pv_blnDisplayRow
        rowTKGL.Visible = pv_blnDisplayRow
        rowSO_QD.Visible = True
        rowCQ_QD.Visible = True
        rowNgay_QD.Visible = True
    End Sub

    Private Sub ShowHideControlByLoaiThue(ByVal pv_strLoaiThue As String)
        Select Case pv_strLoaiThue
            Case "01" 'NguonNNT.NNThue_CTN, NguonNNT.NNThue_VL, NguonNNT.SoThue_CTN, NguonNNT.VangLai
                'An thong tin To khai
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False

                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case "02"
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case "03" 'NguonNNT.TruocBa
                'Show thong tin Truoc ba
                rowSoKhung.Visible = True
                rowSoMay.Visible = True

                'an thong tin to khai
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
            Case "04" 'NguonNNT.HaiQuan
                'Show thong tin to khai
                rowLHXNK.Visible = True
                rowToKhaiSo.Visible = True
                rowNgayDK.Visible = True

                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case "05"
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case "06"
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case Else '"05" Khong xac dinh
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
                rowSO_QD.Visible = True
                rowCQ_QD.Visible = True
                rowNgay_QD.Visible = True

        End Select

    End Sub

    Private Sub ShowHideControlByPT_TT(ByVal pv_strPT_TT As String, ByVal pv_strTT As String)
        'lblStatus.Text = ""
        If pv_strPT_TT = "01" Or pv_strPT_TT = "03" Or pv_strPT_TT = "05" Then 'Chuyển khoản
            'If pv_strPT_TT = "03" Then ' Chỉ GL mới lấy ra số dư vì chuyển khoản đã trừ tiền trước rồi
            rowTK_KH_NH.Visible = True
            rowNganHangNhan.Visible = True
            rowNganHangChuyen.Visible = True
            rowTKGL.Visible = False
            If pv_strTT.Equals("05") Then
                rowSoDuNH.Visible = True
                rowTK_KH_NH.Visible = True
                Dim v_strSoTKKH As String = txtTK_KH_NH.Text
                Dim v_strLoaiTien As String = txtMaNT.Text
                If v_strSoTKKH.Length > 0 Then
                    Dim v_objCoreBank As New CorebankServiceESB.clsCoreBank()
                    Dim v_strDescTKKH As String = String.Empty
                    Dim v_strSoDuNH As String = String.Empty
                    Dim v_strErrorCode As String = String.Empty
                    Dim v_strErrorMsg As String = String.Empty

                    If (txtTK_KH_NH.Text.Trim.Length > 0) Then
                        Dim xmlDoc As New XmlDocument
                        Dim ma_loi As String = ""
                        Dim so_du As String = ""
                        Dim ten_TK_KH As String = ""
                        Dim branch_tk As String = ""
                        If pv_strPT_TT = "05" Then
                        Else
                            Dim v_strReturn As String = v_objCoreBank.GetTK_KH_NH(txtTK_KH_NH.Text.Trim, ma_loi, so_du, branch_tk)
                            ten_TK_KH = v_objCoreBank.GetTen_TK_KH_NH(txtTK_KH_NH.Text.Trim)
                            If ma_loi = "00000" Or ma_loi Is Nothing Then
                                txtTenTK_KH_NH.Text = ten_TK_KH
                                txtSoDu_KH_NH.Text = VBOracleLib.Globals.Format_Number(so_du, ".")
                            Else
                                lblStatus.Text = "Không truy vấn được thông tin tài khoản.Mã lỗi:" & v_strErrorCode & ",mô tả:" & clsCTU.MappingErrorCode(v_strErrorCode)
                            End If
                        End If

                    End If
                End If
            Else
                rowSoDuNH.Visible = False
            End If
        ElseIf pv_strPT_TT = "00" Then  'Tiền mặt
            rowTK_KH_NH.Visible = False
            rowNganHangNhan.Visible = False
            rowNganHangChuyen.Visible = False
            rowSoDuNH.Visible = False
            rowTKGL.Visible = True
            'ElseIf pv_strPT_TT = "01" Then  'chuyên khoan KH
            '    rowSoDuNH.Visible = False
        Else 'Báo có
            rowTK_KH_NH.Visible = False
            rowNganHangNhan.Visible = False
            rowNganHangChuyen.Visible = False
            rowSoDuNH.Visible = False
            rowTKGL.Visible = True
        End If
    End Sub

    Protected Sub cmdChuyenTra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyenTra.Click
        Try
            If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
                lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới."
                Exit Sub
            End If

            'If Check_Mode() <> gblnTTBDS Then
            '    lblStatus.Text = "Trạng thái BDS đã thay đổi. Hãy thoát khỏi chương trình và đăng nhập lại"
            '    Exit Sub
            'End If

            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT

                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    Exit Sub
                End If
                If strTT_Data = "02" Then
                    ChungTu.buChungTu.CTU_SetTrangThai(key, "01", "", "", "", "", "", "", v_strTrangThai_CT, drpLyDoHuy.Text.ToString())
                    LoadDSCT(ddlTrangThai.SelectedValue)
                    EnabledButtonWhenDoTransaction(False)
                    cmdHuy.Enabled = False
                Else
                    ChungTu.buChungTu.CTU_SetTrangThai(key, "03", "", "", "", "", "", "", "", drpLyDoHuy.Text.ToString())
                    'unhold tài khoản khách hàng
                    Dim strTK_KH_NH As String = txtTK_KH_NH.Text.Trim
                    Dim strPTTinhPhi As String = ""
                    Dim strTTien As String = txtTTIEN.Text.Trim.Replace(".", "")

                End If

                Dim strFeeAmount As String = txtCharge.Text.Trim.Replace(".", "")
                Dim strVATAmount As String = txtVAT.Text.Trim.Replace(".", "")
                Dim strErrorCode As String = ""
                Dim strErrorMsg As String = ""

                EnabledButtonWhenDoTransaction(False)
                LoadDSCT(ddlTrangThai.SelectedValue)
                lblStatus.Text = "Chuyển trả chứng từ thành công."
            Else
                lblStatus.Text = "Không tìm thấy chứng từ cần tìm.Hãy thử lại"
                Exit Sub
            End If
        Catch ex As Exception
            logger.Error(ex.StackTrace)
            logger.Error("Error source: " & ex.Source & vbNewLine _
                  & "Error code: System error!" & vbNewLine _
                  & "Error message: " & ex.Message)
        End Try
    End Sub

    'Protected Sub cmdKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdKS.Click
    '    Dim v_blnReturn As Boolean = False
    '    Dim cls_corebank As New CorebankServiceESB.clsCoreBank()
    '    Try
    '        'Disable button tranh lam viec 2 lan
    '        EnabledButtonWhenDoTransaction(False)
    '        Dim strUser = mv_objUser.Ten_DN.ToString

    '        Dim tien_tongTN As Decimal = Convert.ToDecimal(txtTongTrichNo.Text.ToString().Replace(".", ""))

    '        If Not Business.Common.mdlCommon.CheckHanMuc_KSV(Session.Item("User").ToString(), tien_tongTN) Then
    '            lblStatus.Text = "Hạn mức của bạn không đủ để duyệt giao dịch này."
    '            Exit Sub
    '        End If
    '        'End Hạn mức
    '        'Kiểm tra ngày làm việc

    '        If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
    '            lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới"
    '            EnabledButtonWhenDoTransaction(True)
    '            Exit Sub
    '        End If


    '        Dim v_strErrorCode As String = String.Empty
    '        Dim v_strErrorMessage As String = String.Empty
    '        Dim v_strPM_rm_ref_no As String = ""
    '        Dim v_strPM_so_du As String = ""
    '        Dim v_objGDV As CTuUser
    '        'Dat lai gia tri
    '        Dim v_strSHKB As String = hdfSHKB.Value
    '        Dim v_strNgay_KB As String = hdfNgay_KB.Value
    '        Dim v_strSo_CT As String = hdfSo_CT.Value
    '        Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
    '        Dim v_strSo_BT As String = hdfSo_BT.Value
    '        Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
    '        Dim v_strMa_NV As String = hdfMa_NV.Value
    '        Dim v_strMa_NH_B As String = txtMA_NH_B.Text.Trim
    '        v_objGDV = New CTuUser(v_strMa_NV)
    '        Dim strHT_TT As String = ddlHTTT.Text.Trim
    '        Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value

    '        If ((Not v_strSHKB Is Nothing) _
    '             And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then

    '            Dim cls_songphuong As New CorebankServiceESB.clsSongPhuong

    '            Dim strResult As String = cls_songphuong.ValiadSHB(v_strSHKB)

    '            Dim warningMessage As String = "Lệnh thu ngân sách có thể thu tại SHB, bạn có muốn tiếp tục?"

    '            If strResult.Length >= 8 Then
    '                If strResult.Equals(v_strMa_NH_B) Then
    '                    'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsProcessWarningMessage", "jsProcessWarningMessage('" + warningMessage + "');", True)
    '                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsProcessKS", "jsProcessKS('OK');", True)
    '                    'Me.Page.ClientScript.RegisterStartupScript(Me.GetType(), "jsProcessKS", "jsProcessKS('OK');", True)
    '                Else
    '                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsProcessWarningMessage", "jsProcessWarningMessage('" + warningMessage + "');", True)
    '                    'Me.Page.ClientScript.RegisterStartupScript(Me.GetType(), "jsProcessWarningMessage", "jsProcessWarningMessage('" + warningMessage + "');", True)
    '                End If
    '            Else
    '                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsProcessKS", "jsProcessKS('OK');", True)
    '                'Me.Page.ClientScript.RegisterStartupScript(Me.GetType(), "jsProcessKS", "jsProcessKS('OK');", True)
    '            End If



    '        End If
    '    Catch ex As Exception
    '        Dim sbErrMsg As StringBuilder
    '        sbErrMsg = New StringBuilder()
    '        sbErrMsg.Append("Lỗi trong quá trình kiểm soát chứng từ")
    '        sbErrMsg.Append(vbCrLf)
    '        sbErrMsg.Append(ex.Message)
    '        sbErrMsg.Append(vbCrLf)
    '        sbErrMsg.Append(ex.StackTrace)

    '        LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

    '        If Not ex.InnerException Is Nothing Then
    '            sbErrMsg = New StringBuilder()

    '            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ ")
    '            sbErrMsg.Append(vbCrLf)
    '            sbErrMsg.Append(ex.InnerException.Message)
    '            sbErrMsg.Append(vbCrLf)
    '            sbErrMsg.Append(ex.InnerException.StackTrace)

    '            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)
    '        End If
    '        lblStatus.Text = sbErrMsg.ToString
    '        Throw ex
    '    End Try
    'End Sub
    Protected Sub cmdKSBK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdKSBK.Click
        Dim v_blnReturn As Boolean = False
        Dim cls_corebank As New CorebankServiceESB.clsCoreBank()
        Try
            'Disable button tranh lam viec 2 lan
            EnabledButtonWhenDoTransaction(False)
            Dim strUser = mv_objUser.Ten_DN.ToString

            Dim tien_tongTN As Decimal = Convert.ToDecimal(txtTongTrichNo.Text.ToString().Replace(".", ""))

            If Not Business.Common.mdlCommon.CheckHanMuc_KSV(Session.Item("User").ToString(), tien_tongTN) Then
                lblStatus.Text = "Hạn mức của bạn không đủ để duyệt giao dịch này."
                Exit Sub
            End If
            'End Hạn mức
            'Kiểm tra ngày làm việc

            If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
                lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới"
                EnabledButtonWhenDoTransaction(True)
                Exit Sub
            End If


            Dim v_strErrorCode As String = String.Empty
            Dim v_strErrorMessage As String = String.Empty
            Dim v_strPM_rm_ref_no As String = ""
            Dim v_strPM_so_du As String = ""
            Dim v_objGDV As CTuUser
            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            v_objGDV = New CTuUser(v_strMa_NV)
            Dim strHT_TT As String = ddlHTTT.Text.Trim
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value

            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT
                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If


                'Cập nhật số lần nhấn kiểm soát: check cột save_type
                Dim solan As Integer = 0
                Dim strClick As String = ChungTu.buChungTu.CTU_Get_LanKS(key)
                If strClick.Length > 0 Then
                    solan = CInt(strClick) + 1
                    strClick = solan.ToString()
                Else
                    strClick = "1"
                End If
                ChungTu.buChungTu.CTU_SetLanKS(key, strClick)
                'end cập nhật số lần

                'Lấy trực tiếp phương thức thanh toán trong DB
                Dim strTongTienTT As String = txtTTIEN.Text.Replace(".", "")
                Dim strSoduTaiKhoanKH As String = txtSoDu_KH_NH.Text.Replace(".", "")
                Dim str_Fee As String = txtCharge.Text.Trim.Replace(".", "")
                Dim str_VAT As String = txtVAT.Text.Trim.Replace(".", "")
                'mv_objUser.MA_CN =>lay theo ma chi nhanh cua user tao dien
                ' Dim v_objCoreBank As New clsCoreBankUtilPGB

                'If strHT_TT = "01" Then
                '    If txtSoDu_KH_NH.Text.Length = 0 Then
                '        lblStatus.Text = "Tài khoản khách hàng không đủ để thanh toán"
                '        EnabledButtonWhenDoTransaction(True)
                '        Exit Sub
                '    End If
                '    If Double.Parse(strTongTienTT) > Double.Parse(strSoduTaiKhoanKH) Then
                '        lblStatus.Text = "Tài khoản không đủ để thanh toán.Hãy kiểm tra lại"
                '        EnabledButtonWhenDoTransaction(True)
                '        Exit Sub
                '    End If
                'End If

                Dim strMaNNT As String = txtMaNNT.Text.Trim
                Dim strTenNNT As String = Globals.RemoveSign4VietnameseString(txtTenNNT.Text.Trim)
                Dim strDiaChiNNT As String = Globals.RemoveSign4VietnameseString(txtDChiNNT.Text.Trim)
                Dim strSoCT_NH As String = txtSoCT_NH.Text.Trim
                Dim strErrorCode As String = ""
                Dim strErrorMessege As String = ""
                Dim a As Integer = 1
                Dim str_MaTinh As String = txtTinhKB.Text
                Dim str_TenTinh As String = Globals.RemoveSign4VietnameseString(txtTenTinhKB.Text)
                Dim str_AccountNo As String = txtTK_KH_NH.Text.Trim
                Dim str_HTTT As String
                If strHT_TT = "01" Or strHT_TT = "05" Then
                    str_HTTT = "A"
                Else
                    str_HTTT = "G"
                End If
                Dim intRowcount As Integer
                Dim str_Desc As String = Globals.RemoveSign4VietnameseString(BuildRemarkForPayment(intRowcount))


                Dim str_clk As String = ""
                If intRowcount = 1 Then
                    str_clk = BuildCKTM()
                End If
                Dim str_loaiPhi As String = ""

                Dim strMa_NH_A As String = txtMA_NH_A.Text.Trim
                Dim strTen_NH_A As String = txtTen_NH_A.Text.Trim
                Dim str_TKKB As String = txtTKCo.Text.Trim
                Dim str_TenKB As String = Globals.RemoveSign4VietnameseString(txtTenKB.Text.Trim & "-" & txtTenCQThu.Text)
                Dim strMa_NH_TT As String = txtMA_NH_TT.Text.Trim
                Dim strTen_NH_TT As String = txtTEN_NH_TT.Text.Trim
                Dim strMa_NH_B As String = txtMA_NH_B.Text.Trim
                Dim strTen_NH_B As String = txtTen_NH_B.Text.Trim
                Dim str_CIFNo As String = txtTenTK_KH_NH.Text.Split("|")(0)
                Dim str_TenKH As String = txtTenTK_KH_NH.Text
                Dim str_RefNo As String = txtREF_NO.Text.Trim
                Dim strLoaiTien As String = txtMaNT.Text
                Dim str_MaSanPham As String = txtSanPham.Text.Trim
                Dim str_TimeInput As String = txtTimeLap.Text
                Dim strTKCo As String = txtTKCo.Text.Trim
                Dim strTenCQT As String = Globals.RemoveSign4VietnameseString(txtTenCQThu.Text.Trim)
                Dim TRAN_ID As String = ""
                Dim ngay_GD As String = ConvertNumberToDate(mv_objUser.Ngay_LV).ToString("dd/MM/yyyy")
                Dim ngay_GD_SP As String = ConvertNumberToDate(mv_objUser.Ngay_LV).ToString("yyyyMMdd")
                Dim TT_CITAD As String = ""
                Dim strMa_NguyenTe As String = clsCTU.GetMA_NguyenTe(txtMaNT.Text.Trim)
                If RadioButton1.Checked = True And strMa_NH_TT.Substring(2, 3).ToString() <> "101" And strTKCo = "7111" Then
                    TT_CITAD = "1"
                Else
                    TT_CITAD = "0"
                End If
                Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)

                'Built to table
                Dim str_PayMent As String = ""
                Dim rm_ref_no As String = ""
                Dim req_Internal_rmk As String = ""
                Dim sMACN As String = hdfMACN.Value
                Dim sTenMaCN As String = ""
                sTenMaCN = Get_TenCN(hdfMACN.Value)
                req_Internal_rmk = hdfMACN.Value & "-" & sTenMaCN

                'check SP tại SHB
                Dim cls_songphuong As New CorebankServiceESB.clsSongPhuong

                Dim strResult As String = cls_songphuong.ValiadSHB(v_strSHKB)


                Dim strNHB As String = ""
                Dim strACC_NHB As String = ""
                Dim strMA_CN_SP As String = ""
                If strResult.Length > 8 Then
                    Dim strarr() As String
                    strarr = strResult.Split(";")
                    strNHB = strarr(0).ToString
                    strACC_NHB = strarr(1).ToString
                    strMA_CN_SP = strarr(2).ToString
                End If


                Dim strMACN_TK As String = ""

                Dim boolSP As Boolean = False
                If strNHB.Length >= 8 And strNHB.Equals(strMa_NH_B) Then
                    ''đi theo hàm thanh toán mới, sau đó gửi song phương
                    boolSP = True
                    'v_strErrorCode = "00000"
                    'rm_ref_no = Date.Now.ToString("yyyyMMddHHmmss")

                    'Nếu thanh toán được kho bạc SHB mới truy vấn tài khoản để lấy mã chi nhánh tài khoản
                    If strHT_TT = "01" Then
                        If (txtTK_KH_NH.Text.Trim.Length > 0) Then
                            Dim xmlDoc As New XmlDocument
                            Dim ma_loi As String = ""
                            Dim so_du As String = ""
                            Dim ten_TK_KH As String = ""
                            Dim branch_tk As String = ""
                            Dim v_strReturn As String = cls_corebank.GetTK_KH_NH(txtTK_KH_NH.Text.Trim, ma_loi, so_du, branch_tk)

                            If ma_loi = "00000" Or ma_loi Is Nothing Then
                                strMACN_TK = branch_tk
                            Else
                                lblStatus.Text = "Không truy vấn được thông tin tài khoản.Mã lỗi:" & v_strErrorCode & ",mô tả:" & clsCTU.MappingErrorCode(v_strErrorCode)
                                EnabledButtonWhenDoTransaction(True)
                                Exit Sub
                            End If
                        End If
                    ElseIf strHT_TT = "05" Then
                        strMACN_TK = sMACN
                    End If

                    'cap nhat TTSP ve 1
                    ChungTu.buChungTu.CTU_Update_TTSP(v_strSo_CT, "1")
                    str_PayMent = cls_corebank.PaymentSP("TAX", ngay_GD_SP, v_strSo_CT, sMACN, strMACN_TK, str_AccountNo, strTongTienTT, txtMaNT.Text.Trim, req_Internal_rmk, str_Desc, _
                             v_strMa_NV, mv_objUser.Ma_NV, "ETAX_ND", strTKCo, strMa_NH_B.ToString().Substring(2, 3), strMa_NH_B, strACC_NHB, strMA_CN_SP, v_strErrorCode, rm_ref_no)


                Else

                    lblStatus.Text = "Kho bạc không được thu tại SHB, vui lòng kiểm tra lại ngân hàng hưởng."
                    Exit Sub

                End If



                If v_strErrorCode = "00000" Then
                    'Cập nhật lại trạng thái của chứng từ
                    'Insert log_paymen
                    v_strTrangThai_CT = "01" 'Trạng thái chưa cập nhật vào BDS
                    hdfTrangThai_CT.Value = v_strTrangThai_CT
                    lbSoFT.Text = "Số CT_NH:" & rm_ref_no
                    'txtSoDu_KH_NH.Text = v_strPM_so_du
                    'update remark và ngay_kh_nh
                    ChungTu.buChungTu.CTU_Update_Remarks(key, str_Desc)
                    ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, rm_ref_no, "", "", mv_objUser.Ma_NV)
                    lblStatus.Text = "Kiểm soát chứng từ thành công."
                    Dim ma_loi As String = ""
                    Dim so_du As String = ""
                    Dim strReturn_SODU As String = ""
                    'gui di song phuong
                    If boolSP = True Then
                        Try
                            cls_songphuong.SendChungTuSP(v_strSo_CT, "TQ")
                        Catch ex As Exception
                            'chi ghi log, khong throw, van gui thue binh thuong
                            LogApp.WriteLog("KiemSoatCTKhac.SendChungTuSP", ex, "Lỗi ghi chứng từ", HttpContext.Current.Session("TEN_DN"))
                        End Try
                   
                    End If
                Else
                    lblStatus.Text = "Đã có lỗi xảy ra khi hạch toán chứng từ.Mã lỗi: " & v_strErrorCode & ",mô tả:" & v_strErrorMessage & ". Vui lòng liên hệ QTV để được trợ giúp."
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If


                'Chuyen thong tin sang thue

                Dim blnSuccess As Boolean = True
                Dim errcode As String = ""
                Dim errdesc As String = ""
                
                'CAP NHAT TRANG THAI DA CHUYEN THUE
                v_strTrangThai_CT = "01" 'Trạng thái đã ks
                Dim strTT_CThue As String = IIf(blnSuccess, "1", "0")
                ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, "", "", "", "", "", "", IIf(blnSuccess, "1", "0"))
                cmdChuyenTra.Enabled = False
                cmdINCT.Enabled = True
                ' cmdHuy.Enabled = True
                LoadDSCT(ddlTrangThai.SelectedValue)
                hdfTrangThai_CThue.Value = strTT_CThue
                EnabledButtonWhenDoTransaction(False)
            End If
        Catch ex As Exception

            LogApp.WriteLog("KiemSoatCT.GIP", ex, "Lỗi trong quá trình kiểm soát chứng từ", HttpContext.Current.Session("TEN_DN"))

            lblStatus.Text = ex.Message
            Throw ex
        End Try
    End Sub

    Protected Sub cmdHuy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdHuy.Click
        Dim v_blnReturn As Boolean = False
        Try
            cmdHuy.Enabled = False
            EnabledButtonWhenDoTransaction(False)
            Dim strUser = mv_objUser.Ten_DN.ToString
            'If clsCTU.checkGioGD() = "0" Then
            '    lblStatus.Text = "Đã hết giờ giao dịch trong ngày. Không được tiếp tục hủy lệnh!"
            '    Exit Sub
            'End If
            'If Not (txtMatKhau.Text.Length = 0) Then
            '    If Not Business.KetNoi_LDAP.CheckPassWord(strUser, txtMatKhau.Text) Then
            '        lblStatus.Text = "Mật khẩu kiểm soát không hợp lệ.Hãy thử lại!"
            '        EnabledButtonWhenDoTransaction(False)
            '        cmdHuy.Enabled = True
            '        Exit Sub
            '    End If
            'Else
            '    lblStatus.Text = "Phải nhập mật khẩu khi Duyệt hủy."
            '    EnabledButtonWhenDoTransaction(False)
            '    cmdHuy.Enabled = True
            '    Exit Sub
            'End If
            'Kiểm tra ngày làm việc
            If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
                lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới"
                EnabledButtonWhenDoTransaction(False)
                cmdHuy.Enabled = True
                Exit Sub
            End If

            'Kiểm tra mật khẩu kiểm soát


            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            Dim v_strTT_ChuyenThue As String = hdfTrangThai_CThue.Value
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT
                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If
                'Chuyen thong tin sang thue
                Dim blnSuccess As Boolean = True
                Dim strLyDoHuy As String = ""
                Dim errcode As String = ""
                Dim errMess As String = ""
                If (v_strTrangThai_CT = "01" Or v_strTrangThai_CT = "02") And v_strTT_ChuyenThue = "1" Then
                    Try
                        logger.Info("huyCTU - MST:" & txtMaNNT.Text & " SHKB: " & key.SHKB & " Ma NV: " & key.Ma_NV)
                        ProcMegService.ProMsg.getMSG24(txtMaNNT.Text, v_strSo_CT, errcode, errMess)
                        ' GIPBankInterface.huyCTU(txtMaNNT.Text, v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV)
                        If (errcode = "01") Then
                            lblStatus.Text = "Đã chuyển dữ liệu hủy sang Cơ quan Thuế thành công"
                        Else
                            blnSuccess = False
                            lblStatus.Text = "Chưa chuyển được số hủy liệu sang Cơ quan Thuế. Mã lỗi: " & errcode & " Mô tả: " & errMess
                            cmdChuyenThue.Enabled = False
                            Return
                        End If

                        ' LogDebug.WriteLog("TB thanh cong - MST:" & txtMaNNT.Text & " SHKB: " & key.SHKB & " Ma NV: " & key.Ma_NV, EventLogEntryType.Information)
                    Catch ex As Exception
                        blnSuccess = False
                        lblStatus.Text = "Chưa chuyển được số hủy liệu sang Cơ quan Thuế. Mã lỗi: " & ex.ToString
                        cmdChuyenThue.Enabled = False
                        Exit Sub
                    End Try
                    strLyDoHuy = drpLyDoHuy.SelectedValue.Trim()
                    'CAP NHAT TRANG THAI DA CHUYEN THUE
                    v_strTrangThai_CT = "04" 'Trạng thái đã huy
                    ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, "", "", "", "", mv_objUser.Ma_NV, "", "0", strLyDoHuy)
                    LoadDSCT(ddlTrangThai.SelectedValue)
                    EnabledButtonWhenDoTransaction(False)
                Else
                    strLyDoHuy = drpLyDoHuy.SelectedValue.Trim()
                    'CAP NHAT TRANG THAI DA CHUYEN THUE
                    v_strTrangThai_CT = "04" 'Trạng thái đã huy
                    ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, "", "", "", "", mv_objUser.Ma_NV, "", "1", strLyDoHuy)
                    lblStatus.Text = "Chứng từ đã hủy thành công"
                    LoadDSCT(ddlTrangThai.SelectedValue)
                    EnabledButtonWhenDoTransaction(False)
                End If



            End If
        Catch ex As Exception

            LogApp.WriteLog("cmdHuy.GIP", ex, "Lỗi trong quá trình hủy chứng từ", HttpContext.Current.Session("TEN_DN"))

        Finally
        End Try
    End Sub

    Private Sub EnabledButtonWhenDoTransaction(ByVal blnEnabled As Boolean)
        'cmdKS.Enabled = blnEnabled
        cmdChuyenTra.Enabled = blnEnabled
    End Sub

    Protected Sub btnSelectCT_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnSelectCT As LinkButton = CType(sender, LinkButton)
            Dim v_strRetArgument As String() = btnSelectCT.CommandArgument.ToString.Split(",")
            lblStatus.Text = ""
            If (v_strRetArgument.Length > 3) Then
                Dim v_strSHKB As String = v_strRetArgument(0)
                Dim v_strNgay_KB As String = v_strRetArgument(1)
                Dim v_strSo_CT As String = v_strRetArgument(2)
                Dim v_strKyHieu_CT As String = v_strRetArgument(3)
                Dim v_strSo_BT As String = v_strRetArgument(4)
                Dim v_strMa_Dthu As String = v_strRetArgument(5)
                Dim v_strMa_NV As String = v_strRetArgument(6)
                Dim v_strTrangThai_CT As String = v_strRetArgument(7)
                Dim v_strLanClickKS As String = v_strRetArgument(8)
                'Load thông tin chi tiết khi người dùng chọn 1 CT cụ thể
                If ((Not v_strSHKB Is Nothing) And (Not v_strMa_NV Is Nothing) _
                     And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                    Dim key As New KeyCTu
                    key.Ma_Dthu = v_strMa_Dthu
                    key.Kyhieu_CT = v_strKyHieu_CT
                    key.Ma_NV = v_strMa_NV
                    key.Ngay_KB = v_strNgay_KB
                    key.SHKB = v_strSHKB
                    key.So_BT = v_strSo_BT
                    key.So_CT = v_strSo_CT
                    LoadCTChiTietAll(key)

                    'Dat lai gia tri
                    hdfSHKB.Value = v_strSHKB
                    hdfNgay_KB.Value = v_strNgay_KB
                    hdfSo_CT.Value = v_strSo_CT
                    hdfKyHieu_CT.Value = v_strKyHieu_CT
                    hdfSo_BT.Value = v_strSo_BT
                    hdfMa_Dthu.Value = v_strMa_Dthu
                    hdfMa_NV.Value = v_strMa_NV
                    hdfTrangThai_CT.Value = v_strTrangThai_CT
                    hdfClickKS.Value = v_strLanClickKS
                End If
            End If
            ' ClientScript.RegisterStartupScript(Me.GetType(), "InitPage", "alert('OK');LoadBtnSubmit();", True)
        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub

    Protected Sub ddlTrangThai_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTrangThai.SelectedIndexChanged
        LoadDSCT(ddlTrangThai.SelectedValue)
        ClearCTHeader()
        CreateDumpChiTietCTGrid()
        ShowHideCTRow(False)
    End Sub
    Private Function BuildRemarkForPayment(ByRef intRowcount As Integer) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)

        Dim v_strRetRemark As String = String.Empty
        Dim str_TenMST As String = txtTenNNT.Text.ToString()
        Dim str_MST As String = " MST" & txtMaNNT.Text.ToString()
        Dim str_MaLThue As String = "LThue" & "03"  'ddlLoaiThue.SelectedValue.ToString()
        Dim str_NNT As String = "NgayNT" & txtNGAY_KH_NH.Text.ToString() & ";"
        Dim str_KyThue As String = ""
        Dim remark_loaithue As String = ""

        If ddlLoaiThue.SelectedValue = "07" Then
            remark_loaithue = ";SoQD " & txtSO_QD.Text.Trim.ToString() & ";CQQD " & txtCQ_QD.Text.Trim.ToString() & ";NgayQD " & txtNGAY_QD.Text.Trim.ToString()
        End If

        For i = 0 To v_intRowCnt - 1
            v_strRetRemark &= "NDKT" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text
            v_strRetRemark &= "C" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text
            v_strRetRemark &= "S" & VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text, ".").Replace(".", "")
            v_strRetRemark &= " ND " & CType(grdChiTiet.Items(i).Controls(5).Controls(1), TextBox).Text
            v_strRetRemark &= ",KT" & CType(grdChiTiet.Items(i).Controls(8).Controls(1), TextBox).Text
            v_strRetRemark &= IIf(i < v_intRowCnt - 1, ";", "")
        Next

        Dim str_GhiChu As String = "." & txtGhiChu.Text.Trim
        v_strRetRemark = str_TenMST & str_MST & str_MaLThue & str_NNT & v_strRetRemark & remark_loaithue & str_GhiChu
        v_strRetRemark = Globals.RemoveSign4VietnameseString(v_strRetRemark)
        'If v_intRowCnt > 1 Then
        '    v_strRetRemark = str_B1D & str_MST & str_MaLThue & str_NNT & v_strRetRemark & "KT" & str_KyThue & str_K1T & str_NopThue & str_GhiChu
        'Else
        '    v_strRetRemark = str_MST & str_MaLThue & str_NNT & v_strRetRemark & "KT" & str_KyThue
        'End If
        intRowcount = v_intRowCnt
        Return v_strRetRemark
    End Function
    Private Function BuildRemarkForPayment_V20(ByRef intRowcount As Integer) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)

        Dim v_strRetRemark As String = String.Empty
        Dim str_TenMST As String = txtTenNNT.Text.ToString()
        'For i = 0 To v_intRowCnt - 1
        '    v_strRetRemark &= ",KT" & CType(grdChiTiet.Items(i).Controls(8).Controls(1), TextBox).Text
        'Next
        If ddlLoaiThue.SelectedValue = "03" Then
            v_strRetRemark &= ";SK " & txtSoKhung.Text.Trim.ToString() & ";SM " & txtSoMay.Text.Trim.ToString()
        ElseIf ddlLoaiThue.SelectedValue = "07" Then
            v_strRetRemark &= ";CQQD " & txtCQ_QD.Text.Trim.ToString()
        End If
        Dim str_GhiChu As String = ".DNCT " & txtGhiChu.Text.Trim
        v_strRetRemark &= str_GhiChu
        v_strRetRemark = Globals.RemoveSign4VietnameseString(v_strRetRemark)

        intRowcount = v_intRowCnt
        v_strRetRemark = V_Substring(v_strRetRemark, 210)

        Return v_strRetRemark
    End Function
    Private Function BuildEcomForPayment_V20(ByRef intRowcount As Integer) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)

        Dim v_strRetRemark As String = String.Empty
        Dim remark_loaithue As String = ""
        'If v_intRowCnt > 1 Then
        For i = 0 To v_intRowCnt - 1

            v_strRetRemark &= V_Substring(CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text, 4)
            v_strRetRemark &= "~" & V_Substring(CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text, 3)
            v_strRetRemark &= "~" & V_Substring(VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text, ".").Replace(".", ""), 20)
            v_strRetRemark &= "~" & V_Substring(CType(grdChiTiet.Items(i).Controls(5).Controls(1), TextBox).Text, 100)

            If ddlLoaiThue.SelectedValue = "07" Then
                v_strRetRemark &= "~" & V_Substring(txtSO_QD.Text.Trim.ToString(), 30)
                'Ngày quyet dinh  //Đinh dạng dd-MM-yyyy
                Dim ngay_qd As String = String.Empty
                If Not txtNGAY_QD.Text Is Nothing Then
                    ngay_qd = DateTime.ParseExact(txtNGAY_QD.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("dd-MM-yyyy")
                End If

                v_strRetRemark &= "~" & V_Substring(ngay_qd, 10)
            Else
                Dim kythue As String = String.Empty
                v_strRetRemark &= "~" 'Khong co so quyet dinh /so to khai
                If Not CType(grdChiTiet.Items(i).Controls(8).Controls(1), TextBox).Text Is Nothing Then
                    kythue = CType(grdChiTiet.Items(i).Controls(8).Controls(1), TextBox).Text '"00-" & DateTime.ParseExact(CType(grdChiTiet.Items(i).Controls(8).Controls(1), TextBox).Text, "MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("MM-yyyy")
                End If
                v_strRetRemark &= "~" & V_Substring(kythue, 10)
            End If
            'If i <> 0 Then
            v_strRetRemark &= "####"

            'End If
        Next

        'End If
        v_strRetRemark = Globals.RemoveSign4VietnameseString(v_strRetRemark)
        intRowcount = v_intRowCnt

        Return v_strRetRemark
    End Function
    Private Function BuildDTLCitadXML(ByRef intRowcount As Integer) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim str_KTBatDau As String = "<VSTD>"
        Dim str_KTKetThuc As String = "</VSTD>"
        Dim v_strRetRemark As String = String.Empty

        For i = 0 To v_intRowCnt - 1
            v_strRetRemark &= str_KTBatDau
            v_strRetRemark &= "<MCH>" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text & "</MCH>"
            v_strRetRemark &= "<NDK>" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text & "</NDK>"
            v_strRetRemark &= "<STN>" & VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text, ".") & "</STN>"
            v_strRetRemark &= "<NDG>" & CType(grdChiTiet.Items(i).Controls(5).Controls(1), TextBox).Text & "</NDG>"
            v_strRetRemark &= str_KTKetThuc
        Next
        intRowcount = v_intRowCnt
        Return v_strRetRemark
    End Function
    Private Function BuildCKTM() As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim v_strRetRemark As String = String.Empty

        For i = 0 To v_intRowCnt - 1
            v_strRetRemark &= "C" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text
            v_strRetRemark &= "T" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text
        Next

        Return v_strRetRemark
    End Function

    Protected Sub grdDSCT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdDSCT.PageIndexChanging
        Dim v_ds As DataSet = GetDSCT(ddlTrangThai.SelectedValue)
        grdDSCT.PageIndex = e.NewPageIndex
        grdDSCT.DataSource = v_ds
        grdDSCT.DataBind()
    End Sub

    Protected Sub cmdChuyenThue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyenThue.Click
        Dim v_blnReturn As Boolean = False
        Try
            'Disable button tranh lam viec 2 lan
            EnabledButtonWhenDoTransaction(False)
            Dim strUser = mv_objUser.Ten_DN.ToString
            'Kiểm tra ngày làm việc
            If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
                lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới"
                'EnabledButtonWhenDoTransaction(True)
                Exit Sub
            End If

            'Kiểm tra mật khẩu kiểm soát
            'If Not (txtMatKhau.Text.Length = 0) Then
            '    If Not Business.KetNoi_LDAP.CheckPassWord(strUser, txtMatKhau.Text) Then
            '        lblStatus.Text = "Mật khẩu kiểm soát không hợp lệ.Hãy thử lại!"
            '        EnabledButtonWhenDoTransaction(False)
            '        Exit Sub
            '    End If
            'Else
            '    lblStatus.Text = "Phải nhập mật khẩu khi kiểm soát."
            '    EnabledButtonWhenDoTransaction(False)
            '    Exit Sub
            'End If
            cmdChuyenThue.Enabled = False
            'Kiểm tra có đang chuyển/nhân dữ liệu hay không
            'If CheckBathRun() <> "0" Then
            '    lblStatus.Text = "Hệ thống đang kết xuất dữ liệu sang kho bạc. Hãy chờ và kiểm soát lại"
            'Exit Sub
            'End If

            'Kiểm tra trạng thái của BDS hiện thời
            'If Check_Mode() <> gblnTTBDS Then
            '    lblStatus.Text = "Trạng thái BDS đã thay đổi. Hãy thoát khỏi chương trình và đăng nhập lại"
            'Exit Sub
            'End If

            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT
                Dim errcode As String = ""
                Dim errdesc As String = ""
                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    EnabledButtonWhenDoTransaction(False)
                    Exit Sub
                End If
                Dim blnSuccess As Boolean = True
                Try
                    'GIPBankInterface.capnhatCTU(txtMaNNT.Text, v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV)
                    'ProcMegService.ProMsg.getMSG21(txtMaNNT.Text, v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, errcode, errdesc)
                    'sonmt: cap nhan ham gui chung tu qua gip
                    ProcMegService.ProMsg.getMSG21(txtMaNNT.Text, v_strSHKB, v_strSo_CT, v_strSo_BT, v_strMa_NV, "01", errcode, errdesc)

                    If errcode = "01" Then
                        lblStatus.Text = "Chứng từ đã chuyển số liệu sang Cơ quan Thuế thành công"
                        cmdChuyenThue.Enabled = False
                    Else
                        blnSuccess = False
                        lblStatus.Text = "Chứng từ chưa chuyển được số liệu sang Cơ quan Thuế"
                        cmdChuyenThue.Enabled = False
                        Return
                    End If

                Catch ex As Exception
                    blnSuccess = False
                    lblStatus.Text = "Chứng từ chưa chuyển được số liệu sang Cơ quan Thuế. Lỗi: " & ex.Message
                    cmdChuyenThue.Enabled = False
                End Try
                Dim str_TTThue As String = String.Empty
                If v_strTrangThai_CT = "01" Then
                    If blnSuccess = True Then
                        str_TTThue = "1"
                    Else
                        str_TTThue = "0"
                    End If

                End If
                If v_strTrangThai_CT = "04" Then
                    If blnSuccess = True Then
                        str_TTThue = "0"
                    Else
                        str_TTThue = "1"
                    End If
                End If
                ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, "", "", "", "", "", "", str_TTThue)
            End If
            LoadDSCT(ddlTrangThai.SelectedValue)
        Catch ex As Exception

            LogApp.WriteLog("cmdChuyenThue.GIP", ex, "Lỗi trong quá trình chuyển sang thuế", HttpContext.Current.Session("TEN_DN"))

        Finally
        End Try
    End Sub

    Protected Sub cmdChuyenBDS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyenBDS.Click
        Dim v_blnReturn As Boolean = False

        Try
            'Disable button tranh lam viec 2 lan
            EnabledButtonWhenDoTransaction(False)

            'Kiểm tra ngày làm việc
            If mv_objUser.Ngay_LV <> clsCTU.GetWorkingDate() Then
                lblStatus.Text = "Ngày làm việc của hệ thống đã thay đổi.Hãy thoát khỏi chương trình để bắt đầu ngày làm việc mới"
                EnabledButtonWhenDoTransaction(True)
                Exit Sub
            End If

            'Kiểm tra mật khẩu kiểm soát
            'If Not (txtMatKhau.Text.Length = 0) Then
            '    If (mv_objUser.Mat_Khau.Equals(EncryptStr(txtMatKhau.Text))) = False Then
            '        lblStatus.Text = "Mật khẩu chuyển BDS không hợp lệ.Hãy thử lại"
            '        EnabledButtonWhenDoTransaction(True)
            '        Exit Sub
            '    End If
            'Else
            '    lblStatus.Text = "Phải nhập mật khẩu khi chuyển BDS."
            '    EnabledButtonWhenDoTransaction(True)
            '    Exit Sub
            'End If

            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai(key)
                Dim strTT_Form As String = v_strTrangThai_CT
                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    EnabledButtonWhenDoTransaction(False)
                    Exit Sub
                End If
                Dim strTongTienTT As String = txtTTIEN.Text.Replace(".", "")
                Dim v_strAmt As String = strTongTienTT 'txtTongTrichNo.Text.Replace(".", "") 'strTongTienTT
                Dim teller_id As String = clsCTU.Get_TellerID(v_strMa_NV) 'giao dich vien
                Dim approver_id As String = mv_objUser.TELLER_ID 'Kiem soat vien

                Dim v_strHostName As String = String.Empty
                Dim v_strDBName As String = String.Empty
                Dim v_strDBUser As String = String.Empty
                Dim v_strDBPass As String = String.Empty
                Dim v_strHost_REFNo As String = String.Empty
                Dim v_strBDS_REFNo As String = String.Empty

                Dim v_total_ID As String = "924"
                lblStatus.Text = "Không thể lấy tham số của BDS hãy kiểm tra lại."
                If (clsCTU.TCS_GetThamSoBDS(mv_objUser.MA_CN, v_strHostName, v_strDBName, v_strDBUser, v_strDBPass)) Then
                    Dim v_objBDS As New BDSHelper(v_strHostName, v_strDBName, v_strDBUser, v_strDBPass)
                    If v_objBDS.IncreaseTotal(v_total_ID, teller_id, v_strAmt) Then
                        'Cập nhật lại trạng thái của chứng từ thành đã kiểm soát
                        v_strTrangThai_CT = "01" 'Trạng thái đã kiểm soát
                        ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, v_strHost_REFNo, v_strBDS_REFNo, mv_objUser.Ma_NV)
                    Else
                        lblStatus.Text = "Không thể cập nhật số tiền vào BDS.Hãy kiểm tra lại"
                        EnabledButtonWhenDoTransaction(True)
                        Exit Sub
                    End If
                Else
                    lblStatus.Text = "Không thể lấy tham số của BDS hãy kiểm tra lại."
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If


                lblStatus.Text = "Kiểm soát chứng từ thành công.Xin chờ một lát để gửi thông tin sang thuế"

                'Chuyen thong tin sang thue
                Dim blnSuccess As Boolean = True
                Try

                    lblStatus.Text = "Chứng từ đã được cập nhật thông tin vào BDS và chuyển số liệu sang Cơ quan Thuế thành công"
                Catch ex As Exception
                    blnSuccess = False
                    lblStatus.Text = "Chứng từ đã được cập nhật thông tin vào BDS nhưng chưa chuyển được số liệu sang Cơ quan Thuế"
                    Exit Sub
                End Try

                'CAP NHAT TRANG THAI DA CHUYEN THUE
                v_strTrangThai_CT = "01" 'Trạng thái đã ks
                ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT, "", "", "", "", "", "", IIf(blnSuccess, "1", "0"))

                LoadDSCT(ddlTrangThai.SelectedValue)
                EnabledButtonWhenDoTransaction(True)

            End If
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình kiểm soát chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())
            ' Throw ex
        Finally
        End Try
    End Sub
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub cmdINCT_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdINCT.Click
        Try
            hdfInCTU.Value = "1"
        Catch ex As Exception

        End Try
    End Sub
    Public Shared Function V_Substring(ByVal pv_s As String, ByVal lenght As Integer) As String
        If Not pv_s Is Nothing And pv_s.Length > lenght Then
            pv_s = pv_s.Substring(0, lenght)
        End If
        Return pv_s
    End Function
End Class

