﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage01.master" AutoEventWireup="false"
    CodeFile="frmLogin.aspx.vb" Inherits="pages_HeThong_frmLogin" Title="Đăng nhập-Hệ thống thu thuế tập trung" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg01_MainContent" runat="Server">
    <div style="/*background-image: url(../images/login-ghep_02.gif)*/">
        <table id="Table10" cellspacing="0" cellpadding="0" border="0" height="450px" width="64%">
            <tr>
                <td valign="middle" align="center">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td width="9">
                                <img height="223" src="../images/left_box_login.gif" width="9">
                            </td>
                            <td style="border-top: #ededed 1px solid" valign="top" background="../images/bg_box_login.gif">
                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td valign="top" align="center" width="145">
                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                <tr>
                                                    <td valign="top" align="center">
                                                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td align="center">
                                                                    <img height="89" src="../images/img_key.gif" width="86">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top" align="center">
                                            <table cellspacing="0" cellpadding="0" width="90%" border="0">
                                                <tr>
                                                    <td height="30">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <span class="txt">
                                                            <asp:Label ID="lblError" runat="server" ForeColor="Red">Bạn phải đăng nhập trước khi sử dụng ! Nếu bạn 
										      chưa có tài khoản xin vui lòng thông báo cho quản trị hệ thống !</asp:Label></span>
                                                    </td>
                                                </tr>
                                                <tr height="20">
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table cellspacing="2" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td align="left" width="30%">
                                                                    <strong>Tên truy cập</strong>
                                                                </td>
                                                                <td align="right" colspan="2">
                                                                    <input id="txtUser" type="text" style="font-size: 8pt; font-family: Arial; width: 90%"
                                                                        class="textbox" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <strong>Mật khẩu</strong>
                                                                </td>
                                                                <td align="right" colspan="2">
                                                                    <input id="txtPass" type="password" style="font-size: 8pt; font-family: Arial; width: 90%"
                                                                        class="textbox" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="30">
                                                                    &nbsp;
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                                <td align="right">
                                                                    <asp:ImageButton ID="cmdLogin" runat="server" ImageUrl="../images/bt_login.gif" ImageAlign="AbsMiddle">
                                                                    </asp:ImageButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="10">
                                <img src="../images/right_box_login.gif" width="10">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
