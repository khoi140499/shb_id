﻿Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports Business
Imports System.Data
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Net
Imports CorebankServiceESB

Partial Class pages_HeThong_frmLogin
    Inherits System.Web.UI.Page
    Private acceptConnect As String = ConfigurationManager.AppSettings.Get("LDAP").ToString()
    Private maxWrongLogin As Integer = 3

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Session.RemoveAll()
        Session.Remove("User")
        Session.Remove("MA_CN_USER_LOGON")
        Session.Remove("UserName")
        Session.Remove("MA_NHOM")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Me.txtUser.Attributes.Add("onKeyDown", "if(event.keyCode==13) {event.keyCode=9;}")
        'Me.txtPass.Attributes.Add("onKeyDown", "if(event.keyCode==13) {event.keyCode=9;}")
        txtUser.Focus()
    End Sub

    Protected Sub cmdLogin_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles cmdLogin.Click
        ' Dim Action As String = "LOGIN"
        'LogApp.AddAction(Action, "OK")

        'Duc anh: Log bat dau lay gia tri tham so MAX_LOGIN_WRONG : Start
        Dim dtStartGetMaxLoginParam As DateTime = DateTime.Now
        Dim strGetMaxLoginParam As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Bat dau lay gia tri tham so MAX_LOGIN_WRONG. UserName:" + txtUser.Value.Trim()
        Log_Action_Login(strGetMaxLoginParam)
        'Duc anh: Log bat dau lay gia tri tham so MAX_LOGIN_WRONG : End
        Dim dtTableMaxLoginWrong As DataTable = DataAccess.ExecuteToTable("select giatri_ts from tcs_thamso_ht where ten_ts='MAX_LOGIN_WRONG'")
        'Duc anh: Log ket thuc kiem tra LDAP hay ko : Start
        Dim dtEndGetMaxLoginParam As DateTime = DateTime.Now
        Dim totalSecondGetMaxLoginParam As String = CalculateTimeDiff(dtStartGetMaxLoginParam, dtEndGetMaxLoginParam)
        Dim strEndLogGetMaxLoginParamString As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Ket thuc lay gia tri tham so MAX_LOGIN_WRONG. UserName:" + txtUser.Value.Trim() + " - TotalSecond: " + totalSecondGetMaxLoginParam
        Log_Action_Login(strEndLogGetMaxLoginParamString)
        'Duc anh: Log ket thuc kiem tra LDAP hay ko : End

        If Not dtTableMaxLoginWrong Is Nothing And dtTableMaxLoginWrong.Rows.Count > 0 Then
            maxWrongLogin = Integer.Parse(dtTableMaxLoginWrong.Rows(0)("giatri_ts").ToString())
        End If

        Dim cnUser As DataAccess
        Dim transCT As IDbTransaction
        Dim strLog_DN As String = ""
        cnUser = New DataAccess
        Try
            If ValidForm() Then

                Dim strSQL_LDAP As String
                Dim strUser As String = txtUser.Value.Trim()
                Dim strPass As String = txtPass.Value.Trim()
                Dim strUserDomain As String = ""
                Dim strIP_Local = Request.ServerVariables("REMOTE_HOST").ToString()
                ''Kiem tra User co duoc dang nhap hay khong ***
                '    Dim strSQL_User As String = "select ldap from tcs_dm_nhanvien where UPPER (ten_dn) = UPPER ('" & strUser & "') and ma_nv <> 1"
                '    Dim dt_User As New DataTable
                '    dt_User = DataAccess.ExecuteToTable(strSQL_User)
                '    If Not dt_User Is Nothing And dt_User.Rows.Count > 0 Then
                '        Me.lblError.Text = "Tài khoản của bạn hiện đang đăng nhập trên một máy khác hoặc bạn đã thoát không đúng cách. Xin vui lòng liên hệ quản trị hệ thống!!!"
                '        Exit Sub
                '    End If
                ''Ket thuc ***

                'Kiem tra LDAP hay ko
                'strSQL_LDAP = "select * from tcs_dm_nhanvien where UPPER (ten_dn) = UPPER ('" & strUser & "') AND tinh_trang='0'"
                'Duc anh: Log bat dau lay thong tin tai khoan qua username : Start
                Dim dtStartLogGetInfo As DateTime = DateTime.Now
                Dim strStartLogGetInfoString As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Bat dau lay thong tin nhan vien qua username. UserName:" + strUser
                Log_Action_Login(strStartLogGetInfoString)
                'Duc anh: Log bat dau lay thong tin tai khoan qua username : End
                strSQL_LDAP = "select * from tcs_dm_nhanvien where UPPER (ten_dn) = UPPER ('" & strUser & "')  AND tinh_trang='0'"
                'Duc anh: Log ket thuc lay thong tin tai khoan qua username : Start
                Dim dtEndLogGetInfo As DateTime = DateTime.Now
                Dim totalSecondGetInfo As String = CalculateTimeDiff(dtStartLogGetInfo, dtEndLogGetInfo)
                Dim strEndLogLDAPString As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Ket thuc lay thong tin nhan vien qua username. UserName:" + strUser + " - TotalSecond: " + totalSecondGetInfo
                Log_Action_Login(strEndLogLDAPString)
                'Duc anh: Log ket thuc lay thong tin tai khoan qua username : End

                Dim dt_LDAP As New DataTable
                Dim dt As New DataTable
                Dim strMA_NHOM_ADMIN As String = ""
                dt_LDAP = DataAccess.ExecuteToTable(strSQL_LDAP)
                'Ket thuc
                If Not dt_LDAP Is Nothing And dt_LDAP.Rows.Count > 0 Then

                    'Duc Anh - Check tài khoản có bị khóa hay không : Start
                    Dim tinhTrang As String = dt_LDAP.Rows(0)("tinh_trang").ToString()

                    If tinhTrang = "1" Then
                        lblError.Text = "Tài khoản của bạn đã bị khóa. Vui lòng liên hệ với quản trị hệ thống để kích hoạt lại tài khoản"
                        Return
                    End If
                    'Duc Anh - Check tài khoản có bị khóa hay không : Start

                    'Duc Anh - Check so lan dang nhap sai : Start
                    Dim MA_NV As String = dt_LDAP.Rows(0)("MA_NV").ToString()
                    Dim strWrongLogCount As String = dt_LDAP.Rows(0)("LAN_DN_SAI").ToString()

                    Dim wrongLogCount As Integer = 0
                    If strWrongLogCount <> "" Then
                        wrongLogCount = Integer.Parse(strWrongLogCount)
                    End If

                    If wrongLogCount >= maxWrongLogin Then
                        Me.lblError.Text = "Bạn đã đăng nhập sai quá " + maxWrongLogin.ToString() + " lần. Tài khoản của bạn đã bị khóa. Vui lòng liên hệ với quản trị hệ thống để mở khóa tài khoản"
                        HeThong.buUsers.UpdateTinhTrang(MA_NV, "1")
                        Return
                    End If
                    'Duc Anh - Check so lan dang nhap sai : End

                    'Duc Anh - Check user có sử dụng tài khoản trong vòng 90 ngày hay không : Start
                    'Duc anh: Log bat dau lay thoi gian dang nhap cuoi cung : Start
                    Dim dtStartLogGetLastLogin As DateTime = DateTime.Now
                    Dim strStartLogGetLastLogin As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Bat dau lay thoi gian dang nhap cuoi cung. UserName:" + strUser
                    Log_Action_Login(strStartLogGetLastLogin)
                    'Duc anh: Log bat dau lay thoi gian dang nhap cuoi cung : End
                    Dim strGetLastLogin As String = "select * from ( select ten_dn,ma_nv,action_time,status,ma_cn,row_number() over(order by action_time desc nulls last) rnm from TCS_LOG_DANGNHAP where ma_nv='" & MA_NV & "' order by action_time desc) where rnm=1"
                    Dim dtGetLastLogin As DataTable = DataAccess.ExecuteToTable(strGetLastLogin)
                    'Duc anh: Log ket thuc kiem tra LDAP hay ko : Start
                    Dim dtEndLogGetLastLogin As DateTime = DateTime.Now
                    Dim totalSecondGetLastLogin As String = CalculateTimeDiff(dtStartLogGetLastLogin, dtEndLogGetLastLogin)
                    Dim strEndLogGetLastLoginString As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Ket thuc lay thoi gian dang nhap cuoi cung. UserName:" + strUser + " - TotalSecond: " + totalSecondGetLastLogin
                    Log_Action_Login(strEndLogGetLastLoginString)
                    'Duc anh: Log ket thuc kiem tra LDAP hay ko : End

                    If dtGetLastLogin.Rows.Count > 0 Then
                        Dim dtLastLogin As DateTime = Convert.ToDateTime(dtGetLastLogin.Rows(0)("action_time").ToString())
                        Dim dtCurrentTime As DateTime = Convert.ToDateTime(DataAccess.ExecuteSQLScalar("SELECT TO_CHAR(SYSDATE) FROM DUAL").ToString())
                        Dim notUseDateCount As Long = DateDiff(DateInterval.Day, dtLastLogin, dtCurrentTime)
                        Dim dayTimeCount As Integer = Integer.Parse(DataAccess.ExecuteSQLScalar("select GIATRI_TS from TCS_THAMSO_HT where TEN_TS='DAY_TIMEOUT_TO_LOCK_ACCOUNT'").ToString())

                        If dayTimeCount <= notUseDateCount Then
                            HeThong.buUsers.UpdateTinhTrang(MA_NV, "1")
                            lblError.Text = "Bạn đã không sử dụng tài khoản trong vòng " & dayTimeCount.ToString() & " ngày. Tài khoản của bạn đã bị khóa tạm thời. Vui lòng liên hệ với quản trị hệ thống để kích hoạt lại tài khoản"
                            Return
                        End If
                    End If
                    'Duc Anh - Check user có sử dụng tài khoản trong vòng 90 ngày hay không : End

                    If dt_LDAP.Rows(0)("LDAP").ToString() = "0" Then
                        strUser = dt_LDAP.Rows(0)("ten_dn").ToString()
                        strUserDomain = dt_LDAP.Rows(0)("userdomain").ToString()
                        strMA_NHOM_ADMIN = dt_LDAP.Rows(0)("MA_NHOM").ToString()
                        If (acceptConnect.Equals("true")) Then
                            'Duc anh: Log bat dau ket noi LDAP : Start
                            Dim dtStartLogLDAPConnect As DateTime = DateTime.Now
                            Dim strStartLogLDAPConnect As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Bat dau ket noi LDAP. UserName:" + strUser
                            Log_Action_Login(strStartLogLDAPConnect)
                            'Duc anh: Log bat dau ket noi LDAP : End
                            Dim checkConnectLDAP As Boolean = Business.KetNoi_LDAP.IsAuthenticated(strUser, strUserDomain, strPass)
                            'Duc anh: Log ket thuc ket noi LDAP : Start
                            Dim dtEndLogLDAPConnect As DateTime = DateTime.Now
                            Dim totalSecondLDAPConnect As String = CalculateTimeDiff(dtStartLogLDAPConnect, dtEndLogLDAPConnect)
                            Dim strEndLogLDAPConnectString As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Ket thuc ket noi LDAP. UserName:" + strUser + " - TotalSecond: " + totalSecondLDAPConnect
                            Log_Action_Login(strEndLogLDAPConnectString)
                            'Duc anh: Log ket thuc ket noi LDAP : End
                            If (checkConnectLDAP = False) Then
                                Me.txtUser.Value = ""
                                Me.txtPass.Value = ""
                                Me.lblError.Text = "Thông tin LDAP (Tên đăng nhập hoặc mật khẩu truy cập) không hợp lệ!"
                                'Duc anh: Log bat log dang nhap trong DB : Start
                                Dim dtStartLogInsertLogLogin As DateTime = DateTime.Now
                                Dim strStartLogInsertLogLogin As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Bat dau log dang nhap trong DB. UserName:" + strUser
                                Log_Action_Login(strStartLogInsertLogLogin)
                                'Duc anh: Log bat dau log dang nhap trong DB : End
                                HeThong.buUsers.Insert_Log_DN(dt_LDAP.Rows(0)("MA_NV").ToString(), strUser, strIP_Local.ToString(), 0, dt_LDAP.Rows(0)("MA_CN").ToString().ToString(), "")
                                'Duc anh: Log ket log dang nhap trong DB : Start
                                Dim dtEndLogInsertLogLogin As DateTime = DateTime.Now
                                Dim totalSecondInsertLogLogin As String = CalculateTimeDiff(dtStartLogInsertLogLogin, dtEndLogInsertLogLogin)
                                Dim strEndLogInsertLogLoginString As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Ket thuc log dang nhap trong DB:" + strUser + " - TotalSecond: " + totalSecondInsertLogLogin
                                Log_Action_Login(strEndLogInsertLogLoginString)
                                'Duc anh: Log ket log dang nhap trong DB : End
                                Return
                            End If
                            'END CHECK LDAP
                        End If
                    Else
                        If dt_LDAP.Rows(0)("MAT_KHAU").ToString() <> EncryptStr(strPass) Then
                            Me.txtUser.Value = ""
                            Me.txtPass.Value = ""
                            Me.lblError.Text = "Tên đăng nhập hoặc mật khẩu truy cập không hợp lệ!"

                            If wrongLogCount < maxWrongLogin Then
                                'Duc anh: Log bat dau them lan dang nhap sai : Start
                                Dim dtStartLogAddWrongLogin As DateTime = DateTime.Now
                                Dim strStartLogAddWrongLogin As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Bat dau log dang nhap trong DB. UserName:" + strUser
                                Log_Action_Login(strStartLogAddWrongLogin)
                                'Duc anh: Log bat dau them lan dang nhap sai : End
                                HeThong.buUsers.Update_WrongLoginAcount(MA_NV, (wrongLogCount + 1).ToString())
                                'Duc anh: Log ket thuc them lan dang nhap sai : Start
                                Dim dtEndLogAddWrongLogin As DateTime = DateTime.Now
                                Dim totalSecondAddWrongLogin As String = CalculateTimeDiff(dtStartLogAddWrongLogin, dtEndLogAddWrongLogin)
                                Dim strEndLogAddWrongLoginString As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Ket thuc log dang nhap trong DB:" + strUser + " - TotalSecond: " + totalSecondAddWrongLogin
                                Log_Action_Login(strEndLogAddWrongLoginString)
                                'Duc anh: Log ket thuc them lan dang nhap sai : End
                            End If

                            Return
                        End If
                    End If

                    Session.Item("User") = dt_LDAP.Rows(0)("MA_NV").ToString()
                    Session.Item("MA_CN_USER_LOGON") = dt_LDAP.Rows(0)("MA_CN").ToString()
                    Session.Item("UserName") = dt_LDAP.Rows(0)("TEN_DN").ToString()
                    Session.Item("MA_NHOM") = dt_LDAP.Rows(0)("MA_NHOM").ToString()
                    Session.Item("dsMenu") = Nothing
                    GetGlobalVars(dt_LDAP.Rows(0)("BAN_LV").ToString())
                    Dim v_strSessionID As String = ""
                    v_strSessionID = Session.SessionID
                    'Check xem co session chua
                    Dim v_strSqlS As String = "SELECT COUNT(1) FROM TCS_DM_SESSION WHERE MA_NV = '" & Session.Item("User") & "'"

                    Dim v_intCount As Integer = Integer.Parse(DataAccess.ExecuteSQLScalar(v_strSqlS))
                    If v_intCount = 0 Then
                        v_strSqlS = "INSERT INTO TCS_DM_SESSION VALUES(SEQ_TCS_DM_SESSION.NEXTVAL,'" & Session.Item("User") & "','" & v_strSessionID & "')"
                    Else
                        v_strSqlS = "UPDATE TCS_DM_SESSION SET SESSION_CODE= '" & v_strSessionID & "' WHERE MA_NV = '" & Session.Item("User") & "'"
                    End If
                    'Duc anh: Log bat check session ton tai hay chua : Start
                    Dim dtStartLogCheckSession As DateTime = DateTime.Now
                    Dim strStartLogCheckSession As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Bat dau check session ton tai hay chua. UserName:" + strUser
                    Log_Action_Login(strStartLogCheckSession)
                    'Duc anh: Log bat dau check session ton tai hay chua : End
                    DataAccess.ExecuteNonQuery(v_strSqlS, CommandType.Text)
                    'Duc anh: Log ket thuc check session ton tai hay chua : Start
                    Dim dtEndLogCheckSession As DateTime = DateTime.Now
                    Dim totalSecondCheckSession As String = CalculateTimeDiff(dtStartLogCheckSession, dtEndLogCheckSession)
                    Dim strEndLogCheckSessionString As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Ket thuc check session ton tai hay chua:" + strUser + " - TotalSecond: " + totalSecondCheckSession
                    Log_Action_Login(strEndLogCheckSessionString)
                    'Duc anh: Log ket thuc check session ton tai hay chua : End

                    'Check xem co session chua
                    'Cap nhap log dang nhap
                    'Duc anh: Log bat log dang nhap trong DB : Start
                    Dim dtStartLogInsertLogLogin1 As DateTime = DateTime.Now
                    Dim strStartLogInsertLogLogin1 As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Bat dau log dang nhap trong DB. UserName:" + strUser
                    Log_Action_Login(strStartLogInsertLogLogin1)
                    'Duc anh: Log bat dau log dang nhap trong DB : End
                    HeThong.buUsers.Insert_Log_DN(Session.Item("User").ToString().Trim(), strUser, strIP_Local.ToString(), 1, Session.Item("MA_CN_USER_LOGON").ToString(), "")
                    'Duc anh: Log ket log dang nhap trong DB : Start
                    Dim dtEndLogInsertLogLogin1 As DateTime = DateTime.Now
                    Dim totalSecondInsertLogLogin1 As String = CalculateTimeDiff(dtStartLogInsertLogLogin1, dtEndLogInsertLogLogin1)
                    Dim strEndLogInsertLogLoginString1 As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Ket thuc log dang nhap trong DB:" + strUser + " - TotalSecond: " + totalSecondInsertLogLogin1
                    Log_Action_Login(strEndLogInsertLogLoginString1)
                    'Duc anh: Log ket log dang nhap trong DB : End

                    'Duc anh: Log bat dau cap nhat trang thai login : Start
                    Dim dtStartLogUpdateLoginStatus As DateTime = DateTime.Now
                    Dim strStartLogUpdateLoginStatus As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Bat dau cap nhat trang thai login. UserName:" + strUser
                    Log_Action_Login(strStartLogUpdateLoginStatus)
                    'Duc anh: Log bat dau cap nhat trang thai login : End
                    HeThong.buUsers.UpdateTTLogin(dt_LDAP.Rows(0)("MA_NV").ToString(), "1", strIP_Local)
                    'Duc anh: Log ket log dang nhap trong DB : Start
                    Dim dtEndLogUpdateLoginStatus As DateTime = DateTime.Now
                    Dim totalSecondUpdateLoginStatus As String = CalculateTimeDiff(dtStartLogUpdateLoginStatus, dtEndLogUpdateLoginStatus)
                    Dim strEndLogUpdateLoginStatusString As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Ket thuc cap nhat trang thai login:" + strUser + " - TotalSecond: " + totalSecondUpdateLoginStatus
                    Log_Action_Login(strEndLogInsertLogLoginString1)
                    'Duc anh: Log ket log dang nhap trong DB : End

                    If wrongLogCount <= maxWrongLogin Then
                        'Duc anh: Log bat dau cap nhat so lan dang nhap sai ve 0 : Start
                        Dim dtStartLogUpdateWrongLoginTo0 As DateTime = DateTime.Now
                        Dim strStartLogUpdateWrongLoginTo0 As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Bat dau cap nhat trang thai login. UserName:" + strUser
                        Log_Action_Login(strStartLogUpdateWrongLoginTo0)
                        'Duc anh: Log bat dau cap nhat so lan dang nhap sai ve 0 : End
                        HeThong.buUsers.Update_WrongLoginAcount(MA_NV, "0")
                        'Duc anh: Log ket log dang nhap trong DB : Start
                        Dim dtEndLogUpdateWrongLoginTo0 As DateTime = DateTime.Now
                        Dim totalSecondUpdateWrongLoginTo0 As String = CalculateTimeDiff(dtStartLogUpdateWrongLoginTo0, dtEndLogUpdateWrongLoginTo0)
                        Dim strEndLogUpdateWrongLoginTo0String As String = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + " - Ket thuc cap nhat trang thai login:" + strUser + " - TotalSecond: " + totalSecondUpdateWrongLoginTo0
                        Log_Action_Login(strEndLogInsertLogLoginString1)
                        'Duc anh: Log ket log dang nhap trong DB : End
                    End If
                    Log_Action_Login("----------------------------------------------------------------------------------------------" + Environment.NewLine + Environment.NewLine + Environment.NewLine)
                    Response.Redirect("frmHomeNV.aspx", False)
                Else
                    Log_Action_Login("----------------------------------------------------------------------------------------------" + Environment.NewLine + Environment.NewLine + Environment.NewLine)
                    Me.lblError.Text = "Có lỗi khi đăng nhập hệ thống.Xin vui lòng đăng nhập lại hoặc liên hệ quản trị hệ thống!!!"
                End If
            End If
        Catch ex As Exception
            Me.lblError.Text = "Có lỗi khi đăng nhập hệ thống.Xin vui lòng đăng nhập lại hoặc liên hệ quản trị hệ thống!!!"
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' LogDebug.WriteLog(ex.Message, Diagnostics.EventLogEntryType.Error)
        End Try
    End Sub

    Private Function ValidForm() As Boolean
        If Me.txtUser.Value = "" Then
            clsCommon.ShowMessageBox(Me, "Tên đăng nhập không được để trống!!!")
            Return False
        End If
        If Me.txtPass.Value = "" Then
            clsCommon.ShowMessageBox(Me, "Mật khẩu không được để trống!!!")
            Return False
        End If
        Return True
    End Function

    Private Sub Log_Action_Login(ByVal strLogContent As String)
        Dim strLog_Login_Path As String = ConfigurationManager.AppSettings.Get("FILE_LOG_LOGIN").ToString()
        ETAX_GDT.Utility.Log_Action_CustomPath(strLogContent, strLog_Login_Path)
    End Sub

    Protected Function CalculateTimeDiff(ByVal dtStartTime As DateTime, ByVal dtEndTime As DateTime) As String
        Dim TotalMilisecond As Double = (dtEndTime - dtStartTime).TotalMilliseconds
        Dim diffTime As TimeSpan = TimeSpan.FromMilliseconds(TotalMilisecond)
        Dim TotalSecond As Double = diffTime.TotalSeconds
        Return TotalSecond.ToString()
    End Function

    'public static void Log_Action(string Tran_Type,string MSG_TEXT)
    '    {
    '        StreamWriter fileWriter = null;
    '        string filePath = strLogPath + DateTime.Now.ToString("_yyyy_MM_dd") + ".txt";
    '        string datetime = DateTime.Now.ToString("dd/MM/yyyy HH:MM:ss");

    '    If (File.Exists(filePath)) Then
    '            fileWriter = File.AppendText(filePath);
    '    Else
    '            fileWriter = File.CreateText(filePath);
    '        Try
    '        {
    '            fileWriter.Write(datetime + " : " + Tran_Type + " MSG=[" + MSG_TEXT + "]");
    '            fileWriter.WriteLine();
    '        }
    '        Finally
    '        {
    '            fileWriter.Close();
    '        }
    '    }

End Class
