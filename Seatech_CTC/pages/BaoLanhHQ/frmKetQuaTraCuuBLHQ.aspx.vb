﻿Imports System.Data
Imports VBOracleLib
Imports Business
Imports Business.NewChungTu
Imports Business.Common.mdlCommon

Partial Class pages_BaoLanhHQ_frmKetQuaTraCuuBLHQ
    Inherits System.Web.UI.Page

    Private mv_objUser As New CTuUser
    Private Shared i_PageIndex As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler btnSearch.Click, AddressOf btnSearch_Click
            Exit Sub
        End If
        Try
            LoadForm()
            Load_ddl_LoaiBL()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub dtgDSCT_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgDSCT.PageIndexChanged
        Try
            LayDSCT(Session.Item("para").ToString)
            dtgDSCT.CurrentPageIndex = e.NewPageIndex
            dtgDSCT.DataBind()
            i_PageIndex = e.NewPageIndex
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

#Region "ButtonClick"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Response.Redirect("frmTraCuuBLHQ.aspx")
    End Sub
#End Region

#Region "Function"

    Sub LoadForm()
        Dim v_strSHKB As String = String.Empty
        Dim v_strMaDiemThu As String = String.Empty
        Dim strNgay_LV As String = ""

        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not Session.Item("User") Is Nothing Then
            v_strSHKB = clsCTU.TCS_GetKHKB(CType(Session("User"), Integer))
            v_strMaDiemThu = clsCTU.TCS_GetMaDT(CType(Session("User"), Integer))
            mv_objUser = New CTuUser(Session.Item("User"))
        End If

        If Not IsPostBack Then
            Try
                hdnSHKB.Value = v_strSHKB
                hdnMaNV.Value = mv_objUser.Ma_NV

                'Lay danh sach bao lanh theo dieu kien tim kiem
                If Not Request.Params("para") Is Nothing Then
                    Session.Item("para") = Request.Params("para")
                    i_PageIndex = 0
                End If

                LayDSCT(Session.Item("para").ToString)

                ' View chi tiet bao lanh
                If Not Request.Params("SHKB") Is Nothing And Not Request.Params("Ngay_KB") Is Nothing And Not Request.Params("So_BT") Is Nothing And Not Request.Params("Ma_NV") Is Nothing Then
                    Dim strSHKB As String = Request.Params("SHKB").Trim
                    Dim strNgayKB As String = Request.Params("Ngay_KB").Trim
                    Dim strMaNV As String = Request.Params("Ma_NV").Trim
                    Dim strSoCT As String = Request.Params("So_CT").Trim
                    Dim strSoBT As String = Request.Params("So_BT").Trim

                    Load_Detail(strSHKB, strNgayKB, strSoCT, strSoBT, strMaNV)
                End If
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub

    Sub LayDSCT(ByVal strPara As String)
        Dim dtCT As DataTable

        'Lay danh sach so chung tu
        dtCT = dtlCTU_Load(strPara, Session.Item("User").ToString)
        If dtCT.Rows.Count > 0 Then
            dtgDSCT.DataSource = dtCT
            dtgDSCT.CurrentPageIndex = i_PageIndex
            dtgDSCT.DataBind()
            lblTongSo.Text = dtCT.Rows.Count
            i_PageIndex = 0
        Else
            dtgDSCT.DataSource = Nothing
            dtgDSCT.DataBind()
            lblTongSo.Text = "0"
            i_PageIndex = 0
        End If
    End Sub

    Private Function dtlCTU_Load(ByVal strPara As String, ByVal strUser As String) As DataTable
        Dim dt As New DataTable
        Dim strSQL As String
        Dim strWhereSTT As String = ""
        Dim SPLIT As String = "zzz"

        Dim arr() As String = Regex.Split(strPara, SPLIT)

        Dim strSoBL As String = arr(0)
        Dim strSoCT As String = arr(1)
        Dim strSoTKhai As String = arr(2)
        Dim strTrangThai As String = arr(3)
        Dim strTuNgayBL As String = arr(4)
        Dim strDenNgayBL As String = arr(5)
        Dim strChiNhanh As String = arr(6)
        Dim strSoTien As String = arr(7)
        Dim strNguoiLap As String = arr(8)
        Dim strTTTruyenHQ As String = arr(9)
        Dim strLoaiBCao As String = arr(10)
        Dim strNgayKS As String = arr(11)
        Dim strNgayLap As String = arr(12)
        Dim strCif As String = arr(13)
        Dim strPhanCap As String = arr(14)
        Try
            If strSoCT <> "" Then
                strWhereSTT = strWhereSTT & " AND UPPER(hdr.So_CT) LIKE '%" & strSoCT.ToUpper() & "%'"
            End If
            If strSoBL <> "" Then
                strWhereSTT = strWhereSTT & " AND UPPER(hdr.SO_BL) LIKE '%" & strSoBL.ToUpper() & "%'"
            End If
            If strSoTKhai <> "" Then
                strWhereSTT = strWhereSTT & " AND hdr.SO_TK LIKE '%" & strSoTKhai & "%'"
            End If
            If strTrangThai <> "" Then
                If strTrangThai = "04" Then
                    strWhereSTT = strWhereSTT & " AND (hdr.TRANG_THAI = " & Globals.EscapeQuote(strTrangThai) & " AND hdr.MA_KS = 0) "
                ElseIf strTrangThai = "09" Then
                    strWhereSTT = strWhereSTT & " AND (hdr.TRANG_THAI = '04' AND hdr.MA_KS <> 0) "
                ElseIf strTrangThai = "06" Then
                    strWhereSTT = strWhereSTT & "and (hdr.TRANG_THAI = '01' and hdr.trang_thai_tt <> 0)"
                ElseIf strTrangThai = "01" Then
                    strWhereSTT = strWhereSTT & "and (hdr.TRANG_THAI = '01' and hdr.trang_thai_tt = 0)"
                ElseIf strTrangThai = "02" Then
                    strWhereSTT = strWhereSTT & "and (hdr.TRANG_THAI = '02')"
                ElseIf strTrangThai = "08" Then
                    strWhereSTT = strWhereSTT & "and (hdr.TRANG_THAI = '04' AND hdr.MA_KS <> 0 )"
                Else
                    strWhereSTT = strWhereSTT & " AND hdr.TRANG_THAI = " & Globals.EscapeQuote(strTrangThai)
                End If
            End If
            If strTuNgayBL <> "" Then
                strWhereSTT = strWhereSTT & " AND TO_CHAR(hdr.NGAY_BATDAU,'yyyymmdd') >= " & strTuNgayBL
            End If
            If strDenNgayBL <> "" Then
                strWhereSTT = strWhereSTT & " AND TO_CHAR(hdr.NGAY_KETTHUC,'yyyymmdd') <= " & strDenNgayBL
            End If
            If strSoTien <> "" Then
                strWhereSTT = strWhereSTT & " AND hdr.TTIEN = " & strSoTien
            End If
            If strNguoiLap <> "" Then
                strWhereSTT = strWhereSTT & " AND hdr.TEN_NV LIKE '%" & strNguoiLap & "%'"
            End If
            If strNgayKS <> "" Then
                strWhereSTT = strWhereSTT & " AND to_char(hdr.ngay_ks,'dd/mm/yyyy') = '" & strNgayKS & "'"
            End If
            If strNgayLap <> "" Then
                strWhereSTT = strWhereSTT & " AND hdr.ngay_ct = '" & ConvertDateToNumber(strNgayLap) & "'"
            End If
            If strCif <> "" Then
                strWhereSTT = strWhereSTT & " AND hdr.cif = '" & strCif & "'"
            End If
            'If Not strChiNhanh Is Nothing Then
            '    If strChiNhanh.Length > 0 Then
            '        'If strChiNhanh.Substring(3, 2) = "00" Then
            '        strWhereSTT = strWhereSTT & " and hdr.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn in (select cn.id from tcs_dm_chinhanh cn where TO_NUMBER(cn.branch_id)= " & CInt(strChiNhanh) & ")) "
            '    Else
            '        strWhereSTT = strWhereSTT & " and hdr.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn in (select cn.id from tcs_dm_chinhanh cn where cn.branch_id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "')) "
            '    End If
            '    'End If
            'ElseIf strChiNhanh.Equals("") Then

            'End If 
            If strChiNhanh Is Nothing Or strChiNhanh = "" Then
                If strPhanCap = "2" Then
                    If strChiNhanh <> Session.Item("MA_CN_USER_LOGON").ToString Then
                        strWhereSTT = strWhereSTT & " and hdr.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv ,tcs_dm_chinhanh d where nv.ma_cn = d.id and d.id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "')"
                    Else
                        strWhereSTT = strWhereSTT & " and hdr.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv ,tcs_dm_chinhanh d where nv.ma_cn = d.id and d.brach_id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "')"
                    End If

                ElseIf strPhanCap = "0" Then
                    strWhereSTT = strWhereSTT & " and hdr.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn='" & Session.Item("MA_CN_USER_LOGON").ToString & "')"
                End If
            Else
                If strPhanCap = "2" Then
                    strWhereSTT = strWhereSTT & " and hdr.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv ,tcs_dm_chinhanh d where nv.ma_cn='" & strChiNhanh & "')"
                ElseIf strPhanCap = "0" Then
                    strWhereSTT = strWhereSTT & " and hdr.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn='" & Session.Item("MA_CN_USER_LOGON").ToString & "')"
                Else
                    strWhereSTT = strWhereSTT & " and hdr.ma_nv in (select nv.ma_nv from tcs_dm_nhanvien nv where nv.ma_cn='" & strChiNhanh & "')"
                End If
            End If

            strSQL = " SELECT " & _
                         " hdr.SHKB, hdr.KYHIEU_CT, hdr.SO_CT, hdr.SO_BT,hdr.SO_BL, hdr.Ma_NV, hdr.SO_BT || '/' || hdr.Ma_NV Ten, hdr.TRANG_THAI TT, hdr.MA_DTHU, hdr.TTien, hdr.NGAY_KB, hdr.MA_XA,hdr.TEN_NV,nv.TEN_DN, " & _
                         " Case TRANG_THAI " & _
                         " when '00' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("00") & " alt=" & Business.CTuCommon.Get_TrangThai("00") & "/>' " & _
                         " when '01' then case TRANG_THAI_TT when 0 then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("01", "1") & " alt=" & Business.CTuCommon.Get_TrangThai("01") & "/>' when 1 then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("01", "1", "0", "1") & " alt=" & Business.CTuCommon.Get_TrangThai("07") & "/>' END  " & _
                         " when '03' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("03") & " alt=" & Business.CTuCommon.Get_TrangThai("03") & "/>' " & _
                         " when '04' then case ma_ks when '0' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("04") & " alt=" & Business.CTuCommon.Get_TrangThai("04") & "/>' else '<img src=" & Business.CTuCommon.Get_ImgTrangThai("04", "0", "1") & " alt=" & Business.CTuCommon.Get_TrangThai("04") & "/>' end " & _
                         " when '05' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("05") & " alt=" & Business.CTuCommon.Get_TrangThai("05") & "/>' " & _
                         " when '02' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("02") & " alt=" & Business.CTuCommon.Get_TrangThai("02") & "/>' " & _
                         " when '07' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("07") & " alt=" & Business.CTuCommon.Get_TrangThai("07") & "/>' " & _
                         " End TRANG_THAI" & _
                     " FROM tcs_baolanh_hdr hdr, tcs_dm_nhanvien nv  " & _
                     " WHERE hdr.ma_nv=nv.Ma_nv " & strWhereSTT & _
                             " ORDER BY so_bt DESC "

            dt = DatabaseHelp.ExecuteToTable(strSQL)
            Return dt
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Sub Load_Detail(ByVal strSHKB As String, ByVal strNgayKB As String, ByVal strSoCT As String, ByVal strSoBT As String, ByVal strMaNV As String)
        Dim hdr As New DataTable
        Dim dtls As New DataTable
        clsCTU.TCS_GetBaoLanh(strSHKB, strSoCT, strSoBT, strMaNV, hdr, dtls, strNgayKB)

        ' Dua du lieu vao form
        If hdr.Rows.Count > 0 Then
            hdnSoBT.Value = hdr.Rows(0)("so_bt").ToString()
            hdnSoCT.Value = hdr.Rows(0)("so_ct").ToString()
            hdnKHCT.Value = hdr.Rows(0)("kyhieu_ct").ToString()
            hdnNgayCT.Value = hdr.Rows(0)("ngay_ct").ToString()
            hdnTThai.Value = hdr.Rows(0)("trang_thai").ToString()
            hdnMaDThu.Value = hdr.Rows(0)("ma_dthu").ToString()

            hdnMaNNT.Value = hdr.Rows(0)("ma_nnthue").ToString()
            hdnTongSoNgay.Value = hdr.Rows(0)("songay_bl").ToString()
            hdnNgayHL.Value = hdr.Rows(0)("ngay_batdau").ToString()
            hdnNgayHetHan.Value = hdr.Rows(0)("ngay_ketthuc").ToString()

            txtKHCT.Text = hdr.Rows(0)("kyhieu_ct").ToString()
            txtSoCT.Text = hdr.Rows(0)("so_ct").ToString()
            txtNgayLap.Text = ConvertNumberToDate(hdr.Rows(0)("ngay_ct").ToString()).ToString("dd/MM/yyyy")
            txtNguoiLap.Text = hdr.Rows(0)("ten_nv").ToString()
            txtNguoiKS.Text = hdr.Rows(0)("ten_ks").ToString()
            txtSoBaoLanh.Text = hdr.Rows(0)("so_bl").ToString()
            rdbKieuBL.SelectedValue = hdr.Rows(0)("kieu_bl").ToString()
            txtNgayHL.Text = hdr.Rows(0)("ngay_batdau").ToString()
            txtNgayHetHan.Text = hdr.Rows(0)("ngay_ketthuc").ToString()
            txtTongSoNgay.Text = hdr.Rows(0)("songay_bl").ToString()
            txtMaNNT.Text = hdr.Rows(0)("ma_nnthue").ToString()
            txtTenNNT.Text = hdr.Rows(0)("ten_nnthue").ToString()
            txtDiaChi.Text = hdr.Rows(0)("dc_nnthue").ToString()
            'txtTK.Text = hdr.Rows(0)("tk_no").ToString()
            'txtTenTKNS.Text = hdr.Rows(0)("ten_tk_ns").ToString()

            'MNBV
            txtMaHQPH.Text = hdr.Rows(0)("ma_hq_ph").ToString()
            txtTenMaHQPH.Text = hdr.Rows(0)("ten_hq_ph").ToString()

            txtCifNumber.Text = hdr.Rows(0)("cif").ToString()

            txtMaCQT.Text = hdr.Rows(0)("ma_cqthu").ToString()
            txtMaHQ.Text = hdr.Rows(0)("ma_hq").ToString()
            txtTenHQ.Text = hdr.Rows(0)("ten_cqthu").ToString()
            txtTenCQT.Text = hdr.Rows(0)("ten_cqthu").ToString()
            txtToKhaiSo.Text = hdr.Rows(0)("so_tk").ToString()
            txtNgayDK.Text = hdr.Rows(0)("ngay_tk").ToString()
            txtLHXNK.Text = hdr.Rows(0)("lhxnk").ToString()
            txtDescLHXNK.Text = hdr.Rows(0)("ten_lhxnk").ToString()
            drpLoaiTien.SelectedValue = hdr.Rows(0)("ma_loaitien").ToString()
            txtDienGiai.Text = hdr.Rows(0)("dien_giai").ToString()
            txtTrangThaiCT.Text = Business.CTuCommon.Get_TenTrangThai(hdr.Rows(0)("trang_thai").ToString())
            ' them hop dong ngoai thương
            If hdr.Rows(0)("TRANG_THAI_TT").ToString <> "1" Then
                txtTrangThai.Text = "Chưa thanh toán"
            Else
                txtTrangThai.Text = "Đã thanh toán"
            End If
            txtHD_NT.Text = hdr.Rows(0)("hd_nt").ToString()
            txtNgay_HD.Text = hdr.Rows(0)("ngay_hd").ToString()
            txtHDon.Text = hdr.Rows(0)("hoa_don").ToString()
            txtNgay_HDon.Text = hdr.Rows(0)("ngay_hdon").ToString()
            txtVTD.Text = hdr.Rows(0)("vt_don").ToString()
            txtNgay_VTD.Text = hdr.Rows(0)("ngay_vtd").ToString()
            'txtCapChuong.Text = hdr.Rows(0)("ma_chuong").ToString()
            txtMA_DV_DD.Text = hdr.Rows(0)("MA_DV_DD").ToString()
            txtTen_DV_DD.Text = hdr.Rows(0)("TEN_DV_DD").ToString()
            txtFone_Num.Text = hdr.Rows(0)("fone_num").ToString()
            txt_FAX_Num.Text = hdr.Rows(0)("fax_num").ToString()
            txtSo_DKKD.Text = hdr.Rows(0)("so_dkkd").ToString()
            txtCoQCap.Text = hdr.Rows(0)("cq_cap").ToString()
            txtNgay_CQCap.Text = hdr.Rows(0)("ngay_cqcap").ToString()
            ddlLoaiBL.SelectedValue = hdr.Rows(0)("type_bl").ToString()
            txtTongTien.Text = String.Format("{0:0,0}", CType(hdr.Rows(0)("ttien").ToString(), Double)).Replace(",", ".")
            txtTT_TTHU.Text = String.Format("{0:0,0}", CType(hdr.Rows(0)("ttien_tthu").ToString(), Double)).Replace(",", ".")
            txtTTien_T24.Text = String.Format("{0:0,0}", CType(hdr.Rows(0)("ttien_nt").ToString(), Double)).Replace(",", ".")
            Load_ddl_LoaiBL()
            If txtToKhaiSo.Text.Trim = "99999" Then
                divHD_NT.Attributes("style") = "display:block;"
            End If
            If hdr.Rows(0)("trang_thai").ToString() = "03" Then
                lblLyDoCTra.Text = hdr.Rows(0)("ly_do_ctra").ToString()
                lblLyDoCTra.Enabled = False
            End If
            If hdr.Rows(0)("LY_DO_HUY") IsNot Nothing Then
                If hdr.Rows(0)("LY_DO_HUY").ToString.Trim.Length > 0 Then
                    drpLyDoHuy.SelectedValue = hdr.Rows(0)("LY_DO_HUY").ToString.Trim
                    drpLyDoHuy.Enabled = False
                End If
            End If
        End If
        ' Du lieu chi tiet
        If dtls.Rows.Count > 0 Then
            If dtls.Rows.Count >= 1 Then
                chkCheck_1.Checked = True
                txtMaQuy_1.Text = dtls.Rows(0)("ma_chuong").ToString()
                txtMaChuong_1.Text = dtls.Rows(0)("ma_nkt").ToString()
                txtNDKT_1.Text = dtls.Rows(0)("ma_ndkt").ToString()
                txtNoiDung_1.Text = dtls.Rows(0)("noi_dung").ToString()

                If dtls.Rows(0)("sotien").ToString() = "" Then
                    txtTien_1.Text = "0"
                Else
                    txtTien_1.Text = String.Format("{0:0,0}", CType(dtls.Rows(0)("sotien").ToString(), Double)).Replace(",", ".")
                End If

                'txtTien_1.Text = dtls.Rows(0)("sotien").ToString()
                txtKyThue_1.Text = dtls.Rows(0)("ky_thue").ToString()
                txtKHTK_1.Text = dtls.Rows(0)("ma_khtk").ToString()
                txtTongTien.Text = dtls.Rows(0)("sotien").ToString()
            End If

            If dtls.Rows.Count >= 2 Then
                chkCheck_2.Checked = True
                txtMaQuy_2.Text = dtls.Rows(1)("ma_chuong").ToString()
                txtMaChuong_2.Text = dtls.Rows(1)("ma_nkt").ToString()
                txtNDKT_2.Text = dtls.Rows(1)("ma_ndkt").ToString()
                txtNoiDung_2.Text = dtls.Rows(1)("noi_dung").ToString()
                If dtls.Rows(1)("sotien").ToString() = "" Then
                    txtTien_2.Text = "0"
                Else
                    txtTien_2.Text = String.Format("{0:0,0}", CType(dtls.Rows(1)("sotien").ToString(), Double)).Replace(",", ".")
                End If
                'txtTien_2.Text = dtls.Rows(1)("sotien").ToString()
                txtKyThue_2.Text = dtls.Rows(1)("ky_thue").ToString()
                txtKHTK_2.Text = dtls.Rows(1)("ma_khtk").ToString()
                txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dtls.Rows(1)("sotien").ToString())
            End If

            If dtls.Rows.Count >= 3 Then
                chkCheck_3.Checked = True
                txtMaQuy_3.Text = dtls.Rows(2)("ma_chuong").ToString()
                txtMaChuong_3.Text = dtls.Rows(2)("ma_nkt").ToString()
                txtNDKT_3.Text = dtls.Rows(2)("ma_ndkt").ToString()
                txtNoiDung_3.Text = dtls.Rows(2)("noi_dung").ToString()

                If dtls.Rows(2)("sotien").ToString() = "" Then
                    txtTien_3.Text = "0"
                Else
                    txtTien_3.Text = String.Format("{0:0,0}", CType(dtls.Rows(2)("sotien").ToString(), Double)).Replace(",", ".")
                End If

                'txtTien_3.Text = dtls.Rows(2)("sotien").ToString()
                txtKyThue_3.Text = dtls.Rows(2)("ky_thue").ToString()
                txtKHTK_3.Text = dtls.Rows(2)("ma_khtk").ToString()
                txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dtls.Rows(2)("sotien").ToString())
            End If

            If dtls.Rows.Count >= 4 Then
                chkCheck_4.Checked = True
                txtMaQuy_4.Text = dtls.Rows(3)("ma_chuong").ToString()
                txtMaChuong_4.Text = dtls.Rows(3)("ma_nkt").ToString()
                txtNDKT_4.Text = dtls.Rows(3)("ma_ndkt").ToString()
                txtNoiDung_4.Text = dtls.Rows(3)("noi_dung").ToString()

                If dtls.Rows(3)("sotien").ToString() = "" Then
                    txtTien_4.Text = "0"
                Else
                    txtTien_4.Text = String.Format("{0:0,0}", CType(dtls.Rows(3)("sotien").ToString(), Double)).Replace(",", ".")
                End If

                'txtTien_4.Text = dtls.Rows(3)("sotien").ToString()
                txtKyThue_4.Text = dtls.Rows(3)("ky_thue").ToString()
                txtKHTK_4.Text = dtls.Rows(3)("ma_khtk").ToString()
                txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dtls.Rows(3)("sotien").ToString())
            End If

            If dtls.Rows.Count >= 5 Then
                chkCheck_5.Checked = True
                txtMaQuy_5.Text = dtls.Rows(4)("ma_chuong").ToString()
                txtMaChuong_5.Text = dtls.Rows(4)("ma_nkt").ToString()
                txtNDKT_5.Text = dtls.Rows(4)("ma_ndkt").ToString()
                txtNoiDung_5.Text = dtls.Rows(4)("noi_dung").ToString()

                If dtls.Rows(4)("sotien").ToString() = "" Then
                    txtTien_5.Text = "0"
                Else
                    txtTien_5.Text = String.Format("{0:0,0}", CType(dtls.Rows(4)("sotien").ToString(), Double)).Replace(",", ".")
                End If

                'txtTien_5.Text = dtls.Rows(3)("sotien").ToString()
                txtKyThue_5.Text = dtls.Rows(4)("ky_thue").ToString()
                txtKHTK_5.Text = dtls.Rows(4)("ma_khtk").ToString()
                txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dtls.Rows(4)("sotien").ToString())
            End If

            If dtls.Rows.Count >= 6 Then
                chkCheck_6.Checked = True
                txtMaQuy_6.Text = dtls.Rows(5)("ma_chuong").ToString()
                txtMaChuong_6.Text = dtls.Rows(5)("ma_nkt").ToString()
                txtNDKT_6.Text = dtls.Rows(5)("ma_ndkt").ToString()
                txtNoiDung_6.Text = dtls.Rows(5)("noi_dung").ToString()

                If dtls.Rows(5)("sotien").ToString() = "" Then
                    txtTien_6.Text = "0"
                Else
                    txtTien_6.Text = String.Format("{0:0,0}", CType(dtls.Rows(5)("sotien").ToString(), Double)).Replace(",", ".")
                End If

                'txtTien_6.Text = dtls.Rows(5)("sotien").ToString()
                txtKyThue_6.Text = dtls.Rows(5)("ky_thue").ToString()
                txtKHTK_6.Text = dtls.Rows(5)("ma_khtk").ToString()
                txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dtls.Rows(5)("sotien").ToString())

            End If

            If dtls.Rows.Count >= 7 Then
                chkCheck_7.Checked = True
                txtMaQuy_7.Text = dtls.Rows(6)("ma_chuong").ToString()
                txtMaChuong_7.Text = dtls.Rows(6)("ma_nkt").ToString()
                txtNDKT_7.Text = dtls.Rows(6)("ma_ndkt").ToString()
                txtNoiDung_7.Text = dtls.Rows(6)("noi_dung").ToString()

                If dtls.Rows(6)("sotien").ToString() = "" Then
                    txtTien_7.Text = "0"
                Else
                    txtTien_7.Text = String.Format("{0:0,0}", CType(dtls.Rows(6)("sotien").ToString(), Double)).Replace(",", ".")
                End If

                'txtTien_7.Text = dtls.Rows(6)("sotien").ToString()
                txtKyThue_7.Text = dtls.Rows(6)("ky_thue").ToString()
                txtKHTK_7.Text = dtls.Rows(6)("ma_khtk").ToString()
                txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dtls.Rows(6)("sotien").ToString())

            End If

            If txtTongTien.Text = "" Then
                txtTongTien.Text = "0"
                txtTT_TTHU.Text = "0"
                txtTTien_T24.Text = "0"
            Else
                txtTongTien.Text = String.Format("{0:0,0}", CType(txtTongTien.Text, Double)).Replace(",", ".")
                
            End If

        End If

    End Sub
    Sub Load_ddl_LoaiBL()
        If ddlLoaiBL.SelectedValue = "31" Then
            rowMAHQPH.Visible = True
            rowMAHQ.Visible = True
            rowLHXNK.Visible = True
            rowNGAYDK.Visible = True
            rowSOTK.Visible = True
            rowLoaiTien.Visible = True
            rowHD.Visible = False
        ElseIf ddlLoaiBL.SelectedValue = "32" Then
            rowMAHQPH.Visible = False
            rowMAHQ.Visible = False
            rowLHXNK.Visible = False
            rowNGAYDK.Visible = False
            rowSOTK.Visible = False
            rowLoaiTien.Visible = False
            rowHD.Visible = True
        ElseIf ddlLoaiBL.SelectedValue = "33" Then
            rowMAHQPH.Visible = False
            rowMAHQ.Visible = False
            rowLHXNK.Visible = False
            rowNGAYDK.Visible = False
            rowSOTK.Visible = False
            rowLoaiTien.Visible = False
            rowHD.Visible = False
        End If
    End Sub
#End Region

End Class
