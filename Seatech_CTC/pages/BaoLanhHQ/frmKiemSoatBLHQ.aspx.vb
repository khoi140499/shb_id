﻿Imports System.Data
Imports VBOracleLib
Imports Business
Imports Business.NewChungTu
Imports Business.Common.mdlCommon
Imports VBOracleLib.Globals
Imports VBOracleLib.Security

Partial Class pages_BaoLanhHQ_frmKiemSoatBLHQ
    Inherits System.Web.UI.Page

    Private Shared mv_objUser As New CTuUser
    Private Shared mv_Ngay_LV As String
    Private Shared i_PageIndex As Integer = 0
    Private Const str_Ma_BL_TK As String = "31"
    Private Const str_Ma_BL_VD As String = "32"
    Private Const str_Ma_BL_Chung As String = "33"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'tynk-kiểm tra ngày nghỉ
            Dim check As Integer = 0
            If Not Business.CTuCommon.GetNgayNghi() Then
                ' clsCommon.ShowMessageBox(Me, "Hôm nay là ngày nghỉ. Bạn không được vào chức năng này!!!")
                Response.Redirect("~/pages/frmHomeNV.aspx?CheckNN=1", False)
                check = 1
                Exit Sub
            End If
            If check = 1 Then
                Response.Redirect("~/pages/frmHomeNV.aspx?CheckNN=1", False)
                Exit Sub
            End If
            If Not clsCommon.GetSessionByID(Session.Item("User")) Then
                Response.Redirect("~/pages/Warning.html", False)
                RemoveHandler btnChuyenTra.Click, AddressOf btnChuyenTra_Click
                RemoveHandler btnGuiLai.Click, AddressOf btnGuiLai_Click
                RemoveHandler btnHuy.Click, AddressOf btnHuy_Click
                RemoveHandler btnSearch.Click, AddressOf btnSearch_Click
                RemoveHandler btnKiemSoat.Click, AddressOf btnKiemSoat_Click
                RemoveHandler cmdIn.Click, AddressOf cmdIn_Click
                ' RemoveHandler cmdThanhToan.Click, AddressOf cmdThanhToan_Click
                Exit Sub
            End If
            'If Not Page.IsPostBack Then
            LoadForm()
            'btnKiemSoat.Attributes.Add("onclick", "javascript:disableButton() ;" + Page.ClientScript.GetPostBackEventReference(btnKiemSoat, "").ToString())
            'btnChuyenTra.Attributes.Add("onclick", "javascript:disableButton() ;" + Page.ClientScript.GetPostBackEventReference(btnChuyenTra, "").ToString())
            'btnHuy.Attributes.Add("onclick", "this.disabled=true;" + Page.ClientScript.GetPostBackEventReference(btnHuy, "").ToString())
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub drpTrangThai_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpTrangThai.SelectedIndexChanged
        Try
            LayDSCT()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub dtgDSCT_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgDSCT.PageIndexChanged
       Try
            LayDSCT()
            dtgDSCT.CurrentPageIndex = e.NewPageIndex
            dtgDSCT.DataBind()
            i_PageIndex = e.NewPageIndex
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

#Region "ButtonClick"

    Protected Sub btnKiemSoat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKiemSoat.Click
        Try
            'Kiểm tra mật khẩu kiểm soát
            Dim strUser = mv_objUser.Ten_DN.ToString
            'If Not (txtMatKhau.Text.Length = 0) Then
            '    If Not Business.KetNoi_LDAP.CheckPassWord(strUser, txtMatKhau.Text) Then
            '        lblStatus.Text = "Mật khẩu kiểm soát không hợp lệ.Hãy thử lại!"
            '        Exit Sub
            '    End If
            'Else
            '    lblStatus.Text = "Phải nhập mật khẩu khi kiểm soát."
            '    Exit Sub
            'End If
            KiemSoatBL()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnChuyenTra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChuyenTra.Click
        Try
            ChuyenTraBL()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnGuiLai_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuiLai.Click
        Try
            GuiLaiBL()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnHuy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHuy.Click
        Try
            'Kiểm tra mật khẩu kiểm soát
            Dim strUser = mv_objUser.Ten_DN.ToString
            'If Not (txtMatKhau.Text.Length = 0) Then
            '    'If Not Business.KetNoi_LDAP.CheckPassWord(strUser, txtMatKhau.Text) Then
            '    '    lblStatus.Text = "Mật khẩu kiểm soát không hợp lệ.Hãy thử lại!"
            '    '    Exit Sub
            '    'End If
            'Else
            '    lblStatus.Text = "Phải nhập mật khẩu khi kiểm soát."
            '    Exit Sub
            'End If
            HuyBL()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Try
            LayDSCT()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

#End Region

#Region "Function"

    Sub LoadForm()
        Dim v_strSHKB As String = String.Empty
        Dim v_strMaDiemThu As String = String.Empty
        Dim strNgay_LV As String = ""

        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not Session.Item("User") Is Nothing Then
            v_strSHKB = clsCTU.TCS_GetKHKB(CType(Session("User"), Integer))
            v_strMaDiemThu = clsCTU.TCS_GetMaDT(CType(Session("User"), Integer))
            mv_objUser = New CTuUser(Session.Item("User"))
            mv_Ngay_LV = clsCTU.TCS_GetNgayLV(mv_objUser.Ma_NV)

        End If

        If Not IsPostBack Then
            Try
                hdnSHKB.Value = v_strSHKB
                hdnMaNV.Value = mv_objUser.Ma_NV
                'hdnMa8SoCN = mv_objUser.MA_NH_CN
                'hdnTenCN = mv_objUser.TEN_NH_CN


                ' View chi tiet chung tu
                If Not Request.Params("SHKB") Is Nothing And Not Request.Params("So_CT") Is Nothing And Not Request.Params("So_BT") Is Nothing Then
                    Dim strSHKB As String = Request.Params("SHKB").Trim
                    Dim strMaNV As String = hdnMaNV.Value.Trim
                    Dim strSoCT As String = Request.Params("So_CT").Trim
                    Dim strSoBT As String = Request.Params("So_BT").Trim
                    Dim strTT As String = Request.Params("TT").Trim
                    Dim strNgayKB As String = Request.Params("NGAY_KB").Trim
                    Load_Detail(strSHKB, strSoCT, strSoBT, strMaNV, strNgayKB)

                    'If strTT <> "" Then
                    '    drpTrangThai.SelectedValue = strTT
                    'End If

                    If strTT = "01" Then
                        btnKiemSoat.Enabled = False
                        btnChuyenTra.Enabled = False
                        btnGuiLai.Enabled = False
                        If hdnTrangThaiTT.Value = "0" Then
                            btnHuy.Enabled = False
                            cmdIn.Enabled = True
                            'cmdThanhToan.Enabled = True
                        Else
                            btnHuy.Enabled = False
                            cmdIn.Enabled = False
                            ' cmdThanhToan.Enabled = False
                        End If
                    ElseIf strTT = "02" Then
                        btnKiemSoat.Enabled = False
                        btnChuyenTra.Enabled = True
                        btnGuiLai.Enabled = False
                        If hdnTrangThaiTT.Value = "0" Then
                            btnHuy.Enabled = True
                            cmdIn.Enabled = True
                            'cmdThanhToan.Enabled = True
                        Else
                            btnHuy.Enabled = False
                            cmdIn.Enabled = False
                            ' cmdThanhToan.Enabled = False
                        End If

                    ElseIf strTT = "05" Or strTT = "07" Then
                        btnKiemSoat.Enabled = True
                        btnChuyenTra.Enabled = True
                        btnGuiLai.Enabled = False
                        btnHuy.Enabled = False
                        cmdIn.Enabled = False
                        'cmdThanhToan.Enabled = False
                    ElseIf strTT = "04" Then
                        btnKiemSoat.Enabled = False
                        btnChuyenTra.Enabled = False
                        btnGuiLai.Enabled = False
                        btnHuy.Enabled = False
                        cmdIn.Enabled = True
                        'cmdThanhToan.Enabled = False
                    ElseIf strTT = "02" Then
                        btnKiemSoat.Enabled = False
                        btnChuyenTra.Enabled = False
                        btnGuiLai.Enabled = False
                        btnHuy.Enabled = True
                        cmdIn.Enabled = True
                        'cmdThanhToan.Enabled = False
                    Else
                        btnKiemSoat.Enabled = False
                        btnChuyenTra.Enabled = False
                        btnGuiLai.Enabled = False
                        btnHuy.Enabled = False
                        cmdIn.Enabled = False
                        'cmdThanhToan.Enabled = False
                    End If

                    lblStatus.Text = "Xem thông tin bảo lãnh"

                End If
                LayDSCT()
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub
    Sub EnableButon(ByVal strTT As String)

        If strTT = "01" Then
            btnKiemSoat.Enabled = False
            btnChuyenTra.Enabled = False
            btnGuiLai.Enabled = False
            btnHuy.Enabled = True
            cmdIn.Enabled = True
        ElseIf strTT = "05" Or strTT = "07" Then
            btnKiemSoat.Enabled = True
            btnChuyenTra.Enabled = True
            btnGuiLai.Enabled = False
            btnHuy.Enabled = False
            cmdIn.Enabled = False
            'cmdThanhToan.Enabled = False
        ElseIf strTT = "04" Then
            btnKiemSoat.Enabled = False
            btnChuyenTra.Enabled = False
            btnGuiLai.Enabled = False
            btnHuy.Enabled = False
            cmdIn.Enabled = False
            'cmdThanhToan.Enabled = False
        ElseIf strTT = "02" Then
            btnKiemSoat.Enabled = False
            btnChuyenTra.Enabled = True
            btnGuiLai.Enabled = False
            btnHuy.Enabled = True
            cmdIn.Enabled = True
            'cmdThanhToan.Enabled = False
        Else
            btnKiemSoat.Enabled = False
            btnChuyenTra.Enabled = False
            btnGuiLai.Enabled = False
            btnHuy.Enabled = False
            cmdIn.Enabled = False
            'cmdThanhToan.Enabled = False
        End If
    End Sub
    Sub KiemSoatBL()
        lblError.Text = ""
        lblSuccess.Text = ""
        If hdnSHKB.Value.Trim <> "" And hdnSoCT.Value.Trim <> "" And hdnSoBT.Value.Trim <> "" And hdnMaNV.Value.Trim <> "" Then
            Dim strSHKB As String = hdnSHKB_CT.Value.Trim
            Dim strSoCT As String = hdnSoCT.Value.Trim
            Dim strSoBT As String = hdnSoBT.Value.Trim
            Dim strTT As String = hdnTThai.Value.Trim
            Dim strMaNV As String = hdnMaNV_LapCT.Value.Trim
            Dim strMaDThu As String = hdnMaDThu.Value.Trim
            Dim strNgayKB As String = hdnNgayKB.Value.Trim()

            Dim Ma_KS As String = Session.Item("User").ToString
            Dim strMaNNT As String = hdnMaNNT.Value.Trim
            Dim strThoiGianBL As String = hdnTongSoNgay.Value.Trim
            Dim strThoiGianBatDauBL As String = hdnNgayHL.Value.Trim
            Dim strThoiGianKetThucBL As String = hdnNgayHetHan.Value.Trim

            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""
            Dim strTransactionId As String = ""
            Dim strso_tn_ct As String = ""
            Dim strngay_tn_ct As String = ""
            Dim strLoai_BL As String = ddlLoaiBL.SelectedValue.ToString()
            Dim hdr As New infDoiChieuNHHQ_HDR
            Dim dtls() As infDoiChieuNHHQ_DTL
            Dim key As New KeyCTu
            key.SHKB = strSHKB
            key.Ngay_KB = hdnNgayKB.Value
            key.Ma_NV = strMaNV
            key.So_CT = strSoCT
            Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai_BL(key)
            Dim strTT_Form As String = strTT
            If (strTT_Data <> strTT_Form) Then
                lblError.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                Exit Sub
            End If
            Dim checkHuyBLTuChinh As Boolean = False
            If strTT = "07" Then
                checkHuyBLTuChinh = HuyBLTuChinh()
            End If
            'Kiem soat nhung bao lanh GDV gui sang (Trang thai: 05)
            If strTT = "05" Then
                'Gui thong tin bao lanh sang Hai quan
                If (ChungTu.buChungTu.CTU_checkExist_SoBL(txtSoBaoLanh.Text.Trim)) Then
                    lblError.Text = "Số bảo lãnh " & txtSoBaoLanh.Text & " đã được lập và chuyển Hải quan thành công. Vui lòng kiểm tra lại."
                    txtSoBaoLanh.Focus()
                    Exit Sub
                End If
                'If (ChungTu.buChungTu.CTU_checkExist_SoTK(txtToKhaiSo.Text.Trim)) Then
                '    lblError.Text = "Số tờ khai " & txtToKhaiSo.Text.Trim & " đã được lập và chuyển Hải quan thành công. Vui lòng nhập số tờ khai khác."
                '    txtToKhaiSo.Focus()
                '    Exit Sub
                'End If
                'CustomsService.ProcessMSG.getMSG62(strMaNNT, strSHKB, strSoCT, strSoBT, strNgayKB, strLoai_BL, _
                '                                   strso_tn_ct, strngay_tn_ct, strErrorNum, strErrorMes, strTransactionId)
                If strErrorNum.Trim = "0" Then
                    clsCTU.TCS_UpdateTrangThai(strSHKB, strSoCT, strSoBT, Ma_KS, "01", "", strNgayKB)
                    ' strErrorNum = "0"
                    'Insert du lieu vao bang DOICHIEU_NHHQ
                    hdr.ma_dv_dd = txtMA_DV_DD.Text.ToString()
                    hdr.ten_dv_dd = txtTen_DV_DD.Text.ToString()
                    Dim TransactionHQ_Type_DC As String = ""
                    If strLoai_BL = "31" Then
                        TransactionHQ_Type_DC = "M81"
                    ElseIf strLoai_BL = "32" Then
                        TransactionHQ_Type_DC = "M85"
                    Else
                        TransactionHQ_Type_DC = "M87"
                    End If
                    clsCTU.SetObjectToDoiChieuNHHQ(strSHKB, strSoCT, strSoBT, strMaNV, "BAOLANH", hdr, dtls, strMaDThu, strErrorNum, _
                                                   TransactionHQ_Type_DC, strNgayKB, strso_tn_ct, strngay_tn_ct, False)
                    hdr.transaction_id = strTransactionId
                    ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls)

                    lblSuccess.Text = "Kiểm soát bảo lãnh và truyền HQ thành công!"
                    btnKiemSoat.Enabled = False
                    btnChuyenTra.Enabled = False
                    cmdIn.Enabled = True
                    btnHuy.Enabled = False
                    drpLyDoHuy.Enabled = False
                    cmdThanhToan.Enabled = True
                    hdnTThai.Value = "01"
                    drpTrangThai.SelectedValue = "99"
                    LayDSCT()

                Else
                    lblError.Text = strErrorNum & " - " & strErrorMes
                End If
            ElseIf strTT = "07" And checkHuyBLTuChinh = True Then
                Dim TransactionHQ_Type_DC As String = ""
                If strLoai_BL = "31" Then
                    TransactionHQ_Type_DC = "M81"
                ElseIf strLoai_BL = "32" Then
                    TransactionHQ_Type_DC = "M85"
                Else
                    TransactionHQ_Type_DC = "M87"
                End If
                'Gui thong tin bao lanh sang Hai quan
                'CustomsService.ProcessMSG.getMSG62(strMaNNT, strSHKB, strSoCT, strSoBT, strNgayKB, strLoai_BL, _
                '                                   strso_tn_ct, strngay_tn_ct, strErrorNum, strErrorMes, strTransactionId)
                If strErrorNum.Trim = "0" Then
                    hdr.ma_dv_dd = txtMA_DV_DD.Text.ToString()
                    hdr.ten_dv_dd = txtTen_DV_DD.Text.ToString()
                    clsCTU.TCS_UpdateTrangThai(strSHKB, strSoCT, strSoBT, Ma_KS, "01", "2", strNgayKB)

                    'Insert du lieu vao bang DOICHIEU_NHHQ
                    clsCTU.SetObjectToDoiChieuNHHQ(strSHKB, strSoCT, strSoBT, strMaNV, "BAOLANH", hdr, dtls, strMaDThu, strErrorNum, _
                                                   TransactionHQ_Type_DC, strNgayKB, strso_tn_ct, strngay_tn_ct, False)
                    hdr.transaction_id = strTransactionId
                    ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls)

                    lblSuccess.Text = "Kiểm soát bảo lãnh và truyền sang HQ thành công!"
                    'ResetField()
                    drpTrangThai.SelectedValue = "99"
                    LayDSCT()
                    drpLyDoHuy.Enabled = False
                    btnKiemSoat.Enabled = False
                    btnChuyenTra.Enabled = False
                    cmdIn.Enabled = True
                    btnHuy.Enabled = False
                    cmdThanhToan.Enabled = True
                    hdnTThai.Value = "01"
                Else
                    lblError.Text = strErrorNum & " - " & strErrorMes
                    btnKiemSoat.Enabled = True
                    cmdIn.Enabled = False
                    btnHuy.Enabled = False
                    cmdThanhToan.Enabled = False
                End If
            ElseIf strTT = "07" And checkHuyBLTuChinh = False Then
                lblError.Text = "Tu chỉnh không thành công do không hủy được lệnh gốc"
            Else
                lblError.Text = "Bảo lãnh đã được kiểm soát!"
                drpLyDoHuy.Enabled = False
                btnKiemSoat.Enabled = False
                btnChuyenTra.Enabled = False
                cmdIn.Enabled = True
                btnHuy.Enabled = True
                cmdThanhToan.Enabled = True
            End If
        Else
            lblError.Text = "Bạn phải chọn 1 bản ghi để thực hiện!"
        End If
    End Sub

    Sub GuiLaiBL()
        lblError.Text = ""
        lblSuccess.Text = ""
        If hdnSHKB.Value.Trim <> "" And hdnSoCT.Value.Trim <> "" And hdnSoBT.Value.Trim <> "" And hdnMaNV.Value.Trim <> "" Then
            Dim strSHKB As String = hdnSHKB.Value.Trim
            Dim strSoCT As String = hdnSoCT.Value.Trim
            Dim strSoBT As String = hdnSoBT.Value.Trim
            Dim strTT As String = hdnTThai.Value.Trim
            Dim strMaNV As String = hdnMaNV.Value.Trim
            Dim strMaDThu As String = hdnMaDThu.Value.Trim
            Dim strNgayKB As String = hdnNgayKB.Value.Trim

            Dim ma_KS As String = Session.Item("User").ToString
            Dim strMaNNT As String = hdnMaNNT.Value.Trim
            Dim strThoiGianBL As String = hdnTongSoNgay.Value.Trim
            Dim strThoiGianBatDauBL As String = hdnNgayHL.Value.Trim
            Dim strThoiGianKetThucBL As String = hdnNgayHetHan.Value.Trim

            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""

            Dim hdr As New infDoiChieuNHHQ_HDR
            Dim dtls() As infDoiChieuNHHQ_DTL
            Dim strTransactionID As String = ""
            Dim strso_tn_ct As String = ""
            Dim strngay_tn_ct As String = ""
            Dim strLoai_BL As String = ddlLoaiBL.SelectedValue.ToString()
            'Gui lai thong tin bao lanh sang Hai quan
            'CustomsService.ProcessMSG.getMSG62(strMaNNT, strSHKB, strSoCT, strSoBT, strNgayKB, strLoai_BL, _
            '                                       strso_tn_ct, strngay_tn_ct, strErrorNum, strErrorMes, strTransactionID)
           ' strErrorNum = "0"
            If strErrorNum.Trim = "0" Then
                clsCTU.TCS_UpdateTrangThai(strSHKB, strSoCT, strSoBT, ma_KS, "01", "", strNgayKB)

                'Insert du lieu vao bang DOICHIEU_NHHQ
                clsCTU.SetObjectToDoiChieuNHHQ(strSHKB, strSoCT, strSoBT, strMaNV, "BAOLANH", hdr, dtls, strMaDThu, strErrorNum, _
                                                   TransactionHQ_Type.M81.ToString(), strNgayKB, strso_tn_ct, strngay_tn_ct, False)
                hdr.transaction_id = strTransactionID
                ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls)

                lblSuccess.Text = "Gửi lại bảo lãnh thành công!"
                ResetField()
                LayDSCT()
            Else
                lblError.Text = strErrorNum & " - " & strErrorMes
            End If

        Else
            lblError.Text = "Bạn phải chọn 1 bản ghi để thực hiện!"
        End If
    End Sub

    Sub ChuyenTraBL()
        lblError.Text = ""
        lblSuccess.Text = ""
        If hdnSHKB.Value.Trim <> "" And hdnSoCT.Value.Trim <> "" And hdnSoBT.Value.Trim <> "" And hdnMaNV.Value.Trim <> "" Then
            Dim strSHKB As String = hdnSHKB_CT.Value.Trim()
            Dim strSoCT As String = hdnSoCT.Value.Trim
            Dim strSoBT As String = hdnSoBT.Value.Trim
            Dim strTT As String = hdnTThai.Value.Trim
            Dim strMaNV As String = hdnMaNV_LapCT.Value.Trim
            Dim strLyDoCTra As String = txtLyDoCTra.Text
            Dim strNgayKB As String = hdnNgayKB.Value.Trim
            Dim ma_KS As String = Session.Item("User").ToString
            Dim key As New KeyCTu
            key.SHKB = strSHKB
            key.Ngay_KB = hdnNgayKB.Value
            key.Ma_NV = strMaNV
            key.So_CT = strSoCT
            Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai_BL(key)
            Dim strTT_Form As String = strTT
            If (strTT_Data <> strTT_Form) Then
                lblError.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                Exit Sub
            End If
            If Not strLyDoCTra = "" Then
                'Chuyen tra nhung bao lanh GDV gui sang (Trang thai: 05)
                If (strTT = "05" OrElse strTT = "07") Then
                    clsCTU.TCS_UpdateTrangThai(strSHKB, strSoCT, strSoBT, ma_KS, "03", "", strNgayKB, strLyDoCTra)
                    lblSuccess.Text = "Chuyển trả bảo lãnh thành công!"
                    ResetField()
                    LayDSCT()
                    btnChuyenTra.Enabled = False
                    btnHuy.Enabled = False
                    cmdIn.Enabled = False
                    btnKiemSoat.Enabled = False
                ElseIf (strTT = "02") Then
                    clsCTU.TCS_UpdateTrangThai(strSHKB, strSoCT, strSoBT, ma_KS, "01", "", strNgayKB, strLyDoCTra)
                    lblSuccess.Text = "Chuyển trả chừng tử Hủy - chờ duyệt thành công!"
                    ResetField()
                    LayDSCT()
                    btnChuyenTra.Enabled = False
                    btnHuy.Enabled = False
                    cmdIn.Enabled = False
                    btnKiemSoat.Enabled = False
                Else
                    lblError.Text = "Bảo lãnh đã được kiểm soát!"
                    btnChuyenTra.Enabled = False
                    btnHuy.Enabled = True
                    cmdIn.Enabled = True
                    btnKiemSoat.Enabled = False
                End If
            Else
                lblError.Text = "Bạn chưa nhập lý do chuyển trả!"
            End If
        Else
            lblError.Text = "Bạn phải chọn 1 bản ghi để thực hiện!"
        End If
    End Sub

    Function HuyBLTuChinh() As Boolean
        lblError.Text = ""
        lblSuccess.Text = ""
        If hdnSHKB.Value.Trim <> "" And hdnSoCT.Value.Trim <> "" And hdnSoBT.Value.Trim <> "" And hdnMaNV.Value.Trim <> "" Then
            Dim strSoCTTuChinh As String = hdnSoCTTuChinh.Value.Trim
            Dim strSHKB As String = ""

            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""
            '21/09/2012-ManhNV
            Dim strTransactionID As String = ""
            Dim strKHCT As String = ""
            Dim strSoCT As String = ""
            Dim strNgayCT As String = ""
            Dim strSoBT As String = ""
            Dim strNgayKB As String = ""
            Dim Kieu_BL_M91 As String = ""
            Dim Kieu_BL As String = ""
            If ddlLoaiBL.SelectedValue = str_Ma_BL_TK Then
                Kieu_BL = "M81"
                Kieu_BL_M91 = "M91"
            ElseIf ddlLoaiBL.SelectedValue = str_Ma_BL_VD Then
                Kieu_BL = "M85"
                Kieu_BL_M91 = "M95"
            Else
                Kieu_BL = "M87"
                Kieu_BL_M91 = "M97"
            End If

            Dim hdr As New infDoiChieuNHHQ_HDR
            Dim dtls() As infDoiChieuNHHQ_DTL
            Dim strLyDoHuy As String = "2"
            Dim strso_tn_ct As String = ""
            Dim strngay_tn_ct As String = ""
            Dim strso_tn_ct_ych As String = ""
            Dim strLoai_BL As String = ddlLoaiBL.SelectedValue.ToString()
            Dim strTN_CT As String = ChungTu.buChungTu.CTU_TN_CT_NHHQ(strSoCTTuChinh, Kieu_BL)
            strso_tn_ct_ych = strTN_CT.Split(";")(0)
            strso_tn_ct = strTN_CT.Split(";")(1)
            Dim strSQL As String = ""
            strSQL = "select kyhieu_ct, so_ct, ngay_ct, so_bt, shkb, ngay_kb from tcs_baolanh_hdr where so_ct = '" & strSoCTTuChinh & "'"
            Dim dt As New DataTable
            dt = DatabaseHelp.ExecuteToTable(strSQL)

            If dt.Rows.Count = 1 Then
                strKHCT = dt.Rows(0)("kyhieu_ct").ToString()
                strSoCT = dt.Rows(0)("so_ct").ToString()
                strNgayCT = dt.Rows(0)("ngay_ct").ToString()
                strSoBT = dt.Rows(0)("so_bt").ToString()
                strSHKB = dt.Rows(0)("shkb").ToString()
                strNgayKB = dt.Rows(0)("ngay_kb").ToString()
                'CustomsService.ProcessMSG.getMSG72(strso_tn_ct_ych, strso_tn_ct, strngay_tn_ct, strErrorNum, strErrorMes, strTransactionID)
            Else
                Return False
            End If

            'Insert du lieu vao bang DOICHIEU_NHHQ
            clsCTU.SetObjectToDoiChieuNHHQ(strSHKB, strSoCT, strSoBT, hdnMaNV.Value, "BAOLANH", hdr, dtls, "", strErrorNum, _
                                           Kieu_BL_M91, strNgayKB, strso_tn_ct, strngay_tn_ct, False)
            hdr.transaction_id = strTransactionID
            hdr.ma_dv_dd = txtMA_DV_DD.Text.ToString()
            hdr.ten_dv_dd = txtTen_DV_DD.Text.ToString()
            hdr.ngay_kb = DateTime.Now().ToString("yyyyMMdd")
            ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls)
            Dim ma_KS As String = Session.Item("User").ToString
            If strErrorNum.Trim = "0" Then
                clsCTU.TCS_UpdateTrangThai(strSHKB, strSoCT, strSoBT, ma_KS, "04", strLyDoHuy, strNgayKB)
            Else
                Return False
            End If
            
        Else
            lblError.Text = "Bạn phải chọn 1 bản ghi để thực hiện!"
            Return False
        End If
        Return True
    End Function
    Private Function Check_CT_Goc(ByVal strSo_CT As String) As Boolean
        Dim strReturn As Boolean = True
        Dim strSQL As String = ""
        Dim strKQ As String
        strSQL = "select count(*) from tcs_baolanh_hdr where so_ct_tuchinh = '" & strSo_CT & "' and trang_thai<>'04'"
        Try
            strKQ = DataAccess.ExecuteSQLScalar(strSQL)
            If strKQ > 0 Then strReturn = False
        Catch ex As Exception
            Return strReturn
        End Try
       
        Return strReturn
    End Function
    Sub HuyBL()
        lblError.Text = ""
        lblSuccess.Text = ""
        If hdnSHKB.Value.Trim <> "" And hdnSoCT.Value.Trim <> "" And hdnSoBT.Value.Trim <> "" And hdnMaNV.Value.Trim <> "" Then
            Dim strSHKB As String = hdnSHKB_CT.Value.Trim
            Dim strSoCT As String = hdnSoCT.Value.Trim
            Dim strSoBT As String = hdnSoBT.Value.Trim
            Dim strKHCT As String = hdnKHCT.Value.Trim
            Dim strNgayCT As String = hdnNgayCT.Value.Trim
            Dim strNgayKB As String = hdnNgayKB.Value.Trim()
            Dim strTT As String = hdnTThai.Value.Trim
            Dim strMaNV As String = hdnMaNV_LapCT.Value.Trim

            Dim strMaNNT As String = hdnMaNNT.Value.Trim
            Dim strThoiGianBL As String = hdnTongSoNgay.Value.Trim
            Dim strThoiGianBatDauBL As String = hdnNgayHL.Value.Trim
            Dim strThoiGianKetThucBL As String = hdnNgayHetHan.Value.Trim
            Dim Kieu_BL_M91 As String = ""
            Dim Kieu_BL As String = ""
            If ddlLoaiBL.SelectedValue = str_Ma_BL_TK Then
                Kieu_BL = "M81"
                Kieu_BL_M91 = "M91"
            ElseIf ddlLoaiBL.SelectedValue = str_Ma_BL_VD Then
                Kieu_BL = "M85"
                Kieu_BL_M91 = "M95"
            Else
                Kieu_BL = "M87"
                Kieu_BL_M91 = "M97"
            End If
            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""
            '21/09/2012-ManhNV
            Dim strTransactionID As String = ""
            Dim strso_tn_ct As String = ""
            Dim strngay_tn_ct As String = ""
            Dim strso_tn_ct_ych As String = ""
            Dim strLoai_BL As String = ddlLoaiBL.SelectedValue.ToString()
            Dim strTN_CT As String = ChungTu.buChungTu.CTU_TN_CT_NHHQ(strSoCT, Kieu_BL)
            strso_tn_ct_ych = strTN_CT.Split(";")(0)
            strso_tn_ct = strTN_CT.Split(";")(1)
            Dim hdr As New infDoiChieuNHHQ_HDR
            Dim dtls() As infDoiChieuNHHQ_DTL
            Dim strLyDoHuy As String = ""
            Dim key As New KeyCTu
            key.SHKB = strSHKB
            key.Ngay_KB = strNgayKB
            key.Ma_NV = strMaNV
            key.So_CT = strSoCT
            Dim ma_KS As String = Session.Item("User").ToString
            Dim strTT_Data As String = ChungTu.buChungTu.CTU_Get_TrangThai_BL(key)
            Dim strTT_Form As String = strTT
            If (strTT_Data <> strTT_Form) Then
                lblError.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                Exit Sub
            End If
            'check chứng từ đó đang có chứng từ tu chỉnh ko
            If Check_CT_Goc(strSoCT) = False Then
                lblSuccess.Text = "Không hủy được chứng từ này vì đang được tu chỉnh!"
                Exit Sub
            End If
            ' Neu Bao lanh dao kiem soat: Gui thong tin Huy bao lanh sang Hai quan
            If strTT = "01" Or strTT = "02" Then
                'CustomsService.ProcessMSG.getMSG72(strso_tn_ct_ych, strso_tn_ct, strngay_tn_ct, strErrorNum, strErrorMes, strTransactionID)
                'strErrorNum = "0"
                If strErrorNum.Trim = "0" Then
                    hdr.ma_dv_dd = txtMA_DV_DD.Text.ToString()
                    hdr.ten_dv_dd = txtTen_DV_DD.Text.ToString()
                    strLyDoHuy = drpLyDoHuy.SelectedValue.Trim()
                    ' clsCTU.TCS_UpdateTrangThai(strSHKB, strSoCT, strSoBT, ma_KS, "04", strLyDoHuy, strNgayKB)
                    clsCTU.TCS_UpdateTrangThai2(strSHKB, strSoCT, strSoBT, "04", strLyDoHuy, strNgayKB, , , ma_KS)

                    lblSuccess.Text = "Hủy bảo lãnh và chuyển Hải quan thành công!"
                    btnHuy.Enabled = False
                    'ResetField()
                    LayDSCT()
                Else
                    clsCTU.TCS_UpdateTrangThai2(strSHKB, strSoCT, strSoBT, "01", "", strNgayKB, strErrorNum & " - " & strErrorMes, , ma_KS)
                    lblError.Text = strErrorNum & " - " & strErrorMes
                    LayDSCT()
                End If
                'Insert du lieu vao bang DOICHIEU_NHHQ
                clsCTU.SetObjectToDoiChieuNHHQ(strSHKB, strSoCT, strSoBT, strMaNV, "BAOLANH", hdr, dtls, "", strErrorNum, _
                                           Kieu_BL_M91, strNgayKB, strso_tn_ct, strngay_tn_ct, False)
                hdr.transaction_id = strTransactionID
                ChungTu.buChungTu.InsertDoiChieuNHHQ(hdr, dtls)
            End If
        Else
        lblError.Text = "Bạn phải chọn 1 bản ghi để thực hiện!"
        End If
    End Sub

    Sub LayDSCT()
        Dim soCT As String = ""
        Dim soBL As String = ""
        If Not Request.Params("SoCT") Is Nothing Then
            soCT = Request.Params("SoCT").ToString
        End If

        If Not txtSoBL_Search.Text = "" Then
            soCT = txtSoBL_Search.Text.ToString()
            'ElseIf Not Request.Params("So_BL") Is Nothing Then
            '    soBL = Request.Params("So_BL").ToString
        End If

        Dim dtCT As DataTable
        Dim strTT As String = ""
        'strTT = drpTrangThai.SelectedValue.Trim
        'Lay danh sach so chung tu
        'If drpTrangThai.SelectedValue.ToString = "99" Then
        '    strTT = ""
        'Else
        '    strTT = drpTrangThai.SelectedValue.ToString
        'End If
        'If txtSoBL_Search.Text = "" Then
        '    'Lay danh sach so chung tu
        '    dtCT = dtlCTU_Load(mv_objUser.Ma_NV, mv_Ngay_LV, "", soCT, soBL)
        'End If
        If txtSoBL_Search.Text <> "" Then
            'Lay danh sach so chung tu
            dtCT = dtlCTU_Load_Search(mv_objUser.Ma_NV, mv_Ngay_LV, strTT, soCT, soBL)
        Else
            If drpTrangThai.SelectedValue.ToString = "99" Then
                strTT = ""
                dtCT = dtlCTU_Load(mv_objUser.Ma_NV, mv_Ngay_LV, "", soCT, soBL)
            Else
                strTT = drpTrangThai.SelectedValue.ToString
                dtCT = dtlCTU_Load_Search(mv_objUser.Ma_NV, mv_Ngay_LV, strTT, soCT, soBL)
            End If
        End If
        If dtCT.Rows.Count > 0 Then
            dtgDSCT.DataSource = dtCT
            dtgDSCT.CurrentPageIndex = i_PageIndex
            dtgDSCT.DataBind()
            lblTongSo.Text = dtCT.Rows.Count
            i_PageIndex = 0
        Else
            dtgDSCT.DataSource = Nothing
            dtgDSCT.DataBind()
            lblTongSo.Text = ""
            i_PageIndex = 0
        End If
    End Sub
    ' Lay danh sach chung tu chua kiem soat, kiem soat loi, huy
    Private Shared Function dtlCTU_Load_Search(ByVal strUser As String, ByVal lNgayKB As Long, ByVal pv_strTrangThai As String, ByVal SoCT As String, Optional ByVal SoBL As String = "") As DataTable
        Dim dt As New DataTable
        Dim strSQL As String
        Dim strWhereSTT As String = ""
        Dim strNhomNSD As String = ""
        Dim dsNSD As DataSet = Nothing
        Dim where_TT As String = ""
        Try
            strWhereSTT = strWhereSTT & " AND (hdr.MA_NV IN (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN = '" & mv_objUser.MA_CN & "' ) "
          

            strWhereSTT = strWhereSTT & "  ) "
            If SoCT <> "" Then
                strWhereSTT = strWhereSTT & " AND (hdr.So_CT like '%" & SoCT & "%' or hdr.So_BL like '%" & SoCT & "%')"
            End If
            If pv_strTrangThai = "04" Then
                where_TT = "and hdr.TRANG_THAI = '" & pv_strTrangThai & "' and hdr.ma_ks <> 0"
            ElseIf pv_strTrangThai = "06" Then
                where_TT = "and (hdr.TRANG_THAI = '01' and hdr.trang_thai_tt <> 0)"
            ElseIf pv_strTrangThai = "01" Then
                where_TT = "and (hdr.TRANG_THAI = '01' and hdr.trang_thai_tt = 0)"
            ElseIf pv_strTrangThai = "02" Then
                where_TT = "and (hdr.TRANG_THAI = '02' )"
            Else
                where_TT = "and hdr.TRANG_THAI = '" & pv_strTrangThai & "'"
            End If
            'strWhereSTT = strWhereSTT & " AND TTien between '" & mv_objUser.HAN_MUC & "' AND '" & mv_objUser.HAN_MUC_DEN & "' "
            'strWhereSTT = strWhereSTT & " AND hdr.TTien between '" & Double.Parse(mv_objUser.HAN_MUC) * 1000000 & "' AND '" & Double.Parse(mv_objUser.HAN_MUC_DEN) * 1000000 & "' "

            'If SoBL <> "" Then
            '    strWhereSTT = strWhereSTT & " AND So_BL like '%" & SoBL & "%'"
            'End If

            strSQL = " SELECT " & _
                     " hdr.SHKB, hdr.KYHIEU_CT, hdr.SO_CT, hdr.SO_BT, hdr.Ma_NV, hdr.SO_BL, hdr.SO_BT || '/' || hdr.Ma_NV Ten, hdr.TRANG_THAI TT, hdr.MA_DTHU, hdr.TTien, hdr.NGAY_KB, hdr.MA_XA, hdr.TEN_NV,nv.TEN_DN, " & _
                     " Case TRANG_THAI " & _
                     " when '00' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("00") & " alt=" & Business.CTuCommon.Get_TrangThai("00") & "/>' " & _
                     " when '01' then case TRANG_THAI_TT when 0 then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("01", "1") & " alt=" & Business.CTuCommon.Get_TrangThai("01") & "/>' when 1 then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("01", "1", "0", "1") & " alt=" & Business.CTuCommon.Get_TrangThai("07") & "/>' END  " & _
                     " when '03' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("03") & " alt=" & Business.CTuCommon.Get_TrangThai("03") & "/>' " & _
                     " when '04' then case ma_ks when '0' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("04") & " alt=" & Business.CTuCommon.Get_TrangThai("04") & "/>' else '<img src=" & Business.CTuCommon.Get_ImgTrangThai("04", "0", "1") & " alt=" & Business.CTuCommon.Get_TrangThai("04") & "/>' end " & _
                     " when '05' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("05") & " alt=" & Business.CTuCommon.Get_TrangThai("05") & "/>' " & _
                     " when '02' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("02") & " alt=" & Business.CTuCommon.Get_TrangThai("02") & "/>' " & _
                     " when '07' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("07") & " alt=" & Business.CTuCommon.Get_TrangThai("07") & "/>' " & _
                     " End TRANG_THAI" & _
                     " FROM tcs_baolanh_hdr hdr,tcs_dm_nhanvien nv " & _
                     " WHERE (hdr.ma_nv = nv.ma_nv) " & strWhereSTT & _
            IIf(pv_strTrangThai.Length > 0, where_TT, "")
            strSQL = strSQL & _
                     " ORDER BY hdr.so_bt DESC "
            dt = DatabaseHelp.ExecuteToTable(strSQL)
            Return dt
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Shared Function dtlCTU_Load(ByVal strUser As String, ByVal lNgayKB As Long, ByVal pv_strTrangThai As String, ByVal SoCT As String, ByVal SoBL As String) As DataTable
        Dim dt As New DataTable
        Dim strSQL As String
        Dim strWhereSTT As String = ""
        Dim strNhomNSD As String = ""
        Dim dsNSD As DataSet = Nothing
        Try
            'strWhereSTT = strWhereSTT & " AND MA_NV IN (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strUser & "')) "
            strWhereSTT = strWhereSTT & " AND (hdr.MA_NV IN (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN = '" & mv_objUser.MA_CN & "' ) "
            If SoCT <> "" Then
                strWhereSTT = strWhereSTT & " AND (hdr.So_CT ='" & SoCT & "' or hdr.So_BL ='%" & SoCT & "%')"
            End If

            strWhereSTT = strWhereSTT & "  ) "

            'strWhereSTT = strWhereSTT & " AND TTien between '" & mv_objUser.HAN_MUC & "' AND '" & mv_objUser.HAN_MUC_DEN & "' "

            'strWhereSTT = strWhereSTT & " AND hdr.TTien between '" & Double.Parse(mv_objUser.HAN_MUC) * 1000000 & "' AND '" & Double.Parse(mv_objUser.HAN_MUC_DEN) * 1000000 & "' "

            If SoBL <> "" Then
                strWhereSTT = strWhereSTT & " AND hdr.So_BL like '%" & SoBL & "%'"
            End If

            strSQL = " SELECT " & _
                     " hdr.SHKB, hdr.KYHIEU_CT, hdr.SO_CT, hdr.SO_BT, hdr.Ma_NV, hdr.SO_BL, hdr.SO_BT || '/' || hdr.Ma_NV Ten, hdr.TRANG_THAI TT, hdr.MA_DTHU, hdr.TTien, hdr.NGAY_KB, hdr.MA_XA, hdr.TEN_NV,nv.TEN_DN, " & _
                     " Case TRANG_THAI " & _
                     " when '00' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("00") & " alt=" & Business.CTuCommon.Get_TrangThai("00") & "/>' " & _
                     " when '01' then case TRANG_THAI_TT when 0 then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("01", "1") & " alt=" & Business.CTuCommon.Get_TrangThai("01") & "/>' when 1 then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("01", "1", "0", "1") & " alt=" & Business.CTuCommon.Get_TrangThai("07") & "/>' END  " & _
                     " when '03' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("03") & " alt=" & Business.CTuCommon.Get_TrangThai("03") & "/>' " & _
                     " when '04' then case ma_ks when '0' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("04") & " alt=" & Business.CTuCommon.Get_TrangThai("04") & "/>' else '<img src=" & Business.CTuCommon.Get_ImgTrangThai("04", "0", "1") & " alt=" & Business.CTuCommon.Get_TrangThai("04") & "/>' end " & _
                     " when '05' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("05") & " alt=" & Business.CTuCommon.Get_TrangThai("05") & "/>' " & _
                     " when '02' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("02") & " alt=" & Business.CTuCommon.Get_TrangThai("02") & "/>' " & _
                     " when '07' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("07") & " alt=" & Business.CTuCommon.Get_TrangThai("07") & "/>' " & _
                     " End TRANG_THAI" & _
                     " FROM tcs_baolanh_hdr hdr, tcs_dm_nhanvien nv  " & _
                     " WHERE hdr.ma_nv=nv.ma_nv and " & _
                     "hdr.NGAY_KB = " & lNgayKB & strWhereSTT & _
                     "AND hdr.trang_thai <> '00' AND (hdr.trang_thai <> '04' or (hdr.trang_thai = '04' and hdr.ma_ks <> 0))" & _
                     IIf(pv_strTrangThai.Length > 0, "and hdr.TRANG_THAI = '" & pv_strTrangThai & "'", "")

            'IIf(strMa_Cn2.Length > 0, ", ma_cn_2 = '" & strMa_Cn2 & "'", "") & _
            'If pv_strTrangThai = "05" Then
            '    strSQL = strSQL & _
            '    " OR TRANG_THAI = '07')"
            'Else
            '    strSQL = strSQL & _
            '    " ) "
            'End If
            strSQL = strSQL & " ORDER BY hdr.so_bt DESC "
            'strSQL = strSQL & _
            '         " AND MA_NV in (SELECT MA_NV FROM TCS_DM_NHANVIEN WHERE MA_CN =(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" & CInt(strUser) & "')) " & _

            '  " AND MA_DTHU='" & clsCTU.TCS_GetMaDT(strUser) & "'" & _
            dt = DatabaseHelp.ExecuteToTable(strSQL)
            Return dt
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Sub Load_Detail(ByVal strSHKB As String, ByVal strSoCT As String, ByVal strSoBT As String, ByVal strMaNV As String, Optional ByVal strNgayKB As String = "")
        Dim hdr As New DataTable
        Dim dtls As New DataTable
        clsCTU.TCS_GetBaoLanhDetail(strSHKB, strSoCT, strSoBT, strMaNV, hdr, dtls, strNgayKB)

        ' Dua du lieu vao form
        If hdr.Rows.Count > 0 Then
            hdnSoBT.Value = hdr.Rows(0)("so_bt").ToString()
            hdnSoCT.Value = hdr.Rows(0)("so_ct").ToString()
            hdnKHCT.Value = hdr.Rows(0)("kyhieu_ct").ToString()
            hdnNgayCT.Value = hdr.Rows(0)("ngay_ct").ToString()
            hdnNgayKB.Value = hdr.Rows(0)("ngay_kb").ToString()
            hdnTThai.Value = hdr.Rows(0)("trang_thai").ToString()
            hdnMaDThu.Value = hdr.Rows(0)("ma_dthu").ToString()
            hdnMaNV_LapCT.Value = hdr.Rows(0)("ma_nv").ToString()
            hdnSHKB_CT.Value = hdr.Rows(0)("shkb").ToString()
            hdnSHKB.Value = hdr.Rows(0)("shkb").ToString()

            hdnMaNNT.Value = hdr.Rows(0)("ma_nnthue").ToString()
            hdnTongSoNgay.Value = hdr.Rows(0)("songay_bl").ToString()
            hdnNgayHL.Value = hdr.Rows(0)("ngay_batdau").ToString()
            hdnNgayHetHan.Value = hdr.Rows(0)("ngay_ketthuc").ToString()

            txtKHCT.Text = hdr.Rows(0)("kyhieu_ct").ToString()
            txtSoCT.Text = hdr.Rows(0)("so_ct").ToString()
            txtNgayLap.Text = ConvertNumberToDate(hdr.Rows(0)("ngay_ct").ToString()).ToString("dd/MM/yyyy")
            txtNguoiLap.Text = hdr.Rows(0)("ten_nv").ToString()
            txtNguoiKS.Text = hdr.Rows(0)("ten_ks").ToString()

            txtSoBaoLanh.Text = hdr.Rows(0)("so_bl").ToString()
            'rdbKieuBL.SelectedValue = hdr.Rows(0)("kieu_bl").ToString()
            txtNgayHL.Text = hdr.Rows(0)("ngay_batdau").ToString()
            txtNgayHetHan.Text = hdr.Rows(0)("ngay_ketthuc").ToString()
            txtTongSoNgay.Text = hdr.Rows(0)("songay_bl").ToString()
            txtMaNNT.Text = hdr.Rows(0)("ma_nnthue").ToString()
            txtTenNNT.Text = hdr.Rows(0)("ten_nnthue").ToString()
            txtDiaChi.Text = hdr.Rows(0)("dc_nnthue").ToString()
            txtTK.Text = hdr.Rows(0)("tk_no").ToString()
            txtTenTKNS.Text = hdr.Rows(0)("ten_tk_ns").ToString()
            txtMaCQT.Text = hdr.Rows(0)("ma_cqthu").ToString()
            txtTenCQT.Text = hdr.Rows(0)("ten_cqthu").ToString()
            'txtMaHQ.Text = hdr.Rows(0)("ma_hq_ph").ToString()
            txtToKhaiSo.Text = hdr.Rows(0)("so_tk").ToString()
            txtVTD01.Text = hdr.Rows(0)("vt_don").ToString()
            txtNgayVTD01.Text = hdr.Rows(0)("ngay_vtd").ToString()

            txtVTD02.Text = hdr.Rows(0)("vt_don2").ToString()
            txtNgayVTD02.Text = hdr.Rows(0)("ngay_vtd2").ToString()
            txtVTD03.Text = hdr.Rows(0)("vt_don3").ToString()
            txtNgayVTD03.Text = hdr.Rows(0)("ngay_vtd3").ToString()
            txtVTD04.Text = hdr.Rows(0)("vt_don4").ToString()
            txtNgayVTD04.Text = hdr.Rows(0)("ngay_vtd4").ToString()
            txtVTD05.Text = hdr.Rows(0)("vt_don5").ToString()
            txtNgayVTD05.Text = hdr.Rows(0)("ngay_vtd5").ToString()
            txtMA_HQ_KB.Text = hdr.Rows(0)("ma_hq").ToString()
            txtTenMA_HQ_KB.Text = hdr.Rows(0)("ten_hq").ToString()
            txtFone_Num.Text = hdr.Rows(0)("fone_num").ToString()
            txt_FAX_Num.Text = hdr.Rows(0)("fax_num").ToString()
            txtSo_DKKD.Text = hdr.Rows(0)("so_dkkd").ToString()
            txtCoQCap.Text = hdr.Rows(0)("cq_cap").ToString()
            txtNgay_CQCap.Text = hdr.Rows(0)("ngay_cqcap").ToString()
            '//' ****************************************************************************
            '//' Người sửa: Anhld
            '//' Ngày 25.09.2012
            '//' Mục đích: Thay doi ngay_ht = ngay_tk
            '//'***************************************************************************** */
            txtNgayDK.Text = hdr.Rows(0)("ngay_tk").ToString()
            'txtNgayDK.Text = hdr.Rows(0)("ngay_ht").ToString()

            txtCifNumber.Text = hdr.Rows(0)("cif").ToString()

            txtLHXNK.Text = hdr.Rows(0)("lhxnk").ToString()
            txtDescLHXNK.Text = hdr.Rows(0)("ten_lhxnk").ToString()
            drpLoaiTien.SelectedValue = hdr.Rows(0)("ma_loaitien").ToString()
            txtDienGiai.Text = hdr.Rows(0)("dien_giai").ToString()
            txtTrangThaiCT.Text = Business.CTuCommon.Get_TenTrangThai(hdr.Rows(0)("trang_thai").ToString())
            hdnTrangThaiTT.value = hdr.Rows(0)("TRANG_THAI_TT").ToString
            'If hdr.Rows(0)("TRANG_THAI_TT").ToString <> "1" Then
            '    txtTrangThai.Text = "Chưa thanh toán"
            'Else
            '    txtTrangThai.Text = "Đã thanh toán"
            'End If
            hdnSoCTTuChinh.Value = hdr.Rows(0)("so_ct_tuchinh").ToString()

            '***************************************************************'
            'Author: Anhld
            'Goals: Them ma hai quan, ten hai quan, ma hai quan phat hanh, ten hai quan phat hanh
            'Date: 20/09/2012
            txtMaHQ.Text = hdr.Rows(0)("ma_hq").ToString()
            txtTenHQ.Text = hdr.Rows(0)("ten_hq").ToString()
            txtMaHQPH.Text = hdr.Rows(0)("ma_hq_ph").ToString()
            txtTenMaHQPH.Text = hdr.Rows(0)("ten_hq_ph").ToString()
            '***************************************************************'
            ' them hop dong ngoai thương
            txtHD_NT.Text = hdr.Rows(0)("hd_nt").ToString()
            txtNgay_HD.Text = hdr.Rows(0)("ngay_hd").ToString()
            txtHDon.Text = hdr.Rows(0)("hoa_don").ToString()
            txtNgay_HDon.Text = hdr.Rows(0)("ngay_hdon").ToString()
            txtVTD.Text = hdr.Rows(0)("vt_don").ToString()
            txtNgay_VTD.Text = hdr.Rows(0)("ngay_vtd").ToString()
            'txtCapChuong.Text = hdr.Rows(0)("ma_chuong").ToString()
            txtMA_DV_DD.Text = hdr.Rows(0)("MA_DV_DD").ToString()
            txtTen_DV_DD.Text = hdr.Rows(0)("TEN_DV_DD").ToString()
            ddlLoaiBL.SelectedValue = hdr.Rows(0)("type_bl").ToString()
            Load_ddl_LoaiBL()
            DM_LTIEN(hdr.Rows(0)("ma_loaitien").ToString())
            If txtToKhaiSo.Text.Trim = "99999" Then
                divHD_NT.Attributes("style") = "display:block;"
            End If
            If hdr.Rows(0)("trang_thai").ToString() = "03" Then
                txtLyDoCTra.Text = hdr.Rows(0)("ly_do_ctra").ToString()
                txtLyDoCTra.Enabled = False
            End If
            If hdr.Rows(0)("LY_DO_HUY") IsNot Nothing Then
                If hdr.Rows(0)("LY_DO_HUY").ToString.Trim.Length > 0 Then
                    drpLyDoHuy.SelectedValue = hdr.Rows(0)("LY_DO_HUY").ToString.Trim
                    drpLyDoHuy.Enabled = False
                
                End If
            Else
                drpLyDoHuy.Enabled = True
            End If
            txtTongTien.Text = VBOracleLib.Globals.Format_Number(hdr.Rows(0)("ttien").ToString().Trim, ".")
            txtTS_TienBL_T24.Text = VBOracleLib.Globals.Format_Number(hdr.Rows(0)("ttien_nt").ToString().Trim, ".")
            txtTT_TTHU.Text = VBOracleLib.Globals.Format_Number(hdr.Rows(0)("ttien_tthu").ToString().Trim, ".")
        End If
        '' Du lieu chi tiet
        'If dtls.Rows.Count > 0 Then
        '    If dtls.Rows.Count >= 1 Then
        '        txtMaQuy_1.Text = dtls.Rows(0)("ma_chuong").ToString()
        '        txtMaChuong_1.Text = dtls.Rows(0)("ma_nkt").ToString()
        '        txtNDKT_1.Text = dtls.Rows(0)("ma_ndkt").ToString()
        '        txtNoiDung_1.Text = dtls.Rows(0)("noi_dung").ToString()
        '        txtTien_1.Text = dtls.Rows(0)("sotien").ToString()
        '        txtKyThue_1.Text = dtls.Rows(0)("ky_thue").ToString()
        '        txtKHTK_1.Text = dtls.Rows(0)("ma_khtk").ToString()
        '        txtTongTien.Text = dtls.Rows(0)("sotien").ToString()
        '    End If

        '    If dtls.Rows.Count >= 2 Then
        '        txtMaQuy_2.Text = dtls.Rows(1)("ma_chuong").ToString()
        '        txtMaChuong_2.Text = dtls.Rows(1)("ma_nkt").ToString()
        '        txtNDKT_2.Text = dtls.Rows(1)("ma_ndkt").ToString()
        '        txtNoiDung_2.Text = dtls.Rows(1)("noi_dung").ToString()
        '        txtTien_2.Text = dtls.Rows(1)("sotien").ToString()
        '        txtKyThue_2.Text = dtls.Rows(1)("ky_thue").ToString()
        '        txtKHTK_2.Text = dtls.Rows(1)("ma_khtk").ToString()
        '        txtTongTien.Text = Integer.Parse(txtTongTien.Text) + Integer.Parse(dtls.Rows(1)("sotien").ToString())
        '    End If

        '    If dtls.Rows.Count >= 3 Then
        '        txtMaQuy_3.Text = dtls.Rows(2)("ma_chuong").ToString()
        '        txtMaChuong_3.Text = dtls.Rows(2)("ma_nkt").ToString()
        '        txtNDKT_3.Text = dtls.Rows(2)("ma_ndkt").ToString()
        '        txtNoiDung_3.Text = dtls.Rows(2)("noi_dung").ToString()
        '        txtTien_3.Text = dtls.Rows(2)("sotien").ToString()
        '        txtKyThue_3.Text = dtls.Rows(2)("ky_thue").ToString()
        '        txtKHTK_3.Text = dtls.Rows(2)("ma_khtk").ToString()
        '        txtTongTien.Text = Integer.Parse(txtTongTien.Text) + Integer.Parse(dtls.Rows(2)("sotien").ToString())
        '    End If

        '    If dtls.Rows.Count >= 4 Then
        '        txtMaQuy_4.Text = dtls.Rows(3)("ma_chuong").ToString()
        '        txtMaChuong_4.Text = dtls.Rows(3)("ma_nkt").ToString()
        '        txtNDKT_4.Text = dtls.Rows(3)("ma_ndkt").ToString()
        '        txtNoiDung_4.Text = dtls.Rows(3)("noi_dung").ToString()
        '        txtTien_4.Text = dtls.Rows(3)("sotien").ToString()
        '        txtKyThue_4.Text = dtls.Rows(3)("ky_thue").ToString()
        '        txtKHTK_4.Text = dtls.Rows(3)("ma_khtk").ToString()
        '        txtTongTien.Text = Integer.Parse(txtTongTien.Text) + Integer.Parse(dtls.Rows(3)("sotien").ToString())
        '    End If

        '    If dtls.Rows.Count >= 5 Then
        '        txtMaQuy_5.Text = dtls.Rows(4)("ma_chuong").ToString()
        '        txtMaChuong_5.Text = dtls.Rows(4)("ma_nkt").ToString()
        '        txtNDKT_5.Text = dtls.Rows(4)("ma_ndkt").ToString()
        '        txtNoiDung_5.Text = dtls.Rows(4)("noi_dung").ToString()
        '        txtTien_5.Text = dtls.Rows(4)("sotien").ToString()
        '        txtKyThue_5.Text = dtls.Rows(4)("ky_thue").ToString()
        '        txtKHTK_5.Text = dtls.Rows(4)("ma_khtk").ToString()
        '        txtTongTien.Text = Integer.Parse(txtTongTien.Text) + Integer.Parse(dtls.Rows(4)("sotien").ToString())
        '    End If
        '    If dtls.Rows.Count >= 6 Then
        '        txtMaQuy_6.Text = dtls.Rows(5)("ma_chuong").ToString()
        '        txtMaChuong_6.Text = dtls.Rows(5)("ma_nkt").ToString()
        '        txtNDKT_6.Text = dtls.Rows(5)("ma_ndkt").ToString()
        '        txtNoiDung_6.Text = dtls.Rows(5)("noi_dung").ToString()
        '        txtTien_6.Text = dtls.Rows(5)("sotien").ToString()
        '        txtKyThue_6.Text = dtls.Rows(5)("ky_thue").ToString()
        '        txtKHTK_6.Text = dtls.Rows(5)("ma_khtk").ToString()
        '        txtTongTien.Text = Integer.Parse(txtTongTien.Text) + Integer.Parse(dtls.Rows(5)("sotien").ToString())
        '    End If
        '    If dtls.Rows.Count >= 7 Then
        '        txtMaQuy_7.Text = dtls.Rows(6)("ma_chuong").ToString()
        '        txtMaChuong_7.Text = dtls.Rows(6)("ma_nkt").ToString()
        '        txtNDKT_7.Text = dtls.Rows(6)("ma_ndkt").ToString()
        '        txtNoiDung_7.Text = dtls.Rows(6)("noi_dung").ToString()
        '        txtTien_7.Text = dtls.Rows(6)("sotien").ToString()
        ''        txtKyThue_7.Text = dtls.Rows(6)("ky_thue").ToString()
        ''        txtKHTK_7.Text = dtls.Rows(6)("ma_khtk").ToString()
        ''        txtTongTien.Text = Integer.Parse(txtTongTien.Text) + Integer.Parse(dtls.Rows(6)("sotien").ToString())
        ''    End If

        ''    'Format number
        ''    txtTien_1.Text = VBOracleLib.Globals.Format_Number(txtTien_1.Text, ".")
        ''    txtTien_2.Text = VBOracleLib.Globals.Format_Number(txtTien_2.Text, ".")
        ''    txtTien_3.Text = VBOracleLib.Globals.Format_Number(txtTien_3.Text, ".")
        ''    txtTien_4.Text = VBOracleLib.Globals.Format_Number(txtTien_4.Text, ".")
        ''    txtTien_5.Text = VBOracleLib.Globals.Format_Number(txtTien_5.Text, ".")
        ''    txtTien_6.Text = VBOracleLib.Globals.Format_Number(txtTien_6.Text, ".")
        ''    txtTien_7.Text = VBOracleLib.Globals.Format_Number(txtTien_7.Text, ".")
        ''    txtTongTien.Text = VBOracleLib.Globals.Format_Number(txtTongTien.Text, ".")
        ''    txtTS_TienBL_T24.Text = txtTongTien.Text
        'EnableButon(hdnTThai.Value)
        'End If

    End Sub

    Public Shared Sub GetBaoLanh(ByVal strSHKB As String, ByVal strSoCT As String, ByVal strSoBT As String, ByVal strMaNV As String, _
                               ByRef hdr As DataTable, ByRef dtls As DataTable)
        clsCTU.TCS_GetBaoLanh(strSHKB, strSoCT, strSoBT, CInt(strMaNV), hdr, dtls)
    End Sub

    Sub ResetField()
        btnKiemSoat.Enabled = False
        btnChuyenTra.Enabled = False
        btnGuiLai.Enabled = False
        btnHuy.Enabled = False
        cmdThanhToan.Enabled = False
        hdnSHKB.Value = ""
        hdnMaNV.Value = ""
        hdnSoBT.Value = ""
        hdnSoCT.Value = ""
        hdnKHCT.Value = ""
        hdnNgayCT.Value = ""
        hdnNgayKB.Value = ""
        hdnTThai.Value = ""
        hdnMaDThu.Value = ""
        hdnTrangThaiTT.Value = ""
        hdnMaNNT.Value = ""
        hdnTongSoNgay.Value = ""
        hdnNgayHL.Value = ""
        hdnNgayHetHan.Value = ""

        hdnMethod.Value = ""
        lblStatus.Text = "Xem thông tin bảo lãnh"
        txtKHCT.Text = ""
        txtSoCT.Text = ""
        txtNgayLap.Text = ""
        txtNguoiLap.Text = ""
        txtNguoiKS.Text = ""
        txtSoBaoLanh.Text = ""
        'rdbKieuBL.SelectedValue = "0"
        txtNgayHL.Text = ""
        txtNgayHetHan.Text = ""
        txtTongSoNgay.Text = ""
        txtMaNNT.Text = ""
        txtTenNNT.Text = ""
        txtDiaChi.Text = ""
        txtTK.Text = ""
        txtTenTKNS.Text = ""
        txtMaCQT.Text = ""
        txtTenCQT.Text = ""
        txtToKhaiSo.Text = ""
        txtNgayDK.Text = "" 'Now.ToString("dd/MM/yyyy")
        txtLHXNK.Text = ""
        txtDescLHXNK.Text = ""
        txtDienGiai.Text = ""
        txtTongTien.Text = ""
        txtHD_NT.Text = ""
        txtNgay_HD.Text = ""
        txtHDon.Text = ""
        txtNgay_HDon.Text = ""
        txtVTD.Text = ""
        txtNgay_VTD.Text = ""
        txtCifNumber.Text = ""
        
        txtMaQuy_1.Text = ""
        txtMaQuy_2.Text = ""
        txtMaQuy_3.Text = ""
        txtMaQuy_4.Text = ""
        txtMaQuy_5.Text = ""
        txtMaQuy_6.Text = ""
        txtMaQuy_7.Text = ""

        txtMaChuong_1.Text = ""
        txtMaChuong_2.Text = ""
        txtMaChuong_3.Text = ""
        txtMaChuong_4.Text = ""
        txtMaChuong_5.Text = ""
        txtMaChuong_6.Text = ""
        txtMaChuong_7.Text = ""

        txtNDKT_1.Text = ""
        txtNDKT_2.Text = ""
        txtNDKT_3.Text = ""
        txtNDKT_4.Text = ""
        txtNDKT_5.Text = ""
        txtNDKT_6.Text = ""
        txtNDKT_7.Text = ""

        txtNoiDung_1.Text = ""
        txtNoiDung_2.Text = ""
        txtNoiDung_3.Text = ""
        txtNoiDung_4.Text = ""
        txtNoiDung_5.Text = ""
        txtNoiDung_6.Text = ""
        txtNoiDung_7.Text = ""

        txtTien_1.Text = ""
        txtTien_2.Text = ""
        txtTien_3.Text = ""
        txtTien_4.Text = ""
        txtTien_5.Text = ""
        txtTien_6.Text = ""
        txtTien_7.Text = ""

        txtKyThue_1.Text = ""
        txtKyThue_2.Text = ""
        txtKyThue_3.Text = ""
        txtKyThue_4.Text = ""
        txtKyThue_5.Text = ""
        txtKyThue_6.Text = ""
        txtKyThue_7.Text = ""

        txtKHTK_1.Text = ""
        txtKHTK_2.Text = ""
        txtKHTK_3.Text = ""
        txtKHTK_4.Text = ""
        txtKHTK_5.Text = ""
        txtKHTK_6.Text = ""
        txtKHTK_7.Text = ""
    End Sub

#End Region

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        LayDSCT()
    End Sub

    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Dim strSo_CT As String = txtSoCT.Text.Trim
        If strSo_CT.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Phải chọn 1 chứng từ để in")
        Else
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmInThuBL.aspx?SoCT=" & strSo_CT, "ManPower")
        End If

    End Sub
    Protected Sub cmdThanhToan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdThanhToan.Click
        Try
            Dim strSHKB As String
            Dim strSoCT As String
            Dim strSoBT As String
            Dim strTT As String
            If hdnSHKB.Value.Trim <> "" And hdnSoCT.Value.Trim <> "" And hdnSoBT.Value.Trim <> "" And hdnMaNV.Value.Trim <> "" Then
                strSHKB = hdnSHKB.Value.Trim
                strSoCT = hdnSoCT.Value.Trim
                strSoBT = hdnSoBT.Value.Trim
                strTT = hdnTThai.Value.Trim
            Else
                lblError.Text = "Bạn phải chọn 1 chứng từ để thực hiện!"
                Exit Sub
            End If
            If Not (txtMatKhau.Text.Length = 0) Then
                If Not (mv_objUser.Mat_Khau.Equals(EncryptStr(txtMatKhau.Text))) Then
                    lblError.Text = "Mật khẩu của kiểm soát không hợp lệ.Hãy thử lại"
                    'EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If
            Else
                lblError.Text = "Phải nhập mật khẩu khi kiểm soát."
                'EnabledButtonWhenDoTransaction(True)
                Exit Sub
            End If
            Dim objHDR As New infChungTuHDR
            objHDR.So_CT = strSoCT
            objHDR.SHKB = strSHKB
            objHDR.Ngay_KB = hdnNgayKB.Value
            objHDR.Ma_NV = hdnMaNV.Value
            If Check_CT_Goc(strSoCT) = False Then
                lblError.Text = "Không thanh toán được chứng từ vì đang được tu chỉnh!"
                cmdThanhToan.Enabled = False
                Exit Sub
            End If
            If strTT = "01" Then
                ChungTu.buChungTu.UpdateTrangThaiThanhToan(strSoCT, strTT, strSoBT, strSHKB)
                lblError.Text = ""
                lblSuccess.Text = "Thực hiện thanh toán thành công!"
                txtTrangThai.Text = "Đã thanh toán"
                hdnTrangThaiTT.Value = "1"
                EnableButon("01")
                LayDSCT()
            Else
                lblStatus.Text = ""
                lblError.Text = "Thao tác không thành công!"
                Exit Sub
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlLoaiBL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLoaiBL.SelectedIndexChanged
        Load_ddl_LoaiBL()
    End Sub
    Sub Load_ddl_LoaiBL()
        If ddlLoaiBL.SelectedValue = "31" Then
            rowMAHQPH.Visible = True
            rowMAHQ.Visible = True
            rowLHXNK.Visible = True
            rowNGAYDK.Visible = True
            rowSOTK.Visible = True
            rowLoaiTien.Visible = True
            rowHD.Visible = False
            rowHD.Visible = False
            rowVTD.Visible = False
            rowVTD1.Visible = False
            rowVTD2.Visible = False
            rowVTD3.Visible = False
            rowVTD4.Visible = False
            rowVTD5.Visible = False
            rowMA_HQ_KB.Visible = False
        ElseIf ddlLoaiBL.SelectedValue = "32" Then
            rowMAHQPH.Visible = False
            rowMAHQ.Visible = True
            rowLHXNK.Visible = False
            rowNGAYDK.Visible = False
            rowSOTK.Visible = False
            rowLoaiTien.Visible = False
            rowHD.Visible = True
            rowVTD.Visible = False
            If txtVTD01.Text <> "" Then
                rowVTD1.Visible = True
            Else
                rowVTD1.Visible = False
            End If
            If txtVTD02.Text <> "" Then
                rowVTD2.Visible = True
            Else
                rowVTD2.Visible = False
            End If
            If txtVTD03.Text <> "" Then
                rowVTD3.Visible = True
            Else
                rowVTD3.Visible = False
            End If
            If txtVTD04.Text <> "" Then
                rowVTD4.Visible = True
            Else
                rowVTD4.Visible = False
            End If
            If txtVTD05.Text <> "" Then
                rowVTD5.Visible = True
            Else
                rowVTD5.Visible = False
            End If
            rowMA_HQ_KB.Visible = True
        ElseIf ddlLoaiBL.SelectedValue = "33" Then
            rowMAHQPH.Visible = False
            rowMAHQ.Visible = True
            rowLHXNK.Visible = False
            rowNGAYDK.Visible = False
            rowSOTK.Visible = False
            rowLoaiTien.Visible = False
            rowHD.Visible = False
            rowVTD.Visible = False
            rowVTD1.Visible = False
            rowVTD2.Visible = False
            rowVTD3.Visible = False
            rowVTD4.Visible = False
            rowVTD5.Visible = False
            rowMA_HQ_KB.Visible = False
        End If
    End Sub
    Private Sub DM_LTIEN(ByVal pv_strMA_LTIEN As String)
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable

        Dim strSQL As String = "SELECT MA_LT, MA_LT ||'-'||ten_lt ten_lt from tcs_dm_loaitien_thue where ma_lt = '" & pv_strMA_LTIEN & "'"
        dt = DataAccess.ExecuteToTable(strSQL)
        If Not dt Is Nothing Then
            drpLoaiTien.DataSource = dt
            drpLoaiTien.DataTextField = "ten_lt"
            drpLoaiTien.DataValueField = "MA_LT"
            drpLoaiTien.DataBind()
        End If

    End Sub
End Class
