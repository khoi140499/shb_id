﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmLapBaoLanhHQ.aspx.vb" Inherits="pages_BaoLanhHQ_frmLapBaoLanhHQ"
    Title="Lập bảo lãnh hải quan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popcalendar.js" type="text/javascript"></script>

    <script src="../../javascript/jquery/jquery-1.3.2.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">

/**
        Begin Duc Anh Added
    **/
    function pageLoad() {
     Sys.Net.WebRequestManager.add_invokingRequest(onInvoke);
     Sys.Net.WebRequestManager.add_completedRequest(onComplete);  
        $(document).ready(function () {
        document.getElementById('divStatus').innerHTML=""
        /***************************KEY_UP**********************************/
        $("#<%=txtMaHQ.clientID%>").keyup(function () {
            $(this).val($(this).val().replace(/([a-z])/, function (s) { return s.toUpperCase() }));
        });
         $("#<%=txtMA_HQ_KB.clientID%>").keyup(function () {
            $(this).val($(this).val().replace(/([a-z])/, function (s) { return s.toUpperCase() }));
        });
        $("#<%=txtSoBaoLanh.clientID%>").keyup(function () {
            $(this).val($(this).val().replace(/([a-z])/, function (s) { return s.toUpperCase() }));
          });
        $("#<%=txtMaHQPH.clientID%>").keyup(function () {
            $(this).val($(this).val().replace(/([a-z])/, function (s) { return s.toUpperCase() }));
        });


        $("#<%=txtLHXNK.clientID%>").keyup(function () {
            $(this).val($(this).val().replace(/([a-z])/, function (s) { return s.toUpperCase() }));
        });

        /************************KEY_BLUR*************************************/
        $("#<%=txtMaCQT.clientID%>").blur(function () {
            Get_TenCQThu();
        });
//         $("#<%=txtSoBaoLanh.clientID%>").blur(function () {
//                    Get_SoBL();
//                });

        $("#<%=txtMaHQ.clientID%>").blur(function () {
            Get_TenMaHQ();
        });
        $("#<%=txtMA_HQ_KB.clientID%>").blur(function () {
            Get_TenMaHQ_KB();
        });
        $("#<%=txtMaHQPH.clientID%>").blur(function () {
            Get_TenMaHQPH();
        });

        $("#<%=txtLHXNK.clientID%>").blur(function () {
            if ($("#<%=txtLHXNK.clientID%>").val() != "") {
                Get_TenLHXNK();
            }
        });

        $("#<%=txtMaNNT.clientID%>").blur(function () {
            if ($("#<%=txtMaNNT.clientID%>").val() != "") {
                Get_TenNNT();
                //Get_CifNum();
            }
        });
//        $("#<%=txtMaNNT.clientID%>").keypress(function () {
//            if (event.keyCode == 13 || e.keyCode == 13) { Get_TenNNT(); };
//        });
//         $("#<%=txtToKhaiSo.clientID%>").blur(function () {
//         if  ($get('<%=txtToKhaiSo.clientID%>').value=="99999")
//         {
//             $('#<%=divHD_NT.ClientID %>').show();
//         }else
//             $('#<%=divHD_NT.ClientID %>').hide();
//         {

//         }
//            
//        });
        
        $("#<%=chkCheck_1.clientID%>").click(function(){
            jsCalTotal();
        });
        $("#<%=chkCheck_2.clientID%>").click(function(){
            jsCalTotal();
        });
        $("#<%=chkCheck_3.clientID%>").click(function(){
            jsCalTotal();
        });
        $("#<%=chkCheck_4.clientID%>").click(function(){
            jsCalTotal();
        });
        $("#<%=chkCheck_5.clientID%>").click(function(){
            jsCalTotal();
        });
        $("#<%=chkCheck_6.clientID%>").click(function(){
            jsCalTotal();
        });
        $("#<%=chkCheck_7.clientID%>").click(function(){
            jsCalTotal();
        });


        /*************************KEY_PRESS************************************/
        $("#<%=txtMaNNT.clientID%>").keypress(function () {
            if (event.keyCode == 13 || e.keyCode == 13) { ShowLov('NNT'); };
        });

        $("#<%=txtLHXNK.clientID%>").keypress(function () {
            if (event.keyCode == 13) { ShowLov('LHXNK'); };
        });

        $("#<%=txtMaCQT.clientID%>").keypress(function () {
            if (event.keyCode == 13) { ShowLov('CQT'); };
        });

        $("#<%=txtMaHQ.clientID%>").keypress(function () {
            if (event.keyCode == 13 || e.keyCode == 13) { ShowLov('MHQ'); };
        });
        $("#<%=txtMA_HQ_KB.clientID%>").keypress(function () {
            if (event.keyCode == 13 || e.keyCode == 13) { ShowLov('MHQ_KB'); };
        });
        $("#<%=txtMaHQPH.clientID%>").keypress(function () {
            if (event.keyCode == 13) { ShowLov('MHQPH'); };
        });
       // jsCalTotal();
    });
}
    /**
    Begin Duc Anh Added
    **/
    /****************************TEN_CQTHU*********************************/
    
    function jsAddNewDetailRow(rowID)
    {
        if (rowID=='2')
        
        {   
           //alert($get('<%=txtNgayVTD01.clientID%>').value);
           $('#<%=rowVTD2.ClientID %>').show();
           //document.getElementById('#<%=txtNgayVTD01.ClientID %>').style.display = 'none';
           //$get('#<%=txtNgayVTD01.ClientID %>').style.display = 'none';
          //$('#<%=rowVTD2.ClientID %>').show(); 
//           document.getElementById('<% = rowVTD2.ClientID %>').style.visibility = 'hidden';
//          document.getElementById('<% = rowVTD2.ClientID %>').style.display = "block";
//          //document.getElementById('<%=rowVTD2.clientID%>').style.display='block';
//          // // document.getElementById('rowVTD2').style.display='';
        }
        
        else if (rowID=='3')
        {
            document.getElementById('rowVTD3').style.display='';
        }
        else if (rowID=='4')
        {
            document.getElementById('rowVTD4').style.display='';
        }
        else if (rowID=='5')
        {
            document.getElementById('rowVTD5').style.display='';
        }         
    }
   
    function Get_TenCQThu() {
        PageMethods.Get_TenCQThu($get('<%=txtMaCQT.clientID%>').value, Get_TenCQThu_Complete, Get_TenCQThu_Error);
    }

    function Get_TenCQThu_Complete(result, methodName) {
        if (result.length > 0) {
            $get('<%=txtTenCQT.clientID%>').value = result;
        }
        else {
            $get('<%=txtTenCQT.clientID%>').value = result;
        }
    }
    function Get_TenCQThu_Error(error, userContext, methodName) {
        if (error !== null) {
            $get('<%=txtTenCQT.clientID%>').value = result;
        }
    }
  
    /***************************TEN_MA_HQ**********************************/
    function Get_TenMaHQ() {
        PageMethods.Get_TenMaHQ($get('<%=txtMaHQ.clientID%>').value, Get_TenMaHQ_Complete, Get_TenMaHQ_Error);
    }

    function Get_TenMaHQ_Complete(result, methodName) {
        if (result.length > 0) {
            $get('<%=txtTenHQ.clientID%>').value = result;
        }
        else {
            $get('<%=txtTenHQ.clientID%>').value = result;
        }
    }
    function Get_TenMaHQ_Error(error, userContext, methodName) {
        if (error !== null) {
            $get('<%=txtTenHQ.clientID%>').value = result;
        }
    }
     /***************************TEN_MA_HQ_KB**********************************/
     function Get_TenMaHQ_KB() {
        PageMethods.Get_TenMaHQ($get('<%=txtMA_HQ_KB.clientID%>').value, Get_TenMaHQ_KB_Complete, Get_TenMaHQ_KB_Error);
    }

    function Get_TenMaHQ_KB_Complete(result, methodName) {
        if (result.length > 0) {
            $get('<%=txtTenMA_HQ_KB.clientID%>').value = result;
        }
        else {
            $get('<%=txtTenMA_HQ_KB.clientID%>').value = result;
        }
    }
    function Get_TenMaHQ_KB_Error(error, userContext, methodName) {
        if (error !== null) {
            $get('<%=txtTenMA_HQ_KB.clientID%>').value = result;
        }
    }
    
    /***************************TEN_MA_HQ_PH**********************************/
    function Get_TenMaHQPH() {
        PageMethods.Get_TenMaHQ($get('<%=txtMaHQPH.clientID%>').value, Get_TenMaHQPH_Complete, Get_TenMaHQPH_Error);
    }

    function Get_TenMaHQPH_Complete(result, methodName) {
        if (result.length > 0) {
            $get('<%=txtTenMaHQPH.clientID%>').value = result;
        }
        else {
            $get('<%=txtTenMaHQPH.clientID%>').value = result;
        }
    }
    function Get_TenMaHQPH_Error(error, userContext, methodName) {
        if (error !== null) {
            $get('<%=txtTenMaHQPH.clientID%>').value = result;
        }
    }
    /*****************************TEN_LHXNK********************************/
    function Get_TenLHXNK() {
        PageMethods.Get_TenLHXNK($get('<%=txtLHXNK.clientID%>').value, Get_TenLHXNK_Complete, Get_TenLHXNK_Error);
    }

    function Get_TenLHXNK_Complete(result, methodName) {
        if (result.length > 0) {
            $get('<%=txtDescLHXNK.clientID%>').value = result;
        }
        else {
            $get('<%=txtDescLHXNK.clientID%>').value = result;
        }
    }
    function Get_TenLHXNK_Error(error, userContext, methodName) {
        if (error !== null) {
            $get('<%=txtDescLHXNK.clientID%>').value = result;
        }
    }
    function Get_SoBL() {
    if ($get('<%=txtSoBaoLanh.clientID%>').value!=""){
         var strMa_NV = $get('<%=hdnMaNV.clientID%>').value;
//         $get('<%=txtNgayHL.clientID%>').value='';
//         $get('<%=txtNgayHetHan.clientID%>').value='';
//         $get('<%=txtDienGiai.clientID%>').value='';
//         $get('<%=txtTS_TienBL_T24.clientID%>').value='';
//         $get('<%=txtCifNumber.clientID%>').value='';
        //Bỏ check số MD
        PageMethods.Get_SoBL($get('<%=txtSoBaoLanh.clientID%>').value.trim(), strMa_NV,Get_SoBL_Complete, Get_SoBL_Error);
     }else{
        alert('Số bảo lãnh không được để trống!');
     }
    } 

    function Get_SoBL_Complete(result, methodName) {
        if (result.length > 0) {
        document.getElementById('divStatus').innerHTML=""
            document.getElementById('divStatus').innerHTML
            var xmlstring = result;
            var xmlDoc = jsGetXMLDoc(xmlstring);
            ProcessSO_BL(xmlDoc);
        }else {
       // alert('vào đê');
          document.getElementById('divStatus').innerHTML="Lỗi trong quá trình lấy thông tin số bảo lãnh." 
        }
    }
    function Get_SoBL_Error(error, userContext, methodName) {
        document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin số bảo lãnh.Mô tả : " + error;
    }
     function ProcessSO_BL(xmlDoc)    
      { 
      var rootCTU_HDR = xmlDoc.getElementsByTagName('PARAMETER')[0]; 
        if (rootCTU_HDR!=null)
       {  
        if (xmlDoc.getElementsByTagName("RETCODE")[0].childNodes[0].nodeValue=="0")
           {
           
        	if (xmlDoc.getElementsByTagName("NGAYLAP")[0].childNodes[0]!=null) $get('<%=hdnNgayT24.clientID%>').value =xmlDoc.getElementsByTagName("NGAYLAP")[0].childNodes[0].nodeValue;        
        	if (xmlDoc.getElementsByTagName("NGAYHL")[0].childNodes[0]!=null) $get('<%=txtNgayHL.clientID%>').value = xmlDoc.getElementsByTagName("NGAYHL")[0].childNodes[0].nodeValue;        
        	if (xmlDoc.getElementsByTagName("NGAYHH")[0].childNodes[0]!=null) $get('<%=txtNgayHetHan.clientID%>').value =xmlDoc.getElementsByTagName("NGAYHH")[0].childNodes[0].nodeValue;                  	
        	if(xmlDoc.getElementsByTagName("DIENGIAI")[0].childNodes[0]!=null) $get('<%=txtDienGiai.clientID%>').value = xmlDoc.getElementsByTagName("DIENGIAI")[0].childNodes[0].nodeValue;                  	
        	if(xmlDoc.getElementsByTagName("AMOUNT")[0].childNodes[0]!=null) $get('<%=txtTS_TienBL_T24.clientID%>').value = xmlDoc.getElementsByTagName("AMOUNT")[0].childNodes[0].nodeValue;
        	if(xmlDoc.getElementsByTagName("CIFID")[0].childNodes[0]!=null) $get('<%=txtCifNumber.clientID%>').value = xmlDoc.getElementsByTagName("CIFID")[0].childNodes[0].nodeValue;
        	if(xmlDoc.getElementsByTagName("MST")[0].childNodes[0]!=null) $get('<%=txtMaNNT.clientID%>').value = xmlDoc.getElementsByTagName("MST")[0].childNodes[0].nodeValue;
        	
        	jsFormatNumber('<%=txtTS_TienBL_T24.clientID%>')               	
        	datediff();
        }else {
                
                document.getElementById('divStatus').innerHTML="Mã lỗi: " + rootCTU_HDR.getElementsByTagName("ResponseCode")[0].childNodes[0].nodeValue + ". Mô tả: "+ rootCTU_HDR.getElementsByTagName("Description")[0].childNodes[0].nodeValue;
        }       	
        }            
    }
    /**************************TEN_NNT***********************************/
    function Get_TenNNT() {
        PageMethods.Get_TenNNT($get('<%=txtMaNNT.clientID%>').value, Get_TenNNT_Complete, Get_TenNNT_Error);
    }

    function Get_TenNNT_Complete(result, methodName) {
        if (result.length > 0) {
            $get('<%=txtTenNNT.clientID%>').value = result;
        }
        else {
            $get('<%=txtTenNNT.clientID%>').value = result;
        }
    }
    function Get_TenNNT_Error(error, userContext, methodName) {
        if (error !== null) {
            $get('<%=txtTenNNT.clientID%>').value = result;
        }
    }
    /*************************************************************/
    /**
    End Duc Anh Added
    **/
 function FindDanhMuc(strPage,txtID, txtTitle,txtFocus) 
    {        
        var strSHKB;
         var returnValue;
        if (document.getElementById('<%=hdnSHKB.clientID%>').value.length>0)
        {
            strSHKB=document.getElementById('<%=hdnSHKB.clientID%>').value;
        }
        else
        {
            strSHKB= defSHKB.split(';')[0];
        }
            
         returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&SHKB=" +strSHKB +"&initParam=" + $get(txtID).value,"", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                if (txtTitle!=null){
                    document.getElementById(txtTitle).value = returnValue.Title;                    
                }
                if (txtFocus!=null){
                    document.getElementById(txtFocus).focus();
                }
        }
    }

     function FindNNT_NEW(txtID, txtTitle) 
     {
        var strSHKB;
        if (document.getElementById('<%=hdnSHKB.clientID%>').value.length>0)
        {
            strSHKB=document.getElementById('<%=hdnSHKB.clientID%>').value;
        }
         else
        {
            strSHKB= defSHKB.split(';')[0];
        }
        var returnValue = window.showModalDialog("../../Find_DM/Find_NNT_NEW.aspx?Src=TK&SHKB=" + strSHKB + "&initParam=" + $get('<%=hdnSHKB.clientID%>').value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;
                //document.getElementById(txtFocus).focus();
            }       
    }
    
    function ShowLov(strType)
    {
        if (strType=="NNT") return FindNNT_NEW('<%=txtMaNNT.clientID%>', '<%=txtTenNNT.clientID%>');                 
        if (strType=="SHKB") return FindDanhMuc('KhoBac','txtSHKB', 'txtTenKB','txtSHKB');                 
        if (strType=="DBHC") return FindDanhMuc('DBHC','txtMaDBHC', 'txtTenDBHC','txtMaCQThu');         
        if (strType=="CQT") return FindDanhMuc('CQThu_HaiQuan','<%=txtMaCQT.clientID%>','<%=txtTenCQT.clientID%>',null);         
        if (strType=="LHXNK") return FindDanhMuc('LHXNK','<%=txtLHXNK.clientID%>','<%=txtDescLHXNK.clientID%>', null);
        if (strType == "MHQ") return FindDanhMuc('Ma_HaiQuan', '<%=txtMaHQ.clientID%>', '<%=txtTenHQ.clientID%>', null);
        if (strType == "MHQPH") return FindDanhMuc('Ma_HaiQuan', '<%=txtMaHQPH.clientID%>', '<%=txtTenMaHQPH.clientID%>', null);
        if (strType == "MHQ_KB") return FindDanhMuc('Ma_HaiQuan', '<%=txtMA_HQ_KB.clientID%>', '<%=txtTenMA_HQ_KB.clientID%>', null);
    }
    function ShowMLNS(strType)
    {  
        if (strType=="MQ_01") return FindDanhMuc('CapChuong','<%=txtMaQuy_1.clientID%>', null, null);
        if (strType=="MQ_02") return FindDanhMuc('CapChuong','<%=txtMaQuy_2.clientID%>', null, null);
        if (strType=="MQ_03") return FindDanhMuc('CapChuong','<%=txtMaQuy_3.clientID%>', null, null);
        if (strType=="MQ_04") return FindDanhMuc('CapChuong','<%=txtMaQuy_4.clientID%>', null, null);
        if (strType=="MQ_05") return FindDanhMuc('CapChuong','<%=txtMaQuy_5.clientID%>', null, null);
        
        if (strType=="MC") return FindDanhMuc('MaKhoan','<%=txtCapChuong.clientID%>', null, '');
        if (strType=="MC_01") return FindDanhMuc('MaKhoan','<%=txtMaChuong_1.clientID%>', null, '<%=txtNDKT_1.clientID%>');
        if (strType=="MC_02") return FindDanhMuc('MaKhoan','<%=txtMaChuong_2.clientID%>',null,'<%=txtNDKT_2.clientID%>');
        if (strType=="MC_03") return FindDanhMuc('MaKhoan','<%=txtMaChuong_3.clientID%>',null,'<%=txtNDKT_3.clientID%>');
        if (strType=="MC_04") return FindDanhMuc('MaKhoan','<%=txtMaChuong_4.clientID%>',null,'<%=txtNDKT_4.clientID%>');        
        
        if (strType=="MM_01") return FindDanhMuc('MucTMuc','<%=txtNDKT_1.clientID%>','<%=txtNoiDung_1.clientID%>','<%=txtNoiDung_1.clientID%>');  
        if (strType=="MM_02") return FindDanhMuc('MucTMuc','<%=txtNDKT_2.clientID%>','<%=txtNoiDung_2.clientID%>','<%=txtNoiDung_2.clientID%>'); 
        if (strType=="MM_03") return FindDanhMuc('MucTMuc','<%=txtNDKT_3.clientID%>','<%=txtNoiDung_3.clientID%>','<%=txtNoiDung_3.clientID%>'); 
        if (strType=="MM_04") return FindDanhMuc('MucTMuc','<%=txtNDKT_4.clientID%>','<%=txtNoiDung_4.clientID%>','<%=txtNoiDung_4.clientID%>'); 
    }
    
    function daysFromString(dateString)
    {
        // split strings at / and return array
        var splittedString = dateString.split("/");
        // make a new date. Caveat: Months are 0-based in JS
        var newDate = new Date(parseInt(splittedString[2], 10), parseInt(splittedString[1], 10)-1, parseInt(splittedString[0], 10));
        // returns days since jan 1 1970
        return Math.round(newDate.getTime() / (24*3600*1000));
    }

    function datediff() {      
        ds1 = document.getElementById('<%=txtNgayHL.clientID%>').value
        ds2 = document.getElementById('<%=txtNgayHetHan.clientID%>').value        
        if  (ds1 != '' && ds2 != ''){
          var dateDays1 = daysFromString(ds1);
          var dateDays2 = daysFromString(ds2);
          var diff = dateDays2 - dateDays1; 
          if(dateDays2<dateDays1){
          window.alert('Ngày Hết hạn BL phải lớn hơn Ngày hiệu lực BL');
          document.getElementById('<%=txtTongSoNgay.clientID%>').value = '';
          document.getElementById('<%=txtNgayHetHan.clientID%>').value = '';
          document.getElementById("txtNgayHetHan").focus();
          }
          else{
          document.getElementById('<%=txtTongSoNgay.clientID%>').value = diff;
          }
         }
         else
          document.getElementById('<%=txtTongSoNgay.clientID%>').value = '';      
        }
    
    function jsCalTotal()
    {        
        var dblTongTien = 0;             
        if ($get('<%=chkCheck_1.clientID%>').checked){             
            if ($get('<%=txtTien_1.clientID%>').value.length > 0) {            
                dblTongTien += parseFloat ($get('<%=txtTien_1.clientID%>').value.replaceAll('.',''));    
                jsFormatNumber('<%=txtTien_1.clientID%>');
            }    
        }
        if ($get('<%=chkCheck_2.clientID%>').checked){
            if ($get('<%=txtTien_2.clientID%>').value.length > 0) {
                dblTongTien += parseFloat ($get('<%=txtTien_2.clientID%>').value.replaceAll('.',''));
                jsFormatNumber('<%=txtTien_2.clientID%>');
            }
        }
        if ($get('<%=chkCheck_3.clientID%>').checked){
            if ($get('<%=txtTien_3.clientID%>').value.length > 0){
                dblTongTien += parseFloat ($get('<%=txtTien_3.clientID%>').value.replaceAll('.',''));
                jsFormatNumber('<%=txtTien_3.clientID%>');
            } 
        }
        if ($get('<%=chkCheck_4.clientID%>').checked){                
            if ($get('<%=txtTien_4.clientID%>').value.length > 0) {        
                dblTongTien += parseFloat ($get('<%=txtTien_4.clientID%>').value.replaceAll('.',''));
                jsFormatNumber('<%=txtTien_4.clientID%>');
            }
        }
        if ($get('<%=chkCheck_5.clientID%>').checked){
             if ($get('<%=txtTien_5.clientID%>').value.length > 0) {        
                dblTongTien += parseFloat ($get('<%=txtTien_5.clientID%>').value.replaceAll('.',''));
                jsFormatNumber('<%=txtTien_5.clientID%>');
            }
        }
        if ($get('<%=txtTien_6.clientID%>').value.length > 0) {        
            dblTongTien += parseFloat ($get('<%=txtTien_6.clientID%>').value.replaceAll('.',''));
            jsFormatNumber('<%=txtTien_6.clientID%>');
        }
        if ($get('<%=chkCheck_7.clientID%>').checked){
            if ($get('<%=txtTien_7.clientID%>').value.length > 0) {        
                dblTongTien += parseFloat ($get('<%=txtTien_7.clientID%>').value.replaceAll('.',''));
                jsFormatNumber('<%=txtTien_7.clientID%>');
            }
        }
        
        if ($get('<%=txtTien_1.clientID%>').value.length > 0) {    
            jsFormatNumber('<%=txtTien_1.clientID%>');
        }
        if ($get('<%=txtTien_2.clientID%>').value.length > 0) {   
            jsFormatNumber('<%=txtTien_2.clientID%>');
        }
        if ($get('<%=txtTien_3.clientID%>').value.length > 0) {    
            jsFormatNumber('<%=txtTien_3.clientID%>');
        }
        if ($get('<%=txtTien_4.clientID%>').value.length > 0) {   
            jsFormatNumber('<%=txtTien_4.clientID%>');
        }
        if ($get('<%=txtTien_5.clientID%>').value.length > 0) { 
            jsFormatNumber('<%=txtTien_5.clientID%>');
        }
        if ($get('<%=txtTien_6.clientID%>').value.length > 0) {   
            jsFormatNumber('<%=txtTien_6.clientID%>');
        }
        if ($get('<%=txtTien_7.clientID%>').value.length > 0) {
            jsFormatNumber('<%=txtTien_7.clientID%>');
        } 
        
       $get('<%=txtTongTien.clientID%>').value = dblTongTien;
       jsFormatNumber('<%=txtTongTien.clientID%>');    
       //alert($get('<%=txtTongTien.clientID%>').value);                           
    }    
    
    function valInteger(obj) {
        var i, strVal, blnChange;
        blnChange = false
        strVal = "";

        for (i = 0; i < (obj.value).length; i++) {
            switch (obj.value.charAt(i)) {
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9": strVal = strVal + obj.value.charAt(i);
                    break;
                default: blnChange = true;
                    break;
            }
        }
        if (blnChange) {
            obj.value = strVal;
        }
    }
    function jsGetNDKT(txtNDKT,varStt)
    {
        //alert(txtNDKT)
        if (txtNDKT.value.length > 0)
        {
        var strNDKT= txtNDKT.value;      	
        PageMethods.GetNDKT(strNDKT,varStt,GetNDKT_Complete,GetNDKT_Error);
        }
    }

    function GetNDKT_Complete(result,methodName)
    {   
        if (result.length > 0){        
           var arr2 = result.split(';');
           if( arr2[1] != "-9999"){
               if(arr2[0] == "1"){
                   $get('<%=txtNDKT_1.clientID%>').value=arr2[1];
                   $get('<%=txtNoiDung_1.clientID%>').value=arr2[2]; 
               }else if(arr2[0] == "2"){
                   $get('<%=txtNDKT_2.clientID%>').value=arr2[1];
                   $get('<%=txtNoiDung_2.clientID%>').value=arr2[2]; 
               }else if(arr2[0] == "3"){
                   $get('<%=txtNDKT_3.clientID%>').value=arr2[1];
                   $get('<%=txtNoiDung_3.clientID%>').value=arr2[2]; 
               }else if(arr2[0] == "4"){
                   $get('<%=txtNDKT_4.clientID%>').value=arr2[1];
                   $get('<%=txtNoiDung_4.clientID%>').value=arr2[2]; 
               }else if(arr2[0] == "5"){
                   $get('<%=txtNDKT_5.clientID%>').value=arr2[1];
                   $get('<%=txtNoiDung_5.clientID%>').value=arr2[2]; 
               }else if(arr2[0] == "6"){
                   $get('<%=txtNDKT_6.clientID%>').value=arr2[1];
                   $get('<%=txtNoiDung_6.clientID%>').value=arr2[2]; 
               }else if(arr2[0] == "7"){
                   $get('<%=txtNDKT_7.clientID%>').value=arr2[1];
                   $get('<%=txtNoiDung_7.clientID%>').value=arr2[2]; 
               }
           }else{
                alert(arr2[2]);
                if(arr2[0] == "1"){
                   $get('<%=txtNDKT_1.clientID%>').value='';
                   $get('<%=txtNoiDung_1.clientID%>').value='';
                   $get('<%=txtNDKT_1.clientID%>').focus(); 
               }else if(arr2[0] == "2"){
                   $get('<%=txtNDKT_2.clientID%>').value='';
                   $get('<%=txtNoiDung_2.clientID%>').value=''; 
                   $get('<%=txtNDKT_2.clientID%>').focus(); 
               }else if(arr2[0] == "3"){
                   $get('<%=txtNDKT_3.clientID%>').value='';
                   $get('<%=txtNoiDung_3.clientID%>').value=''; 
                    $get('<%=txtNDKT_3.clientID%>').focus(); 
               }else if(arr2[0] == "4"){
                   $get('<%=txtNDKT_4.clientID%>').value='';
                   $get('<%=txtNoiDung_4.clientID%>').value=''; 
                    $get('<%=txtNDKT_4.clientID%>').focus(); 
               }else if(arr2[0] == "5"){
                   $get('<%=txtNDKT_5.clientID%>').value='';
                   $get('<%=txtNoiDung_5.clientID%>').value=''; 
                   $get('<%=txtNDKT_5.clientID%>').focus(); 
               }else if(arr2[0] == "6"){
                   $get('<%=txtNDKT_6.clientID%>').value='';
                   $get('<%=txtNoiDung_6.clientID%>').value=''; 
                   $get('<%=txtNDKT_6.clientID%>').focus(); 
               }else if(arr2[0] == "7"){
                   $get('<%=txtNDKT_7.clientID%>').value='';
                   $get('<%=txtNoiDung_7.clientID%>').value=''; 
                   $get('<%=txtNDKT_7.clientID%>').focus(); 
               }
           }           
        }   
        else{
            document.getElementById('divStatus').innerHTML='Không tìm thấy thông tin ngân hàng nhận.Hãy kiểm tra lại'; 
        }                     
    }
    function GetNDKT_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
            document.getElementById('errorMessage').innerHTML="Lỗi trong quá trình lấy thông tin ngân hàng nhận";
        }
    }
    
    
      function jsFormatNumber(txtCtl){
        document.getElementById(txtCtl).value = localeNumberFormat(document.getElementById(txtCtl).value.replaceAll('.',''),'.');
    }
    function jsGetXMLDoc(xmlString)
    {
        var xmlDoc;
        //if (xmlString==null||xmlString.length==0) return;
        try //Internet Explorer
        {
           // alert (xmlString);
            xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async="false";
            xmlDoc.loadXML(xmlString);
            return xmlDoc; 
        }
        catch(e)
        {
            parser=new DOMParser();
            xmlDoc=parser.parseFromString(xmlString,"text/xml");
            return xmlDoc;
        }
    }
    function jsNoiDungKT(txtMaMuc,txtNoiDung)
    {           
        var userContext = {};
        userContext.ControlID = txtNoiDung;
        
        var strMaNDKT = document.getElementById(txtMaMuc).value ;                     
        if (strMaNDKT.length > 0)
        {
            PageMethods.Get_TenTieuMuc(strMaNDKT,Get_NoiDungKT_Complete,Get_NoiDungKT_Error,userContext);  
        }        
    }
     function Get_NoiDungKT_Complete(result,userContext,methodName)
    {                             
        if (result.length>0)
        {        
            document.getElementById(userContext.ControlID).value=result;
            document.getElementById('lblError').innerHTML="Tìm thấy nội dung kinh tế phù hợp";
        }else
        {
            document.getElementById('lblError').innerHTML="Không tìm thấy nội dung kinh tế phù hợp.Hãy tìm lại";
        }
    }
    function Get_NoiDungKT_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
            document.getElementById('lblError').innerHTML="Lỗi trong quá trình lấy nội dung kinh tế" + error.get_message();
        }
    }                
    function localeNumberFormat(amount,delimiter) {
	    try {
	   
		    amount = parseFloat(amount);
	    }catch (e) {
		    throw ('localeNumberFormat caused INVALID FLOAT with value ' + amount)
		    return null;
	    }
	    if (delimiter == null || delimiter == undefined) { delimiter = '.'; }
	    
	    // convert to string
	    if (amount.match != 'function') { amount = amount.toString(); }

	    // validate as numeric
	    var regIsNumeric = /[^\d,\.-]+/igm;
	    var results = amount.match(regIsNumeric);

	    if (results != null) {
		    outputText('INVALID NUMBER', eOutput)
		    return null;
	    }

	    var minus = amount.indexOf('-') >= 0 ? '-' : '';
	    amount = amount.replace('-', '');
	    var amtLen = amount.length;
	    var decPoint = amount.indexOf(',');
	    var wholeNumberEnd = decPoint > 0 ? amtLen - (amtLen - decPoint) : amtLen;

	    var wholeNumber = amount.substr(0, wholeNumberEnd);
	    
	    var numberEnd=amount.substr(decPoint,amtLen);
	    var fraction = amount.substr(wholeNumberEnd, amtLen - wholeNumberEnd);

	    var segments = (wholeNumberEnd - (wholeNumberEnd % 3)) / 3;
	    var rvsNumber = wholeNumber.split('').reverse().join('');
	    var output = '';

	    for (i = 0; i < wholeNumberEnd; i++) {
		    if (i % 3 == 0 && i != 0 && i != wholeNumberEnd) { output += delimiter; }
		    output += rvsNumber.charAt(i);
	    }
	    output = minus +  output.split('').reverse().join('') + fraction ;
	    return output;
    }    
       
    String.prototype.replaceAll = function(strTarget,strSubString)
    {
        var strText = this;
        var intIndexOfMatch = strText.indexOf( strTarget );
        while (intIndexOfMatch != -1){
            strText = strText.replace( strTarget, strSubString )
            intIndexOfMatch = strText.indexOf( strTarget );
        }
        return( strText );
    } 
 
    function mask(str,textbox,loc,delim){
        var locs = loc.split(',');
        for (var i = 0; i <= locs.length; i++){
	        for (var k = 0; k <= str.length; k++){
	            if (k == locs[i]){
	            if (str.substring(k, k+1) != delim){
	                    str = str.substring(0,k) + delim + str.substring(k,str.length)
	                }
	            }
	        }
        }
        textbox.value = str
    }
    function jsFormatNumbertoDate(dateString)
    {
    if (dateString.length>0)
    {
        var year        = dateString.substring(0,4);
        var month       = dateString.substring(4,6);
        var day         = dateString.substring(6,8);
        var date        = day +"/" +month +"/"+year  ;
        return date;
    }
    
    }
    function valInteger(obj) {
        var i, strVal, blnChange;
        blnChange = false
        strVal = "";

        for (i = 0; i < (obj.value).length; i++) {
            switch (obj.value.charAt(i)) {
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9": strVal = strVal + obj.value.charAt(i);
                    break;
                default: blnChange = true;
                    break;
            }
        }
        if (blnChange) {
            obj.value = strVal;
        }
        jsFormatNumber('<%=txtTongTien.clientID%>');
    }
    function onInvoke(sender, asHgs)
    {     
       document.getElementById('divProgress').style.display='';
    }

    function onComplete(sender, args)
    {
       document.getElementById('divProgress').style.display='none';
    }
    function pageUnload()
    {
       Sys.Net.WebRequestManager.remove_invokingRequest(onInvoke);
       Sys.Net.WebRequestManager.remove_completedRequest(onComplete);
    }
    function disableButton() {
    document.getElementById('<%=btnGhi.clientID%>').disabled=true;
    document.getElementById('<%=btnGhiMoi.clientID%>').disabled=true;
    document.getElementById('<%=btnHuy.clientID%>').disabled=true;
    document.getElementById('<%=btnThemMoi.clientID%>').disabled=true;
    }
    function InThu()
    {
    var url = "../BaoCao/frmInThuBL.aspx?SoCT=" + $get('<%=txtSoCT.clientID%>').value ;
    window.open (url);
    }
    </script>

    <style type="text/css">
        .style1
        {
            width: 10%;
            height: 22px;
        }
        .style2
        {
            width: 90%;
            height: 22px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnMethod" runat="server" />
            <asp:HiddenField ID="hdnSHKB" runat="server" />
            <asp:HiddenField ID="hdnMaNV" runat="server" />
            <asp:HiddenField ID="hdnSoBT" runat="server" />
            <asp:HiddenField ID="hdnMaHQ" runat="server" />
            <asp:HiddenField ID="hdnTenHQ" runat="server" />
            <asp:HiddenField ID="hdnNgayKB" runat="server" />
            <asp:HiddenField ID="hdnNgayT24" runat="server" />
            <asp:HiddenField ID="hdnTrangThai" runat="server" />
            <asp:HiddenField ID="hdnTrangThaiTT" runat="server" />
            <asp:HiddenField ID="hdnTrangThaiBLT24" runat="server" />
            <asp:HiddenField ID="hdnSO_CT" runat="server" />
            <asp:HiddenField ID="hdnMa_KS" runat="server" />
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 10px;">
                        
                    </td>
                    <td valign="top" align="center">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr valign="top">
                                    <td align="center" valign="top">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td class="pageTitle">
                                                        <span>LẬP BẢO LÃNH HẢI QUAN</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="errorMessage">
                                                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="color: Blue">
                                                        <asp:Label ID="lblSuccess" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100%" align="center" valign="top" style="padding-left: 3; padding-right: 3">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="left" valign="top">
                                                                        <table width="100%" align="center">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td valign="top" width="250px" height="500px">
                                                                                        <div id="Div1" style="height: 50px; width: 250px;">
                                                                                            <table width="100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <label>
                                                                                                            Số CT</label>:
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="txtSoBL_Search" runat="server" Width="150px"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <asp:Button ID="btnSearch" runat="server" Text="Tìm" UseSubmitBehavior="false" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                        <div id="ctl00_MstPg05_MainContent_Panel2" style="height: 600px; width: 290px; clear: both">
                                                                                            <table class="grid_data" cellspacing="0" rules="all" border="1" id="grdDSCT" style="width: 100%;
                                                                                                border-collapse: collapse;">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td colspan="3">
                                                                                                            <div id="divDSCT">
                                                                                                                <asp:DataGrid ID="dtgDSCT" runat="server" AutoGenerateColumns="False" TabIndex="9"
                                                                                                                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True" PageSize="20">
                                                                                                                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                                                                                                                    <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
                                                                                                                    <ItemStyle CssClass="grid_item" />
                                                                                                                    <Columns>
                                                                                                                        <asp:BoundColumn DataField="TRANG_THAI" HeaderText="TT">
                                                                                                                            <HeaderStyle Width="5%" HorizontalAlign="Center"></HeaderStyle>
                                                                                                                            <ItemStyle HorizontalAlign="Center" Width="0"></ItemStyle>
                                                                                                                        </asp:BoundColumn>
                                                                                                                        <asp:TemplateColumn HeaderText="Số CT" HeaderStyle-Width="70px">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SO_CT") %>'
                                                                                                                                    NavigateUrl='<%#"~/pages/BaoLanhHQ/frmLapBaoLanhHQ.aspx?SHKB=" & DataBinder.Eval(Container.DataItem, "SHKB") & "&SO_CT=" & DataBinder.Eval(Container.DataItem, "SO_CT") & _
                                                                                                                        "&SO_BT=" & DataBinder.Eval(Container.DataItem, "SO_BT") & "&TT=" & DataBinder.Eval(Container.DataItem, "TT") & "&NGAY_KB=" & DataBinder.Eval(Container.DataItem, "NGAY_KB") %>'>
                                                                                                                                </asp:HyperLink>
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle></HeaderStyle>
                                                                                                                        </asp:TemplateColumn>
                                                                                                                        <%--<asp:TemplateColumn HeaderText="Số BL" HeaderStyle-Width="100px">
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SO_BL") %>'
                                                                                                                        NavigateUrl='<%#"~/pages/BaoLanhHQ/frmLapBaoLanhHQ.aspx?SHKB=" & DataBinder.Eval(Container.DataItem, "SHKB") & "&SO_CT=" & DataBinder.Eval(Container.DataItem, "SO_CT") & _
                                                                                                                        "&SO_BT=" & DataBinder.Eval(Container.DataItem, "SO_BT") & "&TT=" & DataBinder.Eval(Container.DataItem, "TT") & "&SO_BL=" & DataBinder.Eval(Container.DataItem, "SO_BL") %>'>
                                                                                                                    </asp:HyperLink>
                                                                                                                </ItemTemplate>
                                                                                                                <HeaderStyle Width="45%"></HeaderStyle>
                                                                                                            </asp:TemplateColumn>--%>
                                                                                                                        <asp:TemplateColumn HeaderText="Tên NV/Số BL" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="200px">
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblMaNV" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TEN_NV") %>' />
                                                                                                                                <asp:Label ID="lblDelimiter" runat="server" Text='/' />
                                                                                                                                <asp:Label ID="lblSoBT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SO_BL") %>' />
                                                                                                                            </ItemTemplate>
                                                                                                                            <HeaderStyle Width="220px"></HeaderStyle>
                                                                                                                            <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                                                Font-Underline="False" HorizontalAlign="Left" />
                                                                                                                        </asp:TemplateColumn>
                                                                                                                    </Columns>
                                                                                                                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                                                                                                                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                                                                                                                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                                                                                                                    </PagerStyle>
                                                                                                                </asp:DataGrid>
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td colspan="3" align="right" style="display: none">
                                                                                                        <asp:Button ID="btnRefresh" CssClass="ButtonCommand" runat="server" Text="Refresh" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr class="img">
                                                                                                    <td align="left" style="width: 100%" colspan="5">
                                                                                                        <hr style="width: 50%" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr class="img">
                                                                                                    <td style="width: 100%" colspan="3">
                                                                                                        <table>
                                                                                                            <tr style="display: none">
                                                                                                                <td style="width: 10%">
                                                                                                                    <img src="../../images/icons/ChuaKS.png" />
                                                                                                                </td>
                                                                                                                <td style="width: 90%" colspan="2">
                                                                                                                    Chưa Kiểm soát
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 10%">
                                                                                                                    <img src="../../images/icons/ChuyenKS.png" />
                                                                                                                </td>
                                                                                                                <td style="width: 90%" colspan="2">
                                                                                                                    Chờ Kiểm soát
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="style1">
                                                                                                                    <img src="../../images/icons/ChuyenKsHS.gif" />
                                                                                                                </td>
                                                                                                                <td colspan="2" class="style2">
                                                                                                                    Tu chỉnh chờ Kiểm soát
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 10%">
                                                                                                                    <img src="../../images/icons/DaKS.png" />
                                                                                                                </td>
                                                                                                                <td style="width: 90%" colspan="2">
                                                                                                                    Đã Kiểm soát
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <%-- <tr>
                                                                                                        <td style="width: 10%">
                                                                                                            <img src="../../images/icons/Da_TT.png" />
                                                                                                        </td>
                                                                                                        <td style="width: 90%" colspan="2">
                                                                                                            Đã Thanh Toán
                                                                                                        </td>
                                                                                                    </tr>--%>
                                                                                                            <tr>
                                                                                                                <td style="width: 10%">
                                                                                                                    <img src="../../images/icons/HuyHS.gif" />
                                                                                                                </td>
                                                                                                                <td style="width: 90%" colspan="2">
                                                                                                                    Hủy CT đã KS bởi GDV
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 10%">
                                                                                                                    <img src="../../images/icons/KSLoi.png" />
                                                                                                                </td>
                                                                                                                <td style="width: 90%" colspan="2">
                                                                                                                    Chuyển trả
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 10%">
                                                                                                                    <img src="../../images/icons/Huy.png" />
                                                                                                                </td>
                                                                                                                <td style="width: 90%" colspan="2">
                                                                                                                    Chứng từ bị Hủy bởi GDV
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td style="width: 10%">
                                                                                                                    <img src="../../images/icons/Huy_KS.png" />
                                                                                                                </td>
                                                                                                                <td style="width: 90%" colspan="2">
                                                                                                                    Chứng từ bị Hủy bởi KSV
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr class="img">
                                                                                                    <td align="left" style="width: 100%" colspan="3">
                                                                                                        <hr style="width: 50%" />
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td valign="top" width="630px" align="right">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" border="1" width="630px">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td colspan="2" width="100%" align="left">
                                                                                                        <div id="Panel1" style="height: auto; width: 100%;">
                                                                                                            <table id="grdHeader" cellspacing="0" cellpadding="1" rules="all" border="1" style="border-width: 1px;
                                                                                                                border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse;">
                                                                                                                <tbody>
                                                                                                                    <tr>
                                                                                                                        <td style="width: 30%;">
                                                                                                                            <b>Loại Bảo lãnh </b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 30%" colspan="2">
                                                                                                                            <asp:DropDownList ID="ddlLoaiBL" runat="server" Style="width: 100%" CssClass="inputflat"
                                                                                                                                AutoPostBack="True">
                                                                                                                                <asp:ListItem Value="31">Bảo lãnh tờ khai</asp:ListItem>
                                                                                                                                <asp:ListItem Value="32">Bảo lãnh vận đơn</asp:ListItem>
                                                                                                                                <asp:ListItem Value="33">Bảo lãnh chung</asp:ListItem>
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                        <td style="width: 50%" colspan="3">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowKHCT" runat="server">
                                                                                                                        <td style="width: 25%; padding-left: 10px">
                                                                                                                            <b>KHCT</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%">
                                                                                                                            <asp:TextBox ID="txtKHCT" runat="server" CssClass="inputflat" Style="width: 95%"
                                                                                                                                Enabled="false"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 10%;" align="right">
                                                                                                                            <b>Số CT</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 15%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtSoCT" runat="server" CssClass="inputflat" Enabled="false" Style="width: 95%"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 10%">
                                                                                                                            <b>Ngày lập </b>
                                                                                                                            <asp:TextBox ID="txtNgayLap" runat="server" CssClass="inputflat" Width="70%" Enabled="false"></asp:TextBox>&nbsp;
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowUserMaker" runat="server">
                                                                                                                        <td style="width: 25%; padding-left: 10px">
                                                                                                                            <b>User lập</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%">
                                                                                                                            <asp:TextBox ID="txtNguoiLap" runat="server" CssClass="inputflat" Style="width: 95%"
                                                                                                                                Enabled="false"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 10%; padding-left: 0px" align="right">
                                                                                                                            <b>User KS</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 15%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNguoiKS" runat="server" CssClass="inputflat" Enabled="False"
                                                                                                                                Width="95%"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <%-- <td style="width: 10%;  visibility:hidden " align="right">
                                                                                                                    <b>T.Thái</b>
                                                                                                                </td>--%>
                                                                                                                        <td style="width: 15%; visibility: hidden">
                                                                                                                            <asp:TextBox ID="txtTrangThai" runat="server" CssClass="inputflat" Width="270px"
                                                                                                                                Font-Bold="True" ForeColor="#FF3300"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <%--<td style="width: 10%; " align="right">
                                                                                                                    <b>T.Thái CT</b>
                                                                                                                </td>
                                                                                                                <td >
                                                                                                                    <asp:TextBox ID="txtTrangThaiCT" runat="server" CssClass="inputflat" Width="99%" 
                                                                                                                        Font-Bold="True" ForeColor="#FF3300" Enabled="false"></asp:TextBox>
                                                                                                                </td>--%>
                                                                                                                    </tr>
                                                                                                                    <tr class="grid_header">
                                                                                                                        <td align="left" colspan="6">
                                                                                                                            &nbsp;
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowSoBL" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Số bảo lãnh</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtSoBaoLanh" runat="server" CssClass="inputflat" Style="width: 95%;"
                                                                                                                                MaxLength="16" onblur="Get_SoBL();"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 10%;" align="right">
                                                                                                                            <b>T.Thái CT</b>
                                                                                                                        </td>
                                                                                                                        <td colspan="2">
                                                                                                                            <asp:TextBox ID="txtTrangThaiCT" runat="server" CssClass="inputflat" Width="99%"
                                                                                                                                Font-Bold="True" ForeColor="#FF3300" Enabled="false"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowNgayHL" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày hiệu lực</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="3">
                                                                                                                            <asp:TextBox ID="txtNgayHL" runat="server" Enabled ="false" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnCalendarNgayHL" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" Enabled ="false"/>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" align="right">
                                                                                                                            <b>Ngày HHL</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgayHetHan" runat="server" Enabled ="false" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnCalendarNgayHetHan" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" Enabled ="false"/>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowSoNgayBL" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Tổng số ngày BL</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 75%" colspan="5">
                                                                                                                            <asp:TextBox ID="txtTongSoNgay" onkeyup="valInteger(this)" runat="server" CssClass="inputflat"
                                                                                                                                Style="width: 98%"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowMST" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Mã số thuế</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtMaNNT" runat="server" Enabled ="false" CssClass="inputflat_lookup" Style="width: 95%;
                                                                                                                                background-color: Aqua; font-weight: bold"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowTenNNT" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Tên người NT</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 50%" colspan="5">
                                                                                                                            <asp:TextBox ID="txtTenNNT" runat="server" CssClass="inputflat" Style="width: 85%"></asp:TextBox>
                                                                                                                            &nbsp;
                                                                                                                            <%--<img src="../../images/search.jpeg" width="14" height="14" onclick="ShowLov('NNT')" />--%>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowSOTK" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Tờ khai số</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtToKhaiSo" runat="server" CssClass="inputflat_lookup" Style="width: 95%;
                                                                                                                                background-color: Aqua; font-weight: bold"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 50%" colspan="2">
                                                                                                                        </td>
                                                                                                                        <td style="width: 0%;">
                                                                                                                            <asp:Button ID="btnTruyVanHQ" runat="server" AccessKey="T" Text="[(T)ruy vấn HQ]"
                                                                                                                                UseSubmitBehavior="false" CssClass="ButtonCommand" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowNgayDK" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày đăng ký</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgayDK" runat="server" CssClass="inputflat_lookup" Width="70%"
                                                                                                                                Style="background-color: Aqua; font-weight: bold" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:return mask(this.value,this,'2,5','/');"
                                                                                                                                MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnImageNgayDK" ImageAlign="AbsMiddle" runat="server" ImageUrl="~/images/cal.gif"
                                                                                                                                TabIndex="20" />
                                                                                                                        </td>
                                                                                                                        <td style="width: 50%" colspan="4">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowMA_HQ_KB" runat="server" style="display:none">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Mã HQ KB</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtMA_HQ_KB" runat="server" CssClass="inputflat" Style="width: 95%;
                                                                                                                                font-weight: bold"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 50%" colspan="3">
                                                                                                                            <asp:TextBox ID="txtTenMA_HQ_KB" runat="server" CssClass="inputflat" Style="width: 85%"></asp:TextBox>
                                                                                                                            &nbsp;
                                                                                                                            <img src="../../images/search.jpeg" width="14" height="14" onclick="ShowLov('MHQ')" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowHD" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Số hợp đồng</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtHDon" runat="server" CssClass="inputflat" MaxLength="15" Style="width: 97%;
                                                                                                                                font-weight: bold"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày hđồng</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgay_HDon" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnImageNgay_HDon" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" /> &nbsp;
                                                                                                                            </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowVTD1" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Số VTĐ</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtVTD01" runat="server" CssClass="inputflat" MaxLength="15" Style="width: 97%;
                                                                                                                                font-weight: bold"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày VTĐ</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgayVTD01" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"                                                                                                                               MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnImageNgay_VTD01" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                            <asp:ImageButton ID="btnImageAdd1" ToolTip="Thêm dòng" ImageAlign="AbsMiddle" runat="server" ImageUrl="~/images/add.gif" />    
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowVTD2" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Số VTĐ 02</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtVTD02" runat="server" CssClass="inputflat" MaxLength="15" Style="width: 97%;
                                                                                                                                font-weight: bold"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày VTĐ 02</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgayVTD02" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnImageNgay_VTD02" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                                <asp:ImageButton ID="btnImageAdd2"  ToolTip="Thêm dòng" ImageAlign="AbsMiddle" runat="server" ImageUrl="~/images/add.gif" />    
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowVTD3" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Số VTĐ 03</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtVTD03" runat="server" CssClass="inputflat" MaxLength="15" Style="width: 97%;
                                                                                                                                font-weight: bold"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày VTĐ 03</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgayVTD03" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnImageNgay_VTD03" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                                <asp:ImageButton ID="btnImageAdd3"  ToolTip="Thêm dòng" ImageAlign="AbsMiddle" runat="server" ImageUrl="~/images/add.gif" />    
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowVTD4" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Số VTĐ 04</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtVTD04" runat="server" CssClass="inputflat" MaxLength="15" Style="width: 97%;
                                                                                                                                font-weight: bold"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày VTĐ 04</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgayVTD04" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnImageNgay_VTD04" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                                <asp:ImageButton ID="btnImageAdd4"   ToolTip="Thêm dòng" ImageAlign="AbsMiddle" runat="server" ImageUrl="~/images/add.gif" />    
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowVTD5" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Số VTĐ 05</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtVTD05" runat="server" CssClass="inputflat" MaxLength="15" Style="width: 97%;
                                                                                                                                font-weight: bold"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày VTĐ 05</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgayVTD05" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnImageNgay_VTD05"  ToolTip="Thêm dòng" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowMAHQ" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b><asp:Label ID="lblMa_HQ" Text="Mã HQ" runat="server"></asp:Label></b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtMaHQ" runat="server" CssClass="inputflat" Style="width: 95%;
                                                                                                                                font-weight: bold"  onblur="Get_TenMaHQ();"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 50%" colspan="3">
                                                                                                                            <asp:TextBox ID="txtTenHQ" runat="server" CssClass="inputflat" Style="width: 85%"></asp:TextBox>
                                                                                                                            &nbsp;
                                                                                                                            <img src="../../images/search.jpeg" width="14" height="14" onclick="ShowLov('MHQ')" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowLHXNK" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Loại hình XNK</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtLHXNK" runat="server" CssClass="inputflat" Style="width: 95%;
                                                                                                                                font-weight: bold"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 30%" colspan="3">
                                                                                                                            <asp:TextBox ID="txtDescLHXNK" runat="server" CssClass="inputflat" Style="width: 70%"></asp:TextBox>
                                                                                                                            &nbsp;
                                                                                                                            <img src="../../images/search.jpeg" width="14" height="14" onclick="ShowLov('LHXNK')" />
                                                                                                                        </td>
                                                                                                                        <%--<td style="width: 0%; display:none">
                                                                                                                    <asp:Button ID="btnTruyVanHQ" runat="server" AccessKey="T" Text="[(T)ruy vấn HQ]"  UseSubmitBehavior="false"
                                                                                                                        CssClass="ButtonCommand" />
                                                                                                                </td>--%>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowLoaiTien" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Loại tiền </b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 90%;" colspan="5">
                                                                                                                            <asp:DropDownList ID="drpLoaiTien" runat="server" Style="width: 100%" CssClass="inputflat">
                                                                                                                            </asp:DropDownList>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowCapChuong" runat="server" style="visibility: hidden; display: none">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Mã Chương</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtCapChuong" runat="server" CssClass="inputflat_lookup" Style="width: 95%;
                                                                                                                                background-color: Aqua; font-weight: bold" MaxLength="3" onkeypress="if (event.keyCode==13) ShowMLNS('MC')"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowSOCIF" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Số CIF</b>(<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtCifNumber" runat="server" Enabled ="false" CssClass="inputflat" Style="width: 95%;"
                                                                                                                                MaxLength="16"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 50%" colspan="3">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowAddress" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Địa chỉ</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 75%" colspan="5">
                                                                                                                            <asp:TextBox ID="txtDiaChi" runat="server" CssClass="inputflat" 
                                                                                                                                Style="width: 98%"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowFone_Num" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Số điện thoại</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtFone_Num" runat="server" CssClass="inputflat" onkeyup="valInteger(this)" MaxLength="15" Style="width: 85%"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" >
                                                                                                                            <b>FAX</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txt_FAX_Num" runat="server" CssClass="inputflat" Style="width: 85%"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowSo_DKKD" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Số đăng ký KD</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtSo_DKKD" runat="server" CssClass="inputflat" Style="width: 85%"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày Cấp</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgay_CQCap" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnImageNgay_CQCap" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowCQCap" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Cơ quan cấp</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="5">
                                                                                                                            <asp:TextBox ID="txtCoQCap" runat="server" CssClass="inputflat" Style="width: 85%"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="Tr7" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Mã DV đại diện</b>(<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtMA_DV_DD" runat="server" CssClass="inputflat" Style="width: 95%;"
                                                                                                                                MaxLength="16"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 50%" colspan="3">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="Tr8" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Tên DV đại diện</b>(<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 75%" colspan="5">
                                                                                                                            <asp:TextBox ID="txtTen_DV_DD" runat="server" CssClass="inputflat" Style="width: 98%"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr style="display: none">
                                                                                                                        <td style="width: 25%; display: none">
                                                                                                                            <b>TK thu NS</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%; display: none">
                                                                                                                            <asp:TextBox ID="txtTK" runat="server" CssClass="inputflat" Style="width: 98%" Text=""></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 50%; display: none" colspan="4">
                                                                                                                            <asp:TextBox ID="txtSHKB" runat="server" CssClass="inputflat" Style="width: 98%"
                                                                                                                                Text=""></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowCQTHU" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>CQ Thu</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%">
                                                                                                                            <asp:TextBox ID="txtMaCQT" runat="server" CssClass="inputflat" Style="width: 95%;
                                                                                                                                background-color: Aqua; font-weight: bold"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 50%" colspan="4">
                                                                                                                            <asp:TextBox ID="txtTenCQT" runat="server" CssClass="inputflat" Style="width: 85%"></asp:TextBox>
                                                                                                                            &nbsp;
                                                                                                                            <img src="../../images/search.jpeg" width="14" height="14" onclick="ShowLov('CQT')" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowMAHQPH" runat="server" style="visibility: hidden; display: none">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Mã HQ PH</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%">
                                                                                                                            <asp:TextBox ID="txtMaHQPH" runat="server" CssClass="inputflat" Style="width: 95%;
                                                                                                                                background-color: Aqua; font-weight: bold"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 50%" colspan="4">
                                                                                                                            <asp:TextBox ID="txtTenMaHQPH" runat="server" CssClass="inputflat" Style="width: 85%"></asp:TextBox>
                                                                                                                            &nbsp;
                                                                                                                            <img src="../../images/search.jpeg" width="14" height="14" onclick="ShowLov('MHQPH')" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowDes" runat="server">
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Diễn giải</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 75%" colspan="5">
                                                                                                                            <asp:TextBox ID="txtDienGiai" runat="server" Enabled ="false" MaxLength='200' CssClass="inputflat"
                                                                                                                                Style="width: 99%"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr>
                                                                                                                        <td colspan="6">
                                                                                                                            <div id="divHD_NT" runat="server" style="width: 100%; margin-top: 5px; display: none">
                                                                                                                                <table id="grdHeader_1" cellspacing="0" cellpadding="1" rules="all" border="1" style="border-width: 1px;
                                                                                                                                    margin-top: 5px; border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%;
                                                                                                                                    border-collapse: collapse;">
                                                                                                                                    <tr id="rowHD_NT" runat="server">
                                                                                                                                        <td style="width: 21%;">
                                                                                                                                            <b>HĐ NThương</b> (<span style="color: Red">*</span>)
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                                            <asp:TextBox ID="txtHD_NT" runat="server" CssClass="inputflat" Style="width: 98%;
                                                                                                                                                font-weight: bold" MaxLength="50"></asp:TextBox>
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 15%;">
                                                                                                                                            <b>Ngày HĐ</b> (<span style="color: Red">*</span>)
                                                                                                                                        </td>
                                                                                                                                        <td>
                                                                                                                                            <asp:TextBox ID="txtNgay_HD" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                                MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                                            <asp:Image ID="btnImageNgay_HD" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                    <tr id="rowVTD" runat="server">
                                                                                                                                        <td style="width: 21%;">
                                                                                                                                            <b>Vận tải đơn</b> (<span style="color: Red">*</span>)
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                                            <asp:TextBox ID="txtVTD" runat="server" CssClass="inputflat" Style="width: 98%; font-weight: bold"></asp:TextBox>
                                                                                                                                        </td>
                                                                                                                                        <td style="width: 15%;">
                                                                                                                                            <b>Ngày VTĐ</b> (<span style="color: Red">*</span>)
                                                                                                                                        </td>
                                                                                                                                        <td>
                                                                                                                                            <asp:TextBox ID="txtNgay_VTD" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                                MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                                            <asp:Image ID="btnImageNgay_VTD" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                                        </td>
                                                                                                                                    </tr>
                                                                                                                                </table>
                                                                                                                            </div>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowProgress">
                                                                                    <td colspan="2" align="left">
                                                                                        <div style="background-color: Aqua; display: none" id="divProgress">
                                                                                            <b style="font-weight: bold; font-size: 15pt">Đang lấy dữ liệu tại server.Xin chờ một
                                                                                                lát...</b>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" align="left">
                                                                                        <div id='divStatus' style="font-weight: bold; width: 100%; font-size: large; background: red;
                                                                                            display: none;" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="width: 630px; display: none;">
                                                                                    <td colspan="2" align="right" width="630px">
                                                                                        <table width="100%" border="1">
                                                                                            <tr class="grid_header">
                                                                                                <td align="left" style="width: 92%">
                                                                                                    Thông tin chi tiết (<span style="color: Red">*</span>)
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="display: none">
                                                                                    <td colspan="3" width="630px" align="right">
                                                                                        <table class="grid_data" cellspacing="0" rules="all" border="1" style="border-width: 1px;
                                                                                            border-style: solid; border-color: Black; border-collapse: collapse;" width="100%">
                                                                                            <tbody>
                                                                                                <tr class="grid_header">
                                                                                                    <td align="center">
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 70px;">
                                                                                                        Chương
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 70px; display: none">
                                                                                                        Khoản
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 70px;">
                                                                                                        Tiểu mục
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        Nội dung
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 120px;">
                                                                                                        Số Tiền
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        Kỳ thuế
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        KKHTK
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="rowGridDetail1" class="grid_item">
                                                                                                    <td align="center">
                                                                                                        <asp:CheckBox ID="chkCheck_1" runat="server" Checked="false"></asp:CheckBox>
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <asp:TextBox ID="txtMaQuy_1" runat="server" CssClass="inputflat" MaxLength="3" onkeypress="if (event.keyCode==13) ShowMLNS('MQ_01')"
                                                                                                            Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="display: none">
                                                                                                        <asp:TextBox ID="txtMaChuong_1" runat="server" CssClass="inputflat" MaxLength="3"
                                                                                                            Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"
                                                                                                            onkeypress="if (event.keyCode==13) ShowMLNS('MC_01')"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <asp:TextBox ID="txtNDKT_1" runat="server" CssClass="inputflat" MaxLength="4" Style="width: 90%;
                                                                                                            border-color: White; font-weight: bold; text-align: center;" onkeypress="if (event.keyCode==13) ShowMLNS('MM_01')"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        <asp:TextBox ID="txtNoiDung_1" runat="server" CssClass="inputflat" Style="width: 90%;
                                                                                                            border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <asp:TextBox ID="txtTien_1" runat="server" CssClass="inputflat" onfocus="this.select()"
                                                                                                            onkeyup="valInteger(this)" MaxLength="17" onblur="jsCalTotal();" Style="width: 90%;
                                                                                                            border-color: White; font-weight: bold; text-align: right;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        <asp:TextBox ID="txtKyThue_1" runat="server" CssClass="inputflat" onblur="javascript:CheckMonthYear(this)"
                                                                                                            onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2','/');}"
                                                                                                            Style="width: 0%; width: 0%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        <asp:TextBox ID="txtKHTK_1" runat="server" CssClass="inputflat" Style="width: 0%;
                                                                                                            width: 0%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="Tr1" class="grid_item">
                                                                                                    <td align="center">
                                                                                                        <asp:CheckBox ID="chkCheck_2" runat="server" Checked="false"></asp:CheckBox>
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <asp:TextBox ID="txtMaQuy_2" runat="server" CssClass="inputflat" MaxLength="3" onkeypress="if (event.keyCode==13) ShowMLNS('MQ_02')"
                                                                                                            Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="display: none">
                                                                                                        <asp:TextBox ID="txtMaChuong_2" runat="server" CssClass="inputflat" MaxLength="3"
                                                                                                            onkeypress="if (event.keyCode==13) ShowMLNS('MC_02')" Style="width: 90%; border-color: White;
                                                                                                            font-weight: bold; text-align: center; display: none"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <asp:TextBox ID="txtNDKT_2" runat="server" CssClass="inputflat" MaxLength="4" onkeypress="if (event.keyCode==13) ShowMLNS('MM_02')"
                                                                                                            Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        <asp:TextBox ID="txtNoiDung_2" runat="server" CssClass="inputflat" Style="width: 90%;
                                                                                                            border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <asp:TextBox ID="txtTien_2" runat="server" CssClass="inputflat" onfocus="this.select()"
                                                                                                            onkeyup="valInteger(this)" MaxLength="17" onblur="jsCalTotal();" Style="width: 90%;
                                                                                                            border-color: White; font-weight: bold; text-align: right;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        <asp:TextBox ID="txtKyThue_2" runat="server" CssClass="inputflat" onblur="javascript:CheckMonthYear(this)"
                                                                                                            onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2','/');}"
                                                                                                            Style="width: 0%; width: 0%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        <asp:TextBox ID="txtKHTK_2" runat="server" CssClass="inputflat" Style="width: 0%;
                                                                                                            width: 0%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="Tr2" class="grid_item">
                                                                                                    <td align="center">
                                                                                                        <asp:CheckBox ID="chkCheck_3" runat="server" Checked="false"></asp:CheckBox>
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <asp:TextBox ID="txtMaQuy_3" runat="server" CssClass="inputflat" MaxLength="3" onkeypress="if (event.keyCode==13) ShowMLNS('MQ_03')"
                                                                                                            Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="display: none">
                                                                                                        <asp:TextBox ID="txtMaChuong_3" runat="server" CssClass="inputflat" MaxLength="3"
                                                                                                            onkeypress="if (event.keyCode==13) ShowMLNS('MC_03')" Style="width: 90%; border-color: White;
                                                                                                            font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <asp:TextBox ID="txtNDKT_3" runat="server" CssClass="inputflat" MaxLength="4" onkeypress="if (event.keyCode==13) ShowMLNS('MM_03')"
                                                                                                            Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        <asp:TextBox ID="txtNoiDung_3" runat="server" CssClass="inputflat" Style="width: 90%;
                                                                                                            border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <asp:TextBox ID="txtTien_3" runat="server" CssClass="inputflat" onfocus="this.select()"
                                                                                                            onkeyup="valInteger(this)" MaxLength="17" onblur="jsCalTotal();" Style="width: 90%;
                                                                                                            border-color: White; font-weight: bold; text-align: right;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        <asp:TextBox ID="txtKyThue_3" runat="server" CssClass="inputflat" onblur="javascript:CheckMonthYear(this)"
                                                                                                            onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2','/');}"
                                                                                                            Style="width: 0%; width: 0%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        <asp:TextBox ID="txtKHTK_3" runat="server" CssClass="inputflat" Style="width: 0%;
                                                                                                            width: 0%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="Tr3" class="grid_item">
                                                                                                    <td align="center">
                                                                                                        <asp:CheckBox ID="chkCheck_4" runat="server" Checked="false"></asp:CheckBox>
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <asp:TextBox ID="txtMaQuy_4" runat="server" CssClass="inputflat" MaxLength="3" onkeypress="if (event.keyCode==13) ShowMLNS('MQ_04')"
                                                                                                            Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="display: none">
                                                                                                        <asp:TextBox ID="txtMaChuong_4" runat="server" CssClass="inputflat" MaxLength="3"
                                                                                                            onkeypress="if (event.keyCode==13) ShowMLNS('MC_04')" Style="width: 90%; border-color: White;
                                                                                                            font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <asp:TextBox ID="txtNDKT_4" runat="server" CssClass="inputflat" MaxLength="4" onkeypress="if (event.keyCode==13) ShowMLNS('MM_04')"
                                                                                                            Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        <asp:TextBox ID="txtNoiDung_4" runat="server" CssClass="inputflat" Style="width: 90%;
                                                                                                            border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <asp:TextBox ID="txtTien_4" runat="server" CssClass="inputflat" onfocus="this.select()"
                                                                                                            onkeyup="valInteger(this)" MaxLength="17" onblur="jsCalTotal();" Style="width: 90%;
                                                                                                            border-color: White; font-weight: bold; text-align: right;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        <asp:TextBox ID="txtKyThue_4" runat="server" CssClass="inputflat" onblur="javascript:CheckMonthYear(this)"
                                                                                                            onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2','/');}"
                                                                                                            Style="width: 0%; width: 0%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        <asp:TextBox ID="txtKHTK_4" runat="server" CssClass="inputflat" Style="width: 0%;
                                                                                                            width: 0%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="Tr4" class="grid_item">
                                                                                                    <td align="center">
                                                                                                        <asp:CheckBox ID="chkCheck_5" runat="server" Checked="false"></asp:CheckBox>
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <asp:TextBox ID="txtMaQuy_5" runat="server" CssClass="inputflat" MaxLength="3" onkeypress="if (event.keyCode==13) ShowMLNS('MQ_05')"
                                                                                                            Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="display: none">
                                                                                                        <asp:TextBox ID="txtMaChuong_5" runat="server" CssClass="inputflat" MaxLength="3"
                                                                                                            onkeypress="if (event.keyCode==13) ShowMLNS('MC_05')" Style="width: 90%; border-color: White;
                                                                                                            font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <asp:TextBox ID="txtNDKT_5" runat="server" CssClass="inputflat" MaxLength="4" onkeypress="if (event.keyCode==13) ShowMLNS('MM_05')"
                                                                                                            Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        <asp:TextBox ID="txtNoiDung_5" runat="server" CssClass="inputflat" Style="width: 90%;
                                                                                                            border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <asp:TextBox ID="txtTien_5" runat="server" CssClass="inputflat" onfocus="this.select()"
                                                                                                            onkeyup="valInteger(this)" MaxLength="17" onblur="jsCalTotal();" Style="width: 90%;
                                                                                                            border-color: White; font-weight: bold; text-align: right;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        <asp:TextBox ID="txtKyThue_5" runat="server" CssClass="inputflat" onblur="javascript:CheckMonthYear(this)"
                                                                                                            onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2','/');}"
                                                                                                            Style="width: 0%; width: 0%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        <asp:TextBox ID="txtKHTK_5" runat="server" CssClass="inputflat" Style="width: 0%;
                                                                                                            width: 0%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="Tr5" class="grid_item">
                                                                                                    <td align="center">
                                                                                                        <asp:CheckBox ID="chkCheck_6" runat="server" Checked="false"></asp:CheckBox>
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <asp:TextBox ID="txtMaQuy_6" runat="server" CssClass="inputflat" MaxLength="3" onkeypress="if (event.keyCode==13) ShowMLNS('MQ_05')"
                                                                                                            Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="display: none">
                                                                                                        <asp:TextBox ID="txtMaChuong_6" runat="server" CssClass="inputflat" MaxLength="3"
                                                                                                            onkeypress="if (event.keyCode==13) ShowMLNS('MC_05')" Style="width: 90%; border-color: White;
                                                                                                            font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <asp:TextBox ID="txtNDKT_6" runat="server" CssClass="inputflat" MaxLength="4" onkeypress="if (event.keyCode==13) ShowMLNS('MM_05')"
                                                                                                            Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        <asp:TextBox ID="txtNoiDung_6" runat="server" CssClass="inputflat" Style="width: 90%;
                                                                                                            border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <asp:TextBox ID="txtTien_6" runat="server" CssClass="inputflat" onfocus="this.select()"
                                                                                                            onkeyup="valInteger(this)" MaxLength="17" onblur="jsCalTotal();" Style="width: 90%;
                                                                                                            border-color: White; font-weight: bold; text-align: right;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        <asp:TextBox ID="txtKyThue_6" runat="server" CssClass="inputflat" onblur="javascript:CheckMonthYear(this)"
                                                                                                            onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2','/');}"
                                                                                                            Style="width: 0%; width: 0%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        <asp:TextBox ID="txtKHTK_6" runat="server" CssClass="inputflat" Style="width: 0%;
                                                                                                            width: 0%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="Tr6" class="grid_item">
                                                                                                    <td align="center">
                                                                                                        <asp:CheckBox ID="chkCheck_7" runat="server" Checked="false"></asp:CheckBox>
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <asp:TextBox ID="txtMaQuy_7" runat="server" CssClass="inputflat" MaxLength="3" onkeypress="if (event.keyCode==13) ShowMLNS('MQ_05')"
                                                                                                            Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="display: none">
                                                                                                        <asp:TextBox ID="txtMaChuong_7" runat="server" CssClass="inputflat" MaxLength="3"
                                                                                                            onkeypress="if (event.keyCode==13) ShowMLNS('MC_05')" Style="width: 90%; border-color: White;
                                                                                                            font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center">
                                                                                                        <asp:TextBox ID="txtNDKT_7" runat="server" CssClass="inputflat" MaxLength="4" onkeypress="if (event.keyCode==13) ShowMLNS('MM_05')"
                                                                                                            Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="left">
                                                                                                        <asp:TextBox ID="txtNoiDung_7" runat="server" CssClass="inputflat" Style="width: 90%;
                                                                                                            border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="right">
                                                                                                        <asp:TextBox ID="txtTien_7" runat="server" CssClass="inputflat" onfocus="this.select()"
                                                                                                            onkeyup="valInteger(this)" MaxLength="17" onblur="jsCalTotal();" Style="width: 90%;
                                                                                                            border-color: White; font-weight: bold; text-align: right;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        <asp:TextBox ID="txtKyThue_7" runat="server" CssClass="inputflat" onblur="javascript:CheckMonthYear(this)"
                                                                                                            onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2','/');}"
                                                                                                            Style="width: 0%; width: 0%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="center" style="width: 0px; display: none;">
                                                                                                        <asp:TextBox ID="txtKHTK_7" runat="server" CssClass="inputflat" Style="width: 0%;
                                                                                                            width: 0%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" height="5">
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" valign="top" style="width: 300px">
                                                                                        Chế độ làm việc :
                                                                                        <asp:Label ID="lblStatus" runat="server" Text="" Style="color: blue"></asp:Label>
                                                                                    </td>
                                                                                    <td align="right">
                                                                                        <table width="300px">
                                                                                            <tbody>
                                                                                                <tr align="right">
                                                                                                    
                                                                                                    <td align="right" valign="top">
                                                                                                        Tổng Tiền bảo lãnh: &nbsp;
                                                                                                        <asp:TextBox ID="txtTongTien" runat="server" CssClass="inputflat" Text="0" onblur="jsFormatNumber(this);"
                                                                                                            Style="width: 100px; text-align: right; font-weight: bold" onkeyup="valInteger(this)"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <%--<td align="left" valign="top" style="width: 300px">
                                                                                
                                                                            </td>--%>
                                                                                    <td align="right" colspan="2">
                                                                                        <table width="600px">
                                                                                            <tbody>
                                                                                                <tr align="right">
                                                                                                    
                                                                                                    <td align="right" valign="top">
                                                                                                        Tổng tiền bảo lãnh trong core: &nbsp;
                                                                                                        <asp:TextBox ID="txtTS_TienBL_T24" runat="server" CssClass="inputflat" Text="0" Enabled="false"
                                                                                                            Style="width: 100px; text-align: right; font-weight: bold"></asp:TextBox>
                                                                                                    </td>
                                                                                                    <td align="right" valign="top">
                                                                                                        Tổng tiền truy vấn tờ khai: &nbsp;
                                                                                                        <asp:TextBox ID="txtTT_TTHU" runat="server" CssClass="inputflat" Text="0" Enabled="false"
                                                                                                            Style="width: 100px; text-align: right; font-weight: bold"></asp:TextBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="2" height="5">
                                                                                        Lý do chuyển trả:
                                                                                        <asp:TextBox ID="lblLyDoCTra" Width="500px" ForeColor="Red" Font-Bold="true" ReadOnly="true"
                                                                                            runat="server"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td width="75%" align="right" valign="top">
                                                                        <asp:Button ID="btnThemMoi" runat="server" Text="Lập (M)ới" AccessKey="L" CssClass="ButtonCommand"
                                                                            UseSubmitBehavior="false" />&nbsp;
                                                                        <asp:Button ID="btnGhi" runat="server" Text="Ghi &amp; Chuyển (K)S" AccessKey="K"
                                                                            UseSubmitBehavior="false" CssClass="ButtonCommand" />
                                                                        <span style="display: none">
                                                                            <asp:Button ID="btnGhiMoi" runat="server" Text="[G]hi" AccessKey="G" CssClass="ButtonCommand"
                                                                                UseSubmitBehavior="false" Visible="true" />&nbsp;</span>
                                                                        <asp:Button ID="btnChuyenKS" runat="server" Text="(C)huyển KS" AccessKey="C" CssClass="ButtonCommand"
                                                                            Enabled="false" Visible="False" UseSubmitBehavior="false" />&nbsp;
                                                                        <asp:Button ID="btnHuy" runat="server" Text="(H)ủy" AccessKey="H" CssClass="ButtonCommand"
                                                                            UseSubmitBehavior="false" />&nbsp;
                                                                        <asp:Button ID="btnTuChinh" runat="server" Text="(T)u Chỉnh" AccessKey="T" CssClass="ButtonCommand"
                                                                            UseSubmitBehavior="false" />&nbsp;
                                                                        <asp:Button ID="cmdIn" runat="server" Text="(I)n Thư BL " AccessKey="I" Enabled="false"
                                                                            OnClientClick="InThu()" CssClass="ButtonCommand" Width="105px" UseSubmitBehavior="false" />
                                                                        &nbsp;
                                                                        <asp:Button ID="cmdThanhToan" runat="server" Text="(T)hanh toán" Enabled="false"
                                                                            Visible="false" CssClass="ButtonCommand" UseSubmitBehavior="false" />
                                                                        &nbsp;
                                                                        <asp:Button ID="cmdIn_Thu" runat="server" Text="(I)n Thư BL " AccessKey="I" Enabled="false"
                                                                            Visible="false" UseSubmitBehavior="false" CssClass="ButtonCommand" Width="108px" />
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="height: 5">
                                    <td>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td style="width: 10px;">
                        
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
