﻿Imports System.Data
Imports VBOracleLib
Imports Business
Imports Business.NewChungTu
Imports Business.Common.mdlCommon
Imports CustomsService
Imports Business.BaoCao

Partial Class pages_BaoLanhHQ_frmLapBaoLanhHQ
    Inherits System.Web.UI.Page

    Private mv_objUser As New CTuUser
    Private Ma_CN As String = ""
    Private Shared str_Ngay_LV As String
    Private str_MaNV As String
    Private mblnEdit As Boolean = False
    Private Shared i_PageIndex As Integer = 0
    Private Shared cls_corebank As New CorebankServiceESB.clsCoreBank

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not clsCommon.GetSessionByID(Session.Item("User")) Then
                Response.Redirect("~/pages/Warning.html", False)
                RemoveHandler btnGhi.Click, AddressOf btnGhi_Click
                RemoveHandler btnGhiMoi.Click, AddressOf btnGhiMoi_Click
                RemoveHandler btnHuy.Click, AddressOf btnHuy_Click
                RemoveHandler btnSearch.Click, AddressOf btnSearch_Click
                RemoveHandler btnThemMoi.Click, AddressOf btnThemMoi_Click
                RemoveHandler btnTruyVanHQ.Click, AddressOf btnTruyVanHQ_Click
                RemoveHandler btnTuChinh.Click, AddressOf btnTuChinh_Click
                Exit Sub
            End If
            If Not Session.Item("User") Is Nothing Then
                mv_objUser = New CTuUser(Session.Item("User"))
            Else
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            'tynk-kiểm tra ngày nghỉ
            Dim check As Integer = 0
            If Not Business.CTuCommon.GetNgayNghi() Then
                ' clsCommon.ShowMessageBox(Me, "Hôm nay là ngày nghỉ. Bạn không được vào chức năng này!!!")
                Response.Redirect("~/pages/frmHomeNV.aspx?CheckNN=1", False)
                check = 1
                Exit Sub
            End If
            If check = 1 Then
                Response.Redirect("~/pages/frmHomeNV.aspx?CheckNN=1", False)
                Exit Sub
            End If
            If Not IsPostBack Then
                LoadForm()
                Load_ddl_LoaiBL()
                DM_LTIEN()
            End If
            txtSoBaoLanh.Attributes.Add("onblur", "javascript:Get_SoBL() ;")
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub dtgDSCT_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgDSCT.PageIndexChanged
        'LayDSCT()
        LayDSCT()
        dtgDSCT.CurrentPageIndex = e.NewPageIndex
        dtgDSCT.DataBind()
        i_PageIndex = e.NewPageIndex
    End Sub

    '#Region "ButtonClick"

    Protected Sub btnThemMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnThemMoi.Click
        Try
            'ResetField(False)
            ResetField(True)
            EnableField(True)
            EnableField_DTL(True)
            lblError.Text = ""
            lblSuccess.Text = ""
            txtDienGiai.Text = "Bảo lãnh tiền mặt"
            btnGhi.Enabled = True
            btnGhiMoi.Enabled = True
            btnHuy.Enabled = False
            btnTuChinh.Enabled = False
            cmdIn.Enabled = False
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Protected Sub btnGhiMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGhiMoi.Click
        Try
            Ghi_CTUBL()
        Catch ex As Exception
            lblError.Text = "Lỗi trong quá trình ghi chứng từ!"
        End Try
        Exit Sub
    End Sub

    Protected Sub btnHuy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHuy.Click
        Dim strSHKB As String
        Dim strSoCT As String
        Dim strSoBT As String
        Dim strTT As String
        If Not Request.Params("SHKB") Is Nothing And Not Request.Params("So_CT") Is Nothing And Not Request.Params("So_BT") Is Nothing Then
            strSHKB = Request.Params("SHKB").Trim
            strSoCT = Request.Params("So_CT").Trim
            strSoBT = Request.Params("So_BT").Trim
            strTT = Request.Params("TT").Trim
            Dim objHDR As New infChungTuHDR
            objHDR.So_CT = strSoCT
            objHDR.SHKB = strSHKB
            objHDR.Ngay_KB = hdnNgayKB.Value
            objHDR.Ma_NV = hdnMaNV.Value
            If CheckBeforeTuChinh(objHDR) = False Then
                lblError.Text = "Chứng từ đã được tu chỉnh và gửi lên kiểm soát viên. Bạn không thể hủy chứng từ này"
                cmdThanhToan.Enabled = False
                Exit Sub
            End If
        End If
        Try
            CancelBaoLanh()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    'Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
    '    Try
    '        'LayDSCT()
    '        If Not mv_blFlag Then
    '            LayDSCT()
    '        Else
    '            dtgDSCT.DataSource = mv_dtTable
    '            dtgDSCT.DataBind()
    '            txtSoBL_Search.Text = mv_txtSearch
    '        End If
    '    Catch ex As Exception
    '        lblError.Text = ex.Message
    '    End Try
    'End Sub

    Protected Sub btnTruyVanHQ_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTruyVanHQ.Click
        Try
            TruyVanHQ()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    '#End Region

    '#Region "Function"

    Sub LoadForm()
        Dim v_strSHKB As String = String.Empty
        Dim v_strMaDiemThu As String = String.Empty
        Dim strNgay_LV As String = ""

        btnHuy.Enabled = False
        btnTuChinh.Enabled = False

        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not Session.Item("User") Is Nothing Then
            v_strSHKB = clsCTU.TCS_GetKHKB(CType(Session("User"), Integer))
            v_strMaDiemThu = clsCTU.TCS_GetMaDT(CType(Session("User"), Integer))
            mv_objUser = New CTuUser(Session.Item("User"))
            Ma_CN = mv_objUser.MA_CN
            str_Ngay_LV = mv_objUser.Ngay_LV
        End If

        If Not IsPostBack Then
            Try

            
                hdnSHKB.Value = v_strSHKB
                hdnMaNV.Value = mv_objUser.Ma_NV
                txtDienGiai.Text = "Bảo lãnh tiền mặt"


                ResetField()
                'txtNgayHL.Text = DateTime.Now.ToString("dd/MM/yyyy")
                'txtNgayHetHan.Text = DateTime.Now.ToString("dd/MM/yyyy")
                'txtNgayDK.Text = DateTime.Now.ToString("dd/MM/yyyy")
                'txtNgay_HD.Text = DateTime.Now.ToString("dd/MM/yyyy")
                'txtNgay_HDon.Text = DateTime.Now.ToString("dd/MM/yyyy")
                'txtNgay_VTD.Text = DateTime.Now.ToString("dd/MM/yyyy")
                clsCommon.addPopUpCalendarToImage(btnCalendarNgayHL, txtNgayHL, "dd/mm/yyyy")
                clsCommon.addPopUpCalendarToImage(btnCalendarNgayHetHan, txtNgayHetHan, "dd/mm/yyyy")
                clsCommon.addPopUpCalendarToImage(btnImageNgayDK, txtNgayDK, "dd/mm/yyyy")
                clsCommon.addPopUpCalendarToImage(btnImageNgay_HD, txtNgay_HD, "dd/mm/yyyy")
                clsCommon.addPopUpCalendarToImage(btnImageNgay_HDon, txtNgay_HDon, "dd/mm/yyyy")
                clsCommon.addPopUpCalendarToImage(btnImageNgay_VTD01, txtNgayVTD01, "dd/mm/yyyy")
                clsCommon.addPopUpCalendarToImage(btnImageNgay_VTD02, txtNgayVTD02, "dd/mm/yyyy")
                clsCommon.addPopUpCalendarToImage(btnImageNgay_VTD03, txtNgayVTD03, "dd/mm/yyyy")
                clsCommon.addPopUpCalendarToImage(btnImageNgay_VTD04, txtNgayVTD04, "dd/mm/yyyy")
                clsCommon.addPopUpCalendarToImage(btnImageNgay_VTD05, txtNgayVTD05, "dd/mm/yyyy")
                clsCommon.addPopUpCalendarToImage(btnImageNgay_CQCap, txtNgay_CQCap, "dd/mm/yyyy")
                LayDSCT()

                ' View chi tiet chung tu
                If Not Request.Params("SHKB") Is Nothing And Not Request.Params("So_CT") Is Nothing And Not Request.Params("So_BT") Is Nothing Then
                    Dim strSHKB As String = Request.Params("SHKB").Trim
                    Dim strSoCT As String = Request.Params("So_CT").Trim
                    Dim strSoBT As String = Request.Params("So_BT").Trim
                    Dim strTT As String = Request.Params("TT").Trim
                    Dim strNgayKB As String = Request.Params("NGAY_KB").Trim
                    Load_Detail(strSHKB, strSoCT, strSoBT, hdnMaNV.Value, strNgayKB)

                End If

                txtNDKT_1.Attributes("onBlur") = "jsGetNDKT(" & txtNDKT_1.ClientID & ",1)"
                txtNDKT_2.Attributes("onBlur") = "jsGetNDKT(" & txtNDKT_2.ClientID & ",2)"
                txtNDKT_3.Attributes("onBlur") = "jsGetNDKT(" & txtNDKT_3.ClientID & ",3)"
                txtNDKT_4.Attributes("onBlur") = "jsGetNDKT(" & txtNDKT_4.ClientID & ",4)"
                txtNDKT_5.Attributes("onBlur") = "jsGetNDKT(" & txtNDKT_5.ClientID & ",5)"
                txtNDKT_6.Attributes("onBlur") = "jsGetNDKT(" & txtNDKT_6.ClientID & ",6)"
                txtNDKT_7.Attributes("onBlur") = "jsGetNDKT(" & txtNDKT_7.ClientID & ",7)"
                'txtAccountNumber.Attributes("onBlur") = "IsAccNumberValid(" & txtAccountNumber.ClientID & ")"

            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
                Throw ex
            End Try
        End If
    End Sub
    Private Sub EnableButton(ByVal strTT As String, Optional ByVal strMa_KS As Boolean = False)
        If strTT = "00" Then
            hdnMethod.Value = "Edit"
            lblStatus.Text = "Sửa thông tin bảo lãnh"
            btnThemMoi.Enabled = True
            btnGhi.Enabled = True
            btnHuy.Enabled = True
            btnTuChinh.Enabled = False
            cmdIn.Enabled = False
            cmdIn_Thu.Enabled = False
            btnChuyenKS.Enabled = True
            'cmdThanhToan.Enabled = False
            EnableField(True)
            EnableField_DTL(True)
        ElseIf strTT = "03" Then
            hdnMethod.Value = "ChuyenKS"
            lblStatus.Text = "Bảo lãnh bị chuyển trả"
            btnThemMoi.Enabled = True
            btnGhiMoi.Enabled = False
            btnHuy.Enabled = True
            btnGhi.Enabled = True
            btnTuChinh.Enabled = False
            cmdIn.Enabled = False
            cmdIn_Thu.Enabled = False
            btnChuyenKS.Enabled = False
            'cmdThanhToan.Enabled = False
            EnableField(True)
            EnableField_DTL(True)
        ElseIf strTT = "01" Then

            btnTuChinh.Enabled = True
            'cmdThanhToan.Enabled = True
            cmdIn.Enabled = True
            ' btnTuChinh.Enabled = True
            btnThemMoi.Enabled = True
            btnGhi.Enabled = False
            btnGhiMoi.Enabled = False
            btnHuy.Enabled = True
            cmdIn_Thu.Enabled = False
            btnChuyenKS.Enabled = False

            hdnMethod.Value = "View"
            lblStatus.Text = "Xem thông tin bảo lãnh"
            EnableField(False)
            EnableField_DTL(False)
        ElseIf strTT = "05" Or strTT = "07" Then
            hdnMethod.Value = "View"
            lblStatus.Text = "Xem thông tin bảo lãnh"
            btnThemMoi.Enabled = True
            btnGhi.Enabled = False
            btnChuyenKS.Enabled = False
            btnGhiMoi.Enabled = False
            btnHuy.Enabled = False
            btnTuChinh.Enabled = False
            cmdIn.Enabled = False
            ' cmdThanhToan.Enabled = False
            cmdIn_Thu.Enabled = False
            EnableField(False)
            EnableField_DTL(False)
        ElseIf strTT = "04" Then
            hdnMethod.Value = "View"
            lblStatus.Text = "Xem thông tin bảo lãnh"
            btnThemMoi.Enabled = True
            btnGhi.Enabled = False
            btnChuyenKS.Enabled = False
            btnGhiMoi.Enabled = False
            btnHuy.Enabled = False
            btnTuChinh.Enabled = False
            'cmdThanhToan.Enabled = False
            If strMa_KS = True Then
                cmdIn.Enabled = True
            Else
                cmdIn.Enabled = False
            End If

            cmdIn_Thu.Enabled = False
            EnableField(False)
            EnableField_DTL(False)
        ElseIf strTT = "02" Then
            hdnMethod.Value = "View"
            lblStatus.Text = "Xem thông tin bảo lãnh"
            btnThemMoi.Enabled = True
            btnGhi.Enabled = False
            btnChuyenKS.Enabled = False
            btnGhiMoi.Enabled = False
            btnHuy.Enabled = False
            btnTuChinh.Enabled = False
            cmdIn.Enabled = False
            ' cmdThanhToan.Enabled = False
            cmdIn_Thu.Enabled = False
            EnableField(False)
            EnableField_DTL(False)
        Else
            hdnMethod.Value = "View"
            lblStatus.Text = "Xem thông tin bảo lãnh"
            btnThemMoi.Enabled = True
            btnGhi.Enabled = False
            btnChuyenKS.Enabled = False
            btnGhiMoi.Enabled = False
            btnHuy.Enabled = False
            btnTuChinh.Enabled = False
            cmdIn.Enabled = False
            cmdIn_Thu.Enabled = False
            ' cmdThanhToan.Enabled = False
            EnableField(False)
            EnableField_DTL(False)
            EnableButtonAdd(False)
        End If
    End Sub
    Sub ResetField(Optional ByVal p_bolFlag As Boolean = True)
        mv_objUser = New CTuUser(Session.Item("User"))
        hdnMaNV.Value = mv_objUser.Ma_NV
        Dim strNgayLV As String = clsCTU.TCS_GetNgayLV(hdnMaNV.Value)
        txtNgayLap.Text = ConvertNumberToDate(strNgayLV).ToString("dd/MM/yyyy")
        'mblnEdit = False
        'str_nam_kb = Mid(strNgayLV, 3, 2)
        'str_thang_kb = Mid(strNgayLV, 5, 2)
        hdnSoBT.Value = ""
        hdnMethod.Value = "Add"
        lblStatus.Text = "Lập mới bảo lãnh"
        txtKHCT.Text = clsCTU.TCS_GetKHCT(hdnSHKB.Value, hdnMaNV.Value, "05")
        'txtKHCT.Text = clsCTU.TCS_GetKHCT(hdnSHKB.Value)
        'If p_bolFlag Then
        txtSoCT.Text = ""
        'End If
        txtNguoiLap.Text = mv_objUser.Ten_DN
        txtNguoiKS.Text = ""
        txtSoBaoLanh.Text = ""
        'rdbKieuBL.SelectedValue = "0"
        txtNgayHL.Text = ""
        txtNgayHetHan.Text = ""
        txtTongSoNgay.Text = ""
        txtMaNNT.Text = ""
        txtTenNNT.Text = ""
        txtDiaChi.Text = ""
        txtTK.Text = ""
        txtMaCQT.Text = ""
        txtTenCQT.Text = ""
        txtMaHQ.Text = ""
        txtTenHQ.Text = ""
        txtMaHQPH.Text = ""
        txtTenMaHQPH.Text = ""
        txtToKhaiSo.Text = ""
        txtNgayDK.Text = "" 'Now.ToString("dd/MM/yyyy")
        txtLHXNK.Text = ""
        txtDescLHXNK.Text = ""
        txtDienGiai.Text = "Bảo lãnh tiền mặt"
        txtTrangThai.Text = ""
        txtTrangThaiCT.Text = ""
        lblLyDoCTra.Text = ""
        txtCifNumber.Text = ""
        txtHD_NT.Text = ""
        txtNgay_HD.Text = ""
        txtHDon.Text = ""
        txtNgay_HDon.Text = ""
        txtVTD.Text = ""
        txtNgay_VTD.Text = ""
        hdnTrangThai.Value = ""
        txtCapChuong.Text = ""
        txtMA_DV_DD.Text = ""
        txtTen_DV_DD.Text = ""
        txtTS_TienBL_T24.Text = ""
        ClearGridDetail()

    End Sub


    Sub ClearGridDetail()
        txtTongTien.Text = ""

        chkCheck_1.Checked = False
        chkCheck_2.Checked = False
        chkCheck_3.Checked = False
        chkCheck_4.Checked = False
        chkCheck_5.Checked = False

        txtMaQuy_1.Text = ""
        txtMaQuy_2.Text = ""
        txtMaQuy_3.Text = ""
        txtMaQuy_4.Text = ""
        txtMaQuy_5.Text = ""
        txtMaQuy_6.Text = ""
        txtMaQuy_7.Text = ""

        txtMaChuong_1.Text = ""
        txtMaChuong_2.Text = ""
        txtMaChuong_3.Text = ""
        txtMaChuong_4.Text = ""
        txtMaChuong_5.Text = ""
        txtMaChuong_6.Text = ""
        txtMaChuong_7.Text = ""

        txtNDKT_1.Text = ""
        txtNDKT_2.Text = ""
        txtNDKT_3.Text = ""
        txtNDKT_4.Text = ""
        txtNDKT_5.Text = ""
        txtNDKT_6.Text = ""
        txtNDKT_7.Text = ""

        txtNoiDung_1.Text = ""
        txtNoiDung_2.Text = ""
        txtNoiDung_3.Text = ""
        txtNoiDung_4.Text = ""
        txtNoiDung_5.Text = ""
        txtNoiDung_6.Text = ""
        txtNoiDung_7.Text = ""

        txtTien_1.Text = ""
        txtTien_2.Text = ""
        txtTien_3.Text = ""
        txtTien_4.Text = ""
        txtTien_5.Text = ""
        txtTien_6.Text = ""
        txtTien_7.Text = ""

        txtKyThue_1.Text = ""
        txtKyThue_2.Text = ""
        txtKyThue_3.Text = ""
        txtKyThue_4.Text = ""
        txtKyThue_5.Text = ""
        txtKyThue_6.Text = ""
        txtKyThue_7.Text = ""

        txtKHTK_1.Text = ""
        txtKHTK_2.Text = ""
        txtKHTK_3.Text = ""
        txtKHTK_4.Text = ""
        txtKHTK_5.Text = ""
        txtKHTK_6.Text = ""
        txtKHTK_7.Text = ""
    End Sub

    Sub LayDSCT()
        Dim soCT As String = ""
        Dim soBL As String = ""
        If Not Request.Params("SoCT") Is Nothing Then
            soCT = Request.Params("SoCT").ToString
        End If

        If Not txtSoBL_Search.Text = "" Then
            soCT = txtSoBL_Search.Text.ToString()
        End If
        Dim dtCT As DataTable
        'Lay danh sach so chung tu
        dtCT = dtlCTU_Load(hdnMaNV.Value, str_Ngay_LV, "", soCT, soBL)
        If dtCT.Rows.Count > 0 Then
            dtgDSCT.DataSource = dtCT
            dtgDSCT.CurrentPageIndex = i_PageIndex
            dtgDSCT.DataBind()
            i_PageIndex = 0
        Else
            dtgDSCT.DataSource = Nothing
            dtgDSCT.DataBind()
            i_PageIndex = 0
        End If
    End Sub

    Sub LayDSCT_Search()
        Dim soCT As String = ""
        Dim soBL As String = ""
        'If Not Request.Params("SoCT") Is Nothing Then
        '    soCT = Request.Params("SoCT").ToString.Trim
        'End If

        If Not txtSoBL_Search.Text = "" Then
            soCT = txtSoBL_Search.Text.ToString()
            'ElseIf Not Request.Params("So_BL") Is Nothing Then
            '    soBL = Request.Params("So_BL").ToString
        End If



        Dim dtCT As DataTable
        If Not txtSoBL_Search.Text = "" Then
            'Lay danh sach so chung tu
            dtCT = dtlCTU_Load_Search(hdnMaNV.Value, str_Ngay_LV, "", soCT, soBL)
        End If
        If txtSoBL_Search.Text = "" Then
            'Lay danh sach so chung tu
            dtCT = dtlCTU_Load(hdnMaNV.Value, str_Ngay_LV, "", soCT, soBL)
        End If

        If dtCT.Rows.Count > 0 Then
            dtgDSCT.DataSource = dtCT
            dtgDSCT.DataBind()
            'mv_blFlag = True
            'mv_txtSearch = txtSoBL_Search.Text
            'mv_dtTable = dtCT
        Else
            dtgDSCT.DataSource = Nothing
            dtgDSCT.DataBind()
            'mv_blFlag = False
            'mv_txtSearch = ""
            'mv_dtTable = Nothing
        End If
    End Sub

    '    ' Lay danh sach chung tu chua kiem soat, kiem soat loi, huy
    Private Shared Function dtlCTU_Load(ByVal strUser As String, ByVal lNgayKB As Long, ByVal pv_strTrangThai As String, ByVal SoCT As String, Optional ByVal SoBL As String = "") As DataTable
        Dim dt As New DataTable
        Dim strSQL As String
        Dim strWhereSTT As String = ""
        Try
            If SoCT <> "" Then
                strWhereSTT = strWhereSTT & " AND So_CT ='" & SoCT & "'"
            End If

            'If SoBL <> "" Then
            '    strWhereSTT = strWhereSTT & " AND So_CT like '%" & SoBL & "%'"
            'End If

            strSQL = " SELECT " & _
                     " SHKB, KYHIEU_CT, SO_CT, SO_BT, Ma_NV, SO_BL, SO_BT || '/' ||Ma_NV Ten, TRANG_THAI TT, MA_DTHU, TTien, NGAY_KB, MA_XA,TEN_NV, " & _
                     " Case TRANG_THAI " & _
                     " when '00' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("00") & " alt=" & Business.CTuCommon.Get_TrangThai("00") & "/>' " & _
                     " when '01' then case TRANG_THAI_TT when 0 then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("01", "1") & " alt=" & Business.CTuCommon.Get_TrangThai("01") & "/>' when 1 then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("01", "1", "0", "1") & " alt=" & Business.CTuCommon.Get_TrangThai("07") & "/>' END  " & _
                     " when '03' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("03") & " alt=" & Business.CTuCommon.Get_TrangThai("03") & "/>' " & _
                     " when '04' then case ma_ks when '0' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("04") & " alt=" & Business.CTuCommon.Get_TrangThai("04") & "/>' else '<img src=" & Business.CTuCommon.Get_ImgTrangThai("04", "0", "1") & " alt=" & Business.CTuCommon.Get_TrangThai("04") & "/>' end " & _
                     " when '05' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("05") & " alt=" & Business.CTuCommon.Get_TrangThai("05") & "/>' " & _
                     " when '02' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("02") & " alt=" & Business.CTuCommon.Get_TrangThai("02") & "/>' " & _
                     " when '07' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("07") & " alt=" & Business.CTuCommon.Get_TrangThai("07") & "/>' " & _
                     " End TRANG_THAI" & _
                     " FROM tcs_baolanh_hdr  " & _
                     " WHERE NGAY_KB = " & lNgayKB & strWhereSTT & _
                     " AND Ma_NV = " & CInt(strUser) & _
                     " ORDER BY so_bt DESC "
            dt = DatabaseHelp.ExecuteToTable(strSQL)
            Return dt
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    ' Lay danh sach chung tu chua kiem soat, kiem soat loi, huy
    Private Shared Function dtlCTU_Load_Search(ByVal strUser As String, ByVal lNgayKB As Long, ByVal pv_strTrangThai As String, ByVal SoCT As String, Optional ByVal SoBL As String = "") As DataTable
        Dim dt As New DataTable
        Dim strSQL As String
        Dim strWhereSTT As String = ""
        Try
            If SoCT <> "" Then
                strWhereSTT = strWhereSTT & " AND (So_CT like '%" & SoCT & "%' or SO_BL like '%" & SoCT & "%')"
            End If

            'If SoBL <> "" Then
            '    strWhereSTT = strWhereSTT & " AND So_BL like '%" & SoBL & "%'"
            'End If

            strSQL = " SELECT " & _
                     " SHKB, KYHIEU_CT, SO_CT, SO_BT, Ma_NV, SO_BL, SO_BT || '/' ||Ma_NV Ten, TRANG_THAI TT, MA_DTHU, TTien, NGAY_KB, MA_XA,TEN_NV, " & _
                     " Case TRANG_THAI " & _
                     " when '00' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("00") & " alt=" & Business.CTuCommon.Get_TrangThai("00") & "/>' " & _
                     " when '01' then case TRANG_THAI_TT when 0 then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("01", "1") & " alt=" & Business.CTuCommon.Get_TrangThai("01") & "/>' when 1 then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("01", "1", "0", "1") & " alt=" & Business.CTuCommon.Get_TrangThai("07") & "/>' END  " & _
                     " when '03' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("03") & " alt=" & Business.CTuCommon.Get_TrangThai("03") & "/>' " & _
                     " when '04' then case ma_ks when '0' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("04") & " alt=" & Business.CTuCommon.Get_TrangThai("04") & "/>' else '<img src=" & Business.CTuCommon.Get_ImgTrangThai("04", "0", "1") & " alt=" & Business.CTuCommon.Get_TrangThai("04") & "/>' end " & _
                     " when '05' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("05") & " alt=" & Business.CTuCommon.Get_TrangThai("05") & "/>' " & _
                     " when '07' then '<img src=" & Business.CTuCommon.Get_ImgTrangThai("07") & " alt=" & Business.CTuCommon.Get_TrangThai("07") & "/>' " & _
                     " End TRANG_THAI" & _
                     " FROM tcs_baolanh_hdr  " & _
                     " WHERE Ma_NV = " & CInt(strUser) & strWhereSTT & _
                     " ORDER BY so_bt DESC "
            dt = DatabaseHelp.ExecuteToTable(strSQL)
            Return dt
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Sub Load_Detail(ByVal strSHKB As String, ByVal strSoCT As String, ByVal strSoBT As String, ByVal strMaNV As String, ByVal strNgayKB As String)
        Dim hdr As New DataTable
        Dim dtls As New DataTable

        clsCTU.TCS_GetBaoLanhDetail(strSHKB, strSoCT, strSoBT, strMaNV, hdr, dtls, strNgayKB)

        ' Dua du lieu vao form
        If hdr.Rows.Count > 0 Then
            txtKHCT.Text = hdr.Rows(0)("kyhieu_ct").ToString()
            txtSoCT.Text = hdr.Rows(0)("so_ct").ToString()
            hdnSO_CT.Value = hdr.Rows(0)("so_ct").ToString()
            hdnSoBT.Value = hdr.Rows(0)("so_bt").ToString()
            txtNgayLap.Text = ConvertNumberToDate(hdr.Rows(0)("ngay_ct").ToString()).ToString("dd/MM/yyyy")
            txtNguoiLap.Text = hdr.Rows(0)("ten_nv").ToString()
            txtNguoiKS.Text = hdr.Rows(0)("ten_ks").ToString()
            hdnMa_KS.Value = hdr.Rows(0)("ma_ks").ToString()
            txtSoBaoLanh.Text = hdr.Rows(0)("so_bl").ToString()
            'rdbKieuBL.SelectedValue = hdr.Rows(0)("kieu_bl").ToString()
            txtNgayHL.Text = hdr.Rows(0)("ngay_batdau").ToString()
            txtNgayHetHan.Text = hdr.Rows(0)("ngay_ketthuc").ToString()
            txtTongSoNgay.Text = hdr.Rows(0)("songay_bl").ToString()
            txtMaNNT.Text = hdr.Rows(0)("ma_nnthue").ToString()
            txtTenNNT.Text = hdr.Rows(0)("ten_nnthue").ToString()
            txtDiaChi.Text = hdr.Rows(0)("dc_nnthue").ToString()
            txtTK.Text = hdr.Rows(0)("tk_no").ToString()
            txtMaCQT.Text = hdr.Rows(0)("ma_cqthu").ToString()
            txtTenCQT.Text = hdr.Rows(0)("ten_cqthu").ToString()
            txtToKhaiSo.Text = hdr.Rows(0)("so_tk").ToString()
            txtSHKB.Text = hdr.Rows(0)("shkb").ToString()
            txtTK.Text = hdr.Rows(0)("tk_co").ToString()

            txtNgayDK.Text = hdr.Rows(0)("ngay_tk").ToString()
            txtLHXNK.Text = hdr.Rows(0)("lhxnk").ToString()
            txtDescLHXNK.Text = hdr.Rows(0)("ten_lhxnk").ToString()
            drpLoaiTien.SelectedValue = hdr.Rows(0)("ma_loaitien").ToString()
            txtDienGiai.Text = hdr.Rows(0)("dien_giai").ToString()
            txtTrangThaiCT.Text = Business.CTuCommon.Get_TenTrangThai(hdr.Rows(0)("trang_thai"))
            hdnTrangThai.Value = hdr.Rows(0)("trang_thai").ToString
            hdnTrangThaiTT.Value = hdr.Rows(0)("TRANG_THAI_TT").ToString
            txtCifNumber.Text = hdr.Rows(0)("cif").ToString()
            hdnTrangThaiBLT24.Value = hdr.Rows(0)("TRANG_THAI_BL").ToString()
            txtTS_TienBL_T24.Text = String.Format("{0:0,0}", CType(hdr.Rows(0)("ttien_nt").ToString(), Double)).Replace(",", ".")
            txtTongTien.Text = String.Format("{0:0,0}", CType(hdr.Rows(0)("ttien").ToString(), Double)).Replace(",", ".")
            txtTT_TTHU.Text = String.Format("{0:0,0}", CType(hdr.Rows(0)("ttien_tthu").ToString(), Double)).Replace(",", ".")
            If hdr.Rows(0)("TRANG_THAI_TT").ToString <> "1" Then
                txtTrangThai.Text = "Chưa thanh toán"
            Else
                txtTrangThai.Text = "Đã thanh toán"
            End If
            '***************************************************************'
            'Author: Anhld
            'Goals: Them ma hai quan, ten hai quan, ma hai quan phat hanh, ten hai quan phat hanh
            'Date: 20/09/2012
            txtMaHQ.Text = hdr.Rows(0)("ma_hq").ToString()
            txtTenHQ.Text = hdr.Rows(0)("ten_hq").ToString()
            txtMaHQPH.Text = hdr.Rows(0)("ma_hq_ph").ToString()
            txtTenMaHQPH.Text = hdr.Rows(0)("ten_hq_ph").ToString()
            lblLyDoCTra.Text = hdr.Rows(0)("ly_do_ctra").ToString()
            ' them hop dong ngoai thương
            txtHD_NT.Text = hdr.Rows(0)("hd_nt").ToString()
            txtNgay_HD.Text = hdr.Rows(0)("ngay_hd").ToString()
            txtHDon.Text = hdr.Rows(0)("hoa_don").ToString()
            txtNgay_HDon.Text = hdr.Rows(0)("ngay_hdon").ToString()
            txtVTD01.Text = hdr.Rows(0)("vt_don").ToString()
            txtNgayVTD01.Text = hdr.Rows(0)("ngay_vtd").ToString()

            txtVTD02.Text = hdr.Rows(0)("vt_don2").ToString()
            txtNgayVTD02.Text = hdr.Rows(0)("ngay_vtd2").ToString()
            txtVTD03.Text = hdr.Rows(0)("vt_don3").ToString()
            txtNgayVTD03.Text = hdr.Rows(0)("ngay_vtd3").ToString()
            txtVTD04.Text = hdr.Rows(0)("vt_don4").ToString()
            txtNgayVTD04.Text = hdr.Rows(0)("ngay_vtd4").ToString()
            txtVTD05.Text = hdr.Rows(0)("vt_don5").ToString()
            txtNgayVTD05.Text = hdr.Rows(0)("ngay_vtd5").ToString()
            txtMA_HQ_KB.Text = hdr.Rows(0)("ma_hq").ToString()
            txtTenMA_HQ_KB.Text = hdr.Rows(0)("ten_hq").ToString()
            If txtToKhaiSo.Text.Trim = "99999" Then
                divHD_NT.Attributes("style") = "display:block;"
            End If
            txtFone_Num.Text = hdr.Rows(0)("fone_num").ToString()
            txt_FAX_Num.Text = hdr.Rows(0)("fax_num").ToString()
            txtSo_DKKD.Text = hdr.Rows(0)("so_dkkd").ToString()
            txtCoQCap.Text = hdr.Rows(0)("cq_cap").ToString()
            txtNgay_CQCap.Text = hdr.Rows(0)("ngay_cqcap").ToString()
            '***************************************************************'
            hdnNgayKB.Value = hdr.Rows(0)("ngay_kb").ToString()
            'txtCapChuong.Text = hdr.Rows(0)("ma_chuong").ToString()
            txtMA_DV_DD.Text = hdr.Rows(0)("MA_DV_DD").ToString()
            txtTen_DV_DD.Text = hdr.Rows(0)("TEN_DV_DD").ToString()
            ddlLoaiBL.SelectedValue = hdr.Rows(0)("type_bl").ToString()
            Load_ddl_LoaiBL()
            mblnEdit = True
            If hdr.Rows(0)("trang_thai") = "01" Then
                EnableField(False)
                EnableField_DTL(False)
            End If
            Dim strTT_KS As Boolean = False
            If hdr.Rows(0)("ten_ks").ToString() <> "" Then
                strTT_KS = True
            End If
            EnableButton(hdr.Rows(0)("trang_thai").ToString, strTT_KS)
        End If
        '' Du lieu chi tiet
        'If dtls.Rows.Count > 0 Then
        '    If dtls.Rows.Count >= 1 Then
        '        chkCheck_1.Checked = True
        '        txtMaQuy_1.Text = dtls.Rows(0)("ma_chuong").ToString()
        '        txtMaChuong_1.Text = dtls.Rows(0)("ma_nkt").ToString()
        '        txtNDKT_1.Text = dtls.Rows(0)("ma_ndkt").ToString()
        '        txtNoiDung_1.Text = dtls.Rows(0)("noi_dung").ToString()

        '        If dtls.Rows(0)("sotien").ToString() = "" Then
        '            txtTien_1.Text = "0"
        '        Else
        '            txtTien_1.Text = String.Format("{0:0,0}", CType(dtls.Rows(0)("sotien").ToString(), Double)).Replace(",", ".")
        '        End If

        '        'txtTien_1.Text = dtls.Rows(0)("sotien").ToString()
        '        txtKyThue_1.Text = dtls.Rows(0)("ky_thue").ToString()
        '        txtKHTK_1.Text = dtls.Rows(0)("ma_khtk").ToString()
        '        txtTongTien.Text = dtls.Rows(0)("sotien").ToString()
        '    End If

        '    If dtls.Rows.Count >= 2 Then
        '        chkCheck_2.Checked = True
        '        txtMaQuy_2.Text = dtls.Rows(1)("ma_chuong").ToString()
        '        txtMaChuong_2.Text = dtls.Rows(1)("ma_nkt").ToString()
        '        txtNDKT_2.Text = dtls.Rows(1)("ma_ndkt").ToString()
        '        txtNoiDung_2.Text = dtls.Rows(1)("noi_dung").ToString()

        '        If dtls.Rows(1)("sotien").ToString() = "" Then
        '            txtTien_2.Text = "0"
        '        Else
        '            txtTien_2.Text = String.Format("{0:0,0}", CType(dtls.Rows(1)("sotien").ToString(), Double)).Replace(",", ".")
        '        End If

        '        'txtTien_2.Text = dtls.Rows(1)("sotien").ToString()
        '        txtKyThue_2.Text = dtls.Rows(1)("ky_thue").ToString()
        '        txtKHTK_2.Text = dtls.Rows(1)("ma_khtk").ToString()
        '        txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dtls.Rows(1)("sotien").ToString())
        '    End If

        '    If dtls.Rows.Count >= 3 Then
        '        chkCheck_3.Checked = True
        '        txtMaQuy_3.Text = dtls.Rows(2)("ma_chuong").ToString()
        '        txtMaChuong_3.Text = dtls.Rows(2)("ma_nkt").ToString()
        '        txtNDKT_3.Text = dtls.Rows(2)("ma_ndkt").ToString()
        '        txtNoiDung_3.Text = dtls.Rows(2)("noi_dung").ToString()

        '        If dtls.Rows(2)("sotien").ToString() = "" Then
        '            txtTien_3.Text = "0"
        '        Else
        '            txtTien_3.Text = String.Format("{0:0,0}", CType(dtls.Rows(2)("sotien").ToString(), Double)).Replace(",", ".")
        '        End If

        '        'txtTien_3.Text = dtls.Rows(2)("sotien").ToString()
        '        txtKyThue_3.Text = dtls.Rows(2)("ky_thue").ToString()
        '        txtKHTK_3.Text = dtls.Rows(2)("ma_khtk").ToString()
        '        txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dtls.Rows(2)("sotien").ToString())
        '    End If

        '    If dtls.Rows.Count >= 4 Then
        '        chkCheck_4.Checked = True
        '        txtMaQuy_4.Text = dtls.Rows(3)("ma_chuong").ToString()
        '        txtMaChuong_4.Text = dtls.Rows(3)("ma_nkt").ToString()
        '        txtNDKT_4.Text = dtls.Rows(3)("ma_ndkt").ToString()
        '        txtNoiDung_4.Text = dtls.Rows(3)("noi_dung").ToString()

        '        If dtls.Rows(3)("sotien").ToString() = "" Then
        '            txtTien_4.Text = "0"
        '        Else
        '            txtTien_4.Text = String.Format("{0:0,0}", CType(dtls.Rows(3)("sotien").ToString(), Double)).Replace(",", ".")
        '        End If

        '        'txtTien_4.Text = dtls.Rows(3)("sotien").ToString()
        '        txtKyThue_4.Text = dtls.Rows(3)("ky_thue").ToString()
        '        txtKHTK_4.Text = dtls.Rows(3)("ma_khtk").ToString()
        '        txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dtls.Rows(3)("sotien").ToString())
        '    End If

        '    If dtls.Rows.Count >= 5 Then
        '        chkCheck_5.Checked = True
        '        txtMaQuy_5.Text = dtls.Rows(4)("ma_chuong").ToString()
        '        txtMaChuong_5.Text = dtls.Rows(4)("ma_nkt").ToString()
        '        txtNDKT_5.Text = dtls.Rows(4)("ma_ndkt").ToString()
        '        txtNoiDung_5.Text = dtls.Rows(4)("noi_dung").ToString()

        '        If dtls.Rows(4)("sotien").ToString() = "" Then
        '            txtTien_5.Text = "0"
        '        Else
        '            txtTien_5.Text = String.Format("{0:0,0}", CType(dtls.Rows(4)("sotien").ToString(), Double)).Replace(",", ".")
        '        End If

        '        'txtTien_5.Text = dtls.Rows(4)("sotien").ToString()
        '        txtKyThue_5.Text = dtls.Rows(4)("ky_thue").ToString()
        '        txtKHTK_5.Text = dtls.Rows(4)("ma_khtk").ToString()
        '        txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dtls.Rows(4)("sotien").ToString())
        '    End If

        '    If dtls.Rows.Count >= 6 Then
        '        chkCheck_6.Checked = True
        '        txtMaQuy_6.Text = dtls.Rows(5)("ma_chuong").ToString()
        '        txtMaChuong_6.Text = dtls.Rows(5)("ma_nkt").ToString()
        '        txtNDKT_6.Text = dtls.Rows(5)("ma_ndkt").ToString()
        '        txtNoiDung_6.Text = dtls.Rows(5)("noi_dung").ToString()

        '        If dtls.Rows(5)("sotien").ToString() = "" Then
        '            txtTien_6.Text = "0"
        '        Else
        '            txtTien_6.Text = String.Format("{0:0,0}", CType(dtls.Rows(5)("sotien").ToString(), Double)).Replace(",", ".")
        '        End If

        '        'txtTien_6.Text = dtls.Rows(5)("sotien").ToString()
        '        txtKyThue_6.Text = dtls.Rows(5)("ky_thue").ToString()
        '        txtKHTK_6.Text = dtls.Rows(5)("ma_khtk").ToString()
        '        txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dtls.Rows(5)("sotien").ToString())
        '    End If

        '    If dtls.Rows.Count >= 7 Then
        '        chkCheck_7.Checked = True
        '        txtMaQuy_7.Text = dtls.Rows(6)("ma_chuong").ToString()
        '        txtMaChuong_7.Text = dtls.Rows(6)("ma_nkt").ToString()
        '        txtNDKT_7.Text = dtls.Rows(6)("ma_ndkt").ToString()
        '        txtNoiDung_7.Text = dtls.Rows(6)("noi_dung").ToString()

        '        If dtls.Rows(6)("sotien").ToString() = "" Then
        '            txtTien_7.Text = "0"
        '        Else
        '            txtTien_7.Text = String.Format("{0:0,0}", CType(dtls.Rows(6)("sotien").ToString(), Double)).Replace(",", ".")
        '        End If

        '        txtTien_7.Text = dtls.Rows(6)("sotien").ToString()
        '        txtKyThue_7.Text = dtls.Rows(6)("ky_thue").ToString()
        '        txtKHTK_7.Text = dtls.Rows(6)("ma_khtk").ToString()
        '        txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dtls.Rows(6)("sotien").ToString())


        '    End If

        '    If txtTongTien.Text = "" Then
        '        txtTongTien.Text = "0"
        '    Else
        '        txtTongTien.Text = VBOracleLib.Globals.Format_Number(hdr.Rows(0)("ttien").ToString().Trim, ".")
        '    End If

        ' End If

    End Sub
    Sub EnableField(ByVal bValue As Boolean)
        txtSoBaoLanh.Enabled = bValue
        txtNgayHL.Enabled = bValue
        txtNgayHetHan.Enabled = bValue
        txtTongSoNgay.Enabled = bValue
        txtMaNNT.Enabled = bValue
        txtTenNNT.Enabled = bValue
        txtDiaChi.Enabled = bValue
        txtTK.Enabled = bValue
        txtMaCQT.Enabled = bValue
        txtTenCQT.Enabled = bValue
        txtToKhaiSo.Enabled = bValue
        txtNgayDK.Enabled = bValue
        txtLHXNK.Enabled = bValue
        txtDescLHXNK.Enabled = bValue
        drpLoaiTien.Enabled = bValue
        txtDienGiai.Enabled = bValue
        txtMaHQ.Enabled = bValue
        txtTenHQ.Enabled = bValue
        '  btnTruyVanHQ.Enabled = bValue
        txtCifNumber.Enabled = bValue
        txtHD_NT.Enabled = bValue
        txtNgay_HD.Enabled = bValue
        txtHDon.Enabled = bValue
        txtNgay_HDon.Enabled = bValue
        txtVTD.Enabled = bValue
        txtNgay_VTD.Enabled = bValue
        txtMaHQPH.Enabled = bValue
        txtTenMaHQPH.Enabled = bValue
        txtCapChuong.Enabled = bValue
        ddlLoaiBL.Enabled = bValue
        txtMA_DV_DD.Enabled = bValue
        txtTen_DV_DD.Enabled = bValue
        txtVTD01.Enabled = bValue
        txtNgayVTD01.Enabled = bValue
        txtVTD02.Enabled = bValue
        txtNgayVTD02.Enabled = bValue
        txtVTD03.Enabled = bValue
        txtNgayVTD03.Enabled = bValue
        txtVTD04.Enabled = bValue
        txtNgayVTD04.Enabled = bValue
        txtVTD05.Enabled = bValue
        txtNgayVTD05.Enabled = bValue
        txtMA_HQ_KB.Enabled = bValue
        txtTenMA_HQ_KB.Enabled = bValue
        txtFone_Num.Enabled = bValue
        txt_FAX_Num.Enabled = bValue
        txtSo_DKKD.Enabled = bValue
        txtCoQCap.Enabled = bValue
        txtNgay_CQCap.Enabled = bValue
    End Sub
    Sub EnableField_DTL(ByVal bValue As Boolean)
        txtMaChuong_1.Enabled = bValue
        txtMaChuong_2.Enabled = bValue
        txtMaChuong_3.Enabled = bValue
        txtMaChuong_4.Enabled = bValue
        txtMaChuong_5.Enabled = bValue
        txtMaChuong_6.Enabled = bValue
        txtMaChuong_7.Enabled = bValue

        txtMaQuy_1.Enabled = bValue
        txtMaQuy_2.Enabled = bValue
        txtMaQuy_3.Enabled = bValue
        txtMaQuy_4.Enabled = bValue
        txtMaQuy_5.Enabled = bValue
        txtMaQuy_6.Enabled = bValue
        txtMaQuy_7.Enabled = bValue

        txtNDKT_1.Enabled = bValue
        txtNDKT_2.Enabled = bValue
        txtNDKT_3.Enabled = bValue
        txtNDKT_4.Enabled = bValue
        txtNDKT_5.Enabled = bValue
        txtNDKT_6.Enabled = bValue
        txtNDKT_7.Enabled = bValue

        txtTien_1.Enabled = bValue
        txtTien_2.Enabled = bValue
        txtTien_3.Enabled = bValue
        txtTien_4.Enabled = bValue
        txtTien_5.Enabled = bValue
        txtTien_6.Enabled = bValue
        txtTien_7.Enabled = bValue

        txtKHTK_1.Enabled = bValue
        txtKHTK_2.Enabled = bValue
        txtKHTK_3.Enabled = bValue
        txtKHTK_4.Enabled = bValue
        txtKHTK_5.Enabled = bValue
        txtKHTK_6.Enabled = bValue
        txtKHTK_7.Enabled = bValue

        txtNoiDung_1.Enabled = bValue
        txtNoiDung_2.Enabled = bValue
        txtNoiDung_3.Enabled = bValue
        txtNoiDung_4.Enabled = bValue
        txtNoiDung_5.Enabled = bValue
        txtNoiDung_6.Enabled = bValue
        txtNoiDung_7.Enabled = bValue

        txtKyThue_1.Enabled = bValue
        txtKyThue_2.Enabled = bValue
        txtKyThue_3.Enabled = bValue
        txtKyThue_4.Enabled = bValue
        txtKyThue_5.Enabled = bValue
        txtKyThue_6.Enabled = bValue
        txtKyThue_7.Enabled = bValue

        chkCheck_1.Enabled = bValue
        chkCheck_2.Enabled = bValue
        chkCheck_3.Enabled = bValue
        chkCheck_4.Enabled = bValue
        chkCheck_5.Enabled = bValue
        chkCheck_6.Enabled = bValue
        chkCheck_7.Enabled = bValue


        'txtMaQuy_2.ReadOnly = True
        'txtMaChuong_2.ReadOnly = True
        'txtNDKT_2.ReadOnly = True
        'txtTien_2.ReadOnly = True

    End Sub
    Sub TruyVanHQ()
        lblError.Text = ""
        If txtMaNNT.Text.Trim <> "" And txtToKhaiSo.Text.Trim <> "" Then
            Dim strMaNNT As String = txtMaNNT.Text.Trim
            Dim strTenDTNT As String = txtTenNNT.Text.Trim
            Dim strMaHQ As String = txtMaHQ.Text.Trim
            Dim strTenHQ As String = txtTenHQ.Text.Trim
            Dim strNamDK As String = ""
            If txtNgayDK.Text.Trim.Length > 0 Then
                strNamDK = txtNgayDK.Text.Trim.Substring(6, 4)
            End If
            Dim strMaLH As String = txtLHXNK.Text.Trim
            Dim strSoTK As String = txtToKhaiSo.Text.Trim
            Dim strErrCode As String = ""
            Dim strErrMSG As String = ""
            'Get thong tin to khai ben Hai quan, cap nhat thong tin lay duoc vao database
            'CustomsService.ProcessMSG.getMSG12(strMaNNT, strNamDK, strSoTK, strErrCode, strErrMSG)

            'Lay du lieu tu database ban TCS_DS_TOKHAI
            Dim dt As DataTable = clsCTU.TCS_GetTruyVanHQ(strMaNNT, strMaHQ, strSoTK, strMaLH, "1")

            If Not dt Is Nothing And dt.Rows.Count > 0 Then
                txtTenNNT.Text = dt.Rows(0)("ten_nnt").ToString
                If dt.Rows(0)("dia_chi").ToString.Trim <> "" Then
                    txtDiaChi.Text = dt.Rows(0)("dia_chi").ToString
                End If
                txtTK.Text = dt.Rows(0)("tk_thu_ns").ToString
                txtNgayDK.Text = dt.Rows(0)("ngay_tk").ToString
                txtTongTien.Text = dt.Rows(0)("so_phainop").ToString
                txtMaHQ.Text = dt.Rows(0)("ma_hq").ToString
                txtTenHQ.Text = dt.Rows(0)("ten_hq").ToString
                txtMaHQPH.Text = dt.Rows(0)("ma_hq_ph").ToString
                txtTenMaHQPH.Text = dt.Rows(0)("ten_hq_ph").ToString
                hdnMaHQ.Value = dt.Rows(0)("ma_hq_ph").ToString
                hdnTenHQ.Value = dt.Rows(0)("ten_hq_ph").ToString
                txtMaCQT.Text = dt.Rows(0)("ma_cqthu").ToString
                txtLHXNK.Text = dt.Rows(0)("lh_xnk").ToString
                txtTK.Text = dt.Rows(0)("tk_thu_ns").ToString.Replace(".", "")
                txtSHKB.Text = dt.Rows(0)("ma_kb").ToString.Replace(".", "")
                ' hdnSHKB.Value = dt.Rows(0)("ma_cqthu").ToString
                'Lay ten CQT
                If Not dt.Rows(0)("ma_cqthu") Is Nothing And dt.Rows(0)("ma_cqthu").ToString.Trim <> "" Then
                    txtTenCQT.Text = Business.Common.mdlCommon.Get_TenCQThu(dt.Rows(0)("ma_cqthu"))
                    'txtTenCQT.ReadOnly = True
                End If

                'Lay ten LHXNK
                If Not dt.Rows(0)("lh_xnk") Is Nothing And dt.Rows(0)("lh_xnk").ToString.Trim <> "" Then
                    txtDescLHXNK.Text = Business.Common.mdlCommon.Get_TenLHXNK(dt.Rows(0)("lh_xnk"))
                    'txtDescLHXNK.ReadOnly = True
                End If

                If dt.Rows.Count >= 1 Then
                    chkCheck_1.Checked = True
                    txtMaQuy_1.Text = dt.Rows(0)("ma_chuong").ToString().Trim()
                    txtMaChuong_1.Text = dt.Rows(0)("ma_khoan").ToString().Trim()
                    txtNDKT_1.Text = dt.Rows(0)("ma_tmuc").ToString().Trim()
                    txtNoiDung_1.Text = Get_TenMTM(dt.Rows(0)("ma_tmuc").ToString().Trim())

                    If dt.Rows(0)("so_phainop").ToString().Trim() <> "" Then
                        'txtTien_1.Text = Double.Parse(dt.Rows(0)("so_phainop").ToString().Trim())
                        txtTien_1.Text = String.Format("{0:0,0}", Double.Parse(dt.Rows(0)("so_phainop").ToString().Trim())).Replace(",", ".")
                        txtTongTien.Text = Double.Parse(dt.Rows(0)("so_phainop").ToString())
                    End If


                    'txtMaQuy_1.ReadOnly = True
                    'txtMaChuong_1.ReadOnly = True
                    'txtNDKT_1.ReadOnly = True
                    'txtTien_1.ReadOnly = True
                End If
                If dt.Rows.Count >= 2 Then
                    chkCheck_2.Checked = True
                    txtMaQuy_2.Text = dt.Rows(1)("ma_chuong").ToString().Trim()
                    txtMaChuong_2.Text = dt.Rows(1)("ma_khoan").ToString().Trim()
                    txtNDKT_2.Text = dt.Rows(1)("ma_tmuc").ToString().Trim()
                    txtNoiDung_2.Text = Get_TenMTM(dt.Rows(1)("ma_tmuc").ToString().Trim())

                    If dt.Rows(1)("so_phainop").ToString().Trim() <> "" Then
                        'txtTien_2.Text = Double.Parse(dt.Rows(1)("so_phainop").ToString().Trim())
                        txtTien_2.Text = String.Format("{0:0,0}", Double.Parse(dt.Rows(1)("so_phainop").ToString().Trim())).Replace(",", ".")
                        txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dt.Rows(1)("so_phainop").ToString())
                    End If

                    'txtMaQuy_2.ReadOnly = True
                    'txtMaChuong_2.ReadOnly = True
                    'txtNDKT_2.ReadOnly = True
                    'txtTien_2.ReadOnly = True
                End If
                If dt.Rows.Count >= 3 Then
                    chkCheck_3.Checked = True
                    txtMaQuy_3.Text = dt.Rows(2)("ma_chuong").ToString().Trim()
                    txtMaChuong_3.Text = dt.Rows(2)("ma_khoan").ToString().Trim()
                    txtNDKT_3.Text = dt.Rows(2)("ma_tmuc").ToString().Trim()
                    txtNoiDung_3.Text = Get_TenMTM(dt.Rows(2)("ma_tmuc").ToString().Trim())

                    If dt.Rows(2)("so_phainop").ToString().Trim() <> "" Then
                        'txtTien_3.Text = Double.Parse(dt.Rows(2)("so_phainop").ToString().Trim())
                        txtTien_3.Text = String.Format("{0:0,0}", Double.Parse(dt.Rows(2)("so_phainop").ToString().Trim())).Replace(",", ".")
                        txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dt.Rows(2)("so_phainop").ToString())
                    End If

                    'txtMaQuy_3.ReadOnly = True
                    'txtMaChuong_3.ReadOnly = True
                    'txtNDKT_3.ReadOnly = True
                    'txtTien_3.ReadOnly = True
                End If
                If dt.Rows.Count >= 4 Then
                    chkCheck_4.Checked = True
                    txtMaQuy_4.Text = dt.Rows(3)("ma_chuong").ToString().Trim()
                    txtMaChuong_4.Text = dt.Rows(3)("ma_khoan").ToString().Trim()
                    txtNDKT_4.Text = dt.Rows(3)("ma_tmuc").ToString().Trim()
                    txtNoiDung_4.Text = Get_TenMTM(dt.Rows(3)("ma_tmuc").ToString().Trim())

                    If dt.Rows(3)("so_phainop").ToString().Trim() <> "" Then
                        'txtTien_4.Text = Double.Parse(dt.Rows(3)("so_phainop").ToString().Trim())
                        txtTien_4.Text = String.Format("{0:0,0}", Double.Parse(dt.Rows(3)("so_phainop").ToString().Trim())).Replace(",", ".")
                        txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dt.Rows(3)("so_phainop").ToString())
                    End If

                    'txtMaQuy_4.ReadOnly = True
                    'txtMaChuong_4.ReadOnly = True
                    'txtNDKT_4.ReadOnly = True
                    'txtTien_4.ReadOnly = True
                End If
                If dt.Rows.Count >= 5 Then
                    chkCheck_5.Checked = True
                    txtMaQuy_5.Text = dt.Rows(4)("ma_chuong").ToString().Trim()
                    txtMaChuong_5.Text = dt.Rows(4)("ma_khoan").ToString().Trim()
                    txtNDKT_5.Text = dt.Rows(4)("ma_tmuc").ToString().Trim()
                    txtNoiDung_5.Text = Get_TenMTM(dt.Rows(4)("ma_tmuc").ToString().Trim())

                    If dt.Rows(4)("so_phainop").ToString().Trim() <> "" Then
                        'txtTien_5.Text = Double.Parse(dt.Rows(4)("so_phainop").ToString().Trim())
                        txtTien_5.Text = String.Format("{0:0,0}", Double.Parse(dt.Rows(4)("so_phainop").ToString().Trim())).Replace(",", ".")
                        txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dt.Rows(4)("so_phainop").ToString())
                    End If

                    'txtMaQuy_5.ReadOnly = True
                    'txtMaChuong_5.ReadOnly = True
                    'txtNDKT_5.ReadOnly = True
                    'txtTien_5.ReadOnly = True
                End If
                If dt.Rows.Count >= 6 Then
                    chkCheck_6.Checked = True
                    txtMaQuy_6.Text = dt.Rows(5)("ma_chuong").ToString().Trim()
                    txtMaChuong_6.Text = dt.Rows(5)("ma_khoan").ToString().Trim()
                    txtNDKT_6.Text = dt.Rows(5)("ma_tmuc").ToString().Trim()
                    txtNoiDung_6.Text = Get_TenMTM(dt.Rows(5)("ma_tmuc").ToString().Trim())

                    If dt.Rows(5)("so_phainop").ToString().Trim() <> "" Then
                        'txtTien_6.Text = Double.Parse(dt.Rows(5)("so_phainop").ToString().Trim())
                        txtTien_6.Text = String.Format("{0:0,0}", Double.Parse(dt.Rows(5)("so_phainop").ToString().Trim())).Replace(",", ".")
                        txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dt.Rows(5)("so_phainop").ToString())
                    End If

                    'txtMaQuy_6.ReadOnly = True
                    'txtMaChuong_6.ReadOnly = True
                    'txtNDKT_6.ReadOnly = True
                    'txtTien_6.ReadOnly = True
                End If
                If dt.Rows.Count >= 7 Then
                    chkCheck_7.Checked = True
                    txtMaQuy_7.Text = dt.Rows(6)("ma_chuong").ToString().Trim()
                    txtMaChuong_7.Text = dt.Rows(6)("ma_khoan").ToString().Trim()
                    txtNDKT_7.Text = dt.Rows(6)("ma_tmuc").ToString().Trim()
                    txtNoiDung_7.Text = Get_TenMTM(dt.Rows(6)("ma_tmuc").ToString().Trim())

                    If dt.Rows(6)("so_phainop").ToString().Trim() <> "" Then
                        'txtTien_7.Text = Double.Parse(dt.Rows(6)("so_phainop").ToString().Trim())
                        txtTien_7.Text = String.Format("{0:0,0}", Double.Parse(dt.Rows(6)("so_phainop").ToString().Trim())).Replace(",", ".")
                        txtTongTien.Text = Double.Parse(txtTongTien.Text) + Double.Parse(dt.Rows(6)("so_phainop").ToString())
                    End If

                    'txtMaQuy_7.ReadOnly = True
                    'txtMaChuong_7.ReadOnly = True
                    'txtNDKT_7.ReadOnly = True
                    'txtTien_7.ReadOnly = True

                End If
                txtTongTien.Text = String.Format("{0:0,0}", (Double.Parse(txtTongTien.Text))).Replace(",", ".")
                txtTT_TTHU.Text = txtTongTien.Text
            Else
                ClearGridDetail()
                lblError.Text = "Không có thông tin tờ khai hải quan!"
            End If
        Else
            lblError.Text = "Bạn phải nhập thông tin đủ các trường:<b> Mã số thuế, Số tờ khai!</b>"
        End If
    End Sub

    '    Public Shared Sub GetBaoLanh(ByVal strSHKB As String, ByVal strSoCT As String, ByVal strSoBT As String, ByVal strMaNV As String, _
    '                               ByRef hdr As DataTable, ByRef dtls As DataTable)
    '        clsCTU.TCS_GetBaoLanh(strSHKB, strSoCT, strSoBT, CInt(strMaNV), hdr, dtls)
    '    End Sub

    Sub CancelBaoLanh()
        lblError.Text = ""
        lblSuccess.Text = ""
        Dim strSHKB As String
        Dim strSoCT As String
        Dim strSoBT As String
        Dim strTT As String
        Dim strMA_KS As String = hdnMa_KS.Value.ToString()
        If hdnSHKB.Value.Trim <> "" And hdnSoBT.Value.Trim <> "" And hdnTrangThai.Value <> "" And hdnSO_CT.Value.Trim <> "" Then
            strSHKB = hdnSHKB.Value.Trim
            strSoCT = hdnSO_CT.Value.Trim 'txtSoCT.Text.Trim
            strSoBT = hdnSoBT.Value.Trim
            strTT = hdnTrangThai.Value.Trim
            ' Trang thai chua kiem soat, kiem soat loi thi duoc Huy
            If strTT = "00" Or strTT = "03" Then
                clsCTU.TCS_UpdateTrangThai(strSHKB, strSoCT, strSoBT, "0", "04", "", str_Ngay_LV)
                lblSuccess.Text = "Hủy bảo lãnh thành công!"
                EnableButton("04", False)
                LayDSCT()
            ElseIf strTT = "01" And (Not txtNguoiKS.Text Is Nothing) Then
                clsCTU.TCS_UpdateTrangThai(strSHKB, strSoCT, strSoBT, strMA_KS, "02", "", str_Ngay_LV)
                lblSuccess.Text = "Giao dịch viên hủy bảo lãnh thành công!"
                EnableButton("02", False)
                LayDSCT()
            Else

                lblError.Text = "Bảo lãnh không được hủy!"
            End If
        ElseIf Not Request.Params("SHKB") Is Nothing And Not Request.Params("So_CT") Is Nothing And Not Request.Params("So_BT") Is Nothing Then
            strSHKB = Request.Params("SHKB").Trim
            strSoCT = Request.Params("So_CT").Trim
            strSoBT = Request.Params("So_BT").Trim
            strTT = Request.Params("TT").Trim
            ' Trang thai chua kiem soat, kiem soat loi thi duoc Huy
            If strTT = "00" Or strTT = "03" Then
                clsCTU.TCS_UpdateTrangThai(strSHKB, strSoCT, strSoBT, "0", "04", "", str_Ngay_LV)
                lblSuccess.Text = "Hủy bảo lãnh thành công!"
                EnableButton("04", False)
                LayDSCT()
            ElseIf strTT = "01" And (Not txtNguoiKS.Text Is Nothing) Then
                clsCTU.TCS_UpdateTrangThai(strSHKB, strSoCT, strSoBT, strMA_KS, "02", "", str_Ngay_LV)
                lblSuccess.Text = "Giao dịch viên hủy bảo lãnh thành công!"
                EnableButton("02", False)
                LayDSCT()
            Else
                lblError.Text = "Bảo lãnh không được hủy!"
            End If
        Else
            lblError.Text = "Bạn phải chọn 1 bản ghi để thực hiện!"
        End If
    End Sub
    Sub Ghi_CTUBL()

        If txtToKhaiSo.Text.Trim = "99999" Then
            divHD_NT.Attributes("style") = "display:block;"
        End If
        If HttpContext.Current.Session("User") Is Nothing Then
            lblStatus.Text = "-1;Phiên làm việc của bạn bị quá hạn. Hãy thoát chương trình và đăng nhập lại"
            Exit Sub
        End If
        If CInt(HttpContext.Current.Session("User")) = 0 Then
            lblStatus.Text = "-1;Phiên làm việc của bạn bị quá hạn. Hãy thoát chương trình và đăng nhập lại"
            Exit Sub
        End If
        'Kiểm tra Số tiền có bằng Tổng số tiền được bảo lãnh hay không
        If txtNgayDK.Text.ToString <> "" And txtNgayHL.Text.ToString() <> "" Then
            If ConvertDateToNumber(txtNgayDK.Text) > ConvertDateToNumber(txtNgayHL.Text) Then
                lblError.Text = "Ngày bắt đầu hiệu lực phải lớn hơn ngày đăng ký tờ khai. Vui lòng kiểm tra lại"
                Exit Sub
            End If
        End If
        'If txtTongTien.Text.ToString.Replace(".", "") <> txtTS_TienBL_T24.Text.ToString().Replace(".", "") Then
        '    lblError.Text = "Tổng số tiền phải bằng tổng số tiền được bảo lãnh"
        '    Exit Sub
        'End If
        Dim objHDR As New infChungTuHDR
        Dim objDTL() As infChungTuDTL
        Dim strSo_BT As String
        Dim strTrang_Thai As String
        If Not ValidForm() Then
            Exit Sub
        End If
        If hdnTrangThai.Value <> "" Then
            mblnEdit = True
        Else
            mblnEdit = False
        End If
        If hdnTrangThai.Value <> "01" Then
            strTrang_Thai = "00"
        Else
            strTrang_Thai = "07"
        End If
        lblError.Text = ""
        FormToBean(objHDR, objDTL, strTrang_Thai, mblnEdit)
        strSo_BT = objHDR.So_BT
        If objHDR.TTien < 0 Then
            lblError.Text = "Không lập chứng từ với số tiền bằng 0"
            Exit Sub
        End If
        If (ChungTu.buChungTu.CTU_checkExist_SoBL(objHDR.So_bl)) Then
            lblError.Text = "Số bảo lãnh" & objHDR.So_bl & " đã được lập và chuyển Hải quan thành công. Vui lòng nhập số bảo lãnh khác."
            txtSoBaoLanh.Focus()
            Exit Sub
        End If
        'If (ChungTu.buChungTu.CTU_checkExist_SoTK(objHDR.So_TK)) Then
        '    lblError.Text = "Số tờ khai " & objHDR.So_TK & " đã được lập và chuyển Hải quan thành công. Vui lòng nhập số tờ khai khác."
        '    txtToKhaiSo.Focus()
        '    Exit Sub
        'End If
        If hdnTrangThai.Value = "" Then ' nếu là lập mới
            If Not ChungTu.buChungTu.CTU_checkExist_bySoCT(objHDR.So_CT) Then
                If ChungTu.buChungTu.InsertBaoLanh(objHDR, objDTL) Then
                    If txtToKhaiSo.Text.Equals("99999") Then
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "key", "show_div_hd();", True)
                    End If
                    lblSuccess.Text = "Lập mới chứng từ thành công!"
                    mblnEdit = True
                    txtSoBL_Search.Text = ""
                    EnableButton(strTrang_Thai)
                    hdnTrangThai.Value = strTrang_Thai
                    LayDSCT()
                Else
                    lblError.Text = "Lỗi khi lập mới chứng từ!"
                End If
            End If
        Else ' nếu là sửa chứng từ
            If ChungTu.buChungTu.UpdateBaoLanh(objHDR, objDTL) Then
                lblSuccess.Text = "Cập nhật chứng từ thành công!"
                hdnTrangThai.Value = strTrang_Thai
                mblnEdit = True
                EnableField(True)
                EnableField_DTL(True)
                LayDSCT()
            Else
                lblError.Text = "Lỗi khi cập nhật chứng từ!"
            End If
        End If
        txtTrangThaiCT.Text = Business.CTuCommon.Get_TenTrangThai(strTrang_Thai)
        
    End Sub
    Protected Sub btnGhi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGhi.Click
        ''hdnMethod.Value = "ThemMoi"
        'If hdnTrangThai.Value = "00" Then
        '    hdnMethod.Value = "ChuyenKS"
        '    SaveOrUpdateBL()
        'ElseIf hdnTrangThai.Value = "03" Then
        '    hdnMethod.Value = "Edit"
        '    SaveOrUpdateBL()
        'End If
        ''Try
        ''    SaveOrUpdateBL()
        ''Catch ex As Exception
        ''    lblError.Text = ex.Message
        ''    Throw ex
        ''End Try
        Try
            If HttpContext.Current.Session("User") Is Nothing Then
                lblStatus.Text = "-1;Phiên làm việc của bạn bị quá hạn. Hãy thoát chương trình và đăng nhập lại"
                Exit Sub
            End If
            If CInt(HttpContext.Current.Session("User")) = 0 Then
                lblStatus.Text = "-1;Phiên làm việc của bạn bị quá hạn. Hãy thoát chương trình và đăng nhập lại"
                Exit Sub
            End If
            If txtToKhaiSo.Text.Trim = "99999" Then
                divHD_NT.Attributes("style") = "display:block;"
            End If
            'Kiểm tra Số tiền có bằng Tổng số tiền được bảo lãnh hay không
            If txtNgayDK.Text.ToString <> "" And txtNgayHL.Text.ToString() <> "" Then
                If ConvertDateToNumber(txtNgayDK.Text) > ConvertDateToNumber(txtNgayHL.Text) Then
                    lblError.Text = "Ngày bắt đầu hiệu lực phải lớn hơn ngày đăng ký tờ khai. Vui lòng kiểm tra lại"
                    Exit Sub
                End If
            End If
            'If txtTongTien.Text.ToString.Replace(".", "") <> txtTS_TienBL_T24.Text.ToString().Replace(".", "") Then
            '    lblError.Text = "Tổng số tiền phải bằng tổng số tiền được bảo lãnh"
            '    Exit Sub
            'End If
            
            Dim objHDR As New infChungTuHDR
            Dim objDTL() As infChungTuDTL
            Dim strTrang_Thai As String
            If Not ValidForm() Then
                Exit Sub
            End If
            If (hdnTrangThai.Value <> "" And hdnTrangThai.Value <> "01") Then
                mblnEdit = True
            Else
                mblnEdit = False
            End If
            If hdnTrangThai.Value <> "01" Then
                strTrang_Thai = "05"
            Else
                strTrang_Thai = "07"
            End If
            lblError.Text = ""
            FormToBean(objHDR, objDTL, strTrang_Thai, mblnEdit)
            If objHDR.TTien < 0 Then
                lblError.Text = "Không lập chứng từ với số tiền bằng 0"
                Exit Sub
            End If


            If hdnTrangThai.Value = "" Then ' nếu là lập mới
                If (ChungTu.buChungTu.CTU_checkExist_SoBL(objHDR.So_bl)) Then
                    lblError.Text = "Số bảo lãnh " & objHDR.So_bl & " đã được lập và chuyển Hải quan thành công. Vui lòng nhập số bảo lãnh khác."
                    txtSoBaoLanh.Focus()
                    Exit Sub
                End If
                'If (ChungTu.buChungTu.CTU_checkExist_SoTK(objHDR.So_TK)) Then
                '    lblError.Text = "Số tờ khai " & objHDR.So_TK & " đã được lập và chuyển Hải quan thành công. Vui lòng nhập số tờ khai khác."
                '    txtToKhaiSo.Focus()
                '    Exit Sub
                'End If
                If Not ChungTu.buChungTu.CTU_checkExist_bySoCT(objHDR.So_CT) Then
                    If ChungTu.buChungTu.InsertBaoLanh(objHDR, objDTL) Then
                        If txtToKhaiSo.Text.Equals("99999") Then
                            Page.ClientScript.RegisterStartupScript(Me.GetType(), "key", "show_div_hd();", True)
                        End If
                        lblSuccess.Text = "Ghi và chuyển kiểm soát thành công!"
                        txtSoBL_Search.Text = ""
                        hdnTrangThai.Value = strTrang_Thai
                        mblnEdit = True
                        EnableButton(strTrang_Thai)
                        LayDSCT()
                    Else
                        lblError.Text = "Lỗi khi chuyển kiểm soát!"
                    End If
                Else
                    lblError.Text = "Chứng từ này đã tồn tại trong hệ thống"
                    lblSuccess.Text = ""
                    Exit Sub
                End If
            ElseIf hdnTrangThai.Value = "01" Then ' nếu là tu chỉnh

                objHDR.Ngay_KB = clsCTU.TCS_GetNgayLV(hdnMaNV.Value)
                objHDR.So_CT = txtSoCT.Text.ToString
                If CheckBeforeTuChinh(objHDR) Then
                    Dim str_nam_kb As String
                    Dim str_thang_kb As String
                    Dim strNgayLV As String = clsCTU.TCS_GetNgayLV(hdnMaNV.Value)
                    txtNgayLap.Text = ConvertNumberToDate(strNgayLV).ToString("dd/MM/yyyy")
                    str_nam_kb = Mid(strNgayLV, 3, 2)
                    str_thang_kb = Mid(strNgayLV, 5, 2)
                    objHDR.Trang_Thai = "07"
                    objHDR.So_CT_TuChinh = txtSoCT.Text
                    objHDR.So_CT = str_nam_kb + str_thang_kb + CTuCommon.Get_SoBL()
                    If ChungTu.buChungTu.InsertBaoLanh(objHDR, objDTL) Then
                        lblSuccess.Text = "Chứng từ đã được tu chỉnh và gửi kiểm soát thành công!"
                        hdnTrangThai.Value = strTrang_Thai
                        'hdnTrangThaiTT.Value = "1"
                        mblnEdit = True
                        EnableButton("07")
                        LayDSCT()
                    Else
                        lblError.Text = "Lỗi lập tu chỉnh chứng từ!"
                        lblSuccess.Text = ""
                    End If
                Else
                    lblError.Text = "Chứng từ này đã được tu chỉnh và chuyển lên kiểm soát viên."
                    lblSuccess.Text = ""
                End If
            Else ' nếu là sửa ' trang thái 00
                If (ChungTu.buChungTu.CTU_checkExist_SoBL(objHDR.So_bl)) Then
                    lblError.Text = "Số bảo lãnh " & objHDR.So_bl & " đã được lập và chuyển Hải quan thành công. Vui lòng nhập số bảo lãnh khác."
                    txtSoBaoLanh.Focus()
                    Exit Sub
                End If
                'If (ChungTu.buChungTu.CTU_checkExist_SoTK(objHDR.So_TK)) Then
                '    lblError.Text = "Số tờ khai " & objHDR.So_TK & " đã được lập và chuyển Hải quan thành công. Vui lòng nhập số tờ khai khác."
                '    txtToKhaiSo.Focus()
                '    Exit Sub
                'End If
                If ChungTu.buChungTu.UpdateBaoLanh(objHDR, objDTL) Then
                    lblSuccess.Text = "Cập nhật chứng từ và chuyển Kiểm soát thành công!"
                    hdnTrangThai.Value = strTrang_Thai
                    mblnEdit = True
                    EnableButton(strTrang_Thai)

                Else
                    lblError.Text = "Lỗi khi cập nhật và chuyển kiểm soát!"
                    lblSuccess.Text = ""
                End If
            End If
            txtTrangThaiCT.Text = Business.CTuCommon.Get_TenTrangThai(strTrang_Thai)
            LayDSCT()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    
    '    Sub SaveOrUpdateBL()
    '        Dim objHDR As New infChungTuHDR
    '        Dim objDTL() As infChungTuDTL
    '        mv_objUser = New CTuUser(Session.Item("User"))
    '        ' Ma_CN = mv_objUser.MA_CN

    '        If HttpContext.Current.Session("User") Is Nothing Then
    '            lblStatus.Text = "-1;Phiên làm việc của bạn bị quá hạn. Hãy thoát chương trình và đăng nhập lại"
    '            Exit Sub
    '        End If
    '        If CInt(HttpContext.Current.Session("User")) = 0 Then
    '            lblStatus.Text = "-1;Phiên làm việc của bạn bị quá hạn. Hãy thoát chương trình và đăng nhập lại"
    '            Exit Sub
    '        End If
    '        'If StoN(txtTongTien.Text.Trim.Replace(".", "")) = 0 Then
    '        '    lblSuccess.Text = "Không được lập chứng từ với tổng tiền bằng 0"
    '        '    Exit Sub
    '        'End If
    '        str_nam_kb = Mid(objHDR.Ngay_KB, 1, 2)
    '        str_thang_kb = Mid(objHDR.Ngay_KB, 5, 2)
    '        If Not ValidForm() Then
    '            Return
    '        Else
    '            lblError.Text = ""
    '            FormToBean(objHDR, objDTL, "")
    '            If objHDR.TTien < 0 Then
    '                Exit Sub
    '            End If
    '            If hdnMethod.Value = "Add" Then
    '                If Not ChungTu.buChungTu.CTU_checkExist_bySoCT(objHDR.So_CT) Then
    '                    If ChungTu.buChungTu.InsertBaoLanh(objHDR, objDTL) Then
    '                        lblSuccess.Text = "Gửi kiểm soát thành công!"
    '                        ResetField()
    '                        mv_txtSearch = ""
    '                        txtSoBL_Search.Text = ""
    '                        mv_blFlag = False
    '                        LayDSCT()
    '                    Else
    '                        lblError.Text = "Lỗi khi chuyển kiểm soát!"
    '                    End If
    '                End If
    '            ElseIf hdnMethod.Value = "Edit" Then
    '                If hdnTrangThai.Value = "03" Then
    '                    objHDR.Trang_Thai = "05"
    '                    If ChungTu.buChungTu.UpdateBaoLanh(objHDR, objDTL) Then
    '                        lblSuccess.Text = "Ghi và chuyển Kiểm soát thành công!"
    '                        ResetField()
    '                        LayDSCT()
    '                    Else
    '                        lblError.Text = "Lỗi khi chuyển kiểm soát!"
    '                    End If
    '                Else
    '                    objHDR.Trang_Thai = hdnTrangThai.Value
    '                    If ChungTu.buChungTu.UpdateBaoLanh(objHDR, objDTL) Then
    '                        lblSuccess.Text = "Update chứng từ thành công!"
    '                        ResetField()
    '                        LayDSCT()
    '                    Else
    '                        lblError.Text = "Lỗi khi cập nhật chứng từ!"
    '                    End If
    '                End If


    '                'Else
    '                '    lblError.Text = "Không được sửa thông tin bảo lãnh!"
    '            ElseIf hdnMethod.Value = "ThemMoi" Then
    '                objHDR.Trang_Thai = "00"
    '                If Not ChungTu.buChungTu.CTU_checkExist_bySoCT(objHDR.So_CT) Then
    '                    If ChungTu.buChungTu.InsertBaoLanh(objHDR, objDTL) Then
    '                        lblSuccess.Text = "Lập chứng từ thành công!"
    '                        ResetField()
    '                        LayDSCT()
    '                    Else
    '                        lblError.Text = "Lỗi xảy ra khi lập chứng từ!"
    '                    End If

    '                End If
    '            ElseIf hdnMethod.Value = "ChuyenKS" Then
    '                objHDR.Trang_Thai = "05"
    '                If ChungTu.buChungTu.Update_TT_BaoLanh(objHDR) Then
    '                    lblSuccess.Text = "Chuyển KS thành công!"
    '                    ResetField()
    '                    LayDSCT()
    '                Else
    '                    lblError.Text = "Lỗi xảy ra khi Chuyển KS!"
    '                End If
    '            ElseIf hdnMethod.Value = "TuChinh" Then
    '                'Cap nhat lai ngay kho bac
    '                objHDR.Ngay_KB = clsCTU.TCS_GetNgayLV(hdnMaNV.Value)
    '                If CheckBeforeTuChinh(objHDR) Then
    '                    str_nam_kb = Mid(objHDR.Ngay_KB, 3, 2)
    '                    str_thang_kb = Mid(objHDR.Ngay_KB, 5, 2)
    '                    objHDR.Trang_Thai = "07"
    '                    objHDR.So_CT_TuChinh = objHDR.So_CT
    '                    objHDR.So_CT = str_nam_kb + str_thang_kb + CTuCommon.Get_SoBL()

    '                    If ChungTu.buChungTu.InsertBaoLanh(objHDR, objDTL) Then
    '                        lblSuccess.Text = "Gửi kiểm soát thành công!"
    '                        ResetField()
    '                        LayDSCT()
    '                    Else
    '                        lblError.Text = "Lỗi khi chuyển kiểm soát!"
    '                    End If
    '                Else
    '                    lblError.Text = "Đã có chứng từ được tu chỉnh và chuyển lên kiểm soát viên."
    '                End If

    '            ElseIf hdnMethod.Value = "View" Then
    '                lblError.Text = "Bảo lãnh không được sửa!"
    '            End If
    '        End If

    '    End Sub
    Private Function Check_CitadCharacter(ByVal str_check As String) As Boolean
        Dim illegalChars As Char() = "!@#$%&^*(){}[]""_+<>/".ToCharArray()
        Dim str As String = str_check

        For Each ch As Char In str
            If Not Array.IndexOf(illegalChars, ch) = -1 Then
                Return False
            End If
        Next
        Return True
    End Function

    Function ValidForm() As Boolean
        If txtSoBaoLanh.Text.Length = 0 Then
            lblError.Text = "Số bảo lãnh không được để trống!"
            txtSoBaoLanh.Focus()
            Return False
        End If
        If txtCifNumber.Text.Length = 0 Then
            lblError.Text = "Số Cif không được để trống!"
            txtCifNumber.Focus()
            Return False
        End If
        If txtSoBaoLanh.Text.Trim = "" Then
            lblError.Text = "Bạn phải nhập số bảo lãnh!"
            txtSoBaoLanh.Focus()
            Return False
        End If

        If txtNgayHL.Text.Trim = "" Then
            lblError.Text = "Bạn phải nhập ngày hiệu lực!"
            txtNgayHL.Focus()
            Return False
        End If

        If txtNgayHetHan.Text.Trim = "" Then
            lblError.Text = "Bạn phải nhập ngày hết hạn!"
            txtNgayHetHan.Focus()
            Return False
        End If

        If txtTongSoNgay.Text.Trim = "" Then
            lblError.Text = "Bạn phải nhập tổng số ngày bảo lãnh!"
            txtTongSoNgay.Focus()
            Return False
        End If

        If txtMaNNT.Text.Trim = "" Then
            lblError.Text = "Bạn phải nhập mã số thuế!"
            txtMaNNT.Focus()
            Return False
        End If
        If txtTenNNT.Text.Trim = "" Then
            lblError.Text = "Bạn phải nhập Tên người nộp thuế!"
            txtTenNNT.Focus()
            Return False
        End If
        If Not IsNumeric(txtMA_DV_DD.Text) Then
            lblError.Text = "Mã đơn vị đại diện phải là số!"
            txtMA_DV_DD.Focus()
            Return False
        End If
        If txtMA_DV_DD.Text.Trim.Length < 10 Or txtMA_DV_DD.Text.Trim.Length > 14 Then
            lblError.Text = "Bạn phải nhập Mã đơn vị đại diện từ 10 đến 14 ký tự!"
            txtMA_DV_DD.Focus()
            Return False
        End If
        If txtTen_DV_DD.Text.Trim = "" Then
            lblError.Text = "Bạn phải nhập Tên đơn vị đại diện!"
            txtTen_DV_DD.Focus()
            Return False
        End If
        If txtTongTien.Text.Trim().Length = 0 Then
            lblError.Text = "Bạn phải nhập vào số tiền bảo lãnh!"
            txtTongTien.Focus()
            Return False
        End If
        If Convert.ToDecimal(txtTongTien.Text.Replace(".", "").ToString()) <= 0 Then
            lblError.Text = "Không nhập với số tiền nhỏ hơn hoặc bằng 0!"
            txtTongTien.Focus()
            Return False
        End If
        'If txtTK.Text.Trim = "" Then
        '    lblError.Text = "Bạn phải nhập tài khoản thu ngân sách!"
        '    txtTK.Focus()
        '    Return False
        'End If

        'If Not Check_CitadCharacter(txtTenNNT.Text) Then
        '    lblError.Text = "Tên người nộp tiền chứa ký tự đặc biệt vui lòng kiểm tra lại.!"
        '    txtTenNNT.Focus()
        '    Return False
        'End If
        'If Not Check_CitadCharacter(txtDiaChi.Text) Then
        '    lblError.Text = "Địa chỉ người nộp tiền chứa ký tự đặc biệt vui lòng kiểm tra lại.!"
        '    txtDiaChi.Focus()
        '    Return False
        'End If
        'If Not Check_CitadCharacter(txtDienGiai.Text) Then
        '    lblError.Text = "Diễn giải chứa ký tự đặc biệt vui lòng kiểm tra lại.!"
        '    txtDienGiai.Focus()
        '    Return False
        'End If
        'If txtMaCQT.Text.Trim = "" Then
        '    lblError.Text = "Bạn phải nhập mã cơ quan thu!"
        '    txtMaCQT.Focus()
        '    Return False
        'End If
        If ddlLoaiBL.SelectedValue = "31" Then
            If txtMaHQ.Text.Trim = "" Then
                lblError.Text = "Bạn phải nhập thông tin mã hải quan!"
                txtMaHQ.Focus()
                Return False
            End If
            If txtTenHQ.Text.Trim = "" Then
                lblError.Text = "Mã hải quan không đúng!"
                txtLHXNK.Focus()
                Return False
            End If
            If txtToKhaiSo.Text.Trim = "" Then
                lblError.Text = "Bạn phải nhập số tờ khai!"
                txtToKhaiSo.Focus()
                Return False
            End If

            If txtNgayDK.Text.Trim = "" Then
                lblError.Text = "Bạn phải nhập ngày đăng ký!"
                txtNgayDK.Focus()
                Return False
            End If

            If txtLHXNK.Text.Trim = "" Then
                lblError.Text = "Bạn phải nhập mã loại hình xuất nhập khẩu!"
                txtLHXNK.Focus()
                Return False
            End If
            If txtDescLHXNK.Text.Trim = "" Then
                lblError.Text = "Mã loại hình không đúng!"
                txtLHXNK.Focus()
                Return False
            End If
            If Not Valid_LHXNK(txtLHXNK.Text.Trim) Then
                lblError.Text = "Mã loại hình xuất nhập khẩu không tồn tại!"
                txtLHXNK.Focus()
                Return False
            End If
        ElseIf ddlLoaiBL.SelectedValue = "32" Then
            If (txtHDon.Text.Trim = "") Or (txtVTD01.Text.Trim = "") Then
                lblError.Text = "Bạn phải nhập số hợp đồng hoặc vận tải đơn"
                txtHD_NT.Focus()
                Return False
            End If
            If txtNgay_HDon.Text.Trim = "" Then
                lblError.Text = "Bạn phải nhập ngày hợp đồng ngoại thương"
                txtNgay_HD.Focus()
                Return False
            End If
            If txtMaHQ.Text.Trim = "" Then
                lblError.Text = "Bạn phải nhập thông tin mã hải quan kho bạc!"
                txtMaHQ.Focus()
                Return False
            End If
            If txtTenHQ.Text.Trim = "" Then
                lblError.Text = "Mã hải quan không đúng!"
                txtLHXNK.Focus()
                Return False
            End If
        ElseIf ddlLoaiBL.SelectedValue = "33" Then
            If txtMaHQ.Text.Trim = "" Then
                lblError.Text = "Bạn phải nhập thông tin mã hải quan kho bạc!"
                txtMaHQ.Focus()
                Return False
            End If
            If txtTenHQ.Text.Trim = "" Then
                lblError.Text = "Mã hải quan không đúng!"
                txtLHXNK.Focus()
                Return False
            End If
        End If

        If txtDienGiai.Text.Trim = "" Then
            lblError.Text = "Bạn phải nhập thông tin diễn giải!"
            txtDienGiai.Focus()
            Return False
        End If

        If txtCifNumber.Text.Trim = "" Then
            lblError.Text = "Bạn phải nhập thông tin số cif!"
            txtCifNumber.Focus()
            Return False
        End If

        Return True
    End Function


    Sub FormToBean(ByRef hdr As NewChungTu.infChungTuHDR, ByRef dtls As NewChungTu.infChungTuDTL(), ByVal strTrangThai_CT As String, Optional ByVal str_edit As Boolean = False)
        Dim objDTL As infChungTuDTL
        Dim k As Integer = 0
        Dim tongTien As Double = 0

        hdr.KHCT = txtKHCT.Text.Trim 'clsCTU.TCS_GetKHCT(hdr.SHKB)
        hdr.So_CT = txtSoCT.Text.Trim
        hdr.Ngay_CT = ConvertDateToNumber(txtNgayLap.Text.Trim)
        If Not txtSHKB.Text.Trim = "" Then
            hdr.SHKB = txtSHKB.Text.Trim
        Else
            hdr.SHKB = hdnSHKB.Value
        End If
        hdr.Ten_kb = Get_TenKhoBac(hdr.SHKB)
        Dim strNgayLV As String = clsCTU.TCS_GetNgayLV(hdnMaNV.Value)
        If Not hdnNgayKB.Value = "" Then
            hdr.Ngay_KB = hdnNgayKB.Value
        Else
            hdr.Ngay_KB = strNgayLV
        End If
        Dim str_nam_kb As String = Mid(strNgayLV, 3, 2)
        Dim str_thang_kb As String = Mid(strNgayLV, 5, 2)
        hdr.Ma_NV = CInt(hdnMaNV.Value)
        hdr.Ten_nv = txtNguoiLap.Text.Trim
        If str_edit = True Then
            hdr.So_BT = hdnSoBT.Value
            hdr.So_CT = hdnSO_CT.Value.Trim 'txtSoCT.Text
        Else
            hdr.So_BT = clsCTU.Get_SoBT(str_Ngay_LV, hdr.Ma_NV)
            hdnSoBT.Value = hdr.So_BT
            hdr.So_CT = str_nam_kb + str_thang_kb + CTuCommon.Get_SoBL()
            'txtSoCT.Text = hdr.So_CT
            hdnSO_CT.Value = hdr.So_CT
        End If
       
        '        'If hdnMethod.Value = "ThemMoi" Or hdnMethod.Value = "Add" Or hdnMethod.Value = "TuChinh" Then
        '        hdr.So_BT = clsCTU.Get_SoBT(clsCTU.TCS_GetNgayLV(hdnMaNV.Value), hdr.Ma_NV)
        '        'ElseIf hdnMethod.Value = "Edit" Then
        '        '    hdr.So_BT = hdnSoBT.Value
        '        'End If

        hdr.Ma_DThu = clsCTU.TCS_GetMaDT_KB(hdnSHKB.Value)

        hdr.Ma_KS = ""
        hdr.So_bl = txtSoBaoLanh.Text.Trim
        hdr.Kieu_bl = ddlLoaiBL.SelectedValue
        Dim strValue As String = txtNgayHL.Text.Trim
        If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
            hdr.Ngay_batdau = ToValue(strValue, "DATE")
        Else
            hdr.Ngay_batdau = "null"
        End If

        strValue = txtNgayHetHan.Text.Trim
        If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
            hdr.Ngay_ketthuc = ToValue(strValue, "DATE")
        Else
            hdr.Ngay_ketthuc = "null"
        End If

        hdr.Songay_bl = txtTongSoNgay.Text.Trim
        hdr.Ma_NNThue = txtMaNNT.Text.Trim
        hdr.Ten_NNThue = txtTenNNT.Text.Trim
        hdr.DC_NNThue = txtDiaChi.Text.Trim
        hdr.TK_KH_NH = txtTK.Text.Trim
        hdr.Ma_CQThu = txtMaCQT.Text.Trim
        hdr.Ten_cqthu = txtTenCQT.Text.Trim

        hdr.So_TK = txtToKhaiSo.Text.Trim
        hdr.MA_HQ = txtMaHQ.Text.Trim
        hdr.Ma_hq_ph = txtMaHQ.Text.Trim
        If txtMA_HQ_KB.Text.ToString <> "" Then
            hdr.MA_HQ = txtMA_HQ_KB.Text.Trim.ToString
            hdr.Ma_hq_ph = txtMA_HQ_KB.Text.Trim.ToString
        End If
        hdr.TEN_HQ = txtTenHQ.Text.Trim
        'hdr.Ma_hq_ph = txtMaHQPH.Text.Trim
        'hdr.TEN_HQ_PH = txtTenMaHQPH.Text.Trim

        hdr.TEN_HQ_PH = txtTenMaHQPH.Text.Trim
        hdr.MA_HQ_KB = txtTenMaHQPH.Text.Trim
        hdr.CIF = txtCifNumber.Text
        'hoapt them hoa don, van tai don
        hdr.HD_NT = txtHD_NT.Text.Trim
        hdr.TrangThai_BL = hdnTrangThaiBLT24.Value
        'them loai bl, ma cap chuong
        hdr.LOAI_BL = ddlLoaiBL.SelectedValue
        hdr.MA_CAP_CHUONG = txtCapChuong.Text.ToString
        strValue = txtNgay_HD.Text.Trim
        If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
            hdr.Ngay_HD_NT = ToValue(strValue, "DATE")
        Else
            hdr.Ngay_HD_NT = "null"
        End If
        hdr.HDon = txtHDon.Text.Trim
        hdr.VTD = txtVTD01.Text.Trim
        hdr.VTD2 = txtVTD02.Text.Trim
        hdr.VTD3 = txtVTD03.Text.Trim
        hdr.VTD4 = txtVTD04.Text.Trim
        hdr.VTD5 = txtVTD05.Text.Trim
        strValue = txtNgay_HDon.Text.Trim
        If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
            hdr.Ngay_HDon = ToValue(strValue, "DATE")
        Else
            hdr.Ngay_HDon = "null"
        End If

        strValue = txtNgayVTD01.Text.Trim
        If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
            hdr.Ngay_VTD = ToValue(strValue, "DATE")
        Else
            hdr.Ngay_VTD = "null"
        End If
        strValue = txtNgayVTD02.Text.Trim
        If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
            hdr.Ngay_VTD2 = ToValue(strValue, "DATE")
        Else
            hdr.Ngay_VTD2 = "null"
        End If
        strValue = txtNgayVTD03.Text.Trim
        If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
            hdr.Ngay_VTD3 = ToValue(strValue, "DATE")
        Else
            hdr.Ngay_VTD3 = "null"
        End If
        strValue = txtNgayVTD04.Text.Trim
        If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
            hdr.Ngay_VTD4 = ToValue(strValue, "DATE")
        Else
            hdr.Ngay_VTD4 = "null"
        End If
        strValue = txtNgayVTD05.Text.Trim
        If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
            hdr.Ngay_VTD5 = ToValue(strValue, "DATE")
        Else
            hdr.Ngay_VTD5 = "null"
        End If

        '        '************************END******************************'
        hdr.Fone_Num = txtFone_Num.Text.Trim
        hdr.Fax_Num = txt_FAX_Num.Text.Trim
        hdr.So_DKKD = txtSo_DKKD.Text.Trim
        hdr.CQ_CAP = txtCoQCap.Text.Trim
        strValue = txtNgay_CQCap.Text.Trim
        If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
            hdr.Ngay_CQCap = ToValue(strValue, "DATE")
        Else
            hdr.Ngay_CQCap = "null"
        End If
        strValue = txtNgayDK.Text.Trim
        If (strValue.Replace("/", "").Replace("-", "").Trim() <> "") Then
            hdr.Ngay_TK = ToValue(strValue, "DATE")
        Else
            hdr.Ngay_TK = "null"
        End If

        hdr.LH_XNK = txtLHXNK.Text.Trim
        hdr.Ten_lhxnk = txtDescLHXNK.Text.Trim
        hdr.Dien_giai = txtDienGiai.Text.Trim
        hdr.Trang_Thai = strTrangThai_CT ' Trang thai chuyen kiem soat
        hdr.Ma_loaitien = drpLoaiTien.SelectedValue.Trim()

        ' DBHC
        hdr.Ma_Xa = clsCTU.TCS_GetMaDBHC(hdnMaNV.Value)
        hdr.XA_ID = Get_XaID(hdr.Ma_Xa)
        'DV_DD
        hdr.MA_DV_DD = txtMA_DV_DD.Text.ToString()
        hdr.TEN_DV_DD = txtTen_DV_DD.Text.ToString()

        If chkCheck_1.Checked = True And StoN(txtTien_1.Text.Trim().Replace(".", "")) > 0 Then
            objDTL = New infChungTuDTL
            objDTL.ID = getDataKey("TCS_BAOLANH_DTL_SEQ.NEXTVAL")
            objDTL.SHKB = hdr.SHKB
            objDTL.Ngay_KB = hdr.Ngay_KB
            objDTL.Ma_NV = hdr.Ma_NV
            objDTL.So_BT = hdr.So_BT
            objDTL.Ma_DThu = hdr.Ma_DThu

            objDTL.Ma_Chuong = txtMaQuy_1.Text.Trim
            objDTL.Ma_nkt = txtMaChuong_1.Text.Trim()
            objDTL.Ma_ndkt = txtNDKT_1.Text.Trim()
            objDTL.Noi_Dung = txtNoiDung_1.Text.Trim()
            objDTL.SoTien = StoN(txtTien_1.Text.Trim().Replace(".", ""))
            tongTien = tongTien + StoN(txtTien_1.Text.Trim().Replace(".", ""))
            objDTL.Ky_Thue = txtKyThue_1.Text.Trim()
            objDTL.Ma_khtk = txtKHTK_1.Text.Trim()
            ReDim Preserve dtls(k)
            dtls(k) = objDTL
            k = k + 1
        End If

        If chkCheck_2.Checked = True And StoN(txtTien_2.Text.Trim().Replace(".", "")) > 0 Then
            objDTL = New infChungTuDTL
            objDTL.ID = getDataKey("TCS_BAOLANH_DTL_SEQ.NEXTVAL")
            objDTL.SHKB = hdr.SHKB
            objDTL.Ngay_KB = hdr.Ngay_KB
            objDTL.Ma_NV = hdr.Ma_NV
            objDTL.So_BT = hdr.So_BT
            objDTL.Ma_DThu = hdr.Ma_DThu

            objDTL.Ma_Chuong = txtMaQuy_2.Text.Trim
            objDTL.Ma_nkt = txtMaChuong_2.Text.Trim()
            objDTL.Ma_ndkt = txtNDKT_2.Text.Trim()
            objDTL.Noi_Dung = txtNoiDung_2.Text.Trim()
            objDTL.SoTien = StoN(txtTien_2.Text.Trim().Replace(".", ""))
            tongTien = tongTien + StoN(txtTien_2.Text.Trim().Replace(".", ""))
            objDTL.Ky_Thue = txtKyThue_2.Text.Trim()
            objDTL.Ma_khtk = txtKHTK_2.Text.Trim()
            ReDim Preserve dtls(k)
            dtls(k) = objDTL
            k = k + 1
        End If

        If chkCheck_3.Checked = True And StoN(txtTien_3.Text.Trim().Replace(".", "")) > 0 Then
            objDTL = New infChungTuDTL
            objDTL.ID = getDataKey("TCS_BAOLANH_DTL_SEQ.NEXTVAL")
            objDTL.SHKB = hdr.SHKB
            objDTL.Ngay_KB = hdr.Ngay_KB
            objDTL.Ma_NV = hdr.Ma_NV
            objDTL.So_BT = hdr.So_BT
            objDTL.Ma_DThu = hdr.Ma_DThu

            objDTL.Ma_Chuong = txtMaQuy_3.Text.Trim
            objDTL.Ma_nkt = txtMaChuong_3.Text.Trim()
            objDTL.Ma_ndkt = txtNDKT_3.Text.Trim()
            objDTL.Noi_Dung = txtNoiDung_3.Text.Trim()
            objDTL.SoTien = StoN(txtTien_3.Text.Trim().Replace(".", ""))
            tongTien = tongTien + StoN(txtTien_3.Text.Trim().Replace(".", ""))
            objDTL.Ky_Thue = txtKyThue_3.Text.Trim()
            objDTL.Ma_khtk = txtKHTK_3.Text.Trim()
            ReDim Preserve dtls(k)
            dtls(k) = objDTL
            k = k + 1
        End If

        If chkCheck_4.Checked = True And StoN(txtTien_4.Text.Trim().Replace(".", "")) > 0 Then
            objDTL = New infChungTuDTL
            objDTL.ID = getDataKey("TCS_BAOLANH_DTL_SEQ.NEXTVAL")
            objDTL.SHKB = hdr.SHKB
            objDTL.Ngay_KB = hdr.Ngay_KB
            objDTL.Ma_NV = hdr.Ma_NV
            objDTL.So_BT = hdr.So_BT
            objDTL.Ma_DThu = hdr.Ma_DThu

            objDTL.Ma_Chuong = txtMaQuy_4.Text.Trim
            objDTL.Ma_nkt = txtMaChuong_4.Text.Trim()
            objDTL.Ma_ndkt = txtNDKT_4.Text.Trim()
            objDTL.Noi_Dung = txtNoiDung_4.Text.Trim()
            objDTL.SoTien = StoN(txtTien_4.Text.Trim().Replace(".", ""))
            tongTien = tongTien + StoN(txtTien_4.Text.Trim().Replace(".", ""))
            objDTL.Ky_Thue = txtKyThue_4.Text.Trim()
            objDTL.Ma_khtk = txtKHTK_4.Text.Trim()
            ReDim Preserve dtls(k)
            dtls(k) = objDTL
            k = k + 1
        End If

        If chkCheck_5.Checked = True And StoN(txtTien_5.Text.Trim().Replace(".", "")) > 0 Then
            objDTL = New infChungTuDTL
            objDTL.ID = getDataKey("TCS_BAOLANH_DTL_SEQ.NEXTVAL")
            objDTL.SHKB = hdr.SHKB
            objDTL.Ngay_KB = hdr.Ngay_KB
            objDTL.Ma_NV = hdr.Ma_NV
            objDTL.So_BT = hdr.So_BT
            objDTL.Ma_DThu = hdr.Ma_DThu

            objDTL.Ma_Chuong = txtMaQuy_5.Text.Trim
            objDTL.Ma_nkt = txtMaChuong_5.Text.Trim()
            objDTL.Ma_ndkt = txtNDKT_5.Text.Trim()
            objDTL.Noi_Dung = txtNoiDung_5.Text.Trim()
            objDTL.SoTien = StoN(txtTien_5.Text.Trim().Replace(".", ""))
            tongTien = tongTien + StoN(txtTien_5.Text.Trim().Replace(".", ""))
            objDTL.Ky_Thue = txtKyThue_5.Text.Trim()
            objDTL.Ma_khtk = txtKHTK_5.Text.Trim()
            ReDim Preserve dtls(k)
            dtls(k) = objDTL
            k = k + 1
        End If

        If chkCheck_6.Checked = True And StoN(txtTien_6.Text.Trim().Replace(".", "")) > 0 Then
            objDTL = New infChungTuDTL
            objDTL.ID = getDataKey("TCS_BAOLANH_DTL_SEQ.NEXTVAL")
            objDTL.SHKB = hdr.SHKB
            objDTL.Ngay_KB = hdr.Ngay_KB
            objDTL.Ma_NV = hdr.Ma_NV
            objDTL.So_BT = hdr.So_BT
            objDTL.Ma_DThu = hdr.Ma_DThu

            objDTL.Ma_Chuong = txtMaQuy_6.Text.Trim
            objDTL.Ma_nkt = txtMaChuong_6.Text.Trim()
            objDTL.Ma_ndkt = txtNDKT_6.Text.Trim()
            objDTL.Noi_Dung = txtNoiDung_6.Text.Trim()
            objDTL.SoTien = StoN(txtTien_6.Text.Trim().Replace(".", ""))
            tongTien = tongTien + StoN(txtTien_6.Text.Trim().Replace(".", ""))
            objDTL.Ky_Thue = txtKyThue_6.Text.Trim()
            objDTL.Ma_khtk = txtKHTK_6.Text.Trim()
            ReDim Preserve dtls(k)
            dtls(k) = objDTL
            k = k + 1
        End If
        If chkCheck_7.Checked = True And StoN(txtTien_7.Text.Trim().Replace(".", "")) > 0 Then
            objDTL = New infChungTuDTL
            objDTL.ID = getDataKey("TCS_BAOLANH_DTL_SEQ.NEXTVAL")
            objDTL.SHKB = hdr.SHKB
            objDTL.Ngay_KB = hdr.Ngay_KB
            objDTL.Ma_NV = hdr.Ma_NV
            objDTL.So_BT = hdr.So_BT
            objDTL.Ma_DThu = hdr.Ma_DThu

            objDTL.Ma_Chuong = txtMaQuy_7.Text.Trim
            objDTL.Ma_nkt = txtMaChuong_7.Text.Trim()
            objDTL.Ma_ndkt = txtNDKT_7.Text.Trim()
            objDTL.Noi_Dung = txtNoiDung_7.Text.Trim()
            objDTL.SoTien = StoN(txtTien_7.Text.Trim().Replace(".", ""))
            tongTien = tongTien + StoN(txtTien_7.Text.Trim().Replace(".", ""))
            objDTL.Ky_Thue = txtKyThue_7.Text.Trim()
            objDTL.Ma_khtk = txtKHTK_7.Text.Trim()
            ReDim Preserve dtls(k)
            dtls(k) = objDTL
            k = k + 1
        End If
        hdr.TT_TThu = txtTT_TTHU.Text.ToString().Replace(".", "")
        hdr.TTien = txtTongTien.Text.ToString().Replace(".", "")
        hdr.TTien_NT = txtTS_TienBL_T24.Text.ToString().Replace(".", "")

        'Check xem thuoc ben nao duyet han muc bao lanh
        'hdr.MA_CN_2 = GetMaCnDuyet(tongTien.ToString)


    End Sub




    '#End Region

    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenCQThu(ByVal pv_strMaCQThu As String) As String
        Return clsCTU.Get_TenCQHQThu(pv_strMaCQThu.Trim)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenMaHQ(ByVal pv_strMaHQ As String) As String
        Return clsCTU.Get_TenMaHQ(pv_strMaHQ.Trim)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenLHXNK(ByVal pv_strMaLH As String) As String
        Return clsCTU.Get_TenLH(pv_strMaLH.Trim)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenNNT(ByVal pv_strMaNNT As String) As String
        Return clsCTU.Get_TenNNT(pv_strMaNNT.Trim)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_CifNumber(ByVal pv_strMaNNT As String) As String
        Return clsCTU.Get_CifNum(pv_strMaNNT.Trim)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenTieuMuc(ByVal pv_strMaTMuc As String) As String

        Return clsCTU.Get_TenTieuMuc(pv_strMaTMuc)
    End Function
    <System.Web.Services.WebMethod()> _
        Public Shared Function Get_SoBL(ByVal strSoBL As String, ByVal strMa_NV As String) As String
        Dim strResult As String = ""
        strResult = cls_corebank.GetSOBL(strSoBL)
        'Dim strErrNumber As String = ""
        'Dim strErrMsg As String = ""
        '' mv_objUser = New CTuUser(hdnMaNV.Value)
        'Dim Ma_CN As String = clsCTU.TCS_GetMaChiNhanh(strMa_NV)
        'If strSoBL.Trim <> "" Then
        '    strResult = clsCoreBank.GetMD(strSoBL, Ma_CN, strErrNumber, strErrMsg)
        'End If

        Return strResult
    End Function
    Protected Sub btnTuChinh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTuChinh.Click
        Try
            lblError.Text = ""
            lblSuccess.Text = ""
            If Not Request.Params("SHKB") Is Nothing And Not Request.Params("So_CT") Is Nothing And Not Request.Params("So_BT") Is Nothing Then
                Dim objHDR As New infChungTuHDR
                objHDR.So_CT = Request.Params("So_CT").ToString
                objHDR.SHKB = txtSHKB.Text
                objHDR.Ngay_KB = hdnNgayKB.Value
                objHDR.Ma_NV = hdnMaNV.Value
                If CheckBeforeTuChinh(objHDR) = False Then
                    lblError.Text = "Chứng từ đã được tu chỉnh và gửi lên kiểm soát viên. Bạn không thể tu chỉnh CT này"
                    btnTuChinh.Enabled = False
                    Exit Sub
                End If
                hdnMethod.Value = "TuChinh"
                lblStatus.Text = "Tu chỉnh bảo lãnh"

                btnGhi.Enabled = True
                btnGhiMoi.Enabled = False
                btnHuy.Enabled = False
                btnTuChinh.Enabled = False
                cmdIn.Enabled = False
                'cmdThanhToan.Enabled = False
                EnableField(True)
                EnableField_DTL(True)
                mblnEdit = False
                txtSoBaoLanh.Enabled = True
                txtSoBaoLanh.ReadOnly = True
                txtNgayHL.Enabled = False
                txtNgayHetHan.Enabled = False
                txtTongSoNgay.Enabled = False
                txtCifNumber.Enabled = False
                txtDienGiai.Enabled = False

            Else
                lblError.Text = "Bạn phải chọn 1 bản ghi để thực hiện tu chỉnh!"
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        LayDSCT_Search()
    End Sub

    'Protected Sub chkCheck_1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCheck_1.CheckedChanged
    '    TinhTongTien()
    'End Sub
    'Sub TinhTongTien()
    '    Dim tongTien As Double = 0
    '    If chkCheck_1.Checked = True Then
    '        tongTien = tongTien + StoN(txtTien_1.Text.Trim().Replace(".", ""))
    '    End If
    '    If chkCheck_2.Checked = True Then
    '        tongTien = tongTien + StoN(txtTien_2.Text.Trim().Replace(".", ""))
    '    End If

    '    If chkCheck_3.Checked = True Then
    '        tongTien = tongTien + StoN(txtTien_3.Text.Trim().Replace(".", ""))
    '    End If

    '    If chkCheck_4.Checked = True Then
    '        tongTien = tongTien + StoN(txtTien_4.Text.Trim().Replace(".", ""))
    '    End If

    '    If chkCheck_5.Checked = True Then
    '        tongTien = tongTien + StoN(txtTien_5.Text.Trim().Replace(".", ""))
    '    End If

    '    If chkCheck_6.Checked = True Then
    '        tongTien = tongTien + StoN(txtTien_6.Text.Trim().Replace(".", ""))
    '    End If
    '    If chkCheck_7.Checked = True Then
    '        tongTien = tongTien + StoN(txtTien_7.Text.Trim().Replace(".", ""))
    '    End If
    '    txtTongTien.Text = tongTien
    'End Sub

    'Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
    '    hdfInCTU.Value = "1"
    'End Sub

    '    Protected Sub dtgDSCT_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtgDSCT.SelectedIndexChanged

    '    End Sub

   


    Protected Sub txtNDKT_2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNDKT_2.TextChanged
        Dim noi_dung As String = Get_TenTieuMuc(txtNDKT_2.Text)
        If Not noi_dung.Length = 0 Then
            txtNoiDung_2.Text = noi_dung
        End If
    End Sub

    Protected Sub txtNDKT_3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNDKT_3.TextChanged
        Dim noi_dung As String = Get_TenTieuMuc(txtNDKT_3.Text)
        If Not noi_dung.Length = 0 Then
            txtNoiDung_3.Text = noi_dung
        End If
    End Sub

    Protected Sub txtNDKT_4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNDKT_4.TextChanged
        Dim noi_dung As String = Get_TenTieuMuc(txtNDKT_4.Text)
        If Not noi_dung.Length = 0 Then
            txtNoiDung_4.Text = noi_dung
        End If
    End Sub

    Protected Sub cmdIn_Thu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn_Thu.Click
        Dim strSo_CT As String = hdnSO_CT.Value 'txtSoCT.Text.Trim
        Dim frm As New infBaoCao
        Dim dsChungTu As DataSet
        dsChungTu = Business.buChungTu.CTU_BL_Header(strSo_CT)
        frm.TCS_DS = dsChungTu
        Session("objBaoCao") = frm
        'clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & Report_Type.Thu_Bao_Lanh, "ManPower")
    End Sub

    Private Function GetMaCnDuyet(ByVal pv_strTT As String) As String
        Dim v_strReturn As String = ""
        Try
            mv_objUser = New CTuUser(hdnMaNV.Value)
            Ma_CN = mv_objUser.MA_CN
            Dim v_strSql As String = ""
            v_strSql = "SELECT PHAN_CAP FROM TCS_DM_CHINHANH WHERE ID ='" & Ma_CN & "'"

            Dim v_strCN As String = DataAccess.ExecuteSQLScalar(v_strSql)

            Dim v_dbMaxHMD As Double = 0.0
            Dim v_dbMaxHMD_2 As Double = 0.0
            Dim v_strCN2 As String

            If v_strCN IsNot Nothing Then
                Select Case v_strCN
                    Case "1"        'Hoi so
                        v_strSql = "SELECT ID FROM TCS_DM_CHINHANH WHERE TINH_TRANG = 1 AND ID ='" & Ma_CN & "'"
                    Case "2"        'Chi nhanh
                        v_strSql = "SELECT NVL(MAX(HAN_MUC_DEN),0) FROM TCS_DM_NHANVIEN WHERE  MA_NHOM = 15 AND MA_CN='" & Ma_CN & "' "
                        v_dbMaxHMD = Double.Parse(DataAccess.ExecuteSQLScalar(v_strSql))
                        If (Double.Parse(pv_strTT) > (v_dbMaxHMD * 1000000)) Then  'Tong tien > max  => Day len cap cao hon (Hoi so)
                            v_strSql = "SELECT ID FROM TCS_DM_CHINHANH WHERE TINH_TRANG = 1 AND PHAN_CAP =1"
                        Else
                            v_strSql = "SELECT ID FROM TCS_DM_CHINHANH WHERE TINH_TRANG = 1 AND ID ='" & Ma_CN & "'"
                        End If
                    Case "0"        'Phong giao dich
                        v_strSql = "SELECT NVL(MAX(HAN_MUC_DEN),0) FROM TCS_DM_NHANVIEN WHERE  MA_NHOM = 15 AND MA_CN='" & Ma_CN & "' "
                        v_dbMaxHMD = Double.Parse(DataAccess.ExecuteSQLScalar(v_strSql))
                        If (Double.Parse(pv_strTT) > (v_dbMaxHMD * 1000000)) Then  'Neu lon hon thi day cho cap cao hon (Chi nhanh)
                            v_strSql = "SELECT T1.ID FROM TCS_DM_CHINHANH T,TCS_DM_CHINHANH T1 WHERE T.TINH_TRANG = 1" & _
                                        "AND T.ID = '" & Ma_CN & "' AND T1.ID = T.BRANCH_ID"
                            'Kiem tra xem tong tien co > Max chi nhanh hay khong
                            'Neu > thi chuyen len hoi so
                            'Nguoc lai thi la chi nhanh
                            v_strCN2 = DataAccess.ExecuteSQLScalar(v_strSql)
                            v_strSql = "SELECT NVL(MAX(HAN_MUC_DEN),0) FROM TCS_DM_NHANVIEN WHERE  MA_NHOM = 15 AND MA_CN='" & v_strCN2 & "' "

                            v_dbMaxHMD_2 = Double.Parse(DataAccess.ExecuteSQLScalar(v_strSql))
                            If (Double.Parse(pv_strTT) > (v_dbMaxHMD_2 * 1000000)) Then  'Tong tien > max  => Day len cap cao hon (Hoi so)
                                v_strSql = "SELECT ID FROM TCS_DM_CHINHANH WHERE TINH_TRANG = 1 AND PHAN_CAP =1"
                            Else
                                v_strSql = "SELECT ID FROM TCS_DM_CHINHANH WHERE TINH_TRANG = 1 AND ID ='" & v_strCN2 & "'"
                            End If
                        Else    'Muc phong giao dich
                            v_strSql = "SELECT ID FROM TCS_DM_CHINHANH WHERE TINH_TRANG = 1 AND ID ='" & Ma_CN & "'"
                        End If
                End Select

                v_strReturn = DataAccess.ExecuteSQLScalar(v_strSql)
            End If
        Catch ex As Exception

        End Try
        Return v_strReturn
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetNDKT(ByVal pv_strNDKT As String, ByVal pv_strSTT As String) As String
        Dim v_strReturn As String = "00;handx"
        Try
            Dim v_strSql As String = "SELECT MA_TMUC AS MA_DM, TEN AS TEN_DM FROM TCS_DM_MUC_TMUC WHERE MA_TMUC LIKE '" & pv_strNDKT & "' " & _
                                    " AND TINH_TRANG = '1' ORDER BY MA_TMUC"
            Dim v_dtRes As DataTable = DataAccess.ExecuteToTable(v_strSql)
            If (v_dtRes IsNot Nothing) Then
                If v_dtRes.Rows.Count > 0 Then
                    Return pv_strSTT & ";" & v_dtRes.Rows(0)(0).ToString() & ";" & v_dtRes.Rows(0)(1).ToString()
                Else
                    Return pv_strSTT & ";" & "-9999;Không tồn tại tiểu mục. Hãy chọn lại."
                End If
            Else
                Return pv_strSTT & ";" & "-9999;Không tồn tại tiểu mục. Hãy chọn lại."
            End If
        Catch ex As Exception

        End Try
        Return v_strReturn
    End Function

    Private Function CheckBeforeTuChinh(ByVal pv_objHdr As infChungTuHDR) As Boolean
        Dim v_blFlag As Boolean = False
        Try
            Dim v_strSql As String = "SELECT COUNT(1) FROM TCS_BAOLANH_HDR  " & _
                        "WHERE SHKB = '" & pv_objHdr.SHKB & "' AND NGAY_KB = '" & pv_objHdr.Ngay_KB & "'  " & _
                        "AND MA_NV= '" & pv_objHdr.Ma_NV & "' AND SO_CT_TUCHINH= '" & pv_objHdr.So_CT & "' AND TRANG_THAI = '07'"
            Dim v_intC As Integer = Integer.Parse(DataAccess.ExecuteSQLScalar(v_strSql))

            If (v_intC > 0) Then
                Return False
            Else
                Return True
            End If

        Catch ex As Exception

        End Try
        Return v_blFlag
    End Function
    Private Function CheckCapChuong(ByVal pv_CapChuong As String) As Boolean
        Dim v_blFlag As Boolean = False
        Try
            Dim v_strSql As String = "SELECT * FROM TCS_DM_CAP_CHUONG WHERE MA_CAP = 1 AND MA_CHUONG = '" & pv_CapChuong & "'  "
            Dim v_intC As Integer = Integer.Parse(DataAccess.ExecuteSQLScalar(v_strSql))

            If (v_intC > 0) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception

        End Try
        Return v_blFlag
    End Function

    Protected Sub cmdThanhToan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdThanhToan.Click
        Try
            Dim strSHKB As String
            Dim strSoCT As String
            Dim strSoBT As String
            Dim strTT As String
            If Not Request.Params("SHKB") Is Nothing And Not Request.Params("So_CT") Is Nothing And Not Request.Params("So_BT") Is Nothing Then
                strSHKB = Request.Params("SHKB").Trim
                strSoCT = Request.Params("So_CT").Trim
                strSoBT = Request.Params("So_BT").Trim
                strTT = Request.Params("TT").Trim
                Dim objHDR As New infChungTuHDR
                objHDR.So_CT = strSoCT
                objHDR.SHKB = strSHKB
                objHDR.Ngay_KB = hdnNgayKB.Value
                objHDR.Ma_NV = hdnMaNV.Value
                If CheckBeforeTuChinh(objHDR) = False Then
                    lblError.Text = "Chứng từ đã được tu chỉnh và gửi lên kiểm soát viên. Bạn không thể thanh toán"
                    cmdThanhToan.Enabled = False
                    Exit Sub
                End If
                If strTT = "01" Then
                    ChungTu.buChungTu.UpdateTrangThaiThanhToan(strSoCT, strTT, strSoBT, strSHKB)
                    lblSuccess.Text = "Thực hiện thanh toán thành công!"
                    txtTrangThai.Text = "Đã thanh toán"
                    hdnTrangThaiTT.Value = "1"
                    EnableButton("01", False)
                    LayDSCT()
                Else
                    lblError.Text = "Thao tác không thành công!"
                    Exit Sub
                End If
            Else
                lblError.Text = "Bạn phải chọn 1 chứng từ để thực hiện!"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlLoaiBL_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLoaiBL.SelectedIndexChanged
        Load_ddl_LoaiBL()
    End Sub
    Sub Load_ddl_LoaiBL()
        If ddlLoaiBL.SelectedValue = "31" Then
            rowMAHQPH.Visible = True
            rowMAHQ.Visible = True
            rowLHXNK.Visible = True
            rowNgayDK.Visible = True
            rowSOTK.Visible = True
            rowLoaiTien.Visible = True
            rowHD.Visible = False
            rowVTD1.Visible = False
            rowVTD2.Visible = False
            rowVTD3.Visible = False
            rowVTD4.Visible = False
            rowVTD5.Visible = False
            'rowMA_HQ_KB.Visible = False
            lblMa_HQ.Text = "Mã HQ"
        ElseIf ddlLoaiBL.SelectedValue = "32" Then
            rowMAHQPH.Visible = False
            rowMAHQ.Visible = True
            rowLHXNK.Visible = False
            rowNgayDK.Visible = False
            rowSOTK.Visible = False
            rowLoaiTien.Visible = False
            rowHD.Visible = True
            rowVTD1.Visible = True
            If txtVTD02.Text <> "" Then
                rowVTD2.Visible = True
            Else
                rowVTD2.Visible = False
            End If
            If txtVTD03.Text <> "" Then
                rowVTD3.Visible = True
            Else
                rowVTD3.Visible = False
            End If
            If txtVTD04.Text <> "" Then
                rowVTD4.Visible = True
            Else
                rowVTD4.Visible = False
            End If
            If txtVTD05.Text <> "" Then
                rowVTD5.Visible = True
            Else
                rowVTD5.Visible = False
            End If
            'rowMA_HQ_KB.Visible = True
            lblMa_HQ.Text = "Mã HQ Kho bạc"
        ElseIf ddlLoaiBL.SelectedValue = "33" Then
            rowMAHQPH.Visible = False
            rowMAHQ.Visible = True
            rowLHXNK.Visible = False
            rowNgayDK.Visible = False
            rowSOTK.Visible = False
            rowLoaiTien.Visible = False
            rowHD.Visible = False
            rowVTD1.Visible = False
            rowVTD2.Visible = False
            rowVTD3.Visible = False
            rowVTD4.Visible = False
            rowVTD5.Visible = False
            'rowMA_HQ_KB.Visible = False
            lblMa_HQ.Text = "Mã HQ Kho bạc"
        End If
    End Sub
    Private Sub DM_LTIEN()
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable

        Dim strSQL As String = "SELECT MA_LT, MA_LT ||'-'||ten_lt ten_lt from tcs_dm_loaitien_thue where ma_lt=1"
        dt = DataAccess.ExecuteToTable(strSQL)
        If Not dt Is Nothing Then
            drpLoaiTien.DataSource = dt
            drpLoaiTien.DataTextField = "ten_lt"
            drpLoaiTien.DataValueField = "MA_LT"
            drpLoaiTien.DataBind()
        End If
        
    End Sub

    'Protected Sub txtNgayVTD01_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNgayVTD01.TextChanged
    '    rowVTD2.Visible = True
    '    txtVTD02.Focus()
    'End Sub

    'Protected Sub txtNgayVTD02_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNgayVTD02.TextChanged
    '    rowVTD3.Visible = True
    '    txtVTD03.Focus()
    'End Sub

    'Protected Sub txtNgayVTD03_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNgayVTD03.TextChanged
    '    rowVTD4.Visible = True
    '    txtVTD04.Focus()
    'End Sub

    'Protected Sub txtNgayVTD04_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNgayVTD04.TextChanged
    '    rowVTD5.Visible = True
    '    txtVTD05.Focus()
    'End Sub

   
    Protected Sub btnImageAdd1_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImageAdd1.Click
        rowVTD2.Visible = True
        txtVTD02.Focus()
    End Sub

    Protected Sub btnImageAdd2_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImageAdd2.Click
        rowVTD3.Visible = True
        txtVTD03.Focus()
    End Sub

    Protected Sub btnImageAdd3_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImageAdd3.Click
        rowVTD4.Visible = True
        txtVTD04.Focus()
    End Sub

    Protected Sub btnImageAdd4_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImageAdd4.Click
        rowVTD5.Visible = True
        txtVTD05.Focus()
    End Sub
    Sub EnableButtonAdd(ByVal enable As Boolean)
        btnImageAdd1.Visible = enable
        btnImageAdd2.Visible = enable
        btnImageAdd3.Visible = enable
        btnImageAdd4.Visible = enable
    End Sub
End Class
