﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmTraCuuBLHQ.aspx.vb" Inherits="pages_ChungTu_frmTraCuuBLHQ" Title="Tra cứu bảo lãnh hải quan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/popcalendar.js" type="text/javascript"></script>
    <script src="../../javascript/jquery/jquery-1.3.2.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    /***************************Kich hoat su kien**********************************/    
    $(document).ready(function () {
        $("#<%=txtMaNV.clientID%>").blur(function () {
            Get_TenNV();
        });       
    });
    /******************LOV*******************************************************/
    function ShowLov(strType)
    {        
        if (strType=="CNNganHang") return FindDanhMuc('CNNganHang', '<%=txtMaCN.ClientID %>', '<%=txtTenCN.ClientID %>', '<%=btnSearch.ClientID %>');
        if (strType=="DM_NV") return FindDanhMuc('DM_NV', '<%=txtMaNV.ClientID %>', '<%=txtTenNV.ClientID %>', '<%=btnSearch.ClientID %>');       
                          
    }
    function FindDanhMuc(strPage,txtID, txtTitle,txtFocus) 
    {
        returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&initParam=" + $get(txtID).value,"", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
        if (returnValue != null) {
            document.getElementById(txtID).value = returnValue.ID;
            if (txtTitle!=null){
                document.getElementById(txtTitle).value = returnValue.Title;                    
            }
            if (txtFocus != null) {
                //if ($get('txtFocus').getAttribute('disabled') != 'disabled') {
                    try {
                        document.getElementById(txtFocus).focus();
                    }
                catch (e) {
                        document.getElementById(txtFocus).focus();
                    };
                //}
            }
        }
        //NextFocus();
    }    
    
    /***************************TEN_MA_NV**********************************/    
    function Get_TenNV() {
        PageMethods.Get_TenNV($get('<%=txtMaNV.clientID%>').value, Get_TenNV_Complete, Get_TenNV_Error);
    }

    function Get_TenNV_Complete(result, methodName) {
        if (result.length > 0) {
            $get('<%=txtTenNV.clientID%>').value = result;
        }
        else {
            $get('<%=txtTenNV.clientID%>').value = "";
        }
    }
    function Get_TenNV_Error(error, userContext, methodName) {
        if (error !== null) {
            $get('<%=txtTenNV.clientID%>').value = "";
        }
    }
    function jsCalTotal(){
    
        if ($get('<%= txtSoTien.ClientID %>').value.length > 0) {            
                    jsFormatNumber('<%= txtSoTien.ClientID %>');
                }  
    }
    function jsFormatNumber(txtCtl){
        document.getElementById(txtCtl).value = localeNumberFormat(document.getElementById(txtCtl).value.replaceAll('.',''),'.');
    }
         
    function localeNumberFormat(amount,delimiter) {
	    try {
	   
		    amount = parseFloat(amount);
	    }catch (e) {
		    throw ('localeNumberFormat caused INVALID FLOAT with value ' + amount)
		    return null;
	    }
	    if (delimiter == null || delimiter == undefined) { delimiter = '.'; }
	    
	    // convert to string
	    if (amount.match != 'function') { amount = amount.toString(); }

	    // validate as numeric
	    var regIsNumeric = /[^\d,\.-]+/igm;
	    var results = amount.match(regIsNumeric);

	    if (results != null) {
		    outputText('INVALID NUMBER', eOutput)
		    return null;
	    }

	    var minus = amount.indexOf('-') >= 0 ? '-' : '';
	    amount = amount.replace('-', '');
	    var amtLen = amount.length;
	    var decPoint = amount.indexOf(',');
	    var wholeNumberEnd = decPoint > 0 ? amtLen - (amtLen - decPoint) : amtLen;

	    var wholeNumber = amount.substr(0, wholeNumberEnd);
	    
	    var numberEnd=amount.substr(decPoint,amtLen);
	    var fraction = amount.substr(wholeNumberEnd, amtLen - wholeNumberEnd);

	    var segments = (wholeNumberEnd - (wholeNumberEnd % 3)) / 3;
	    var rvsNumber = wholeNumber.split('').reverse().join('');
	    var output = '';

	    for (i = 0; i < wholeNumberEnd; i++) {
		    if (i % 3 == 0 && i != 0 && i != wholeNumberEnd) { output += delimiter; }
		    output += rvsNumber.charAt(i);
	    }
	    output = minus +  output.split('').reverse().join('') + fraction ;
	    return output;
    }    
       
    String.prototype.replaceAll = function(strTarget,strSubString)
    {
        var strText = this;
        var intIndexOfMatch = strText.indexOf( strTarget );
        while (intIndexOfMatch != -1){
            strText = strText.replace( strTarget, strSubString )
            intIndexOfMatch = strText.indexOf( strTarget );
        }
        return( strText );
    } 
 
    function mask(str,textbox,loc,delim){
        var locs = loc.split(',');
        for (var i = 0; i <= locs.length; i++){
	        for (var k = 0; k <= str.length; k++){
	            if (k == locs[i]){
	            if (str.substring(k, k+1) != delim){
	                    str = str.substring(0,k) + delim + str.substring(k,str.length)
	                }
	            }
	        }
        }
        textbox.value = str
    }
    //***************************************************************************/
//    $(document).ready(function(){
////            $("#<%=txtMaCN.ClientID %>").keypress(function(){
////                if (event.keyCode==13){ShowLov('CNNganHang');}
////            }); 
//    /************************KEY_BLUR*************************************/
//        $("#<%=txtMaCN.clientID%>").blur(function () {
//        
//            if($("#<%=txtMaCN.clientID%>").val().length == 3 && $("#<%=txtMaCN.clientID%>").val().length != 0)
//            {
//                $("#<%=txtMaCN.clientID%>").val($("#<%=txtMaCN.clientID%>").val() + '00');
//            }
//            if($("#<%=txtMaCN.clientID%>").val().length < 3){
//                $("#<%=txtTenCN.clientID%>").val("");
//            } else {
//                Get_TenCN();
//            }
//        });   
//    });
//    
//    /****************************TEN_CQTHU*********************************/
//    function Get_TenCN() {
//        PageMethods.Get_TenCNNH($get('<%=txtMaCN.clientID%>').value, Get_TenCNNH_Complete, Get_TenCNNH_Error);
//    }

//    function Get_TenCNNH_Complete(result, methodName) {
//        if (result.length > 0) {
//            $get('<%=txtTenCN.clientID%>').value = result;
//        }
//        else {
//            $get('<%=txtTenCN.clientID%>').value = result;
//        }
//    }
//    function Get_TenCNNH_Error(error, userContext, methodName) {
//        if (error !== null) {
//            $get('<%=txtTenCN.clientID%>').value = result;
//        }
//    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">TRA CỨU BẢO LÃNH HẢI QUAN</asp:Label>
            </td>            
        </tr>
       <tr>
        <td class="errorMessage">                                            
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>                                            
        </td>
        </tr>
        <tr>
            <td style="color:Blue">                                            
                <asp:Label ID="lblSuccess" runat="server" Text=""></asp:Label>                                           
            </td>
        </tr>  
        <tr>
            <td width='100%' align="left" valign="top" class='nav'>
                <div id="divTracuu">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody><tr>
                            <td width="90%" align="left" valign="top">
                                <table width="100%" border="0" cellspacing="1" cellpadding="2" class="form_input">
                                    <tbody>
                                    <tr style="height:25px;">
                                        <td width="10%" align="left" valign="middle" class="form_label">
                                            Số bảo lãnh
                                        </td>
                                        <td width="20%" align="left" valign="top" class="form_control">
                                            <asp:TextBox ID="txtSoBL" MaxLength="16" runat="server" CssClass="inputflat"
                                            style="width: 80%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>                                             
                                        </td>
                                        <td width="10%" align="left" valign="middle" class="form_label" >
                                            Ngày HLBL từ
                                        </td>
                                        <td width="20%" align="left" valign="top" class="form_control" >                                            
                                            <asp:TextBox ID="txtTunNgayBL" runat="server" CssClass="inputflat"
                                            onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="CheckDate(this);" MaxLength="10"
                                            style="width: 80%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox> 
                                            <asp:Image ID="btnImageTuNgayBL" imagealign="AbsMiddle" runat="server" ImageUrl="~/images/cal.gif"  TabIndex="20"/>                                            
                                           
                                        </td>
                                        <td width="15%" align="left" valign="middle" class="form_label">
                                            Ngày hết HLBL đến
                                        </td>
                                        <td align="left" valign="top" class="form_control">
                                            <asp:TextBox ID="txtDenNgayBL" runat="server" CssClass="inputflat"
                                            onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="CheckDate(this);" MaxLength="10"
                                            style="width: 80%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>                                             
                                            <asp:Image ID="btnImageDenNgayBL" imagealign="AbsMiddle" runat="server" ImageUrl="~/images/cal.gif"  TabIndex="20"/> 
                                        </td>
                                    </tr>
                                    <tr align="left" style="height:25px;">
                                        <td class="form_label" valign="middle">
                                            Số chứng từ
                                        </td>
                                        <td class="form_control">
                                            <asp:TextBox ID="txtSoCTu" runat="server" CssClass="inputflat"
                                            style="width: 80%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox> 
                                        </td>
                                        <td class="form_label" valign="middle" style="width: 12%">
                                            Số tiền
                                        </td>
                                        <td class="form_control">
                                            <asp:TextBox ID="txtSoTien" runat="server" CssClass="inputflat" onfocus="this.select()" MaxLength="17"
                                            style="width: 80%; border-color: White; font-weight: bold; text-align: right;" onblur="jsCalTotal();"></asp:TextBox> 
                                        </td>
                                        <td class="form_label" valign="middle">
                                            Trạng thái BL
                                        </td>
                                        <td class="form_control">
                                           <asp:DropDownList ID="drpTrangThai" runat="server" style="width: 100%" CssClass="inputflat">                                                
                                            <asp:ListItem Value="">Tất cả</asp:ListItem>
                                           <%-- <asp:ListItem Value="00">Lập mới</asp:ListItem>  --%>                                                                                                                               
                                            <asp:ListItem Value="05">Chờ kiểm soát</asp:ListItem>
                                            <asp:ListItem Value="07">Tu Chỉnh chờ kiểm soát</asp:ListItem>                                            
                                            <asp:ListItem Value="01">Đã kiểm soát</asp:ListItem>
                                            <%--<asp:ListItem Value="06">Đã thanh toán</asp:ListItem>--%>
                                            <asp:ListItem Value="03">Chuyển trả</asp:ListItem>
                                            <asp:ListItem Value="02">Hủy bởi GDV - Chờ duyệt</asp:ListItem>
                                            <asp:ListItem Value="04">Đã hủy bởi GDV</asp:ListItem>
                                            <asp:ListItem Value="09">Đã hủy bởi KSV</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr style="height:25px;">
                                        <td width="100" align="left" valign="middle" class="form_label">
                                            Số tờ khai
                                        </td>
                                        <td align="left" valign="top" class="form_control">
                                            <asp:TextBox ID="txtSoTKhai" runat="server" CssClass="inputflat"
                                            style="width: 80%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox> 
                                        </td>
                                         <td class="form_label" valign="middle" style="width: 12%">
                                            Người lập
                                        </td>
                                        <td class="form_control" colspan="3">                                            
                                           <asp:TextBox ID="txtMaNV" runat="server" CssClass="inputflat"
                                            style="width: 20%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                            <asp:TextBox ID="txtTenNV" runat="server" CssClass="inputflat"
                                            style="width: 70%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                            <img src="../../images/search.jpeg" width="14" height="14" onclick="ShowLov('DM_NV')" />
                                        </td>                                                                              
                                    </tr>
                                    <tr style="height:25px;">
                                        <td class="form_label" valign="middle">
                                            Chi nhánh
                                        </td>
                                        <td class="form_control" style="width: 21%" >
                                            <asp:DropDownList ID="txtMaCN"  runat="server" CssClass="inputflat"
                                                AutoPostBack="true" style="width: 95%; border-color: White; font-weight: bold; text-align: left;">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtTenCN" runat="server" CssClass="inputflat" Enabled ="false"
                                            style="width: 70%; display:none; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>                                            
                                            <%--<img src="../../images/search.jpeg" width="14" id="Img1" runat="server" height="14" onclick="ShowLov('CNNganHang')" />--%>
                                        </td>
                                        <td class="form_label" valign="middle">
                                            Ngày lập
                                        </td>
                                        <td class="form_control">
                                            <asp:TextBox ID="txtNgayLap" runat="server" CssClass="inputflat"
                                            onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="CheckDate(this);" MaxLength="10"
                                            style="width: 80%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>                                             
                                            <asp:Image ID="btnImageNgayLap" imagealign="AbsMiddle" runat="server" ImageUrl="~/images/cal.gif"  TabIndex="20"/> 
                                        </td>
                                        <td class="form_label" valign="middle">
                                            Số CIF
                                        </td>
                                        <td class="form_control" >
                                            <asp:TextBox ID="txtCif" MaxLength="16" runat="server" CssClass="inputflat"
                                            style="width: 80%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox> 
                                        </td>
                                       
                                        
                                    </tr>
                                    <tr style="height:0px; display:none">
                                         <td class="form_label" valign="middle" visible="false" style="display:none">
                                            Ngày KS
                                        </td>
                                        <td class="form_control"  style="display:none" visible="false">
                                            <asp:TextBox ID="txtNgayKS" runat="server" CssClass="inputflat"
                                            onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="CheckDate(this);" MaxLength="10"
                                            style="width: 80%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>                                             
                                            <asp:Image ID="btnImageNgayKS" imagealign="AbsMiddle" runat="server" ImageUrl="~/images/cal.gif"  TabIndex="20"/> 
                                        </td>
                                        <td class="form_label" valign="middle">                                            
                                        </td>
                                        <td class="form_control" >
                                        </td>
                                        <td class="form_label" valign="middle">                                            
                                        </td>
                                        <td class="form_control" >
                                        </td>
                                    </tr>
                                    <tr style="height:25px; display:none" valign="middle">
                                        <td class="form_label" valign="middle">
                                            <%--Tình trạng BL--%>
                                        </td>
                                        <td class="form_control">
                                            <asp:DropDownList ID="drpTinhTrang" runat="server" style="width: 100%" CssClass="inputflat" Visible="false">                                                
                                                <asp:ListItem Value="">Tất cả</asp:ListItem> 
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" valign="middle" class="form_label" style="width: 12%">
                                            <%--T.T Truyền HQ--%>
                                        </td>
                                        <td align="left" colspan="3" valign="top" class="form_control">
                                             <asp:DropDownList ID="drpTruyenHQ" runat="server" style="width: 100%" Visible="false" 
                                                 CssClass="inputflat" Width="50%">      
                                                 <asp:ListItem Value="">Tất cả</asp:ListItem>                                            
                                            </asp:DropDownList>   
                                        </td>                                      
                                    </tr>
                                    <tr style="height:25px; display: none" >
                                        <td align="left" valign="middle" class="form_label" colspan="2">
                                            
                                        </td>
                                        <td align="left" valign="middle" class="form_label" style="width: 12%">
                                            <%--Loại báo cáo--%>
                                        </td>
                                        <td align="left" colspan="3" valign="top" class="form_control">
                                           <asp:DropDownList ID="drpLoaiBC" runat="server" style="width: 100%" Visible="false"
                                                CssClass="inputflat" Width="50%">
                                                <asp:ListItem Value="">Tất cả</asp:ListItem>                                                 
                                            </asp:DropDownList>   
                                        </td> 
                                    </tr>                                    
                                </tbody></table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="padding:10px">
                                <asp:Button ID="btnSearch" runat="server" Text="Tìm kiếm" CssClass="ButtonCommand" /> &nbsp;                             
                            </td>
                        </tr>
                    </tbody></table>
                </div>                
            </td>
        </tr>
    </table>
</asp:Content>
