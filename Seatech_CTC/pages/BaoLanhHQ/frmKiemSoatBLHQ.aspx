﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmKiemSoatBLHQ.aspx.vb" Inherits="pages_BaoLanhHQ_frmKiemSoatBLHQ"
    Title="Kiểm soát bảo lãnh hải quan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popcalendar.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">  

 function FindDanhMuc(strPage,txtID, txtTitle,txtFocus) 
    {        
        var strSHKB;
         var returnValue;
        if (document.getElementById('<%=hdnSHKB.clientID%>').value.length>0)
        {
            strSHKB=document.getElementById('<%=hdnSHKB.clientID%>').value;
        }
        else
        {
            strSHKB= defSHKB.split(';')[0];
        }
            
         returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&SHKB=" +strSHKB +"&initParam=" + $get(txtID).value,"", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                if (txtTitle!=null){
                    document.getElementById(txtTitle).value = returnValue.Title;                    
                }
                if (txtFocus!=null){
                    document.getElementById(txtFocus).focus();
                }
        }
    }

     function FindNNT_NEW(txtID, txtTitle) 
     {
        var strSHKB;
        if (document.getElementById('<%=hdnSHKB.clientID%>').value.length>0)
        {
            strSHKB=document.getElementById('<%=hdnSHKB.clientID%>').value;
        }
         else
        {
            strSHKB= defSHKB.split(';')[0];
        }
        var returnValue = window.showModalDialog("../../Find_DM/Find_NNT_NEW.aspx?SHKB=" + strSHKB + "&initParam=" + $get('<%=hdnSHKB.clientID%>').value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;
                //document.getElementById(txtFocus).focus();
            }       
    }
    
    function ShowLov(strType)
    {
        if (strType=="NNT") return FindNNT_NEW('<%=txtMaNNT.clientID%>', '<%=txtTenNNT.clientID%>');                 
        if (strType=="SHKB") return FindDanhMuc('KhoBac','txtSHKB', 'txtTenKB','txtSHKB');                 
        if (strType=="DBHC") return FindDanhMuc('DBHC','txtMaDBHC', 'txtTenDBHC','txtMaCQThu'); 
        if (strType=="TaiKhoan") return FindDanhMuc('TaiKhoan','<%=txtTK.clientID%>', '<%=txtTenTKNS.clientID%>', null);         
        if (strType=="CQT") return FindDanhMuc('CQThu','<%=txtMaCQT.clientID%>','<%=txtTenCQT.clientID%>',null);         
        if (strType=="LHXNK") return FindDanhMuc('LHXNK','<%=txtLHXNK.clientID%>','<%=txtDescLHXNK.clientID%>', null);                                    
    }
    function ShowMLNS(strType)
    {        
        if (strType=="MQ_01") return FindDanhMuc('MaQuy','<%=txtMaQuy_1.clientID%>', null, null);
        if (strType=="MQ_02") return FindDanhMuc('MaQuy','<%=txtMaQuy_2.clientID%>', null, null);
        if (strType=="MQ_03") return FindDanhMuc('MaQuy','<%=txtMaQuy_3.clientID%>', null, null);
        if (strType=="MQ_04") return FindDanhMuc('MaQuy','<%=txtMaQuy_4.clientID%>', null, null);
        if (strType=="MQ_05") return FindDanhMuc('MaQuy','<%=txtMaQuy_5.clientID%>', null, null);
    
        if (strType=="MC_01") return FindDanhMuc('CapChuong','<%=txtMaChuong_1.clientID%>', null, '<%=txtNDKT_1.clientID%>');
        if (strType=="MC_02") return FindDanhMuc('CapChuong','<%=txtMaChuong_2.clientID%>',null,'<%=txtNDKT_2.clientID%>');
        if (strType=="MC_03") return FindDanhMuc('CapChuong','<%=txtMaChuong_3.clientID%>',null,'<%=txtNDKT_3.clientID%>');
        if (strType=="MC_04") return FindDanhMuc('CapChuong','<%=txtMaChuong_4.clientID%>',null,'<%=txtNDKT_4.clientID%>');        
        
        if (strType=="MM_01") return FindDanhMuc('MucTMuc','<%=txtNDKT_1.clientID%>','<%=txtNoiDung_1.clientID%>','<%=txtNoiDung_1.clientID%>');  
        if (strType=="MM_02") return FindDanhMuc('MucTMuc','<%=txtNDKT_2.clientID%>','<%=txtNoiDung_2.clientID%>','<%=txtNoiDung_2.clientID%>'); 
        if (strType=="MM_03") return FindDanhMuc('MucTMuc','<%=txtNDKT_3.clientID%>','<%=txtNoiDung_3.clientID%>','<%=txtNoiDung_3.clientID%>'); 
        if (strType=="MM_04") return FindDanhMuc('MucTMuc','<%=txtNDKT_4.clientID%>','<%=txtNoiDung_4.clientID%>','<%=txtNoiDung_4.clientID%>'); 
    }
    
    function daysFromString(dateString)
    {
        // split strings at / and return array
        var splittedString = dateString.split("/");
        // make a new date. Caveat: Months are 0-based in JS
        var newDate = new Date(parseInt(splittedString[2], 10), parseInt(splittedString[1], 10)-1, parseInt(splittedString[0], 10));
        // returns days since jan 1 1970
        return Math.round(newDate.getTime() / (24*3600*1000));
    }

    function datediff() {      
        ds1 = document.getElementById('<%=txtNgayHL.clientID%>').value
        ds2 = document.getElementById('<%=txtNgayHetHan.clientID%>').value        
        if  (ds1 != '' && ds2 != ''){
          var dateDays1 = daysFromString(ds1);
          var dateDays2 = daysFromString(ds2);
          var diff = dateDays2 - dateDays1 ;          
          document.getElementById('<%=txtTongSoNgay.clientID%>').value = diff;
         }
         else
          document.getElementById('<%=txtTongSoNgay.clientID%>').value = '';      
        }
    
    function jsCalTotal()
    {        
        var dblTongTien = 0;        
                
        if ($get('<%=txtTien_1.clientID%>').value.length > 0) {            
            dblTongTien += parseFloat ($get('<%=txtTien_1.clientID%>').value.replaceAll('.',''));    
            jsFormatNumber('<%=txtTien_1.clientID%>');
        }    
        if ($get('<%=txtTien_2.clientID%>').value.length > 0) {
            dblTongTien += parseFloat ($get('<%=txtTien_2.clientID%>').value.replaceAll('.',''));
            jsFormatNumber('<%=txtTien_2.clientID%>');
        }
        if ($get('<%=txtTien_3.clientID%>').value.length > 0){
            dblTongTien += parseFloat ($get('<%=txtTien_3.clientID%>').value.replaceAll('.',''));
            jsFormatNumber('<%=txtTien_3.clientID%>');
        }                 
        if ($get('<%=txtTien_4.clientID%>').value.length > 0) {        
            dblTongTien += parseFloat ($get('<%=txtTien_4.clientID%>').value.replaceAll('.',''));
            jsFormatNumber('<%=txtTien_4.clientID%>');
        }
         if ($get('<%=txtTien_5.clientID%>').value.length > 0) {        
            dblTongTien += parseFloat ($get('<%=txtTien_5.clientID%>').value.replaceAll('.',''));
            jsFormatNumber('<%=txtTien_5.clientID%>');
        }
        
       $get('<%=txtTongTien.clientID%>').value = dblTongTien;
       jsFormatNumber('<%=txtTongTien.clientID%>');                               
    }    
    
      function jsFormatNumber(txtCtl){
        document.getElementById(txtCtl).value = localeNumberFormat(document.getElementById(txtCtl).value.replaceAll('.',''),'.');
    }
         
    function localeNumberFormat(amount,delimiter) {
	    try {
	   
		    amount = parseFloat(amount);
	    }catch (e) {
		    throw ('localeNumberFormat caused INVALID FLOAT with value ' + amount)
		    return null;
	    }
	    if (delimiter == null || delimiter == undefined) { delimiter = '.'; }
	    
	    // convert to string
	    if (amount.match != 'function') { amount = amount.toString(); }

	    // validate as numeric
	    var regIsNumeric = /[^\d,\.-]+/igm;
	    var results = amount.match(regIsNumeric);

	    if (results != null) {
		    outputText('INVALID NUMBER', eOutput)
		    return null;
	    }

	    var minus = amount.indexOf('-') >= 0 ? '-' : '';
	    amount = amount.replace('-', '');
	    var amtLen = amount.length;
	    var decPoint = amount.indexOf(',');
	    var wholeNumberEnd = decPoint > 0 ? amtLen - (amtLen - decPoint) : amtLen;

	    var wholeNumber = amount.substr(0, wholeNumberEnd);
	    
	    var numberEnd=amount.substr(decPoint,amtLen);
	    var fraction = amount.substr(wholeNumberEnd, amtLen - wholeNumberEnd);

	    var segments = (wholeNumberEnd - (wholeNumberEnd % 3)) / 3;
	    var rvsNumber = wholeNumber.split('').reverse().join('');
	    var output = '';

	    for (i = 0; i < wholeNumberEnd; i++) {
		    if (i % 3 == 0 && i != 0 && i != wholeNumberEnd) { output += delimiter; }
		    output += rvsNumber.charAt(i);
	    }
	    output = minus +  output.split('').reverse().join('') + fraction ;
	    return output;
    }    
       
    String.prototype.replaceAll = function(strTarget,strSubString)
    {
        var strText = this;
        var intIndexOfMatch = strText.indexOf( strTarget );
        while (intIndexOfMatch != -1){
            strText = strText.replace( strTarget, strSubString )
            intIndexOfMatch = strText.indexOf( strTarget );
        }
        return( strText );
    } 
 
    function mask(str,textbox,loc,delim){
        var locs = loc.split(',');
        for (var i = 0; i <= locs.length; i++){
	        for (var k = 0; k <= str.length; k++){
	            if (k == locs[i]){
	            if (str.substring(k, k+1) != delim){
	                    str = str.substring(0,k) + delim + str.substring(k,str.length)
	                }
	            }
	        }
        }
        textbox.value = str
    }
    
    function valInteger(obj) {
        var i, strVal, blnChange;
        blnChange = false
        strVal = "";

        for (i = 0; i < (obj.value).length; i++) {
            switch (obj.value.charAt(i)) {
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9": strVal = strVal + obj.value.charAt(i);
                    break;
                default: blnChange = true;
                    break;
            }
        }
        if (blnChange) {
            obj.value = strVal;
        }
    }
   function disableButton() {
    document.getElementById('<%=btnKiemSoat.clientID%>').disabled=true;
    document.getElementById('<%=btnChuyenTra.clientID%>').disabled=true;
    } 
    </script>

    <style type="text/css">
        .style1
        {
            width: 222px;
        }
        .style2
        {
            width: 43px;
        }
        .ButtonCommand
        {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <asp:HiddenField ID="hdnMethod" runat="server" />
    <asp:HiddenField ID="hdnSHKB" runat="server" />
    <asp:HiddenField ID="hdnMaNV" runat="server" />
    <asp:HiddenField ID="hdnSoBT" runat="server" />
    <asp:HiddenField ID="hdnSoCT" runat="server" />
    <asp:HiddenField ID="hdnKHCT" runat="server" />
    <asp:HiddenField ID="hdnNgayCT" runat="server" />
    <asp:HiddenField ID="hdnNgayKB" runat="server" />
    <asp:HiddenField ID="hdnTThai" runat="server" />
    <asp:HiddenField ID="hdnMaDThu" runat="server" />
    <asp:HiddenField ID="hdnMaNV_LapCT" runat="server" />
    <asp:HiddenField ID="hdnSHKB_CT" runat="server" />
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 10px;">
                
            </td>
            <td valign="top" align="center">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                        <tr valign="top">
                            <td align="center" valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td class="pageTitle">
                                                <span>KIỂM SOÁT BẢO LÃNH HẢI QUAN</span>
                                            </td>
                                        </tr>
                                        <tr align="left">
                                            <td class="errorMessage">
                                                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr align="left">
                                            <td style="color: Blue">
                                                <asp:Label ID="lblSuccess" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100%" align="center" valign="top" style="padding-left: 3; padding-right: 3">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <table width="100%" align="center">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="top" height="500px" class="style1">
                                                                                <div id="Div1" style="height: 50px; width: 250px;">
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <label>
                                                                                                    Số CT</label>:
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="txtSoBL_Search" runat="server" Width="150px"></asp:TextBox>
                                                                                            </td>
                                                                                            <td class="style2">
                                                                                                <asp:Button ID="btnSearch" runat="server" Text="Tìm" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                                <div id="ctl00_MstPg05_MainContent_Panel2" style="height: 450px; width: 250px;">
                                                                                    <table class="grid_data" cellspacing="0" rules="all" border="1" id="grdDSCT" style="width: 100%;
                                                                                        border-collapse: collapse;">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td colspan="3">
                                                                                                    <div id="divDSCT">
                                                                                                        <asp:DataGrid ID="dtgDSCT" runat="server" AutoGenerateColumns="False" TabIndex="9"
                                                                                                            Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                                                                                                            <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                                                                                                            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
                                                                                                            <ItemStyle CssClass="grid_item" />
                                                                                                            <Columns>
                                                                                                                <asp:BoundColumn DataField="TRANG_THAI" HeaderText="TT">
                                                                                                                    <HeaderStyle Width="10px"></HeaderStyle>
                                                                                                                    <ItemStyle HorizontalAlign="Center" Width="0"></ItemStyle>
                                                                                                                </asp:BoundColumn>
                                                                                                                <asp:TemplateColumn HeaderText="Số CT" HeaderStyle-Width="50px">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SO_CT") %>'
                                                                                                                            NavigateUrl='<%#"~/pages/BaoLanhHQ/frmKiemSoatBLHQ.aspx?SHKB=" & DataBinder.Eval(Container.DataItem, "SHKB") & "&SO_CT=" & DataBinder.Eval(Container.DataItem, "SO_CT") & _
                                                                                                                        "&SO_BT=" & DataBinder.Eval(Container.DataItem, "SO_BT") & "&TT=" & DataBinder.Eval(Container.DataItem, "TT") & "&NGAY_KB=" & DataBinder.Eval(Container.DataItem, "NGAY_KB") %>'>
                                                                                                                        </asp:HyperLink>
                                                                                                                    </ItemTemplate>
                                                                                                                    <HeaderStyle Width="50px"></HeaderStyle>
                                                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                                        Font-Underline="False" HorizontalAlign="Center" />
                                                                                                                </asp:TemplateColumn>
                                                                                                                <asp:TemplateColumn HeaderText="Tên ĐN/Số BL" HeaderStyle-Width="160px">
                                                                                                                    <ItemTemplate>
                                                                                                                        <asp:Label ID="lblMaNV" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TEN_DN")%>' />
                                                                                                                        <asp:Label ID="lblDelimiter" runat="server" Text='/' />
                                                                                                                        <asp:Label ID="lblSoBT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SO_BL")%>' />
                                                                                                                    </ItemTemplate>
                                                                                                                    <HeaderStyle Width="160px"></HeaderStyle>
                                                                                                                    <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                                                        Font-Underline="False" HorizontalAlign="Center" />
                                                                                                                </asp:TemplateColumn>
                                                                                                            </Columns>
                                                                                                            <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                                                                                                                Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                                                                                                                Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                                                                                                            </PagerStyle>
                                                                                                        </asp:DataGrid>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <table style="width: 100%">
                                                                                        <tr>
                                                                                            <td>
                                                                                                Tổng số
                                                                                            </td>
                                                                                            <td colspan="2" align="right" style="font-weight: bold">
                                                                                                <asp:Label ID="lblTongSo" runat="server" Text=""></asp:Label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                Trạng thái
                                                                                            </td>
                                                                                            <td colspan="3" align="right" style="width: 180px;">
                                                                                                <asp:DropDownList ID="drpTrangThai" runat="server" CssClass="inputflat" AutoPostBack="True"
                                                                                                    Width="110px">
                                                                                                    <asp:ListItem Value="99" Selected="True">-- Tất cả --</asp:ListItem>
                                                                                                    <asp:ListItem Value="05">Chờ kiểm soát</asp:ListItem>
                                                                                                    <asp:ListItem Value="01">Đã kiểm soát</asp:ListItem>
                                                                                                    <%--<asp:ListItem Value="06">Đã thanh toán</asp:ListItem>--%>
                                                                                                    <asp:ListItem Value="03">Chuyển trả</asp:ListItem>
                                                                                                    <asp:ListItem Value="02">Hủy bởi GDV - chờ duyệt</asp:ListItem>
                                                                                                    <asp:ListItem Value="04">Đã hủy bởi KSV</asp:ListItem>
                                                                                                    <asp:ListItem Value="07">Tu Chỉnh chờ kiểm soát</asp:ListItem>
                                                                                                </asp:DropDownList> 
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="3" align="right" style="display: none">
                                                                                                <asp:Button ID="btnRefresh" CssClass="ButtonCommand" runat="server" Text="Refresh" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="img">
                                                                                            <td align="left" style="width: 100%" colspan="5">
                                                                                                <hr style="width: 50%" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="img">
                                                                                            <td style="width: 100%" colspan="3">
                                                                                                <table>
                                                                                                    <tr>
                                                                                                        <td style="width: 10%">
                                                                                                            <img src="../../images/icons/ChuyenKS.png" />
                                                                                                        </td>
                                                                                                        <td style="width: 90%" colspan="2">
                                                                                                            Chờ Kiểm soát
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 10%">
                                                                                                            <img src="../../images/icons/ChuyenKsHS.gif" />
                                                                                                        </td>
                                                                                                        <td style="width: 90%" colspan="2">
                                                                                                            Tu chỉnh chờ Kiểm soát
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 10%">
                                                                                                            <img src="../../images/icons/DaKS.png" />
                                                                                                        </td>
                                                                                                        <td style="width: 90%" colspan="2">
                                                                                                            Đã Kiểm soát
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                   <%-- <tr style="display:none">
                                                                                                        <td style="width: 10%">
                                                                                                            <img src="../../images/icons/Da_TT.png" />
                                                                                                        </td>
                                                                                                        <td style="width: 90%" colspan="2">
                                                                                                            Đã thanh toán
                                                                                                        </td>
                                                                                                    </tr>--%>
                                                                                                     <tr>
                                                                                                        <td style="width: 10%">
                                                                                                            <img src="../../images/icons/HuyHS.gif" />
                                                                                                        </td>
                                                                                                        <td style="width: 90%" colspan="2">
                                                                                                            Hủy bởi GDV - chờ duyệt
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 10%">
                                                                                                            <img src="../../images/icons/KSLoi.png" />
                                                                                                        </td>
                                                                                                        <td style="width: 90%" colspan="2">
                                                                                                            Chuyển trả
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style="width: 10%">
                                                                                                            <img src="../../images/icons/Huy_KS.png" />
                                                                                                        </td>
                                                                                                        <td style="width: 90%" colspan="2">
                                                                                                            Chứng từ bị Hủy bởi KSV
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="img">
                                                                                            <td align="left" style="width: 100%" colspan="3">
                                                                                                <hr style="width: 50%" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr class="img">
                                                                                            <td style="width: 100%" colspan="3">
                                                                                                Alt + K : Kiểm soát<br />
                                                                                                Alt + C : Chuyển trả<br />
                                                                                                Alt + H : Hủy<br />
                                                                                                Alt + C : In thông báo
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </td>
                                                                            <td valign="top" width="630px" align="right">
                                                                                <asp:HiddenField ID="hdnTrangThaiTT" runat="server" />
                                                                                <table border="0" cellpadding="0" cellspacing="0" width="630px">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td colspan="2" width="100%" align="right">
                                                                                                <div id="Panel1" style="height: AUTO; width: 100%;">
                                                                                                    <table id="grdHeader" cellspacing="0" cellpadding="1" rules="all" border="1" style="border-width: 1px;
                                                                                                        border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse;">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td style="width: 30%; ">
                                                                                                                    <b>Loại Bảo lãnh </b>
                                                                                                                </td>
                                                                                                                <td style="width: 20%" colspan="2">
                                                                                                                    <asp:DropDownList ID="ddlLoaiBL" runat="server" Style="width: 100%" Enabled="False" CssClass="inputflat" AutoPostBack="True">
                                                                                                                        <asp:ListItem Value="31">Bảo lãnh tờ khai</asp:ListItem>
                                                                                                                        <asp:ListItem Value="32">Bảo lãnh vận đơn</asp:ListItem>
                                                                                                                        <asp:ListItem Value="33">Bảo lãnh chung</asp:ListItem>
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                                <td style="width: 50%" colspan="3">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr align="left" id="rowKHCT" runat="server">
                                                                                                                <td style="width: 30%; padding-left: 10px">
                                                                                                                    <b>KHCT</b>
                                                                                                                </td>
                                                                                                                <td style="width: 15%;">
                                                                                                                    <asp:TextBox ID="txtKHCT" runat="server" CssClass="inputflat" Style="width: 95%"
                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td style="width: 5%; padding-left: 10px;">
                                                                                                                    <b>Số CT</b>
                                                                                                                </td>
                                                                                                                <td style="width: 5%">
                                                                                                                    <asp:TextBox ID="txtSoCT" runat="server" CssClass="inputflat" Enabled="false"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td style="width: 20%;" align="left">
                                                                                                                    <b>Ngày lập</b>
                                                                                                                </td>
                                                                                                                <td style="width: 10%;">
                                                                                                                    <asp:TextBox ID="txtNgayLap" runat="server" CssClass="inputflat" Width="50%" Enabled="false"></asp:TextBox>&nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr align="left" id="rowUserMaker" runat="server">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Người lập</b>
                                                                                                                </td>
                                                                                                                <td style="width: 25%">
                                                                                                                    <asp:TextBox ID="txtNguoiLap" runat="server" CssClass="inputflat" Style="width: 95%"
                                                                                                                        Enabled="false"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Người KS</b>
                                                                                                                </td>
                                                                                                                <td style="width: 25%">
                                                                                                                    <asp:TextBox ID="txtNguoiKS" runat="server" CssClass="inputflat" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td style="width: 10%; visibility:hidden" align="left">
                                                                                                                    <b>T.Thái CT</b>
                                                                                                                </td>
                                                                                                                <td style=" visibility:hidden">
                                                                                                                    <asp:TextBox ID="txtTrangThai" runat="server" CssClass="inputflat" 
                                                                                                                        Font-Bold="True" ForeColor="#FF3300" ReadOnly="True"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr class="grid_header">
                                                                                                                <td align="left" colspan="6">
                                                                                                                    <div id="divTTBDS">
                                                                                                                        Thông tin về NNT</div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr align="left" id="rowSOBL" runat="server">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Số bảo lãnh</b> (<span style="color: Red">*</span>)
                                                                                                                </td>
                                                                                                                <td style="width: 25%" colspan="2">
                                                                                                                    <asp:TextBox ID="txtSoBaoLanh" runat="server" CssClass="inputflat" Style="width: 95%"
                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>T.Thái</b>
                                                                                                                </td>
                                                                                                               <td style="width: 25%" colspan="2">
                                                                                                                 <asp:TextBox ID="txtTrangThaiCT" runat="server" CssClass="inputflat" Width="95%"
                                                                                                                        Font-Bold="True" ForeColor="#FF3300" Enabled="false"></asp:TextBox>
                                                                                                                   
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            
                                                                                                            <tr align="left" id="rowNgayHL" runat="server">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Ngày hiệu lực BL</b> (<span style="color: Red">*</span>)
                                                                                                                </td>
                                                                                                                <td style="width: 25%" colspan="2">
                                                                                                                    <asp:TextBox ID="txtNgayHL" runat="server" CssClass="inputflat" Width="80%" Enabled="False"
                                                                                                                        MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                    <asp:HiddenField ID="hdnNgayHL" runat="server" />
                                                                                                                    <asp:HiddenField ID="hdnSoCTTuChinh" runat="server" />
                                                                                                                </td>
                                                                                                                <td style="width: 15%;">
                                                                                                                    <b>Ngày hết hạn BL</b> (<span style="color: Red">*</span>)
                                                                                                                </td>
                                                                                                                <td style="width: 25%" colspan="2">
                                                                                                                    <asp:TextBox ID="txtNgayHetHan" runat="server" CssClass="inputflat" Width="80%" Enabled="False"
                                                                                                                        MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                    <asp:HiddenField ID="hdnNgayHetHan" runat="server" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr align="left" id="rowSoNgayBL" runat="server">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Tổng số ngày BL</b>
                                                                                                                </td>
                                                                                                                <td style="width: 75%" colspan="5">
                                                                                                                    <asp:TextBox ID="txtTongSoNgay" runat="server" CssClass="inputflat" Style="width: 95%"
                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                    <asp:HiddenField ID="hdnTongSoNgay" runat="server" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr align="left" id="rowMST" runat="server">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Mã số thuế</b> (<span style="color: Red">*</span>)
                                                                                                                </td>
                                                                                                                <td style="width: 25%" colspan="2">
                                                                                                                    <asp:TextBox ID="txtMaNNT" runat="server" CssClass="inputflat_lookup" Style="background-color: Aqua;width: 95%"
                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                    <asp:HiddenField ID="hdnMaNNT" runat="server" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr id="rowTenNNT" runat="server">
                                                                                                                        <td style="width: 25%; padding-left: 10px" >
                                                                                                                            <b>Tên người NT</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 50%" colspan="5">
                                                                                                                            <asp:TextBox ID="txtTenNNT" runat="server" CssClass="inputflat" Style="width: 95%"
                                                                                                                        ReadOnly="true"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowMA_HQ_KB" runat="server">
                                                                                                                        <td style="width: 25%;padding-left: 10px">
                                                                                                                            <b>Mã HQ KB</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtMA_HQ_KB" runat="server" CssClass="inputflat" Style="width: 95%;
                                                                                                                                font-weight: bold" Enabled="False"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 50%" colspan="3">
                                                                                                                            <asp:TextBox ID="txtTenMA_HQ_KB" runat="server" CssClass="inputflat" Style="width: 85%" Enabled="False"></asp:TextBox>
                                                                                                                            &nbsp;
                                                                                                                            <img src="../../images/search.jpeg" width="14" height="14" onclick="ShowLov('MHQ')" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                            <tr id="rowHD" runat="server">
                                                                                                                        <td style="width: 25%;padding-left: 10px">
                                                                                                                            <b>Số hợp đồng</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtHDon" runat="server" CssClass="inputflat" MaxLength="50" Style="width: 97%;
                                                                                                                                font-weight: bold" Enabled="False"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày hđồng</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgay_HDon" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                MaxLength="10" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnImageNgay_HDon" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowVTD1" runat="server">
                                                                                                                        <td style="width: 25%;padding-left: 10px">
                                                                                                                            <b>Số VTĐ</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtVTD01" runat="server" CssClass="inputflat" MaxLength="15" Style="width: 97%;
                                                                                                                                font-weight: bold" Enabled="False"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày VTĐ</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgayVTD01" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                MaxLength="10" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnImageNgay_VTD01" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowVTD2" runat="server">
                                                                                                                        <td style="width: 25%;padding-left: 10px">
                                                                                                                            <b>Số VTĐ 02</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtVTD02" runat="server" CssClass="inputflat" MaxLength="15" Style="width: 97%;
                                                                                                                                font-weight: bold" Enabled="False"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày VTĐ 02</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgayVTD02" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                MaxLength="10" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnImageNgay_VTD02" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowVTD3" runat="server">
                                                                                                                        <td style="width: 25%;padding-left: 10px">
                                                                                                                            <b>Số VTĐ 03</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtVTD03" runat="server" CssClass="inputflat" MaxLength="15" Style="width: 97%;
                                                                                                                                font-weight: bold" Enabled="False"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày VTĐ 03</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgayVTD03" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                MaxLength="10" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnImageNgay_VTD03" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowVTD4" runat="server">
                                                                                                                        <td style="width: 25%;padding-left: 10px">
                                                                                                                            <b>Số VTĐ 04</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtVTD04" runat="server" CssClass="inputflat" MaxLength="15" Style="width: 97%;
                                                                                                                                font-weight: bold" Enabled="False"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày VTĐ 04</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgayVTD04" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                MaxLength="10" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnImageNgay_VTD04" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowVTD5" runat="server">
                                                                                                                        <td style="width: 25%;padding-left: 10px">
                                                                                                                            <b>Số VTĐ 05</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtVTD05" runat="server" CssClass="inputflat" MaxLength="15" Style="width: 97%;
                                                                                                                                font-weight: bold" Enabled="False"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày VTĐ 05</b> (<span style="color: Red">*</span>)
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgayVTD05" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                MaxLength="10" Enabled="False"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnImageNgay_VTD05" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr align="left" id="rowSOTK" runat="server">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Tờ khai số</b> (<span style="color: Red">*</span>)
                                                                                                                </td>
                                                                                                                <td style="width: 25%" colspan="2">
                                                                                                                    <asp:TextBox ID="txtToKhaiSo" runat="server" CssClass="inputflat_lookup" Style="width: 95%"
                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td style="width: 50%" colspan="4">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr align="left" id="rowNGAYDK" runat="server">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Ngày đăng ký</b> (<span style="color: Red">*</span>)
                                                                                                                </td>
                                                                                                                <td style="width: 25%" colspan="2">
                                                                                                                    <asp:TextBox ID="txtNgayDK" runat="server" CssClass="inputflat_lookup" Width="95%" Enabled="False"
                                                                                                                        MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                </td>
                                                                                                                <td style="width: 50%" colspan="4">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                                    <tr align="left" id="rowMAHQ" runat="server">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Mã HQ</b> (<span style="color: Red">*</span>)
                                                                                                                </td>
                                                                                                                <td style="width: 25%">
                                                                                                                    <asp:TextBox ID="txtMaHQ" runat="server" CssClass="inputflat" Enabled="False" Style="width: 95%"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td style="width: 50%" colspan="4">
                                                                                                                    <asp:TextBox ID="txtTenHQ" runat="server" CssClass="inputflat" Style="width: 85%"
                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            
                                                                                                            <tr align="left" id="rowLHXNK" runat="server">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Loại hình XNK</b> (<span style="color: Red">*</span>)
                                                                                                                </td>
                                                                                                                <td style="width: 25%">
                                                                                                                    <asp:TextBox ID="txtLHXNK" runat="server" CssClass="inputflat" Enabled="False" Style="width: 95%"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td style="width: 50%" colspan="4">
                                                                                                                    <asp:TextBox ID="txtDescLHXNK" runat="server" CssClass="inputflat" Enabled="False"
                                                                                                                        Style="width: 85%"></asp:TextBox>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr align="left" id="rowSOCIF" runat="server">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Số CIF</b>
                                                                                                                </td>
                                                                                                                <td style="width: 75%" colspan="5">
                                                                                                                    <asp:TextBox ID="txtCifNumber" runat="server" CssClass="inputflat" Style="width: 95%"
                                                                                                                        Enabled="false"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr align="left" id="rowAddress" runat="server">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Địa chỉ</b>
                                                                                                                </td>
                                                                                                                <td style="width: 75%" colspan="5">
                                                                                                                    <asp:TextBox ID="txtDiaChi" runat="server" CssClass="inputflat" Style="width: 95%"
                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                                    <tr id="rowFone_Num" runat="server">
                                                                                                                        <td style="width: 25%; padding-left: 10px">
                                                                                                                            <b>Số điện thoại</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtFone_Num" runat="server" CssClass="inputflat" Style="width: 85%" Enabled="false"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;"  >
                                                                                                                            <b>FAX</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txt_FAX_Num" runat="server" CssClass="inputflat" Style="width: 85%" Enabled="false"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowSo_DKKD" runat="server">
                                                                                                                        <td style="width: 25%; padding-left: 10px">
                                                                                                                            <b>Số đăng ký KD</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="2">
                                                                                                                            <asp:TextBox ID="txtSo_DKKD" runat="server" CssClass="inputflat" Style="width: 85%" Enabled="false"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;">
                                                                                                                            <b>Ngày Cấp</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%;" colspan="2">
                                                                                                                            <asp:TextBox ID="txtNgay_CQCap" runat="server" CssClass="inputflat" Width="80%" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                MaxLength="10" Enabled="false"></asp:TextBox>&nbsp;
                                                                                                                            <asp:Image ID="btnImageNgay_CQCap" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                    <tr id="rowCQCap" runat="server">
                                                                                                                        <td style="width: 25%; padding-left: 10px">
                                                                                                                            <b>Cơ quan cấp</b>
                                                                                                                        </td>
                                                                                                                        <td style="width: 25%" colspan="5">
                                                                                                                            <asp:TextBox ID="txtCoQCap" runat="server" CssClass="inputflat" Style="width: 85%" Enabled="false"></asp:TextBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                            <tr id="rowCapChuong" runat="server" align="left" style="visibility:hidden; display:none">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Mã Chương</b> (<span style="color: Red">*</span>)
                                                                                                                </td>
                                                                                                                <td style="width: 20%" colspan="2">
                                                                                                                    <asp:TextBox ID="txtCapChuong" runat="server" Enabled="False" CssClass="inputflat_lookup" Style="width: 95%;
                                                                                                                        background-color: Aqua; font-weight: bold" MaxLength="3" onkeypress="if (event.keyCode==13) ShowMLNS('MC')"></asp:TextBox>
                                                                                                                </td>
                                                                                                                
                                                                                                            </tr>
                                                                                                            
                                                                                                            <tr align="left" id="Tr8" runat="server">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Mã DV đại diện</b>
                                                                                                                </td>
                                                                                                                <td style="width: 75%" colspan="5">
                                                                                                                    <asp:TextBox ID="txtMA_DV_DD" runat="server" CssClass="inputflat" Style="width: 95%"
                                                                                                                        Enabled="false"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr align="left" id="Tr10" runat="server">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Tên DV đại diện</b>
                                                                                                                </td>
                                                                                                                <td style="width: 75%" colspan="5">
                                                                                                                    <asp:TextBox ID="txtTen_DV_DD" runat="server" CssClass="inputflat" Style="width: 95%"
                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <!--<tr>
                                                                                                            <td style="width: 25%; padding-left: 10px">
                                                                                                                <b>TK thu NS</b> (<span style="color: Red">*</span>)
                                                                                                            </td>
                                                                                                            <td style="width: 25%">
                                                                                                                <asp:TextBox ID="txtTK" runat="server" CssClass="inputflat" style="width: 98%" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td style="width: 50%" colspan="4">
                                                                                                                <asp:TextBox ID="txtTenTKNS" runat="server" CssClass="inputflat" 
                                                                                                                    style="width: 85%" Enabled="False"></asp:TextBox>
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>-->
                                                                                                            <tr align="left" id="rowCQTHU" runat="server" style="visibility:hidden; display:none">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>CQ Thu</b> (<span style="color: Red">*</span>)
                                                                                                                </td>
                                                                                                                <td style="width: 25%">
                                                                                                                    <asp:TextBox ID="txtMaCQT" runat="server" CssClass="inputflat" Enabled="False" Style="width: 95%"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td style="width: 50%" colspan="4">
                                                                                                                    <asp:TextBox ID="txtTenCQT" runat="server" CssClass="inputflat" Style="width: 85%"
                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                    &nbsp;
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr align="left" id="rowMAHQPH" runat="server" style="visibility:hidden; display:none">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Mã HQ PH</b> (<span style="color: Red">*</span>)
                                                                                                                </td>
                                                                                                                <td style="width: 25%">
                                                                                                                    <asp:TextBox ID="txtMaHQPH" runat="server" CssClass="inputflat" Style="width: 95%;
                                                                                                                        background-color: Aqua; font-weight: bold" Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                                <td style="width: 50%" colspan="4">
                                                                                                                    <asp:TextBox ID="txtTenMaHQPH" runat="server" CssClass="inputflat" Style="width: 85%"
                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                    &nbsp;
                                                                                                                    <img src="../../images/search.jpeg" width="14" height="14" onclick="ShowLov('MHQ')" />
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            
                                                                                                            
                                                                                                            <tr style="width: 100%;display:none">
                                                                                                                <td colspan="6">
                                                                                                                    <div id="divHD_NT" runat="server" style="width: 100%;display:none">
                                                                                                                        <table id="Table1" cellspacing="0" cellpadding="1" rules="all" border="1" style="border-width: 1px;
                                                                                                                            border-style: solid; font-family: Verdana; font-size: 8pt; width: 97%; border-collapse: collapse;">
                                                                                                                            <tr align="left" id="rowHD_NT" runat="server">
                                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                                    <b>Hợp đồng ngoại thương</b> (<span style="color: Red">*</span>)
                                                                                                                                </td>
                                                                                                                                <td style="width: 25%" colspan="2">
                                                                                                                                    <asp:TextBox ID="txtHD_NT" runat="server" CssClass="inputflat" Enabled="False" Style="width: 98%;
                                                                                                                                        font-weight: bold" MaxLength="50"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td style="width: 15%;">
                                                                                                                                    <b>Ngày hợp đồng</b> (<span style="color: Red">*</span>)
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtNgay_HD" runat="server" CssClass="inputflat" Width="80%" Enabled="False"
                                                                                                                                        onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                        onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                        MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                                    <asp:Image ID="btnImageNgay_HD" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                        ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            
                                                                                                                            <tr align="left" id="rowVTD" runat="server">
                                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                                    <b>Vận tải đơn</b> (<span style="color: Red">*</span>)
                                                                                                                                </td>
                                                                                                                                <td style="width: 25%" colspan="2">
                                                                                                                                    <asp:TextBox ID="txtVTD" runat="server" CssClass="inputflat" Enabled="False" Style="width: 98%;
                                                                                                                                        font-weight: bold"></asp:TextBox>
                                                                                                                                </td>
                                                                                                                                <td style="width: 15%;">
                                                                                                                                    <b>Ngày vận tải đơn</b> (<span style="color: Red">*</span>)
                                                                                                                                </td>
                                                                                                                                <td>
                                                                                                                                    <asp:TextBox ID="txtNgay_VTD" runat="server" CssClass="inputflat" Width="80%" Enabled="False"
                                                                                                                                        onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                                                                                                        onblur="CheckDate(this); javascript:datediff()" onfocus="javascript:datediff()"
                                                                                                                                        MaxLength="10"></asp:TextBox>&nbsp;
                                                                                                                                    <asp:Image ID="btnImageNgay_VTD" ImageAlign="AbsMiddle" runat="server" onclick="javascript:datediff()"
                                                                                                                                        ImageUrl="~/images/cal.gif" TabIndex="20" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr align="left" id="rowLoaiTien" runat="server">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Loại tiền HQ</b> (<span style="color: Red">*</span>)
                                                                                                                </td>
                                                                                                                <td style="width: 25%" colspan="2">
                                                                                                                    <asp:DropDownList ID="drpLoaiTien" runat="server" Style="width: 100%" CssClass="inputflat"
                                                                                                                        Enabled="False">
                                                                                                                        <%--<asp:ListItem Value="">-----</asp:ListItem>--%>
                                                                                                                        <%--<asp:ListItem Value="1">Thuế</asp:ListItem>--%>
                                                                                                                        <%--<asp:ListItem Value="1">VND</asp:ListItem>
                                                                                                                        <asp:ListItem Value="2">Truy thu thuế</asp:ListItem>
                                                                                                                        <asp:ListItem Value="3">Phạt chậm nộp thuế</asp:ListItem>
                                                                                                                        <asp:ListItem Value="4">Lệ phí HQ</asp:ListItem>
                                                                                                                        <asp:ListItem Value="5">Phạt vi phạm hành chính</asp:ListItem>--%>
                                                                                                                    </asp:DropDownList>
                                                                                                                </td>
                                                                                                                <td style="width: 50%" colspan="4">
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr align="left">
                                                                                                                <td style="width: 25%; padding-left: 10px">
                                                                                                                    <b>Diễn giải</b> (<span style="color: Red">*</span>)
                                                                                                                </td>
                                                                                                                <td style="width: 75%" colspan="5">
                                                                                                                    <asp:TextBox ID="txtDienGiai" runat="server" CssClass="inputflat" Style="width: 80%"
                                                                                                                        Enabled="False"></asp:TextBox>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" height="2" align="left">
                                                                                                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                                                                                    <ProgressTemplate>
                                                                                                        <div style="background-color: Aqua;">
                                                                                                            <b style="font-size: 15pt">Đang lấy dữ liệu tại server.Xin chờ một lát...</b>
                                                                                                        </div>
                                                                                                    </ProgressTemplate>
                                                                                                </asp:UpdateProgress>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr style="width: 99%;display:none">
                                                                                            <td colspan="2" align="right">
                                                                                                <table width="100%" border="1">
                                                                                                    <tr class="grid_header">
                                                                                                        <td align="left" style="width: 92%">
                                                                                                            Thông tin chi tiết (<span style="color: Red">*</span>)
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr style="width: 99%;display:none">
                                                                                            <td colspan="2" width="99%">
                                                                                                <table class="grid_data" cellspacing="0" rules="all" style="border-style: solid;
                                                                                                    border-color: Black; border-collapse: collapse;">
                                                                                                    <tbody>
                                                                                                        <tr class="grid_header">
                                                                                                            <td align="center" style="width: 10%">
                                                                                                                Chương
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                Khoản
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 10%">
                                                                                                                Tiểu Mục
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 60%">
                                                                                                                Nội dung
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 20%">
                                                                                                                Tiền
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                Kỳ thuế
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                KHTK
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="rowGridDetail1" class="grid_item">
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtMaQuy_1" runat="server" CssClass="inputflat" MaxLength="3" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtMaChuong_1" runat="server" CssClass="inputflat" MaxLength="3"
                                                                                                                    Enabled="False" Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtNDKT_1" runat="server" CssClass="inputflat" MaxLength="4" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtNoiDung_1" runat="server" CssClass="inputflat" ReadOnly="true"
                                                                                                                    Style="width: 99%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtTien_1" runat="server" CssClass="inputflat" Enabled="False" onfocus="this.select()"
                                                                                                                    onkeyup="valInteger(this)" MaxLength="17" onblur="jsCalTotal();" Style="width: 90%;
                                                                                                                    border-color: White; font-weight: bold; text-align: right;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtKyThue_1" runat="server" CssClass="inputflat" MaxLength="7" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtKHTK_1" runat="server" CssClass="inputflat" MaxLength="5" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="Tr1" class="grid_item">
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtMaQuy_2" runat="server" CssClass="inputflat" MaxLength="3" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtMaChuong_2" runat="server" CssClass="inputflat" MaxLength="3"
                                                                                                                    Enabled="False" Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtNDKT_2" runat="server" CssClass="inputflat" MaxLength="4" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtNoiDung_2" runat="server" CssClass="inputflat" ReadOnly="true"
                                                                                                                    Style="width: 99%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtTien_2" runat="server" CssClass="inputflat" Enabled="False" Style="width: 90%;
                                                                                                                    border-color: White; font-weight: bold; text-align: right;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtKyThue_2" runat="server" CssClass="inputflat" MaxLength="7" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtKHTK_2" runat="server" CssClass="inputflat" MaxLength="5" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="Tr2" class="grid_item">
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtMaQuy_3" runat="server" CssClass="inputflat" MaxLength="3" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtMaChuong_3" runat="server" CssClass="inputflat" MaxLength="3"
                                                                                                                    Enabled="False" Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtNDKT_3" runat="server" CssClass="inputflat" MaxLength="4" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtNoiDung_3" runat="server" CssClass="inputflat" ReadOnly="true"
                                                                                                                    Style="width: 99%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtTien_3" runat="server" CssClass="inputflat" Enabled="False" Style="width: 90%;
                                                                                                                    border-color: White; font-weight: bold; text-align: right;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtKyThue_3" runat="server" CssClass="inputflat" MaxLength="7" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtKHTK_3" runat="server" CssClass="inputflat" MaxLength="5" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="Tr3" class="grid_item">
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtMaQuy_4" runat="server" CssClass="inputflat" MaxLength="3" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtMaChuong_4" runat="server" CssClass="inputflat" MaxLength="3"
                                                                                                                    Enabled="False" Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtNDKT_4" runat="server" CssClass="inputflat" MaxLength="4" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtNoiDung_4" runat="server" CssClass="inputflat" ReadOnly="true"
                                                                                                                    Style="width: 99%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtTien_4" runat="server" CssClass="inputflat" Enabled="False" Style="width: 90%;
                                                                                                                    border-color: White; font-weight: bold; text-align: right;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtKyThue_4" runat="server" CssClass="inputflat" MaxLength="7" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtKHTK_4" runat="server" CssClass="inputflat" MaxLength="5" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="Tr4" class="grid_item">
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtMaQuy_5" runat="server" CssClass="inputflat" MaxLength="3" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtMaChuong_5" runat="server" CssClass="inputflat" MaxLength="3"
                                                                                                                    Enabled="False" Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtNDKT_5" runat="server" CssClass="inputflat" MaxLength="4" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtNoiDung_5" runat="server" CssClass="inputflat" ReadOnly="true"
                                                                                                                    Style="width: 99%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtTien_5" runat="server" CssClass="inputflat" Enabled="False" Style="width: 90%;
                                                                                                                    border-color: White; font-weight: bold; text-align: right;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtKyThue_5" runat="server" CssClass="inputflat" MaxLength="7" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtKHTK_5" runat="server" CssClass="inputflat" MaxLength="5" Enabled="False"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="Tr5" class="grid_item">
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtMaQuy_6" runat="server" CssClass="inputflat" MaxLength="3" onkeypress="if (event.keyCode==13) ShowMLNS('MQ_05')"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"
                                                                                                                    Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtMaChuong_6" runat="server" CssClass="inputflat" MaxLength="3"
                                                                                                                    onkeypress="if (event.keyCode==13) ShowMLNS('MC_05')" Style="width: 90%; border-color: White;
                                                                                                                    font-weight: bold; text-align: center;" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtNDKT_6" runat="server" CssClass="inputflat" MaxLength="4" onkeypress="if (event.keyCode==13) ShowMLNS('MM_05')"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"
                                                                                                                    Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtNoiDung_6" runat="server" CssClass="inputflat" Style="width: 99%;
                                                                                                                    border-color: White; font-weight: bold; text-align: left;" ReadOnly="true"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtTien_6" runat="server" CssClass="inputflat" onfocus="this.select()"
                                                                                                                    onkeyup="valInteger(this)" MaxLength="17" onblur="jsCalTotal();" Style="width: 90%;
                                                                                                                    border-color: White; font-weight: bold; text-align: right;" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtKyThue_6" runat="server" CssClass="inputflat" MaxLength="7" onblur="javascript:CheckMonthYear(this)"
                                                                                                                    onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2','/');}"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"
                                                                                                                    Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtKHTK_6" runat="server" CssClass="inputflat" MaxLength="5" Style="width: 90%;
                                                                                                                    border-color: White; font-weight: bold; text-align: center;" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr id="Tr6" class="grid_item">
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtMaQuy_7" runat="server" CssClass="inputflat" MaxLength="3" onkeypress="if (event.keyCode==13) ShowMLNS('MQ_05')"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"
                                                                                                                    Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtMaChuong_7" runat="server" CssClass="inputflat" MaxLength="3"
                                                                                                                    onkeypress="if (event.keyCode==13) ShowMLNS('MC_05')" Style="width: 90%; border-color: White;
                                                                                                                    font-weight: bold; text-align: center;" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtNDKT_7" runat="server" CssClass="inputflat" MaxLength="4" onkeypress="if (event.keyCode==13) ShowMLNS('MM_05')"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"
                                                                                                                    Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtNoiDung_7" runat="server" CssClass="inputflat" Style="width: 99%;
                                                                                                                    border-color: White; font-weight: bold; text-align: left;" ReadOnly="true"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center">
                                                                                                                <asp:TextBox ID="txtTien_7" runat="server" CssClass="inputflat" onfocus="this.select()"
                                                                                                                    onkeyup="valInteger(this)" MaxLength="17" onblur="jsCalTotal();" Style="width: 90%;
                                                                                                                    border-color: White; font-weight: bold; text-align: right;" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtKyThue_7" runat="server" CssClass="inputflat" MaxLength="7" onblur="javascript:CheckMonthYear(this)"
                                                                                                                    onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2','/');}"
                                                                                                                    Style="width: 90%; border-color: White; font-weight: bold; text-align: center;"
                                                                                                                    Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                            <td align="center" style="width: 0%; display: none">
                                                                                                                <asp:TextBox ID="txtKHTK_7" runat="server" CssClass="inputflat" MaxLength="5" Style="width: 90%;
                                                                                                                    border-color: White; font-weight: bold; text-align: center;" Enabled="False"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" height="5">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" valign="top" style="width: 300px">
                                                                                                Chế độ làm việc :
                                                                                                <asp:Label ID="lblStatus" runat="server" Text="" Style="color: blue"></asp:Label>
                                                                                            </td>
                                                                                            <td align="right">
                                                                                                <table width="300px">
                                                                                                    <tbody>
                                                                                                        <tr align="right">
                                                                                                            <td align="right" valign="top">
                                                                                                                Tổng Tiền bảo lãnh: &nbsp;
                                                                                                                <asp:TextBox ID="txtTongTien" runat="server" CssClass="inputflat" Enabled="false"
                                                                                                                    Style="width: 100px; text-align: right; font-weight: bold"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr align="right">
                                                                                                            <td align="right" valign="top">
                                                                                                                Tổng tiền truy vấn tờ khai: &nbsp;
                                                                                                                <asp:TextBox ID="txtTT_TTHU" runat="server" CssClass="inputflat" Enabled="false"
                                                                                                                    Style="width: 100px; text-align: right; font-weight: bold"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr align="right">
                                                                                                            <td align="right" valign="top">
                                                                                                                Tổng tiền bảo lãnh trong Core: &nbsp;
                                                                                                                <asp:TextBox ID="txtTS_TienBL_T24" runat="server" CssClass="inputflat" Enabled="false"
                                                                                                                    Style="width: 100px; text-align: right; font-weight: bold"></asp:TextBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr align="right">
                                                                                                            <td align="right" valign="top">
                                                                                                                Lý do hủy: &nbsp;
                                                                                                                <asp:DropDownList ID="drpLyDoHuy" runat="server" Style="width: 60%" CssClass="inputflat">
                                                                                                                    <asp:ListItem Value="0">Không có trong dữ liệu HQ/NH </asp:ListItem>
                                                                                                                    <asp:ListItem Value="2">Điện thừa </asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr align="right">
                                                                                                            <td align="Left" valign="top">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td colspan="2" height="5" align="right">
                                                                                                Lý do chuyển trả(<span style="color: Red">*</span>)
                                                                                                <asp:TextBox ID="txtLyDoCTra" runat="server" ForeColor="Red" Width="500px"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                        <td style="width:25%"></td>
                                                                            <td  height="5" align="right" style="display:none">
                                                                                Mật khẩu Kiểm soát <span class="requiredField">(*)</span>:
                                                                                <asp:TextBox runat="server" ID="txtMatKhau" Width="100px" Text="" TextMode="Password"
                                                                                    Style="height: 30; background-color: Silver" />
                                                                            </td>
                                                                            <td  align="right" valign="top" colspan="1">
                                                                                <asp:Button ID="btnKiemSoat" runat="server" Text="(K)iểm soát" AccessKey="K" CssClass="ButtonCommand" />&nbsp;
                                                                                <asp:Button ID="btnChuyenTra" runat="server" Text="(C)huyển trả" AccessKey="C" CssClass="ButtonCommand" />&nbsp;
                                                                                <asp:Button ID="btnGuiLai" runat="server" Text="(G)ửi lại" AccessKey="G" Visible="false"
                                                                                    CssClass="ButtonCommand" />&nbsp;
                                                                                <asp:Button ID="btnHuy" runat="server" Text=" Duyệt(H)ủyBL" CssClass="ButtonCommand"
                                                                                    AccessKey="H" />&nbsp;
                                                                                <asp:Button ID="cmdThanhToan" runat="server" Text="(T)hanh toán" Enabled="false"  AccessKey="T"
                                                                    CssClass="ButtonCommand"   UseSubmitBehavior="false" Visible="false"/>
                                                                                <asp:Button ID="cmdIn" runat="server" Text="(I)n Thư BL " AccessKey="I" Enabled="false"
                                                                                    CssClass="ButtonCommand" />
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="height: 5">
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td style="width: 10px;">
               
            </td>
        </tr>
    </table>
</asp:Content>
