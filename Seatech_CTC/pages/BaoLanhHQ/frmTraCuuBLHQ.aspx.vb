﻿Imports Business
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports System.Diagnostics
Imports System.Xml.XmlDocument
Imports Business.NewChungTu
Imports System.Xml
Imports Business.BaoCao

Partial Class pages_ChungTu_frmTraCuuBLHQ
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler btnSearch.Click, AddressOf btnSearch_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                LoadForm()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub LoadForm()
        txtTunNgayBL.Text = DateTime.Now.ToString("dd/MM/yyyy")
        txtDenNgayBL.Text = DateTime.Now.ToString("dd/MM/yyyy")
        'txtNgayKS.Text = DateTime.Now.ToString("dd/MM/yyyy")
        txtNgayLap.Text = DateTime.Now.ToString("dd/MM/yyyy")
        clsCommon.addPopUpCalendarToImage(btnImageTuNgayBL, txtTunNgayBL, "dd/mm/yyyy")
        clsCommon.addPopUpCalendarToImage(btnImageDenNgayBL, txtDenNgayBL, "dd/mm/yyyy")
        'clsCommon.addPopUpCalendarToImage(btnImageNgayKS, txtNgayKS, "dd/mm/yyyy")
        clsCommon.addPopUpCalendarToImage(btnImageNgayLap, txtNgayLap, "dd/mm/yyyy")
        DM_DiemThu()
        'getChiNhanh()

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim strSoBL As String = txtSoBL.Text.Trim
        Dim strSoCT As String = txtSoCTu.Text.Trim
        Dim strSoTKhai As String = txtSoTKhai.Text.Trim
        Dim strTrangThai As String = drpTrangThai.SelectedValue.Trim
        Dim strTuNgayBL As String = txtTunNgayBL.Text.Trim
        Dim strDenNgayBL As String = txtDenNgayBL.Text.Trim
        Dim strSoChiNhanh As String = txtMaCN.SelectedValue.ToString()
        Dim strSoTien As String = txtSoTien.Text.Trim.Replace(".", "")
        Dim strNguoiLap As String = txtMaNV.Text.Trim
        Dim strTTTruyenHQ As String = drpTruyenHQ.SelectedValue.Trim
        Dim strLoaiBCao As String = drpLoaiBC.SelectedValue.Trim
        Dim strNgayKS As String = txtNgayKS.Text.Trim
        Dim strNgayLap As String = txtNgayLap.Text.Trim
        Dim strCif As String = txtCif.Text.Trim
        Dim strPhanCap As String = ""
        Dim strPara As String = ""
        Dim SPLIT As String = "zzz"
        'If strSoChiNhanh.Length = 3 Then
        '    strSoChiNhanh = strSoChiNhanh & "00"
        'End If
        If txtMaCN.SelectedValue.ToString() = "" Then
            strPhanCap = checkHSC()
        Else
            strPhanCap = checkHSC(txtMaCN.SelectedValue.ToString())
        End If
        If strTuNgayBL <> "" Then
            strTuNgayBL = ConvertDateToNumber(strTuNgayBL)
        End If
        If strDenNgayBL <> "" Then
            strDenNgayBL = ConvertDateToNumber(strDenNgayBL)
        End If
        If strTuNgayBL <> "" And strDenNgayBL <> "" Then
            If (strTuNgayBL > strDenNgayBL) Then
                clsCommon.ShowMessageBox(Me, "Từ ngày không được lớn hơn Đến ngày. Bạn hãy chọn lại.")
                txtTunNgayBL.Focus()
                Return
            End If
        End If
        'If txtSoBL.Text.ToString() = "" Then

        '    clsCommon.ShowMessageBox(Me, "Số bảo lãnh yêu cầu 16 ký tự")
        '    txtSoBL.Focus()
        '    Return

        'End If
        'If txtCif.Text.ToString() <> "" Then
        '    If txtCif.Text.Length < 10 Or txtCif.Text.Length > txtCif.MaxLength Then
        '        clsCommon.ShowMessageBox(Me, "Số Cif phải nhập trong khoảng 10 đến 16 ký tự")
        '        txtCif.Focus()
        '        Return
        '    End If
        'End If

        strPara = strSoBL & SPLIT & strSoCT & SPLIT & strSoTKhai & SPLIT & strTrangThai & SPLIT & _
        strTuNgayBL & SPLIT & strDenNgayBL & SPLIT & strSoChiNhanh & SPLIT & strSoTien & SPLIT & strNguoiLap & SPLIT & _
        strTTTruyenHQ & SPLIT & strLoaiBCao & SPLIT & strNgayKS & SPLIT & strNgayLap & SPLIT & strCif & SPLIT & strPhanCap

        Response.Redirect("frmKetQuaTraCuuBLHQ.aspx?para=" & strPara)

    End Sub

    Private Sub getChiNhanh()
        Dim dt As New DataTable
        Dim strSQL As String
        Dim strMaCN As String = ""
        Dim strTenCN As String = ""
        Dim strUser As String = Session.Item("User").ToString

        Try
            strSQL = "select a.ma_cn, b.name ten_cn from tcs_dm_nhanvien a, tcs_dm_chinhanh b where (1=1) " & _
                    "AND a.ma_cn = b. id " & _
                    "AND ma_nv = " & CInt(strUser)
            '"AND MA_NHOM = '02'"

            dt = DatabaseHelp.ExecuteToTable(strSQL)

            If Not dt Is Nothing And dt.Rows.Count > 0 Then
                strMaCN = dt.Rows(0)("MA_CN").ToString
                strTenCN = dt.Rows(0)("TEN_CN").ToString
            End If
            
            If strMaCN.Trim.Substring(0, 3) <> "011" Then
                txtMaCN.Text = strMaCN
                txtTenCN.Text = strTenCN
                txtMaCN.Enabled = False
                'Img1.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function
    Private Function checkHSC(ByVal strMa_CN As String) As String
        Dim returnValue = ""
        'returnValue = buBaoCao.checkHSC_MaCN(strMa_CN)
        Return returnValue
    End Function
    Private Sub DM_DiemThu()

        Dim dsDiemThu As DataSet
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        strcheckHSC = checkHSC()
        If strcheckHSC = "1" Then
            strSQL = "SELECT a.id, a.id ||'-'||a.name nam FROM tcs_dm_chinhanh a  "
        ElseIf strcheckHSC = "2" Then
            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        Else
            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        End If
        Try
            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(txtMaCN, dsDiemThu, 1, 0, "Tất cả")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            Throw ex
        Finally

        End Try
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenCNNH(ByVal pv_strMaCN As String) As String

        If Not pv_strMaCN Is Nothing Then
            Return clsCTU.TCS_GetTenCNNH(pv_strMaCN.Substring(0, 3))
        Else
            Return ""
        End If

    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenNV(ByVal pv_strMaNV As String) As String
        If Not pv_strMaNV Is Nothing Then
            Return clsCTU.TCS_GetTenNV(pv_strMaNV)
        Else
            Return ""
        End If
    End Function
End Class
