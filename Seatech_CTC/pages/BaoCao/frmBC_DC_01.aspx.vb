﻿Imports Business
Imports VBOracleLib
Imports System.IO
Imports System.Data
Imports ETAX_GDT

Partial Class pages_BaoCao_frmBC_DC_01
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Request.Params("ngay_dc") Is Nothing Then
            If Not Request.Params("ma_gd_dc") Is Nothing Then
                Dim ngay_dc As String = Request.Params("ngay_dc")
                Dim ma_gd_dc As String = Request.Params("ma_gd_dc")

                lblNgayDC.Text = "Ngày " & ngay_dc.Substring(6, 2) & " tháng " & ngay_dc.Substring(4, 2) & " năm " & ngay_dc.Substring(0, 4)
                LoadData(ngay_dc, ma_gd_dc)
            End If
        End If
    End Sub
    Private Sub LoadData(ByVal strNgay_Dc As String, ByVal ma_gd_dc As String)
        Try
            'Lấy header
            Dim sql As String = "Select * from NTDT_BAOCAO_HDR WHERE MA_BCAO='01DC-NHTM-NTDT' AND TO_CHAR(NGAY_DC,'yyyyMMdd')=" & strNgay_Dc & " AND ma_gdich_dc = '" + ma_gd_dc + "' order by id_bcao desc"
            Dim dt As DataTable = DataAccess.ExecuteToTable(sql)
            Dim dt_detail As New DataTable
            If dt.Rows.Count > 0 Then

                lblSum_TC.Text = String.Format("{0:n0}", dt.Rows(0)("SODIEN_TCONG"))
                lblSum_TC_Amount_VND.Text = String.Format("{0:n0}", dt.Rows(0)("tongtien_tcong_vnd"))
                lblSum_TC_Amount_USD.Text = String.Format("{0:0,0.00}", dt.Rows(0)("tongtien_tcong_usd"))
                lblTuNgay.Text = convertDayofYear(dt.Rows(0)("TU_NGAY_DC"))
                lblDenNgay.Text = convertDayofYear(dt.Rows(0)("DEN_NGAY_DC"))
                lblGioDC.Text = convertDayofYear(dt.Rows(0)("NGAY_GDICH_DC"))
                'Lấy detail
                Dim sql_detail As String = "Select r1.*,NVL(r1.MA_NGUYENTE, 'VND') as MA_NGUYENTE_R1, ct.so_ct_nh as ref_no from NTDT_BAOCAO_CTIET_R1 r1, tcs_ctu_ntdt_hdr ct WHERE r1.so_ctu = ct.so_ctu and ID_BCAO=" & dt.Rows(0)("ID_BCAO") & " order by r1.so_ctu"
                dt_detail = DataAccess.ExecuteReturnDataSet(sql_detail, CommandType.Text).Tables(0)
            End If
            grdChungTu.DataSource = dt_detail
            grdChungTu.DataBind()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnKetXuat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKetXuat.Click
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=BC_DC_01.xls")
        Response.Charset = "utf-8"
        Response.ContentType = "application/vnd.ms-excel"
        Using sw1 As New StringWriter()
            Dim hw1 As New HtmlTextWriter(sw1)
            tableBaoCao.RenderControl(hw1)
            Response.Output.Write(sw1.ToString)
            Response.Flush()
            Response.End()
        End Using
    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        ' Verifies that the control is rendered
    End Sub
    Private Function convertDayofYear(ByVal strInput As String) As String
        If strInput.Length < 13 Then
            Return ""
        End If
        Dim year As Integer = 0
        Dim dayOfYear As Integer = 0
        year = Integer.Parse(strInput.Substring(0, 4))
        dayOfYear = Integer.Parse(strInput.Substring(4, 3))
        Dim hh As String = strInput.Substring(7, 2)
        Dim mm As String = strInput.Substring(9, 2)
        Dim ss As String = strInput.Substring(11, 2)
        Dim ngay As String = ""
        Dim theDate As DateTime = New DateTime(year, 1, 1).AddDays(dayOfYear - 1)
        ngay = hh + ":" + mm + ":" + ss + " ngày " + theDate.ToString("dd/MM/yyyy")
        Return ngay
    End Function

End Class
