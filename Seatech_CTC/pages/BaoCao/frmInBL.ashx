﻿<%@ WebHandler Language="VB" Class="frmInBL" %>

Imports System
Imports System.Web
Imports System.Web.HttpRequest
Imports System.IO
Imports Business
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports Business.NewChungTu
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine


Public Class frmInBL : Implements IHttpHandler, System.Web.SessionState.IReadOnlySessionState
    Private _mvStrShkb As String
    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenKB As String
    Private mv_strTenTinhTP As String
    Private mv_strMaTinhTP As String
    Private mv_strSHKBDEF As String
    Private pdvTCS As New CrystalDecisions.Shared.ParameterDiscreteValue
    Private rptTCS As New ReportDocument
    Private pcTCS As New CrystalDecisions.Shared.ParameterValues
    'Private Shared strTT_TIENMAT As String = ConfigurationManager.AppSettings.Get("TK_TIENMAT").ToString()
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim key As New KeyCTu
        Dim isBS As String = Nothing
        Dim so_ct As String = ""
        Dim ref As String = ""
        If Not context.Request.QueryString.Item("so_ct") Is Nothing Then
            so_ct = Business.CTuCommon.SQLInj(context.Request.QueryString.Item("so_ct").ToString)
        End If
        
       
        'Dim sMaCN As String = "'4.90','4.91','4.31','4.32','4.82'"
        'If Business.CTuCommon.checkPhanQuyenInBL(HttpContext.Current.Session("User"), sMaCN) = False Then
        '    Exit Sub
        'End If
      
        Try
            Dim ds As DataSet = Nothing
            Dim objCTU As Business.BienLai.infoCTUBIENLAI = Nothing
            objCTU = Business.BienLai.buBienLai.GetCTUDetail(so_ct)
            ds = Business.BienLai.buBienLai.CTU_IN_BL_NEW(so_ct)
            
            
            Dim v_strMa_CN_Uer As String = clsCTU.TCS_GetMaChiNhanh(objCTU.MA_NV)
            Dim v_strBranchCode As String = clsCTU.TCS_GetMaCN_NH(v_strMa_CN_Uer)
            Dim v_strNameCN As String = clsCTU.TCS_GetTenCNNH(v_strMa_CN_Uer)
           
            Dim strFileName As String = ""
            Dim v_strReportName As String = "In chứng từ thu ngân sách"

            strFileName = context.Server.MapPath("RPT\CT_BL_NEW.rpt")

            If (Not File.Exists(strFileName)) Then
            End If
            rptTCS.Load(strFileName)
            ds.Tables(0).TableName = "BL"

            rptTCS.SetDataSource(ds)
            Dim mCopy As String = ""
            isBS = "1"
            If Not isBS Is Nothing Then
                If isBS.Equals("1") Then
                    mCopy = "Bản Chính"
                Else
                    mCopy = "Bản Sao"
                End If
            End If
            AddParam("Copy", mCopy)
            If objCTU.MA_NT.Equals("VND") Then
                Dim Tien_Bang_Chu As String = TCS_Dich_So(objCTU.TTIEN, "đồng")
                AddParam("Tien_Bang_Chu", Tien_Bang_Chu)
            End If
            AddParam("LyDo_Phat_VPHC", clsCommon.gf_CorrectString(objCTU.LY_DO))
            AddParam("TEN_NNTHUE", clsCommon.gf_CorrectString(objCTU.TEN_NNTHUE))
            AddParam("TEN_NNTIEN", clsCommon.gf_CorrectString(objCTU.TEN_NNTIEN))
            AddParam("MA_NNTHUE", clsCommon.gf_CorrectString(objCTU.MA_NNTHUE))
            AddParam("DC_NNTHUE", clsCommon.gf_CorrectString(objCTU.DC_NNTIEN))

            Dim strTenTinhTP As String = ""
            'If hdr.TEN_HUYEN_NNTHUE <> "" Then
            '    AddParam("HUYEN_NNTHUE", clsCommon.gf_CorrectString(hdr.TEN_HUYEN_NNTHUE))
            'Else
            '    AddParam("HUYEN_NNTHUE", "")
            'End If
            'If hdr.TEN_TINH_NNTHUE <> "" Then
            '    AddParam("TINH_NNTHUE", clsCommon.gf_CorrectString(hdr.TEN_TINH_NNTHUE))
            'Else
            '    AddParam("TINH_NNTHUE", "")
            'End If

            Dim ten_cqthu As String = Get_TenCQThu(clsCommon.gf_CorrectString(objCTU.MA_CQTHU))
            Dim ten_kb As String = Get_TenKhoBac(objCTU.SHKB)

            AddParam("HUYEN_NNTHUE", "")
            AddParam("TINH_NNTHUE", "")
            AddParam("SO_QD", clsCommon.gf_CorrectString(objCTU.SO_QD))
            AddParam("NGAY_QD", clsCommon.gf_CorrectString(objCTU.NGAY_QD))

            AddParam("TEN_CQQD", clsCommon.gf_CorrectString(objCTU.TEN_CQQD))
            AddParam("TEN_KB", ten_kb)
            AddParam("TEN_CQTHU", ten_cqthu)
            AddParam("TEN_LH_THU", objCTU.TEN_LHTHU)
            AddParam("SO_BIENLAI", clsCommon.gf_CorrectString(objCTU.SOBL))

            AddParam("SO_BL", clsCommon.gf_CorrectString(objCTU.SOBL))
            AddParam("SO_CT_NH", clsCommon.gf_CorrectString(objCTU.REF_CORE_TTSP))
            AddParam("KH_BL", clsCommon.gf_CorrectString(objCTU.KYHIEU_BL))
            AddParam("IsTamThu", clsCommon.gf_CorrectString(objCTU.LOAIBIENLAI))
            Try
                rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
                rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
                'rptTCS.ExportToHttpResponse(ExportFormatType.PortableDocFormat, context.Response, False, "GiaynoptienNT")
                Try
                    'rptTCS.Load(Server.MapPath(@"MyReport.rpt"))
                    Dim oStream As System.IO.Stream
                    Dim byteArray As Byte() = New Byte() {}
                    oStream = rptTCS.ExportToStream(ExportFormatType.PortableDocFormat)
                    byteArray = New Byte(oStream.Length) {}
                    oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1))
                    context.Response.ClearContent()
                    context.Response.ClearHeaders()
                    context.Response.ContentType = "application/pdf"
                    
                    context.Response.BinaryWrite(byteArray)
                    context.Response.Flush()
                    context.Response.Close()
                Catch ex As Exception
                    LogDebug.WriteLog("1.Exception: " & ex.Message & " \n " & ex.StackTrace, Diagnostics.EventLogEntryType.Error)
                End Try
            Catch ex As Exception

            Finally
                rptTCS.Close()
                rptTCS.Dispose()
            End Try
        Catch ex As Exception
            log.Error(ex.Message + ex.StackTrace)
            Dim xxx = "Lỗi trong quá trình in báo cáo" + ex.Message
            context.Response.Write("<script language=" + "javascript" + ">alert('" + xxx + "');</script>")
        Finally

        End Try
    End Sub



    Private Sub AddParam(ByVal strName As String, ByVal strValue As String)
        Try
            pdvTCS.Value = strValue
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields(strName).ApplyCurrentValues(pcTCS)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub


    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class