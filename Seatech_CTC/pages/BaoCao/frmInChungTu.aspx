﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmInChungTu.aspx.vb" ClientTarget="uplevel" Inherits="pages_BaoCao_frmInChungTu" %>
<% Response.charset="utf-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <style type="text/css">
        .style2
        {
            width: 268435456px;
        }
        .style3
        {
            width: 60%;
            height: 20px;
        }
        .style5
        {
            text-align: center;
        }
        .style6
        {
            font-family: "Times New Roman", Times, serif;
            font-size: 14pt;
            font-weight: bold;
            height: 26px;
        }
        .style7
        {
            font-family: "Times New Roman", Times, serif;
            font-size: 11pt;
        }
        .style8
        {
            font-family: "Times New Roman", Times, serif;
            font-style: italic;
            font-size: 11pt;
        }
        .style9
        {
            font-family: "Times New Roman", Times, serif;
        }
        .style11
        {
            color: #5DC8D2;
            font-size: 10pt;
            font-weight: bold;
        }
        .style12
        {
            font-size: 10pt;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server" style="font-size: 8pt; font-family: Arial;">
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0" bgcolor="#ffffff">
       
        <tr>
            <td style="padding-right: 3px; padding-left: 3px" valign="top" align="center">
                <table id="Table4" width="650px" border="0" height="910px">
                 <tr align="right">
                 <td colspan="6">
                     <img alt="" src="../../images/logo-abb.jpg" 
                         style="width: 130px; height: 40px" /></td>
        </tr>
        <tr align="right">
        <td colspan="6" class="style11" style="height:30px">
           NGÂN HÀNG TMCP AN BÌNH</td>
        </tr>
         <tr align="right">
         <td colspan="6" class="style12">
             Trụ sở chính: 170 Hai bà Trưng, P. Đa Kao, Q.1, TP. Hồ Chí Minh
             <br />
             Điện thoại: (84-8) 38 244 855 - Fax: (84-8) 38 244 856 - www.abbank.vn
           </td>
        </tr>
        <tr align="left">
        <td colspan="6" >
                <hr style="border-color:Red; background-color:Red" />

        </td>
        </tr>
                <tr>
                    <td colspan="6" style="text-align: center" class="style6">MẪU 05/GNTNS-MSB
                    </td>
                </tr>
                 <tr>
                     <td colspan="6" style="text-align: center"><span class="style8">(Theo mẫu C1 - 02/NS tại Thông tư</span><span 
                             class="style7"> số 128/2008/TT-BTC ngày 24/12/2008 của BTC) </span>
                     </td>
                </tr>
              <tr align="left" style="width: 20%">
                                    <td valign='top'>
                                        <table cellpadding='0' cellspacing='0' style="border-style: solid; border-width: thin;
                                            width: 100%; height: 40px;">
                                            <tr>
                                                <td  class="style5">
                                                    <span class="style9"><font size="2">Không ghi vào
                                                    </font>
                                                    <br />
                                                    <font size="2">khu vực này
                                                    </font></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
             </tr>       
              <tr>
                <td style="font-size: 10pt; font-family: Arial; font-weight: bold" align="center"
                    colspan="6" valign="bottom">
                    <asp:Label ID="Label4" runat="server" 
                        style="font-family: 'Times New Roman', Times, serif; font-size: 13pt"><b>GIẤY NỘP TIỀN VÀO NGÂN SÁCH NHÀ NƯỚC</b></asp:Label>
                </td>
            </tr>  
                    <tr>
                        <td style="width: 20%">
                        </td>
                        <td align="center" colspan="4">
                            <%--<asp:CheckBox ID="ChkTM" runat="server" Text=" Tiền mặt:" TextAlign="Left" />
                            &nbsp;
                            <asp:CheckBox ID="ChkCK" runat="server" Text=" Chuyển khoản:" TextAlign="Left" />--%>
                            Tiền mặt:&nbsp;<asp:Image ID="imgTienMat" runat="server" />&nbsp;Chuyển khoản:&nbsp;<asp:Image
                                ID="imgChuyenKhoan" runat="server" />
                        </td>
                        <td style="width: 30%" align="left">
                            Mã hiệu:
                            <asp:Label ID="lbMaHieu" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%">
                        </td>
                        <td colspan="4" align="center" style="font-style: italic;" class="style3">
                            (Đánh dấu X vào ô tương ứng)
                        </td>
                        <td style="width: 30%" align="left">
                            Số:
                            <asp:Label ID="lbSoHieu" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr style="height: 20px;">
                        <td class="" align="center" colspan="6">
                            <asp:Label ID="lbCoppy" runat="server" Font-Bold="true"></asp:Label>, Ngày NH:
                            <asp:Label ID="lbNgayNH" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td colspan="3" style="color: Black;">
                            Người nộp thuế:
                            <asp:Label runat="server" ID="lbNNThue" Font-Bold="True">
                               
                            </asp:Label>
                        </td>
                        <td colspan="3" style="color: Black">
                            Mã số thuế:
                            <asp:Label runat="server" ID="lbMaST" Font-Bold="True">
                               
                            </asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td colspan="6">
                            Địa chỉ:
                            <asp:Label runat="server" ID="lbDiachi" Font-Bold="True">
                              
                            </asp:Label>
                        </td>
                    </tr>
                    <tr align="left" valign="bottom">
                        <td class="" style="height: 14px" align="left" valign="bottom" colspan="6">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <col width="50%" />
                                <col width="50%" />
                                <tr>
                                    <td valign="bottom">
                                        <asp:Label ID="Label15" runat="server" CssClass="label2">Huyện:</asp:Label>
                                        <asp:Label ID="lbHuyen" runat="server" Font-Bold="True"> </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="Label21" runat="server" CssClass="label2">Tỉnh,TP:</asp:Label>
                                        <asp:Label ID="lbTinh" runat="server" Font-Bold="True"></asp:Label>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="left">
                        <td colspan="3" class="title_label2" style="color: Black;">
                            <asp:Label runat="server" ID="Label37"> Người nộp thay:</asp:Label>
                            <asp:Label runat="server" ID="lbNNThay" Font-Bold="True">
                            </asp:Label>
                        </td>
                        <td colspan="3" class="title_label2" style="color: Black">
                            <asp:Label runat="server" ID="Label12"> Mã số thuế:</asp:Label>
                            <asp:Label runat="server" ID="lbMaSTNNThay" Font-Bold="True">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td colspan="6">
                            Địa chỉ:
                            <asp:Label runat="server" ID="lbDCNNT" Font-Bold="True">
                              
                            </asp:Label>
                        </td>
                    </tr>
                    <tr align="left" valign="bottom">
                        <td class="" style="height: 14px" align="left" valign="bottom" colspan="6">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <col width="50%" />
                                <col width="50%" />
                                <tr>
                                    <td valign="bottom">
                                        <asp:Label ID="Label13" runat="server" CssClass="label2">Huyện:</asp:Label>
                                        <asp:Label ID="lbHuyenNTT" runat="server" Font-Bold="True"> </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="Label44" runat="server" CssClass="label2">Tỉnh,TP:</asp:Label>
                                        <asp:Label ID="lbTinhNNT" runat="server" Font-Bold="True"></asp:Label>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div id="DIVCK" runat="server">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr align="left" valign="bottom">
                                        <td class="" style="height: 14px" align="left" valign="bottom" colspan="6">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <col width="50%" />
                                                <col width="50%" />
                                                <tr>
                                                    <td valign="bottom">
                                                        <asp:Label ID="Label47" runat="server" CssClass="label2">Đề nghị:</asp:Label>
                                                        <asp:Label ID="lbNH" runat="server" Font-Bold="True"> </asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="Label49" runat="server" CssClass="label2">Trích TK số:</asp:Label>
                                                        <asp:Label ID="lbTKTrich" runat="server" Font-Bold="True"></asp:Label>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr align="left" valign="bottom">
                                        <td class="" style="height: 14px" align="left" valign="bottom" colspan="6">
                                            <table width="100%" border="0" cellpadding="0">
                                                <col width="50%" />
                                                <col width="50%" />
                                                <tr>
                                                    <td valign="bottom">
                                                        <asp:Label ID="Label51" runat="server" CssClass="label2">Để chuyển vào KBNN:</asp:Label>
                                                        <asp:Label ID="lbKBNN" runat="server" Font-Bold="True"> </asp:Label>
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="Label55" runat="server" CssClass="label2">Tỉnh,TP:</asp:Label>
                                                        <asp:Label ID="LbTinhKB" runat="server" Font-Bold="True"></asp:Label>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr align="left">
                        <td colspan="6">
                            <div id="DIVTM" runat="server">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr align="left">
                                        <td class="style2" style="color: Black; width: 60%">
                                            <asp:Label runat="server" ID="Label48"> Nộp tiền mặt để chuyển cho KBNN:</asp:Label>
                                            <asp:Label runat="server" ID="lbTenKB" Font-Bold="True">
                                            </asp:Label>
                                        </td>
                                        <td class="title_label2" style="color: Black">
                                            <asp:Label runat="server" ID="Label54"> Tỉnh, TP:</asp:Label>
                                            <asp:Label runat="server" ID="lbTenTinh" Font-Bold="True">
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr align="left" valign="bottom">
                        <td class="" style="height: 14px" align="left" valign="bottom" colspan="6">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <col width="50%" />
                                <col width="50%" />
                                <tr>
                                    <td valign="bottom">
                                        <asp:Label ID="Label50" runat="server" CssClass="label2">Để ghi thu NSNN:</asp:Label>
                                        <asp:Label ID="lbTKCo" runat="server" Font-Bold="True"> </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="Label57" runat="server" CssClass="label2">hoặc nộp vào TK tạm thu số:</asp:Label>
                                        <asp:Label ID="lbTKCo1" runat="server" Font-Bold="True"></asp:Label>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="left" valign="bottom">
                        <td class="" style="height: 14px" align="left" valign="bottom" colspan="6">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <col width="50%" />
                                <col width="50%" />
                                <tr>
                                    <td valign="bottom">
                                        <asp:Label ID="Label56" runat="server" CssClass="label2">Cơ quan quản lý thu:</asp:Label>
                                        <asp:Label ID="lbTenCQThu" runat="server" Font-Bold="True"> </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="Label59" runat="server" CssClass="label2">Mã số:</asp:Label>
                                        <asp:Label ID="lbMaCQThu" runat="server" Font-Bold="True"></asp:Label>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="left">
                        <td colspan="2" class="title_label2" style="color: Black; width: 60%">
                            <asp:Label runat="server" ID="Label58"> Tờ khai HQ, QĐ số:</asp:Label>
                            <asp:Label runat="server" ID="lbSoTK" Font-Bold="True">
                            </asp:Label>
                        </td>
                        <td style="color: Black; width: 15%">
                            <asp:Label runat="server" ID="Label60"> Ngày:</asp:Label>
                            <asp:Label runat="server" ID="lbNgayTK" Font-Bold="True">
                            </asp:Label>
                        </td>
                        <td colspan="3" class="title_label2" style="color: Black">
                            <asp:Label runat="server" ID="Label61"> Loại hình XNK:</asp:Label>
                            <asp:Label runat="server" ID="lbLHXNK" Font-Bold="True">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr align="left" valign="bottom">
                        <td class="" style="height: 14px" align="left" valign="bottom" colspan="6">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <col width="50%" />
                                <col width="50%" />
                                <tr>
                                    <td valign="bottom">
                                        <asp:Label ID="Label63" runat="server" CssClass="label2">(hoặc) Bảng kê biên lai số:</asp:Label>
                                        <asp:Label ID="lbSoBK" runat="server" Font-Bold="True"> </asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="Label65" runat="server" CssClass="label2">Ngày:</asp:Label>
                                        <asp:Label ID="lbNgayBK" runat="server" Font-Bold="True"></asp:Label>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr valign="bottom" style="height: 20px">
                        <td colspan="6">
                            <asp:DataGrid ID="gridDeatail" runat="server" AutoGenerateColumns="False" TabIndex="9"
                                Width="100%" BorderColor="#989898" CssClass="grid_data" FooterStyle-Wrap="true"
                                ShowFooter="true">
                                <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                                <HeaderStyle BackColor="#E4E5D7" Font-Bold="true"></HeaderStyle>
                                <ItemStyle CssClass="grid_item" />
                                <Columns>
                                    <asp:BoundColumn DataField="STT" Visible="true" HeaderText="STT">
                                        <HeaderStyle Width="10px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="0px"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Noidung" HeaderText="Nội dung các khoản nộp NS">
                                        <HeaderStyle></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="CC" HeaderText="Mã chương">
                                        <HeaderStyle Width="80px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="MTM" HeaderText="Mã NDKT(TM)">
                                        <HeaderStyle Width="80px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="KyThue" HeaderText="Kỳ thuế" FooterText="Cộng">
                                        <HeaderStyle Width="80px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        <FooterStyle Font-Bold="true" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="SoTien" HeaderText="Số tiền">
                                        <HeaderStyle Width="80px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        <FooterStyle Font-Bold="true" />
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr align="left">
                        <td colspan="6" style="height: 35px">
                            <asp:Label ID="Label2" runat="server">Số tiền bằng chữ: </asp:Label>
                            <asp:Label ID="lbTienChu" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td align="left" colspan="6" style="height: 14px">
                            <table cellpadding="0" cellspacing="0" width="100%" style="border-style: solid; border-width:thin">
                            <tr>
                                <td class="title_label2" colspan="2">
                                    <asp:Label ID="Label42" runat="server" Style="text-decoration: underline" Font-Bold="true">PHẦN NGÂN HÀNG GHI</asp:Label>
                                </td>
                              </tr>
                                <tr >
                                    <td valign="top">
                                        <table cellpadding="4" cellspacing="1" border="0" width="100%" >
                                         <tr>
                                         <td class="label2">
                                            <asp:Label ID="Label9" runat="server">Mã CN/Số CT:</asp:Label>
                                            <asp:Label ID="lbChinhanh" runat="server" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td class="label2">
                                            <asp:Label ID="Label7" runat="server">Số RM:</asp:Label>
                                            <asp:Label ID="lbSoRM" runat="server" Font-Bold="true"></asp:Label>
                                        </td>
                                        </tr>
                                        <tr>
                                        <td class="label2">
                                            <asp:Label ID="Label10" runat="server">Seq:</asp:Label>
                                            <asp:Label ID="lbSoSeq" runat="server" Font-Bold="true"></asp:Label>
                                        </td>
                                        <td class="label2">
                                         <asp:Label ID="Label67" runat="server">Mã GDV:</asp:Label>
                                         <asp:Label ID="lbMaGDV" runat="server" Font-Bold="true"></asp:Label>
                                        </td>  
                                        </tr>
                                        <tr>
                                           <td class="label2">
                                                <asp:Label ID="Label6" runat="server">Ngày GD:</asp:Label>
                                                <asp:Label ID="lbNgayGD" runat="server" Font-Bold="true"></asp:Label>
                                             </td>
                                             <td class="boldlabel2">
                                                <asp:Label ID="Label68" runat="server">Mã KSV:</asp:Label>
                                                <asp:Label ID="lbMaKSV" runat="server" Font-Bold="true"></asp:Label>
                                            </td>
                                        </tr>
                                         </table>
                                         
                                           <%-- 
                                           
                                                
                                                
                                                
                                               
                                            
                                            <tr>
                                                
                                                
                                                 
                                               
                                            </tr>
                                            <tr>
                                             <td class="label2">
                                                    <asp:Label ID="Label69" runat="server">Nợ TKKH </asp:Label>&nbsp;<asp:Label ID="lbSoTKKH" runat="server" ></asp:Label>:
                                                    <asp:Label ID="lbTKKH" runat="server" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td class="label2" colspan="2">
                                                    <asp:Label ID="Label70" runat="server">Có TK 420201001:</asp:Label>
                                                    <asp:Label ID="lbTKPhi" runat="server" Font-Bold="true"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                             <td class="boldlabel2" >
                                                    <asp:Label ID="Label71" runat="server">Có TK 280701018:</asp:Label>
                                                    <asp:Label ID="lbTKThue" runat="server" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td class="boldlabel2" colspan="2">
                                                    <asp:Label ID="Label72" runat="server">Có TK 290201001:</asp:Label>
                                                    <asp:Label ID="lbTKVAT" runat="server" Font-Bold="true"></asp:Label>
                                                </td>
                                                </tr>--%>
                                       
                                    </td>
                                    <td>
                                     <table cellpadding="4" cellspacing="1" border="0" width="100%">
                                     <tr>
                                         <td class="label2">
                                            <asp:Label ID="Label69" runat="server">Nợ TKKH </asp:Label>&nbsp;<asp:Label ID="lbSoTKKH" runat="server" ></asp:Label>:
                                            <asp:Label ID="lbTKKH" runat="server" Font-Bold="true"></asp:Label>
                                        </td>
                                     </tr>
                                     <tr>
                                         <td class="label2" colspan="2">
                                              <asp:Label ID="Label71" runat="server">Có TK 280701018:</asp:Label>
                                                <asp:Label ID="lbTKThue" runat="server" Font-Bold="true"></asp:Label>
                                        </td>
                                     </tr>
                                     <tr>
                                            <td class="boldlabel2" >
                                             <asp:Label ID="Label70" runat="server">Có TK 420201001:</asp:Label>
                                            <asp:Label ID="lbTKPhi" runat="server" Font-Bold="true"></asp:Label>
                                             
                                            </td>
                                     </tr>
                                     <tr>
                                        <td class="boldlabel2" colspan="2">
                                            <asp:Label ID="Label72" runat="server">Có TK 290201001:</asp:Label>
                                            <asp:Label ID="lbTKVAT" runat="server" Font-Bold="true"></asp:Label>
                                        </td>
                                     </tr>
                                     </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="left">
                        <td colspan="6" style="height: 100px; padding-top: 10px" valign="top">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0" style="font-weight: 700">
                                <col width="40%" />
                                <col width="60%" />
                                <tr>
                                    <td class="title_label2" valign="top" align="center" style="height: 60px;">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td class="title_label2">
                                                    <asp:Label ID="Label31" runat="server">ĐỐI TƯỢNG NỘP TIỀN</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label2" style="height: 40px">
                                                    <i>
                                                        <asp:Label ID="lblNgayNop" runat="server">Ngày..........tháng..........năm..........</asp:Label></i>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="boldlabel2">
                                                    <asp:Label ID="Label66" runat="server">NGƯỜI NỘP</asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" align="center" style="height: 60px">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td class="title_label2" colspan="2">
                                                    <asp:Label ID="Label32" runat="server">NGÂN HÀNG TMCP HÀNG HẢI VIỆT NAM</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="label2" colspan="2" style="height: 40px">
                                                    <i>
                                                        <asp:Label ID="lblNgayNhan" runat="server">Ngày..........tháng..........năm..........</asp:Label></i>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="boldlabel2">
                                                    <asp:Label ID="Label36" runat="server">THANH TOÁN VIÊN</asp:Label>
                                                </td>
                                                <td class="boldlabel2">
                                                    <asp:Label ID="Label8" runat="server">KIỂM SOÁT VIÊN</asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="left">
                        <td colspan="6" valign="top">
                            <table cellpadding="0" cellspacing="0" width="100%" border="0">
                                <col width="40%" />
                                <col width="60%" />
                                <tr>
                                    <td valign="top" align="center" style="height: 20px">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbNNThueKy" runat="server" Style="font-weight: 700"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top" align="center" style="height: 20px">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td style="width: 50%;" align="center">
                                                    <asp:Label ID="lbTTV" runat="server" Font-Bold="true"></asp:Label>
                                                </td>
                                                <td style="width: 50%;" align="center">
                                                    <asp:Label ID="lbKSV" runat="server" Font-Bold="true"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="left" valign="bottom" style="height: 20px">
                        <td colspan="6">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <col width="70%" />
                                <col width="30%" />
                                <tr align="left">
                                    <td colspan="2">
                                        <hr style="border-color:Red; background-color:Red"  />
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td>
                                                                                Lần ban hành/sửa đổi: 01/00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Mã số: QĐ.DV.&nbsp;22&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;                                          
                                        <asp:Label ID="Label3" runat="server" Font-Italic="true" Text="Trang 1/1" 
                                            style="text-align: right" />
                                    </td>
                                </tr>
                               
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
    </form>
</body>
</html>
