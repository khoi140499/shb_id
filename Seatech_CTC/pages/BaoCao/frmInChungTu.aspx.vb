﻿Imports Business.BaoCao
Imports Business
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports Business.NewChungTu

Partial Class pages_BaoCao_frmInChungTu
    Inherits System.Web.UI.Page
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenKB As String
    Private mv_strTenTinhTP As String
    Private mv_strMaTinhTP As String
    Private mv_strSHKBDEF As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Session.Item("User").ToString.Length = 0 Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Not clsCommon.GetSessionByID(Session.Item("User")) Then
                Response.Redirect("~/pages/Warning.html", False)
                Exit Sub
            End If
            If Not Session.Item("User") Is Nothing Then
                Dim v_dt As DataTable = clsCTU.TCS_GetParams(CType(Session("User"), Integer))
                For Each v_dr As DataRow In v_dt.Rows
                    Select Case v_dr("TEN_TS").ToString
                        Case "MA_DT"
                            mv_strMaDiemThu = v_dr("GIATRI_TS").ToString
                        Case "SHKB"
                            mv_strSHKB = v_dr("GIATRI_TS").ToString
                        Case "KHCT"
                            mv_strKyHieuCT = v_dr("GIATRI_TS").ToString
                        Case "TEN_TINH"
                            mv_strTenTinhTP = v_dr("GIATRI_TS").ToString
                            'Case "SHKB"
                            'mv_strSHKBDEF = v_dr("GIATRI_TS").ToString
                    End Select
                Next
            End If

            Dim key As New KeyCTu
            Dim objHDR As New NewChungTu.infChungTuHDR
            Dim isBS As String = Nothing
            If Request.QueryString.Item("SHKB") Is Nothing Then

            Else
                key.Kyhieu_CT = mv_strKyHieuCT
                key.So_CT = Request.QueryString.Item("SoCT").ToString
                key.SHKB = Request.QueryString.Item("SHKB").ToString
                key.Ngay_KB = ConvertDateToNumber(Request.QueryString.Item("NgayKB").ToString)
                key.Ma_NV = Request.QueryString.Item("MaNV").ToString
                key.So_BT = Request.QueryString.Item("SoBT").ToString
                isBS = Request.QueryString.Item("BS").ToString
                If mv_strSHKB <> key.SHKB Then
                    key.Ma_Dthu = GetMaDThuBySHKB(key.SHKB)
                Else
                    key.Ma_Dthu = mv_strMaDiemThu
                End If
            End If

            Dim hdr As New NewChungTu.infChungTuHDR
            Dim dtl As NewChungTu.infChungTuDTL()
            hdr = Business.buChungTu.CTU_Header(key)
            dtl = Business.buChungTu.CTU_ChiTiet(key)

            Dim ds As DataSet = Business.buChungTu.CTU_IN_LASER(key)
            'gLoaiBaoCao = Report_Type.TCS_CTU_LASER

            Dim frm As New Business.BaoCao.infBaoCao
            frm.TCS_DS = ds
            lbMaHieu.Text = hdr.KyHieu_CT.ToString
            Dim v_strMa_CN_Uer As String = clsCTU.TCS_GetMaChiNhanh(hdr.Ma_NV)
            Dim v_strBranchCode As String = clsCTU.TCS_GetMaCN_NH(v_strMa_CN_Uer)
            lbSoHieu.Text = v_strBranchCode & " - " & hdr.So_CT
            lbNNThue.Text = hdr.Ten_NNThue.ToString()
            If Exists_VangLai(hdr.Ma_NNThue) Then  ' Neu la ma DTNT dac biet se khong in ra GNT
                lbMaST.Text = ""
            Else ' Nguoc lai van in binh thuong
                lbMaST.Text = clsCommon.gf_CorrectString(hdr.Ma_NNThue)
            End If

            lbDiachi.Text = clsCommon.gf_CorrectString(hdr.DC_NNThue)
            lbNNThay.Text = clsCommon.gf_CorrectString(hdr.Ten_NNTien)
            lbMaSTNNThay.Text = clsCommon.gf_CorrectString(hdr.Ma_NNTien)
            lbDCNNT.Text = clsCommon.gf_CorrectString(hdr.DC_NNTien)

            lbHuyen.Text = GetQuanHuyenByXaID(hdr.Ma_DThu)
            lbTinh.Text = GetTinhThanhByMaXa(hdr.Ma_DThu)
            lbHuyenNTT.Text = clsCommon.gf_CorrectString(hdr.QUAN_HUYEN_NNTIEN)
            lbTinhNNT.Text = clsCommon.gf_CorrectString(hdr.TINH_TPHO_NNTIEN)

            lbNH.Text = "NH TMCP ĐẠI DƯƠNG - " & hdr.MA_NH_A.ToString
            'If hdr.PT_TT.Trim() = "00" Then
            '    lbTKNo.Text = ParmValues_Load("NH_TKTM").ToString
            'Else
            '    frm.TKNo = clsCommon.gf_CorrectString(hdr.TK_KH_NH)
            '    lbTKNo.Text = clsCommon.gf_CorrectString(hdr.TK_KH_NH)
            'End If

            'lbTKNo.Text = strFormatTK(clsCommon.gf_CorrectString(hdr.TK_No))
            lbTKTrich.Text = clsCommon.gf_CorrectString(hdr.TK_KH_NH)
            If hdr.Ma_LThue = "04" Then
                lbTKCo1.Text = clsCommon.gf_CorrectString(hdr.TK_Co)
            Else
                lbTKCo.Text = clsCommon.gf_CorrectString(hdr.TK_Co)
            End If


            lbNgayNH.Text = Convert.ToDateTime(clsCommon.gf_CorrectString(hdr.NGAY_KH_NH)).ToString(gc_DEFAULT_FORMAT_DATE)

            'Kienvt : neu trang thai daks thi in ban sao
            If Not isBS Is Nothing Then
                If isBS.Equals("1") Then
                    lbCoppy.Text = "Bản Chính"
                Else
                    lbCoppy.Text = "Bản Sao"
                End If
            End If


            'If hdr.Lan_In <= "1" Then
            '    lbCoppy.Text = "Bản Chính"
            'Else
            '    lbCoppy.Text = "Bản Sao"
            'End If
            mv_strTenKB = Get_TenKhoBac(key.SHKB)
            lbKBNN.Text = mv_strTenKB
            lbTenKB.Text = mv_strTenKB

            LbTinhKB.Text = mv_strTenTinhTP
            lbTenTinh.Text = mv_strTenTinhTP
            lbMaCQThu.Text = hdr.Ma_CQThu.ToString
            lbTenCQThu.Text = Get_TenCQThu(clsCommon.gf_CorrectString(hdr.Ma_CQThu))
            'lbMaQuy.Text = Business.ChungTu.buChungTu.CTU_ChiTiet(key)(0).MaQuy.ToString

            lbSoTK.Text = clsCommon.gf_CorrectString(hdr.So_TK)
            If hdr.Ngay_TK <> "" Then
                lbNgayTK.Text = Convert.ToDateTime(clsCommon.gf_CorrectString(hdr.Ngay_TK)).ToString(gc_DEFAULT_FORMAT_DATE)
            Else
                lbNgayTK.Text = ""
            End If


            Dim strTenVT As String = ""
            Dim strTenLHXNK As String = ""

            Get_LHXNK_ALL(clsCommon.gf_CorrectString(hdr.LH_XNK), strTenLHXNK, strTenVT)
            lbLHXNK.Text = clsCommon.gf_CorrectString(strTenVT)

            lbSoBK.Text = clsCommon.gf_CorrectString(hdr.So_BK)
            lbNgayBK.Text = clsCommon.gf_CorrectString(hdr.Ngay_BK)
            lbTienChu.Text = TCS_Dich_So(hdr.TTien, "đồng")


            gridDeatail.Columns(5).FooterText = Globals.Format_Number(hdr.TTien.ToString, ".")

            'lbMaKB.Text = clsCommon.gf_CorrectString(hdr.SHKB)
            'lbMaDBHC.Text = clsCommon.gf_CorrectString(hdr.Ma_Xa)
            'lbTKNS.Text = strFormatTK(clsCommon.gf_CorrectString(hdr.TK_Co))
            lbNNThueKy.Text = clsCommon.gf_CorrectString(hdr.Ten_NNTien)
            lbKSV.Text = Get_HoTenNV(clsCommon.gf_CorrectString(hdr.Ma_KS))
            lbTTV.Text = Get_HoTenNV(clsCommon.gf_CorrectString(hdr.Ma_NV))
            lbMaGDV.Text = Get_TellerNV(clsCommon.gf_CorrectString(hdr.Ma_NV))
            lbMaKSV.Text = Get_TellerNV(clsCommon.gf_CorrectString(hdr.Ma_KS))
            lbTKKH.Text = Globals.Format_Number(Double.Parse(hdr.TTien.ToString) + Double.Parse(hdr.PHI_GD.ToString) + Double.Parse(hdr.PHI_VAT.ToString), ".")
            lbTKPhi.Text = Globals.Format_Number(hdr.PHI_GD.ToString, ".")
            lbTKThue.Text = Globals.Format_Number(hdr.TTien.ToString, ".")
            lbTKVAT.Text = Globals.Format_Number(hdr.PHI_VAT.ToString, ".")
            lbNgayGD.Text = Convert.ToDateTime(hdr.Ngay_HT).ToString("dd/MM/yyyy  hh:mm:ss")
            lbSoTKKH.Text = clsCommon.gf_CorrectString(hdr.TK_KH_NH)
            lbSoRM.Text = hdr.So_RM.ToString
            lbSoSeq.Text = hdr.REF_NO.ToString
            lbChinhanh.Text = v_strBranchCode & " - " & hdr.So_CT


            If lbNgayNH.Text.Length > 0 Then
                lblNgayNop.Text = "Ngày " & lbNgayNH.Text.Substring(0, 2) & " tháng " & lbNgayNH.Text.Substring(3, 2) & " năm " & lbNgayNH.Text.Substring(6, 4) & "..."
                lblNgayNhan.Text = "Ngày " & lbNgayNH.Text.Substring(0, 2) & " tháng " & lbNgayNH.Text.Substring(3, 2) & " năm " & lbNgayNH.Text.Substring(6, 4) & "..."
            End If

            'chan trang
            If hdr.PT_TT.Trim() = "00" Then
                'Tiền Mặt
                DIVTM.Visible = True
                DIVCK.Visible = False
                imgChuyenKhoan.ImageUrl = "../../images/uncheckmark.png"
                imgTienMat.ImageUrl = "../../images/checkmark.png"
            Else
                DIVTM.Visible = False
                DIVCK.Visible = True
                imgChuyenKhoan.ImageUrl = "../../images/checkmark.png"
                imgTienMat.ImageUrl = "../../images/uncheckmark.png"
            End If
            gridDeatail.DataSource = ds
            gridDeatail.DataBind()
            Business.ChungTu.buChungTu.CTU_Update_LanIn(key)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '    clsCommon.WriteLog(ex, "Có lỗi trong quá trình In hoặc Cập nhật chứng từ", Session.Item("TEN_DN"))
            'Throw (ex)
        End Try
    End Sub

    Protected Sub gridDeatail_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridDeatail.ItemDataBound
        Try
            If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
                e.Item.Cells(5).Text = Globals.Format_Number(e.Item.Cells(5).Text, ".")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetMaDThuBySHKB(ByVal shkb As String) As String
        Dim strSql As String = "SELECT * FROM TCS_THAMSO where ten_ts='SHKB' and giatri_ts='" & shkb & "'"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ma_dthu").ToString
        Else
            Return ""
        End If
    End Function
    Private Function GetQuanHuyenByXaID(ByVal strMa_Dthu As String) As String

        Dim strSql As String = "SELECT ten FROM tcs_dm_xa where ma_xa= (select Giatri_ts from tcs_thamso where upper(ten_ts)='MA_DBHC' and ma_dthu ='" & strMa_Dthu & "')"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ten").ToString
        Else
            Return ""
        End If
    End Function
    Private Function GetTinhThanhByMaXa(ByVal strMa_Dthu As String) As String
        Dim strSql As String = "SELECT ten FROM tcs_dm_xa where ma_xa=(select Giatri_ts from tcs_thamso where upper(ten_ts)='MA_TTP' and ma_dthu ='" & strMa_Dthu & "')"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ten").ToString
        Else
            Return ""
        End If
    End Function

    'Private Function GetQuanHuyen(ByVal strMa_Xa As String) As String
    '    Dim cnTD As DataAccess
    '    Dim strSql As String
    '    Dim p As IDbDataParameter
    '    Dim arrIn As IDataParameter()
    '    Dim arrOut As ArrayList


    '    cnTD = New DataAccess
    '    ReDim arrIn(0)

    '    p = cnTD.GetParameter("p_xa", ParameterDirection.Input, strMa_Xa, DbType.String)
    '    p.Size = 10
    '    arrIn(0) = p

    '    'p = cnTD.GetParameter("p_lstdiemthu", ParameterDirection.Input, pv_lngNgayKS, DbType.String)
    '    'p.Size = 200
    '    'arrIn(1) = p

    '    strSql = "CTC_OWNER.tcs_pck_util.fnc_get_ten_tinhthanh_by_xa"
    '    arrOut = cnTD.ExecuteNonQuery(strSql, CommandType.StoredProcedure, arrIn)
    '    Return arrOut(0).ToString
    'End Function
End Class
