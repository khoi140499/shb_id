﻿Imports VBOracleLib
Imports System.IO
Imports System.Data

Partial Class pages_BaoCao_frmBC_DC_02
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim ngay_dc As String = ""
            Dim ma_gd_dc As String = ""


            If Not Request.Params("ngay_dc") Is Nothing Then
                ngay_dc = Request.Params("ngay_dc")
                lblNgayDC.Text = "Ngày " & ngay_dc.Substring(6, 2) & " tháng " & ngay_dc.Substring(4, 2) & " năm " & ngay_dc.Substring(0, 4)
            End If

            If Not Request.Params("ma_gd_dc") Is Nothing Then
                ma_gd_dc = Request.Params("ma_gd_dc")
            End If

            Dim header As String = "<table id='TableResult' width='100%' border='1' bgColor='#ffffff' borderColor='#000000' cellSpacing='0' cellPadding='0' style='font-size:10.0pt; font-family:Calibri; background:white;'>" & _
                                        " <tr style='background:yellow;'>" & _
                                            " <td align='center'>STT</td>" & _
                                            " <td align='center'>Số tham chiếu</td>" & _
                                            " <td align='center'>Số giấy nộp tiền</td>" & _
                                            " <td align='center'>Mã ref</td>" & _
                                            " <td align='center'>Mã hiệu chứng từ</td>" & _
                                            " <td align='center'>Số chứng từ</td>" & _
                                            " <td align='center' colspan='2'>Thông tin NNT</td>" & _
                                            " <td align='center'>Ngày gửi GNT</td>" & _
                                            " <td align='center'>Thời gian TCT truyền</td> " & _
                                            " <td align='center'>Thời gian NH nhận</td>" & _
                                            " <td align='center'>Ngày nộp thuế điện tử</td>" & _
                                            " <td align='center'>Số tiền</td>" & _
                                            " <td align='center'>Loại tiền</td> " & _
                                            " <td align='center' colspan='2'>Trạng thái</td>" & _
                                        "</tr>" & _
                                         "<tr style='background:yellow;'>" & _
                                            "<td>&nbsp;</td>" & _
                                            "<td>&nbsp;</td>" & _
                                            "<td>&nbsp;</td>" & _
                                            "<td>&nbsp;</td>" & _
                                            "<td>&nbsp;</td>" & _
                                            "<td>&nbsp;</td>" & _
                                            "<td align='center'>Tên NNT</td >" & _
                                            "<td align='center'>MST</td> " & _
                                            "<td>&nbsp;</td>" & _
                                            "<td>&nbsp;</td>" & _
                                            "<td>&nbsp;</td>" & _
                                            "<td>&nbsp;</td>" & _
                                            "<td>&nbsp;</td>" & _
                                            "<td>&nbsp;</td>" & _
                                            "<td align='center'>Tại TCT</td>" & _
                                            "<td align='center'>Tại NH</td>" & _
                                        "</tr>"
            Dim detail As String = ""
            Dim sql As String = "Select * from NTDT_BAOCAO_HDR WHERE MA_BCAO='02DC-NHTM-NTDT' AND TO_CHAR(NGAY_DC,'yyyyMMdd')=" & ngay_dc & " AND ma_gdich_dc = '" + ma_gd_dc + "' order by id_bcao desc"
            Dim dt As DataTable = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text).Tables(0)
            If dt.Rows.Count > 0 Then
                lblSum_TONGDIEN_LECH.Text = String.Format("{0:n0}", dt.Rows(0)("TONGDIEN_LECH"))
                lblSum_TONGTIEN_LECH_USD.Text = String.Format("{0:0,0.00}", dt.Rows(0)("tongtien_lech_usd"))
                lblSum_TONGTIEN_LECH_VND.Text = String.Format("{0:n0}", dt.Rows(0)("tongtien_lech_vnd"))
                lblSum_TONGDIEN_TCTCO_NHTMKO.Text = String.Format("{0:n0}", dt.Rows(0)("TONGDIEN_TCTCO_NHTMKO"))
                lblSum_TONGTIEN_TCTCO_NHTMKO_USD.Text = String.Format("{0:0,0.00}", dt.Rows(0)("tongtien_tctco_nhtmko_usd"))
                lblSum_TONGTIEN_TCTCO_NHTMKO_VND.Text = String.Format("{0:n0}", dt.Rows(0)("tongtien_tctco_nhtmko_vnd"))
                lblSum_TONGDIEN_NHTMCO_TCTKO.Text = String.Format("{0:n0}", dt.Rows(0)("TONGDIEN_NHTMCO_TCTKO"))
                lblSum_TONGTIEN_NHTMCO_TCTKO_USD.Text = String.Format("{0:0,0.00}", dt.Rows(0)("tongtien_nhtmco_tctko_usd"))
                lblSum_TONGTIEN_NHTMCO_TCTKO_VND.Text = String.Format("{0:n0}", dt.Rows(0)("tongtien_nhtmco_tctko_vnd"))
                lblSum_TONGDIEN_LECH_TTHAI.Text = String.Format("{0:n0}", dt.Rows(0)("TONGDIEN_LECH_TTHAI"))
                lblSum_TONGTIEN_LECH_TTHAI_USD.Text = String.Format("{0:0,0.00}", dt.Rows(0)("tongtien_lech_tthai_usd"))
                lblSum_TONGTIEN_LECH_TTHAI_VND.Text = String.Format("{0:n0}", dt.Rows(0)("tongtien_lech_tthai_vnd"))
                lblTuNgay.Text = convertDayofYear(dt.Rows(0)("TU_NGAY_DC"))
                lblDenNgay.Text = convertDayofYear(dt.Rows(0)("DEN_NGAY_DC"))
                lblGioDC.Text = convertDayofYear(dt.Rows(0)("NGAY_GDICH_DC"))
                'Lấy detail
                detail &= "<tr><td>I</td><td>&nbsp;</td><td colspan='14'>TCT có - NH không có</td></tr>"
                Dim sql_detail_1 As String = "Select r2.*,NVL(r2.MA_NGUYENTE, 'VND') as MA_NGUYENTE_R2, ct.so_ct_nh as ref_no from NTDT_BAOCAO_CTIET_R2 r2, tcs_ctu_ntdt_hdr ct WHERE r2.so_ctu = ct.so_ctu(+) and CODE_CTIET='1' AND ID_BCAO=" & dt.Rows(0)("ID_BCAO") & " order by r2.so_ctu"
                Dim dt_detail_1 As DataTable = DataAccess.ExecuteReturnDataSet(sql_detail_1, CommandType.Text).Tables(0)
                If dt_detail_1.Rows.Count > 0 Then
                    For i = 0 To dt_detail_1.Rows.Count - 1
                        Dim row As String = "<tr>"
                        row &= "<td align='center'>" & (i + 1) & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("MA_THAMCHIEU") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("SO_GNT") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("ref_no") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("MA_HIEU_CTU") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("SO_CTU") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("TEN_NNT") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("MST") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("NGAY_NOP_GNT") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("TGIAN_TCT") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("TGIAN_NHTM") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("NGAY_NOP_THUE") & "</td>" & _
                                "<td align='right'>" & String.Format("{0:0,0.00}", dt_detail_1.Rows(i)("SO_TIEN")) & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("MA_NGUYENTE_R2") & "</td>" & _
                                "<td align='center'>" & clsCTU.GetMoTa_TrangThai_CT_NTDT(dt_detail_1.Rows(i)("TTHAI_TCT").ToString) & "</td>" & _
                                "<td align='center'>" & clsCTU.GetMoTa_TrangThai_CT_NTDT(dt_detail_1.Rows(i)("TTHAI_NHTM").ToString) & "</td></tr>"
                        detail &= row
                    Next
                End If
                detail &= "<tr><td>II</td><td>&nbsp;</td><td colspan='14'>NH có - TCT không có</td></tr>"
                Dim sql_detail_2 As String = "Select r2.*,NVL(r2.MA_NGUYENTE, 'VND') as MA_NGUYENTE_R2, ct.so_ct_nh as ref_no from NTDT_BAOCAO_CTIET_R2 r2, tcs_ctu_ntdt_hdr ct WHERE r2.so_ctu = ct.so_ctu(+) and CODE_CTIET='2' AND ID_BCAO=" & dt.Rows(0)("ID_BCAO") & " order by r2.so_ctu"
                Dim dt_detail_2 As DataTable = DataAccess.ExecuteReturnDataSet(sql_detail_2, CommandType.Text).Tables(0)
                If dt_detail_2.Rows.Count > 0 Then
                    For i = 0 To dt_detail_2.Rows.Count - 1
                        Dim row As String = "<tr>"
                        row &= "<td align='center'>" & (i + 1) & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_2.Rows(i)("MA_THAMCHIEU") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_2.Rows(i)("SO_GNT") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_2.Rows(i)("ref_no") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_2.Rows(i)("MA_HIEU_CTU") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_2.Rows(i)("SO_CTU") & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("TEN_NNT") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_2.Rows(i)("MST") & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("NGAY_NOP_GNT") & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("TGIAN_TCT") & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("TGIAN_NHTM") & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("NGAY_NOP_THUE") & "</td>" & _
                                "<td align='right'>" & String.Format("{0:0,0.00}", dt_detail_2.Rows(i)("SO_TIEN")) & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("MA_NGUYENTE_R2") & "</td>" & _
                                "<td align='center'>" & clsCTU.GetMoTa_TrangThai_CT_NTDT(dt_detail_2.Rows(i)("TTHAI_TCT").ToString) & "</td>" & _
                                "<td align='center'>" & clsCTU.GetMoTa_TrangThai_CT_NTDT(dt_detail_2.Rows(i)("TTHAI_NHTM").ToString) & "</td></tr>"
                        detail &= row
                    Next
                End If
                detail &= "<tr><td>III</td><td>&nbsp;</td><td colspan='14'>Chênh lệch về trạng thái</td></tr>"
                Dim sql_detail_3 As String = "Select r2.*,NVL(r2.MA_NGUYENTE, 'VND') as MA_NGUYENTE_R2, ct.so_ct_nh as ref_no from NTDT_BAOCAO_CTIET_R2 r2, tcs_ctu_ntdt_hdr ct WHERE r2.so_ctu = ct.so_ctu(+) and CODE_CTIET='3' AND ID_BCAO=" & dt.Rows(0)("ID_BCAO") & " order by r2.so_ctu"
                Dim dt_detail_3 As DataTable = DataAccess.ExecuteReturnDataSet(sql_detail_3, CommandType.Text).Tables(0)
                If dt_detail_3.Rows.Count > 0 Then
                    For i = 0 To dt_detail_3.Rows.Count - 1
                        Dim row As String = "<tr>"
                        row &= "<td align='right'>" & (i + 1) & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_3.Rows(i)("MA_THAMCHIEU") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_3.Rows(i)("SO_GNT") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_3.Rows(i)("ref_no") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_3.Rows(i)("MA_HIEU_CTU") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_3.Rows(i)("SO_CTU") & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("TEN_NNT") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_3.Rows(i)("MST") & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("NGAY_NOP_GNT") & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("TGIAN_TCT") & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("TGIAN_NHTM") & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("NGAY_NOP_THUE") & "</td>" & _
                                "<td align='right'>" & String.Format("{0:0,0.00}", dt_detail_3.Rows(i)("SO_TIEN")) & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("MA_NGUYENTE_R2") & "</td>" & _
                                "<td align='center'>" & clsCTU.GetMoTa_TrangThai_CT_NTDT(dt_detail_3.Rows(i)("TTHAI_TCT").ToString) & "</td>" & _
                                "<td align='center'>" & clsCTU.GetMoTa_TrangThai_CT_NTDT(dt_detail_3.Rows(i)("TTHAI_NHTM").ToString) & "</td></tr>"
                        detail &= row
                    Next
                End If
            Else
                detail &= "<tr><td align='center'>I</td><td>&nbsp;</td><td colspan='14'>TCT có - NH không có</td></tr>"
                detail &= "<tr><td align='center'>II</td><td>&nbsp;</td><td colspan='14'>NH có - TCT không có</td></tr>"
                detail &= "<tr><td align='center'>III</td><td>&nbsp;</td><td colspan='14'>Chênh lệch về trạng thái</td></tr>"
            End If
            Dim tableResult As String = ""
            tableResult &= header & detail & "</table>"
            ExportTable.InnerHtml = tableResult
        Catch ex As Exception
            ExportTable.InnerText = ex.Message
        End Try
    End Sub

    Protected Sub btnKetXuat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKetXuat.Click
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=BC_DC_02.xls")
        Response.Charset = "utf-8"
        Response.ContentType = "application/vnd.ms-excel"
        Using sw1 As New StringWriter()
            Dim hw1 As New HtmlTextWriter(sw1)
            tableBaoCao.RenderControl(hw1)
            Response.Output.Write(sw1.ToString)
            Response.Flush()
            Response.End()
        End Using
    End Sub
    Private Function convertDayofYear(ByVal strInput As String) As String
        If strInput.Length < 13 Then
            Return ""
        End If
        Dim year As Integer = 0
        Dim dayOfYear As Integer = 0
        year = Integer.Parse(strInput.Substring(0, 4))
        dayOfYear = Integer.Parse(strInput.Substring(4, 3))
        Dim hh As String = strInput.Substring(7, 2)
        Dim mm As String = strInput.Substring(9, 2)
        Dim ss As String = strInput.Substring(11, 2)
        Dim ngay As String = ""
        Dim theDate As DateTime = New DateTime(year, 1, 1).AddDays(dayOfYear - 1)
        ngay = hh + ":" + mm + ":" + ss + " ngày " + theDate.ToString("dd/MM/yyyy")
        Return ngay
    End Function
End Class
