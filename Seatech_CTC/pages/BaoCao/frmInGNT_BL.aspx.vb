﻿Imports Business.BaoCao
Imports Business
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports Business.NewChungTu
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO
Imports Business_HQ
Imports System.Globalization

Partial Class pages_BaoCao_frmInGNT_BL
    Inherits System.Web.UI.Page

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenKB As String
    Private mv_strTenTinhTP As String
    Private mv_strMaTinhTP As String
    Private mv_strSHKBDEF As String
    Private pdvTCS As New CrystalDecisions.Shared.ParameterDiscreteValue
    Private rptTCS As New ReportDocument
    Private pcTCS As New CrystalDecisions.Shared.ParameterValues
    'Private Shared strTT_TIENMAT As String = ConfigurationManager.AppSettings.Get("TK_TIENMAT").ToString()
    Private ten_NH As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LogApp.AddDebug("cmdInCT_Click", "LAP CHUNG TU BIEN LAI: In chung tu")
        Try
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Session.Item("User").ToString.Length = 0 Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Not clsCommon.GetSessionByID(Session.Item("User")) Then
                Response.Redirect("~/pages/Warning.html", False)
                Exit Sub
            End If

            Dim provider As System.Globalization.CultureInfo = System.Globalization.CultureInfo.InvariantCulture
            Dim dDateTemp As DateTime

            Dim so_ct As String = ""

            Dim objHDR As New Business.ChungTuBL.infChungTuBL_HDR

            If Not Request.QueryString.Item("so_ct") Is Nothing Then
                so_ct = Request.QueryString.Item("so_ct").ToString
            End If

            Dim hdr As New Business.ChungTuBL.infChungTuBL_HDR
            Dim dtl As Business.ChungTuBL.infChungTuBL_DTL()


            Dim oChungTU As Business.ChungTuBL.infChungTuBL = Nothing
            oChungTU = Business.SP.SongPhuongController.GetChungTuBL(so_ct)
            If oChungTU Is Nothing Then Return
            hdr = oChungTU.HDR
            dtl = oChungTU.ListDTL.ToArray()

            Dim v_strMa_CN_Uer As String = clsCTU.TCS_GetMaChiNhanh(hdr.Ma_NV)
            Dim v_strBranchCode As String = clsCTU.TCS_GetMaCN_NH(v_strMa_CN_Uer)
            Dim v_strNameCN As String = clsCTU.TCS_GetTenCNNH(v_strMa_CN_Uer)
            Dim ds As DataSet = Business.ChungTuSP.buChungTu.CTU_IN_BL(so_ct)
            Dim strFileName As String = ""
            Dim v_strReportName As String = "In chứng từ thu ngân sách"

            strFileName = Server.MapPath("RPT\C102BL.rpt")

            If (Not File.Exists(strFileName)) Then
            End If
            rptTCS.Load(strFileName)
            ds.Tables(0).TableName = "CTU"

            rptTCS.SetDataSource(ds)
            Dim mCopy As String = ""
            Dim isBS As String = Business.ChungTuSP.buChungTu.CTU_Get_LanIn_CTBL(so_ct)
            If Not isBS Is Nothing Then
                If isBS.Equals("0") Then
                    mCopy = "(Bản Chính)"
                Else
                    mCopy = "(Bản copy)"
                End If
            End If
            AddParam("Copy", mCopy)
            AddParam("KHCT", clsCommon.gf_CorrectString(hdr.KyHieu_CT))
            AddParam("SoCT", clsCommon.gf_CorrectString(hdr.So_CT))
            AddParam("Ten_NNThue", clsCommon.gf_CorrectString(hdr.Ten_NNThue))
            AddParam("Ma_NNThue", clsCommon.gf_CorrectString(hdr.Ma_NNThue))
            AddParam("DC_NNThue", clsCommon.gf_CorrectString(hdr.DC_NNThue))
            Dim strTenTinhTP As String = ""
            If hdr.TEN_HUYEN_NNTHUE <> "" Then
                AddParam("Huyen_NNThue", clsCommon.gf_CorrectString(hdr.TEN_HUYEN_NNTHUE))
            Else
                AddParam("Huyen_NNThue", "")
            End If
            If hdr.TEN_TINH_NNTHUE <> "" Then
                AddParam("Tinh_NNThue", clsCommon.gf_CorrectString(hdr.TEN_TINH_NNTHUE))
            Else
                AddParam("Tinh_NNThue", "")
            End If


            AddParam("Ten_NNTien", clsCommon.gf_CorrectString(hdr.Ten_NNTien))
            AddParam("Ma_NNTien", clsCommon.gf_CorrectString(hdr.Ma_NNTien))
            AddParam("DC_NNTien", clsCommon.gf_CorrectString(hdr.DC_NNTIEN))

            If hdr.TEN_HUYEN_NNTIEN <> "" Then
                AddParam("Huyen_NNTien", hdr.TEN_HUYEN_NNTIEN)
            Else
                AddParam("Huyen_NNTien", "")
            End If
            If hdr.TEN_TINH_NNTIEN <> "" Then
                AddParam("Tinh_NNTien", hdr.TEN_TINH_NNTIEN)
            Else
                AddParam("Tinh_NNTien", "")
            End If
           

            AddParam("NHA", ten_NH & "-" & Get_TenCN(hdr.Ma_CN.ToString))
            AddParam("Ten_KB", Get_TenKhoBac(hdr.SHKB))
            AddParam("Ten_CQThu", Get_TenCQThu(clsCommon.gf_CorrectString(hdr.Ma_CQThu)))
            AddParam("Ma_CQThu", clsCommon.gf_CorrectString(hdr.Ma_CQThu))
            AddParam("So_TK", "")
            AddParam("Ngay_TK", "")
            'If Not hdr.Ngay_TK Is Nothing And hdr.Ngay_TK <> "" Then
            '    dDateTemp = Date.ParseExact(hdr.Ngay_TK, "dd/MM/yyyy", provider)
            '    AddParam("Ngay_TK", dDateTemp.ToString("yyyy"))
            'Else
            '    AddParam("Ngay_TK", "")
            'End If
            If hdr.Ma_NT.Equals("VND") Then
                AddParam("Tien_Bang_Chu", TCS_Dich_So(hdr.TTien, "đồng"))
            Else
                AddParam("Tien_Bang_Chu", TCS_Dich_So((hdr.TTien * hdr.Ty_Gia), "đồng"))
            End If
            AddParam("Ten_KeToan", Get_HoTenNV(clsCommon.gf_CorrectString(hdr.Ma_NV)))
            AddParam("Key_MauCT", clsCommon.gf_CorrectString(hdr.PT_TT))

            Dim tk_co As String = clsCommon.gf_CorrectString(hdr.TK_CO)
            If tk_co.Length >= 4 Then
                tk_co = tk_co.Substring(0, 4)
            End If

            AddParam("IsTamThu", clsCommon.gf_CorrectString(hdr.MA_NTK))
         


            AddParam("TK_KN_NH", clsCommon.gf_CorrectString(hdr.TK_KH_NH))
            AddParam("MA_CN", clsCommon.gf_CorrectString(hdr.MA_CN))
            AddParam("ngay_GD", dDateTemp.ToString("dd/MM/yyyy"))

            AddParam("TKNo", clsCommon.gf_CorrectString(hdr.TK_KH_NH))


            Dim dTTien As Double
            If Not Double.TryParse(hdr.TTIEN, dTTien) Then
                dTTien = 0
            End If

            AddParam("tienTKNO", Globals.Format_Number(dTTien, "."))
            AddParam("TienTKNH", Globals.Format_Number(hdr.TTIEN.ToString, "."))

            AddParam("Tinh_KB", clsCTU.Get_TenTinh_KB_BL(Business.SP.SongPhuongController.GetMa_DBHC(hdr.SHKB)))
            AddParam("SO_FT", hdr.SO_CT_NH.ToString())
            AddParam("NHB", clsCommon.gf_CorrectString(hdr.TEN_NH_B))

            If hdr.MA_NT.Equals("VND") Or hdr.MA_NT.Equals("USD") Then
                AddParam("LoaiTien", clsCommon.gf_CorrectString(hdr.MA_NT))
                AddParam("Ma_NT", "")
            Else
                AddParam("Ma_NT", clsCommon.gf_CorrectString(hdr.MA_NT))
            End If
            If hdr.PT_TT.ToString().Equals("01") Then
                hdr.PT_TT = "01"
            End If
            If hdr.PT_TT.Equals("05") Then
                hdr.PT_TT = "00"
            End If
            AddParam("PT_TT", hdr.PT_TT.ToString())

            AddParam("SO_FCC", hdr.SO_CT_NH)
            Dim dtGetTenGDV As DataTable = DataAccess.ExecuteToTable("select ten from tcs_dm_nhanvien where ma_nv='" & hdr.MA_NV & "'")

            If dtGetTenGDV.Rows.Count > 0 Then
                AddParam("TEN_GDV", dtGetTenGDV.Rows(0)(0).ToString)
            End If

            Dim dtGetTenCN As DataTable = DataAccess.ExecuteToTable("select name from tcs_dm_chinhanh where branch_id='" & hdr.MA_CN & "'")

            If dtGetTenCN.Rows.Count > 0 Then
                AddParam("ten_cn", dtGetTenCN.Rows(0)(0).ToString)
            End If

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.HasExportButton = True
            rpvMain.DisplayToolbar = True
            rpvMain.EnableToolTips = True
            rpvMain.HasPageNavigationButtons = True
            rpvMain.ShowAllPageIds = True
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
            Dim frm As New Business.BaoCao.infBaoCao
            frm.TCS_DS = ds

            'Business.ChungTu.buChungTu.CTU_Update_LanIn(key)
            Business.ChungTuSP.buChungTu.CTU_Update_LanIn_CTBL(so_ct)
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '    clsCommon.WriteLog(ex, "Có lỗi trong quá trình In giấy nộp tiền", Session.Item("TEN_DN"))
            'Throw (ex)
        End Try
    End Sub

    Private Sub AddParam(ByVal strName As String, ByVal strValue As String)
        Try
            pdvTCS.Value = strValue
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields(strName).ApplyCurrentValues(pcTCS)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub


    Private Function GetMaDThuBySHKB(ByVal shkb As String) As String
        Dim strSql As String = "SELECT * FROM TCS_THAMSO where ten_ts='SHKB' and giatri_ts='" & shkb & "'"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ma_dthu").ToString
        Else
            Return ""
        End If
    End Function
    Private Function GetQuanHuyenByXaID(ByVal strMa_Dthu As String) As String

        Dim strSql As String = "SELECT ten FROM tcs_dm_xa where ma_xa= (select Giatri_ts from tcs_thamso where upper(ten_ts)='MA_DBHC' and ma_dthu ='" & strMa_Dthu & "')"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ten").ToString
        Else
            Return ""
        End If
    End Function
    Private Function GetTinhThanhByMaXa(ByVal strMa_Dthu As String) As String
        Dim strSql As String = "SELECT ten FROM tcs_dm_xa where ma_xa=(select Giatri_ts from tcs_thamso where upper(ten_ts)='MA_TTP' and ma_dthu ='" & strMa_Dthu & "')"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ten").ToString
        Else
            Return ""
        End If
    End Function
End Class
