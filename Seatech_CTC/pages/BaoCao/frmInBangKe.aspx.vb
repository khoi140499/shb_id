﻿Imports Business.BaoCao
Imports Business
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports Business.NewChungTu
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO
Imports Business_HQ
Imports System.Globalization

Partial Class pages_BaoCao_frmInBangKe
    Inherits System.Web.UI.Page
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenKB As String
    Private mv_strTenTinhTP As String
    Private mv_strMaTinhTP As String
    Private mv_strSHKBDEF As String
    Private pdvTCS As New CrystalDecisions.Shared.ParameterDiscreteValue
    Private rptTCS As New ReportDocument
    Private pcTCS As New CrystalDecisions.Shared.ParameterValues
    'Private Shared strTT_TIENMAT As String = ConfigurationManager.AppSettings.Get("TK_TIENMAT").ToString()
    Private ten_NH As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LogApp.AddDebug("cmdInBangKe_Click", "LAP CHUNG TU BIEN LAI: In bảng kê biên lai")
        Try
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Session.Item("User").ToString.Length = 0 Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Not clsCommon.GetSessionByID(Session.Item("User")) Then
                Response.Redirect("~/pages/Warning.html", False)
                Exit Sub
            End If

            Dim provider As System.Globalization.CultureInfo = System.Globalization.CultureInfo.InvariantCulture
            Dim dDateTemp As DateTime

            Dim so_ct As String = ""

            Dim objHDR As New Business.ChungTuBL.infChungTuBL_HDR

            If Not Request.QueryString.Item("so_ct") Is Nothing Then
                so_ct = Request.QueryString.Item("so_ct").ToString
            End If

            Dim hdr As New Business.ChungTuBL.infChungTuBL_HDR
            Dim dtl As Business.ChungTuBL.infChungTuBL_DTL()


            Dim oChungTU As Business.ChungTuBL.infChungTuBL = Nothing
            oChungTU = Business.SP.SongPhuongController.GetCT_IN_BL(so_ct)
            If oChungTU Is Nothing Then Return
            hdr = oChungTU.HDR
            dtl = oChungTU.ListDTL.ToArray()

            Dim v_strMa_CN_Uer As String = clsCTU.TCS_GetMaChiNhanh(hdr.Ma_NV)
            Dim v_strBranchCode As String = clsCTU.TCS_GetMaCN_NH(v_strMa_CN_Uer)
            Dim v_strNameCN As String = clsCTU.TCS_GetTenCNNH(v_strMa_CN_Uer)
            Dim ds As DataSet = Business.ChungTuSP.buChungTu.CTU_IN_BANGKE(so_ct)
            Dim strFileName As String = ""
            Dim v_strReportName As String = "In bảng kê biên lai"

            strFileName = Server.MapPath("RPT\rptBangKeBienLai.rpt")

            If (Not File.Exists(strFileName)) Then
            End If

            rptTCS.Load(strFileName)
            ds.Tables(0).TableName = "BangKeBL"

            rptTCS.SetDataSource(ds)

            Dim mCopy As String = ""
            Dim isBS As String = Business.ChungTuSP.buChungTu.CTU_Get_LanIn_BK(so_ct)
            If Not isBS Is Nothing Then
                If isBS.Equals("0") Then
                    mCopy = "(Bản Chính)"
                Else
                    mCopy = "(Bản copy)"
                End If
            End If
            AddParam("Copy", mCopy)
            Dim ttien_vphc As Double = hdr.TTIEN_VPHC
            Dim ttien_nopcham As Double = hdr.TTIEN_NOP_CHAM


            If hdr.MA_NT.Equals("VND") Then
                AddParam("Tien_Bang_Chu", TCS_Dich_So((ttien_vphc + ttien_nopcham), "đồng"))
            Else
                AddParam("Tien_Bang_Chu", TCS_Dich_So(((ttien_vphc + ttien_nopcham) * hdr.TY_GIA), "đồng"))
            End If
            AddParam("TEN_KB", Get_TenKhoBac(hdr.SHKB))
            AddParam("TEN_NH_B", hdr.TEN_NH_B)

            AddParam("SO_CT", clsCommon.gf_CorrectString(hdr.SO_CT))
            AddParam("NGAY_CT", clsCommon.gf_CorrectString(hdr.NGAY_KH_NH))
            AddParam("TEN_CQQD", clsCommon.gf_CorrectString(hdr.TEN_CQQD))
            AddParam("TEN_LHTHU", clsCommon.gf_CorrectString(hdr.TEN_LHTHU))

            Dim ngay_ct As String = clsCommon.gf_CorrectString(hdr.NGAY_KH_NH)

            If ngay_ct.Length >= 10 Then
                AddParam("NGAY", ngay_ct.Substring(0, 2))
                AddParam("THANG", ngay_ct.Substring(3, 2))
                AddParam("NAM", ngay_ct.Substring(6, 4))
            Else
                AddParam("NGAY", " ")
                AddParam("THANG", " ")
                AddParam("NAM", " ")
            End If


            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape

            rpvMain.HasExportButton = True
            rpvMain.DisplayToolbar = True
            rpvMain.EnableToolTips = True
            rpvMain.HasPageNavigationButtons = True
            rpvMain.ShowAllPageIds = True
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
            Dim frm As New Business.BaoCao.infBaoCao
            frm.TCS_DS = ds

            ' Business.ChungTuSP.buChungTu.
            Business.ChungTuSP.buChungTu.CTU_Update_LanIn_BK(so_ct)

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '    clsCommon.WriteLog(ex, "Có lỗi trong quá trình In bảng kê biên lai", Session.Item("TEN_DN"))
            'Throw (ex)
        End Try
    End Sub

    Private Sub AddParam(ByVal strName As String, ByVal strValue As String)
        Try
            pdvTCS.Value = strValue
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields(strName).ApplyCurrentValues(pcTCS)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub


    Private Function GetMaDThuBySHKB(ByVal shkb As String) As String
        Dim strSql As String = "SELECT * FROM TCS_THAMSO where ten_ts='SHKB' and giatri_ts='" & shkb & "'"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ma_dthu").ToString
        Else
            Return ""
        End If
    End Function
    Private Function GetQuanHuyenByXaID(ByVal strMa_Dthu As String) As String

        Dim strSql As String = "SELECT ten FROM tcs_dm_xa where ma_xa= (select Giatri_ts from tcs_thamso where upper(ten_ts)='MA_DBHC' and ma_dthu ='" & strMa_Dthu & "')"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ten").ToString
        Else
            Return ""
        End If
    End Function
    Private Function GetTinhThanhByMaXa(ByVal strMa_Dthu As String) As String
        Dim strSql As String = "SELECT ten FROM tcs_dm_xa where ma_xa=(select Giatri_ts from tcs_thamso where upper(ten_ts)='MA_TTP' and ma_dthu ='" & strMa_Dthu & "')"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ten").ToString
        Else
            Return ""
        End If
    End Function
End Class
