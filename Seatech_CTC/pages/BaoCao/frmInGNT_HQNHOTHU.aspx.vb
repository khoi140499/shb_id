﻿Imports Business.BaoCao
Imports Business
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports Business.NewChungTu
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO
Imports Business_HQ
Imports Business_HQ.NHOTHU
'Imports CustomsV3.MSG.MSG201

Partial Class pages_BaoCao_frmInGNT_HQNHOTHU
    Inherits System.Web.UI.Page
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenKB As String
    Private mv_strTenTinhTP As String
    Private mv_strMaTinhTP As String
    Private mv_strSHKBDEF As String
    Private pdvTCS As New CrystalDecisions.Shared.ParameterDiscreteValue
    Private rptTCS As New ReportDocument
    Private pcTCS As New CrystalDecisions.Shared.ParameterValues

    Private ten_NH As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        Dim BC_Type As String = ""
        Dim str201ID As String = ""
        If Not Request.QueryString.Item("IDCT") Is Nothing Then
            str201ID = Request.QueryString.Item("IDCT").ToString
        End If
        If Not Request.QueryString.Item("BC_TYPE") Is Nothing Then
            BC_Type = Request.QueryString.Item("BC_TYPE").ToString
        End If
        If str201ID.Trim <> "" And BC_Type = "1" Then
            In_GNT(str201ID)
        End If
        If str201ID.Trim <> "" And BC_Type = "2" Then
            Dim tt As String = Request.QueryString.Item("trang_thai").ToString
            In_UNT(str201ID, tt)
        End If
    End Sub
    Sub In_UNT(str201ID As String, tt As String)
        LogApp.AddDebug("btnInUNT", "Chi tiet thuế XNK nho thu: In UY NHIEM THU")
        Try
            Dim strFileName As String = ""
            Dim v_strReportName As String = "In ủy nghiệm thu"
            strFileName = Server.MapPath("RPT\TCS_UNT_HQNHOTHU.rpt")
            If (Not File.Exists(strFileName)) Then
                clsCommon.ShowMessageBox(Me, "Báo cáo TCS_UNT_HQNHOTHU.rpt không tồn tại")
            End If
            Dim ds As DataSet = daCTUHQ201.getHdr201_UNT(str201ID)
            Dim tb As DataTable = DataAccess.ExecuteToTable("select So_hs,MSG_ID201,trangthai,to_char(ngaynhanmsg,'dd/MM/RRRR') ngaynhanmsg,to_char( to_date(ngay_tn_ct,'YYYY-MM-DD""T""HH24:MI:SS'),'DD/MM/RRRR') ngay_tn_ct from tcs_hq247_201 where id='" & str201ID & "'")
            Dim provider As System.Globalization.CultureInfo = System.Globalization.CultureInfo.InvariantCulture
            If ds Is Nothing Then Return
            If ds.Tables.Count < 1 Then Return
            If ds.Tables(0).Rows.Count < 1 Then Return
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Dim dr1 As DataRow = tb.Rows(0)
            rptTCS.Load(strFileName)
            'dsct.Tables(0).TableName = "CTU"
            'rptTCS.SetDataSource(dsct)
            AddParam("so_hd", clsCommon.gf_CorrectString(dr1("so_hs")))
            If dr1("trangthai").ToString = "04" Then
                AddParam("ngayHQ", clsCommon.gf_CorrectString(dr1("ngaynhanmsg")))
                AddParam("ngayNH", clsCommon.gf_CorrectString(dr1("ngay_tn_ct")))
                AddParam("ngay_baono", clsCommon.gf_CorrectString(dr("ngay_thanhtoan")))
                AddParam("nguoiky_hq", "Tổng Cục Hải Quan")
                AddParam("nguoiky_NH", "Ngân Hàng TMCP Sài Gòn-Hà Nội (SHB)")
            Else
                AddParam("ngayHQ", clsCommon.gf_CorrectString(dr1("ngaynhanmsg")))
                AddParam("ngay_baono", "")
                AddParam("nguoiky_hq", "Tổng Cục Hải Quan")
                AddParam("ngayNH", "")
                AddParam("nguoiky_NH", "")
            End If

            AddParam("diengiai", clsCommon.gf_CorrectString(dr("remarks")))
            AddParam("tt", clsCommon.gf_CorrectString(dr1("trangthai")))
            AddParam("tkco", clsCommon.gf_CorrectString(dr("TK_Co")))
            AddParam("tkno", clsCommon.gf_CorrectString(dr("tk_kh_nh")))
            AddParam("so_unt", clsCommon.gf_CorrectString(dr1("msg_id201")))
            AddParam("ten_cqt", clsCommon.gf_CorrectString(dr("ten_hq_ph")))
            AddParam("ten_nnt", clsCommon.gf_CorrectString(dr("ten_nnthue")))
            AddParam("ten_nh_unt", clsCommon.gf_CorrectString(dr("Ten_NH_B")))
            AddParam("ten_nh_ph", "Ngân Hàng TMCP Sài Gòn-Hà Nội(SHB)")
            AddParam("Sotien", clsCommon.gf_CorrectString(dr("TTien")))
            If dr("Ma_NT").ToString.Equals("VND") Then
                AddParam("sotien_chu", TCS_Dich_So(dr("TTien"), "đồng"))
            Else
                Dim ttien As Double = 0
                Dim tg As Double = 1
                Dim ttienNT As Double = 0
                Double.TryParse(dr("TTien").ToString, ttien)
                Double.TryParse(dr("Ty_Gia").ToString, tg)
                ttienNT = ttien * tg
                AddParam("sotien_chu", TCS_Dich_So(ttienNT, "đồng"))
            End If
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
            rpvMain.HasExportButton = True
            rpvMain.DisplayToolbar = True
            rpvMain.EnableToolTips = True
            rpvMain.HasPageNavigationButtons = True
            rpvMain.ShowAllPageIds = True
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
        Catch ex As Exception

        End Try

    End Sub
    Sub In_GNT(str201ID As String)
        LogApp.AddDebug("btnInCT", "Chi tiet thuế XNK nho thu: In GNT")
        Try
            Dim ds As DataSet = daCTUHQ201.getGNT_HQNhothu(str201ID)
            Dim provider As System.Globalization.CultureInfo = System.Globalization.CultureInfo.InvariantCulture
            Dim dDateTemp As DateTime
            If ds Is Nothing Then Return
            If ds.Tables.Count < 1 Then Return
            If ds.Tables(0).Rows.Count < 1 Then Return
            Dim dr As DataRow = ds.Tables(0).Rows(0)
            Dim dsct As DataSet = daCTUHQ201.getChitietGNT_HQNhothu(dr("so_ct").ToString)
            Dim v_strMa_CN_Uer As String = clsCTU.TCS_GetMaChiNhanh(dr("Ma_NV").ToString)
            Dim v_strBranchCode As String = clsCTU.TCS_GetMaCN_NH(v_strMa_CN_Uer)
            Dim v_strNameCN As String = clsCTU.TCS_GetTenCNNH(v_strMa_CN_Uer)
            Dim strFileName As String = ""
            Dim v_strReportName As String = "In chứng từ thu ngân sách"
            strFileName = Server.MapPath("RPT\C102HQNHOTHU.rpt")
            If (Not File.Exists(strFileName)) Then
                clsCommon.ShowMessageBox(Me, "Báo cáo C102HQNHOTHU.rpt không tồn tại")
            End If
            rptTCS.Load(strFileName)
            dsct.Tables(0).TableName = "CTU"
            rptTCS.SetDataSource(dsct)
            AddParam("KHCT", clsCommon.gf_CorrectString(dr("KyHieu_CT")))
            AddParam("SoCT", clsCommon.gf_CorrectString(dr("So_CT")))
            AddParam("Ten_NNThue", clsCommon.gf_CorrectString(dr("Ten_NNThue")))
            AddParam("Ma_NNThue", clsCommon.gf_CorrectString(dr("Ma_NNThue")))
            AddParam("DC_NNThue", clsCommon.gf_CorrectString(dr("DC_NNThue")))
            Dim strTenTinhTP As String = ""
            If dr("QUAN_HUYENNNTIEN").ToString <> "" Then
                AddParam("Huyen_NNThue", clsCTU.Get_TenQuanHuyen(clsCommon.gf_CorrectString(dr("QUAN_HUYENNNTIEN"))))
                AddParam("Tinh_NNThue", strTenTinhTP)
            Else
                AddParam("Huyen_NNThue", "")
                AddParam("Tinh_NNThue", "")
            End If
            'AddParam("Ten_NNTien", clsCommon.gf_CorrectString(dr("Ten_NNTien")))
            'AddParam("Ma_NNTien", clsCommon.gf_CorrectString(dr("Ma_NNTien")))
            'AddParam("DC_NNTien", clsCommon.gf_CorrectString(dr("DC_NNTien")))
            'If dr("Ma_Tinh").ToString <> "" Then
            '    AddParam("Huyen_NNTien", clsCTU.Get_TenQuanHuyen(dr("Ma_Tinh").ToString))
            '    AddParam("Tinh_NNTien", strTenTinhTP)
            'Else
            '    AddParam("Huyen_NNTien", "")
            '    AddParam("Tinh_NNTien", "")
            'End If
            '-----------------------------------------------
            AddParam("Ten_NNTien", "")
            AddParam("Ma_NNTien", "")
            AddParam("DC_NNTien", "")
            AddParam("Huyen_NNTien", "")
            AddParam("Tinh_NNTien", "")
            '-----------------------------------------------
            AddParam("NHA", ten_NH & "-" & Get_TenCN(dr("Ma_CN").ToString))
            ' AddParam("Ten_KB", Get_TenKhoBac(dr("SHKB").ToString))
            AddParam("Ten_KB", dr("ten_kb").ToString)
            'AddParam("Ten_CQThu", Get_TenCQThu(clsCommon.gf_CorrectString(dr("Ma_CQThu"))))
            AddParam("Ten_CQThu", dr("Ten_CQThu"))
            AddParam("Ma_CQThu", clsCommon.gf_CorrectString(dr("Ma_CQThu")))
            AddParam("So_TK", dr("So_TK").ToString)
            If Not dr("Ngay_TK") Is DBNull.Value And dr("Ngay_TK").ToString <> "" Then
                dDateTemp = Date.ParseExact(dr("Ngay_TK").ToString, "dd/MM/yyyy", provider)
                AddParam("Ngay_TK", dDateTemp.ToString("yyyy"))
            Else
                AddParam("Ngay_TK", "")
            End If
            If dr("Ma_NT").ToString.Equals("VND") Then
                AddParam("Tien_Bang_Chu", TCS_Dich_So(dr("TTien"), "đồng"))
            Else
                Dim tt As Double = 0
                Dim tg As Double = 1
                Double.TryParse(dr("TTien").ToString, tt)
                Double.TryParse(dr("Ty_Gia").ToString, tg)
                'AddParam("Tien_Bang_Chu", TCS_Dich_So((dr("TTien") * dr("Ty_Gia")), "đồng"))
                AddParam("Tien_Bang_Chu", TCS_Dich_So(tt * tg, "đồng"))
            End If
            'AddParam("Ten_KeToan", Get_HoTenNV(clsCommon.gf_CorrectString(dr("Ma_NV"))))
            AddParam("Ten_KeToan", "")
            AddParam("Key_MauCT", clsCommon.gf_CorrectString(dr("PT_TT")))
            AddParam("IsTamThu", clsCommon.gf_CorrectString(dr("MA_NTK")))
            AddParam("TK_KN_NH", clsCommon.gf_CorrectString(dr("TK_KH_NH")))
            AddParam("MA_CN", clsCommon.gf_CorrectString(v_strBranchCode))
            Dim ngay_ct As String = Business_HQ.Common.mdlCommon.ConvertNumberToDate(dr("ngay_ct").ToString()).ToString("dd/MM/yyyy")
            'AddParam("ngay_GD", dDateTemp.ToString("dd/MM/yyyy"))
            AddParam("ngay_GD", ngay_ct)
            'If dr("PT_TT = "00" Then
            '    AddParam("TKNo", clsCommon.gf_CorrectString(strTT_TIENMAT))
            'Else
            AddParam("TKNo", clsCommon.gf_CorrectString(dr("TK_KH_NH")))
            'End If
            Dim dTTien, dPhiGD, dPhiVAT As Double
            If Not Double.TryParse(dr("TTien").ToString, dTTien) Then
                dTTien = 0
            End If
            If Not Double.TryParse(dr("PHI_GD").ToString, dPhiGD) Then
                dPhiGD = 0
            End If
            If Not Double.TryParse(dr("PHI_VAT").ToString, dPhiVAT) Then
                dPhiVAT = 0
            End If
            AddParam("tienTKNO", Globals.Format_Number(dTTien + dPhiGD + dPhiVAT, "."))
            AddParam("TienTKNH", Globals.Format_Number(dr("TTien").ToString, "."))
            AddParam("TienVAT", Globals.Format_Number(dr("PHI_VAT").ToString, "."))
            AddParam("TienPhi", Globals.Format_Number(dr("PHI_GD").ToString, "."))
            AddParam("Tinh_KB", clsCTU.Get_TenTinh(dr("Ma_Xa").ToString))
            AddParam("SO_FT", dr("So_CT_NH").ToString())
            AddParam("NHB", clsCommon.gf_CorrectString(dr("Ten_NH_B")))
            If dr("Ma_NT").ToString.Equals("VND") Or dr("Ma_NT").ToString.Equals("USD") Then
                AddParam("LoaiTien", clsCommon.gf_CorrectString(dr("Ma_NT")))
                AddParam("Ma_NT", "")
            Else
                AddParam("Ma_NT", clsCommon.gf_CorrectString(dr("Ma_NT")))
            End If
            AddParam("PT_TT", dr("PT_TT").ToString())
            AddParam("SO_FCC", dr("SEQ_NO").ToString)
            'Dim dtGetTenGDV As DataTable = DataAccess.ExecuteToTable("select ten from tcs_dm_nhanvien where ma_nv='" & dr("Ma_NV").ToString & "'")
            'If dtGetTenGDV.Rows.Count > 0 Then
            '    AddParam("TEN_GDV", dtGetTenGDV.Rows(0)(0).ToString)
            'End If
            'Dim dtGetTenCN As DataTable = DataAccess.ExecuteToTable("select name from tcs_dm_chinhanh where branch_id='" & dr("Ma_CN").ToString & "'")
            'If dtGetTenCN.Rows.Count > 0 Then
            '    AddParam("ten_cn", dtGetTenCN.Rows(0)(0).ToString)
            'End If
            AddParam("TEN_GDV", "")
            AddParam("ten_cn", "")
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
            rpvMain.HasExportButton = True
            rpvMain.DisplayToolbar = True
            rpvMain.EnableToolTips = True
            rpvMain.HasPageNavigationButtons = True
            rpvMain.ShowAllPageIds = True
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình In giấy nộp tiền", Session.Item("TEN_DN"))
        End Try
    End Sub
    Private Sub AddParam(ByVal strName As String, ByVal strValue As String)
        Try
            pdvTCS.Value = strValue
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields(strName).ApplyCurrentValues(pcTCS)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub
    Private Function GetMaDThuBySHKB(ByVal shkb As String) As String
        Dim strSql As String = "SELECT * FROM TCS_THAMSO where ten_ts='SHKB' and giatri_ts='" & shkb & "'"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ma_dthu").ToString
        Else
            Return ""
        End If
    End Function
    Private Function GetQuanHuyenByXaID(ByVal strMa_Dthu As String) As String

        Dim strSql As String = "SELECT ten FROM tcs_dm_xa where ma_xa= (select Giatri_ts from tcs_thamso where upper(ten_ts)='MA_DBHC' and ma_dthu ='" & strMa_Dthu & "')"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ten").ToString
        Else
            Return ""
        End If
    End Function
    Private Function GetTinhThanhByMaXa(ByVal strMa_Dthu As String) As String
        Dim strSql As String = "SELECT ten FROM tcs_dm_xa where ma_xa=(select Giatri_ts from tcs_thamso where upper(ten_ts)='MA_TTP' and ma_dthu ='" & strMa_Dthu & "')"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ten").ToString
        Else
            Return ""
        End If
    End Function
End Class
