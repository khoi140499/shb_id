﻿Imports VBOracleLib
Imports System.IO
Imports System.Data

Partial Class pages_BaoCao_frmBC_DC_04
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim ngay_dc As String = ""
            Dim ma_gd_dc As String = ""

            If Not Request.Params("ngay_dc") Is Nothing Then
                ngay_dc = Request.Params("ngay_dc")
                lblNgayDC.Text = "Ngày " & ngay_dc.Substring(6, 2) & " tháng " & ngay_dc.Substring(4, 2) & " năm " & ngay_dc.Substring(0, 4)
            End If
            If Not Request.Params("ma_gd_dc") Is Nothing Then
                ma_gd_dc = Request.Params("ma_gd_dc")
            End If
            ' tuanda
            ' thêm cột ID khoản nộp, số TK/QĐ/TB
            Dim header As String = "<table id='TableResult' width='100%' border='1' bgColor='#ffffff' borderColor='#000000' cellSpacing='0' cellPadding='0' style='font-size:10.0pt; font-family:Calibri; background:white;'>" & _
                                        " <tr style='background:yellow;'>" & _
                                            " <td align='center' rowspan='2'>STT</td>" & _
                                            " <td align='center' rowspan='2'>Số giấy nộp tiền</td>" & _
                                            " <td align='center' rowspan='2'>Mã hiệu chứng từ</td>" & _
                                            " <td align='center' rowspan='2'>Số chứng từ</td>" & _
                                            " <td align='center' colspan='2'>Thông tin NNT</td>" & _
                                            " <td align='center' colspan='7'>Thông tin chứng từ</td>" & _
                                            " <td align='center' rowspan='2'>Ngày gửi GNT</td> " & _
                                            " <td align='center' rowspan='2'>Ngày nộp thuế điện tử</td>" & _
                                            " <td align='center' rowspan='2'>Trạng thái</td>" & _
                                        "</tr>" & _
                                         "<tr style='background:yellow;'>" & _
                                             "<td align='center'>Tên NNT</td >" & _
                                             "<td align='center'>MST</td> " & _
                                             "<td align='center'>Mã CQ Thu</td>" & _
                                             "<td align='center'>Mã KBNN</td>" & _
                                             "<td align='center'>ID khoản nộp</td>" & _
                                             "<td align='center'>Số tờ khai/QĐ/TB</td>" & _
                                             "<td align='center'>Chương</td>" & _
                                             "<td align='center'>NDKT</td>" & _
                                             "<td align='center'>Số tiền</td>" & _
                                         "</tr>"
            Dim detail As String = ""
            Dim sql As String = "Select * from NTDT_BAOCAO_HDR WHERE MA_BCAO='04DC-NHTM-NTDT' AND TO_CHAR(NGAY_DC,'yyyyMMdd')=" & ngay_dc & " AND ma_gdich_dc = '" + ma_gd_dc + "' order by id_bcao desc"
            Dim dt As DataTable = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text).Tables(0)
            If dt.Rows.Count > 0 Then
                lblTuNgay.Text = convertDayofYear(dt.Rows(0)("TU_NGAY_DC"))
                lblDenNgay.Text = convertDayofYear(dt.Rows(0)("DEN_NGAY_DC"))
                'Lấy detail
                detail &= "<tr><td>&nbsp;</td><td colspan='15'>I. Điện xử lý thành công</td></tr>"
                Dim sum_detail1 As Decimal = 0
                Dim sum_detail1_vnd As Decimal = 0
                Dim sum_detail1_usd As Decimal = 0

                ' tuanda - 05/02/24
                Dim sql_detail_1 As String = "Select dtl.ID_KNOP , dtl.SO_TK_TB_QD, r4.*,NVL(r4.MA_NGUYENTE, 'VND') as MA_NGUYENTE_R4, ct.so_ct_nh as ref_no from NTDT_BAOCAO_CTIET_R4 r4, tcs_ctu_ntdt_hdr ct, TCS_CTU_NTDT_DTL dtl WHERE r4.so_ctu = ct.so_ctu(+) and r4.so_ctu = dtl.so_ct(+) and CODE_CTIET='1' AND ID_BCAO=" & dt.Rows(0)("ID_BCAO") & " order by r4.so_ctu"

                Dim dt_detail_1 As DataTable = DataAccess.ExecuteReturnDataSet(sql_detail_1, CommandType.Text).Tables(0)
                If dt_detail_1.Rows.Count > 0 Then
                    For i = 0 To dt_detail_1.Rows.Count - 1
                        Dim sotien As Decimal = 0

                        Try
                            'LogDebug.WriteLog("TT1:" & dt_detail_1.Rows(i)("SO_TIEN").ToString, Diagnostics.EventLogEntryType.Error)
                            sotien = Decimal.Parse(dt_detail_1.Rows(i)("SO_TIEN"))
                            'LogDebug.WriteLog("TT2:" & sotien, Diagnostics.EventLogEntryType.Error)
                        Catch ex As Exception

                        End Try

                        Dim row As String = "<tr>"
                        row &= "<td align='center'>" & (i + 1) & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("SO_GNT") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("MA_HIEU_CTU") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("SO_CTU") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("TEN_NNT") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("MST") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("MA_CQTHU") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("MA_KBNN") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("ID_KNOP") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("SO_TK_TB_QD") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("MA_CHUONG") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("MA_TMUC") & "</td>" & _
                                "<td align='right'>" & String.Format("{0:n2}", sotien) & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("NGAY_NOP_GNT") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("NGAY_NOP_THUE") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("TTHAI") & "</td> </tr>"
                        detail &= row
                        If dt_detail_1.Rows(i)("MA_NGUYENTE_R4").ToString.Equals("USD") Then
                            sum_detail1_usd += sotien
                        Else
                            sum_detail1_vnd += sotien
                        End If
                        sum_detail1 += sotien
                    Next
                End If
                detail &= "<tr><td>Cộng</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>"
                detail &= "<tr><td>&nbsp;</td><td colspan='15'>II. Điện xử lý không thành công</td></tr>"
                Dim sum_detail2 As Decimal = 0
                Dim sum_detail2_vnd As Decimal = 0
                Dim sum_detail2_usd As Decimal = 0

                ' tuanda - 06/03/24
                Dim sql_detail_2 As String = "Select dtl.ID_KNOP , dtl.SO_TK_TB_QD, r4.*,NVL(r4.MA_NGUYENTE, 'VND') as MA_NGUYENTE_R4, ct.so_ct_nh as ref_no from NTDT_BAOCAO_CTIET_R4 r4, tcs_ctu_ntdt_hdr ct, tcs_ctu_ntdt_dtl dtl WHERE r4.so_ctu = ct.so_ctu(+) and r4.so_ctu = dtl.so_ct(+) and CODE_CTIET='2' AND ID_BCAO=" & dt.Rows(0)("ID_BCAO") & " order by r4.so_ctu"

                Dim dt_detail_2 As DataTable = DataAccess.ExecuteReturnDataSet(sql_detail_2, CommandType.Text).Tables(0)
                If dt_detail_2.Rows.Count > 0 Then
                    For i = 0 To dt_detail_2.Rows.Count - 1
                        Dim sotien As Decimal = 0

                        Try
                            sotien = Decimal.Parse(dt_detail_2.Rows(i)("SO_TIEN"))
                        Catch ex As Exception

                        End Try

                        Dim row As String = "<tr>"
                        row &= "<td align='center'>" & (i + 1) & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_2.Rows(i)("SO_GNT") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_2.Rows(i)("MA_HIEU_CTU") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_2.Rows(i)("SO_CTU") & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("TEN_NNT") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_2.Rows(i)("MST") & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("MA_CQTHU") & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("MA_KBNN") & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("ID_KNOP") & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("SO_TK_TB_QD") & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("MA_CHUONG") & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("MA_TMUC") & "</td>" & _
                                "<td align='right'>" & String.Format("{0:n2}", sotien) & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("NGAY_NOP_GNT") & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("NGAY_NOP_THUE") & "</td>" & _
                                "<td align='center'>" & dt_detail_2.Rows(i)("TTHAI") & "</td> </tr>"
                        detail &= row
                        If dt_detail_2.Rows(i)("MA_NGUYENTE_R4").ToString.Equals("USD") Then
                            sum_detail2_usd += sotien
                        Else
                            sum_detail2_vnd += sotien
                        End If
                        sum_detail2 += sotien
                    Next
                End If
                detail &= "<tr><td>Cộng</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>"
                detail &= "<tr><td>&nbsp;</td><td colspan='15'>III. Điện từ chối xử lý</td></tr>"
                Dim sum_detail3 As Decimal = 0
                Dim sum_detail3_vnd As Decimal = 0
                Dim sum_detail3_usd As Decimal = 0

                ' tuanda - 06/03/2024
                Dim sql_detail_3 As String = "Select dtl.ID_KNOP , dtl.SO_TK_TB_QD, r4.*,NVL(r4.MA_NGUYENTE, 'VND') as MA_NGUYENTE_R4, ct.so_ct_nh as ref_no from NTDT_BAOCAO_CTIET_R4 r4, tcs_ctu_ntdt_hdr ct, tcs_ctu_ntdt_dtl dtl WHERE r4.so_ctu = ct.so_ctu(+) and r4.so_ctu = dtl.so_ct(+) and CODE_CTIET='3' AND ID_BCAO=" & dt.Rows(0)("ID_BCAO") & " order by r4.so_ctu"

                Dim dt_detail_3 As DataTable = DataAccess.ExecuteReturnDataSet(sql_detail_3, CommandType.Text).Tables(0)
                If dt_detail_3.Rows.Count > 0 Then
                    For i = 0 To dt_detail_3.Rows.Count - 1
                        Dim sotien As Decimal = 0

                        Try
                            sotien = Decimal.Parse(dt_detail_3.Rows(i)("SO_TIEN"))
                        Catch ex As Exception

                        End Try


                        Dim row As String = "<tr>"
                        row &= "<td align='right'>" & (i + 1) & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_3.Rows(i)("SO_GNT") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_3.Rows(i)("MA_HIEU_CTU") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_3.Rows(i)("SO_CTU") & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("TEN_NNT") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_3.Rows(i)("MST") & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("MA_CQTHU") & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("MA_KBNN") & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("ID_KNOP") & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("SO_TK_TB_QD") & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("MA_CHUONG") & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("MA_TMUC") & "</td>" & _
                                "<td align='right'>" & String.Format("{0:n2}", sotien) & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("NGAY_NOP_GNT") & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("NGAY_NOP_THUE") & "</td>" & _
                                "<td align='center'>" & dt_detail_3.Rows(i)("TTHAI") & "</td> </tr>"
                        detail &= row
                        If dt_detail_3.Rows(i)("MA_NGUYENTE_R4").ToString.Equals("USD") Then
                            sum_detail3_usd += sotien
                        Else
                            sum_detail3_vnd += sotien
                        End If
                        sum_detail3 += sotien
                    Next
                End If
                detail &= "<tr><td>Cộng</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>"
                Dim tong As Decimal = sum_detail1 + sum_detail2 + sum_detail3
                Dim tong_vnd As Decimal = sum_detail1_vnd + sum_detail2_vnd + sum_detail3_vnd
                Dim tong_usd As Decimal = sum_detail1_usd + sum_detail2_usd + sum_detail3_usd
                detail &= "<tr><td>Tổng cộng</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>"
            Else
                detail &= "<tr><td>&nbsp;</td><td colspan='14'>I. Điện xử lý thành công</td></tr>"
                detail &= "<tr><td>&nbsp;</td><td colspan='14'>II. Điện xử lý không thành công</td></tr>"
                detail &= "<tr><td>&nbsp;</td><td colspan='14'>III. Điện từ chối xử lý</td></tr>"
            End If
            Dim tableResult As String = ""
            tableResult &= header & detail & "</table>"
            ExportTable.InnerHtml = tableResult
        Catch ex As Exception
            ExportTable.InnerText = ex.Message
        End Try
    End Sub

    Protected Sub btnKetXuat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKetXuat.Click
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=BC_DC_04.xls")
        Response.Charset = "utf-8"
        Response.ContentType = "application/vnd.ms-excel"
        Using sw1 As New StringWriter()
            Dim hw1 As New HtmlTextWriter(sw1)
            tableBaoCao.RenderControl(hw1)
            Response.Output.Write(sw1.ToString)
            Response.Flush()
            Response.End()
        End Using
    End Sub
    Private Function convertDayofYear(ByVal strInput As String) As String
        If strInput.Length < 13 Then
            Return ""
        End If
        Dim year As Integer = 0
        Dim dayOfYear As Integer = 0
        year = Integer.Parse(strInput.Substring(0, 4))
        dayOfYear = Integer.Parse(strInput.Substring(4, 3))
        Dim hh As String = strInput.Substring(7, 2)
        Dim mm As String = strInput.Substring(9, 2)
        Dim ss As String = strInput.Substring(11, 2)
        Dim ngay As String = ""
        Dim theDate As DateTime = New DateTime(year, 1, 1).AddDays(dayOfYear - 1)
        ngay = hh + ":" + mm + ":" + ss + " ngày " + theDate.ToString("dd/MM/yyyy")
        Return ngay
    End Function
End Class
