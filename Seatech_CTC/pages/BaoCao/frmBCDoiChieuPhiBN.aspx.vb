﻿Imports Business_HQ.Common
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common.mdlSystemVariables
Imports Business_HQ.BaoCao
Imports System.Data
Imports Business
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security

Partial Class pages_BaoCao_frmBCDoiChieuPhiBN
    Inherits System.Web.UI.Page

    Private mv_strNgay As String

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenDiemThu As String
    Private mv_strTenKB As String
    Private mv_strLoaiBaoCao As String
    Private strMaNT As String

    Private ds As DataSet
    Private frm As Business.BaoCao.infBaoCao
    Private somon As String
    Private str_DateDC_FromDate As String
    Private str_DateDC_ToDate As String
    Private str_FromDate As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session.Item("User") Is Nothing Then
            clsCTU.TCS_SetupParams(CType(Session("User"), Integer), mv_strSHKB, mv_strKyHieuCT, _
                                   mv_strMaDiemThu, mv_strTenDiemThu, mv_strDBThu, mv_strMaNHKB, mv_strTenKB)

        End If
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                DM_DiemThu()
                txtNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                clsCommon.addPopUpCalendarToImage(btnCalendar1, txtNgay, "dd/mm/yyyy")
                clsCommon.get_NguyenTe(cboNguyenTe)
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenCNNH(ByVal pv_strMaCN As String) As String

        If Not pv_strMaCN Is Nothing Then
            Return clsCTU.TCS_GetMaCN_NH(pv_strMaCN.Substring(0, 3))
        Else
            Return ""
        End If

    End Function
    Private Function checkHSC(ByVal strMa_CN As String) As String
        Dim returnValue = ""
        returnValue = buBaoCao.checkHSC_MaCN(strMa_CN)
        Return returnValue
    End Function
    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Try
            Dim strMaCN As String = txtMaCN.SelectedValue.ToString()
            strMaNT = cboNguyenTe.SelectedValue.ToString()
            Dim strMaCN_User = clsCTU.TCS_GetMaChiNhanh(Session("User").ToString)
            Dim strCheckHS As String = ""
            strCheckHS = checkHSC(strMaCN_User)

            Dim d_DateDC_FromDate As Date = DateTime.ParseExact(txtNgay.Text.ToString, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat) ' Convert.ToDateTime(txtDenNgay.Text.ToString)
            str_FromDate = d_DateDC_FromDate.ToString("yyyy-MM-dd")
            d_DateDC_FromDate = d_DateDC_FromDate.AddDays(-1)
            str_DateDC_FromDate = d_DateDC_FromDate.ToString("yyyy-MM-dd")
            str_DateDC_ToDate = DateTime.ParseExact(txtNgay.Text.ToString, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat).ToString("yyyy-MM-dd") ' Convert.ToDateTime(txtDenNgay.Text.ToString)

            If strMaCN = "" And strCheckHS <> "1" Then
                strMaCN = strMaCN_User & ";" & strCheckHS
            End If
            ds = New DataSet
            frm = New Business.BaoCao.infBaoCao
            somon = "0"

            Select Case cboLoaiBaoCao.SelectedValue 'gLoaiBaoCao
                Case "0"
                    clsCommon.ShowMessageBox(Me, "Chưa chọn loại báo cáo!")
                    Exit Sub
                Case "DC_TONGHOP_PBBN"
                    DC_TONGHOP_PBBN(strMaCN)
                Case "DC_CHITIET_PBBN"
                    DC_CHITIET_PBBN(strMaCN)
            End Select

            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If

            frm.TCS_DS = ds
            frm.TCS_Ngay_CT = txtNgay.Text.ToString()
            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao, "ManPower")

        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình Lập bảng kê chứng từ!")
            Throw ex
        Finally
            cmdIn.Enabled = True
        End Try
    End Sub

    Private Sub DC_TONGHOP_PBBN(ByVal strMaCN As String)
        mv_strLoaiBaoCao = Business.Common.mdlCommon.Report_Type.DC_TONGHOP_PBBN
        ds = New Business_HQ.DALTcs_dchieu_nhhq_hdr().getBC_DC_TONGHOP_PBBN(str_DateDC_FromDate, strMaCN)
    End Sub

    Private Sub DC_CHITIET_PBBN(ByVal strMaCN As String)
        mv_strLoaiBaoCao = Business.Common.mdlCommon.Report_Type.DC_CHITIET_PBBN
        ds = New Business_HQ.DALTcs_dchieu_nhhq_hdr().getBC_DC_CHITIET_PBBN(str_DateDC_FromDate, strMaCN)
    End Sub

    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Private Sub DM_DiemThu()

        Dim dsDiemThu As DataSet
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        If strcheckHSC = "1" Then
            strSQL = "SELECT a.id, a.id ||'-'||a.name nam FROM tcs_dm_chinhanh a  "
        Else
            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        End If
        Try
            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(txtMaCN, dsDiemThu, 1, 0, "Tất cả")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '            clsCommon.WriteLog(ex, "Có lỗi trong quá trình lẫy thông tin chi nhánh", Session.Item("TEN_DN"))
            Throw ex
        Finally

        End Try
    End Sub

End Class
