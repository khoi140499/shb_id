﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmBCDoiChieuPhiBN.aspx.vb" Inherits="pages_BaoCao_frmBCDoiChieuPhiBN" title="Báo cáo đối chiếu Phí bộ ban ngành" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/popcalendar.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

    function FindDanhMuc(strPage,txtID, txtTitle,txtFocus) 
    {
        returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&initParam=" + $get(txtID).value,"", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
        if (returnValue != null) {
            document.getElementById(txtID).value = returnValue.ID;
            if (txtTitle!=null){
                document.getElementById(txtTitle).value = returnValue.Title;                    
            }
            if (txtFocus != null) {
                try {
                    document.getElementById(txtFocus).focus();
                }
                catch (e) {
                    document.getElementById(txtFocus).focus();
                };
            }
        }
    }
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
  <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">BÁO CÁO ĐỐI CHIẾU NỘP LỆ PHÍ BỘ BAN NGÀNH</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" align="right">
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td>
                            <table id="Table2" cellpadding="2" cellspacing="1" class="form_input" width="100%">
                                <tr align="left">
                                    <td  class="form_label">
                                        <asp:Label ID="Label5" runat="server" Text="Nguyên Tệ" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="cboNguyenTe" Width="150px" runat="server" CssClass="inputflat">
                                        </asp:DropDownList>
                                    </td>
                                    <td  class="form_label">
                                       <asp:Label ID="lblDV" runat="server" Text="Đơn vị" CssClass="label" style="display:none;"></asp:Label>&nbsp; 
                                    </td>
                                    <td class="form_control">                                        
                                        <asp:DropDownList ID="cboDonVi" Width="145px" runat="server" CssClass="inputflat" style="display:none;">                                            
                                            <asp:ListItem Text="Ngân Hàng" Value="1" ></asp:ListItem>
                                            <asp:ListItem Text="Chi nhánh" Value="2" ></asp:ListItem>
                                            <asp:ListItem Text="Phòng giao dịch" Value="3" ></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                   
                               </tr>
                               <tr align="left">
                                    <td  class="form_label">
                                        <asp:Label ID="lblChiNhanh" runat="server" Text="Chi nhánh" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control" colspan="3">
                                        <asp:DropDownList ID="txtMaCN"  runat="server" CssClass="inputflat"
                                            AutoPostBack="true" style="width: 95%; border-color: White; font-weight: bold; text-align: left;">
                                        </asp:DropDownList>                                        
                                            <asp:TextBox ID="txtTenCN" runat="server" CssClass="inputflat" Enabled ="false"                                            
                                            style="width: 5px; border-color: White; font-weight: bold; text-align: left; display:none;" 
                                            Width="380px"></asp:TextBox>                                            
                                    </td>                                    
                                    
                               </tr>                               
                               <tr align="left">
                                    <td class="form_label" width='15%'>
                                        <asp:Label ID="Label10" runat="server" Text="Ngày" CssClass="label"></asp:Label>
                                    </td>
                                    <td width='25%' class="form_control">
                                        <asp:TextBox runat="server" ID="txtNgay" Width="120px" CssClass="inputflat" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="CheckDate(this);" MaxLength="10"></asp:TextBox>
                                        <asp:Image ID="btnCalendar1" imagealign="AbsMiddle" runat="server" ImageUrl="~/images/cal.gif"  TabIndex="20"/> 
                                    </td>
                                    <td width='15%' class="form_label">
                                        <asp:Label ID="Label4" runat="server" Text="Loại báo cáo" CssClass="label"></asp:Label>
                                    </td>
                                    <td width='45%' class="form_control">
                                        <asp:DropDownList ID="cboLoaiBaoCao" Width="250px" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Chọn loại báo cáo" Value="0" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="01-Tổng hợp thu lệ phí BBN" Value="DC_TONGHOP_PBBN" ></asp:ListItem>
                                            <asp:ListItem Text="03-Chi tiết thu lệ phí BBN" Value="DC_CHITIET_PBBN"></asp:ListItem>                                       
                                        </asp:DropDownList>
                                    </td> 
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="middle" align="center" style="height: 40px">
                <asp:Button ID="cmdIn" runat="server" Text="[In BC]"></asp:Button>
            </td>
        </tr>
    </table>
</asp:Content>

