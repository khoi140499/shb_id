﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmShowReports.aspx.vb" ClientTarget="uplevel"
    Inherits="pages_BaoCao_frmShowReports" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hệ thống thu thuế tập trung</title>
     <script type="text/javascript" src="../../javascript/crystalreportviewers13/js/crviewer/crv.js"></script> 
</head>
<body>
    <form id="form1" runat="server">
    <div >
               <CR:CrystalReportViewer ID="rpvMain" runat="server"  AutoDataBind="true" ToolPanelView="None" HasToggleGroupTreeButton="False" PrintMode="ActiveX"  />
    </div>
    </form>
</body>
</html>
