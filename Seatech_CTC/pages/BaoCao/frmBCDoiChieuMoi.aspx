﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmBCDoiChieuMoi.aspx.vb" Inherits="pages_BaoCao_frmBCDoiChieuMoi" title="Báo cáo đối chiếu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/popcalendar.js" type="text/javascript"></script>
    
    <script src="../../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }
    </script>
    
    <script language="javascript" type="text/javascript">
    function jsBC_DC(_type,ngay_dc, ma_gd) {
        if (_type==1) window.open("../BaoCao/frmBC_DC_01.aspx?ngay_dc=" + ngay_dc + "&ma_gd_dc=" + ma_gd);
        if (_type==2) window.open("../BaoCao/frmBC_DC_02.aspx?ngay_dc=" + ngay_dc + "&ma_gd_dc=" + ma_gd);
        if (_type==3) window.open("../BaoCao/frmBC_DC_03.aspx?ngay_dc=" + ngay_dc + "&ma_gd_dc=" + ma_gd);
        if (_type==4) window.open("../BaoCao/frmBC_DC_04.aspx?ngay_dc=" + ngay_dc + "&ma_gd_dc=" + ma_gd);
        if (_type==5) window.open("../BaoCao/frmBC_DC_05.aspx?ngay_dc=" + ngay_dc + "&ma_gd_dc=" + ma_gd);
    }
    function FindDanhMuc(strPage,txtID, txtTitle,txtFocus) 
    {
        returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&initParam=" + $get(txtID).value,"", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
        if (returnValue != null) {
            document.getElementById(txtID).value = returnValue.ID;
            if (txtTitle!=null){
                document.getElementById(txtTitle).value = returnValue.Title;                    
            }
            if (txtFocus != null) {
                //if ($get('txtFocus').getAttribute('disabled') != 'disabled') {
                    try {
                        document.getElementById(txtFocus).focus();
                    }
                catch (e) {
                        document.getElementById(txtFocus).focus();
                    };
                //}
            }
        }
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
  <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">BÁO CÁO ĐỐI CHIẾU</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" align="right">
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td>
                            <table id="Table2" cellpadding="2" cellspacing="1" class="form_input" width="100%">
                                <tr align="left">
                                    <td class="form_label" width='20%'>
                                        <asp:Label ID="Label10" runat="server" Text="Ngày đối chiếu" CssClass="label"></asp:Label>
                                    </td>
                                    <%--<td width='25%' class="form_control">
                                        <asp:TextBox runat="server" ID="txtNgay" Width="120px" CssClass="inputflat" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="CheckDate(this);" MaxLength="10"></asp:TextBox>
                                        <asp:Image ID="btnCalendar1" imagealign="AbsMiddle" runat="server" ImageUrl="~/images/cal.gif"  TabIndex="20"/> 
                                    </td>--%>
                                    <td class="form_control" ><span style="display:block; width:40%; position:relative">
                                        <input type="text" id='txtNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 88%; display:block; float:left; margin-right:5px"
                                        onblur="CheckDate(this);" onfocus="this.select()" />
                                    </span></td>
                               </tr>
                               <tr align="left">
                                    <td width='20%' class="form_label">
                                        <asp:Label ID="Label4" runat="server" Text="Loại báo cáo" CssClass="label"></asp:Label>
                                    </td>
                                    <td width='45%' class="form_control">
                                        <asp:DropDownList ID="cboLoaiBaoCao" Width="95%" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Chọn loại báo cáo" Value="0" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="01-Báo cáo đối chiếu truyền nhận chứng từ nộp thuế điện tử khớp đúng" Value="1" ></asp:ListItem>
                                            <asp:ListItem Text="02-Báo cáo đối chiếu truyền nhận chứng từ nộp thuế điện tử sai lệch" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="03-Báo cáo đối chiếu truyền nhận chứng từ nộp thuế điện tử tổng hợp (khớp đúng + sai lệch)" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="04-Báo cáo đối chiếu chi tiết điện giao dịch truyền nhận thành công, khớp đúng" Value="4"></asp:ListItem>                                           
                                            <asp:ListItem Text="05-Báo cáo đối chiếu chi tiết điện giao dịch truyền nhận sai lệch" Value="5"></asp:ListItem> 
                                        </asp:DropDownList>
                                    </td> 
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="middle" align="center" style="height: 40px">
                <asp:Button ID="cmdIn" runat="server" Text="Xem báo cáo" ></asp:Button>
                <asp:HiddenField ID="hdfMa_CN" runat="server" />
            </td>
        </tr>
    </table>
    
    <!-- required plugins -->
    <script src="../../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>
</asp:Content>

