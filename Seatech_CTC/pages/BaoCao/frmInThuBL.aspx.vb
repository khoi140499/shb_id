﻿Imports Business.BaoCao
Imports Business
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports Business.NewChungTu
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO
Imports log4net
Imports System.Diagnostics

Partial Class pages_BaoCao_frmInThuBL
    Inherits System.Web.UI.Page

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenKB As String
    Private mv_strTenTinhTP As String
    Private mv_strMaTinhTP As String
    Private mv_strSHKBDEF As String
    Private pdvTCS As New CrystalDecisions.Shared.ParameterDiscreteValue
    Private rptTCS As New ReportDocument
    Private pcTCS As New CrystalDecisions.Shared.ParameterValues
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private Shared strMST_NH As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Taxno").ToString()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        Try
            Dim strSoCT As String = ""
            If Not Request.QueryString.Item("SoCT") Is Nothing Then
                strSoCT = Request.QueryString.Item("SoCT").ToString

                Dim dsChungTu As DataSet
                dsChungTu = Business.buChungTu.CTU_BL_Header(strSoCT)
                Dim v_strReportName As String = "In thư bảo lãnh"
                Dim strFileName As String = ""
                Dim str_TT As String = dsChungTu.Tables(0).Rows(0)("trang_thai").ToString()
                If str_TT = "31" Then
                    strFileName = Server.MapPath("RPT\TCS_THUBL_19TBLR2013 .rpt")
                Else
                    strFileName = Server.MapPath("RPT\TCS_THUBL_21TBLC2013 .rpt")
                End If
                If (Not File.Exists(strFileName)) Then
                End If
                rptTCS.Load(strFileName)
                dsChungTu.Tables(0).TableName = "tbThuBaoLanh1"
                rptTCS.SetDataSource(dsChungTu)
                AddParam("ngay", Mid(clsCTU.TCS_GetNgayLV(Session.Item("User")), 7, 2))
                AddParam("thang", Mid(clsCTU.TCS_GetNgayLV(Session.Item("User")), 5, 2))
                AddParam("nam", Mid(clsCTU.TCS_GetNgayLV(Session.Item("User")), 1, 4))
                AddParam("MST_NH", strMST_NH)
                AddParam("MA_NH_PH", Get_NH_A().Split(";")(0).ToString())
                rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
                rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
                rpvMain.ReportSource = rptTCS
                rpvMain.DataBind()
            End If
        Catch ex As Exception
            logger.Error(ex.StackTrace)
            logger.Error("Error source: " & ex.Source & vbNewLine _
                  & "Error code: System error!" & vbNewLine _
                  & "Error message: " & ex.Message)
        End Try
    End Sub

    Private Sub AddParam(ByVal strName As String, ByVal strValue As String)
        Try
            pdvTCS.Value = strValue
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields(strName).ApplyCurrentValues(pcTCS)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not Me.rptTCS Is Nothing Then
            Me.rptTCS.Dispose()
        End If
    End Sub
End Class
