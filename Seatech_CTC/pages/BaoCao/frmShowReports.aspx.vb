﻿Imports System.IO
Imports System.Data
Imports System.Drawing.Printing
Imports System.Diagnostics
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports VBOracleLib
Imports Business
Imports Business.BaoCao
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables

Partial Class pages_BaoCao_frmShowReports
    Inherits System.Web.UI.Page

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenKB As String

#Region "Biến toàn cục"
    Public FblnPrintSuccess As Boolean
    Private pdvTCS As New CrystalDecisions.Shared.ParameterDiscreteValue
    Private rptTCS As New ReportDocument
    Private pcTCS As New CrystalDecisions.Shared.ParameterValues
#End Region

#Region "Các thuộc tính của Bảng kê chứng từ"
    Private Fds As New DataSet
    Private FTen_KB As String = ""
    Private FMa_KB As String
    Private FMaDiemThu As String = ""
    Private FBanThu As String = ""
    Private FNgay_CT As String = ""
    Private FDichSo As String = ""
    Private FThuquy As String = ""
    Private FTungay As String = ""
    Private FDenngay As String = ""
    Private FTrangThai As String = ""
    Private FTen_KTT As String = ""
    Private FPTTT As String = ""
    Private mblnTienNT As Double
    Private FTien_tronggio As String = ""
    Private FTien_ngoaiGio As String = ""
    Private Fsomon_trongGio As String = ""
    Private Fsomon_ngoaiGio As String = ""
    Private FNgay_NgoaiGio As String = ""
    Private FNgay_TrongGio As String = ""

    Public Property TienTrongGio() As String
        Get
            Return FTien_tronggio
        End Get
        Set(ByVal Value As String)
            FTien_tronggio = Value
        End Set
    End Property
    Public Property TienNgoaiGio() As String
        Get
            Return FTien_ngoaiGio
        End Get
        Set(ByVal Value As String)
            FTien_ngoaiGio = Value
        End Set
    End Property
    Public Property SoMon_TrongGio() As String
        Get
            Return Fsomon_trongGio
        End Get
        Set(ByVal Value As String)
            Fsomon_trongGio = Value
        End Set
    End Property
    Public Property SoMon_NgoaiGio() As String
        Get
            Return Fsomon_ngoaiGio
        End Get
        Set(ByVal Value As String)
            Fsomon_ngoaiGio = Value
        End Set
    End Property
    Public Property Ngay_TrongGio() As String
        Get
            Return FNgay_TrongGio
        End Get
        Set(ByVal Value As String)
            FNgay_TrongGio = Value
        End Set
    End Property
    Public Property Ngay_NgoaiGio() As String
        Get
            Return FNgay_NgoaiGio
        End Get
        Set(ByVal Value As String)
            FNgay_NgoaiGio = Value
        End Set
    End Property


    Public Property TCS_PTTT() As String
        Get
            Return FPTTT
        End Get
        Set(ByVal Value As String)
            FPTTT = Value
        End Set
    End Property

    Public Property TCS_Tungay() As String
        Get
            Return FTungay
        End Get
        Set(ByVal Value As String)
            FTungay = Value
        End Set
    End Property

    Public Property TCS_Denngay() As String
        Get
            Return FDenngay
        End Get
        Set(ByVal Value As String)
            FDenngay = Value
        End Set
    End Property

    Public Property TCS_PrintSuccess() As Boolean
        Get
            Return FblnPrintSuccess
        End Get
        Set(ByVal Value As Boolean)
            FblnPrintSuccess = Value
        End Set
    End Property

    Public Property TCS_Ma_KB() As String
        Get
            Return FMa_KB
        End Get
        Set(ByVal Value As String)
            FMa_KB = Value
        End Set
    End Property

    Public Property TCS_Ten_KB() As String
        Get
            Return FTen_KB
        End Get
        Set(ByVal Value As String)
            FTen_KB = Value
        End Set
    End Property

    Public Property TCS_MaDiemThu() As String
        Get
            Return FMaDiemThu
        End Get
        Set(ByVal Value As String)
            FMaDiemThu = Value
        End Set
    End Property

    Public Property TCS_BanThu() As String
        Get
            Return FBanThu
        End Get
        Set(ByVal Value As String)
            FBanThu = Value
        End Set
    End Property

    Public Property TCS_Ngay_CT() As String
        Get
            Return FNgay_CT
        End Get
        Set(ByVal Value As String)
            FNgay_CT = Value
        End Set
    End Property

    Public Property TCS_DichSo() As String
        Get
            Return FDichSo
        End Get
        Set(ByVal Value As String)
            FDichSo = Value
        End Set
    End Property

    Public Property TCS_DS() As DataSet
        Get
            Return Fds
        End Get
        Set(ByVal Value As DataSet)
            Fds = Value
        End Set
    End Property

    Public Property TCS_Thuquy() As String
        Get
            Return FThuquy
        End Get
        Set(ByVal Value As String)
            FThuquy = Value
        End Set
    End Property

    Public Property TCS_TrangThai() As String
        Get
            Return FTrangThai
        End Get
        Set(ByVal Value As String)
            FTrangThai = Value
        End Set
    End Property

    Public Property TCS_Ten_KTT() As String
        Get
            Return FTen_KTT
        End Get
        Set(ByVal Value As String)
            FTen_KTT = Value
        End Set
    End Property

#End Region

#Region "Các thuộc tính của Phiếu xác nhận chứng từ"
    Private mHDR_So_CT As String = ""
    Private mHDR_Ma_DTNT As String = ""
    Private mHDR_TenDTNT As String = ""
    Private mHDR_KH_CT As String = ""
    Private mHDR_DiaChi As String = ""

    Public Property HDR_So_CT() As String
        Get
            Return mHDR_So_CT
        End Get
        Set(ByVal Value As String)
            mHDR_So_CT = Value
        End Set
    End Property
    Public Property HDR_Ma_DTNT() As String
        Get
            Return mHDR_Ma_DTNT
        End Get
        Set(ByVal Value As String)
            mHDR_Ma_DTNT = Value
        End Set
    End Property

    Public Property HDR_Ten_DTNT() As String
        Get
            Return mHDR_TenDTNT
        End Get
        Set(ByVal Value As String)
            mHDR_TenDTNT = Value
        End Set
    End Property
    Public Property HDR_KH_CT() As String
        Get
            Return mHDR_KH_CT
        End Get
        Set(ByVal Value As String)
            mHDR_KH_CT = Value
        End Set
    End Property
    Public Property HDR_DiaChi() As String
        Get
            Return mHDR_DiaChi
        End Get
        Set(ByVal Value As String)
            mHDR_DiaChi = Value
        End Set
    End Property
#End Region

#Region "Các thuộc tính của GNT từ BLT"
    Private mTen_KB_Tinh As String = ""
    Private mTen_VP As String = ""
    Private mNgay As String = ""
    Private mThang As String = ""
    Private mNam As String = ""
    Private mTen_Nguoi_Nop As String = ""
    Private mTen_TKNo As String = ""
    Private mTK_No As String = ""
    Private mTen_TKCo As String = ""
    Private mTK_Co As String = ""
    Private mNoi_dung As String = ""
    Private mSHKB As String = ""
    Private mMLNS As String = ""
    Private mSo_Tien As Double = 0
    Private mTien_Bang_Chu As String = ""

    Public Property Ten_KB_Tinh() As String
        Get
            Return mTen_KB_Tinh
        End Get
        Set(ByVal Value As String)
            mTen_KB_Tinh = Value
        End Set
    End Property
    Public Property Ten_VP() As String
        Get
            Return mTen_VP
        End Get
        Set(ByVal Value As String)
            mTen_VP = Value
        End Set
    End Property
    Public Property Ngay() As String
        Get
            Return mNgay
        End Get
        Set(ByVal Value As String)
            mNgay = Value
        End Set
    End Property
    Public Property Thang() As String
        Get
            Return mThang
        End Get
        Set(ByVal Value As String)
            mThang = Value
        End Set
    End Property
    Public Property Nam() As String
        Get
            Return mNam
        End Get
        Set(ByVal Value As String)
            mNam = Value
        End Set
    End Property
    Public Property Ten_Nguoi_Nop() As String
        Get
            Return mTen_Nguoi_Nop
        End Get
        Set(ByVal Value As String)
            mTen_Nguoi_Nop = Value
        End Set
    End Property
    Public Property Ten_TKNo() As String
        Get
            Return mTen_TKNo
        End Get
        Set(ByVal Value As String)
            mTen_TKNo = Value
        End Set
    End Property
    Public Property TK_No() As String
        Get
            Return mTK_No
        End Get
        Set(ByVal Value As String)
            mTK_No = Value
        End Set
    End Property
    Public Property Ten_TKCo() As String
        Get
            Return mTen_TKCo
        End Get
        Set(ByVal Value As String)
            mTen_TKCo = Value
        End Set
    End Property
    Public Property TK_Co() As String
        Get
            Return mTK_Co
        End Get
        Set(ByVal Value As String)
            mTK_Co = Value
        End Set
    End Property
    Public Property Noi_dung() As String
        Get
            Return mNoi_dung
        End Get
        Set(ByVal Value As String)
            mNoi_dung = Value
        End Set
    End Property
    Public Property SHKB() As String
        Get
            Return mSHKB
        End Get
        Set(ByVal Value As String)
            mSHKB = Value
        End Set
    End Property
    Public Property MLNS() As String
        Get
            Return mMLNS
        End Get
        Set(ByVal Value As String)
            mMLNS = Value
        End Set
    End Property
    Public Property So_Tien() As Double
        Get
            Return mSo_Tien
        End Get
        Set(ByVal Value As Double)
            mSo_Tien = Value
        End Set
    End Property
    Public Property Tien_Bang_Chu() As String
        Get
            Return mTien_Bang_Chu
        End Get
        Set(ByVal Value As String)
            mTien_Bang_Chu = Value
        End Set
    End Property

#End Region

#Region "Các thuộc tính cua tien gia"
    Private mdtFrom As Date
    Public Property dtFromDate()
        Get
            Return mdtFrom
        End Get
        Set(ByVal Value)
            mdtFrom = Value
        End Set
    End Property

    Private mdtTo As Date
    Public Property dtToDate()
        Get
            Return mdtTo
        End Get
        Set(ByVal Value)
            mdtTo = Value
        End Set
    End Property
#End Region

#Region "Các thuộc tính của Chi Tiết Lỗi"
    Private mstrTSNCode As String = ""

    Public Property TNSCode() As String
        Get
            Return mstrTSNCode
        End Get
        Set(ByVal Value As String)
            mstrTSNCode = Value
        End Set
    End Property
#End Region

#Region "Các thuộc tính của chứng từ"
    Private mKHCT As String = ""
    Private mSoCT As String = ""
    Private mKHBL As String = ""
    Private mSoBL As String = ""
    Private mMa_NNThue As String = ""
    Private mTen_NNThue As String = ""
    Private mDC_NNThue As String = ""
    Private mHuyen_NNThue As String = ""
    Private mTinh_NNThue As String = ""

    Private mMa_NNTien As String = ""
    Private mTen_NNTien As String = ""
    Private mDC_NNTien As String = ""
    Private mHuyen_NNTien As String = ""
    Private mTinh_NNTien As String = ""

    Private mNHA As String = ""
    Private mNHB As String = ""
    Private mTKNo As String = ""
    Private mTKCo As String = ""
    Private mTK_Co_KB As String = ""

    Private mTen_KB As String = ""
    Private mTinh_KB As String = ""
    Private mTen_CQThu As String = ""
    Private mMa_CQThu As String = ""

    Private mSo_TK As String = ""
    Private mNgay_TK As String = ""
    Private mLHXNK As String = ""
    Private mSo_BK As String = ""
    Private mNgay_BK As String = ""
    Private mDBHC As String = ""

    Private mTen_ThuQuy As String = ""
    Private mTen_KeToan As String = ""
    Private mTen_KTT As String = ""
    Private mKey_MauCT As String = "0"
    Private mIsTamThu As String = "0"
    'ICB: thêm vào theo mẫu BC mới
    'Hoàng Văn Anh
    Private mMa_Quy As String = ""
    Private mNgayNH As String = ""
    Private mCopy As String = ""
    Private mTK_GL_NH As String = ""
    Private mTT_KX As String = ""
    Private mInPhucHoi As String = ""
    Private mPT_TT As String = ""

    Private mSoQD As String = ""
    Private mNgayQD As String = ""
    Private mCQQD As String = ""

    Private mLyDo As String = ""
    Private mTien As String = ""
    Private mTen_Tinh As String = ""
    Private mTen_Huyen As String = ""

    Private mMaQuy As String = ""
    Private mTienMat As String = ""
    Private mSoLien As String = ""
    Private mTyGia As String = ""
    Private mTKKH As String = ""
    Private m_BaoCao As String = ""
    Public Property Mau_BCao() As String
        Get
            Return m_BaoCao
        End Get
        Set(ByVal Value As String)
            m_BaoCao = Value
        End Set
    End Property
    Public Property InPhucHoi() As String
        Get
            Return mInPhucHoi
        End Get
        Set(ByVal Value As String)
            mInPhucHoi = Value
        End Set
    End Property
    Public Property TK_Co_KB() As String
        Get
            Return mTK_Co_KB
        End Get
        Set(ByVal Value As String)
            mTK_Co_KB = Value
        End Set
    End Property
    Public Property TK_KH_NH() As String
        Get
            Return mTKKH
        End Get
        Set(ByVal Value As String)
            mTKKH = Value
        End Set
    End Property
    Public Property PT_TT() As String
        Get
            Return mPT_TT
        End Get
        Set(ByVal Value As String)
            mPT_TT = Value
        End Set
    End Property
    Public Property TT_KX() As String
        Get
            Return mTT_KX
        End Get
        Set(ByVal Value As String)
            mTT_KX = Value
        End Set
    End Property
    Public Property TK_GL_NH() As String
        Get
            Return mTK_GL_NH
        End Get
        Set(ByVal Value As String)
            mTK_GL_NH = Value
        End Set
    End Property

    Public Property Copy() As String
        Get
            Return mCopy
        End Get
        Set(ByVal Value As String)
            mCopy = Value
        End Set
    End Property
    Public Property NgayNH() As String
        Get
            Return mNgayNH
        End Get
        Set(ByVal Value As String)
            mNgayNH = Value
        End Set
    End Property
    Public Property Ma_Quy() As String
        Get
            Return mMa_Quy
        End Get
        Set(ByVal Value As String)
            mMa_Quy = Value
        End Set
    End Property
    Public Property KHCT() As String
        Get
            Return mKHCT
        End Get
        Set(ByVal Value As String)
            mKHCT = Value
        End Set
    End Property
    Public Property SoCT() As String
        Get
            Return mSoCT
        End Get
        Set(ByVal Value As String)
            mSoCT = Value
        End Set
    End Property
    Public Property KHBL() As String
        Get
            Return mKHBL
        End Get
        Set(ByVal Value As String)
            mKHBL = Value
        End Set
    End Property
    Public Property SoBL() As String
        Get
            Return mSoBL
        End Get
        Set(ByVal Value As String)
            mSoBL = Value
        End Set
    End Property
    Public Property Ma_NNThue() As String
        Get
            Return mMa_NNThue
        End Get
        Set(ByVal Value As String)
            mMa_NNThue = Value
        End Set
    End Property
    Public Property Ten_NNThue() As String
        Get
            Return mTen_NNThue
        End Get
        Set(ByVal Value As String)
            mTen_NNThue = Value
        End Set
    End Property
    Public Property DC_NNThue() As String
        Get
            Return mDC_NNThue
        End Get
        Set(ByVal Value As String)
            mDC_NNThue = Value
        End Set
    End Property
    Public Property Huyen_NNThue() As String
        Get
            Return mHuyen_NNThue
        End Get
        Set(ByVal Value As String)
            mHuyen_NNThue = Value
        End Set
    End Property
    Public Property Tinh_NNThue() As String
        Get
            Return mTinh_NNThue
        End Get
        Set(ByVal Value As String)
            mTinh_NNThue = Value
        End Set
    End Property

    Public Property Ma_NNTien() As String
        Get
            Return mMa_NNTien
        End Get
        Set(ByVal Value As String)
            mMa_NNTien = Value
        End Set
    End Property
    Public Property Ten_NNTien() As String
        Get
            Return mTen_NNTien
        End Get
        Set(ByVal Value As String)
            mTen_NNTien = Value
        End Set
    End Property
    Public Property DC_NNTien() As String
        Get
            Return mDC_NNTien
        End Get
        Set(ByVal Value As String)
            mDC_NNTien = Value
        End Set
    End Property
    Public Property Huyen_NNTien() As String
        Get
            Return mHuyen_NNTien
        End Get
        Set(ByVal Value As String)
            mHuyen_NNTien = Value
        End Set
    End Property
    Public Property Tinh_NNTien() As String
        Get
            Return mTinh_NNTien
        End Get
        Set(ByVal Value As String)
            mTinh_NNTien = Value
        End Set
    End Property

    Public Property NHA() As String
        Get
            Return mNHA
        End Get
        Set(ByVal Value As String)
            mNHA = Value
        End Set
    End Property
    Public Property NHB() As String
        Get
            Return mNHB
        End Get
        Set(ByVal Value As String)
            mNHB = Value
        End Set
    End Property
    Public Property TKNo() As String
        Get
            Return mTKNo
        End Get
        Set(ByVal Value As String)
            mTKNo = Value
        End Set
    End Property
    Public Property TKCo() As String
        Get
            Return mTKCo
        End Get
        Set(ByVal Value As String)
            mTKCo = Value
        End Set
    End Property

    Public Property Ten_KB() As String
        Get
            Return mTen_KB
        End Get
        Set(ByVal Value As String)
            mTen_KB = Value
        End Set
    End Property
    Public Property Tinh_KB() As String
        Get
            Return mTinh_KB
        End Get
        Set(ByVal Value As String)
            mTinh_KB = Value
        End Set
    End Property
    Public Property Ten_CQThu() As String
        Get
            Return mTen_CQThu
        End Get
        Set(ByVal Value As String)
            mTen_CQThu = Value
        End Set
    End Property
    Public Property Ma_CQThu() As String
        Get
            Return mMa_CQThu
        End Get
        Set(ByVal Value As String)
            mMa_CQThu = Value
        End Set
    End Property

    Public Property So_TK() As String
        Get
            Return mSo_TK
        End Get
        Set(ByVal Value As String)
            mSo_TK = Value
        End Set
    End Property
    Public Property Ngay_TK() As String
        Get
            Return mNgay_TK
        End Get
        Set(ByVal Value As String)
            mNgay_TK = Value
        End Set
    End Property
    Public Property LHXNK() As String
        Get
            Return mLHXNK
        End Get
        Set(ByVal Value As String)
            mLHXNK = Value
        End Set
    End Property
    Public Property So_BK() As String
        Get
            Return mSo_BK
        End Get
        Set(ByVal Value As String)
            mSo_BK = Value
        End Set
    End Property
    Public Property Ngay_BK() As String
        Get
            Return mNgay_BK
        End Get
        Set(ByVal Value As String)
            mNgay_BK = Value
        End Set
    End Property
    Public Property DBHC() As String
        Get
            Return mDBHC
        End Get
        Set(ByVal Value As String)
            mDBHC = Value
        End Set
    End Property

    Public Property Ten_ThuQuy() As String
        Get
            Return mTen_ThuQuy
        End Get
        Set(ByVal Value As String)
            mTen_ThuQuy = Value
        End Set
    End Property
    Public Property Ten_KeToan() As String
        Get
            Return mTen_KeToan
        End Get
        Set(ByVal Value As String)
            mTen_KeToan = Value
        End Set
    End Property
    Public Property Ten_KTT() As String
        Get
            Return mTen_KTT
        End Get
        Set(ByVal Value As String)
            mTen_KTT = Value
        End Set
    End Property
    Public Property Key_MauCT() As String
        Get
            Return mKey_MauCT
        End Get
        Set(ByVal Value As String)
            mKey_MauCT = Value
        End Set
    End Property
    Public Property IsTamThu() As String
        Get
            Return mIsTamThu
        End Get
        Set(ByVal Value As String)
            mIsTamThu = Value
        End Set
    End Property


    Public Property SoQD() As String
        Get
            Return mSoQD
        End Get
        Set(ByVal Value As String)
            mSoQD = Value
        End Set
    End Property
    Public Property NgayQD() As String
        Get
            Return mNgayQD
        End Get
        Set(ByVal Value As String)
            mNgayQD = Value
        End Set
    End Property
    Public Property CQQD() As String
        Get
            Return mCQQD
        End Get
        Set(ByVal Value As String)
            mCQQD = Value
        End Set
    End Property
    Public Property LyDo() As String
        Get
            Return mLyDo
        End Get
        Set(ByVal Value As String)
            mLyDo = Value
        End Set
    End Property
    Public Property Tien() As String
        Get
            Return mTien
        End Get
        Set(ByVal Value As String)
            mTien = Value
        End Set
    End Property
    Public Property Ten_Tinh() As String
        Get
            Return mTen_Tinh
        End Get
        Set(ByVal Value As String)
            mTen_Tinh = Value
        End Set
    End Property
    Public Property Ten_Huyen() As String
        Get
            Return mTen_Huyen
        End Get
        Set(ByVal Value As String)
            mTen_Huyen = Value
        End Set
    End Property
    Public Property MaQuy() As String
        Get
            Return mMaQuy
        End Get
        Set(ByVal Value As String)
            mMaQuy = Value
        End Set
    End Property
    Public Property TienMat() As String
        Get
            Return mTienMat
        End Get
        Set(ByVal Value As String)
            mTienMat = Value
        End Set
    End Property
    Public Property SoLien() As String
        Get
            Return mSoLien
        End Get
        Set(ByVal Value As String)
            mSoLien = Value
        End Set
    End Property
#End Region

#Region "Các thuộc tính của Kho Quỹ"
    Private mstrTenNV As String = ""
    Private mdblTongNop As Double = 0

    Public Property TenNV() As String
        Get
            Return mstrTenNV
        End Get
        Set(ByVal Value As String)
            mstrTenNV = Value
        End Set
    End Property

    Public Property TongNop() As Double
        Get
            Return mdblTongNop
        End Get
        Set(ByVal Value As Double)
            mdblTongNop = Value
        End Set
    End Property
#End Region

#Region "Form event"

    Private Sub AddParam(ByVal strName As String, ByVal strValue As String)
        Try
            'Dim ParamFields As ParameterFields = Me.rpvMain.ParameterFieldInfo()
            'Dim p_EmpID As New ParameterField
            'p_EmpID.Name = strName
            'Dim p_EmpID_Value As New ParameterDiscreteValue

            'If Not strValue Is Nothing Then
            '    p_EmpID_Value.Value = strValue
            '    p_EmpID.CurrentValues.Add(p_EmpID_Value)
            '    ParamFields.Add(p_EmpID)
            'Else
            '    p_EmpID_Value.Value = ""
            '    p_EmpID.CurrentValues.Add(p_EmpID_Value)
            '    ParamFields.Add(p_EmpID)
            'End If
            pdvTCS.Value = strValue
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields(strName).ApplyCurrentValues(pcTCS)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub GetReportInfo()
        '-----------------------------------------------------
        ' Mục đích: Lấy dữ liệu cho báo cáo       
        ' Ngày viết: 13/11/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        Try
            Dim v_strLoaiBaoCao As String = Request.QueryString.Item("BC")
            Select Case v_strLoaiBaoCao 'gLoaiBaoCao
                Case Report_Type.TCS_CTU_LASER
                    '  If (gintMauInCTUA4 = 0) Then
                    TCS_CTU_LASER()
                    '  Else
                    ' TCS_CTU_INKIM()
                    '   End If
                Case Report_Type.TCS_BLTHU
                    TCS_BLTHU()
                Case Report_Type.CTU_CHITIET
                    TCS_BKCTU_ALL("TCS_BKCTU_CHITIET.rpt")
                Case Report_Type.CTU_CQTCHITIET
                    TCS_BKCTU_ALL("TCS_BKCTU_CQTCHITIET.rpt")
                Case Report_Type.CTU_CQTTONG
                    TCS_BKCTU_ALL("TCS_BKCTU_CQTTONG.rpt")
                Case Report_Type.CTU_DBCHITIET
                    TCS_BKCTU_ALL("TCS_BKCTU_DBCHITIET.rpt")
                Case Report_Type.CTU_DBTONG
                    TCS_BKCTU_ALL("TCS_BKCTU_DBTONG.rpt")
                Case Report_Type.CTU_LOAITHUE
                    TCS_BKCTU_ALL("TCS_BKCTU_LOAITHUE.rpt")
                Case Report_Type.CTU_MANNT
                    TCS_BKCTU_ALL("TCS_BKCTU_MANNT.rpt")
                Case Report_Type.CTU_MLNS
                    TCS_BKCTU_ALL("TCS_BKCTU_MLNS.rpt")
                Case Report_Type.CTU_SOCT
                    TCS_BKCTU_ALL("TCS_BKCTU_SOCT.rpt")
                Case Report_Type.CTU_TKNO
                    TCS_BKCTU_ALL("TCS_BKCTU_TKNO.rpt")
                Case Report_Type.CTU_TKCO
                    TCS_BKCTU_ALL("TCS_BKCTU_TKCO.rpt")
                Case Report_Type.CTU_TTSOCT
                    TCS_BKCTU_ALL("TCS_BKCTU_TTSOCT.rpt")
                Case Report_Type.CTU_MAU10
                    TCS_CTU_MAU10()
                Case Report_Type.CTU_MAU10_1068
                    TCS_CTU_MAU10_1068()
                Case Report_Type.CTU_MAU10_1068_tong
                    TCS_CTU_MAU10_1068_tong()
                Case Report_Type.TCS_CTU_TC
                    TCS_CTU_TC()
                Case Report_Type.CTU_BKE
                    TCS_BKE_CTU()
                Case Report_Type.CTU_BKE_TB
                    TCS_BKE_CTU_TB()
                Case Report_Type.CTU_CQTHU
                    TCS_CTU_CQTHU()
                Case Report_Type.CTU_PHUCHOI_CK
                    TCS_CTU_PHUCHOI(True)
                Case Report_Type.CTU_PHUCHOI_TM
                    TCS_CTU_PHUCHOI(False)
                Case Report_Type.CTU_HUY
                    TCS_BKCTU_HUY()
                Case Report_Type.CTU_BKE_CQTHU
                    TCS_BKE_CTU_CQTHU()
                Case Report_Type.CTU_LIETKE
                    TCS_CTU_LIETKE()
                Case Report_Type.GNT_BLT
                    TCS_GNT_BLT()
                    'Chi tiết lỗi
                Case Report_Type.ERR_TDTT
                    TCS_ERR_TDTT()
                Case Report_Type.ERR_TDTT_09
                    TCS_ERR_TDTT_09()
                    'Bảng kê tra cứu           
                Case Report_Type.CTU_XACNHAN
                    TCS_CTU_XACNHAN()
                    'PHIEUKT
                Case Report_Type.PHIEUKT_DBTHU
                    TCS_PHIEUKT_ALL("TCS_PHIEUKT_DBTHU.rpt")
                Case Report_Type.PHIEUKT_TKCO
                    TCS_PHIEUKT_ALL("TCS_PHIEUKT_TKCO.rpt")
                Case Report_Type.PHIEUKT_TKNO
                    TCS_PHIEUKT_ALL("TCS_PHIEUKT_TKNO.rpt")
                Case Report_Type.SOTHUTM
                    TCS_SOTHUTM()
                Case Report_Type.KQ_SOTHU_TIENMAT
                    KQ_SOTHU_TIENMAT()
                Case Report_Type.KQ_BKE_TIENMAT
                    KQ_BKE_TIENMAT()
                Case Report_Type.KQ_SOQUY_TIENMAT
                    KQ_SOQUY_TIENMAT()
                Case Report_Type.DM_CAPCHUONG
                    TCS_DM_ALL("TCS_DM_CAP_CHUONG.rpt", "TCS_DM_CAP_CHUONG", buBaoCao.TCS_DM_CAP_CHUONG(), "In danh mục cấp - chương")
                Case Report_Type.DM_LOAIKHOAN
                    TCS_DM_ALL("TCS_DM_LOAI_KHOAN.rpt", "TCS_DM_LOAI_KHOAN", buBaoCao.TCS_DM_LOAI_KHOAN(), "In danh mục loại - khoản")
                Case Report_Type.DM_MUCTMUC
                    TCS_DM_ALL("TCS_DM_MUC_TMUC.rpt", "TCS_DM_MUC_TMUC", buBaoCao.TCS_DM_MUC_TMUC(), "In danh mục mục - tiểu mục")
                Case Report_Type.DM_DBHC
                    TCS_DM_ALL("TCS_DM_DBHC.rpt", "TCS_DM_DBHC", buBaoCao.TCS_DM_DBHC(), "In danh mục địa bàn hành chính")
                Case Report_Type.DM_CQQD
                    TCS_DM_ALL("TCS_DM_CQQD.rpt", "TCS_DM_CQQD", buBaoCao.TCS_DM_CQQD(), "In danh mục cơ quan quyết định")
                Case Report_Type.DM_CQTHU
                    TCS_DM_ALL("TCS_DM_CQTHU.rpt", "TCS_DM_CQTHU", buBaoCao.TCS_DM_CQThu(), "In danh mục cơ quan thu")
                Case Report_Type.DM_LOAITIEN
                    TCS_DM_ALL("TCS_DM_LOAITIEN.rpt", "TCS_DM_LOAITIEN", buBaoCao.TCS_DM_LOAITIEN(), "In danh mục loại tiền")
                Case Report_Type.DM_NGUYENTE
                    TCS_DM_ALL("TCS_DM_NGUYENTE.rpt", "TCS_DM_NGUYENTE", buBaoCao.TCS_DM_NGUYENTE(), "In danh mục nguyên tệ")
                Case Report_Type.DM_TYGIA
                    TCS_DM_ALL("TCS_DM_TYGIA.rpt", "TCS_DM_TYGIA", buBaoCao.TCS_DM_TYGIA(), "In danh mục tỷ giá")
                Case Report_Type.DM_TAIKHOAN
                    TCS_DM_ALL("TCS_DM_TAIKHOAN.rpt", "TCS_DM_TAIKHOAN", buBaoCao.TCS_DM_TAIKHOAN(), "In danh mục tài khoản")
                Case Report_Type.DM_NNT
                    TCS_DM_ALL("TCS_DM_NNT.rpt", "TCS_DM_NNT", buBaoCao.TCS_DM_NNT(), "In danh mục người nộp thuế")
                Case Report_Type.DM_LHTHU
                    TCS_DM_ALL("TCS_DM_LHTHU.rpt", "TCS_DM_LHTHU", buBaoCao.TCS_DM_LHTHU(), "In danh mục loại hình thu")
                Case Report_Type.DM_LOAITHUE
                    TCS_DM_ALL("TCS_DM_LOAITHUE.rpt", "TCS_DM_LTHUE", buBaoCao.TCS_DM_LOAITHUE(), "In danh mục loại thuế")
                Case Report_Type.DM_TLDT
                    TCS_DM_ALL("TCS_DM_TLDT.rpt", "TCS_DM_TLDT", buBaoCao.TCS_DM_TLDT(), "In danh mục tỷ lệ điều tiết")
                Case Report_Type.DM_NGANHANG
                    TCS_DM_ALL("TCS_DM_NGANHANG.rpt", "TCS_DM_NGANHANG", buBaoCao.TCS_DM_NGANHANG(), "In danh mục ngân hàng")
                Case Report_Type.DM_KHOBAC
                    TCS_DM_ALL("TCS_DM_KHOBAC.rpt", "TCS_DM_KHOBAC", buBaoCao.TCS_DM_KHOBAC(), "In danh mục kho bạc")
                Case Report_Type.DM_DIEMTHU
                    TCS_DM_ALL("TCS_DM_DIEMTHU.rpt", "TCS_DM_DIEMTHU", buBaoCao.TCS_DM_DIEMTHU(), "In danh mục điểm thu")
                Case Report_Type.DM_MAQUY
                    TCS_DM_ALL("TCS_DM_MAQUY.rpt", "DM_MAQUY", buBaoCao.TCS_DM_MAQUY(), "In danh mục mã quỹ")
                Case Report_Type.TCS_BLT_TC
                    TCS_BLT_TC()
                Case Report_Type.TCS_BKBLT_CT
                    TCS_BKBLT_CT()
                Case Report_Type.TCS_BK_DVQD
                    TCS_BK_DVQD()
                Case Report_Type.TCS_THOP_TPHAT
                    TCS_THOP_TPHAT()
                Case Report_Type.TCS_BK_PLTM
                    TCS_BK_PLTM()
                Case Report_Type.TCS_TC_STTHU
                    TCS_TC_STTHU()
                Case Report_Type.TCS_BBTG
                    TCS_BBTG()
                Case Report_Type.TCS_TinhHinhTG
                    TinhHinh_TG()
                Case Report_Type.TCS_TheoDoiTG
                    TheoDoi_TG()
                Case Report_Type.TCS_XuatNhapTG
                    XuatNhap_TG()
                Case Report_Type.TCS_BK_CTU_COA
                    TCS_BKE_CTU_COA()

                    'Trong ngoai gio va thu ho
                Case Report_Type.CTU_TRONGGIO
                    TCS_CTU_Tronggio()
                Case Report_Type.CTU_BCTHUE_PGB
                    TCS_BC_THUE_PGB()
                Case Report_Type.CTU_NGOAIGIO
                    TCS_CTU_Ngoaigio()
                Case Report_Type.CTU_ThuHo
                    TCS_CTU_THUHO()
                Case Report_Type.BKE_DOICHIEU
                    TCS_BKE_DOICHIEU()
                Case Report_Type.TCS_BC_TONG
                    TCS_BC_TONGCN()
                Case Report_Type.TCS_BC_NHCD
                    TCS_BC_TONGNHCD()
                Case Report_Type.TCS_BC_TONG_CT
                    TCS_BC_TONGCN_CT()
                Case Report_Type.TCS_BC_NHCD_CT
                    TCS_BC_TONGNHCD_CT()

                    ' Bao cao doi chieu
                Case Report_Type.DOICHIEU_1
                    DOI_CHIEU_1()
                Case Report_Type.DOICHIEU_2
                    DOI_CHIEU_2()
                Case Report_Type.DOICHIEU_3
                    DOI_CHIEU_3()
                Case Report_Type.DOICHIEU_4
                    DOI_CHIEU_4()
                Case Report_Type.DOICHIEU_5
                    DOI_CHIEU_5()
                Case Report_Type.DOICHIEU_6
                    DOI_CHIEU_6()
                Case Report_Type.DOICHIEU_7
                    DOI_CHIEU_7()
                Case Report_Type.DOICHIEU_8
                    DOI_CHIEU_8()
                Case Report_Type.HQ247_DOI_CHIEU_1
                    HQ247_DOI_CHIEU_1()
                Case Report_Type.HQ247_DOI_CHIEU_2
                    HQ247_DOI_CHIEU_2()
                Case Report_Type.TCS_BC_BL_SODU
                    TCS_BC_BL_SODU()

                Case Report_Type.TCS_BC_BL_DOANHSO
                    TCS_BC_BL_DOANHSO()

                Case Report_Type.TCS_BC_TH_BL_SODU
                    TCS_BC_TH_BL_SODU()

                Case Report_Type.TCS_BC_TH_BL_DOANHSO
                    TCS_BC_TH_BL_DOANHSO()
                Case Report_Type.TCS_BC_DC_02BKDC
                    TCS_BC_DC_02BKDC()
                Case Report_Type.BC_CHITIET_GD_NTDT
                    BC_CHITIET_GD_NTDT()
                Case Report_Type.DC_TONGHOP_PBBN
                    DC_TONGHOP_PBBN()
                Case Report_Type.DC_CHITIET_PBBN
                    DC_CHITIET_PBBN()
                Case Report_Type.BC_CHITIET_GD_NTDT_HQ247
                    BC_CHITIET_GD_NTDT_HQ247()
                Case Report_Type.BC_CHITIET_GD_HQ247_NHOTHU
                    BC_CHITIET_GD_HQ247_NHOTHU()
            End Select
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình Truy xuất báo cáo", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: GetReportInfo() " & ex.Source & vbNewLine _
            '    & "Error code: System error!" & vbNewLine _
            '    & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
    Private Sub BC_CHITIET_GD_HQ247_NHOTHU()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "In chi tiết giao dịch nộp thuế điện tử HQ247 Nhờ thu"
        strFileName = Server.MapPath("RPT\rptHQ247BC_GD_NTDT_NHOTHU.rpt")
        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbChiTietGDNTDT"
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("Tu_Ngay", FTungay)
            rptTCS.SetParameterValue("Den_Ngay", FDenngay)
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '            clsCommon.WriteLog(ex, "Lỗi trong quá trình In chi tiết giao dịch nộp thuế điện tử", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
    Private Sub BC_CHITIET_GD_NTDT_HQ247()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "In chi tiết giao dịch nộp thuế điện tử HQ247"
        strFileName = Server.MapPath("RPT\rptHQ247BC_GD_NTDT.rpt")
        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbChiTietGDNTDT"
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("Tu_Ngay", FTungay)
            rptTCS.SetParameterValue("Den_Ngay", FDenngay)
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi trong quá trình In chi tiết giao dịch nộp thuế điện tử", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub

    Private Sub frmShowReports_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Session.Item("User") Is Nothing Then
                Dim v_dt As DataTable = clsCTU.TCS_GetParams(CType(Session("User"), Integer))
                For Each v_dr As DataRow In v_dt.Rows
                    Select Case v_dr("TEN_TS").ToString
                        Case "MA_DT"
                            mv_strMaDiemThu = v_dr("GIATRI_TS").ToString
                        Case "SHKB"
                            mv_strSHKB = v_dr("GIATRI_TS").ToString
                        Case "KHCT"
                            mv_strKyHieuCT = v_dr("GIATRI_TS").ToString
                            'Case "KHCT"
                            '    mv_strDBThu = v_dr("GIATRI_TS").ToString
                            'Case "KHCT"
                            '    mv_strMaNHKB = v_dr("GIATRI_TS").ToString
                    End Select
                Next
            End If

            Dim frm As infBaoCao = Session("objBaoCao")
            Me.FNgay_CT = clsCommon.gf_CorrectString(frm.TCS_Ngay_CT)
            If Not frm.Fds Is Nothing Then
                Me.Fds = frm.Fds
            Else
                Exit Sub
            End If

            Me.TongNop = frm.TongNop
            Me.FThuquy = clsCommon.gf_CorrectString(frm.TCS_Thuquy)
            Me.FTen_KB = clsCommon.gf_CorrectString(frm.TCS_Ten_KB)
            Me.FMa_KB = clsCommon.gf_CorrectString(frm.TCS_Ma_KB)

            Me.KHCT = clsCommon.gf_CorrectString(frm.KHCT)
            Me.SoCT = clsCommon.gf_CorrectString(frm.SoCT)
            Me.Ten_NNThue = clsCommon.gf_CorrectString(frm.Ten_NNThue)
            Me.Ma_NNThue = clsCommon.gf_CorrectString(frm.Ma_NNThue)
            Me.DC_NNThue = clsCommon.gf_CorrectString(frm.DC_NNThue)
            Me.Huyen_NNThue = clsCommon.gf_CorrectString(frm.Huyen_NNThue)
            Me.Tinh_NNThue = clsCommon.gf_CorrectString(frm.Tinh_NNThue)
            Me.Ten_NNTien = clsCommon.gf_CorrectString(frm.Ten_NNTien)
            Me.Ma_NNTien = clsCommon.gf_CorrectString(frm.Ma_NNTien)
            Me.Ma_NNTien = clsCommon.gf_CorrectString(frm.Ma_NNTien)
            Me.Huyen_NNTien = clsCommon.gf_CorrectString(frm.Huyen_NNTien)
            Me.Tinh_NNTien = clsCommon.gf_CorrectString(frm.Tinh_NNTien)
            Me.TKNo = clsCommon.gf_CorrectString(frm.TKNo.Replace(".", ""))
            Me.TKCo = clsCommon.gf_CorrectString(frm.TKCo.Replace(".", ""))
            Me.TK_No = clsCommon.gf_CorrectString(frm.TK_No)
            Me.TK_Co = clsCommon.gf_CorrectString(frm.TK_Co)
            Me.Ten_KB = clsCommon.gf_CorrectString(frm.Ten_KB)
            Me.Tinh_KB = clsCommon.gf_CorrectString(frm.Tinh_KB)
            Me.Ma_CQThu = clsCommon.gf_CorrectString(frm.Ma_CQThu)
            Me.Ten_CQThu = clsCommon.gf_CorrectString(frm.Ten_CQThu)
            Me.So_TK = clsCommon.gf_CorrectString(frm.So_TK)
            Me.Ngay_TK = clsCommon.gf_CorrectString(frm.Ngay_TK)
            Me.LHXNK = clsCommon.gf_CorrectString(frm.LHXNK)
            Me.So_BK = clsCommon.gf_CorrectString(frm.So_BK)
            Me.Ngay_BK = clsCommon.gf_CorrectString(frm.Ngay_BK)
            Me.Tien_Bang_Chu = clsCommon.gf_CorrectString(frm.Tien_Bang_Chu)
            Me.SHKB = clsCommon.gf_CorrectString(frm.SHKB)
            Me.DBHC = clsCommon.gf_CorrectString(frm.DBHC)
            Me.Ten_ThuQuy = clsCommon.gf_CorrectString(frm.Ten_ThuQuy)
            Me.Ten_KeToan = clsCommon.gf_CorrectString(frm.Ten_KeToan)
            Me.Ten_KTT = clsCommon.gf_CorrectString(frm.Ten_KTT)
            Me.Ngay = clsCommon.gf_CorrectString(frm.Ngay)
            Me.Thang = clsCommon.gf_CorrectString(frm.Thang)
            Me.Nam = clsCommon.gf_CorrectString(frm.Nam)
            ' Me.IsTienNT = frm.IsTienNT
            Me.Key_MauCT = clsCommon.gf_CorrectString(frm.Key_MauCT)
            Me.IsTamThu = clsCommon.gf_CorrectString(frm.IsTamThu)
            Me.Tien = clsCommon.gf_CorrectString(frm.Tien)
            Me.LyDo = clsCommon.gf_CorrectString(frm.LyDo)
            Me.Ten_KeToan = clsCommon.gf_CorrectString(frm.Ten_KeToan)
            Me.CQQD = clsCommon.gf_CorrectString(frm.CQQD)
            Me.KHBL = clsCommon.gf_CorrectString(frm.KHBL)
            Me.NgayQD = clsCommon.gf_CorrectString(frm.NgayQD)
            Me.Ten_Tinh = clsCommon.gf_CorrectString(frm.Ten_Tinh)
            Me.Ten_Huyen = clsCommon.gf_CorrectString(frm.Ten_Huyen)
            Me.NgayNH = clsCommon.gf_CorrectString(frm.NgayNH)
            Me.Ma_Quy = clsCommon.gf_CorrectString(frm.Ma_Quy)
            Me.TK_GL_NH = clsCommon.gf_CorrectString(frm.TK_GL_NH)
            Me.Copy = clsCommon.gf_CorrectString(frm.Copy)
            Me.SoBL = clsCommon.gf_CorrectString(frm.SoBL)
            Me.SoQD = clsCommon.gf_CorrectString(frm.SoQD)
            Me.SoLien = clsCommon.gf_CorrectString(frm.SoLien)
            Me.FDichSo = clsCommon.gf_CorrectString(frm.TCS_DichSo)
            Me.FMaDiemThu = clsCommon.gf_CorrectString(frm.TCS_MaDiemThu)
            Me.TCS_Tungay = clsCommon.gf_CorrectString(frm.TCS_Tungay)
            Me.TCS_Denngay = clsCommon.gf_CorrectString(frm.TCS_Denngay)
            Me.mNgay_BK = clsCommon.gf_CorrectString(frm.Ngay_BK)
            Me.mNgay_TK = clsCommon.gf_CorrectString(frm.Ngay_TK)
            Me.TK_KH_NH = clsCommon.gf_CorrectString(frm.TK_KH_NH)
            Me.NHA = clsCommon.gf_CorrectString(frm.NHA)
            Me.TK_Co_KB = clsCommon.gf_CorrectString(frm.TK_CO_KB)
            Me.FNgay_NgoaiGio = clsCommon.gf_CorrectString(frm.Ngay_NgoaiGio)
            Me.FNgay_TrongGio = clsCommon.gf_CorrectString(frm.Ngay_TrongGio)
            Me.Fsomon_ngoaiGio = clsCommon.gf_CorrectString(frm.SoMon_NgoaiGio)
            Me.Fsomon_trongGio = clsCommon.gf_CorrectString(frm.SoMon_TrongGio)
            Me.FTien_ngoaiGio = clsCommon.gf_CorrectString(frm.TienNgoaiGio)
            Me.FTien_tronggio = clsCommon.gf_CorrectString(frm.TienTrongGio)
            Me.Mau_BCao = clsCommon.gf_CorrectString(frm.Mau_BaoCao)
            GetReportInfo()

            'Dim oStream As MemoryStream
            'oStream = DirectCast(rptTCS.ExportToStream(CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat), MemoryStream)
            'Response.Clear()
            'Response.Buffer = True
            'Response.ContentType = "application/pdf"
            'Response.ContentEncoding = Encoding.UTF8
            'Response.BinaryWrite(oStream.ToArray())
            'Response.[End]()

            'End If           
            'PrintToPrinter()
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '    clsCommon.WriteLog(ex, "Có lỗi trong quá trình lấy thông tin báo cáo", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: frmShowReports_Load() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & ex.Message & vbNewLine _
            '     & "Error message: " & ex.ToString, EventLogEntryType.Error)
        End Try
    End Sub

#End Region

#Region "TCS_CTU"
    Private Sub TCS_CTU_LASER()
        Try
            Dim strFileName As String = ""
            Dim v_strReportName As String = "In chứng từ thu ngân sách"
            strFileName = Server.MapPath("RPT\TCS_CTU_LASER.rpt")
            If (Not File.Exists(strFileName)) Then
            End If
            rptTCS.Load(strFileName)
            Fds.Tables(0).TableName = "CTU"
            rptTCS.SetDataSource(Fds)

            AddParam("NgayNH", mNgayNH)
            AddParam("Ma_Quy", mMa_Quy)
            AddParam("Copy", mCopy)
            AddParam("TK_GL_NH", mTK_GL_NH)
            'mTK_GL_NH{2?TK_KN_NH}
            AddParam("TK_KN_NH", mTKKH)
            AddParam("KHCT", mKHCT)
            AddParam("SoCT", mSoCT)
            AddParam("Ten_NNThue", mTen_NNThue)
            AddParam("Ma_NNThue", mMa_NNThue)
            AddParam("DC_NNThue", mDC_NNThue)
            AddParam("Huyen_NNThue", mHuyen_NNThue)
            AddParam("Tinh_NNThue", mTinh_NNThue)

            AddParam("Ten_NNTien", mTen_NNTien)
            AddParam("Ma_NNTien", mMa_NNTien)
            AddParam("DC_NNTien", mDC_NNTien)
            AddParam("Huyen_NNTien", mHuyen_NNTien)
            AddParam("Tinh_NNTien", mTinh_NNTien)

            AddParam("NHA", mNHA)
            'AddParam("NHB", mNHB)
            AddParam("TKNo", mTKNo)
            AddParam("TKCo", TK_Co)
            'AddParam("TKCo", ParmValues_Load("NH_GL_TG"))
            AddParam("TK_CO_NS", mTK_Co_KB)
            AddParam("Ten_KB", mTen_KB)
            AddParam("Tinh_KB", mTinh_KB)
            AddParam("Ma_CQThu", mMa_CQThu)
            AddParam("Ten_CQThu", mTen_CQThu)

            AddParam("So_TK", mSo_TK)
            AddParam("Ngay_TK", mNgay_TK)
            AddParam("LHXNK", mLHXNK)
            AddParam("So_BK", mSo_BK)
            AddParam("Ngay_BK", mNgay_BK)

            AddParam("Tien_Bang_Chu", mTien_Bang_Chu)
            AddParam("SHKB", mSHKB)
            AddParam("DBHC", mDBHC)
            AddParam("Ten_ThuQuy", mTen_ThuQuy)
            AddParam("Ten_KeToan", mTen_KeToan)

            AddParam("Key_MauCT", mKey_MauCT)
            AddParam("IsTamThu", mIsTamThu)


            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Có lỗi trong quá trình In chứng từ thu ngân sách", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_CTU_LASER() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Public Sub PrintToPrinter()
        '-----------------------------------------------------
        ' Mục đích: In trực tiếp báo cáo ra máy in       
        ' Ngày viết: 13/11/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        Try
            GetReportInfo()
            rptTCS.PrintToPrinter(1, False, 0, 0)
            FblnPrintSuccess = True
        Catch ex As Exception
            FblnPrintSuccess = False
           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace)'
        End Try
    End Sub


    Private Sub TCS_CTU_INKIM()
        Try
            Dim strFileName As String = ""
            Dim v_strReportName As String = "In chứng từ thu ngân sách"
            If (Not mblnTienNT) Then
                strFileName = Server.MapPath("RPT\TCS_CTU_IK_TM.rpt")
            Else
                strFileName = Server.MapPath("RPT\TCS_CTU_IK_NT.rpt")
            End If
            If (Not File.Exists(strFileName)) Then

            End If
            rptTCS.Load(strFileName)
            Fds.Tables(0).TableName = "CTU"

            rptTCS.SetDataSource(Fds)
            AddParam("KHCT", mKHCT)
            AddParam("SoCT", mSoCT)
            AddParam("Ten_NNThue", mTen_NNThue)
            AddParam("Ma_NNThue", mMa_NNThue)
            AddParam("DC_NNThue", mDC_NNThue)
            AddParam("Huyen_NNThue", mHuyen_NNThue)
            AddParam("Tinh_NNThue", mTinh_NNThue)

            AddParam("Ten_NNTien", mTen_NNTien)
            AddParam("Ma_NNTien", mMa_NNTien)
            AddParam("DC_NNTien", mDC_NNTien)
            AddParam("Huyen_NNTien", mHuyen_NNTien)
            AddParam("Tinh_NNTien", mTinh_NNTien)

            'AddParam("NHA", mNHA)
            'AddParam("NHB", mNHB)
            AddParam("TKNo", mTKNo)
            AddParam("TKCo", mTKCo)

            AddParam("Ten_KB", mTen_KB)
            AddParam("Tinh_KB", mTinh_KB)
            AddParam("Ma_CQThu", mMa_CQThu)
            AddParam("Ten_CQThu", mTen_CQThu)

            AddParam("So_TK", mSo_TK)
            AddParam("Ngay_TK", mNgay_TK)
            AddParam("LHXNK", mLHXNK)
            AddParam("So_BK", mSo_BK)
            AddParam("Ngay_BK", mNgay_BK)

            AddParam("Tien_Bang_Chu", mTien_Bang_Chu)
            AddParam("SHKB", mSHKB)
            AddParam("DBHC", mDBHC)
            AddParam("Ten_ThuQuy", mTen_ThuQuy)
            AddParam("Ten_KeToan", mTen_KeToan)
            AddParam("Ten_KTT", mTen_KTT)

            AddParam("Key_MauCT", mKey_MauCT)
            AddParam("IsTamThu", mIsTamThu)
            AddParam("MaQuy", mMaQuy)

            AddParam("Ngay", mNgay)
            AddParam("Thang", mThang)
            AddParam("Nam", mNam)

            If (mblnTienNT) Then
                AddParam("TyGia", mTyGia)
            End If
            AddParam("SoLien", mSoLien)


            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperLetter
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình In chứng từ thu ngân sách", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_CTU_LASER() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Private Sub TCS_BLTHU()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "In biên lai thu"
        If (gintMauInCTUA4 = 0) Then
            strFileName = Server.MapPath("RPT\TCS_BLTHU.rpt")
        Else
            If (gintMauInKimBLT = 0) Then
                strFileName = Server.MapPath("RPT\TCS_BLTHU_IK.rpt")
            Else
                strFileName = Server.MapPath("RPT\TCS_BLTHU_IK_A5.rpt")
            End If
        End If

        If (Not File.Exists(strFileName)) Then

        End If
        Try
            rptTCS.Load(strFileName)

            AddParam("DVThu", mTen_KB)
            AddParam("KH_BL", mKHBL)
            AddParam("So_BL", mSoBL)
            AddParam("Ten_NNT", mTen_NNThue)
            AddParam("DiaChi", mDC_NNThue)
            AddParam("SoQD", mSoQD)
            AddParam("NgayQD", mNgayQD)
            AddParam("CQQD", mCQQD)
            AddParam("Tien_Bang_Chu", mTien_Bang_Chu)

            AddParam("Ngay", mNgay)
            AddParam("Thang", mThang)
            AddParam("Nam", mNam)

            AddParam("LyDo", mLyDo)
            AddParam("Tien", mTien)
            AddParam("Ten_Huyen", mTen_Huyen)
            AddParam("Ten_Tinh", mTen_Tinh)
            AddParam("SoLien", mSoLien)

            '---------------------------------------------------------
            If (gintMauInCTUA4 = 0) Then
                rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
                rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
            Else
                If (gintMauInKimBLT = 0) Then
                    rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperLetter
                    rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
                Else
                    rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperStatement
                    rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape
                End If
            End If

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
            'Me.'Cursor = System.Windows.Forms.Cursors.Default
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình In biên lai thu", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_BLTHU() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Private Sub TCS_CTU_PHUCHOI(ByVal blnChuyenKhoan As Boolean)
        Dim strFileName As String = ""
        If (blnChuyenKhoan) Then
            Dim v_strReportName As String = "In chứng từ phục hồi chuyển khoản"
        Else
            Dim v_strReportName As String = "In chứng từ phục hồi tiền mặt"
        End If
        strFileName = Server.MapPath("RPT\TCS_CTU_PHUCHOI.rpt")
        If (Not File.Exists(strFileName)) Then
        End If
        Try
            rptTCS.Load(strFileName)
            Fds.Tables(0).TableName = "CTU_PHUCHOI"
            rptTCS.SetDataSource(Fds)

            AddParam("Ten_KB", mTen_KB)
            AddParam("Tinh_KB", mTinh_KB)
            AddParam("TienMat", mTienMat)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình In chứng từ phục hồi", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_CTU_PHUCHOI() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
#End Region

#Region "TCS_CTU_Xử lý cuối ngày"

    Private Sub TCS_BKCTU_ALL(ByVal strReportFileName As String)
        '-----------------------------------------------------
        ' Mục đích: Lập các mẫu bảng kê chứng từ cuối ngày
        ' Ngày viết: 15/12/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê chứng từ"
        strFileName = Server.MapPath("RPT\" & strReportFileName)
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("Ten_KB", FTen_KB)
            AddParam("MaDiemThu", FMaDiemThu)
            AddParam("Ban_So", FBanThu)
            AddParam("Ngay_CT", FNgay_CT)
            AddParam("Bang_Chu", FDichSo)

            Select Case strReportFileName.Trim().ToUpper()
                Case "TCS_BKCTU_CHITIET.RPT"
                    rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
                    rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape
                Case Else
                    rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
                    rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
            End Select

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
            'Me.'Cursor = System.Windows.Forms.Cursors.Default
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '    clsCommon.WriteLog(ex, "Có lỗi trong quá trình In bảng kê chứng từ", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_BKCTU_ALL() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
    Private Sub SetPrinterPaperSize(ByVal strPaperName As String)
        Try
            Dim PrintDoc As New PrintDocument
            Dim myPagesize As System.Drawing.Printing.PaperSize

            Dim i As Integer = 0
            PrintDoc.PrintController = New StandardPrintController

            ' Tìm và gán form DiemThu cho report
            For Each myPagesize In PrintDoc.PrinterSettings.PaperSizes
                If PrintDoc.PrinterSettings.PaperSizes(i).PaperName = strPaperName Then
                    PrintDoc.DefaultPageSettings.PaperSize = PrintDoc.PrinterSettings.PaperSizes(i)
                    Exit For
                End If
                i += 1
            Next
        Catch ex As Exception
         log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace)'
        End Try
    End Sub

    Private Sub TCS_PHIEUKT_ALL(ByVal strReportFileName As String)
        '-----------------------------------------------------
        ' Mục đích: Lập các mẫu phiếu kế toán chứng từ cuối ngày
        ' Ngày viết: 15/12/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê phiếu kế toán"
        strFileName = Server.MapPath("RPT\" & strReportFileName)
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("Ten_KB", FTen_KB)
            AddParam("MaDiemThu", FMaDiemThu)
            AddParam("Ban_So", FBanThu)
            AddParam("Ngay_KT", FNgay_CT)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
            'Me.'Cursor = System.Windows.Forms.Cursors.Default

            'SetPrinterPaperSize("DiemThu")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình In bảng kê phiếu kế toán", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_PHIEUKT_ALL() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub


    Private Sub TCS_CTU_LIETKE()
        '-----------------------------------------------------
        ' Mục đích: Lập mẫu bảng liệt kê chứng từ 
        ' Ngày viết: 109/06/2008
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê liệt chứng từ"
        strFileName = Server.MapPath("RPT\TCS_CTU_LIETKE.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("Ten_KB", FTen_KB)
            AddParam("Ma_KB", FMa_KB)
            AddParam("MaDiemThu", FMaDiemThu)
            AddParam("Ngay", FNgay_CT)
            AddParam("Ten_KTT", FTen_KTT)
            AddParam("Ten_KeToan", mTen_KeToan)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Có lỗi trong quá trình In bảng kê liệt chứng từ", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_CTU_LIETKE() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try

    End Sub


    Private Sub TCS_BKCTU_HUY()
        '-----------------------------------------------------
        ' Mục đích: Lập mẫu bảng kê chứng từ hủy
        ' Ngày viết: 15/12/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê chứng từ đã hủy"
        strFileName = Server.MapPath("RPT\TCS_BK_CTU_HUY.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("Ten_KB", FTen_KB)
            '  AddParam("Ma_KB", FMa_KB)
            AddParam("MaDiemThu", FMaDiemThu)
            AddParam("Ngay", FNgay_CT)
            ' AddParam("Ten_KTT", FTen_KTT)
            AddParam("Ten_KeToan", mTen_KeToan)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình In bảng kê chứng từ đã hủy", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_CTU_LIETKE() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Private Sub TCS_SOTHUTM()
        '-----------------------------------------------------
        ' Mục đích: Lập mẫu bảng kê tiền mặt
        ' Ngày viết: 15/12/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê sổ thu tiền mặt"
        strFileName = Server.MapPath("RPT\TCS_SOTHUTM.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)

            rptTCS.SetDataSource(Fds)

            AddParam("Ten_KB", FTen_KB)
            AddParam("MaDiemThu", FMaDiemThu)
            AddParam("Ban_So", FBanThu)
            AddParam("Ngay_ST", FNgay_CT)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình In bảng kê sổ thu tiền mặt", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_SOTHUTM() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Private Sub TCS_CTU_TC()
        '-----------------------------------------------------
        ' Mục đích: Lập bảng kê tra cứu chứng từ
        ' Ngày viết: 08/08/2008
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê chứng từ"
        strFileName = Server.MapPath("RPT\TCS_CTU_TC.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("Ten_KB", FTen_KB)
            AddParam("Ngay_CT", FNgay_CT)
            AddParam("Bang_Chu", FDichSo)
            AddParam("Trang_Thai", FTrangThai)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình In bảng kê chứng từ", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_CTU_TC() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub


    Private Sub TCS_CTU_MAU10()
        '-----------------------------------------------------
        ' Mục đích: Lập mẫu bảng kê chứng từ theo mẫu 10
        ' Ngày viết: 09/09/2008
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê chứng từ"
        strFileName = Server.MapPath("RPT\TCS_BKE_CTU10.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("TenTinh", mTen_KB_Tinh)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Có lỗi trong quá trình In bảng kê chứng từ", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_CTU_MAU10() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub


    Private Sub TCS_CTU_MAU10_1068()
        '-----------------------------------------------------
        ' Mục đích: Lập mẫu bảng kê chứng từ theo mẫu 10
        ' Ngày viết: 09/09/2008
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê chứng từ"
        strFileName = Server.MapPath("RPT\TCS_BKE_CTU10_1068.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("TenTinh", FTen_KB)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Có lỗi trong quá trình In bảng kê chứng từ", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_CTU_MAU10_1068() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
    Private Sub TCS_CTU_MAU10_1068_tong()
        '-----------------------------------------------------
        ' Mục đích: Lập mẫu bảng kê chứng từ theo mẫu 10
        ' Ngày viết: 09/09/2008
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê chứng từ"
        strFileName = Server.MapPath("RPT\TCS_BKE_CTU10_1068_tong.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("TenTinh", FTen_KB)
            AddParam("Ngay_tronggio", FNgay_TrongGio)
            AddParam("somon_tronggio", Fsomon_trongGio)
            AddParam("tien_tronggio", FTien_tronggio)
            AddParam("Ngay_ngoaigio", FNgay_NgoaiGio)
            AddParam("somon_ngoaigio", Fsomon_ngoaiGio)
            AddParam("tien_ngoaigio", FTien_ngoaiGio)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình In bảng kê chứng từ", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_CTU_MAU10_1068() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
#End Region

#Region "TCS_CTU_Tra cứu"
    Private Sub TCS_CTU_XACNHAN()
        '-----------------------------------------------------
        ' Mục đích: In chứng từ xác nhận
        ' Ngày viết: 15/12/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In xác nhận chứng từ"
        strFileName = Server.MapPath("RPT\TCS_PHIEU_XACNHAN.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            pdvTCS.Value = FTen_KB
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ten_KB").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = FMaDiemThu
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("MaDiemthu").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = FNgay_CT
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("HDR_NgayNop").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = FDichSo
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("HDR_BangChu").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mHDR_KH_CT
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("HDR_KH_CT").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mHDR_Ma_DTNT
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("HDR_Ma_DTNT").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mHDR_So_CT
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("HDR_So_CT").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mHDR_TenDTNT
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("HDR_TenDTNT").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mHDR_DiaChi
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("HDR_DiaChi").ApplyCurrentValues(pcTCS)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Có lỗi trong quá trình In xác nhận chứng từ", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_CTU_XACNHAN() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
#End Region

#Region "GNT_BLT"

    Private Sub TCS_GNT_BLT()
        '-----------------------------------------------------
        ' Mục đích: In chứng từ phục hồi và chuyển khoản
        ' Ngày viết: 15/05/2008
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""

        Dim v_strReportName As String = "In giấy nộp tiền tổng hợp từ biên lai thu"
        strFileName = Server.MapPath("RPT\TCS_GNT_BLT.rpt")

        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)

            pdvTCS.Value = mTen_KB_Tinh
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ten_KB_Tinh").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mTen_VP
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ten_VP").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mNgay
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ngay").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mTen_Nguoi_Nop
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ten_Nguoi_Nop").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mTen_TKNo
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ten_TKNo").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mTK_No
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("TK_No").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mTen_TKCo
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ten_TKCo").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mTK_Co
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("TKCo").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mNoi_dung
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Noi_dung").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mSHKB
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("SHKB").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mMLNS
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("MLNS").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mSo_Tien
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("So_Tien").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mTien_Bang_Chu
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Tien_Bang_Chu").ApplyCurrentValues(pcTCS)

            '---------------------------------------------------------
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Có lỗi trong quá trình In giấy nộp tiền tổng hợp từ biên lai thu", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_GNT_BLT() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
#End Region

#Region "DANH MUC"
    Private Sub TCS_DM_ALL(ByVal strReportFileName As String, _
        ByVal strTableName As String, ByVal ds As DataSet, _
        ByVal strTitle As String)
        '-----------------------------------------------------
        ' Mục đích: In các danh mục hệ thống
        ' Ngày viết: 13/11/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = strTitle
        strFileName = Server.MapPath("RPT\" & strReportFileName)
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)

            Fds = ds
            Fds.Tables(0).TableName = strTableName

            rptTCS.SetDataSource(Fds)

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Có lỗi trong quá trình In các danh mục hệ thống", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_DM_ALL() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
#End Region

#Region "AnhHV4: Báo cáo tiền giả"
    Private Sub TinhHinh_TG()
        Try
            'Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim strFileName As String = Server.MapPath("RPT\TCS_BC_TinhHinhThuGiuTG.rpt")
            If (Not File.Exists(strFileName)) Then
                'MsgBox("Không tìm thấy file report : '" & strFileName & "'!", MsgBoxStyle.Critical, "Chú Ý")
                'Return
            End If
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)
            '/// Ten kho bac
            pdvTCS.Value = mv_strTenKB
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ten_KB").ApplyCurrentValues(pcTCS)
            ' Ngày
            pdvTCS.Value = mNgay
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ngay").ApplyCurrentValues(pcTCS)
            '/// dinh dang A4
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Portrait

            Dim v_strReportName As String = " Báo cáo tình hình thu giữ tiền giả."
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
            'frmReport.rpTienGiaView.Refresh()
            'Cursor = System.Windows.Forms.Cursors.Default
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình In báo cáo tình hình tiền giả", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TinhHinh_TG() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub


    Private Sub TheoDoi_TG()
        Try
            'Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim strFileName As String = Server.MapPath("RPT\TCS_BC_TheoDoiThuGiuTG.rpt")
            If (Not File.Exists(strFileName)) Then
                'MsgBox("Không tìm thấy file report : '" & strFileName & "'!", MsgBoxStyle.Critical, "Chú Ý")
                'Return
            End If
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)
            '/// Ten kho bac
            pdvTCS.Value = mv_strTenKB
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ten_KB").ApplyCurrentValues(pcTCS)
            pdvTCS.Value = mNgay
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ngay").ApplyCurrentValues(pcTCS)
            '/// dinh dang A4
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA3
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Landscape

            Dim v_strReportName As String = " Báo cáo theo dõi thu giữ tiền giả."
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình In báo cáo theo dõi thu giữ tiền giả", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TheoDoi_TG() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try

    End Sub

    Private Sub XuatNhap_TG()
        Try
            'Cursor = System.Windows.Forms.Cursors.WaitCursor
            Dim strFileName As String = Server.MapPath("RPT\TCS_BC_TheoDoiNhapXuatTG.rpt")
            If (Not File.Exists(strFileName)) Then
                'MsgBox("Không tìm thấy file report : '" & strFileName & "'!", MsgBoxStyle.Critical, "Chú Ý")
                'Return
            End If
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)
            '/// Ten kho bac
            pdvTCS.Value = mv_strTenKB
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ten_KB").ApplyCurrentValues(pcTCS)
            pdvTCS.Value = mNgay
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ngay").ApplyCurrentValues(pcTCS)
            '/// dinh dang A4
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.Shared.PaperOrientation.Landscape

            Dim v_strReportName As String = " Báo cáo theo dõi nhập, xuất tiền giả."
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Có lỗi trong quá trình In báo cáo theo dõi nhập, xuất tiền giả", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: XuatNhap_TG() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
#End Region

#Region "TCS_BKE_CTU_CQTHU"
    Private Sub TCS_CTU_CQTHU()
        '-----------------------------------------------------
        ' Mục đích: Lập mẫu bảng kê chứng từ cho cơ quan thu
        ' Ngày viết: 15/12/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê chứng từ"
        '  Select Case cqnhan
        'Case CQNhanEnum.ALL
        strFileName = Server.MapPath("RPT\TCS_CTU_CQTHU.rpt")
        '  Case CQNhanEnum.HAIQUAN
        'strFileName = System.Windows.Forms.Application.StartupPath & "\Reports\TCS_CTU_CQTHU_HAIQUAN.rpt"
        '   Case CQNhanEnum.THUE
        ' strFileName = System.Windows.Forms.Application.StartupPath & "\Reports\TCS_CTU_CQTHU_THUE.rpt"
        ' End Select

        If (Not File.Exists(strFileName)) Then
            Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("Ten_KB", FTen_KB)
            AddParam("Tu_Ngay", FTungay)
            AddParam("Den_Ngay", FDenngay)
            AddParam("Tien_Bang_Chu", FDichSo)
            AddParam("Ten_KTT", FTen_KTT)
            AddParam("Ten_KeToan", mTen_KeToan)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape

            rpvMain.ReportSource = rptTCS
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình In bảng kê chứng từ", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_BKE_CTU_CQTHU() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
    Private Sub TCS_BKE_CTU()
        '-----------------------------------------------------
        ' Mục đích: Lập mẫu bảng kê chứng từ cho cơ quan thu
        ' Ngày viết: 15/12/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê chứng từ"
        strFileName = Server.MapPath("RPT\TCS_BKE_CTU.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("Ten_KB", FTen_KB)
            AddParam("Tu_Ngay", FTungay)
            AddParam("Den_Ngay", FDenngay)
            AddParam("Tien_Bang_Chu", FDichSo)
            AddParam("Ten_KTT", FTen_KTT)
            AddParam("Ten_KeToan", mTen_KeToan)
            AddParam("MaDiemThu", FMaDiemThu)
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

            'Me.'Cursor = System.Windows.Forms.Cursors.Default
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Có lỗi trong quá trình In bảng kê chứng từ", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_BKE_CTU() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
    Private Sub TCS_BKE_CTU_COA()
        '-----------------------------------------------------
        ' Mục đích: Lập mẫu bảng kê chứng từ khi KX ở điểm thu ngoài
        ' Ngày viết: 05/12/2008
        ' Người viết: Hoàng Văn Anh
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê chứng từ"
        strFileName = Server.MapPath("RPT\TCS_BK_CT_COA.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("Ten_KB", FTen_KB)
            AddParam("Ten_DT", TCS_MaDiemThu)
            'AddParam("Tu_Ngay", FTungay)
            'AddParam("Den_Ngay", FDenngay)
            'AddParam("Tong_Tien", FDichSo)
            'AddParam("Ten_KTT", FTen_KTT)
            'AddParam("Ten_KeToan", mTen_KeToan)
            'AddParam("Ngay_KB", Now.ToString("dd/MM/yyyy")


            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Có lỗi trong quá trình In bảng kê chứng từ", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_BKE_CTU_COA() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try

    End Sub

    Private Sub TCS_BKE_CTU_TB()
        '-----------------------------------------------------
        ' Mục đích: Lập mẫu bảng kê chứng từ chi tiết cho loại thuế trước bạ
        ' Ngày viết: 23/07/2008
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê chứng từ"
        strFileName = Server.MapPath("RPT\TCS_BKE_CTU_TB.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("Ten_KB", FTen_KB)
            AddParam("Tu_Ngay", FTungay)
            AddParam("Den_Ngay", FDenngay)
            AddParam("Tien_Bang_Chu", FDichSo)
            AddParam("Ten_KTT", FTen_KTT)
            AddParam("Ten_KeToan", mTen_KeToan)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình In bảng kê chứng từ", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_BKE_CTU_TB() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Private Sub TCS_BKE_CTU_CQTHU()
        '-----------------------------------------------------
        ' Mục đích: In bảng kê chứng từ chi tiết
        ' Ngày viết: 23/12/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê chứng từ"
        strFileName = Server.MapPath("RPT\TCS_BKE_CTU_CQTHU.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            pdvTCS.Value = FTen_KB
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("P_Ten_KBac").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = FThuquy
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("P_Ten_NL").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = FNgay_CT
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("P_Ngay").ApplyCurrentValues(pcTCS)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Có lỗi trong quá trình In bảng kê chứng từ", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_BKE_CTU_CQTHU() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Private Sub TCS_KT_CNgay()
        '-----------------------------------------------------
        ' Mục đích: In báo cáo chứng từ sai cuối ngày
        ' Ngày viết: 22/12/2008
        ' Người viết: Nguyễn Mạnh Tuấn
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "Danh sách các chứng từ sai"
        strFileName = Server.MapPath("RPT\TCS_KTCNgay.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Có lỗi trong quá trình In danh sách các chừng từ sai", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_KT_CNgay() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
#End Region

#Region "Chi tiết lỗi"

    Private Sub TCS_ERR_TDTT_THAMSO(ByVal strTitle As String, ByVal strHdr0 As String, ByVal strHdr1 As String)
        '-----------------------------------------------------
        ' Mục đích: Truyền tham số cho Báo cáo chi tiết lỗi của quá trình TDTT
        ' Ngày viết: 25/12/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        Try
            Dim v_strReportName As String = strTitle

            pdvTCS.Value = strTitle
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("TieuDe").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = strHdr0
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Hdr0").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = strHdr1
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Hdr1").ApplyCurrentValues(pcTCS)
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi truyền tham số cho Báo cáo chi tiết lỗi của quá trình TDTT", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_ERR_TDTT_THAMSO() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Private Sub TCS_ERR_TDTT()
        '-----------------------------------------------------
        ' Mục đích: Báo cáo chi tiết lỗi của quá trình trao đổi thông tin
        ' Ngày viết: 25/12/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        Try
            'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

            Dim strFileName As String = Server.MapPath("RPT\TCS_ERR_TDTT.rpt")
            If (Not File.Exists(strFileName)) Then
                'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'Me.Close()
                'Return
            End If

            rptTCS.Load(strFileName)

            Select Case mstrTSNCode
                Case "01"
                    TCS_ERR_TDTT_THAMSO("Danh sách lỗi khi nhận danh mục Cấp - Chương", "Mã cấp", "Mã chương")
                Case "02"
                    TCS_ERR_TDTT_THAMSO("Danh sách lỗi khi nhận danh mục Loại - Khoản", "Mã loại", "Mã khoản")
                Case "03"
                    TCS_ERR_TDTT_THAMSO("Danh sách lỗi khi nhận danh mục Mục - Tiểu mục", "Mã mục", "Mã tiểu mục")
                Case "09"
                    TCS_ERR_TDTT_THAMSO("Danh sách lỗi khi nhận danh mục người nộp thuế", "Mã NNT", "Tên NNT")
                Case "21"
                    TCS_ERR_TDTT_THAMSO("Danh sách lỗi khi nhận danh sách sổ thuế", "Mã NNT", "Tên NNT")
                Case "25"
                    TCS_ERR_TDTT_THAMSO("Danh sách lỗi khi nhận danh sách tờ khai", "Mã NNT", "Tên NNT")
                Case Else
                    'MessageBox.Show("Thông tin TNSCode không chính xác!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'Me.Close()
                    'Return
            End Select

            Fds.Tables(0).TableName = "TCS_ERR_LST"
            rptTCS.SetDataSource(Fds)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi Báo cáo chi tiết lỗi của quá trình trao đổi thông tin", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_ERR_TDTT() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Private Sub TCS_ERR_TDTT_09()
        '-----------------------------------------------------
        ' Mục đích: Báo cáo chi tiết lỗi của quá trình trao đổi thông tin
        ' Ngày viết: 25/12/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        Try
            'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

            Dim strFileName As String = Server.MapPath("RPT\TCS_ERR_TDTT_09.rpt")
            If (Not File.Exists(strFileName)) Then
                'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'Me.Close()
                'Return
            End If

            rptTCS.Load(strFileName)

            Fds.Tables(0).TableName = "TCS_ERR_LST"
            rptTCS.SetDataSource(Fds)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi Báo cáo chi tiết lỗi của quá trình trao đổi thông tin", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_ERR_TDTT_09() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
#End Region

#Region "Kho quỹ"
    Private Sub KQ_CTU_TIENMAT()
        '-----------------------------------------------------
        ' Mục đích: In Bảng kê thực thu chứng từ
        ' Ngày viết: 17/02/2009
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "Bảng kê thực thu chứng từ"
        strFileName = Server.MapPath("RPT\TCS_CTU_TIENMAT.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("Ten_KB", mTen_KB)
            AddParam("Ngay", mNgay)
            AddParam("Ma_NNT", mMa_NNThue)
            AddParam("Ten_NNT", mTen_NNThue)
            AddParam("DC_NNT", mDC_NNThue)
            AddParam("KH_CT", mKHCT)
            AddParam("So_CT", mSoCT)
            AddParam("Tien_Bang_Chu", mTien_Bang_Chu)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi In Bảng kê thực thu chứng từ", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: KQ_CTU_TIENMAT() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Private Sub KQ_SOTHU_TIENMAT()
        '-----------------------------------------------------
        ' Mục đích: In sổ thu tiền mặt
        ' Ngày viết: 16/07/2008
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In sổ thu tiền mặt"
        strFileName = Server.MapPath("RPT\TCS_SOTHU_TIENMAT.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("Ten_KB", FTen_KB)
            AddParam("Ngay", FNgay_CT)
            AddParam("Ma_NV", FThuquy)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi In sổ thu tiền mặt", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: KQ_SOTHU_TIENMAT() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Private Sub KQ_BKE_TIENMAT()
        '-----------------------------------------------------
        ' Mục đích: In sổ thu tiền mặt
        ' Ngày viết: 17/07/2008
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê tiền mặt"
        strFileName = Server.MapPath("RPT\TCS_BKE_TIENMAT.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Dim frm As infBaoCao
        frm = Session("objBaoCao")
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            pdvTCS.Value = FTen_KB
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ten_KB").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = FNgay_CT
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ngay").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = FThuquy
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ma_NV").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = FDichSo
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Bang_Chu").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mstrTenNV
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ten_NV").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = mdblTongNop
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Tong_Nop").ApplyCurrentValues(pcTCS)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi In sổ thu tiền mặt", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: KQ_BKE_TIENMAT() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Private Sub KQ_SOQUY_TIENMAT()
        '-----------------------------------------------------
        ' Mục đích: In sổ thu tiền mặt
        ' Ngày viết: 16/07/2008
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In sổ quỹ tiền mặt"
        strFileName = Server.MapPath("RPT\TCS_SOQUY_TIENMAT.rpt")
        If (Not File.Exists(strFileName)) Then

        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            pdvTCS.Value = FTen_KB
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ten_KB").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = FNgay_CT
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ngay").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = FThuquy
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ma_NV").ApplyCurrentValues(pcTCS)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi In sổ thu tiền mặt", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: KQ_SOQUY_TIENMAT() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
#End Region

#Region "Biên lai thu"
    Private Sub TCS_BLT_TC()
        '-----------------------------------------------------
        ' Mục đích: In tra cứu biên lai thu
        ' Ngày viết: 07/08/2008
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê biên lai thu"
        strFileName = Server.MapPath("RPT\TCS_BLT_TC.rpt")
        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            pdvTCS.Value = FTen_KB
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ten_KB").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = FNgay_CT
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("Ngay").ApplyCurrentValues(pcTCS)

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi In tra cứu biên lai thu", Session.Item("TEN_DN"))
            'LogDebug.WriteLog("Error source: TCS_BLT_TC() " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
#End Region

#Region "Tien Gia"
    Public Function TCS_BBTG()
        '------------------------------
        'Mục đích: In biên bản tiền giả
        'Dầu vào:
        'Xử lý
        'Người viết: HoanNH
        'Ngày viết:03/07/2008
        '-------------------------------
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim strpath As String = Server.MapPath("RPT\TCS_BLTGTienGia.rpt")
        Try
            If Not Fds.Tables(0) Is Nothing Then
                rptTCS.Load(strpath)

                Fds.Tables(0).TableName = "TCS_TIENGIA"
                rptTCS.SetDataSource(Fds)

                pdvTCS.Value = FTen_KB
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("Ten_KB").ApplyCurrentValues(pcTCS)

                rpvMain.ReportSource = rptTCS
                rpvMain.DataBind()
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi In biên bản tiền giả", Session.Item("TEN_DN"))
            'MsgBox("Lỗi xảy ra trong quá trình hiển thị dữ liệu ", MsgBoxStyle.Critical, "Thông báo")
            'LogDebug.Writelog("Lỗi trong quá trình show danh sách CT tra cứu: " & ex.ToString)
        Finally
            'Me.'Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Function
#End Region

#Region "NgocLA"

#Region "TCS_XLCN"
    Private Sub TCS_BKBLT_CT()
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = Server.MapPath("RPT\TCS_BKBLT_CT.rpt")

        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            'Dim objda As New daXLCN

            Fds.Tables(0).TableName = "TCS_BK_BLTHU_CT"
            rptTCS.SetDataSource(Fds.Tables(0))

            pdvTCS.Value = Me.TCS_Ten_KB 'FTen_KB
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("P_Ten_KB").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = Me.TCS_MaDiemThu  'FMaDiemThu
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("P_Ma_DThu").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = Me.TCS_Tungay 'FTungay
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("P_Tu_Ngay").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = Me.TCS_Denngay 'FDenngay
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("P_Den_Ngay").ApplyCurrentValues(pcTCS)

            pdvTCS.Value = Me.TCS_DichSo 'FDichSo
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields("P_Chuoi_Tien").ApplyCurrentValues(pcTCS)

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
            'Me.'Cursor = System.Windows.Forms.Cursors.Default
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi trong quá trình show bản kê hình thức thu", Session.Item("TEN_DN"))
            'LogDebug.Writelog("Lỗi trong quá trình show bản kê hình thức thu: " & ex.ToString)
            'Me.'Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub
    Private Sub TCS_BK_DVQD()
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = Server.MapPath("RPT\TCS_BKBLT_DVQD.rpt")

        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)
            'Dim objda As New daXLCN
            If Not Fds.Tables(0) Is Nothing Then

                Fds.Tables(0).TableName = "TCS_BK_BLTHU_CT"
                rptTCS.SetDataSource(Fds.Tables(0))

                pdvTCS.Value = FTen_KB
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("P_Ten_KB").ApplyCurrentValues(pcTCS)

                pdvTCS.Value = FMaDiemThu
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("P_Ma_DThu").ApplyCurrentValues(pcTCS)

                pdvTCS.Value = FTungay
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("P_Tu_Ngay").ApplyCurrentValues(pcTCS)

                pdvTCS.Value = FDenngay
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("P_Den_Ngay").ApplyCurrentValues(pcTCS)

                pdvTCS.Value = FDichSo
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("P_Chuoi_Tien").ApplyCurrentValues(pcTCS)

                rpvMain.ReportSource = rptTCS
                rpvMain.DataBind()
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi trong quá trình show bản kê hình thức thu", Session.Item("TEN_DN"))
            'MsgBox(ex.ToString, MsgBoxStyle.Critical, "In báo cáo")
        Finally
            'Me.'Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub
    Private Sub TCS_THOP_TPHAT()
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim strFileName As String = Server.MapPath("RPT\TCS_BTHOP_TPHAT.rpt")

        If (Not File.Exists(strFileName)) Then
            'MessageBox.Show("Không tìm thấy file report : '" & strFileName & "'!", "Chú ý", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Me.Close()
            'Return
        End If
        Try
            rptTCS.Load(strFileName)

            If Not Fds.Tables(0) Is Nothing Then

                Fds.Tables(0).TableName = "TCS_BTH_THUPHAT"
                rptTCS.SetDataSource(Fds.Tables(0))

                pdvTCS.Value = FTen_KB
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("P_Ten_KB").ApplyCurrentValues(pcTCS)

                pdvTCS.Value = FTungay
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("P_Tu_Ngay").ApplyCurrentValues(pcTCS)

                pdvTCS.Value = FDenngay
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("P_Den_Ngay").ApplyCurrentValues(pcTCS)

                rpvMain.ReportSource = rptTCS
                rpvMain.DataBind()
            End If
            'Me.'Cursor = System.Windows.Forms.Cursors.Default
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi trong quá trình show bản kê theo đơn vị quyết định", Session.Item("TEN_DN"))
            'MsgBox("Lỗi xảy ra trong quá trình hiển thị dữ liệu ", MsgBoxStyle.Critical, "Thông báo")
            'LogDebug.Writelog("Lỗi trong quá trình show bản kê theo đơn vị quyết định: " & ex.ToString)
        Finally
            'Me.'Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub
#End Region

#Region "Bang Ke Phan loai tien mat"
    Public Function TCS_BK_PLTM()
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim strpath As String = Server.MapPath("RPT\TCS_NHAP_BKPLTM.rpt")
        Try
            If Not Fds.Tables(0) Is Nothing Then
                rptTCS.Load(strpath)

                Fds.Tables(0).TableName = "TCS_BKPLTM"
                rptTCS.SetDataSource(Fds.Tables(0))

                pdvTCS.Value = FTen_KB
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("Ten_KB").ApplyCurrentValues(pcTCS)

                pdvTCS.Value = FNgay_CT
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("Ngay_CT").ApplyCurrentValues(pcTCS)

                pdvTCS.Value = FBanThu
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("Ban_So").ApplyCurrentValues(pcTCS)

                pdvTCS.Value = FMaDiemThu
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("MaDiemThu").ApplyCurrentValues(pcTCS)

                pdvTCS.Value = FDichSo
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("Bang_Chu").ApplyCurrentValues(pcTCS)

                pdvTCS.Value = FThuquy
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("Thuquy").ApplyCurrentValues(pcTCS)

                rpvMain.ReportSource = rptTCS
                rpvMain.DataBind()
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) 'clsCommon.WriteLog(ex, "Lỗi trong quá trình show bản kê phân loại tiền mặt", Session.Item("TEN_DN"))
            'MsgBox("Lỗi xảy ra trong quá trình hiển thị dữ liệu ", MsgBoxStyle.Critical, "Thông báo")
            'LogDebug.Writelog("Lỗi trong quá trình show bản kê phân loại tiền mặt: " & ex.ToString)
        Finally
            'Me.'Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Function
#End Region
    Public Function TCS_TC_STTHU()
        '------------------------------
        'Mục đích: Get dữ liệu cho form báo cáo tra cứu số thực thu
        'Dầu vào:
        'Xử lý
        'Người viết: Ngocla
        'Ngày viết:7/12/2007
        '-------------------------------
        'Me.'Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim strpath As String = Server.MapPath("RPT\TCS_TC_STTHU.rpt")
        Try
            If Not Fds.Tables(0) Is Nothing Then
                rptTCS.Load(strpath)

                Fds.Tables(0).TableName = "TCS_TC_STTHU"
                rptTCS.SetDataSource(Fds.Tables(0))

                pdvTCS.Value = FTen_KB
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("Ten_KB").ApplyCurrentValues(pcTCS)

                pdvTCS.Value = FNgay_CT
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("Ngay_CT").ApplyCurrentValues(pcTCS)

                pdvTCS.Value = FBanThu
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("Ban_So").ApplyCurrentValues(pcTCS)

                pdvTCS.Value = FMaDiemThu
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("MaDiemThu").ApplyCurrentValues(pcTCS)

                pdvTCS.Value = FThuquy
                pcTCS.Add(pdvTCS)
                rptTCS.DataDefinition.ParameterFields("ThuQuy").ApplyCurrentValues(pcTCS)

                rpvMain.ReportSource = rptTCS
                rpvMain.DataBind()
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) 'clsCommon.WriteLog(ex, "Lỗi trong quá trình lấy dữ liệu cho form báo cáo tra cứu số thực thu", Session.Item("TEN_DN"))
        Finally

        End Try
    End Function

#End Region

#Region "Bao cao trong h"

    Private Sub TCS_CTU_Tronggio()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê liệt chứng từ trong giờ"
        strFileName = Server.MapPath("RPT\rpt_bke_theogio_gd_new.rpt")
        If (Not File.Exists(strFileName)) Then
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            '' AddParam("Ten_KB", FTen_KB)
            AddParam("Ma_Dthu", FMaDiemThu)
            AddParam("Ngay", FNgay_CT)
            'AddParam("Tu_Ngay", FTungay)
            'AddParam("Den_Ngay", FDenngay)
            AddParam("ma_BC", Mau_BCao)
            ' AddParam("thangLV", Thang)
            'AddParam("nam", Nam)
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape
            'Fds.WriteXml("baocaotronggio.xml", XmlWriteMode.WriteSchema)
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi trong quá trình In bảng kê liệt chứng từ trong giờ", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub

    Private Sub TCS_CTU_Ngoaigio()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê liệt chứng từ ngoài giờ"
        strFileName = Server.MapPath("RPT\TCS_BKE_NTNG.rpt")
        If (Not File.Exists(strFileName)) Then
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            'AddParam("Ten_KB", FTen_KB)
            'AddParam("Ma_Dthu", FMaDiemThu)
            AddParam("Ngay", FNgay_CT)
            AddParam("ngayLV", Ngay)
            AddParam("thangLV", Thang)
            AddParam("nam", Nam)
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi trong quá trình In bảng kê liệt chứng từ ngoài giờ", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub

    Private Sub TCS_CTU_THUHO()

        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê liệt chứng từ thu hộ"
        strFileName = Server.MapPath("RPT\TCS_BKE_CTThuHo.rpt")
        If (Not File.Exists(strFileName)) Then
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            'AddParam("Ten_KB", FTen_KB)
            AddParam("Ma_Dthu", FMaDiemThu)
            AddParam("Ngay", FNgay_CT)
            AddParam("ngayLV", Ngay)
            AddParam("thangLV", Thang)
            AddParam("nam", Nam)
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi trong quá trình In bảng kê liệt chứng từ thu hộ", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
    'bao cao doi chieu
    Private Sub TCS_BKE_DOICHIEU()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê liệt chứng từ trong giờ"
        strFileName = Server.MapPath("RPT\rpt_bke_doichieu.rpt")
        If (Not File.Exists(strFileName)) Then
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            '' AddParam("Ten_KB", FTen_KB)
            AddParam("Ma_Dthu", FMaDiemThu)
            AddParam("Ngay", FNgay_CT)
            AddParam("Tu_Ngay", FTungay)
            AddParam("Den_ngay", FDenngay)
            'AddParam("nam", Nam)
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
            ' Fds.WriteXml("baocaotronggio.xml", XmlWriteMode.WriteSchema)
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi trong quá trình In bảng kê liệt chứng từ trong giờ", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub

    Private Sub TCS_BC_TONGCN()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê liệt chứng từ trong giờ"
        strFileName = Server.MapPath("RPT\tcs_bc_tong_cn.rpt")
        If (Not File.Exists(strFileName)) Then
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("Tu_Ngay", FTungay)
            AddParam("Den_Ngay", FDenngay)
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape

            'Fds.WriteXml("baocaotronggio.xml", XmlWriteMode.WriteSchema)
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi trong quá trình In bảng kê liệt chứng từ trong giờ", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
    Private Sub TCS_BC_DC_02BKDC()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê liệt chứng từ trong giờ"
        strFileName = Server.MapPath("RPT\rpt_BC02_BKDC_TCTD.rpt")
        If (Not File.Exists(strFileName)) Then
        End If
        Try

            Fds.Tables(0).TableName = "tbDC02_BKDC-TCTD"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("tungay", FTungay)
            rptTCS.SetParameterValue("denngay", FDenngay)


            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi trong quá trình In bảng kê liệt chứng từ trong giờ", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
    Private Sub TCS_BC_TONGNHCD()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê liệt chứng từ trong giờ"
        strFileName = Server.MapPath("RPT\tcs_bc_tong_NHCD.rpt")
        If (Not File.Exists(strFileName)) Then
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("Tu_Ngay", FTungay)
            AddParam("Den_Ngay", FDenngay)
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
            'Fds.WriteXml("baocaotronggio.xml", XmlWriteMode.WriteSchema)
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi trong quá trình In bảng kê liệt chứng từ trong giờ", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
    Private Sub TCS_BC_TONGCN_CT()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê liệt chứng từ trong giờ"
        strFileName = Server.MapPath("RPT\tcs_bc_tong_cn_ct.rpt")
        If (Not File.Exists(strFileName)) Then
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("Tu_Ngay", FTungay)
            AddParam("Den_Ngay", FDenngay)
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape

            'Fds.WriteXml("baocaotronggio.xml", XmlWriteMode.WriteSchema)
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi trong quá trình In bảng kê liệt chứng từ trong giờ", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
    Private Sub TCS_BC_TONGNHCD_CT()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "In bảng kê liệt chứng từ trong giờ"
        strFileName = Server.MapPath("RPT\tcs_bc_tong_NHCD_ct.rpt")
        If (Not File.Exists(strFileName)) Then
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            AddParam("Tu_Ngay", FTungay)
            AddParam("Den_Ngay", FDenngay)
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
            'Fds.WriteXml("baocaotronggio.xml", XmlWriteMode.WriteSchema)
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi trong quá trình In bảng kê liệt chứng từ trong giờ", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
    Private Sub TCS_BC_THUE_PGB()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "Báo cáo thuế - PGB"
        strFileName = Server.MapPath("RPT\cr1.rpt")
        If (Not File.Exists(strFileName)) Then
        End If
        Try
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            '' AddParam("Ten_KB", FTen_KB)
            AddParam("Ma_Dthu", FMaDiemThu)
            AddParam("Ngay", FNgay_CT)
            'AddParam("Tu_Ngay", FTungay)
            'AddParam("Den_Ngay", FDenngay)
            AddParam("ma_BC", Mau_BCao)
            ' AddParam("thangLV", Thang)
            'AddParam("nam", Nam)
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape
            'Fds.WriteXml("baocaotronggio.xml", XmlWriteMode.WriteSchema)
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi trong quá trình In Báo cáo thuế - PGB", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
#End Region

#Region "Cac bao cao doi chieu"

    Private Sub DOI_CHIEU_1()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "Báo cáo tổng hợp hủy thu thuế XNK"
        strFileName = Server.MapPath("RPT\BCTHopCTTThueXNKhau.rpt")
        Dim datet As DateTime = DateTime.ParseExact(FNgay_CT, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBCTHopCTTThueXNKhau"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("gio_1", datet.Hour.ToString())
            rptTCS.SetParameterValue("ngay", datet.Day.ToString())
            rptTCS.SetParameterValue("thang", datet.Month.ToString())
            rptTCS.SetParameterValue("nam", datet.Year.ToString())
            rptTCS.SetParameterValue("soMau", "02")
            rptTCS.SetParameterValue("loai", "Hủy thành công")

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi trong quá trình in Báo cáo tổng hợp hủy thu thuế XNK", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub


    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			TCs the s_ B c_ B l_ DOANHSO.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		09/29/2012 00:00:00
    '''  Mục đích:	    Hiển thị báo cáo doanh số bảo lãnh
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    Private Sub TCS_BC_BL_DOANHSO()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "BÁO CÁO CHI TIẾT DOANH SỐ BẢO LÃNH THUẾ NHẬP KHẨU"
        strFileName = Server.MapPath("RPT\BCCTietDoanhSoBaoLanh.rpt")
        'Dim datet As DateTime = DateTime.ParseExact(FDenngay, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBCCTietSoDuBL"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("tungay", FTungay)
            rptTCS.SetParameterValue("denngay", FDenngay)
 
            rptTCS.SetParameterValue("tieude", "SỐ DƯ")

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi trong quá trình in Báo cáo chi tiết doanh số bảo lãnh thuế nhập khẩu", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			TCs the s_ B c_ B l_ DOANHSO.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		09/29/2012 00:00:00
    '''  Mục đích:	    Hiển thị báo cáo tong hop doanh số bảo lãnh
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    Private Sub TCS_BC_TH_BL_DOANHSO()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "BÁO CÁO DOANH SỐ BẢO LÃNH THUẾ NHẬP KHẨU"
        strFileName = Server.MapPath("RPT\BCTHDoanhSoBaoLanh.rpt")
        'Dim datet As DateTime = DateTime.ParseExact(FDenngay, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBCTHSoDuBL"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("tungay", FTungay)
            rptTCS.SetParameterValue("denngay", FDenngay)

            rptTCS.SetParameterValue("tieude", "SỐ DƯ")

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi trong quá trình in Báo cáo doanh số bảo lãnh thuế nhập khẩu", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub

    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			TCs the s_ B c_ B l_ SODU.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		09/29/2012 00:00:00
    '''  Mục đích:	    Hiển thị báo cáo số dư bảo lãnh
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    Private Sub TCS_BC_BL_SODU()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "BÁO CÁO CHI TIẾT SỐ DƯ BẢO LÃNH THUẾ NHẬP KHẨU"
        strFileName = Server.MapPath("RPT\BCCTietSoDuBaoLanh.rpt")
        Dim datet As DateTime = DateTime.ParseExact(FDenngay, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBCCTietSoDuBL"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("gio", datet.Hour.ToString())
            rptTCS.SetParameterValue("ngay", datet.Day.ToString())
            rptTCS.SetParameterValue("thang", datet.Month.ToString())
            rptTCS.SetParameterValue("nam", datet.Year.ToString())
            rptTCS.SetParameterValue("tungay", FTungay)
            rptTCS.SetParameterValue("tieude", "DOANH SỐ")

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi trong quá trình in Báo cáo chi tiết số dư bảo lãnh thuế nhập khẩu", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			TCs the s_ B c_ B l_ SODU.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		09/29/2012 00:00:00
    '''  Mục đích:	    Hiển thị báo cáo tong hop số dư bảo lãnh
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    Private Sub TCS_BC_TH_BL_SODU()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "BÁO CÁO SỐ DƯ BẢO LÃNH THUẾ NHẬP KHẨU"
        strFileName = Server.MapPath("RPT\BCTHSoDuBaoLanh.rpt")
        'Dim datet As DateTime = DateTime.ParseExact(FDenngay, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBCTHSoDuBL"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            'rptTCS.SetParameterValue("gio", datet.Hour.ToString())
            'rptTCS.SetParameterValue("ngay", datet.Day.ToString())
            'rptTCS.SetParameterValue("thang", datet.Month.ToString())
            rptTCS.SetParameterValue("denngay", FDenngay)
            rptTCS.SetParameterValue("tungay", FTungay)
            rptTCS.SetParameterValue("tieude", "DOANH SỐ")

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi trong quá trình in Báo cáo số dư bảo lãnh thuế nhập khẩu", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
    Private Sub DOI_CHIEU_2()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "Đối chiếu chi tiết bảo lãnh thuế XNK"
        strFileName = Server.MapPath("RPT\BCCTietCTTThueXNKhau.rpt")
        Dim datet As DateTime = DateTime.ParseExact(FNgay_CT, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBCCTietCTTThueXNKhau"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("gio", datet.Hour.ToString())
            rptTCS.SetParameterValue("ngay", datet.Day.ToString())
            rptTCS.SetParameterValue("thang", datet.Month.ToString())
            rptTCS.SetParameterValue("nam", datet.Year.ToString())
            rptTCS.SetParameterValue("tieude", "BẢO LÃNH")

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi trong quá trình Đối chiếu chi tiết bảo lãnh thuế XNK", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub

    Private Sub DOI_CHIEU_3()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "Chi tiết các điện hủy chứng từ bảo lãnh thuế"
        strFileName = Server.MapPath("RPT\rptBCCTietHuyTThueXNKhau.rpt")
        Dim datet As DateTime = DateTime.ParseExact(FNgay_CT, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBCCTietHuyCTTThueXNKhau"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("gio", datet.Hour.ToString())
            rptTCS.SetParameterValue("ngay", datet.Day.ToString())
            rptTCS.SetParameterValue("thang", datet.Month.ToString())
            rptTCS.SetParameterValue("nam", datet.Year.ToString())
            rptTCS.SetParameterValue("tsomon", Fsomon_trongGio)
            rptTCS.SetParameterValue("tieude", "BẢO LÃNH")

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi Chi tiết các điện hủy chứng từ bảo lãnh thuế", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub

    Private Sub DOI_CHIEU_4()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "Chi tiết hủy chứng từ thu thuế XNK"
        strFileName = Server.MapPath("RPT\rptBCCTietHuyTThueXNKhau.rpt")
        Dim datet As DateTime = DateTime.ParseExact(FNgay_CT, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBCCTietHuyCTTThueXNKhau"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("gio", datet.Hour.ToString())
            rptTCS.SetParameterValue("ngay", datet.Day.ToString())
            rptTCS.SetParameterValue("thang", datet.Month.ToString())
            rptTCS.SetParameterValue("nam", datet.Year.ToString())
            rptTCS.SetParameterValue("tsomon", Fsomon_trongGio)
            rptTCS.SetParameterValue("tieude", "THU")

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi Chi tiết hủy chứng từ thu thuế XNK", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub

    Private Sub DOI_CHIEU_5()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "Đối chiếu chi tiết thu thuế XNK"
        strFileName = Server.MapPath("RPT\BCCTietCTTThueXNKhau.rpt")
        Dim datet As DateTime = DateTime.ParseExact(FNgay_CT, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBCCTietCTTThueXNKhau"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("gio", datet.Hour.ToString())
            rptTCS.SetParameterValue("ngay", datet.Day.ToString())
            rptTCS.SetParameterValue("thang", datet.Month.ToString())
            rptTCS.SetParameterValue("nam", datet.Year.ToString())
            rptTCS.SetParameterValue("tieude", "THU")

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi Đối chiếu chi tiết thu thuế XNK", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub

    Private Sub DOI_CHIEU_6()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "Tổng hợp bảo lãnh thuế XNK"
        strFileName = Server.MapPath("RPT\rptBCTHopBLanhXNKhau.rpt")
        Dim datet As DateTime = DateTime.ParseExact(FNgay_CT, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBCTHopCTTThueXNKhau"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("gio", datet.Hour.ToString())
            rptTCS.SetParameterValue("ngay", datet.Day.ToString())
            rptTCS.SetParameterValue("thang", datet.Month.ToString())
            rptTCS.SetParameterValue("nam", datet.Year.ToString())
            rptTCS.SetParameterValue("loai", "Thành công")
            rptTCS.SetParameterValue("mau", "01")
            rptTCS.SetParameterValue("tieude", "")

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi Tổng hợp bảo lãnh thuế XNK", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub

    Private Sub DOI_CHIEU_7()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "Tổng hợp thu thuế XNK"
        strFileName = Server.MapPath("RPT\BCTHopCTTThueXNKhau.rpt")
        Dim datet As DateTime = DateTime.ParseExact(FNgay_CT, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBCTHopCTTThueXNKhau"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("gio_1", datet.Hour.ToString())
            rptTCS.SetParameterValue("ngay", datet.Day.ToString())
            rptTCS.SetParameterValue("thang", datet.Month.ToString())
            rptTCS.SetParameterValue("nam", datet.Year.ToString())
            rptTCS.SetParameterValue("loai", "Thành công")
            rptTCS.SetParameterValue("soMau", "01")

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi Tổng hợp thu thuế XNK", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
    Private Sub HQ247_DOI_CHIEU_1()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "Tổng hợp thu thuế XNK"
        strFileName = Server.MapPath("RPT\HQ247_BCTHopCTTThueXNKhau.rpt")
        Dim datet As DateTime = DateTime.ParseExact(FNgay_CT, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBCTHopCTTThueXNKhau"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("gio_1", datet.Hour.ToString())
            rptTCS.SetParameterValue("ngay", datet.Day.ToString())
            rptTCS.SetParameterValue("thang", datet.Month.ToString())
            rptTCS.SetParameterValue("nam", datet.Year.ToString())
            rptTCS.SetParameterValue("loai", "Thành công")
            rptTCS.SetParameterValue("soMau", "01")

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi Tổng hợp thu thuế XNK", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
    Private Sub HQ247_DOI_CHIEU_2()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "Đối chiếu chi tiết thu thuế XNK"
        '  strFileName = Server.MapPath("RPT\HQ247_BCCTietCTTThueXNKhau.rpt")
        strFileName = Server.MapPath("RPT\HQ247_BCDCCTietCTTThueXNKhau.rpt")
        Dim datet As DateTime = DateTime.ParseExact(FNgay_CT, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBCCTietCTTThueXNKhau"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("gio", datet.Hour.ToString())
            rptTCS.SetParameterValue("ngay", datet.Day.ToString())
            rptTCS.SetParameterValue("thang", datet.Month.ToString())
            rptTCS.SetParameterValue("nam", datet.Year.ToString())
            rptTCS.SetParameterValue("tieude", "THU")

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '     clsCommon.WriteLog(ex, "Lỗi Đối chiếu chi tiết thu thuế XNK", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
    Private Sub DOI_CHIEU_8()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "Tổng hợp hủy bảo lãnh thuế XNK"
        strFileName = Server.MapPath("RPT\rptBCTHopBLanhXNKhau.rpt")
        Dim datet As DateTime = DateTime.ParseExact(FNgay_CT, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBCTHopCTTThueXNKhau"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("gio", datet.Hour.ToString())
            rptTCS.SetParameterValue("ngay", datet.Day.ToString())
            rptTCS.SetParameterValue("thang", datet.Month.ToString())
            rptTCS.SetParameterValue("nam", datet.Year.ToString())
            rptTCS.SetParameterValue("loai", "Hủy thành công")
            rptTCS.SetParameterValue("mau", "02")
            rptTCS.SetParameterValue("tieude", "HỦY ")

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi Tổng hợp hủy bảo lãnh thuế XNK", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
    Private Sub BC_CHITIET_GD_NTDT()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "In chi tiết giao dịch nộp thuế điện tử"
        strFileName = Server.MapPath("RPT\rptBC_GD_NTDT.rpt")
        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbChiTietGDNTDT"
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("Tu_Ngay", FTungay)
            rptTCS.SetParameterValue("Den_Ngay", FDenngay)
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi trong quá trình In chi tiết giao dịch nộp thuế điện tử", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
    Private Sub BC_NTDT()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "In chi tiết giao dịch nộp thuế điện tử"
        strFileName = Server.MapPath("RPT\BC_NTDT.rpt")
        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbNTDT"
            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("tungay", FTungay)
            rptTCS.SetParameterValue("denngay", FDenngay)
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi trong quá trình In chi tiết giao dịch nộp thuế điện tử", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub

    Private Sub DC_TONGHOP_PBBN()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "Tổng hợp thu thuế lệ phí bộ ban ngành"
        strFileName = Server.MapPath("RPT\BcDoiChieuLePhiBBN.rpt")
        Dim datet As DateTime = DateTime.ParseExact(FNgay_CT, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBCTHopCTTThueXNKhau"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("gio_1", datet.Hour.ToString())
            rptTCS.SetParameterValue("ngay", datet.Day.ToString())
            rptTCS.SetParameterValue("thang", datet.Month.ToString())
            rptTCS.SetParameterValue("nam", datet.Year.ToString())
            rptTCS.SetParameterValue("loai", "Thành công")
            rptTCS.SetParameterValue("soMau", "01")

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Tổng hợp thu thuế lệ phí bộ ban ngành", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub

    Private Sub DC_CHITIET_PBBN()
        Dim strFileName As String = ""
        Dim v_strReportName As String = "Tổng hợp chi tiết lệ phí bộ ban ngành"
        strFileName = Server.MapPath("RPT\BcChiTietDoiChieuLePhiBBN.rpt")
        Dim datet As DateTime = DateTime.ParseExact(FNgay_CT, "dd/MM/yyyy", Nothing)

        If (Not File.Exists(strFileName)) Then
        End If
        Try
            Fds.Tables(0).TableName = "tbBcChiTietDcLePhiBBN"

            rptTCS.Load(strFileName)
            rptTCS.SetDataSource(Fds)

            rptTCS.SetParameterValue("ngay", datet.Day.ToString())
            rptTCS.SetParameterValue("thang", datet.Month.ToString())
            rptTCS.SetParameterValue("nam", datet.Year.ToString())

            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Tổng hợp chi tiết lệ phí bộ ban ngành", Session.Item("TEN_DN"))
            'Throw ex
        End Try
    End Sub
#End Region

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not Me.rptTCS Is Nothing Then
            Me.rptTCS.Dispose()
        End If
    End Sub

    Public Sub New()

    End Sub
End Class
