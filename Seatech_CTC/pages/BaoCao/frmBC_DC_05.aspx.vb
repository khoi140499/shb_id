﻿Imports VBOracleLib
Imports System.IO
Imports System.Data

Partial Class pages_BaoCao_frmBC_DC_05
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim ngay_dc As String = ""
            Dim ma_gd_dc As String = ""
            If Not Request.Params("ngay_dc") Is Nothing Then
                ngay_dc = Request.Params("ngay_dc")
                lblNgayDC.Text = "Ngày " & ngay_dc.Substring(6, 2) & " tháng " & ngay_dc.Substring(4, 2) & " năm " & ngay_dc.Substring(0, 4)
            End If
            If Not Request.Params("ma_gd_dc") Is Nothing Then
                ma_gd_dc = Request.Params("ma_gd_dc")
            End If
            ' tuanda
            ' thêm cột ID khoản nộp, số TK/QĐ/TB
            Dim header As String = "<table id='TableResult' width='100%' border='1' bgColor='#ffffff' borderColor='#000000' cellSpacing='0' cellPadding='0' style='font-size:10.0pt; font-family:Calibri; background:white;'>" & _
                                        " <tr style='background:yellow;'>" & _
                                            " <td align='center' rowspan='2'>STT</td>" & _
                                            " <td align='center' rowspan='2'>Số giấy nộp tiền</td> " & _
                                            " <td align='center' rowspan='2'>Mã hiệu chứng từ</td>" & _
                                            " <td align='center' rowspan='2'>Số chứng từ</td>" & _
                                            " <td align='center' rowspan='2'>Ngày nộp GNT</td>" & _
                                            " <td align='center' rowspan='2'>Ngày nộp thuế điện tử</td>" & _
                                            " <td align='center' colspan='2'>Thông tin NNT</td>" & _
                                            " <td align='center'>Mã CQT</td>" & _
                                            " <td align='center'>Mã KBNN</td>" & _
                                            " <td align='center' colspan='6'>Thông tin của TCT</td>" & _
                                            " <td align='center' colspan='6'>Thông tin của NH</td> " & _
                                         "<tr style='background:yellow;'>" & _
                                            "<td align='center'>Tên NNT</td >" & _
                                            "<td align='center'>MST</td> " & _
                                            "<td></td>" & _
                                            "<td></td>" & _
                                            "<td align='center'>ID khoản nộp</td>" & _
                                            "<td align='center'>Số tờ khai/QĐ/TB</td>" & _
                                            "<td align='center'>Chương</td>" & _
                                            "<td align='center'>NDKT</td>" & _
                                            "<td align='center'>Loại tiền</td>" & _
                                            "<td align='center'>Trạng thái</td> " & _
                                            "<td align='center'>ID khoản nộp</td>" & _
                                            "<td align='center'>Số tờ khai/QĐ/TB</td>" & _
                                            "<td align='center'>Chương</td>" & _
                                            "<td align='center'>NDKT</td>" & _
                                            "<td align='center'>Số tiền</td>" & _
                                            "<td align='center'>Trạng thái</td>" & _
                                        "</tr>"
            Dim detail As String = ""
            Dim sql As String = "Select * from NTDT_BAOCAO_HDR WHERE MA_BCAO='05DC-NHTM-NTDT' AND TO_CHAR(NGAY_DC,'yyyyMMdd')=" & ngay_dc & " AND ma_gdich_dc = '" + ma_gd_dc + "' order by id_bcao desc"
            Dim dt As DataTable = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text).Tables(0)
            If dt.Rows.Count > 0 Then
                lblTuNgay.Text = convertDayofYear(dt.Rows(0)("TU_NGAY_DC"))
                lblDenNgay.Text = convertDayofYear(dt.Rows(0)("DEN_NGAY_DC"))
                lblGioDC.Text = convertDayofYear(dt.Rows(0)("NGAY_GDICH_DC"))
                'Lấy detail
                Dim sum_detail1_TCT As Decimal = 0
                Dim sum_detail1_TCT_vnd As Decimal = 0
                Dim sum_detail1_TCT_usd As Decimal = 0
                Dim sum_detail1_NHTM As Decimal = 0
                Dim sum_detail1_NHTM_vnd As Decimal = 0
                Dim sum_detail1_NHTM_usd As Decimal = 0
                ' tuanda - 07/03/24
                Dim sql_detail_1 As String = "Select dtl.ID_KNOP, dtl.SO_TK_TB_QD, r5.*,NVL(r5.MA_NGUYENTE, 'VND') as MA_NGUYENTE_R5,NVL(r5.TCT_MA_NGUYENTE, 'VND') as TCT_MA_NGUYENTE_R5, ct.so_ct_nh as ref_no from NTDT_BAOCAO_CTIET_R5 r5, tcs_ctu_ntdt_hdr ct, tcs_ctu_ntdt_dtl dtl WHERE r5.so_ctu = ct.so_ctu(+) and r5.so_ctu = dtl.so_ct(+) and ID_BCAO=" & dt.Rows(0)("ID_BCAO") & " order by r5.so_ctu"

                Dim dt_detail_1 As DataTable = DataAccess.ExecuteReturnDataSet(sql_detail_1, CommandType.Text).Tables(0)
                If dt_detail_1.Rows.Count > 0 Then
                    For i = 0 To dt_detail_1.Rows.Count - 1
                        Dim sotien_tct As Decimal = 0

                        Try
                            sotien_tct = Decimal.Parse(dt_detail_1.Rows(i)("TCT_SO_TIEN"))
                        Catch ex As Exception

                        End Try

                        Dim sotien_nhtm As Decimal = 0

                        Try
                            sotien_nhtm = Decimal.Parse(dt_detail_1.Rows(i)("NHTM_SO_TIEN"))
                        Catch ex As Exception

                        End Try


                        Dim row As String = "<tr>"
                        row &= "<td align='center'>" & (i + 1) & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("SO_GNT") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("MA_HIEU_CTU") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("SO_CTU") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("NGAY_NOP_GNT") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("NGAY_NOP_THUE") & "</td>" & _
                                "<td align='center'>" & dt_detail_1.Rows(i)("TEN_NNT") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("MST") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("MA_CQTHU") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("MA_KBNN") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("ID_KNOP") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("SO_TK_TB_QD") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("TCT_MA_CHUONG") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("TCT_MA_TMUC") & "</td>" & _
                                "<td align='right'>" & String.Format("{0:0,0.00}", sotien_tct) & "</td>" & _
                                "<td align='center'>" & clsCTU.GetMoTa_TrangThai_CT_NTDT(dt_detail_1.Rows(i)("TCT_TTHAI").ToString) & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("ID_KNOP") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("SO_TK_TB_QD") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("NHTM_MA_CHUONG") & "</td>" & _
                                "<td align='center'>" & "&nbsp;" & dt_detail_1.Rows(i)("NHTM_MA_TMUC") & "</td>" & _
                                "<td align='right'>" & String.Format("{0:0,0.00}", sotien_nhtm) & "</td>" & _
                                "<td align='center'>" & clsCTU.GetMoTa_TrangThai_CT_NTDT(dt_detail_1.Rows(i)("NHTM_TTHAI").ToString) & "</td></tr>"
                        detail &= row
                        If dt_detail_1.Rows(i)("TCT_MA_NGUYENTE_R5").ToString.Equals("USD") Then
                            sum_detail1_TCT_usd += sotien_tct
                        Else
                            sum_detail1_TCT_vnd += sotien_tct
                        End If
                        If dt_detail_1.Rows(i)("MA_NGUYENTE_R5").ToString.Equals("USD") Then
                            sum_detail1_NHTM_usd += sotien_nhtm
                        Else
                            sum_detail1_NHTM_vnd += sotien_nhtm
                        End If


                    Next
                End If
                detail &= "<tr><td>Tổng cộng</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>"
            Else
                detail &= "<tr><td colspan='22'>Không có dữ liệu</td></tr>"
            End If
            Dim tableResult As String = ""
            tableResult &= header & detail & "</table>"
            ExportTable.InnerHtml = tableResult
        Catch ex As Exception
            ExportTable.InnerText = ex.Message
        End Try
    End Sub

    Protected Sub btnKetXuat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKetXuat.Click
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=BC_DC_05.xls")
        Response.Charset = "utf-8"
        Response.ContentType = "application/vnd.ms-excel"
        Using sw1 As New StringWriter()
            Dim hw1 As New HtmlTextWriter(sw1)
            tableBaoCao.RenderControl(hw1)
            Response.Output.Write(sw1.ToString)
            Response.Flush()
            Response.End()
        End Using
    End Sub
    Private Function convertDayofYear(ByVal strInput As String) As String
        If strInput.Length < 13 Then
            Return ""
        End If
        Dim year As Integer = 0
        Dim dayOfYear As Integer = 0
        year = Integer.Parse(strInput.Substring(0, 4))
        dayOfYear = Integer.Parse(strInput.Substring(4, 3))
        Dim hh As String = strInput.Substring(7, 2)
        Dim mm As String = strInput.Substring(9, 2)
        Dim ss As String = strInput.Substring(11, 2)
        Dim ngay As String = ""
        Dim theDate As DateTime = New DateTime(year, 1, 1).AddDays(dayOfYear - 1)
        ngay = hh + ":" + mm + ":" + ss + " ngày " + theDate.ToString("dd/MM/yyyy")
        Return ngay
    End Function
End Class
