﻿Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports Business.BaoCao
Imports System.Data
Imports Business
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports Signature

Partial Class pages_BaoCao_frmBC_GD_NTDT
    Inherits System.Web.UI.Page

    Private mv_strNgay As String

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenDiemThu As String
    Private mv_strTenKB As String
    Private mv_strLoaiBaoCao As String
    Private strMaNT As String

    Private ds As DataSet
    Private frm As infBaoCao
    Private somon As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                DM_TrangThai()
                DM_LoaiThue()
                txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub
    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Dim str_where As String = ""
        
        If txtTuNgay.Value <> "" Then
            str_where &= " AND  TO_CHAR(A.NGAY_NHANG,'yyyyMMdd') >=" & ConvertDateToNumber(txtTuNgay.Value)
        Else
            clsCommon.ShowMessageBox(Me, "Bạn cần nhập vào thời gian từ ngày đến ngày cho báo cáo")
            txtTuNgay.Focus()
            Return
        End If
        If txtDenNgay.Value <> "" Then
            str_where &= " AND  TO_CHAR(A.NGAY_NHANG,'yyyyMMdd') <=" & ConvertDateToNumber(txtDenNgay.Value)
        Else
            clsCommon.ShowMessageBox(Me, "Bạn cần nhập vào thời gian từ ngày đến ngày cho báo cáo")
            txtDenNgay.Focus()
            Return
        End If
        If drpLoaiThue.SelectedIndex > 0 Then
            str_where &= " AND A.MA_LTHUE ='" & drpLoaiThue.SelectedValue & "' "
        End If
        If drpTrangThai.SelectedIndex > 0 Then
            str_where &= " AND A.TTHAI_CTU ='" & drpTrangThai.SelectedValue & "' "
        End If
        Dim sql As String = "SELECT  A.SO_CTU AS SO_CT,B.RESPONSE_MSG AS SO_FT,A.TEN_NNOP AS TEN_NNT,A.MST_NNOP AS MST,A.TONG_TIEN AS SO_TIEN,(CASE WHEN A.MA_NGUYENTE IS NULL or A.MA_NGUYENTE ='VND' THEN A.TONG_TIEN ELSE 0 END) SO_TIEN_VND,(CASE WHEN A.MA_NGUYENTE ='USD' THEN A.TONG_TIEN ELSE 0 END) SO_TIEN_USD, " & _
                            " NVL(A.MA_NGUYENTE,'VND') MA_NGUYENTE,B.REMARK_DESC AS NOI_DUNG, B.TRAN_DATE AS NGAY_GD,A.ND_PHANHOI AS TT_CT_TCT ,A.TEN_TTHAI_CTU AS TT_CT_SHB " & _
                            " FROM TCS_CTU_NTDT_HDR A,TCS_LOG_PAYMENT B,TCS_DM_LTHUE C WHERE A.SO_CTU=B.RM_REF_NO(+) AND A.MA_LTHUE=C.MA_LTHUE(+) " & str_where
        Try
            ds = New DataSet
            frm = New infBaoCao
            ds = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If
            frm.TCS_DS = ds
            frm.TCS_Tungay = txtTuNgay.Value.ToString()
            frm.TCS_Denngay = txtDenNgay.Value.ToString()
            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & Report_Type.BC_CHITIET_GD_NTDT, "ManPower")

        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình Lập bảng kê chứng từ!")
        Finally
            cmdIn.Enabled = True
        End Try
    End Sub
    Private Sub DM_TrangThai()

        Dim dsTrangThai As DataSet
        Dim strSQL As String = ""
        strSQL = "SELECT a.TRANG_THAI, a.MOTA  FROM tcs_dm_trangthai_ntdt a where a.GHICHU='CT' "
        Try
            dsTrangThai = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(drpTrangThai, dsTrangThai, 1, 0, "Tất cả")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình lấy thông tin trạng thái chứng từ", Session.Item("TEN_DN"))
            Throw ex
        Finally

        End Try
    End Sub
    Private Sub DM_LoaiThue()
        Dim cnDiemThu As New DataAccess
        Dim dsLoaiThue As DataSet
        Dim strSQL As String = ""
        strSQL = "SELECT a.MA_LTHUE, a.TEN  FROM tcs_dm_lthue a"
        Try
            dsLoaiThue = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(drpLoaiThue, dsLoaiThue, 1, 0, "Tất cả")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '    clsCommon.WriteLog(ex, "Có lỗi trong quá trình lấy thông tin loại thuế", Session.Item("TEN_DN"))
            Throw ex
        Finally

        End Try
    End Sub
End Class
