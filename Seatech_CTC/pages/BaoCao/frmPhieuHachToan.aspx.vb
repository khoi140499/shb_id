﻿Imports Business.BaoCao
Imports Business
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports Business.NewChungTu
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO

Partial Class pages_BaoCao_frmPhieuHachToan
    Inherits System.Web.UI.Page

    Private pdvTCS As New CrystalDecisions.Shared.ParameterDiscreteValue
    Private rptTCS As New ReportDocument
    Private pcTCS As New CrystalDecisions.Shared.ParameterValues

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LogApp.AddDebug("In chung tu", "CHUNG TU THUE NOI DIA: In chung tu hach toan")
        Try
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Session.Item("User").ToString.Length = 0 Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Not clsCommon.GetSessionByID(Session.Item("User")) Then
                Response.Redirect("~/pages/Warning.html", False)
                Exit Sub
            End If
            Dim SO_CT As String = Request.QueryString.Item("SO_CT").ToString
            Dim ds As DataSet = Get_DataSet(SO_CT)
            If IsEmptyDataSet(ds) Then
                clsCommon.ShowMessageBox(Me, "Lỗi: Không thể in phiếu hạch toán với chứng từ chưa được hạch toán thành công.")
                Exit Sub
            End If
            Dim strFileName As String = ""
            Dim v_strReportName As String = "PHIẾU HẠCH TOÁN"
            strFileName = Server.MapPath("RPT\RPT_PhieuHachToan.rpt")
            rptTCS.Load(strFileName)
            ds.Tables(0).TableName = "tbHachToanCore"
            rptTCS.SetDataSource(ds)
            Get_TTHT(SO_CT)
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA5
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Landscape
            rpvMain.HasExportButton = False
            rpvMain.EnableToolTips = False
            rpvMain.HasPageNavigationButtons = False
            rpvMain.ShowAllPageIds = False
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
        Catch ex As Exception
            clsCommon.WriteLog(ex, "Có lỗi trong quá trình In phiếu hạch toán", Session.Item("TEN_DN"))
            clsCommon.ShowMessageBox(Me, "Lỗi: " & ex.Message)
        End Try
    End Sub

    Private Sub AddParam(ByVal strName As String, ByVal strValue As String)
        Try
            pdvTCS.Value = strValue
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields(strName).ApplyCurrentValues(pcTCS)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub Get_TTHT(ByVal SO_CT As String)
        Dim strSql As String = "SELECT SUBSTR(ref_no,0,4) || '-' || SUBSTR(ref_no,5,3) || '-' || SUBSTR(ref_no,8, length(ref_no)-7) as ref_no,TRAN_TYPE, " & _
                                "ROW_COUNT,NGAY_HL,NGAY_HT,REMARK,AMOUNT FROM tcs_accounting where SO_CT=" & SO_CT
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Dim TRAN_TYPE As String = dt.Rows(0)("TRAN_TYPE").ToString()
            If TRAN_TYPE = "0" Then
                AddParam("TRAN_TYPE", "NORMAL")
            Else
                AddParam("TRAN_TYPE", "CANCEL")
            End If
            AddParam("REF_NO", dt.Rows(0)("REF_NO").ToString())
            AddParam("ENTRY_NO", dt.Rows(0)("ROW_COUNT").ToString())
            AddParam("VALUE_DATE", ConvertNumbertoString(dt.Rows(0)("NGAY_HL").ToString()))
            AddParam("ENTRY_DATE", ConvertNumbertoString(dt.Rows(0)("NGAY_HT").ToString()))
            AddParam("REMARKS", dt.Rows(0)("REMARK").ToString())
            AddParam("TONG_TIEN", dt.Rows(0)("AMOUNT").ToString())
            AddParam("TONG_TIEN_CHU", TCS_Dich_So(Double.Parse(dt.Rows(0)("AMOUNT").ToString()), "đồng"))
            AddParam("TEN_CN", clsCTU.TCS_GetTenCNNH(Session.Item("MA_CN_USER_LOGON").ToString()))
            AddParam("TEN_DN", Session.Item("UserName").ToString())
        End If
    End Sub
    Private Function Get_DataSet(ByVal SO_CT As String) As DataSet
        Dim dt As DataSet
        Try
            Dim strSql As String = "SELECT * FROM tcs_accounting_detail a, tcs_accounting b where a.ACC_ID= b.ACC_ID and b.SO_CT=" & SO_CT
            dt = DataAccess.ExecuteReturnDataSet(strSql, CommandType.Text)
        Catch ex As Exception
            dt = Nothing
        End Try
        Return dt
    End Function

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not Me.rptTCS Is Nothing Then
            Me.rptTCS.Dispose()
        End If
    End Sub
End Class
