﻿Imports Business.BaoCao
Imports Business_HQ
Imports VBOracleLib
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common.mdlSystemVariables
Imports System.Data
Imports Business_HQ.NewChungTu
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO
Imports System.Globalization

Partial Class pages_BaoCao_frmInGNT_PHI
    Inherits System.Web.UI.Page

    Private so_ct As String
    Private pdvTCS As New CrystalDecisions.Shared.ParameterDiscreteValue
    Private rptTCS As New ReportDocument
    Private pcTCS As New CrystalDecisions.Shared.ParameterValues

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LogApp.AddDebug("cmdInCT_Click", "LAP CHUNG TU THUE PHI BO NGANH: In chung tu")
        Try
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Session.Item("User").ToString.Length = 0 Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Not clsCommon.GetSessionByID(Session.Item("User")) Then
                Response.Redirect("~/pages/Warning.html", False)
                Exit Sub
            End If
            
            Dim so_ct As String = ""
            If Not Request.QueryString.Item("so_ct") Is Nothing Then
                so_ct = Request.QueryString.Item("so_ct")
            End If

            Dim strFileName As String = ""
            Dim v_strReportName As String = "In chứng từ thu thuế, phí bộ ngành"

            strFileName = Server.MapPath("RPT\C111Phi.rpt")

            If (Not File.Exists(strFileName)) Then
            End If
            rptTCS.Load(strFileName)

            Dim ds As DataSet = New DataSet()
            Dim dsChiTiet As DataSet = New DataSet()
            Dim dsLine As DataSet = New DataSet()

            dsChiTiet = Business_HQ.HaiQuan.HaiQuan.ListChiTietBySoCT(so_ct)


            ds = Business_HQ.HaiQuan.HaiQuan.GET_GNT_PHI_BO_NGANH(so_ct)
            dsLine = Business_HQ.HaiQuan.HaiQuan.GET_GNT_PHI_BO_NGANH_DETAIL(so_ct)

            dsLine.Tables(0).TableName = "tbCTU_Phi"

            'ds.WriteXml("D:\gntnguyente.xml", XmlWriteMode.WriteSchema)
            rptTCS.SetDataSource(dsLine)
            Dim mCopy As String = ""
            'isBS = Business.ChungTu.buChungTu.CTU_Get_LanIn(key)
            'If Not isBS Is Nothing Then
            '    If isBS.Equals("0") Then
            '        mCopy = "Bản Chính"
            '    Else
            '        mCopy = "Bản Sao"
            '    End If
            'End If

            Dim dr As DataRow = ds.Tables(0).Rows(0)

            AddParam("KHCT", If(dr("KYHIEU_CT") Is Nothing, "........", dr("KYHIEU_CT").ToString()))
            AddParam("SoCT", If(dr("SO_CT") Is Nothing, "........", dr("SO_CT").ToString()))
            AddParam("Ten_NNThue", If(dr("NNT_TEN_DV") Is Nothing, "........", dr("NNT_TEN_DV").ToString()))
            AddParam("Ma_NNThue", If(dr("NNT_MST") Is Nothing, "........", dr("NNT_MST").ToString()))
            AddParam("DC_NNThue", If(dr("NNT_DIACHI") Is Nothing, "........", dr("NNT_DIACHI").ToString()))
            AddParam("Huyen_NNThue", If(dr("NNT_QUAN_HUYEN") Is Nothing, "........", dr("NNT_QUAN_HUYEN").ToString()))
            AddParam("Tinh_NNThue", If(dr("NNT_TINH_TP") Is Nothing, "........", dr("NNT_TINH_TP").ToString()))
            AddParam("Ten_NNTien", If(dr("NNT_TEN_DV") Is Nothing, "........", dr("NNT_TEN_DV").ToString()))
            AddParam("Ma_TK_Chuyen", If(dr("TK_THANHTOAN") Is Nothing, "........", dr("TK_THANHTOAN").ToString()))
            'AddParam("Ten_TK_Chuyen", If(dr("TEN_TK_CHUYEN") Is Nothing, "........", dr("TEN_TK_CHUYEN").ToString()))
            AddParam("Huyen_NNTien", If(dr("TK_THANH_TOAN_QUAN_HUYEN") Is Nothing, "........", dr("TK_THANH_TOAN_QUAN_HUYEN").ToString()))
            AddParam("Tinh_NNTien", If(dr("TK_THANH_TOAN_TINH_TP") Is Nothing, "........", dr("TK_THANH_TOAN_TINH_TP").ToString()))
            AddParam("NHA", If(dr("TEN_NH_PH") Is Nothing, "........", dr("TEN_NH_PH").ToString()))
            AddParam("Ten_KB", "")
            AddParam("Ma_CQThu", If(dr("SO_CT") Is Nothing, "........", ""))
            AddParam("So_CT_PT", If(dr("So_CT_PT") Is Nothing, "........", dr("So_CT_PT").ToString()))

            Dim tongTienVND As Double = 0
            Dim tongTienNT As Double = 0
            'For Each row As DataRow In dsChiTiet.Tables(0).Rows
            tongTienVND = Convert.ToDouble(dr("tt_nt_tongtien_vnd"))
            'Next



            AddParam("Tien_Bang_Chu", TCS_Dich_So(Convert.ToDouble(tongTienVND), "đồng"))
            'ElseIf ma_nt = "VND" Then
            'AddParam("Tien_Bang_Chu", TCS_Dich_So(Convert.ToDouble(tongTienNT), "đồng"))
            'End If


            AddParam("Key_MauCT", "")

            AddParam("Ma_CN", If(dr("ma_cn") Is Nothing, "........", dr("ma_cn").ToString()))
            AddParam("Ngay_GD", Convert.ToDateTime(dr("NGAY_CT")).ToString("dd/MM/yyyy"))
            Dim strDate As String = Convert.ToDateTime(DateTime.Now.ToString()).ToString("dd/MM/yyyy hh:mm:ss")
            Dim provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture
            AddParam("CurrentDateReport", Convert.ToDateTime(DateTime.Now, CultureInfo.InvariantCulture))

            AddParam("TKNo", "")
            AddParam("Tinh_KB", "")
            AddParam("NHB", "")
            AddParam("TK_THU", "")
            AddParam("So_HS", If(dr("SO_HS") Is Nothing, "........", dr("SO_HS").ToString()))
            AddParam("TAIKHOAN_NT_TEN_NH_TH", If(dr("TAIKHOAN_NT_TEN_NH_TH") Is Nothing, "........", dr("TAIKHOAN_NT_TEN_NH_TH").ToString()))
            'AddParam("TAIKHOAN_NT_MA_NH_TH", If(dr("TAIKHOAN_NT_MA_NH_TH") Is Nothing, "........", dr("TAIKHOAN_NT_MA_NH_TH").ToString()))
            AddParam("TAIKHOAN_NT_TK_TH", If(dr("TAIKHOAN_NT_TK_TH") Is Nothing, "........", dr("TAIKHOAN_NT_TK_TH").ToString()))
            AddParam("TAIKHOAN_NT_TEN_TK_TH", If(dr("TAIKHOAN_NT_TEN_TK_TH") Is Nothing, "........", dr("TAIKHOAN_NT_TEN_TK_TH").ToString()))
            AddParam("TK_THANH_TOAN_DIACHI", If(dr("TK_THANH_TOAN_DIACHI") Is Nothing, "........", dr("TK_THANH_TOAN_DIACHI").ToString()))

            AddParam("TienTKNH", Globals.Format_Number(dr("tt_nt_tongtien_vnd").ToString(), "."))
            AddParam("TienVAT", Globals.Format_Number(dr("vat").ToString(), "."))
            AddParam("TienPhi", Globals.Format_Number(dr("fee").ToString(), "."))
            AddParam("tienTKNO", Globals.Format_Number(dr("tongtien_truoc_vat").ToString(), "."))
            AddParam("SO_FCC", "")
            Dim dtGetTenGDV As DataTable = DataAccess.ExecuteToTable("select ten from tcs_dm_nhanvien where ma_nv='" & dr("ma_nv").ToString() & "'")

            If dtGetTenGDV.Rows.Count > 0 Then
                AddParam("TEN_GDV", dtGetTenGDV.Rows(0)(0).ToString)
            End If
            AddParam("TK_KN_NH", clsCommon.gf_CorrectString(dr("tk_thanhtoan").ToString()))
            Dim chk_pttt As Integer = Convert.ToInt64(dr("PTTT").ToString())
            Dim chk_ma_nt As String = dr("TT_NT_MA_NT").ToString()
            Dim chkTienMat As String = String.Empty
            Dim chkChuyenKhoan As String = String.Empty
            Dim chkVND As String = String.Empty
            Dim chkUSD As String = String.Empty

            If chk_pttt = 3 Then
                chkTienMat = "X"
            End If

            If chk_pttt = 1 Then
                chkChuyenKhoan = "X"
            End If

            If chk_ma_nt = "USD" Then
                chkUSD = "X"
            End If

            If chk_ma_nt = "VND" Then
                chkVND = "X"
            End If
            AddParam("LOAI_NT", dr("TT_NT_MA_NT").ToString())
            AddParam("TEN_NDKT", dr("CHUNGTU_CT_TEN_NDKT").ToString())

            AddParam("ChkTienMat", chkTienMat)
            AddParam("ChkChuyenKhoan", chkChuyenKhoan)
            AddParam("ChkVND", chkVND)
            AddParam("ChkUSD", chkUSD)

            'AddParam("ChkUSD", chkUSD)
            'AddParam("ChkUSD", chkUSD)
            'AddParam("ChkUSD", chkUSD)
            'AddParam("ChkUSD", chkUSD)
            'AddParam("ChkUSD", chkUSD)
            'AddParam("ChkUSD", chkUSD)
            'AddParam("ChkUSD", chkUSD)



            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
            'Dim CrExportOptions As ExportOptions

            rpvMain.HasExportButton = True
            rpvMain.DisplayToolbar = True
            rpvMain.EnableToolTips = True
            rpvMain.HasPageNavigationButtons = True
            rpvMain.ShowAllPageIds = True
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
            'Business.ChungTu.buChungTu.CTU_Update_LanIn(key)

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Có lỗi trong quá trình In giấy nộp tiền", Session.Item("TEN_DN"))
            'Throw (ex)
        End Try
    End Sub

    Private Sub AddParam(ByVal strName As String, ByVal strValue As String)
        Try
            pdvTCS.Value = strValue
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields(strName).ApplyCurrentValues(pcTCS)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub









End Class
