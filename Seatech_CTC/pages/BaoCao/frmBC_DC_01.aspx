﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmBC_DC_01.aspx.vb" Inherits="pages_BaoCao_frmBC_DC_01"
    EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Báo cáo đối chiếu 01</title>
    <style type="text/css">
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 100%;">
        <p>
            <asp:Button ID="btnKetXuat" runat="server" Text="Kết xuất" /></p>
        <table width="100%" id="tableBaoCao" runat="server">
            <tr>
                <td align="center" colspan="2">
                    <b style="text-decoration: underline;">TỔNG CỤC THUẾ</b>
                </td>
                <td align="center" colspan="9">
                    <b>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
                <td align="center" colspan="9">
                    <b style="text-decoration: underline;">Độc lập - Tự do - Hạnh phúc</b>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    Số: 01/BC-NTĐT
                </td>
                <td align="center" colspan="9">
                    <b>
                        <asp:Label ID="lblNgayDC" runat="server"></asp:Label></b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
                <td align="center" colspan="9">
                    <b>BÁO CÁO ĐỐI CHIẾU TRUYỀN NHẬN CHỨNG TỪ NỘP THUẾ ĐIỆN TỬ THÀNH CÔNG, KHỚP ĐÚNG</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
                <td align="center" colspan="9">
                    Thời gian đối chiếu: từ
                    <asp:Label ID="lblTuNgay" runat="server"></asp:Label>
                    đến
                    <asp:Label ID="lblDenNgay" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
                <td align="center" colspan="9">
                    Thời điểm đối chiếu:
                    <asp:Label ID="lblGioDC" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
                <td align="center" colspan="9">
                    Cơ quan thuế: Toàn quốc
                </td>
            </tr>
            <tr>
                <td colspan="11">
                    <table border="1" cellpadding="2" cellspacing="0">
                        <tr>
                            <td colspan="4">
                                <b>Kết quả đối chiếu</b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp;
                            </td>
                            <td>
                                <b>Số món</b>
                            </td>
                            <td>
                                <b>Số tiền nguyên tệ</b>
                            </td>
                            <td>
                                <b>Số tiền VND</b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Tổng số truyền nhận thành công
                            </td>
                            <td align="right">
                                <asp:Label ID="lblSum_TC" runat="server"></asp:Label>
                            </td>
                            <td align="right">
                                <asp:Label ID="lblSum_TC_Amount_USD" runat="server"></asp:Label>
                            </td>
                            <td align="right">
                                <asp:Label ID="lblSum_TC_Amount_VND" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="11">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="11">
                    <asp:GridView ID="grdChungTu" runat="server" Width="100%" AutoGenerateColumns="false"
                        HeaderStyle-BackColor="Yellow" EmptyDataText="Không có dữ liệu">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    STT</HeaderTemplate>
                                <ItemTemplate>
                                    <%#Container.DataItemIndex + 1%>
                                </ItemTemplate>
                                <HeaderStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <%--<asp:BoundField DataField="SO_GNT" HeaderText="Số giấy nộp tiền"/>--%>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Số tham chiếu</HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<%#Eval("MA_THAMCHIEU")%>
                                </ItemTemplate>
                                <HeaderStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Số giấy nộp tiền</HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<%#Eval("SO_GNT")%>
                                </ItemTemplate>
                                <HeaderStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Mã ref</HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<%#Eval("ref_no")%>
                                </ItemTemplate>
                                <HeaderStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="MA_HIEU_CTU" HeaderText="Mã hiệu chứng từ" />
                            <%--<asp:BoundField DataField="SO_CTU" HeaderText="Số chứng từ"/>--%>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Số chứng từ</HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<%#Eval("SO_CTU")%>
                                </ItemTemplate>
                                <HeaderStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="TEN_NNT" HeaderText="Tên NNT" HeaderStyle-VerticalAlign="Top" />
                            <%--<asp:BoundField DataField="MST" HeaderText="Mã số thuế"/>--%>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Mã số thuế</HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<%#Eval("MST")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                                <HeaderStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="NGAY_NOP_GNT" HeaderText="Ngày gửi GNT" HeaderStyle-VerticalAlign="Top" />
                            <asp:BoundField DataField="TGIAN_TCT" HeaderText="Thời gian TCT truyền" HeaderStyle-VerticalAlign="Top" />
                            <asp:BoundField DataField="TGIAN_NHTM" HeaderText="Thời gian NH nhận" HeaderStyle-VerticalAlign="Top" />
                            <asp:BoundField DataField="NGAY_NOP_THUE" HeaderText="Ngày nộp thuế điện tử" HeaderStyle-VerticalAlign="Top" />

                            <asp:BoundField DataField="SO_TIEN" DataFormatString="{0:n2}" 
                                HeaderText="Số tiền" ItemStyle-HorizontalAlign="Right" HeaderStyle-VerticalAlign="Top" >
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundField>
                           
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Loại tiền</HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<%#Eval("MA_NGUYENTE_R1")%>
                                </ItemTemplate>
                                <HeaderStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Trạng thái</HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<%#Eval("TTHAI")%>
                                </ItemTemplate>
                                <HeaderStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <%--<asp:BoundField DataField="SO_TIEN" DataFormatString="{0:n0}" 
                                HeaderText="Số tiền" ItemStyle-HorizontalAlign="Right" HeaderStyle-VerticalAlign="Top" >
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundField>--%>
                        </Columns>
                        <HeaderStyle BackColor="Yellow"></HeaderStyle>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
