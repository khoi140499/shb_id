﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmBC_DC_02.aspx.vb" Inherits="pages_BaoCao_frmBC_DC_02" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Báo cáo đối chiếu 02</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width:100%;">
    <p><asp:Button ID="btnKetXuat" runat="server" Text="Kết xuất"/></p>
        <table width="100%" id="tableBaoCao" runat="server">
            <tr>
                <td align="center" colspan="2"><b style="text-decoration: underline;" >TỔNG CỤC THUẾ</b></td>
                <td align="center" colspan="11"><b>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</b></td>
            </tr>
            <tr>
                <td colspan="2"></td>
                <td align="center" colspan="11"><b style="text-decoration: underline;">Độc lập - Tự do - Hạnh phúc</b></td>
            </tr>
            <tr>
                <td align="center" colspan="2">Số: 02/BC-NTĐT</td>
                <td align="center" colspan="11"><b><asp:Label ID="lblNgayDC" runat="server"></asp:Label></b></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td align="center" colspan="11"><b>BÁO CÁO ĐỐI CHIẾU TRUYỀN NHẬN CHỨNG TỪ NỘP THUẾ ĐIỆN TỬ CHÊNH LỆCH</b></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td align="center" colspan="11">Thời gian đối chiếu: từ <asp:Label ID="lblTuNgay" runat="server"></asp:Label> đến <asp:Label ID="lblDenNgay" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td align="center" colspan="11">Thời điểm đối chiếu: <asp:Label ID="lblGioDC" runat="server"></asp:Label> </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td align="center" colspan="11">Cơ quan thuế: Toàn quốc</td>
            </tr>
            <tr>
                <td colspan="13">
                    <table border="1" cellpadding="2" cellspacing="0">
                        <tr>
                            <td colspan="4"><b>Kết quả đối chiếu</b></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                            <td><b>Số món</b></td>
                            <td><b>Số tiền nguyên tệ</b></td>
                            <td><b>Số tiền VND</b></td>
                        </tr>
                         <tr>
                            <td colspan="2">Tổng số truyền nhận chênh lệch</td>
                            <td align="right"><asp:Label ID="lblSum_TONGDIEN_LECH" runat="server"></asp:Label></td>
                            <td align="right"><asp:Label ID="lblSum_TONGTIEN_LECH_USD" runat="server"></asp:Label></td>
                            <td align="right"><asp:Label ID="lblSum_TONGTIEN_LECH_VND" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2">Chi tiết theo loại</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2">-TCT có - NH không có</td>
                            <td align="right"><asp:Label ID="lblSum_TONGDIEN_TCTCO_NHTMKO" runat="server"></asp:Label></td>
                            <td align="right"><asp:Label ID="lblSum_TONGTIEN_TCTCO_NHTMKO_USD" runat="server"></asp:Label></td>
                            <td align="right"><asp:Label ID="lblSum_TONGTIEN_TCTCO_NHTMKO_VND" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2">-NH có - TCT không có</td>
                            <td align="right"><asp:Label ID="lblSum_TONGDIEN_NHTMCO_TCTKO" runat="server"></asp:Label></td>
                            <td align="right"><asp:Label ID="lblSum_TONGTIEN_NHTMCO_TCTKO_USD" runat="server"></asp:Label></td>
                            <td align="right"><asp:Label ID="lblSum_TONGTIEN_NHTMCO_TCTKO_VND" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="2">-TCT có - NH có - chênh lệch về trạng thái</td>
                            <td align="right"><asp:Label ID="lblSum_TONGDIEN_LECH_TTHAI" runat="server"></asp:Label></td>
                            <td align="right"><asp:Label ID="lblSum_TONGTIEN_LECH_TTHAI_USD" runat="server"></asp:Label></td>
                            <td align="right"><asp:Label ID="lblSum_TONGTIEN_LECH_TTHAI_VND" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="13">
                   &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="13">
                    <div id="ExportTable" runat="server">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
