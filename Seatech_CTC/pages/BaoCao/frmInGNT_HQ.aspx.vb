﻿Imports Business.BaoCao
Imports Business
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports Business.NewChungTu
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO
Imports Business_HQ
Imports System.Globalization

Partial Class pages_BaoCao_frmInGNT_HQ
    Inherits System.Web.UI.Page

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenKB As String
    Private mv_strTenTinhTP As String
    Private mv_strMaTinhTP As String
    Private mv_strSHKBDEF As String
    Private pdvTCS As New CrystalDecisions.Shared.ParameterDiscreteValue
    Private rptTCS As New ReportDocument
    Private pcTCS As New CrystalDecisions.Shared.ParameterValues
    'Private Shared strTT_TIENMAT As String = ConfigurationManager.AppSettings.Get("TK_TIENMAT").ToString()
    Private ten_NH As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LogApp.AddDebug("cmdInCT_Click", "LAP CHUNG TU THUE HAI QUAN: In chung tu")
        Try
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Session.Item("User").ToString.Length = 0 Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Not clsCommon.GetSessionByID(Session.Item("User")) Then
                Response.Redirect("~/pages/Warning.html", False)
                Exit Sub
            End If

            Dim provider As System.Globalization.CultureInfo = System.Globalization.CultureInfo.InvariantCulture
            Dim dDateTemp As DateTime

            Dim key As New Business_HQ.NewChungTu.KeyCTu
            Dim objHDR As New Business.NewChungTu.infChungTuHDR

            If Not Request.QueryString.Item("so_ct") Is Nothing Then
                key.So_CT = Request.QueryString.Item("so_ct").ToString
            End If

            Dim hdr As New Business_HQ.NewChungTu.infChungTuHDR
            Dim dtl As Business_HQ.NewChungTu.infChungTuDTL()
            Dim oChungTU As Business_HQ.NewChungTu.infChungTu = Nothing
            oChungTU = HaiQuan.HaiQuanController.GetChungTu(key.So_CT)
            If oChungTU Is Nothing Then Return
            hdr = oChungTU.HDR
            dtl = oChungTU.ListDTL.ToArray()

            Dim v_strMa_CN_Uer As String = clsCTU.TCS_GetMaChiNhanh(hdr.Ma_NV)
            Dim v_strBranchCode As String = clsCTU.TCS_GetMaCN_NH(v_strMa_CN_Uer)
            Dim v_strNameCN As String = clsCTU.TCS_GetTenCNNH(v_strMa_CN_Uer)
            Dim ds As DataSet = Business_HQ.buChungTu.CTU_IN_HQ(key)
            Dim strFileName As String = ""
            Dim v_strReportName As String = "In chứng từ thu ngân sách"

            strFileName = Server.MapPath("RPT\C102HQ.rpt")

            If (Not File.Exists(strFileName)) Then
            End If
            rptTCS.Load(strFileName)
            ds.Tables(0).TableName = "CTU"

            rptTCS.SetDataSource(ds)

            AddParam("KHCT", clsCommon.gf_CorrectString(hdr.KyHieu_CT))
            AddParam("SoCT", clsCommon.gf_CorrectString(hdr.So_CT))
            AddParam("Ten_NNThue", clsCommon.gf_CorrectString(hdr.Ten_NNThue))
            AddParam("Ma_NNThue", clsCommon.gf_CorrectString(hdr.Ma_NNThue))
            AddParam("DC_NNThue", clsCommon.gf_CorrectString(hdr.DC_NNThue))
            Dim strTenTinhTP As String = ""
            If hdr.QUAN_HUYEN_NNTIEN <> "" Then
                AddParam("Huyen_NNThue", clsCTU.Get_TenQuanHuyen(clsCommon.gf_CorrectString(hdr.QUAN_HUYEN_NNTIEN)))
                'strTenTinhTP = clsCTU.Get_TenTinhTP(hdr.QUAN_HUYEN_NNTIEN)
                'strTenTinhTP = strTenTinhTP.Split(";")(1).ToString()
                AddParam("Tinh_NNThue", strTenTinhTP)
            Else
                AddParam("Huyen_NNThue", "")
                AddParam("Tinh_NNThue", "")
            End If
            AddParam("Ten_NNTien", clsCommon.gf_CorrectString(hdr.Ten_NNTien))
            AddParam("Ma_NNTien", clsCommon.gf_CorrectString(hdr.Ma_NNTien))
            AddParam("DC_NNTien", clsCommon.gf_CorrectString(hdr.DC_NNTien))
            If hdr.Ma_Tinh <> "" Then
                AddParam("Huyen_NNTien", clsCTU.Get_TenQuanHuyen(hdr.Ma_Tinh))
                'strTenTinhTP = clsCTU.Get_TenTinhTP(hdr.Ma_Tinh)
                'strTenTinhTP = strTenTinhTP.Split(";")(1).ToString()
                AddParam("Tinh_NNTien", strTenTinhTP)
            Else
                AddParam("Huyen_NNTien", "")
                AddParam("Tinh_NNTien", "")
            End If

            AddParam("NHA", ten_NH & "-" & Get_TenCN(hdr.Ma_CN.ToString))
            AddParam("Ten_KB", Get_TenKhoBac(hdr.SHKB))
            AddParam("Ten_CQThu", Get_TenCQThu(clsCommon.gf_CorrectString(hdr.Ma_CQThu)))
            AddParam("Ma_CQThu", clsCommon.gf_CorrectString(hdr.Ma_CQThu))
            AddParam("So_TK", hdr.So_TK)

            If Not hdr.Ngay_TK Is Nothing And hdr.Ngay_TK <> "" Then
                dDateTemp = Date.ParseExact(hdr.Ngay_TK, "dd/MM/yyyy", provider)
                AddParam("Ngay_TK", dDateTemp.ToString("yyyy"))

                'dDateTemp = Date.ParseExact(hdr.Ngay_TK, "dd/MM/yyyy", Globalization.CultureInfo.InvariantCulture)
                'dDateTemp = Convert.ToDateTime(hdr.Ngay_TK)
                'AddParam("Ngay_TK", DateTime.Now.ToString("yyyy"))
            Else
                AddParam("Ngay_TK", "")
            End If
            If hdr.Ma_NT.Equals("VND") Then
                AddParam("Tien_Bang_Chu", TCS_Dich_So(hdr.TTien, "đồng"))
            Else
                AddParam("Tien_Bang_Chu", TCS_Dich_So((hdr.TTien * hdr.Ty_Gia), "đồng"))
            End If
            AddParam("Ten_KeToan", Get_HoTenNV(clsCommon.gf_CorrectString(hdr.Ma_NV)))
            AddParam("Key_MauCT", clsCommon.gf_CorrectString(hdr.PT_TT))
            AddParam("IsTamThu", clsCommon.gf_CorrectString(hdr.MA_NTK))
            AddParam("TK_KN_NH", clsCommon.gf_CorrectString(hdr.TK_KH_NH))
            AddParam("MA_CN", clsCommon.gf_CorrectString(hdr.Ma_CN))
            AddParam("ngay_GD", dDateTemp.ToString("dd/MM/yyyy"))
            'If hdr.PT_TT = "00" Then
            '    AddParam("TKNo", clsCommon.gf_CorrectString(strTT_TIENMAT))
            'Else
            AddParam("TKNo", clsCommon.gf_CorrectString(hdr.TK_KH_NH))
            'End If

            Dim dTTien, dPhiGD, dPhiVAT As Double
            If Not Double.TryParse(hdr.TTien, dTTien) Then
                dTTien = 0
            End If
            If Not Double.TryParse(hdr.PHI_GD, dPhiGD) Then
                dPhiGD = 0
            End If
            If Not Double.TryParse(hdr.PHI_VAT, dPhiVAT) Then
                dPhiVAT = 0
            End If
            AddParam("tienTKNO", Globals.Format_Number(dTTien + dPhiGD + dPhiVAT, "."))
            AddParam("TienTKNH", Globals.Format_Number(hdr.TTien.ToString, "."))
            AddParam("TienVAT", Globals.Format_Number(hdr.PHI_VAT.ToString, "."))
            AddParam("TienPhi", Globals.Format_Number(hdr.PHI_GD.ToString, "."))
            AddParam("Tinh_KB", clsCTU.Get_TenTinh(hdr.Ma_Xa))
            AddParam("SO_FT", hdr.So_CT_NH.ToString())
            AddParam("NHB", clsCommon.gf_CorrectString(hdr.Ten_NH_B))

            If hdr.Ma_NT.Equals("VND") Or hdr.Ma_NT.Equals("USD") Then
                AddParam("LoaiTien", clsCommon.gf_CorrectString(hdr.Ma_NT))
                AddParam("Ma_NT", "")
            Else
                AddParam("Ma_NT", clsCommon.gf_CorrectString(hdr.Ma_NT))
            End If
            If hdr.PT_TT.ToString().Equals("01") Then
                hdr.PT_TT = "01"
            End If
            If hdr.PT_TT.Equals("05") Then
                hdr.PT_TT = "00"
            End If
            AddParam("PT_TT", hdr.PT_TT.ToString())

            'AddParam("PT_TT", hdr.PT_TT.ToString())
            AddParam("SO_FCC", hdr.So_CT_NH)
            Dim dtGetTenGDV As DataTable = DataAccess.ExecuteToTable("select ten from tcs_dm_nhanvien where ma_nv='" & hdr.Ma_NV & "'")

            If dtGetTenGDV.Rows.Count > 0 Then
                AddParam("TEN_GDV", dtGetTenGDV.Rows(0)(0).ToString)
            End If

            Dim dtGetTenCN As DataTable = DataAccess.ExecuteToTable("select name from tcs_dm_chinhanh where branch_id='" & hdr.Ma_CN & "'")

            If dtGetTenCN.Rows.Count > 0 Then
                AddParam("ten_cn", dtGetTenCN.Rows(0)(0).ToString)
            End If

            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait

            rpvMain.HasExportButton = True
            rpvMain.DisplayToolbar = True
            rpvMain.EnableToolTips = True
            rpvMain.HasPageNavigationButtons = True
            rpvMain.ShowAllPageIds = True
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()
            Dim frm As New Business.BaoCao.infBaoCao
            frm.TCS_DS = ds

            'Business.ChungTu.buChungTu.CTU_Update_LanIn(key)

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Có lỗi trong quá trình In giấy nộp tiền", Session.Item("TEN_DN"))
            'Throw (ex)
        End Try
    End Sub

    Private Sub AddParam(ByVal strName As String, ByVal strValue As String)
        Try
            pdvTCS.Value = strValue
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields(strName).ApplyCurrentValues(pcTCS)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub


    Private Function GetMaDThuBySHKB(ByVal shkb As String) As String
        Dim strSql As String = "SELECT * FROM TCS_THAMSO where ten_ts='SHKB' and giatri_ts='" & shkb & "'"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ma_dthu").ToString
        Else
            Return ""
        End If
    End Function
    Private Function GetQuanHuyenByXaID(ByVal strMa_Dthu As String) As String

        Dim strSql As String = "SELECT ten FROM tcs_dm_xa where ma_xa= (select Giatri_ts from tcs_thamso where upper(ten_ts)='MA_DBHC' and ma_dthu ='" & strMa_Dthu & "')"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ten").ToString
        Else
            Return ""
        End If
    End Function
    Private Function GetTinhThanhByMaXa(ByVal strMa_Dthu As String) As String
        Dim strSql As String = "SELECT ten FROM tcs_dm_xa where ma_xa=(select Giatri_ts from tcs_thamso where upper(ten_ts)='MA_TTP' and ma_dthu ='" & strMa_Dthu & "')"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ten").ToString
        Else
            Return ""
        End If
    End Function
End Class
