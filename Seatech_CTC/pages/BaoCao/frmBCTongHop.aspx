﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmBCTongHop.aspx.vb" Inherits="pages_BaoCao_frmBCTongHop" Title="ĐỐI CHIẾU" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script language="javascript" type="text/javascript">
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }
    </script>

    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popcalendar.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/DatePicker/jquery.js" type="text/javascript"></script>
      <!-- required plugins -->
    <script src="../../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->
    <!-- jquery.datePicker.js -->
    <script src="../../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript" charset="utf-8">
         Date.firstDayOfWeek = 1;
         Date.format = 'dd/mm/yyyy';
         var sDate = new Date();
         var eDate = new Date();
         sDate.setDate(sDate.getDate() - 3650);
         eDate.setDate(eDate.getDate() + 3650);
         $(function () {
             $('.date-pick').datePicker({
                 clickInput: true,
                 startDate: sDate.asString(),
                 endDate: eDate.asString()
             })

             if ($('#ctl00_MstPg05_MainContent_txtTuNgay').val() == "" && $("#ctl00_MstPg05_MainContent_txtDenNgay").val() == "") {
                 $('.date-pick').datePicker({
                     clickInput: true
                 })
             }
         });
    </script>
    <style type="text/css">
        .ButtonCommand {
            height: 26px;
        }
        .auto-style1 {
            width: 21%;
        }
        .auto-style2 {
            width: 16%;
        }
        .auto-style3 {
            width: 15%;
        }
        .auto-style4 {
            width: 14%;
        }
        .auto-style5 {
            width: 17%;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <asp:HiddenField ID="doichieuConfirm" Value="" runat="Server" />
    <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height: 450px">
        <tr>
            <td valign="top">
                <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="pageTitle">
                            <asp:Label ID="Label1" runat="server">BÁO CÁO TỔNG HỢP CHỨNG TỪ</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="errorMessage">
                            <asp:Label ID="lblError" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: Blue" align="left">
                            <asp:Label ID="lblSuccess" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">
                            <table cellpadding="2" cellspacing="1" border="0" class="form_input">
                                <tr>
                                    <td align="left" style="width: 15%" class="form_label">Từ ngày</td>
                                    <td class="form_control" style="width: 35%"><span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                                        <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                                            onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                                    </span></td>
                                    <td align="left" style="width: 15%" class="form_label">Đến ngày</td>
                                    <td class="form_control" style="width: 35%">
                                        <span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                                            <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                                                onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <asp:Button runat="server" ID="btnSearch" Text="Tra cứu" />
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table border="1" width="100%">
                    <tr>
                        <td colspan="6">
                            <b>BÁO CÁO TỔNG HỢP CHỨNG TỪ</b>
                        </td>
                    </tr>
                     <tr>
                        <td class="auto-style1" >
                        </td>
                         <td class="auto-style5" >
                             <b>Thuế nội địa tại quầy</b>
                        </td>
                         <td class="auto-style2" >
                             <b>Thuế hải quan tại quầy</b>
                        </td>
                         <td class="auto-style3" >
                             <b>Thuế điện tử nội địa</b>
                        </td>
                          <td class="auto-style4" >
                             <b>Thuế hải quan 247</b>
                        </td>
                         <td >
                             <b>Điện từ ngân hàng khác</b>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="auto-style1">
                            <asp:Label ID="Label7" runat="server" Text="Tổng giao dịch" CssClass="label"></asp:Label>
                        </td>
                        <td class="auto-style5">
                            <asp:Label ID="lbTongNDTQ" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                        <td class="auto-style2">
                            <asp:Label ID="lbTongHQTQ" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                         <td class="auto-style3">
                            <asp:Label ID="lbTongNTDT" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                         <td class="auto-style4">
                            <asp:Label ID="lbTongHQ247" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                         <td width="40%">
                            <asp:Label ID="lbTongDienNHKhac" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="auto-style1" >
                            <asp:Label ID="Label4" runat="server" Text="Tổng giao dịch đi song phương" CssClass="label"></asp:Label>
                        </td>
                        <td class="auto-style5">
                            <asp:Label ID="lbTongNDTQSP" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                        <td class="auto-style2">
                            <asp:Label ID="lbTongHQTQSP" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                         <td class="auto-style3">
                            <asp:Label ID="lbTongNTDTSP" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                         <td class="auto-style4">
                            <asp:Label ID="lbTongHQ247SP" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                         <td>
                            <asp:Label ID="lbTongNHKhacSP" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="auto-style1" >
                            <asp:Label ID="Label13" runat="server" Text="Tổng số GD gửi song phương lỗi" CssClass="label"></asp:Label>
                        </td>
                        <td class="auto-style5">
                            <asp:Label ID="lbTongNDTQSP_Loi" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                        <td class="auto-style2">
                            <asp:Label ID="lbTongHQTQSP_Loi" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                         <td class="auto-style3" >
                            <asp:Label ID="lbTongNTDTSP_Loi" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                        <td class="auto-style4">
                            <asp:Label ID="lbTongHQ247SP_Loi" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbTongNHKhacSP_Loi" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="auto-style1">
                            <asp:Label ID="Label17" runat="server" Text="Tổng số GD gửi song phương thành công" CssClass="label"></asp:Label>
                        </td>
                        <td class="auto-style5">
                            <asp:Label ID="lbTongNDTQSP_TC" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                        <td class="auto-style2">
                            <asp:Label ID="lbTongHQTQSP_TC" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                          <td class="auto-style3">
                            <asp:Label ID="lbTongNTDTSP_TC" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                        <td class="auto-style4">
                            <asp:Label ID="lbTongHQ247SP_TC" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbTongNHKhacSP_TC" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="auto-style1">
                            <asp:Label ID="Label9" runat="server" Text="Tổng số GD đi Citad" CssClass="label"></asp:Label>
                        </td>
                        <td class="auto-style5">
                            <asp:Label ID="lbTongNDTQ_CITAD" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                        <td class="auto-style2">
                            <asp:Label ID="lbTongHQTQ_CITAD" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                          <td class="auto-style3">
                            <asp:Label ID="lbTongNTDT_CITAD" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                        <td class="auto-style4">
                            <asp:Label ID="lbTongHQ247_CITAD" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbTongNHKhac_CITAD" runat="server" Text="0" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                   
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
