﻿Imports Business.BaoCao
Imports Business
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports Business.NewChungTu
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO
Imports System.Configuration
Imports CorebankServiceESB


Partial Class pages_BaoCao_frmInGNT
    Inherits System.Web.UI.Page
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenKB As String
    Private mv_strTenTinhTP As String
    Private mv_strMaTinhTP As String
    Private mv_strSHKBDEF As String
    Private pdvTCS As New CrystalDecisions.Shared.ParameterDiscreteValue
    Private rptTCS As New ReportDocument
    Private pcTCS As New CrystalDecisions.Shared.ParameterValues
    Private ten_NH As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Session.Item("User").ToString.Length = 0 Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Not clsCommon.GetSessionByID(Session.Item("User")) Then
                Response.Redirect("~/pages/Warning.html", False)
                Exit Sub
            End If
            If Not Session.Item("User") Is Nothing Then
                Dim v_dt As DataTable = clsCTU.TCS_GetParams(CType(Session("User"), Integer))
                For Each v_dr As DataRow In v_dt.Rows
                    Select Case v_dr("TEN_TS").ToString
                        Case "MA_DT"
                            mv_strMaDiemThu = v_dr("GIATRI_TS").ToString
                        Case "SHKB"
                            mv_strSHKB = v_dr("GIATRI_TS").ToString
                        Case "KHCT"
                            mv_strKyHieuCT = v_dr("GIATRI_TS").ToString
                        Case "TEN_TINH"
                            mv_strTenTinhTP = v_dr("GIATRI_TS").ToString
                            'Case "SHKB"
                            'mv_strSHKBDEF = v_dr("GIATRI_TS").ToString
                    End Select
                Next
            End If

            Dim key As New KeyCTu
            Dim objHDR As New NewChungTu.infChungTuHDR
            Dim isBS As String = Nothing
            If Request.QueryString.Item("SHKB") Is Nothing Then

            Else
                key.Kyhieu_CT = mv_strKyHieuCT
                key.So_CT = Request.QueryString.Item("SoCT").ToString
                key.SHKB = Request.QueryString.Item("SHKB").ToString
                key.Ngay_KB = ConvertDateToNumber(Request.QueryString.Item("NgayKB").ToString)
                key.Ma_NV = Request.QueryString.Item("MaNV").ToString
                key.So_BT = Request.QueryString.Item("SoBT").ToString
                isBS = Request.QueryString.Item("BS").ToString
                If mv_strSHKB <> key.SHKB Then
                    key.Ma_Dthu = GetMaDThuBySHKB(key.SHKB)
                Else
                    key.Ma_Dthu = mv_strMaDiemThu
                End If
            End If
            'Dim v_TTCUTOFFTIME As String = clsCTU.TCS_TTCUTOFFTIME().ToString
            'Dim v_TKHachToan As String
            'If v_TTCUTOFFTIME = "1" Then
            '    v_TKHachToan = "120101001"
            'Else
            '    v_TKHachToan = "280701018"
            'End If
            Dim hdr As New NewChungTu.infChungTuHDR
            Dim dtl As NewChungTu.infChungTuDTL()
            hdr = Business.buChungTu.CTU_Header(key)
            dtl = Business.buChungTu.CTU_ChiTiet(key)
            Dim v_strMa_CN_Uer As String = clsCTU.TCS_GetMaChiNhanh(hdr.Ma_NV)

            Dim v_strBranchCode As String = clsCTU.TCS_GetMaCN_NH(v_strMa_CN_Uer)
            Dim ds As DataSet = Business.buChungTu.CTU_IN_LASER(key)
            'gLoaiBaoCao = Report_Type.TCS_CTU_LASER
            Dim strFileName As String = ""
            Dim v_strReportName As String = "In chứng từ thu ngân sách"

            If hdr.Ma_NT.Equals("VND") Then
                ' strFileName = Server.MapPath("RPT\TCS_CTU_LASER_T7.rpt")
                If hdr.Ma_LThue = "04" Then
                    strFileName = Server.MapPath("RPT\C109HQ.rpt")
                Else
                    'strFileName = Server.MapPath("RPT\C102TCT.rpt")
                    strFileName = Server.MapPath("RPT\C102TCTNEW.rpt")
                End If
            Else
                strFileName = Server.MapPath("RPT\TCS_CTU_NguyenTe.rpt")
            End If
            If (Not File.Exists(strFileName)) Then
            End If
            rptTCS.Load(strFileName)
            ds.Tables(0).TableName = "CTU"
            'ds.WriteXml("D:\gntnguyente.xml", XmlWriteMode.WriteSchema)
            rptTCS.SetDataSource(ds)
            Dim mCopy As String = ""
            isBS = Business.ChungTu.buChungTu.CTU_Get_LanIn(key)
            If Not isBS Is Nothing Then
                If isBS.Equals("0") Then
                    mCopy = "Bản Chính"
                Else
                    mCopy = "Bản Sao"
                End If
            End If

            Dim dtFeeVat As DataTable
            dtFeeVat = CorebankServiceESB.clsCoreBank.GetFeeVat(hdr.So_CT_NH)
            If dtFeeVat.Rows.Count > 0 Then
                AddParam("TienPhi", Globals.Format_Number(dtFeeVat.Rows(0)("AMOUNTPHI").ToString(), "."))     '
                AddParam("TienVAT", Globals.Format_Number(dtFeeVat.Rows(0)("AMOUNTVAT").ToString(), "."))     '
            Else
                AddParam("TienPhi", Globals.Format_Number(hdr.PHI_GD.ToString, "."))         '
                AddParam("TienVAT", Globals.Format_Number(hdr.PHI_VAT.ToString, "."))        '
            End If


      
            AddParam("KHCT", hdr.KyHieu_CT)   '
            AddParam("SoCT", hdr.So_CT)       '
            AddParam("TK_KN_NH", clsCommon.gf_CorrectString(hdr.TK_KH_NH))     '
            AddParam("Ten_NNThue", hdr.Ten_NNThue.ToString())     '
            AddParam("Ma_NNThue", clsCommon.gf_CorrectString(hdr.Ma_NNThue))    '
            AddParam("DC_NNThue", clsCommon.gf_CorrectString(hdr.DC_NNThue))    '
            AddParam("Huyen_NNThue", clsCommon.gf_CorrectString(hdr.TEN_HUYEN_NNT))    '
            AddParam("Tinh_NNThue", clsCommon.gf_CorrectString(hdr.TEN_TINH_NNT))     '
            AddParam("Ten_NNTien", clsCommon.gf_CorrectString(hdr.Ten_NNTien))      '
            AddParam("Ma_NNTien", clsCommon.gf_CorrectString(hdr.Ma_NNTien))        '
            AddParam("DC_NNTien", clsCommon.gf_CorrectString(hdr.DC_NNTien))          '
            AddParam("Huyen_NNTien", clsCommon.gf_CorrectString(hdr.HUYEN_NNTHAY))     '
            AddParam("Tinh_NNTien", clsCommon.gf_CorrectString(hdr.TINH_NNTHAY))        '

            AddParam("NHA", ten_NH & " - " & hdr.MA_NH_A.ToString)    '
            AddParam("NHB", hdr.Ten_NH_B.ToString)        '

            AddParam("Ten_KB", Get_TenKhoBac(key.SHKB))       '
            AddParam("Tinh_KB", clsCommon.gf_CorrectString(hdr.QUAN_HUYEN_NNTIEN))       '
            AddParam("Ma_CQThu", clsCommon.gf_CorrectString(hdr.Ma_CQThu))             '
            'AddParam("MaCQThu", clsCommon.gf_CorrectString(hdr.Ma_CQThu))     '
            AddParam("Ten_CQThu", Get_TenCQThu(clsCommon.gf_CorrectString(hdr.Ma_CQThu)))     '

            AddParam("So_TK", hdr.So_TK) '
            If Not hdr.Ngay_TK Is Nothing And hdr.Ngay_TK <> "" Then
                AddParam("Ngay_TK", Convert.ToDateTime(hdr.Ngay_TK).ToString("yyyy"))  '
            Else
                AddParam("Ngay_TK", "")   '
            End If
         
          
            If hdr.Ma_NT.Equals("VND") Then
                AddParam("Tien_Bang_Chu", TCS_Dich_So(hdr.TTien, "đồng"))    '
            Else
                AddParam("Tien_Bang_Chu", TCS_Dich_So((hdr.TTien * hdr.Ty_Gia), "đồng"))    '
            End If


            AddParam("SO_FT", hdr.So_CT_NH)      '

            AddParam("Ten_KeToan", Get_HoTenNV(clsCommon.gf_CorrectString(hdr.Ma_NV)))     '
            If hdr.PT_TT.Equals("01") Then
                AddParam("Key_MauCT", "1")     '
            Else
                AddParam("Key_MauCT", "0")      '
            End If

            If hdr.PT_TT.ToString().Equals("01") Then
                hdr.PT_TT = "01"
            End If
            If hdr.PT_TT.Equals("05") Then
                hdr.PT_TT = "00"
            End If

            AddParam("PT_TT", hdr.PT_TT.ToString())       '
            AddParam("Ma_CN", hdr.MA_CN)       '

            Dim dtGetTenCN As DataTable = DataAccess.ExecuteToTable("select name from tcs_dm_chinhanh where branch_id='" & hdr.Ma_CN & "'")

            If dtGetTenCN.Rows.Count > 0 Then
                AddParam("ten_cn", dtGetTenCN.Rows(0)(0).ToString)            '
            End If


            AddParam("Ngay_GD", Convert.ToDateTime(hdr.Ngay_HT).ToString("dd/MM/yyyy  HH:mm:ss"))    '

            AddParam("TienTKNO", Globals.Format_Number(Double.Parse(hdr.TTien.ToString) + Double.Parse(hdr.PHI_GD.ToString) + Double.Parse(hdr.PHI_VAT.ToString), "."))  '
            AddParam("TienTKNH", Globals.Format_Number(hdr.TTien.ToString, "."))   '

            AddParam("TKNo", clsCommon.gf_CorrectString(hdr.TK_KH_NH))    '
            'If Not hdr.Ma_NT.Equals("VND") Then
            '    AddParam("Ma_NT", clsCommon.gf_CorrectString(hdr.MA_NTK))
            'Else
            '    AddParam("Ma_NT", clsCommon.gf_CorrectString(hdr.Ma_NT))
            'End If

            If Not hdr.Ma_NT.Equals("VND") And Not hdr.Ma_NT.Equals("USD") Then
                AddParam("Ma_NT", clsCommon.gf_CorrectString(hdr.Ma_NT))
            Else
                AddParam("Ma_NT", "")
            End If

            AddParam("LoaiTien", clsCommon.gf_CorrectString(hdr.Ma_NT))      '

            AddParam("SO_FCC", hdr.So_CT_NH)           '
            Dim dtGetTenGDV As DataTable = DataAccess.ExecuteToTable("select ten from tcs_dm_nhanvien where ma_nv='" & hdr.Ma_NV & "'")

            If dtGetTenGDV.Rows.Count > 0 Then
                AddParam("TEN_GDV", dtGetTenGDV.Rows(0)(0).ToString)       '
            End If

      
            AddParam("IsTamThu", hdr.MA_NTK)     '
            rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
            rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
            rpvMain.ReportSource = rptTCS
            rpvMain.DataBind()

            Dim frm As New Business.BaoCao.infBaoCao
            frm.TCS_DS = ds
            Business.ChungTu.buChungTu.CTU_Update_LanIn(key)

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Có lỗi trong quá trình In giấy nộp tiền", Session.Item("TEN_DN"))
            'Throw (ex)
        End Try
    End Sub

    Private Sub AddParam(ByVal strName As String, ByVal strValue As String)
        Try
            pdvTCS.Value = strValue
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields(strName).ApplyCurrentValues(pcTCS)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub


    Private Function GetMaDThuBySHKB(ByVal shkb As String) As String
        Dim strSql As String = "SELECT * FROM TCS_THAMSO where ten_ts='SHKB' and giatri_ts='" & shkb & "'"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ma_dthu").ToString
        Else
            Return ""
        End If
    End Function
    Private Function GetQuanHuyenByXaID(ByVal strMa_Dthu As String) As String

        Dim strSql As String = "SELECT ten FROM tcs_dm_xa where ma_xa= '" & strMa_Dthu & "'"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ten").ToString
        Else
            Return ""
        End If
    End Function
    Private Function GetTinhThanhByMaXa(ByVal strMa_Dthu As String) As String
        Dim strSql As String = "SELECT ten FROM tcs_dm_xa where ma_xa=(select Giatri_ts from tcs_thamso where upper(ten_ts)='MA_TTP' and ma_dthu ='" & strMa_Dthu & "')"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ten").ToString
        Else
            Return ""
        End If
    End Function



    'Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click

    '    rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
    '    rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
    '    ' rpvMain.ReportSource = rptTCS
    '    rptTCS.PrintOptions.PrinterName = ""
    '    rptTCS.PrintToPrinter(1, False, 1, 1)
    '    Dim key As New KeyCTu
    '    If Request.QueryString.Item("SHKB") Is Nothing Then

    '    Else
    '        key.Kyhieu_CT = mv_strKyHieuCT
    '        key.So_CT = Request.QueryString.Item("SoCT").ToString
    '        key.SHKB = Request.QueryString.Item("SHKB").ToString
    '        key.Ngay_KB = ConvertDateToNumber(Request.QueryString.Item("NgayKB").ToString)
    '        key.Ma_NV = Request.QueryString.Item("MaNV").ToString
    '        key.So_BT = Request.QueryString.Item("SoBT").ToString
    '        'isBS = Request.QueryString.Item("BS").ToString
    '        If mv_strSHKB <> key.SHKB Then
    '            key.Ma_Dthu = GetMaDThuBySHKB(key.SHKB)
    '        Else
    '            key.Ma_Dthu = mv_strMaDiemThu
    '        End If
    '    End If
    '    Business.ChungTu.buChungTu.CTU_Update_LanIn(key)
    'End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If Not Me.rptTCS Is Nothing Then
            Me.rptTCS.Dispose()
            rptTCS.Close()
        End If
    End Sub
End Class
