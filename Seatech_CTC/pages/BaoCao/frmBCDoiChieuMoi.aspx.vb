﻿Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports Business.BaoCao
Imports System.Data
Imports Business
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security

Partial Class pages_BaoCao_frmBCDoiChieuMoi
    Inherits System.Web.UI.Page

    Private mv_strNgay As String

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenDiemThu As String
    Private mv_strTenKB As String
    Private mv_strLoaiBaoCao As String
    Private strMaNT As String

    Private ds As DataSet
    Private frm As infBaoCao
    Private somon As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        'If Not clsCommon.GetSessionByID(Session.Item("User")) Then
        '    Response.Redirect("~/pages/Warning.html", False)
        '    Exit Sub
        'End If
        If Not IsPostBack Then
            Try
                txtNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                'clsCommon.addPopUpCalendarToImage(btnCalendar1, txtNgay, "dd/mm/yyyy")
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub
    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click

        If cboLoaiBaoCao.SelectedIndex = 0 Then
            clsCommon.ShowMessageBox(Me, "Bạn hãy chọn loại báo cáo")
        Else
            Dim sql As String = "select * from tcs_qly_dc_ntdt where to_char(ngay_dc,'dd/MM/yyyy') = '" + txtNgay.Value + "'"
            Dim dt As DataTable = New DataTable()
            Try
                dt = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text).Tables(0)
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Lỗi: " + ex.Message)
            End Try
            If dt.Rows.Count > 0 Then
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "CallMyFunction", "jsBC_DC(" & cboLoaiBaoCao.SelectedValue & "," & ConvertDateToNumber(txtNgay.Value) & ", '" & dt.Rows(0)("ma_gdich_dc").ToString() & "');", True)
            Else
                clsCommon.ShowMessageBox(Me, "Chưa nhận được dữ liệu đối chiếu từ TCT")
            End If

        End If
    End Sub
End Class
