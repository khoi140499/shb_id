﻿<%@ WebHandler Language="VB" Class="InGNTpdf" %>

Imports System
Imports System.Web
Imports Business.BaoCao
Imports Business
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports Business.NewChungTu
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO


Public Class InGNTpdf : Implements IHttpHandler, System.Web.SessionState.IReadOnlySessionState
    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenKB As String
    Private mv_strTenTinhTP As String
    Private mv_strMaTinhTP As String
    Private mv_strSHKBDEF As String
    Private pdvTCS As New CrystalDecisions.Shared.ParameterDiscreteValue
    Private rptTCS As New ReportDocument
    Private rptTCS_PHT As New ReportDocument
    Private pcTCS As New CrystalDecisions.Shared.ParameterValues
    ' Private Shared strTT_TIENMAT As String = ConfigurationManager.AppSettings.Get("TK_TIENMAT").ToString()
    Private ten_NH As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name")
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
             
        If Not context.Session("User") Is Nothing Then
            Dim v_dt As DataTable = clsCTU.TCS_GetParams(CType(context.Session("User"), Integer))
            For Each v_dr As DataRow In v_dt.Rows
                Select Case v_dr("TEN_TS").ToString
                    Case "MA_DT"
                        mv_strMaDiemThu = v_dr("GIATRI_TS").ToString
                    Case "SHKB"
                        mv_strSHKB = v_dr("GIATRI_TS").ToString
                    Case "KHCT"
                        mv_strKyHieuCT = v_dr("GIATRI_TS").ToString
                    Case "TEN_TINH"
                        mv_strTenTinhTP = v_dr("GIATRI_TS").ToString
                      
                End Select
            Next
        End If
        Try
           
            Dim key As New KeyCTu
            Dim objHDR As New NewChungTu.infChungTuHDR
            Dim isBS As String = Nothing
            If context.Request.QueryString.Item("SHKB") Is Nothing Then
                Exit Sub
            Else
                key.Kyhieu_CT = mv_strKyHieuCT
                key.So_CT = context.Request.QueryString.Item("SoCT").ToString
                key.SHKB = context.Request.QueryString.Item("SHKB").ToString
                key.Ngay_KB = ConvertDateToNumber(context.Request.QueryString.Item("NgayKB").ToString)
                key.Ma_NV = context.Request.QueryString.Item("MaNV").ToString
                key.So_BT = context.Request.QueryString.Item("SoBT").ToString
                isBS = context.Request.QueryString.Item("BS").ToString
                If mv_strSHKB <> key.SHKB Then
                    key.Ma_Dthu = GetMaDThuBySHKB(key.SHKB)
                Else
                    key.Ma_Dthu = mv_strMaDiemThu
                End If
            End If
            'Dim v_TTCUTOFFTIME As String = clsCTU.TCS_TTCUTOFFTIME().ToString
            'Dim v_TKHachToan As String
            'If v_TTCUTOFFTIME = "1" Then
            '    v_TKHachToan = "120101001"
            'Else
            '    v_TKHachToan = "280701018"
            'End If
            Dim hdr As New NewChungTu.infChungTuHDR
            Dim dtl As NewChungTu.infChungTuDTL()
            hdr = Business.buChungTu.CTU_Header(key)
            dtl = Business.buChungTu.CTU_ChiTiet(key)
            Dim v_strMa_CN_Uer As String = clsCTU.TCS_GetMaChiNhanh(hdr.Ma_NV)

            Dim v_strBranchCode As String = clsCTU.TCS_GetMaCN_NH(v_strMa_CN_Uer)
            Dim ds As DataSet = Business.buChungTu.CTU_IN_LASER(key)
            If hdr.Ma_LThue <> "04" Then
                ds = Business.buChungTu.CTU_IN_LASER_NDTQ(key)
            End If
            
            'gLoaiBaoCao = Report_Type.TCS_CTU_LASER
            Dim strFileName As String = ""
            Dim v_strReportName As String = "In chứng từ thu ngân sách"

            If hdr.Ma_NT.Equals("VND") Then
                ' strFileName = Server.MapPath("RPT\TCS_CTU_LASER_T7.rpt")
                If hdr.Ma_LThue = "04" Then
                    strFileName = context.Server.MapPath("RPT\C109HQ.rpt")
                Else
                    'strFileName = Server.MapPath("RPT\C102TCT.rpt")
                    strFileName = context.Server.MapPath("RPT\C102TCTNEW.rpt")
                End If
            Else
                strFileName = context.Server.MapPath("RPT\TCS_CTU_NguyenTe.rpt")
            End If
            If (Not File.Exists(strFileName)) Then
            End If
            
            'If hdr.Ma_LThue = "03" Then
            '    ds = Business.buChungTu.CTU_IN_TRUOCBA(key)
            'End If
            
            If hdr.Ma_LThue = "03" Then
                ds = Business.buChungTu.CTU_IN_TRUOCBA(key)
            ElseIf hdr.Ma_LThue = "08" Then
                ds = Business.buChungTu.CTU_IN_TRUOCBA_NHADAT(key.So_CT)
            End If

            rptTCS.Load(strFileName)
            ds.Tables(0).TableName = "CTU"
            'ds.WriteXml("D:\gntnguyente.xml", XmlWriteMode.WriteSchema)
            rptTCS.SetDataSource(ds)
            Dim mCopy As String = ""
            isBS = Business.ChungTu.buChungTu.CTU_Get_LanIn(key)
            If Not isBS Is Nothing Then
                If isBS.Equals("0") Then
                    mCopy = "Bản Chính"
                Else
                    mCopy = "Bản Sao"
                End If
            End If

            Dim dtFeeVat As DataTable
            dtFeeVat = CorebankServiceESB.clsCoreBank.GetFeeVat(hdr.So_CT_NH)
            If dtFeeVat.Rows.Count > 0 Then
                AddParam("TienPhi", Globals.Format_Number(dtFeeVat.Rows(0)("AMOUNTPHI").ToString(), "."))     '
                AddParam("TienVAT", Globals.Format_Number(dtFeeVat.Rows(0)("AMOUNTVAT").ToString(), "."))     '
            Else
                AddParam("TienPhi", Globals.Format_Number(hdr.PHI_GD.ToString, "."))         '
                AddParam("TienVAT", Globals.Format_Number(hdr.PHI_VAT.ToString, "."))        '
            End If


      
            AddParam("KHCT", hdr.KyHieu_CT)   '
            AddParam("SoCT", hdr.So_CT)       '
            AddParam("TK_KN_NH", clsCommon.gf_CorrectString(hdr.TK_KH_NH))     '
            AddParam("Ten_NNThue", hdr.Ten_NNThue.ToString())     '
            AddParam("Ma_NNThue", clsCommon.gf_CorrectString(hdr.Ma_NNThue))    '
            AddParam("DC_NNThue", clsCommon.gf_CorrectString(hdr.DC_NNThue))    '
            AddParam("Huyen_NNThue", clsCommon.gf_CorrectString(hdr.TEN_HUYEN_NNT))    '
            AddParam("Tinh_NNThue", clsCommon.gf_CorrectString(hdr.TEN_TINH_NNT))     '
            AddParam("Ten_NNTien", clsCommon.gf_CorrectString(hdr.Ten_NNTien))      '
            AddParam("Ma_NNTien", clsCommon.gf_CorrectString(hdr.Ma_NNTien))        '
            AddParam("DC_NNTien", clsCommon.gf_CorrectString(hdr.DC_NNTien))          '
            AddParam("Huyen_NNTien", clsCommon.gf_CorrectString(hdr.HUYEN_NNTHAY))     '
            AddParam("Tinh_NNTien", clsCommon.gf_CorrectString(hdr.TINH_NNTHAY))        '

            AddParam("NHA", ten_NH & " - " & hdr.MA_NH_A.ToString)    '
            AddParam("NHB", hdr.Ten_NH_B.ToString)        '

            AddParam("Ten_KB", Get_TenKhoBac(key.SHKB))       '
            AddParam("Tinh_KB", clsCommon.gf_CorrectString(hdr.QUAN_HUYEN_NNTIEN))       '
            AddParam("Ma_CQThu", clsCommon.gf_CorrectString(hdr.Ma_CQThu))             '
            'AddParam("MaCQThu", clsCommon.gf_CorrectString(hdr.Ma_CQThu))     '
            AddParam("Ten_CQThu", Get_TenCQThu(clsCommon.gf_CorrectString(hdr.Ma_CQThu)))     '

            AddParam("So_TK", hdr.So_TK) '
            If Not hdr.Ngay_TK Is Nothing And hdr.Ngay_TK <> "" Then
                AddParam("Ngay_TK", Convert.ToDateTime(hdr.Ngay_TK).ToString("yyyy"))  '
            Else
                AddParam("Ngay_TK", "")   '
            End If
         
          
            If hdr.Ma_NT.Equals("VND") Then
                AddParam("Tien_Bang_Chu", TCS_Dich_So(hdr.TTien, "đồng"))    '
            Else
                AddParam("Tien_Bang_Chu", TCS_Dich_So((hdr.TTien * hdr.Ty_Gia), "đồng"))    '
            End If


            AddParam("SO_FT", hdr.So_CT_NH)      '

            AddParam("Ten_KeToan", Get_HoTenNV(clsCommon.gf_CorrectString(hdr.Ma_NV)))     '
            If hdr.PT_TT.Equals("01") Then
                AddParam("Key_MauCT", "1")     '
            Else
                AddParam("Key_MauCT", "0")      '
            End If

            If hdr.PT_TT.ToString().Equals("01") Then
                hdr.PT_TT = "01"
            End If
            If hdr.PT_TT.Equals("05") Then
                hdr.PT_TT = "00"
            End If

            AddParam("PT_TT", hdr.PT_TT.ToString())       '
            AddParam("Ma_CN", hdr.MA_CN)       '

            Dim dtGetTenCN As DataTable = DataAccess.ExecuteToTable("select name from tcs_dm_chinhanh where branch_id='" & hdr.MA_CN & "'")

            If dtGetTenCN.Rows.Count > 0 Then
                AddParam("ten_cn", dtGetTenCN.Rows(0)(0).ToString)            '
            End If


            AddParam("Ngay_GD", Convert.ToDateTime(hdr.Ngay_HT).ToString("dd/MM/yyyy  HH:mm:ss"))    '

            AddParam("TienTKNO", Globals.Format_Number(Double.Parse(hdr.TTien.ToString) + Double.Parse(hdr.PHI_GD.ToString) + Double.Parse(hdr.PHI_VAT.ToString), "."))  '
            AddParam("TienTKNH", Globals.Format_Number(hdr.TTien.ToString, "."))   '

            AddParam("TKNo", clsCommon.gf_CorrectString(hdr.TK_KH_NH))    '
            'If Not hdr.Ma_NT.Equals("VND") Then
            '    AddParam("Ma_NT", clsCommon.gf_CorrectString(hdr.MA_NTK))
            'Else
            '    AddParam("Ma_NT", clsCommon.gf_CorrectString(hdr.Ma_NT))
            'End If

            If Not hdr.Ma_NT.Equals("VND") And Not hdr.Ma_NT.Equals("USD") Then
                AddParam("Ma_NT", clsCommon.gf_CorrectString(hdr.Ma_NT))
            Else
                AddParam("Ma_NT", "")
            End If

            AddParam("LoaiTien", clsCommon.gf_CorrectString(hdr.Ma_NT))      '

            AddParam("SO_FCC", hdr.So_CT_NH)           '
            Dim dtGetTenGDV As DataTable = DataAccess.ExecuteToTable("select ten from tcs_dm_nhanvien where ma_nv='" & hdr.Ma_NV & "'")

            If dtGetTenGDV.Rows.Count > 0 Then
                AddParam("TEN_GDV", dtGetTenGDV.Rows(0)(0).ToString)       '
            End If
            AddParam("IsTamThu", hdr.MA_NTK)
            Business.ChungTu.buChungTu.CTU_Update_LanIn(key)
            Try
                rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
                rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
                'rptTCS.ExportToHttpResponse(ExportFormatType.PortableDocFormat, context.Response, False, "Giaynoptien")
                
                Try
                    'rptTCS.Load(Server.MapPath(@"MyReport.rpt"))
                    Dim oStream As System.IO.Stream
                    Dim byteArray As Byte() = New Byte() {}
                    oStream = rptTCS.ExportToStream(ExportFormatType.PortableDocFormat)
                    byteArray = New Byte(oStream.Length) {}
                    oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1))
                    context.Response.ClearContent()
                    context.Response.ClearHeaders()
                    context.Response.ContentType = "application/pdf"
                    
                    context.Response.BinaryWrite(byteArray)
                    context.Response.Flush()
                    context.Response.Close()
                Catch ex As Exception
                    LogDebug.WriteLog("1.Exception: " & ex.Message & " \n " & ex.StackTrace, Diagnostics.EventLogEntryType.Error)
                End Try
            Catch ex As Exception
                LogDebug.WriteLog("Exception: " & ex.Message & " \n " & ex.StackTrace, Diagnostics.EventLogEntryType.Error)
            Finally
                rptTCS.Close()
                rptTCS.Dispose()
            End Try
            
        Catch ex As Exception
            Dim xxx = "Lỗi trong quá trình in báo cáo : " + ex.Message
            context.Response.Write("<script language=" + "javascript" + ">alert('" + xxx + "');</script>")
        Finally

        End Try
     
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    Private Function GetMaDThuBySHKB(ByVal shkb As String) As String
        Dim strSql As String = "SELECT * FROM TCS_THAMSO where ten_ts='SHKB' and giatri_ts='" & shkb & "'"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ma_dthu").ToString
        Else
            Return ""
        End If
    End Function
    Private Function CreateTableSub() As DataTable
        Dim v_dt As New DataTable()
        v_dt.Columns.Add(New DataColumn("SO_BT"))
        v_dt.Columns.Add(New DataColumn("NO_CO"))
        v_dt.Columns.Add(New DataColumn("MA_CN"))
        v_dt.Columns.Add(New DataColumn("SO_TK"))
        v_dt.Columns.Add(New DataColumn("TEN_TK"))
        v_dt.Columns.Add(New DataColumn("SO_TIEN"))
        v_dt.Columns.Add(New DataColumn("NOI_DUNG"))
        Return v_dt
    End Function
    Private Sub AddParam(ByVal strName As String, ByVal strValue As String)
        Try
            pdvTCS.Value = strValue
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields(strName).ApplyCurrentValues(pcTCS)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub
End Class