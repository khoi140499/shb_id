﻿Imports System.Data
Imports VBOracleLib
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common.mdlSystemVariables
Imports Business_HQ.HeThong
Imports Business_HQ.Common
Imports CustomsServiceV3
Imports Business_HQ


Partial Class pages_BaoCao_frmBCTongHop
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            LoadForm()
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs)
        ViewState("update") = Session("update")
    End Sub


    Sub LoadForm()
        Dim strNgayHT As String = DateTime.Now.ToString("dd/MM/yyyy")
        LoadTTTheoNgay(strNgayHT, strNgayHT)
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Request.QueryString("errmsg") Is Nothing Or Request.QueryString("errmsg") = "" Then
            If Not Request.QueryString("msg") Is Nothing Then
                lblSuccess.Text = Request.QueryString("msg")
            End If
        Else
            lblError.Text = Request.QueryString("errmsg")
        End If
        If Not IsPostBack Then
            ResetField()
        End If
    End Sub
    Sub ResetField()

    End Sub

    Protected Sub LoadTTTheoNgay(ByVal strTuNgay As String, strDenNgay As String)

        Dim strTrangThai As String = ""
        Try
            lbTongNDTQ.Text = getDuLieu("NDTQ", "TOTAL")
            lbTongHQTQ.Text = getDuLieu("HQTQ", "TOTAL")
            lbTongNTDT.Text = getDuLieu("NTDT", "TOTAL")
            lbTongHQ247.Text = getDuLieu("HQ247", "TOTAL")
            lbTongDienNHKhac.Text = getDuLieuNHKhac("TOTAL")

            lbTongNDTQSP.Text = getDuLieu("NDTQ", "TOTAL_SP")
            lbTongHQTQSP.Text = getDuLieu("HQTQ", "TOTAL_SP")
            lbTongNTDTSP.Text = getDuLieu("NTDT", "TOTAL_SP")
            lbTongHQ247SP.Text = getDuLieu("HQ247", "TOTAL_SP")
            lbTongNHKhacSP.Text = getDuLieuNHKhac("TOTAL")

            lbTongNDTQSP_Loi.Text = getDuLieu("NDTQ", "TOTAL_SPLOI")
            lbTongHQTQSP_Loi.Text = getDuLieu("HQTQ", "TOTAL_SPLOI")
            lbTongNTDTSP_Loi.Text = getDuLieu("NTDT", "TOTAL_SPLOI")
            lbTongHQ247SP_Loi.Text = getDuLieu("HQ247", "TOTAL_SPLOI")
            lbTongNHKhacSP_Loi.Text = getDuLieuNHKhac("TOTAL_LOI")

            lbTongNDTQSP_TC.Text = getDuLieu("NDTQ", "TOTAL_SPTC")
            lbTongHQTQSP_TC.Text = getDuLieu("HQTQ", "TOTAL_SPTC")
            lbTongNTDTSP_TC.Text = getDuLieu("NTDT", "TOTAL_SPTC")
            lbTongHQ247SP_TC.Text = getDuLieu("HQ247", "TOTAL_SPTC")
            lbTongNHKhacSP_TC.Text = getDuLieuNHKhac("TOTAL_TC")

            lbTongNDTQ_CITAD.Text = getDuLieu("NDTQ", "TOTAL_CITAD")
            lbTongHQTQ_CITAD.Text = getDuLieu("HQTQ", "TOTAL_CITAD")
            lbTongNTDT_CITAD.Text = getDuLieu("NTDT", "TOTAL_CITAD")
            lbTongHQ247_CITAD.Text = getDuLieu("HQ247", "TOTAL_CITAD")
            lbTongNHKhac_CITAD.Text = "0"

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtTuNgay.Value.Trim() = "" Or txtDenNgay.Value.Trim() = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải chọn từ ngày và đến ngày")
            Return
        End If
        Dim strTuNgay As String = txtTuNgay.Value.Trim()
        Dim strDenNgay As String = txtDenNgay.Value.Trim()
        LoadTTTheoNgay(strTuNgay, strDenNgay)
    End Sub

    Protected Function getDuLieu(ByVal strLThue As String, ByVal strLoaiGD As String) As String
        Dim dt As New DataTable
        Dim strSQL As String = ""
        Try
            strSQL &= "Select count(*) as total from"
            If strLThue = "NDTQ" Or strLThue = "HQTQ" Then
                strSQL &= " tcs_ctu_hdr "
            ElseIf strLThue = "NTDT" Then
                strSQL &= " tcs_ctu_ntdt_hdr "
            ElseIf strLThue = "HQ247" Then
                strSQL &= " tcs_hq247_304  "
            End If


            strSQL &= "where "
            If strLThue = "NDTQ" Or strLThue = "HQTQ" Then
                strSQL &= " ngay_kh_nh >= to_date('" & txtTuNgay.Value.Trim & " 00:00:00','dd/MM/yyyy HH24:MI:SS') and ngay_kh_nh <= to_date('" & txtDenNgay.Value.Trim() & " 23:59:59','dd/MM/yyyy HH24:MI:SS') "
            ElseIf strLThue = "NTDT" Then
                strSQL &= " ngay_nhang >= to_date('" & txtTuNgay.Value.Trim & " 00:00:00','dd/MM/yyyy HH24:MI:SS') and ngay_nhang <= to_date('" & txtDenNgay.Value.Trim() & " 23:59:59','dd/MM/yyyy HH24:MI:SS') "
                strSQL &= " and tthai_ctu='11' "
            ElseIf strLThue = "HQ247" Then
                strSQL &= " ngaynhanmsg >= to_date('" & txtTuNgay.Value.Trim & " 00:00:00','dd/MM/yyyy HH24:MI:SS') and ngaynhanmsg <= to_date('" & txtDenNgay.Value.Trim() & " 23:59:59','dd/MM/yyyy HH24:MI:SS') "
                strSQL &= " and trangthai in ('02','03','04','09','10') "
            End If


            If strLThue = "NDTQ" Then
                strSQL &= " and  ma_lthue <> '04' and trang_thai='01' "
            ElseIf strLThue = "HQTQ" Then
                strSQL &= " and  ma_lthue = '04' and trang_thai='01' "
            End If
            If strLoaiGD = "TOTAL" Then
            ElseIf strLoaiGD = "TOTAL_SP" Then
                strSQL &= " and TT_SP <> '0'"
            ElseIf strLoaiGD = "TOTAL_SPLOI" Then
                strSQL &= " and TT_SP = '1'"
            ElseIf strLoaiGD = "TOTAL_SPTC" Then
                strSQL &= " and TT_SP = '2'"
            ElseIf strLoaiGD = "TOTAL_CITAD" Then
                strSQL &= " and TT_SP = '0'"
            End If

            dt = DatabaseHelp.ExecuteToTable(strSQL)

            If Not dt Is Nothing And dt.Rows.Count > 0 Then
                Return dt.Rows(0)(0).ToString
            End If
        Catch ex As Exception

        End Try

        Return "0"

    End Function
    Protected Function getDuLieuNHKhac(ByVal strLoaiGD As String) As String
        Dim dt As New DataTable
        Dim strSQL As String = ""
        Try
            strSQL &= "Select count(*) as total from"
            strSQL &= " tcs_ctu_songphuong_hdr where"
            strSQL &= " tg_ky >= to_date('" & txtTuNgay.Value.Trim & " 00:00:00','dd/MM/yyyy HH24:MI:SS') and tg_ky <= to_date('" & txtDenNgay.Value.Trim() & " 23:59:59','dd/MM/yyyy HH24:MI:SS') "
            strSQL &= " and trang_thai ='03' "
            If strLoaiGD = "TOTAL" Then
            ElseIf strLoaiGD = "TOTAL_TC" Then
                strSQL &= " and TT_SP <> '0'"
            ElseIf strLoaiGD = "TOTAL_LOI" Then
                strSQL &= " and TT_SP = '0'"
            End If

            dt = DatabaseHelp.ExecuteToTable(strSQL)

            If Not dt Is Nothing And dt.Rows.Count > 0 Then
                Return dt.Rows(0)(0).ToString
            End If
        Catch ex As Exception

        End Try

        Return "0"

    End Function
End Class
