﻿Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports Business.BaoCao
Imports System.Data
Imports Business
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security

Partial Class pages_BaoCao_frmBCDoiChieuHoiSo
    Inherits System.Web.UI.Page
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private mv_strNgay As String

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenDiemThu As String
    Private mv_strTenKB As String
    Private mv_strLoaiBaoCao As String
    Private strMaNT As String

    Private ds As DataSet
    Private frm As infBaoCao
    Private somon As String
    Private str_DateDC_FromDate As String
    Private str_DateDC_ToDate As String
    Private str_FromDate As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session.Item("User") Is Nothing Then
            clsCTU.TCS_SetupParams(CType(Session("User"), Integer), mv_strSHKB, mv_strKyHieuCT, _
                                   mv_strMaDiemThu, mv_strTenDiemThu, mv_strDBThu, mv_strMaNHKB, mv_strTenKB)

        End If
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                'getChiNhanh()
                DM_DiemThu()
                txtNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                clsCommon.addPopUpCalendarToImage(btnCalendar1, txtNgay, "dd/mm/yyyy")
                clsCommon.get_NguyenTe(cboNguyenTe)
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenCNNH(ByVal pv_strMaCN As String) As String

        If Not pv_strMaCN Is Nothing Then
            Return clsCTU.TCS_GetMaCN_NH(pv_strMaCN.Substring(0, 3))
        Else
            Return ""
        End If

    End Function
    Private Function checkHSC(ByVal strMa_CN As String) As String
        Dim returnValue = ""
        returnValue = buBaoCao.checkHSC_MaCN(strMa_CN)
        Return returnValue
    End Function
    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Try
            'Dim strDonVi As String = cboDonVi.SelectedValue
            Dim strMaCN As String = txtMaCN.SelectedValue.ToString()
            strMaNT = cboNguyenTe.SelectedValue.ToString()
            Dim strMaCN_User = clsCTU.TCS_GetMaChiNhanh(Session("User").ToString)
            Dim strCheckHS As String = ""
            Dim d_DateDC_FromDate As Date = DateTime.ParseExact(txtNgay.Text.ToString, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat) ' Convert.ToDateTime(txtDenNgay.Text.ToString)
            str_FromDate = d_DateDC_FromDate.ToString("yyyy-MM-dd")
            d_DateDC_FromDate = d_DateDC_FromDate.AddDays(-1)
            str_DateDC_FromDate = d_DateDC_FromDate.ToString("yyyy-MM-dd")
            str_DateDC_ToDate = DateTime.ParseExact(txtNgay.Text.ToString, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat).ToString("yyyy-MM-dd") ' Convert.ToDateTime(txtDenNgay.Text.ToString)
            strCheckHS = checkHSC(strMaCN_User)
            If strMaCN = "" And strCheckHS <> "1" Then
                strMaCN = strMaCN_User & ";" & strCheckHS
            End If
            ds = New DataSet
            frm = New infBaoCao
            somon = "0"

            Select Case cboLoaiBaoCao.SelectedValue 'gLoaiBaoCao
                Case "0"
                    clsCommon.ShowMessageBox(Me, "Chưa chọn loại báo cáo!")
                    Exit Sub
             
                    'frm.SoMon_TrongGio = somon
                Case Report_Type.DOICHIEU_5.ToString
                    DOICHIEU_5(strMaCN)
              
                Case Report_Type.DOICHIEU_7.ToString
                    DOICHIEU_7(strMaCN)
                
            End Select

            If (mdlCommon.IsEmptyDataSet(ds)) Then
                'clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                'Return
            End If

            frm.TCS_DS = ds
            frm.TCS_Ngay_CT = txtNgay.Text.ToString()
            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao, "ManPower")

        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình lấy thông tin báo cáo!")
        Finally
            cmdIn.Enabled = True
        End Try
    End Sub


   

    Private Sub DOICHIEU_5(ByVal strMaCN As String)
        mv_strLoaiBaoCao = Report_Type.DOICHIEU_5
        Dim objCOn As Business_HQ.HaiQuan.HaiQuanController = New Business_HQ.HaiQuan.HaiQuanController()
        ds = objCOn.getBCCTietTThueXNKhau("M47", str_DateDC_FromDate, str_DateDC_ToDate, strMaCN, cboKenh.SelectedValue)
    End Sub



    Private Sub DOICHIEU_7(ByVal strMaCN As String)
        mv_strLoaiBaoCao = Report_Type.DOICHIEU_7
        Dim objCOn As Business_HQ.HaiQuan.HaiQuanController = New Business_HQ.HaiQuan.HaiQuanController()
        ds = objCOn.getBCTHopCTTThueXNKhau("M47", "851", str_DateDC_FromDate, str_DateDC_ToDate, strMaCN, strMaNT, cboKenh.SelectedValue)
    End Sub

 
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function
    Private Sub DM_DiemThu()

        Dim dsDiemThu As DataSet
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        If strcheckHSC = "1" Then
            'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
            ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
            strSQL = "SELECT a.id, a.id ||'-'||a.name nam FROM tcs_dm_chinhanh a  "

        Else

            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        End If
        Try
            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(txtMaCN, dsDiemThu, 1, 0, "Tất cả")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) 'clsCommon.WriteLog(ex, "Có lỗi trong quá trình lẫy thông tin chi nhánh", Session.Item("TEN_DN")))
            Throw ex
        Finally

        End Try



    End Sub
    'Private Sub getChiNhanh()
    '    Dim dt As New DataTable
    '    Dim strSQL As String
    '    Dim strMaCN As String = ""
    '    Dim strTenCN As String = ""
    '    Dim strUser As String = Session.Item("User").ToString

    '    Try
    '        strSQL = "select a.ma_cn, b.name ten_cn from tcs_dm_nhanvien a, tcs_dm_chinhanh b where (1=1) " & _
    '                "AND a.ma_cn = b. id " & _
    '                "AND ma_nv = " & CInt(strUser)
    '        '"AND MA_NHOM = '02'"

    '        dt = DatabaseHelp.ExecuteToTable(strSQL)

    '        If Not dt Is Nothing And dt.Rows.Count > 0 Then
    '            strMaCN = dt.Rows(0)("MA_CN").ToString
    '            strTenCN = dt.Rows(0)("TEN_CN").ToString
    '        End If
    '        txtMaCN.Text = strMaCN
    '        txtTenCN.Text = strTenCN

    '        If strMaCN.Trim.Substring(0, 3) <> "011" Then
    '            txtMaCN.Enabled = False
    '            Img1.Visible = False
    '            If strMaCN.Trim.Substring(3, 2) = "00" Then
    '                cboDonVi.Items.RemoveAt(0)
    '            Else
    '                cboDonVi.Items.RemoveAt(0)
    '                cboDonVi.Items.RemoveAt(0)
    '                cboDonVi.Enabled = False
    '            End If
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub
End Class
