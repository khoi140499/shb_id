﻿<%@ Page Language="C#" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="true"
    CodeFile="frmBCThuPhiBoNganh.aspx.cs" Inherits="pages_BaoCao_frmBCThuPhiBoNganh"
    Title="Báo cáo thu phí bộ ngành" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">

    <script type="text/javascript" src="../../javascript/google/js/jquery-1.8.0.min.js"></script>

    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" media="screen" href="../../javascript/date/datePicker.css" />

    <script src="../../javascript/date/date.js" type="text/javascript"></script>

    <script src="../../javascript/date/datePicker.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popcalendar.js" type="text/javascript"></script>
    <script src="../../javascript/accounting.min.js" type="text/javascript"></script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">In Báo Cáo Thu Phí Bộ Ngành</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <span id="labelMessage"></span>
            </td>
        </tr>
        <tr>
            <td width="80%" id="tdTextSearch" align="center" valign="top" class='nav'>
                <div id="divTracuu">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="left" valign="top">
                                <table width="100%" border="0" cellspacing="1" cellpadding="2" class="form_input">
                                    <tr>
                                        <td style="width: 15%" align="left" valign="top" class="form_label">
                                            Từ ngày
                                        </td>
                                        <td align="left" valign="top" class="form_control" style="width: 30%">
                                            <span style="display: block; width: 80%; padding-top: 2px; position: relative">
                                                <input type="text" id="textBoxSearchTuNgayCT" class="inputflat date-pick dp-applied validate[required]"
                                                    style="width: 98%; background: Aqua" onkeyup="javascript:return mask(this.value,this,'2,5','/');"
                                                    onblur="CheckDate(this);" onfocus="this.select()" />
                                            </span>
                                        </td>
                                        <td align="left" style="width: 20%" valign="top" class="form_label">
                                            Đến ngày
                                        </td>
                                        <td align="left" valign="top" class="form_control" style="width: 30%">
                                            <span style="display: block; width: 80%; padding-top: 2px; position: relative">
                                                <input type="text" id="textBoxSearchDenNgayCT" class="inputflat date-pick dp-applied validate[required]"
                                                    style="width: 98%; background: Aqua" onkeyup="javascript:return mask(this.value,this,'2,5','/');"
                                                    onblur="CheckDate(this);" onfocus="this.select()" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%" align="left" valign="top" class="form_label">
                                            <b>MST</b>
                                        </td>
                                        <td style="width: 30%;" class="form_control">
                                            <span style="display: block; width: 80%; padding-top: 2px; position: relative">
                                                <input type="text" id="textBoxSearchMST" class="inputflat" style="width: 98%;" />
                                            </span>
                                        </td>
                                        <td style="width: 15%" align="left" valign="top" class="form_label">
                                            <b>Số chứng từ</b>
                                        </td>
                                        <td style="width: 30%;" class="form_control">
                                            <span style="display: block; width: 80%; padding-top: 2px; position: relative">
                                                <input type="text" id="textBoxSearchSoHoSo" class="inputflat" style="width: 98%;" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; height: 18px;" align="left" valign="top" class="form_label">
                                            Năm chứng từ
                                        </td>
                                        <td style="width: 15%; height: 18px;" align="left" valign="top" class="form_control">
                                            <span style="display: block; width: 80%; padding-top: 2px; position: relative">
                                                <input type="text" id="textBoxSearchNam_CT" class="inputflat" style="width: 98%;" />
                                            </span>
                                        </td>
                                        <td style="width: 15%; height: 18px;" align="left" valign="top" class="form_label">
                                            Trạng thái chứng từ
                                        </td>
                                        <td align="left" valign="top" class="form_control" style="height: 20px">
                                            <select id="ddlTrangThaiCT" class="inputflat" style="width: 80%; height: 18px">                                                
                                                <option value="-1">Tất cả </option>
                                                <option value="05">Chờ kiểm soát </option>
                                                <option value="01">Đã kiểm soát </option>
                                                <option value="03">Chuyển trả</option>
                                                <option value="04">CT bị Hủy</option>
                                                <option value="06">Chuyển thuế/HQ lỗi</option>                                           
                                                
                                            </select>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; height: 18px;" align="left" valign="top" class="form_label">
                                            Mã chi nhánh
                                        </td>
                                        <td align="left" valign="top" class="form_control" style="height: 20px">
                                            <select id="ddlMaChiNhanh" class="inputflat" style="width: 80%; height: 18px">
                                                
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center">
                                            <input type="button" id="btnTruyVanKetQua" value="In Phí Bộ Ngành" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>

    <script type="text/javascript">
    
        $(document).ready(function(){
            getDanhMucChiNhanh();
        });
        function getDanhMucChiNhanh()
        {
             $.ajax({
                type: "POST",
                url: "frmBCThuPhiBoNganh.aspx/getDanhMucChiNhanh",                
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(data) {
                         var modelGen=JSON.parse(data.d);    
                         
                         $("#ddlMaChiNhanh").append($('<option>').val(modelGen.RootID).text("Tất cả"));
                         var listChiNhanh = modelGen.ListChiNhanh;
                         for(var i =0;i<listChiNhanh.length;i++)
                         {
                            $("#ddlMaChiNhanh").append($('<option>').val(listChiNhanh[i].ID).text(listChiNhanh[i].NAME));
                         }
                         
                   }                         
            });
        }

        function getStringText(id)
        {
            return $("#"+ id).val();
        }
  
        
        $("#btnTruyVanKetQua").click(function(){
             var root=0;
            if($("#ddlMaChiNhanh option:selected").index()==1)
            {
                root=1;
            }
            var InBaoCao = {
            SOHOSO: getStringText("textBoxSearchSoHoSo"),
            TUNGAY: getStringText("textBoxSearchTuNgayCT"),
            DENNGAY:getStringText("textBoxSearchDenNgayCT"),
            MST: getStringText("textBoxSearchMST"),
            NAM_CT:getStringText("textBoxSearchNam_CT"),
            TRANGTHAI_CT: $("#ddlTrangThaiCT option:selected").val(),
            MA_CN: $("#ddlMaChiNhanh option:selected").val(),
            ROOT :root
            };     
            var url= "../XLCuoiNgay/frmBaoCaoPhiBoNganh.aspx?SOHOSO="+ InBaoCao.SOHOSO +"&TUNGAY="+ InBaoCao.TUNGAY +"&DENNGAY="+ InBaoCao.DENNGAY +"&MST="+ InBaoCao.MST+"&NAM_CT="+ InBaoCao.NAM_CT +"&TRANGTHAI_CT="+ InBaoCao.TRANGTHAI_CT +"&MA_CN="+ InBaoCao.MA_CN;
            window.open(url, 'winname','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes');
      
        });
        
    </script>

    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
    </script>

    <script type="text/javascript">
    
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
          
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            if(dd<10) {
                dd='0'+dd
            } 

            if(mm<10) {
                mm='0'+mm
            } 

            today = dd+'/'+mm+'/'+yyyy;
            
            $("#textBoxSearchTuNgayCT").val(today);
            $("#textBoxSearchDenNgayCT").val(today);
        });
    </script>

</asp:Content>
