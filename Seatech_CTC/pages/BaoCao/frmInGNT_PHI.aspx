﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmInGNT_PHI.aspx.vb" ClientTarget="uplevel" Inherits="pages_BaoCao_frmInGNT_PHI" %>
<% Response.charset="utf-8" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Hệ thống thu thuế tập trung</title>
    <style>
    ::-webkit-scrollbar {
    width: 12px;
}
::-webkit-scrollbar-track {
    background-color: #eaeaea;
    border-left: 1px solid #ccc;
}
::-webkit-scrollbar-thumb {
    background-color: #ccc;
}
::-webkit-scrollbar-thumb:hover {
    background-color: #aaa;
}

    </style>
     <script lang="javaScript" type="text/javascript" src="../../javascript/crystalreportviewers13/js/crviewer/crv.js"></script> 
</head>
<body style="overflow: scroll;min-height:400px;">
    <form id="form1" runat="server">
    <div >
    <br />
        <CR:CrystalReportViewer ID="rpvMain" runat="server" AutoDataBind="true" ToolPanelView="None" HasToggleGroupTreeButton="False" PrintMode="ActiveX"/>  
        
        </div>
    </form>
</body>
</html>
