﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false" CodeFile="frmBCDoiChieu.aspx.vb" 
Inherits="pages_BaoCao_frmBCDoiChieu" title="Báo cáo đối chiếu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/popcalendar.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
//    function ShowLov(strType)
//    {        
//        if (strType=="CNNganHang") return FindDanhMuc('CNNganHang', '<%=txtMaCN.ClientID %>', '<%=txtTenCN.ClientID %>', '<%=txtNgay.ClientID %>');
//                          
//    }
    function FindDanhMuc(strPage,txtID, txtTitle,txtFocus) 
    {
        returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&initParam=" + $get(txtID).value,"", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
        if (returnValue != null) {
            document.getElementById(txtID).value = returnValue.ID;
            if (txtTitle!=null){
                document.getElementById(txtTitle).value = returnValue.Title;                    
            }
            if (txtFocus != null) {
                //if ($get('txtFocus').getAttribute('disabled') != 'disabled') {
                    try {
                        document.getElementById(txtFocus).focus();
                    }
                catch (e) {
                        document.getElementById(txtFocus).focus();
                    };
                //}
            }
        }
        //NextFocus();
    }
//    $(document).ready(function(){
////            $("#<%=txtMaCN.ClientID %>").keypress(function(){
////                if (event.keyCode==13){ShowLov('CNNganHang');}
////            }); 
//    /************************KEY_BLUR*************************************/
//        $("#<%=txtMaCN.clientID%>").blur(function () {
//            if($("#<%=txtMaCN.clientID%>").val().length == 3 && $("#<%=txtMaCN.clientID%>").val().length != 0)
//            {
//                $("#<%=txtMaCN.clientID%>").val($("#<%=txtMaCN.clientID%>").val() + '00');
//            }
//            if($("#<%=txtMaCN.clientID%>").val().length < 3){
//                $("#<%=txtTenCN.clientID%>").val("");
//            } else {
//                Get_TenCN();
//            }
//        });   
//    });
//    
//    /****************************TEN_CQTHU*********************************/
//    function Get_TenCN() {
//        PageMethods.Get_TenCNNH($get('<%=txtMaCN.clientID%>').value, Get_TenCNNH_Complete, Get_TenCNNH_Error);
//    }

//    function Get_TenCNNH_Complete(result, methodName) {
//        if (result.length > 0) {
//            $get('<%=txtTenCN.clientID%>').value = result;
//        }
//        else {
//            $get('<%=txtTenCN.clientID%>').value = result;
//        }
//    }
//    function Get_TenCNNH_Error(error, userContext, methodName) {
//        if (error !== null) {
//            $get('<%=txtTenCN.clientID%>').value = result;
//        }
//    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" Runat="Server">
  <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">BÁO CÁO ĐỐI CHIẾU</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" align="right">
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td>
                            <table id="Table2" cellpadding="2" cellspacing="1" class="form_input" width="100%">
                                <tr align="left">
                                    <td  class="form_label">
                                        <asp:Label ID="Label5" runat="server" Text="Nguyên Tệ" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:DropDownList ID="cboNguyenTe" Width="150px" runat="server" CssClass="inputflat">
                                            <%--<asp:ListItem Text="Tất cả" Value="1" Selected="True"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                    <td  class="form_label">
                                       <asp:Label ID="lblDV" runat="server" Text="Đơn vị" CssClass="label" style="display:none;"></asp:Label>&nbsp; 
                                    </td>
                                    <td class="form_control">                                        
                                        <asp:DropDownList ID="cboDonVi" Width="145px" runat="server" CssClass="inputflat" style="display:none;">                                            
                                            <asp:ListItem Text="Ngân Hàng" Value="1" ></asp:ListItem>
                                            <asp:ListItem Text="Chi nhánh" Value="2" ></asp:ListItem>
                                            <asp:ListItem Text="Phòng giao dịch" Value="3" ></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                   
                               </tr>
                               <tr align="left">
                                    <td  class="form_label">
                                        <asp:Label ID="lblChiNhanh" runat="server" Text="Chi nhánh" CssClass="label"></asp:Label>
                                    </td>
                                    <td class="form_control" colspan="3">
                                        <asp:DropDownList ID="txtMaCN"  runat="server" CssClass="inputflat"
                                            AutoPostBack="true" style="width: 95%; border-color: White; font-weight: bold; text-align: left;">
                                        </asp:DropDownList>
                                        <%--<asp:TextBox ID="txtMaCN" runat="server" CssClass="inputflat"
                                            style="width: 100%; border-color: White; font-weight: bold; text-align: left;"></asp:TextBox>--%>
                                            <asp:TextBox ID="txtTenCN" runat="server" CssClass="inputflat" Enabled ="false"
                                            
                                            style="width: 5px; border-color: White; font-weight: bold; text-align: left; display:none;" 
                                            Width="380px"></asp:TextBox>
                                            <%--<img src="../../images/search.jpeg" width="14" id="Img1" runat="server" height="14" onclick="ShowLov('CNNganHang')" />--%>
                                    </td>                                    
                                    
                               </tr>                               
                               <tr align="left">
                                    <td class="form_label" width='15%'>
                                        <asp:Label ID="Label10" runat="server" Text="Ngày" CssClass="label"></asp:Label>
                                    </td>
                                    <td width='25%' class="form_control">
                                        <asp:TextBox runat="server" ID="txtNgay" Width="120px" CssClass="inputflat" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                            onblur="CheckDate(this);" MaxLength="10"></asp:TextBox>
                                        <asp:Image ID="btnCalendar1" imagealign="AbsMiddle" runat="server" ImageUrl="~/images/cal.gif"  TabIndex="20"/> 
                                    </td>
                                    <td width='15%' class="form_label">
                                        <asp:Label ID="Label4" runat="server" Text="Loại báo cáo" CssClass="label"></asp:Label>
                                    </td>
                                    <td width='45%' class="form_control">
                                        <asp:DropDownList ID="cboLoaiBaoCao" Width="250px" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Chọn loại báo cáo" Value="0" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="01-Tổng hợp thu thuế XNK" Value="DOICHIEU_7" ></asp:ListItem>
                                            <asp:ListItem Text="02-Tổng hợp hủy thu thuế XNK" Value="DOICHIEU_1"></asp:ListItem>
                                            <asp:ListItem Text="03-Chi tiết thu thuế XNK" Value="DOICHIEU_5"></asp:ListItem>
                                            <asp:ListItem Text="04-Chi tiết hủy chứng từ thu thuế XNK" Value="DOICHIEU_4"></asp:ListItem>                                           
                                        </asp:DropDownList>
                                    </td> 
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="middle" align="center" style="height: 40px">
                <asp:Button ID="cmdIn" runat="server" CssClass="ButtonCommand" Text="[In BC]"></asp:Button>&nbsp;&nbsp;
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>

