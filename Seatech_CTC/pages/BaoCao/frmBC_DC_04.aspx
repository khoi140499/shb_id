﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmBC_DC_04.aspx.vb" Inherits="pages_BaoCao_frmBC_DC_04" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Báo cáo đối chiếu 04</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width:100%;">
    <p><asp:Button ID="btnKetXuat" runat="server" Text="Kết xuất"/></p>
        <table width="100%" id="tableBaoCao" runat="server">
            <tr>
                <td align="center" colspan="2"><b style="text-decoration: underline;" >TỔNG CỤC THUẾ</b></td>
                <td align="center" colspan="11"><b>CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</b></td>
            </tr>
            <tr>
                <td colspan="2"></td>
                <td align="center" colspan="11"><b style="text-decoration: underline;">Độc lập - Tự do - Hạnh phúc</b></td>
            </tr>
            <tr>
                <td align="center" colspan="2">Số: 04/BC-NTĐT</td>
                <td align="center" colspan="11"><b><asp:Label ID="lblNgayDC" runat="server"></asp:Label></b></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td align="center" colspan="11"><b>BÁO CÁO CHI TIẾT GIAO DỊCH TRUYỀN NHẬN THÀNH CÔNG</b></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td align="center" colspan="11">Thời gian đối chiếu: từ <asp:Label ID="lblTuNgay" runat="server"></asp:Label> đến <asp:Label ID="lblDenNgay" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td align="center" colspan="11">Thời điểm đối chiếu: <asp:Label ID="lblGioDC" runat="server"></asp:Label> </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td align="center" colspan="11">Cơ quan thuế: Toàn quốc</td>
            </tr>
            <tr>
                <td colspan="13">
                   &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="13">
                    <div id="ExportTable" runat="server" >
                    </div>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
