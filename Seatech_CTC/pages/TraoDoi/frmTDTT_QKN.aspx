﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmTDTT_QKN.aspx.vb" Inherits="pages_TraoDoi_frmTDTT_QKN" Title="Theo dõi quá trình tác nghiệp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">THEO DÕI QUÁ TRÌNH VÀO RA TÁC NGHIỆP</asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" align="left">
                <table id="Table2" width="100%" border="0" cellpadding="0" cellspacing="1" class="form_input">
                    <tr>
                        <td valign="top">
                            <table width="100%">
                                <tr>
                                    <td class="form_label">
                                        <asp:Label ID="lblTuNgay" runat="server" CssClass="label">Từ ngày</asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox runat="server" ID="txtTuNgay" Width="100px" CssClass="inputflat"></asp:TextBox>
                                    </td>
                                    <td class="form_label">
                                        <asp:Label ID="lblDenNgay" runat="server" CssClass="label">Đến ngày</asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox runat="server" ID="txtDenNgay" Width="100px" CssClass="inputflat"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td class="form_label">
                                        <asp:Label ID="lblNguoiTH" runat="server" CssClass="label">Người thực hiện</asp:Label>
                                    </td>
                                    <td class="form_control" colspan="3">
                                        <asp:DropDownList ID="cboNguoiTH" Width="150px" runat="server" CssClass="inputflat">
                                            <asp:ListItem Text="Tất cả" Value="0" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td class="form_label">
                                        <asp:Label ID="Label4" runat="server" CssClass="label">Trạng thái</asp:Label>
                                    </td>
                                    <td class="form_control" colspan="3" height="100px">
                                        <asp:CheckBoxList ID="chkTrangThai" runat="server" CssClass="inputflat2" Width="100%">
                                            <asp:ListItem Text="Lỗi" Value="0" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Không lỗi" Value="1" Selected="True"></asp:ListItem>
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table width="100%">
                                <tr align="left">
                                    <td class="form_label" valign="top">
                                        <asp:Label ID="Label2" runat="server" CssClass="label">Loại dữ liệu</asp:Label>
                                    </td>
                                    <td width='370' class="form_control" colspan="3">
                                        <div style="vertical-align top; height: 145px; overflow: auto; width: 100%;">
                                            <asp:CheckBoxList ID="chkLoaiDL" runat="server" CssClass="inputflat2" Width="100%">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="middle" align="center" style="height: 40px">
                <asp:Button ID="cmdInBK" runat="server" CssClass="ButtonCommand" Text="[Tra cứu]">
                </asp:Button>&nbsp;
                <asp:Button ID="cmdExit" runat="server" CssClass="ButtonCommand" Text="[Thoát]">
                </asp:Button>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:DataGrid ID="grdDetail" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" AllowPaging="True" BorderColor="#999999" PageSize="10">
                    <AlternatingItemStyle BackColor="#F6F6F7" Font-Bold="False" Font-Italic="False" Font-Names="Tahoma"
                        Font-Overline="False" Font-Size="X-Small" Font-Strikeout="False" Font-Underline="False"
                        ForeColor="Black" VerticalAlign="Middle"></AlternatingItemStyle>
                    <HeaderStyle Font-Size="X-Small" BackColor="#E4E5D7" Font-Bold="True" Font-Italic="False"
                        Font-Names="Tahoma" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
                        ForeColor="#993333" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="TSN_NAME" HeaderText="Loại dữ liệu">
                            <HeaderStyle Font-Size="8pt" Font-Names="Arial" Font-Bold="True" HorizontalAlign="Center"
                                Width="150px"></HeaderStyle>
                            <ItemStyle Font-Size="8pt" Font-Names="Arial"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TSN_NAME" HeaderText="Kiểu TĐ">
                            <HeaderStyle Font-Size="8pt" Font-Names="Arial" Font-Bold="True" HorizontalAlign="Center"
                                Width="100px"></HeaderStyle>
                            <ItemStyle Font-Size="8pt" Font-Names="Arial"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="CREA_DATE" HeaderText="Ngày truyền">
                            <HeaderStyle Font-Size="8pt" Font-Names="Arial" Font-Bold="True" HorizontalAlign="Center"
                                Width="100px"></HeaderStyle>
                            <ItemStyle Font-Size="8pt" Font-Names="Arial" HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TRAN_NUM" HeaderText="Số BG TĐ">
                            <HeaderStyle Font-Size="8pt" Font-Names="Arial" Font-Bold="True" HorizontalAlign="Center"
                                Width="80px"></HeaderStyle>
                            <ItemStyle Font-Size="8pt" Font-Names="Arial" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ERR_NUM" HeaderText="Người thực hiện">
                            <HeaderStyle Font-Size="8pt" Font-Names="Arial" Font-Bold="True" HorizontalAlign="Center"
                                Width="300px"></HeaderStyle>
                            <ItemStyle Font-Size="8pt" Font-Names="Arial" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages"></PagerStyle>
                    <FooterStyle BackColor="#D3D3D7" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                        Font-Size="X-Small" Font-Strikeout="False" Font-Underline="False" />
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>
