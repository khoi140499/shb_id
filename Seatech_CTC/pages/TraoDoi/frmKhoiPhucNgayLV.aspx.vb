﻿Imports Business.Common.mdlSystemVariables
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports System.Data

Partial Class pages_TraoDoi_frmKhoiPhucNgayLV
    Inherits System.Web.UI.Page
    Private dtmNgayKS As Date

    Private Sub cmdKhoiPhuc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKhoiPhuc.Click
        'If TCS_KT_DangNhap(Session.Item("User").ToString) <> 1 Then
        '    clsCommon.ShowMessageBox(Me, "                      Bàn thu khác còn đang làm việc...                   " & Chr(13) & _
        '           "Bạn phải nhắc tất cả các bàn thu khác tạm thời thoát ra khỏi chương trình." & Chr(13) & _
        '           "                       Và thực hiện lại chức năng này!                    ")
        '    Exit Sub
        'End If
        ' If clsCommon.ShowMessageBox(Me, "Bạn có muốn khôi phục ngày khóa sổ?") Then
        If KhoiphucKS(dtmNgayKS.ToString("yyyyMMdd")) Then
            clsCommon.ShowMessageBox(Me, "Khôi phục ngày khóa sổ thành công.")

        Else
            clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình khôi phục ngày khoá sổ !")

        End If
        'End If
    End Sub
    Private Sub frmKhoiPhucNgayLV_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim cnKP As DataAccess
        Dim drKP As IDataReader
        Dim lngNgayKS As String
        Dim strSql As String
        Try
            cnKP = New DataAccess
            strSql = "Select Ngay_KS From TCS_KHOASO"
            drKP = cnKP.ExecuteDataReader(strSql, CommandType.Text)
            If drKP.Read() Then
                lngNgayKS = drKP.GetValue(0).ToString()
            End If
        Catch ex As Exception
            'LogDebug.Writelog("Lỗi trong quá trình lấy ngày khôi phục: " & ex.ToString)
            clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình lấy ngày khôi phục.")

        Finally
            If Not drKP Is Nothing Then
                drKP.Close()
            End If
            If Not cnKP Is Nothing Then
                cnKP.Dispose()
            End If
        End Try
        dtmNgayKS = ConvertNumberToDate(lngNgayKS)
        lblNgayKP.Text = dtmNgayKS.ToString("dd/MM/yyyy")
    End Sub
    Public Function KhoiphucKS(ByVal strNgayLV As String) As Boolean
        Dim strSql As String
        Dim cnKS As DataAccess
        Dim iTransKS As IDbTransaction
        Try
            cnKS = New DataAccess
            iTransKS = cnKS.BeginTransaction()
            ' Bắt đầu khoá sổ
            strSql = "Update TCS_KHOASO " & _
                     "Set NGAY_LV = " & strNgayLV
            cnKS.ExecuteNonQuery(strSql, CommandType.Text, iTransKS)
            ' Đưa vào bảng TCS_ThamSo
            strSql = "Update TCS_THAMSO " & _
                     "Set GIATRI_TS='" & ConvertNumbertoString(strNgayLV) & "' " & _
                     " Where TEN_TS='NGAY_LV'"
            cnKS.ExecuteNonQuery(strSql, CommandType.Text, iTransKS)
            iTransKS.Commit()
            '  gdtmNgayLV = CLng(strNgayLV)
            Return True
        Catch ex As Exception
            iTransKS.Rollback()
            'LogDebug.Writelog("Lỗi trong quá trình khôi phục ngày khoá sổ! - " & ex.ToString)
            Return False
        Finally
            If Not iTransKS Is Nothing Then
                iTransKS.Dispose()
            End If
            If Not cnKS Is Nothing Then
                cnKS.Dispose()
            End If
        End Try
    End Function
End Class
