﻿Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports Business.TraoDoi
Imports System.Data

Partial Class pages_TraoDoi_frmKhoaSo
    Inherits System.Web.UI.Page

    Private Sub cmdKhoaSo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKhoaSo.Click
        Try
            Dim strNgayKX As Long
            Dim strMaDThu As String
            strNgayKX = ConvertDateToNumber(txtNgayLV.Text.Trim)
            strMaDThu = clsCTU.TCS_GetMaDT(CType(Session("User"), Integer))
            'buTraoDoi.ExportData(strNgayKX)
            'buKhoaSo.KhoaSo_DThu(strMaDThu)
            'clsCommon.ShowMessageBox(Me, "Khóa sổ thành công!")
        Catch ex As Exception
            Throw ex
            clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình kết xuất dữ liệu !")
        End Try
    End Sub

    Private Sub frmKhoaSo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                txtNgayLV.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình khởi tạo form khoá sổ.")
            End Try
            cmdKhoaSo.Attributes.Add("onClick", "return confirm('Bạn có muốn khóa sổ ngày làm việc hiện tại?')")
        End If
    End Sub

    Protected Sub cmdExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        Response.Redirect("../frmHomeNV.aspx", False)
    End Sub
End Class
