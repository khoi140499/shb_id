﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false"
    CodeFile="frmKhoaSo.aspx.vb" Inherits="pages_TraoDoi_frmKhoaSo" Title="Khóa sổ cuối ngày" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" runat="Server">
    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">KHÓA SỔ CUỐI NGÀY</asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td valign="top" align="left">
                <table border='0' cellpadding='1' cellspacing='2' width='100%' class="form_input">
                    <tr>
                        <td class="form_label" width='170'>
                            <asp:Label ID="lblNgayLV" runat="server" CssClass="label">Ngày LV hiện tại</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox runat="server" ID="txtNgayLV" Width="100px" class='inputflat'></asp:TextBox>
                        </td>
                    </tr>
                   
                </table>
                <br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="cmdKhoaSo" runat="server" Text="Khóa sổ" CssClass="ButtonCommand">
                </asp:Button>&nbsp;<asp:Button ID="cmdExit" runat="server" Text="Thoát" CssClass="ButtonCommand">
                </asp:Button>
            </td>
        </tr>
    </table>
</asp:Content>
