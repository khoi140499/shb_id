﻿Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports Business.TraoDoi
Imports System.Data
Imports Business.NewChungTu
Imports Business.Common

Partial Class pages_TraoDoi_frmKetXuatCT
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.txtNgayKX.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub
   
    Protected Sub cmdKetXuat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdKetXuat.Click
        'Try
        '    Dim strNgayKX As String
        '    strNgayKX = ConvertDateToNumber(txtNgayKX.Text.Trim)
        '    buTraoDoi.ExportData(strNgayKX)
        '    clsCommon.ShowMessageBox(Me, "Đã kết thúc quá trình kết xuất dữ liệu chứng từ GNT !")
        'Catch ex As Exception
        '    Throw ex
        '    clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình kết xuất dữ liệu !")
        'End Try
    End Sub

End Class
