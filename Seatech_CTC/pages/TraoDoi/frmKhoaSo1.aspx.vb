﻿Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports Business.TraoDoi
Imports System.Data

Partial Class pages_TraoDoi_frmKhoaSo1
    Inherits System.Web.UI.Page

    Private Sub cmdKhoaSo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKhoaSo.Click
        Try
            If buTraoDoi.GetTT_FLAG() = "1" Then
                clsCommon.ShowMessageBox(Me, " Truyền tin đang làm việc..." & Chr(13) & _
                        "Đợi truyền tin kết thúc làm việc, sau đó bạn hãy thực hiện lại chức năng này !")
                Exit Sub
            End If

            Dim dtNgayLVNext As Date
            Dim dtNgayDC As Date
            Dim ds As DataSet
            Dim v_strMa_DThu As String = ""
            Dim iCount As Integer

            ' Dang ky lam viec voi CSDL trung gian
            buTraoDoi.SetTN_FLAG(False)

            ' Kiểm tra xem còn dữ liệu chưa kiểm soát
            ds = buKhoaSo.getChungTu("00", Session.Item("User").ToString)
            If Not IsEmptyDataSet(ds) Then
                clsCommon.ShowMessageBox(Me, "Đang còn chứng từ chưa kiểm soát. Bạn hãy thực hiện kiểm  soát chứng từ trước khi khóa sổ")
                ' Bỏ đăng ký làm việc với truyền tin
                buTraoDoi.SetTN_FLAG(True)
                Return
            End If
            For i = 0 To chkBanThu.Items.Count - 1
                If chkBanThu.Items(i).Selected Then
                    v_strMa_DThu &= Trim(chkBanThu.Items(i).Value) & ","
                    iCount += 1
                End If
            Next
            v_strMa_DThu = v_strMa_DThu.Remove(v_strMa_DThu.Length - 1, 1)
            If iCount = 0 Then
                clsCommon.ShowMessageBox(Me, "Phải chọn ít nhất một Điểm thu!")
                Return
            End If
            ds = buKhoaSo.getChungTu("01", Session.Item("User").ToString)
            If IsEmptyDataSet(ds) Then
                'If (Me.hdnbox.Value = "No") Then
                '    buTraoDoi.SetTN_FLAG(True)
                '    Exit Sub
                'End If
            End If

            If Valid_NgayLVNext(txtNgayLVTiep.Text) Then
                dtNgayLVNext = ConvertNumberToDate(ConvertDateToNumber(txtNgayLVTiep.Text))
            Else
                ' Bo dang ky lam viec voi CSDL trung gian. 
                buTraoDoi.SetTN_FLAG(True)
                Exit Sub
            End If


            dtNgayDC = dtNgayLVNext.AddMonths(-1)
            buKhoaSo.KhoaSo(dtNgayLVNext.ToString("yyyyMMdd"), clsCTU.TCS_GetNgayLV(Session.Item("User").ToString), dtNgayDC.ToString("yyyyMM") & "32", v_strMa_DThu)

            ' Neu la oracle thi ket xuat cho co quan thu
            'buKhoaSo.KX_CQThu(gdtmNgayLV)
            ' End If

            'gdtmNgayLV = CLng(dtNgayLVNext.ToString("yyyyMMdd"))
            gintSoBTCT = 1

            ' Bo dang ky lam viec voi CSDL trung gian
            buTraoDoi.SetTN_FLAG(True)

            clsCommon.ShowMessageBox(Me, "Khóa sổ thành công")
            ' Khoá sổ thành công

            'End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình khoá sổ cuối ngày !")
            buTraoDoi.SetTN_FLAG(True)

        Finally
            ' Bỏ trạng thái bận

        End Try
    End Sub

    Private Sub frmKhoaSo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Try
                Dim dtmNgayLV As Date
                txtNgayLV.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
                'txtNgayLVTiep.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))

                ' Lấy ngày làm việc tiếp theo
                dtmNgayLV = ConvertNumberToDate(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
                If dtmNgayLV.DayOfWeek = DayOfWeek.Saturday Then
                    ' Nếu là thứ 7 thì chuyển sang thứ 2 tuần sau
                    dtmNgayLV = dtmNgayLV.AddDays(2)
                Else
                    ' Không thì tăng lên một ngày
                    dtmNgayLV = dtmNgayLV.AddDays(1)
                End If
                txtNgayLVTiep.Text = dtmNgayLV.ToString("dd/MM/yyyy")
                txtNgayLVTiep.Focus()
                LoadDiemThu()
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình khởi tạo form khoá sổ.")
            End Try
            cmdKhoaSo.Attributes.Add("onClick", "return confirm('Bạn có muốn khóa sổ ngày làm việc hiện tại?')")
        End If
    End Sub

    Private Function Valid_NgayLVNext(ByVal strNgayLV As String) As Boolean
        Try
            Dim dtmNgayLV As Date
            If Valid_DateExact(strNgayLV) Then
                If ConvertDateToNumber(strNgayLV) <= clsCTU.TCS_GetNgayLV(Session.Item("User").ToString) Then
                    clsCommon.ShowMessageBox(Me, "Ngày làm việc mới phải lớn hơn ngày khóa sổ.")
                    txtNgayLV.Focus()
                    Return False
                End If
                dtmNgayLV = ConvertNumberToDate((ConvertDateToNumber(strNgayLV)))
                If dtmNgayLV.DayOfWeek = DayOfWeek.Sunday Then
                    clsCommon.ShowMessageBox(Me, "Ngày làm việc không vào chủ nhật.")

                    txtNgayLV.Focus()
                    Return False
                End If
            Else
                clsCommon.ShowMessageBox(Me, "Ngày làm việc không hợp lệ.")
                txtNgayLV.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub LoadDiemThu()
        Dim drDiemthu As IDataReader
        Dim strSQL As String
        Dim tmpstrMa_Dthu As String
        Dim tmpstrTen_Dthu As String
        chkBanThu.Items.Clear()
        Try
            strSQL = "select * from tcs_dm_diemthu order by ma_dthu"
            drDiemthu = DataAccess.ExecuteDataReader(strSQL, CommandType.Text)

            Do While drDiemthu.Read
                tmpstrMa_Dthu = drDiemthu.GetValue(0).ToString
                tmpstrTen_Dthu = drDiemthu.GetValue(1).ToString
                Dim It As ListItem
                It = New ListItem(tmpstrTen_Dthu, tmpstrMa_Dthu)
                chkBanThu.Items.Add(It)
            Loop
            drDiemthu.Close()

            For i = 0 To chkBanThu.Items.Count - 1
                chkBanThu.Items(i).Selected = True
            Next
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình tìm kiếm loại dữ liệu !")
        End Try
    End Sub

    Protected Sub cmdExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        Response.Redirect("../frmHomeNV.aspx", False)
    End Sub
End Class
