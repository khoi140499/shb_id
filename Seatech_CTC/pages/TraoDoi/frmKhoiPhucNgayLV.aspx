﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false"
    CodeFile="frmKhoiPhucNgayLV.aspx.vb" Inherits="pages_TraoDoi_frmKhoiPhucNgayLV"
    Title="Khôi phục ngày làm việc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" runat="Server">
    <table id="tblKPkhoaSo" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td align="center">
                <input id="hdnbox" type="hidden" value="New" name="Hidden1" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="pageTitle">
                <asp:Label ID="id1" runat="server">KHÔI PHỤC NGÀY LÀM VIỆC</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left" class="form_label">
                <asp:Label ID="lblError" Font-Bold="True" Font-Size="8pt" ForeColor="red" Font-Names="Arial"
                    runat="server" CssClass="label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table width="100%" class="form_input">
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblNewPassCaption" CssClass="label" runat="server">Ngày LV mới</asp:Label>
                        </td>
                        <td class="form_label">
                            <asp:Label ID="lblNgayKP" MaxLength="50" ssClass="label" runat="server" Width="250px"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="cmdKhoiPhuc" runat="server" Text="Khôi phục" CssClass="ButtonCommand">
                </asp:Button>&nbsp;&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
