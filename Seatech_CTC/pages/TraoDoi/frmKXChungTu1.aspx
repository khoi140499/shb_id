﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmKXChungTu1.aspx.vb" Inherits="pages_TraoDoi_frmKXChungTu1" Title="Kết xuất dữ liệu sang kho bạc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">KẾT XUẤT DỮ LIỆU SANG KHO BẠC</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left">
                <table border='0' cellpadding='0' cellspacing='0' width='100%' class="form_input">
                    <tr>
                        <td valign="top">
                            <table width='100%'>
                                <tr>
                                    <td class="form_label">
                                        <asp:Label ID="lblTuNgay" runat="server" CssClass="label">Ngày PS</asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox runat="server" ID="txtTuNgay" Width="100px" class='inputflat'></asp:TextBox>
                                    </td>
                                    <%--
                                    <td class="form_label">
                                        <asp:Label ID="lblDenNgay" runat="server" CssClass="label" Visible="false">Đến ngày</asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox runat="server" ID="txtDenNgay" Width="100px" Visible="false" class='inputflat'></asp:TextBox>
                                    </td>--%>
                                    <td class="form_control" colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="form_label">
                                        <asp:Label ID="Label2" runat="server" CssClass="label">Từ Số CT</asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox ID="txtTuSoCT" runat="server" Width='100px' class='inputflat' MaxLength='7' />
                                    </td>
                                    <td class="form_label">
                                        <asp:Label ID="Label3" runat="server" CssClass="label">Đến Số CT</asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox ID="txtDenSoCT" runat="server" Width='100px' class='inputflat' MaxLength='7' />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="form_label">
                                        <asp:Label ID="Label5" runat="server" CssClass="label">Cơ quan thu</asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox ID="txtCQThu" runat="server" Width='100px' class='inputflat' />
                                    </td>
                                    <td class="form_label" colspan="2" align="left">
                                        <asp:TextBox ID="txtTenCQThu" runat="server" Width='220px' class='inputflat' Enabled="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="form_label">
                                        <asp:Label ID="Label7" runat="server" CssClass="label">DBHC</asp:Label>
                                    </td>
                                    <td class="form_control">
                                        <asp:TextBox ID="txtMaDBHC" runat="server" Width='100px' class='inputflat' />
                                    </td>
                                    <td class="form_label" colspan="2" align="left">
                                        <asp:TextBox ID="txtTenDBHC" runat="server" Width='220px' class='inputflat' Enabled="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top">
                            <table width='100%'>
                                <tr valign="top">
                                    <td class="form_label">
                                        <asp:Label ID="Label4" runat="server" CssClass="label">Chi nhánh/Phòng GD</asp:Label>
                                    </td>
                                    <td width='300' align="left" valign="top" class="form_control">
                                        <div style="vertical-align top; height: 85px; overflow: auto; width: 100%;">
                                            <asp:CheckBoxList ID="chkBanThu" runat="server" CssClass="inputflat" Width="100%">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 10px;" align="center">
            </td>
        </tr>
        <tr>
            <td align="center">
                <table width="100%">
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="cmdTraCuu" runat="server" Text="Tra Cứu" CssClass="ButtonCommand" />&nbsp;<asp:Button
                                ID="cmdKetXuat" runat="server" Text="Kết xuất" CssClass="ButtonCommand" />&nbsp;<asp:Button
                                    ID="cmdIn" runat="server" Text="In bảng kê" CssClass="ButtonCommand" />&nbsp;<asp:Button
                                        ID="cmdThoat" runat="server" Text="Thoát" CssClass="ButtonCommand" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 25px;" align="center">
            </td>
        </tr>
        <tr>
            <td align="center" valign="top">
                <asp:DataGrid ID="grdMaster" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="97%" AllowPaging="True" BorderColor="#999999" PageSize="10">
                    <AlternatingItemStyle BackColor="#F6F6F7" Font-Bold="False" Font-Italic="False" Font-Names="Tahoma"
                        Font-Overline="False" Font-Size="X-Small" Font-Strikeout="False" Font-Underline="False"
                        ForeColor="Black" VerticalAlign="Middle"></AlternatingItemStyle>
                    <HeaderStyle Font-Size="X-Small" BackColor="#E4E5D7" Font-Bold="True" Font-Italic="False"
                        Font-Names="Tahoma" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"
                        ForeColor="#993333" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="KyHieu_CT" HeaderText="Ký hiệu CT">
                            <HeaderStyle Font-Size="8pt" Font-Names="Arial" Font-Bold="True" HorizontalAlign="Center"
                                Width="80px"></HeaderStyle>
                            <ItemStyle Font-Size="8pt" Font-Names="Arial" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SO_CT" HeaderText="Số CT">
                            <HeaderStyle Font-Size="8pt" Font-Names="Arial" Font-Bold="True" HorizontalAlign="Center"
                                Width="80px"></HeaderStyle>
                            <ItemStyle Font-Size="8pt" Font-Names="Arial" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_NNTHUE" HeaderText="Mã NNT">
                            <HeaderStyle Font-Size="8pt" Font-Names="Arial" Font-Bold="True" HorizontalAlign="Center"
                                Width="80px"></HeaderStyle>
                            <ItemStyle Font-Size="8pt" Font-Names="Arial" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TEN_NNTHUE" HeaderText="Tên NTT">
                            <HeaderStyle Font-Size="8pt" Font-Names="Arial" Font-Bold="True" HorizontalAlign="Center"
                                Width="250px"></HeaderStyle>
                            <ItemStyle Font-Size="8pt" Font-Names="Arial" HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TTIEN" HeaderText="Số tiền">
                            <HeaderStyle Font-Size="8pt" Font-Names="Arial" Font-Bold="True" HorizontalAlign="Center"
                                Width="120px"></HeaderStyle>
                            <ItemStyle Font-Size="8pt" Font-Names="Arial" HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages"></PagerStyle>
                    <FooterStyle BackColor="#D3D3D7" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                        Font-Size="X-Small" Font-Strikeout="False" Font-Underline="False" />
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td style="height: 25px;" align="center">
            </td>
        </tr>
    </table>
</asp:Content>
