﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false"
    CodeFile="frmKhoaSo1.aspx.vb" Inherits="pages_TraoDoi_frmKhoaSo1" Title="Khóa sổ cuối ngày" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" runat="Server">
    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">KHÓA SỔ CUỐI NGÀY</asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td valign="top" align="left">
                <table border='0' cellpadding='1' cellspacing='2' width='100%' class="form_input">
                    <tr>
                        <td class="form_label" width='170'>
                            <asp:Label ID="lblNgayLV" runat="server" CssClass="label">Ngày LV hiện tại</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox runat="server" ID="txtNgayLV" Width="100px" class='inputflat'></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_label">
                            <asp:Label ID="lblNgayLVTiep" runat="server" CssClass="label">Ngày LV tiếp theo</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox runat="server" ID="txtNgayLVTiep" Width="100px" class='inputflat'></asp:TextBox>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td class="form_label">
                            <asp:Label ID="Label4" runat="server" CssClass="label">Chi nhánh/Phòng GD</asp:Label>
                        </td>
                        <td align="left" valign="top" class="form_control">
                            <div style="vertical-align:top; height: 120px; overflow: auto; width: 100%;">
                                <asp:CheckBoxList ID="chkBanThu" runat="server" CssClass="inputflat" Width="100%">
                                </asp:CheckBoxList>
                            </div>
                        </td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="cmdKhoaSo" runat="server" Text="Khóa sổ" CssClass="ButtonCommand">
                </asp:Button>&nbsp;<asp:Button ID="cmdExit" runat="server" Text="Thoát" CssClass="ButtonCommand">
                </asp:Button>
            </td>
        </tr>
    </table>
</asp:Content>
