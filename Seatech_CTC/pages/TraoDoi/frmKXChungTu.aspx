﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false"
    CodeFile="frmKXChungTu.aspx.vb" Inherits="pages_TraoDoi_frmKetXuatCT" Title="Kết xuất chứng từ" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" runat="Server">
    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">KẾT XUẤT CHỨNG TỪ</asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td valign="top" align="left">
                <table border='0' cellpadding='1' cellspacing='2' width='100%' class="form_input">
                    <tr>
                        <td class="form_label" width='170'>
                            <asp:Label ID="lblNgayKX" runat="server" CssClass="label">Ngày kết xuất</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox runat="server" ID="txtNgayKX" Width="200px" class='inputflat'></asp:TextBox>
                        </td>
                    </tr>
                  
                </table>
                <br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="cmdKetXuat" runat="server" Text="Kết xuất" CssClass="ButtonCommand">
                </asp:Button>
            </td>
        </tr>
    </table>
</asp:Content>
