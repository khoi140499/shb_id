﻿Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports Business.TraoDoi
Imports System.Data
Partial Class pages_TraoDoi_frmTDTT_QTR
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            TCS_LoaiDL()
            Me.txtTuNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            Me.txtDenNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub

    Private Sub TCS_LoaiDL()      
        Dim drKieuDL As IDataReader
        Dim strSQL As String
        Dim tmpstrTsn_Code As String
        Dim tmpstrTsn_Name As String

        chkLoaiDL.Items.Clear()
        Try
            strSQL = "Select TSN_CODE,TSN_NAME From TRAN_LST " & _
                " Where TSN_CODE not In ('77','88','99') " & _
                " Order By TSN_NAME"

            drKieuDL = DataAccess.ExecuteDataReader(strSQL, CommandType.Text)
            Do While drKieuDL.Read
                tmpstrTsn_Code = drKieuDL.GetValue(0).ToString
                tmpstrTsn_Name = drKieuDL.GetValue(1).ToString
                Dim It As ListItem
                It = New ListItem(tmpstrTsn_Name, tmpstrTsn_Code)
                chkLoaiDL.Items.Add(It)

            Loop
            drKieuDL.Close()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình tìm kiếm loại dữ liệu !")
        End Try
    End Sub

    Private Function TCS_Where_CLS() As String
        Dim strResult As String = ""
        Dim i As Byte
        Dim j As Byte
        Dim Count As Byte
        Dim ck As New CheckBoxList


        Try
            ' Xử lý điều kiện loại dữ liệu:
            Count = 0
            For j = 0 To chkLoaiDL.Items.Count - 1
                If chkLoaiDL.Items(i).Selected = True Then
                    Count += 1
                End If
            Next
            If Count = 0 Then
                clsCommon.ShowMessageBox(Me, "Bạn phải chọn ít nhất một loại dữ liệu.")
                ' Khởi tạo lại Kết quả tra cứu ĐTNT
                grdDetail.DataSource = Nothing
                chkLoaiDL.Focus()
                Return ""
            End If
            For i = 0 To chkLoaiDL.Items.Count - 1
                If chkLoaiDL.Items(i).Selected Then
                    'strResult &= "(D.TSN_CODE = '" & Trim(Mid(Trim(chkLoaiDL.Items(i).Value), 101)) & "') " & "Or "
                    strResult &= "(D.TSN_CODE = '" & Trim(chkLoaiDL.Items(i).Value) & "') " & "Or "
                End If
            Next
            ' Bỏ phép toán Or
            strResult = Mid(strResult, 1, Len(strResult) - 3)
            strResult = "(" & strResult & ") And "
            ' Xử lý điều kiện ngày truyền nhận:
            If txtDenNgay.Text > txtTuNgay.Text Then
                clsCommon.ShowMessageBox(Me, "Từ ngày không được lớn hơn đến ngày.")
                grdDetail.DataSource = Nothing
                'grdDetail.Items.Count = 1
                Return ""
            End If
            strResult &= "(D.CREA_DATE >=" & ToValue(txtDenNgay.Text.ToString(), "DATE") & ") " & _
                         "And (D.CREA_DATE < " & ToValue(Convert.ToDateTime(txtTuNgay.Text).AddDays(1).ToString("dd/MM/yyyy"), "DATE") & ") "
            ' Xử lý điều kiện kiêu Truyền/Nhận:
            Select Case cboKieuTruyenNhan.SelectedIndex
                Case 0
                    strResult &= "And (D.PKG_TYPE <> '0') "
                Case 1
                    strResult &= "And (D.PKG_TYPE = '2') "
                Case 2
                    strResult &= "And (D.PKG_TYPE = '1') "
            End Select
        Catch ex As Exception
            strResult = ""
        End Try
        ' Trả lại kết quả cho hàm
        Return strResult
    End Function

    Protected Sub cmdInBK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdInBK.Click
        Try
            Dim tmpstrWhere As String = TCS_Where_CLS()

            ' Xử lý mệnh đề Where
            If tmpstrWhere <> "" Then
                Dim ds As DataSet
                Dim strSQL As String
                Dim intTongsoBG As Integer = 0
                Dim i As Integer = 1

                Try
                    strSQL = "Select T.TSN_NAME, to_char(D.CREA_DATE, 'dd/MM/yyyy HH24:MI:ss') CREA_DATE,D.TRAN_NUM," & _
                     " D.ERR_NUM,(SELECT lcn_name FROM Loca_Lst " & _
                     " Where lcn_code = D.LCN_SEND) SEND_LCN_NAME," & _
                     " (SELECT lcn_name " & _
                     " FROM Loca_Lst " & _
                     " WHERE lcn_code = D.LCN_RECV) REV_LCN_NAME" & _
                     " From TRAN_LST T,DATA_PKG D " & _
                     " Where (T.TSN_CODE = D.TSN_CODE) And " & tmpstrWhere & " " & _
                     " Order By D.CREA_DATE"

                    ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                    If IsEmptyDataSet(ds) Then
                        clsCommon.ShowMessageBox(Me, "Không có dữ liệu phù hợp với các điều kiện tra cứu.")
                        grdDetail.DataSource = Nothing
                        ' Khởi tạo lại grid kết xuất hoặc nhận
                        grdDetail.DataSource = Nothing
                        'grdDetail.Rows.Count = 1
                        txtDenNgay.Focus()
                        Exit Sub
                    End If
                    grdDetail.DataSource = ds.Tables(0).DefaultView
                    grdDetail.DataBind()
                    intTongsoBG = grdDetail.Items.Count - 1
                    ' Đặt quan tâm tới grid
                    'grdDetail.Focus()
                Catch ex As Exception
                    clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình tra cứu truyền nhận !")
                    grdDetail.DataSource = Nothing
                End Try
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình tra cứu truyền nhận !")
        End Try
    End Sub

    Protected Sub cmdExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        Response.Redirect("../frmHomeNV.aspx", False)
    End Sub

End Class
