﻿Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports Business.TraoDoi
Imports System.Data
Imports Business.NewChungTu
Imports Business.Common

Partial Class pages_TraoDoi_frmKXChungTu1
    Inherits System.Web.UI.Page

    Private strWhere As String = ""
    Private strNgayKX As String = ""
    Private strLstBanThu As String = ""


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            LoadDiemThu()
            Me.txtTuNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub

    Private Sub LoadDiemThu()
        Dim drDiemthu As IDataReader
        Dim strSQL As String
        Dim tmpstrMa_Dthu As String
        Dim tmpstrTen_Dthu As String
        chkBanThu.Items.Clear()
        Try
            strSQL = "select * from tcs_dm_diemthu order by ma_dthu"
            drDiemthu = DataAccess.ExecuteDataReader(strSQL, CommandType.Text)

            Do While drDiemthu.Read
                tmpstrMa_Dthu = drDiemthu.GetValue(0).ToString
                tmpstrTen_Dthu = drDiemthu.GetValue(1).ToString
                Dim It As ListItem
                It = New ListItem(tmpstrTen_Dthu, tmpstrMa_Dthu)
                chkBanThu.Items.Add(It)
            Loop
            drDiemthu.Close()

            For i = 0 To chkBanThu.Items.Count - 1
                chkBanThu.Items(i).Selected = True
            Next

        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình tìm kiếm loại dữ liệu !")
        End Try
    End Sub

#Region "Function"

    Private Function getWhereCls() As String
        Dim i As Integer = 0
        Dim iCount As Integer = 0
        Dim strResult As String = String.Empty
        strResult = " (hdr.trang_thai='01') And (HDR.so_bthu In ("
        strLstBanThu = "("
        For i = 0 To chkBanThu.Items.Count - 1
            If chkBanThu.Items(i).Selected Then
                strResult &= Trim(chkBanThu.Items(i).Value) & ","
                strLstBanThu &= Trim(chkBanThu.Items(i).Value) & ","
                iCount += 1
            End If
        Next
        strResult = strResult.Remove(strResult.Length - 1, 1) & ") "
        strLstBanThu = strLstBanThu.Remove(strLstBanThu.Length - 1, 1) & ")"

        If iCount = 0 Then
            clsCommon.ShowMessageBox(Me, "Phải chọn ít nhất một Điểm thu/Phòng GD cần kết xuất !")
            Return String.Empty
        End If
        'strResult &= " (HDR.Ngay_CT >= " & ConvertDateToNumber(txtTuNgay.Text.Trim) & _
        '             ") And (HDR.Ngay_CT <= " & ConvertDateToNumber(txtDenNgay.Text.Trim) & ") "

        strResult &= " And (HDR.Ngay_CT = " & ConvertDateToNumber(txtTuNgay.Text.Trim) & "))"
        strNgayKX &= ConvertDateToNumber(txtTuNgay.Text.Trim)

        If txtTuSoCT.Text.Trim <> "" Then
            strResult &= " And HDR.So_CT>='" & txtTuSoCT.Text.Trim & "'"
        End If
        If txtDenSoCT.Text.Trim <> "" Then
            strResult &= " And HDR.So_CT<='" & txtDenSoCT.Text.Trim & "'"
        End If
        Return strResult
    End Function

    Private Sub fillFlexHeader(ByVal strWhere As String)
        If strWhere <> "" Then
            Dim dsChungTu As DataSet
            Dim dblTongtien As Double = 0
            Dim intTongsoBG As Integer = 0
            Dim intRow As Integer
            Dim objHdr As New infChungTuHDR
            Try
                intRow = 1
                dsChungTu = Business.Common.buChungTuTD.LoadDataHeaderKXGNT(strWhere)
                If IsEmptyDataSet(dsChungTu) Then
                    clsCommon.ShowMessageBox(Me, "Không tìm thấy chứng từ phù hợp !")
                    txtTuNgay.Focus()
                    Exit Sub
                End If
                Me.grdMaster.DataSource = dsChungTu.Tables(0)
                Me.grdMaster.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End If
    End Sub

#End Region


#Region "Form Event"

    Private Sub cmdTraCuu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTraCuu.Click
        Try
            strWhere = getWhereCls()
            If strWhere <> "" Then
                fillFlexHeader(strWhere)
            End If
        Catch ex As Exception
            Throw ex
            clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình tìm kiếm loại dữ liệu !")
        End Try
    End Sub

    Private Sub cmdKetxuat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdKetXuat.Click
        Try
            strWhere = getWhereCls()
            Dim dsChungTu As DataSet
            If strWhere <> "" Then
                Dim intTongso As Integer = 0
                Dim intSoloi As Integer = 0

                dsChungTu = buChungTuTD.LoadDataHeaderKXGNT(strWhere)

                If IsEmptyDataSet(dsChungTu) Then
                    clsCommon.ShowMessageBox(Me, "Không tìm thấy chứng từ phù hợp !")
                    txtTuNgay.Focus()
                    Exit Sub
                End If

                buTraoDoi.ExportData(strNgayKX)
                clsCommon.ShowMessageBox(Me, "Đã kết thúc quá trình kết xuất dữ liệu chứng từ GNT !")
            End If
        Catch ex As Exception
            Throw ex
            clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình kết xuất dữ liệu !")
        End Try
    End Sub

    Private Sub cmdIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdIn.Click
    End Sub

    Protected Sub cmdThoat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdThoat.Click
        Response.Redirect("../frmHomeNV.aspx", False)
    End Sub

#End Region


End Class
