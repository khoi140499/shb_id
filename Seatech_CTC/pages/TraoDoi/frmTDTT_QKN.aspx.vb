﻿Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports Business.TraoDoi
Imports System.Data
Partial Class pages_TraoDoi_frmTDTT_QKN
    Inherits System.Web.UI.Page

    Private Function TCS_Where_CLS() As String
        '-----------------------------------------------------
        ' Mục đích: Xây dựng mệnh đề Where tra cứu truyền nhận.
        ' Tham số: 
        ' Giá trị trả về: - strResult(khác trống)
        '                 - Trống nếu các điều kiện không thoả mãn  
        ' Ngày viết: 15/01/2008
        ' Người viết: Nguyễn Hữu Hoan
        ' ----------------------------------------------------
        Dim strResult As String = ""
        Dim strTmpLoaiDL As String
        Dim i As Byte
        Dim j As Byte
        Dim Count As Byte
        Dim ck As New CheckBoxList


        Try
            ' Xử lý điều kiện loại dữ liệu:
            Count = 0
            For j = 0 To chkLoaiDL.Items.Count - 1
                If chkLoaiDL.Items(i).Selected = True Then
                    Count += 1
                End If
            Next
            If Count = 0 Then
                clsCommon.ShowMessageBox(Me, "Bạn phải chọn ít nhất một loại dữ liệu.")
                ' Khởi tạo lại Kết quả tra cứu ĐTNT
                grdDetail.DataSource = Nothing
                chkLoaiDL.Focus()
                Return ""
            End If
            For i = 0 To chkLoaiDL.Items.Count - 1
                If chkLoaiDL.Items(i).Selected Then
                    'strResult &= "(I.TSN_CODE = '" & Trim(Mid(Trim(chkLoaiDL.Items(i).Value), 101)) & "') " & "Or "
                    strResult &= "(I.TSN_CODE = '" & Trim(chkLoaiDL.Items(i).Value) & "') " & "Or "
                End If
            Next
            ' Bỏ phép toán Or
            strResult = Mid(strResult, 1, Len(strResult) - 3)
            strResult = "(" & strResult & ") And "
            ' Xử lý điều kiện ngày truyền nhận:
            If txtTuNgay.Text > txtDenNgay.Text Then
                clsCommon.ShowMessageBox(Me, "Từ ngày không được lớn hơn đến ngày.")
                grdDetail.DataSource = Nothing
                'grdDetail.Items.Count = 1
                Return ""
            End If
            strResult &= "(I.IEP_DATE >=" & ToValue(txtTuNgay.Text.ToString(), "DATE") & ") " & _
                         "And (I.IEP_DATE < " & ToValue(Convert.ToDateTime(txtDenNgay.Text).AddDays(1).ToString("dd/MM/yyyy"), "DATE") & ") "

            strResult &= " AND ("
            For i = 0 To chkTrangThai.Items.Count - 1
                If chkTrangThai.Items(i).Selected And chkTrangThai.Items(i).Value = "0" Then  ' Lỗi
                    strResult &= "(I.IEP_ERRNO <> 0) " & "Or "
                End If
                If chkTrangThai.Items(i).Selected And chkTrangThai.Items(i).Value = "1" Then  ' Không lỗi
                    strResult &= "(I.IEP_ERRNO = 0) " & "Or "
                End If
            Next



            strResult = Mid(strResult, 1, Len(strResult) - 3)
            strResult &= ") And "
            ' Xử lý điều kiện người thực hiện
            If (cboNguoiTH.SelectedIndex > 0) Then
                strResult &= "(I.IEP_USER = '" & cboNguoiTH.SelectedValue.ToString() & "') And "
            End If
            ' Bỏ phép toán And
            strResult = Mid(strResult, 1, Len(strResult) - 4)
        Catch ex As Exception
            strResult = ""
        End Try
        ' Trả lại kết quả cho hàm
        Return strResult
    End Function
    Private Sub TCS_LoaiDL()
        '-----------------------------------------------------
        ' Mục đích: Khởi tạo danh sách Loại dữ liệu.
        ' Tham số: 
        ' Giá trị trả về:
        ' Ngày viết: 02/08/2005
        ' Người viết: Nguyễn Quốc Việt
        ' ----------------------------------------------------
        Dim cnQKieuDL As New DataAccess
        Dim drKieuDL As IDataReader
        Dim strSQL As String
        Dim tmpstrTsn_Code As String
        Dim tmpstrTsn_Name As String

        chkLoaiDL.Items.Clear()
        Try
            strSQL = "Select TSN_CODE,TSN_NAME " & _
                "From TRAN_LST " & _
                "Where TSN_CODE not In ('77','88','99') " & _
                "Order By TSN_NAME"
            'strSQL = "select ma_nhom,ten_nhom from tcs_dm_nhom"
            drKieuDL = cnQKieuDL.ExecuteDataReader(strSQL, CommandType.Text)

            Do While drKieuDL.Read
                tmpstrTsn_Code = drKieuDL.GetValue(0).ToString
                tmpstrTsn_Name = drKieuDL.GetValue(1).ToString
                Dim It As ListItem
                It = New ListItem(tmpstrTsn_Name, tmpstrTsn_Code)
                chkLoaiDL.Items.Add(It)

            Loop
            drKieuDL.Close()
            cnQKieuDL.Dispose()
        Catch ex As Exception
            If Not drKieuDL Is Nothing Then
                drKieuDL.Close()
            End If
            If Not cnQKieuDL Is Nothing Then
                cnQKieuDL.Dispose()
            End If
            clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình tìm kiếm loại dữ liệu !")
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            TCS_LoaiDL()
            'Load danh muc người lập
            DM_NguoiLap()

            Me.txtDenNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            Me.txtTuNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub

    Private Sub DM_NguoiLap()
        '-----------------------------------------------------
        ' Mục đích: Khởi tạo danh sách nhân viên.
        ' Tham số: 
        ' Giá trị trả về:
        ' Ngày viết: 22/10/2007
        ' Người viết: Lê Hồng Hà
        ' ---------------------------------------------------- 
        Try

            Dim dsNV As DataSet = Get_DM_NhanVien()
            cboNguoiTH.DataSource = dsNV.Tables(0)
            cboNguoiTH.DataTextField = dsNV.Tables(0).Columns(1).ToString
            cboNguoiTH.DataValueField = dsNV.Tables(0).Columns(0).ToString
            cboNguoiTH.DataBind()
            Dim it As ListItem
            it = New ListItem("Tất cả", "0")
            cboNguoiTH.Items.Insert(0, it)
            'cboNguoiTH.Text = "Tất cả"
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub cmdInBK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdInBK.Click
        Dim tmpstrWhere As String = TCS_Where_CLS()
        ' Xử lý mệnh đề Where
        If tmpstrWhere <> "" Then
            Dim cnQKN As DataAccess
            Dim ds As DataSet
            Dim strSQL As String
            Dim intTongsoBG As Integer = 0
            Dim i As Integer = 1

            Try
                cnQKN = New DataAccess

                strSQL = "Select T.TSN_NAME,decode(I.IEP_TYPE,'Y','Kết xuất','Nhận')," & _
                                        "to_char(I.IEP_DATE,'dd/MM/yyyy HH24:MI:ss'),I.IEP_ROWNO,I.IEP_ERRNO,NV.ten," & _
                                        "T.TSN_CODE,I.ID " & _
                                        "From TRAN_LST T,IMEXPORT I,TCS_DM_NHANVIEN NV " & _
                                        "Where " & tmpstrWhere & " And " & _
                                        "(T.TSN_CODE = I.TSN_CODE) And " & _
                                        "I.iep_user = to_char(NV.Ma_NV) " & _
                "Order By I.IEP_DATE"
                ds = cnQKN.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If IsEmptyDataSet(ds) Then
                    clsCommon.ShowMessageBox(Me, "Không có dữ liệu phù hợp với các điều kiện tra cứu.")
                    grdDetail.DataSource = Nothing
                    ' Khởi tạo lại grid kết xuất hoặc nhận
                    txtTuNgay.Focus()
                    Exit Sub
                End If
                grdDetail.DataSource = ds.Tables(0).DefaultView
                grdDetail.Focus()
                Dim strLoi As String
                '  strLoi = Trim(grdDetail.GetData(grdDetail.Row, 4))
                'If "0".Equals(strLoi) Then
                '    cmdChiTiet.Enabled = False
                'Else
                '    cmdChiTiet.Enabled = True
                'End If
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình tra cứu kết xuất hoặc nhận !")
                grdDetail.DataSource = Nothing
                ' Khởi tạo lại grid kết xuất hoặc nhận
                'cmdChiTiet.Enabled = False
                'grdDetail.Rows.Count = 1
            Finally
                If Not cnQKN Is Nothing Then
                    cnQKN.Dispose()
                End If
            End Try
        End If
    End Sub

    Protected Sub cmdExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        Response.Redirect("../frmHomeNV.aspx", False)
    End Sub
End Class
