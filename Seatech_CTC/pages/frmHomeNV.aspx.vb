﻿Imports ETAX_GDT
Imports System.Data
Imports VBOracleLib
Partial Class pages_HeThong_frmHomeNV
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Request.Params("CheckNN") Is Nothing Then
            lbThongBao.Text = "Hôm nay là ngày nghỉ. Bạn không thể truy cập chức năng này!!!"
        End If

        If Not IsPostBack Then
            'loadGrid()
        End If
        

    End Sub

    Protected Sub loadGrid()
        Try
            Dim ds As DataSet
            Dim sql As String = "select * from tcs_dm_msg_ntdt order by id_gdich desc"
            ds = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            If Not ds Is Nothing Then
                dtgdata.DataSource = ds
                dtgdata.DataBind()
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Error source: " + ex.Source + ControlChars.NewLine + "Error code: System error!" + ControlChars.NewLine + "Error message: " + ex.Message)
        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        'ltload.Text = "<img src='../images/loading.gif' />"
        Try
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Info("MSG: Bắt đầu khởi tạo msg")
            Dim msg As ProcessMSG = New ProcessMSG()
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Info("MSG: Bắt đầu lấy msg")
            msg.getAllMsg()
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Info("MSG: Kết thúc lấy msg")
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Error source: " + ex.Source + ControlChars.NewLine + "Error code: System error!" + ControlChars.NewLine + "Error message: " + ex.Message)
        End Try

        loadGrid()


        'ltload.Text = ""
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Dim baocao As ProcessBaoCaoDoiChieu_NH_TTXL = New ProcessBaoCaoDoiChieu_NH_TTXL()
        'baocao.BaoCaoDoiChieu(tbNgayDC.Text)
    End Sub

    Protected Sub btnTest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTest.Click
        Dim msg As ProcessMSG = New ProcessMSG()
        msg.getAllMsgTest()
    End Sub
End Class
