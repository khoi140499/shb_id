﻿Imports System.Data
Imports System.Reflection
Imports Business_HQ.HaiQuan
Imports ConcertCurToText
Imports CustomsServiceV3
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports System.Diagnostics
Imports log4net
Imports Business.Common
Imports CTuCommon = Business.CTuCommon
Imports System.Collections.Generic
Imports Business
Imports Business.BaoCao
Imports System.Xml

Partial Class pages_BienLai_frmKSBienLai
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private mv_objUser As New CTuUser
    Private illegalChars As String = "[&]"
    Private Shared strMaNhom_GDV As String = "" 'ConfigurationManager.AppSettings.Get("GROUP_TELLER").ToString()
    Private Shared strTT_TIENMAT As String = "" 'ConfigurationManager.AppSettings.Get("TK_TIENMAT").ToString()
    Private Shared pv_cifNo As String = String.Empty
    Private Shared cls_corebank As New CorebankServiceESB.clsCoreBank
    Private Shared CORE_VERSION As String = ConfigurationManager.AppSettings.Get("CORE.VERSION").ToString()

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init
        If Not IsPostBack Then
            'CreateDumpChiTietCtGrid() 'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu
            'ClearCtHeader() 'Xóa thông tin ở grid header
            'ShowHideCTRow(False)
            'lblStatus.Text = "Hãy chọn một chứng từ cần kiểm soát"

        End If
        If Session.IsNewSession Then
            ' Force session to be created;
            ' otherwise the session ID changes on every request.
            Session("ForceSession") = DateTime.Now
        End If
        ' 'Sign' the viewstate with the current session.
        Me.ViewStateUserKey = Session.SessionID
        If Page.EnableViewState Then
            ' Make sure ViewState wasn't passed on the querystring.
            ' This helps prevent one-click attacks.
            If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
                Throw New Exception("Viewstate existed, but not on the form.")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not clsCommon.GetSessionByID(Session.Item("User").ToString()) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdKS.Click, AddressOf cmdKS_Click
            RemoveHandler cmdChuyenTra.Click, AddressOf cmdChuyenTra_Click
            RemoveHandler cmdChuyenTTSP.Click, AddressOf cmdChuyenTTSP_Click
            Exit Sub
        End If
        'Kiểm tra trạng thái session
        If Not Session.Item("User") Is Nothing Then
            mv_objUser = New CTuUser(Session.Item("User").ToString())
        Else
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        'Check các quyền của user

        If Not IsPostBack Then
            'Khởi tạo các giá trị khởi đầu cho các điều khiển
            'Load danh sách chứng từ thành lập bởi điểm thu
            LoadDSCT("99")
            LoadDM_Nhanvien()
            LoadDM_TrangThai()
            '_teller = Session.Item("TELLER").ToString()
            '_branhCode = Session.Item("MA_CN_USER_LOGON").ToString()
            'LoadCateLoaiTienThue()
        End If
    End Sub
    Private Sub LoadDSCT(ByVal pv_strTrangThaiCT As String)
        Try
            Dim dsCT As DataSet = GetDSCT(pv_strTrangThaiCT)
            If (Not dsCT Is Nothing) And (dsCT.Tables(0).Rows.Count > 0) Then
                Me.grdDSCT.DataSource = dsCT
                Me.grdDSCT.DataBind()

            Else
                CreateDumpDSCTGrid()
            End If
        Catch ex As Exception
            logger.Error(ex.Message + ex.StackTrace)
        End Try
    End Sub
    Private Sub CreateDumpDSCTGrid()
        'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu
        Dim v_dt As New DataTable()
        v_dt.Columns.Add(New DataColumn("KYHIEU_CT"))
        v_dt.Columns.Add(New DataColumn("NGAY_KB"))
        v_dt.Columns.Add(New DataColumn("SO_CT"))
        v_dt.Columns.Add(New DataColumn("TRANG_THAI"))
        v_dt.Columns.Add(New DataColumn("TTIEN"))
        v_dt.Columns.Add(New DataColumn("MA_NV"))
        v_dt.Columns.Add(New DataColumn("SHKB"))
        v_dt.Columns.Add(New DataColumn("TEN_DN"))
        v_dt.Columns.Add(New DataColumn("MA_KS"))
        v_dt.Columns.Add(New DataColumn("REF_CORE_TTSP"))

        grdDSCT.Controls.Clear()
        'Dim v_dumpRow As DataRow = v_dt.NewRow
        'v_dt.Rows.Add(v_dumpRow)
        grdDSCT.DataSource = v_dt
        grdDSCT.DataBind()
    End Sub
    Private Sub LoadDM_TrangThai()
        Dim dt As DataTable = DM_TRANGTHAI()
        ddlTrangThai.DataSource = dt
        ddlTrangThai.DataTextField = "mota"
        ddlTrangThai.DataValueField = "trangthai"
        ddlTrangThai.DataBind()
        ddlTrangThai.Items.Insert(0, New ListItem("Tất cả", "99"))
        ddlTrangThai.SelectedIndex = 0
    End Sub
    Private Function DM_TRANGTHAI() As DataTable
        Dim dt As DataTable
        dt = Business.BienLai.buBienLai.GET_DM_TRANGTHAI_BL("CTBL")
        Return dt
    End Function
    Private Sub LoadDM_Nhanvien()
        'Dim dt As DataTable = DM_NV()
        'ddlMacker.DataSource = dt
        'ddlMacker.DataTextField = "ten_dn"
        'ddlMacker.DataValueField = "Ma_nv"
        'ddlMacker.DataBind()
        'ddlMacker.Items.Insert(0, New ListItem("Tất cả", "99"))
        'ddlMacker.SelectedIndex = 0
    End Sub
    'Private Function DM_NV() As DataTable
    '    Dim cnDiemThu As New DataAccess
    '    Dim dt As DataTable
    '    Dim dk As String = ""
    '    Dim strcheckHS As String = checkHSC()
    '    Dim str_Ma_Nhom As String = ""
    '    Dim strSQL As String = ""

    '    str_Ma_Nhom = LOAD_MN()

    '    dt = HaiQuan.GET_DM_NV(strcheckHS, str_Ma_Nhom, Session.Item("MA_CN_USER_LOGON").ToString, Session.Item("User").ToString, StrMaNhomGdv)

    '    Return dt
    'End Function
    Public Function LOAD_MN() As String
        Dim str_return As String = ""

        str_return = HaiQuan.GET_MA_NHOM(Session.Item("User").ToString)

        Return str_return
    End Function
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function
    Private Function GetDSCT(ByVal pv_strTrangThaiCT As String) As DataSet
        Try
            If pv_strTrangThaiCT.Equals("99") Then pv_strTrangThaiCT = ""
            'If ddlMacker.SelectedValue = "99" Then
            '    mv_objUser.Ma_NV = ""
            'Else
            '    mv_objUser.Ma_NV = ddlMacker.SelectedValue
            'End If
            If Not HttpContext.Current.Session.Item("User") Is Nothing Then
                mv_objUser = New CTuUser(HttpContext.Current.Session.Item("User").ToString())
            End If
            'mv_objUser.Ban_LV="" cho phep lay chung tu thu ho
            Dim dsCT As DataSet = Business.BienLai.buBienLai.CTU_GetListKiemSoatBL(mv_objUser.Ngay_LV, pv_strTrangThaiCT, _
                                  "", Session.Item("User"), "")
            Return dsCT
        Catch ex As Exception
            logger.Error(ex.StackTrace)
            Return Nothing
        End Try

    End Function
    Protected Sub cmdKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdKS.Click

        logger.Info("Kiem soat chung tu Biên lai : '" + hdfSo_CT.Value.ToString + "' by " + Session.Item("UserName").ToString)
        Dim v_blnReturn As Boolean = False

        Try
            'Disable button tranh lam viec 2 lan
            EnabledButtonWhenDoTransaction(False)

            'Kiểm tra mật khẩu kiểm soát
            'If Not (txtMatKhau.Text.Length = 0) Then
            '    If Not (mv_objUser.Mat_Khau.Equals(VBOracleLib.Security.EncryptStr(txtMatKhau.Text))) Then
            '        lblStatus.Text = "Mật khẩu kiểm soát không hợp lệ.Hãy thử lại"
            '        EnabledButtonWhenDoTransaction(True)
            '        Exit Sub
            '    End If
            'Else
            '    lblStatus.Text = "Phải nhập mật khẩu khi kiểm soát."
            '    EnabledButtonWhenDoTransaction(True)
            '    Exit Sub
            'End If
            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            Dim v_strRefCoreTTSP As String = hdfSoRefCore.Value
            'ddlMaHTTT
            Dim strHT_TT As String = ddlMaHTTT.Text.Trim
          
            If ((Not v_strSHKB Is Nothing) And (Not v_strNgay_KB Is Nothing) And (Not v_strSo_CT Is Nothing)) Then

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = BienLai.buBienLai.CTU_Get_TrangThai(v_strSo_CT)
                Dim strTT_Form As String = v_strTrangThai_CT
                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If

                'Lấy trực tiếp phương thức thanh toán trong DB
                Dim strPT_TT As String = BienLai.buBienLai.CTU_Get_PTTT(v_strSo_CT)
                Dim v_strHost_REFNo As String = String.Empty
                Dim v_strRM_REF_NO As String = String.Empty
                Dim v_strBDS_REFNo As String = String.Empty
                Dim v_strExcutionDate As String = String.Empty
                Dim strTongTienTT As String = txtTongTien.Text.Replace(".", "")
                Dim strSoduTaiKhoanKH As String = txtSoDu_KH_NH.Text.Replace(".", "")
                Dim v_strRollout_acct_no As String = txtTK_KH_NH.Text
                Dim remark As String = txtGHICHU.Text
                remark = Globals.RemoveSign4VietnameseString(remark)
                If remark.Length > 210 Then
                    remark = remark.Substring(0, 210)
                End If
                Dim strTKCo As String = txtTK_NSNN.Text
                'txtMA_NT
                Dim strMa_NT As String = txtMA_NT.Text
                Dim str_AccountNo As String = txtTK_KH_NH.Text.Trim
                Dim v_strAmt As String = strTongTienTT 'txtTongTrichNo.Text.Replace(".", "") 'strTongTienTT
                Dim vFee As String = "0"
                Dim vVat As String = "0"
                Dim teller_id As String = clsCTU.Get_TellerID(v_strMa_NV) 'giao dich vien

                Dim approver_id As String = mv_objUser.TELLER_ID 'Kiem soat vien
                Dim v_total_ID As String

                Dim ma_ChiNhanh As String
                ma_ChiNhanh = clsCTU.TCS_GetMaCN_NH(mv_objUser.MA_CN)

                Dim vStrTranDate As String = clsCTU.TCS_GetNgayBDS().ToString

                Dim cls_songphuong As CorebankServiceESB.clsSongPhuong = New CorebankServiceESB.clsSongPhuong()

                Dim vTtcutofftime As String = clsCTU.TCS_TTCUTOFFTIME().ToString
                Try
                    vStrTranDate = DateTime.ParseExact(vStrTranDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyyMMdd")
                Catch ex As Exception

                End Try
                Dim fromCurrency As String = txtMA_NT.Text
                Dim loainghiepvu As String = "BIENLAI"
                Dim vStrPtht As String = ""
                Dim vTkHachToan As String = ""
                If vTtcutofftime = "1" Then
                    vStrPtht = "TPAYB4CUTOFF"
                    vTkHachToan = "120101001"
                Else
                    vStrPtht = "TPAYAFCUTOFF"
                    vTkHachToan = "280701018"
                End If

                Dim v_strErrorCode As String = ""
                Dim v_strErrorMessage As String = ""
                Dim tt_kb As String = ""
                Dim ten_tt_kb As String = ""
                Dim sMACN As String = hdfMACN.Value
                Dim ngay_GD_SP As String = Business_HQ.Common.mdlCommon.ConvertNumberToDate(mv_objUser.Ngay_LV).ToString("yyyyMMdd")
                Dim strResult As String = cls_songphuong.ValiadSHB(v_strSHKB)

                Dim strNHB As String = ""
                Dim strACC_NHB As String = ""
                Dim strMA_CN_SP As String = ""
                If strResult.Length > 8 Then
                    Dim strarr() As String
                    strarr = strResult.Split(";")
                    strNHB = strarr(0).ToString
                    strACC_NHB = strarr(1).ToString
                    strMA_CN_SP = strarr(2).ToString
                End If


                Dim strMACN_TK As String = ""
                Dim rm_ref_no As String = ""
                Dim req_Internal_rmk As String = ""
                Dim boolSP As Boolean = False
                Dim str_PayMent As String = ""
                If strNHB.Length >= 8 Then
                    ''đi theo hàm thanh toán mới, sau đó gửi song phương
                    boolSP = True
                    'v_strErrorCode = "00000"
                    'rm_ref_no = Date.Now.ToString("yyyyMMddHHmmss")

                    'Nếu thanh toán được kho bạc SHB mới truy vấn tài khoản để lấy mã chi nhánh tài khoản
                    If strHT_TT = "01" Then
                        If (txtTK_KH_NH.Text.Trim.Length > 0) Then
                            Dim xmlDoc As New XmlDocument
                            Dim ma_loi As String = ""
                            Dim so_du As String = ""
                            Dim ten_TK_KH As String = ""
                            Dim branch_tk As String = ""
                            Dim v_strReturn As String = cls_corebank.GetTK_KH_NH(txtTK_KH_NH.Text.Trim, ma_loi, so_du, branch_tk)

                            If ma_loi = "00000" Or ma_loi Is Nothing Then
                                strMACN_TK = branch_tk
                            Else
                                lblStatus.Text = "Không truy vấn được thông tin tài khoản.Mã lỗi:" & v_strErrorCode & ",mô tả:" & clsCTU.MappingErrorCode(v_strErrorCode)
                                EnabledButtonWhenDoTransaction(True)
                                Exit Sub
                            End If
                        End If
                    ElseIf strHT_TT = "05" Then
                        strMACN_TK = sMACN
                    End If

                    'cap nhat TTSP ve 1
                    ChungTu.buChungTu.CTU_Update_TTSP(v_strSo_CT, "1")
                    str_PayMent = cls_corebank.PaymentSP("TAX", ngay_GD_SP, v_strSo_CT, sMACN, strMACN_TK, str_AccountNo, strTongTienTT, strMa_NT, req_Internal_rmk, remark, _
                            v_strMa_NV, mv_objUser.Ma_NV, "ETAX_ND", strTKCo, strNHB.ToString().Substring(2, 3), strNHB, strACC_NHB, strMA_CN_SP, v_strErrorCode, rm_ref_no)

                End If

                'Chuyen thong tin sang TTSP
                Dim blnSuccess As Boolean = True
                Dim errcode As String = ""
                Dim errdesc As String = ""
                Dim errdesc_ttsp As String = ""
                Try
                    If v_strErrorCode.Equals("00000") Then

                        'cap nhat TG_KY, trang_thai='03'
                        DataAccess.ExecuteNonQuery("update tcs_bienlai_hdr set tg_ky = sysdate,ngay_kh_nh=sysdate,so_ct_nh = '" & rm_ref_no & "' , trang_thai='03' where so_ct='" & v_strSo_CT & "'", CommandType.Text)
                        Try
                            'errdesc_ttsp = cls_songphuong.SendBienLaiSP1(v_strSo_CT, v_strRefCoreTTSP)
                            errdesc_ttsp = cls_songphuong.SendBienLaiSP(v_strSo_CT)
                        Catch ex As Exception
                            'chi ghi log, khong throw, van gui thue binh thuong
                            logger.Info(ex.Message + ex.StackTrace)
                        End Try


                        lblStatus.Text = "Chứng từ đã chuyển số liệu sang thanh toan song phương: " & errdesc_ttsp
                        cmdChuyenTTSP.Enabled = False

                    Else
                        lblStatus.Text = "Đã có lỗi xảy ra khi hạch toán chứng từ.Mã lỗi: " & v_strErrorCode & ",mô tả:" & v_strErrorMessage
                        EnabledButtonWhenDoTransaction(True)
                        Exit Sub
                    End If
                Catch ex As Exception
                    blnSuccess = False
                    lblStatus.Text = "Chứng từ đã được hạch toán nhưng chưa chuyển được số liệu sang thanh toán song phương"
                    logger.Info(ex.Message + ex.StackTrace)
                    Exit Sub
                End Try
                LoadDSCT(ddlTrangThai.SelectedValue)
            End If
        Catch ex As Exception
            logger.Info(ex.Message + ex.StackTrace)
        End Try
    End Sub
    Protected Sub cmdChuyenTra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyenTra.Click
        'Disable button tranh lam viec 2 lan
        EnabledButtonWhenDoTransaction(False)

        'Kiểm tra mật khẩu kiểm soát
        'If Not (txtMatKhau.Text.Length = 0) Then
        '    If Not (mv_objUser.Mat_Khau.Equals(VBOracleLib.Security.EncryptStr(txtMatKhau.Text))) Then
        '        lblStatus.Text = "Mật khẩu kiểm soát không hợp lệ.Hãy thử lại"
        '        EnabledButtonWhenDoTransaction(True)
        '        Exit Sub
        '    End If
        'Else
        '    lblStatus.Text = "Phải nhập mật khẩu khi chuyển trả."
        '    EnabledButtonWhenDoTransaction(True)
        '    Exit Sub
        'End If

        'txtLYDO_TC
        'Kiểm tra mật khẩu kiểm soát
        If (txtLYDO_TC.Text.Trim.Length = 0) Then
            lblStatus.Text = "Phải nhập Lý do từ chối khi chuyển trả."
            EnabledButtonWhenDoTransaction(True)
            Exit Sub
        End If

        Dim v_strSHKB As String = hdfSHKB.Value
        Dim v_strNgay_KB As String = hdfNgay_KB.Value
        Dim v_strSo_CT As String = hdfSo_CT.Value
        Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
        Dim v_strMa_NV As String = hdfMa_NV.Value
        Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
        Dim v_strLydoTC As String = txtLYDO_TC.Text.Trim
        If ((Not v_strSHKB Is Nothing) And (Not v_strNgay_KB Is Nothing) And (Not v_strSo_CT Is Nothing)) Then

            'Kiểm tra trạng thái 1 lần nữa trước khi ghi
            Dim strTT_Data As String = BienLai.buBienLai.CTU_Get_TrangThai(v_strSo_CT)
            Dim strTT_Form As String = v_strTrangThai_CT
            If (strTT_Data <> strTT_Form) Then
                lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                Exit Sub
            End If

            ' cập nhật về trạng thái chuyển trả
            If v_strTrangThai_CT.Equals("02") Then
                v_strTrangThai_CT = "04" 'Trạng thái chuyển trả
                BienLai.buBienLai.updateTrangThaiBienLaiKS(v_strSo_CT, v_strNgay_KB, mv_objUser.Ma_NV, v_strTrangThai_CT, v_strLydoTC)
            Else
                lblStatus.Text = "Trạng thái của chứng từ không được phép chuyển trả. Hãy 'Refresh', rồi thực hiện lại"
                EnabledButtonWhenDoTransaction(True)
                Exit Sub
            End If
            LoadDSCT(ddlTrangThai.SelectedValue)
            lblStatus.Text = "Chuyển trả chứng từ thành công."
        End If
    End Sub

    Protected Sub cmdChuyenTTSP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyenTTSP.Click
        'Disable button tranh lam viec 2 lan
        EnabledButtonWhenDoTransaction(False)

        'Kiểm tra mật khẩu kiểm soát
        'If Not (txtMatKhau.Text.Length = 0) Then
        '    If Not (mv_objUser.Mat_Khau.Equals(VBOracleLib.Security.EncryptStr(txtMatKhau.Text))) Then
        '        lblStatus.Text = "Mật khẩu kiểm soát không hợp lệ.Hãy thử lại"
        '        Exit Sub
        '    End If
        'Else
        '    lblStatus.Text = "Phải nhập mật khẩu khi chuyển song phương."
        '    Exit Sub
        'End If
        Dim v_strSHKB As String = hdfSHKB.Value
        Dim v_strNgay_KB As String = hdfNgay_KB.Value
        Dim v_strSo_CT As String = hdfSo_CT.Value
        Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
        Dim v_strMa_NV As String = hdfMa_NV.Value
        Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
        Dim v_strRefCoreTTSP As String = hdfSoRefCore.Value
        If ((Not v_strSHKB Is Nothing) And (Not v_strNgay_KB Is Nothing) And (Not v_strSo_CT Is Nothing)) Then

            'Kiểm tra trạng thái 1 lần nữa trước khi ghi
            Dim strTT_Data As String = BienLai.buBienLai.CTU_Get_TrangThai(v_strSo_CT)
            Dim strTT_Form As String = v_strTrangThai_CT
            If (strTT_Data <> strTT_Form) Then
                lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                EnabledButtonWhenDoTransaction(True)
                Exit Sub
            End If

            If v_strTrangThai_CT.Equals("") Then
                lblStatus.Text = "Trạng thái chứng từ không được gửi sang thanh toán song phương."
                EnabledButtonWhenDoTransaction(False)
                Exit Sub
            Else

                Dim blnSuccess As Boolean = True
                Dim errcode As String = ""
                Dim errdesc As String = ""
                Dim errdesc_ttsp As String = ""
                Dim cls_songphuong As New CorebankServiceESB.clsSongPhuong
                Try
                    If v_strTrangThai_CT.Equals("08") Then

                        Try
                            'errdesc_ttsp = cls_songphuong.SendBienLaiSP(v_strSo_CT, v_strRefCoreTTSP, vStrTranDate, "1")
                            errdesc_ttsp = cls_songphuong.SendBienLaiSP(v_strSo_CT)
                        Catch ex As Exception
                            'chi ghi log, khong throw, van gui thue binh thuong
                            logger.Error(ex.Message + ex.StackTrace)
                        End Try


                        lblStatus.Text = "Chứng từ đã chuyển số liệu sang thanh toan song phương: " & errdesc_ttsp
                        cmdChuyenTTSP.Enabled = False

                    Else
                        lblStatus.Text = "Trạng thái chứng từ không được gửi sang thanh toán song phương."
                        EnabledButtonWhenDoTransaction(False)
                        Exit Sub
                    End If
                Catch ex As Exception
                    blnSuccess = False
                    lblStatus.Text = "Chứng từ đã được hạch toán nhưng chưa chuyển được số liệu sang thanh toán song phương"
                    logger.Error(ex.Message + ex.StackTrace)
                    Exit Sub
                End Try
            End If
            LoadDSCT(ddlTrangThai.SelectedValue)
        End If
    End Sub
    Private Sub ShowHideButtons(ByVal strTrangThai As String)
        Select Case strTrangThai
            Case "02"
                'lblStatus.Text = "Chứng từ chờ kiểm soát"
                cmdKS.Enabled = True
                cmdChuyenTra.Enabled = True
                cmdChuyenTTSP.Enabled = False
            Case "03"
                lblStatus.Text = "Hạch toán thành công"
                cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdChuyenTTSP.Enabled = False
            Case "08"
                'lblStatus.Text = "Chứng từ gửi thanh toán song phương lỗi"
                cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdChuyenTTSP.Enabled = True
            Case "09"
                'lblStatus.Text = "Thành công"
                cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdChuyenTTSP.Enabled = False
            Case "04"
                'lblStatus.Text = "Từ chối - chuyển trả"
                cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdChuyenTTSP.Enabled = False
            Case "05"
                'lblStatus.Text = "Chứng từ đã hủy"
                cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdChuyenTTSP.Enabled = False
        End Select

    End Sub
    Private Sub EnabledButtonWhenDoTransaction(ByVal blnEnabled As Boolean)
        cmdKS.Enabled = blnEnabled
        cmdChuyenTra.Enabled = blnEnabled
    End Sub
    Private Sub LoadCTChiTietAll(ByVal sSOCT As String)
        Try
            Dim v_strTT As String = String.Empty
            Dim v_strTTSP As String = String.Empty
            Dim v_strLoaiThue As String = String.Empty
            Dim v_strPhuongThucTT As String = String.Empty
            Dim v_str_Ma_NNT As String = String.Empty
            Dim v_strSO_CT As String = sSOCT
            Dim vStrProduct As String = ""
            'Load chứng từ header
            Dim dsCT As New DataSet

            Dim objCTU As Business.BienLai.infoCTUBIENLAI = Nothing
            objCTU = Business.BienLai.buBienLai.GetCTUDetailCT(sSOCT)
            hdfMACN.Value = objCTU.MA_CN
            txtSoCTU.Text = objCTU.SO_CT
            txtNgayCTU.Text = objCTU.NGAY_CT
            txtSoRefTTSP.Text = objCTU.REF_CORE_TTSP
            txtSoBL.Text = objCTU.SOBL
            txtKH_BL.Text = objCTU.KYHIEU_BL
            txtSHKB.Text = objCTU.SHKB
            txtTenKB.Text = objCTU.TEN_KB
            txtCQQD.Text = objCTU.CQQD
            txtTenCQQD.Text = objCTU.TEN_CQQD
            txtMaCQThu.Text = objCTU.MA_CQTHU
            txtMA_DBHC.Text = objCTU.MA_DBHC
            txtMA_LOAIHINH.Text = objCTU.MA_LHTHU
            txtTEN_LOAIHINH.Text = objCTU.TEN_LHTHU
            txtTK_NSNN.Text = objCTU.TK_NS
            txtMA_CHUONG.Text = objCTU.MA_CHUONG
            txtMA_TMUC.Text = objCTU.MA_NDKT
            txtNOIDUNG.Text = objCTU.NOI_DUNG
            txtLoaiBienLai.SelectedValue = objCTU.LOAIBIENLAI
            txtMaNNT.Text = objCTU.MA_NNTHUE
            txtTenNNT.Text = objCTU.TEN_NNTHUE
            'GIAYTO_NNTIEN
            txtMaNNTien.Text = objCTU.GIAYTO_NNTIEN
            txtTenNNTien.Text = objCTU.TEN_NNTIEN
            txtDIACHI.Text = objCTU.DC_NNTIEN
            txtSO_QD.Text = objCTU.SO_QD
            txtNGAY_QD.Text = objCTU.NGAY_QD
            txtTongTien.Text = VBOracleLib.Globals.Format_Number(objCTU.TTIEN, ".")
            txtMA_NT.Text = objCTU.MA_NT
            txtTTIEN_VPHC.Text = VBOracleLib.Globals.Format_Number(objCTU.TTIEN_VPHC, ".")
            txtLYDO.Text = objCTU.LY_DO
            txtTTIEN_CHAMNOP.Text = VBOracleLib.Globals.Format_Number(objCTU.TTIEN_NOP_CHAM, ".")
            txtLYDO_CHAMNOP.Text = objCTU.LY_DO_NOP_CHAM
            txtGHICHU.Text = objCTU.DIEN_GIAI
            ddlMaHTTT.SelectedValue = objCTU.HINH_THUC

            If objCTU.HINH_THUC.Equals("01") Then

                txtTK_KH_NH.Text = objCTU.TK_KH_NH
                txtTenTK_KH_NH.Text = objCTU.TEN_KH_NH
                'txtTKGL.Text = objCTU.TTIEN_VPHC
            Else
                'txtTenTKGL.Text = objCTU.TEN_KH_NH
                txtTK_KH_NH.Text = objCTU.TK_KH_NH
                txtTenTK_KH_NH.Text = " "
                'txtTKGL.Text = objCTU.TK_KH_NH
            End If
            txtNGAY_KH_NH.Text = objCTU.NGAY_CT
            txtSoRefTTSP.Text = objCTU.SO_CT_NH
            txtLYDO_TC.Text = objCTU.LY_DO_TUCHOI

            'hdfSoRefCore.Text = objCTU.REF_CORE_TTSP
            'hdfSo_CT.Text = objCTU.SO_CT
            txtSoBL.Text = objCTU.SOBL
            txtCharge.Text = VBOracleLib.Globals.Format_Number(objCTU.PHI_GD, ".")
            txtVAT.Text = VBOracleLib.Globals.Format_Number(objCTU.PHI_VAT, ".")

            txtTTienx.Text = VBOracleLib.Globals.Format_Number(objCTU.TTIEN, ".")

            txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number((Decimal.Parse(objCTU.TTIEN) + Decimal.Parse(objCTU.PHI_GD) + Decimal.Parse(objCTU.PHI_VAT)).ToString, ".")



            Dim MA_NV As String = String.Empty
            Dim TRANG_THAI As String = String.Empty
            Dim MA_CN_USER As String = String.Empty

            MA_CN_USER = objCTU.MA_CN
            TRANG_THAI = objCTU.TRANG_THAI
            MA_NV = objCTU.MA_NV
            hdfSo_CT.Value = objCTU.SO_CT
            ' hdfSoRefCore.Value = objCTU.REF_CORE_TTSP
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

            If Not ex.InnerException Is Nothing Then
                sbErrMsg = New StringBuilder()

                sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ ")
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.Message)
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.StackTrace)

                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)
            End If

            'Throw ex
        Finally
            'Catch ex As Exception
            '    logger.Error(ex.StackTrace)
        End Try
    End Sub
    Protected Sub btnSelectCT_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnSelectCT As LinkButton = CType(sender, LinkButton)
            Dim v_strRetArgument As String() = btnSelectCT.CommandArgument.ToString.Split(",")
            If (v_strRetArgument.Length > 3) Then
                Dim v_strKyHieu_CT As String = v_strRetArgument(0)
                Dim v_strNgay_KB As String = v_strRetArgument(1)
                Dim v_strSo_CT As String = v_strRetArgument(2)
                Dim v_strTrangThai_CT As String = v_strRetArgument(3)
                Dim v_strTTien As String = v_strRetArgument(4)
                Dim v_strMa_NV As String = v_strRetArgument(5)
                Dim v_strSHKB As String = v_strRetArgument(6)
                'Dim v_strNgayKB As String = v_strRetArgument(7)

                Dim v_strTenDN As String = v_strRetArgument(7)
                Dim v_stMaKS As String = v_strRetArgument(8)


                'Load thông tin chi tiết khi người dùng chọn 1 CT cụ thể
                If ((Not v_strSHKB Is Nothing) And (Not v_strMa_NV Is Nothing) _
                      And (Not v_strSo_CT Is Nothing)) Then

                    LoadCTChiTietAll(v_strSo_CT)

                    'Dat lai gia tri
                    hdfSHKB.Value = v_strSHKB
                    hdfNgay_KB.Value = v_strNgay_KB
                    hdfSo_CT.Value = v_strSo_CT
                    hdfKyHieu_CT.Value = v_strKyHieu_CT
                    hdfMa_NV.Value = v_strMa_NV
                    hdfTrangThai_CT.Value = v_strTrangThai_CT


                End If
                ShowHideButtons(v_strTrangThai_CT)
                'lbSoCT.Text = "Số CT: " & v_strSo_CT
                'lbSoCT.Visible = True

                'lbSoFT.Text = "Số FT: " & v_strSO_CT_NH
                'lbSoFT.Visible = True
            End If
        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub
    Protected Sub ddlTrangThai_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTrangThai.SelectedIndexChanged
        LoadDSCT(ddlTrangThai.SelectedValue)
        'ClearCTHeader()
        'CreateDumpChiTietCTGrid()
        'ShowHideCTRow(False)
    End Sub
    Protected Sub grdDSCT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdDSCT.PageIndexChanging
        Dim v_ds As DataSet = GetDSCT(ddlTrangThai.SelectedValue)
        grdDSCT.PageIndex = e.NewPageIndex
        grdDSCT.DataSource = v_ds
        grdDSCT.DataBind()
    End Sub
    'Protected Sub ddlMacker_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMacker.SelectedIndexChanged

    '    LoadDSCT(ddlTrangThai.SelectedValue)
    '    'ClearCTHeader()
    '    'CreateDumpChiTietCTGrid()
    '    'ShowHideCTRow(False)

    'End Sub

    Protected Sub cmdRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefresh.Click
        'LogApp.AddDebug("cmdRefresh_Click", "KIEM SOAT CHUNG TU HQ: Lam moi danh sach chung tu")
        LoadDSCT(ddlTrangThai.SelectedValue)
    End Sub
End Class
