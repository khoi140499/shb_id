﻿<%@ Page Title="" Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmLapBienLaiGiaoThong.aspx.vb" Inherits="pages_BienLai_frmLapBienLaiGiaoThong" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../../Plugins/JQuery/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="../../Plugins/JQuery/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../../javascript/DatePicker/date.js" type="text/javascript"></script>

    <script src="../../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <link href="../../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <link href="../../css/dialog.css" rel="stylesheet" />
    <script src="../../javascript/accounting.min.js" type="text/jscript"></script>

    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function () {
            document.querySelector('#txtCQQD').onclick = function () {
                document.getElementById("txtSHKB").value = '';
                document.getElementById("txtTenKB").value = '';
                document.getElementById("txtTenCQQD").value = '';
                document.getElementById("txtMaCQThu").value = '';
                document.getElementById("txtMA_DBHC").value = '';
                document.getElementById("txtTK_NSNN").value = '';
                document.getElementById("txtMA_CHUONG").value = '';
                document.getElementById("txtMA_TMUC").value = '';
                document.getElementById("txtNOIDUNG").value = '';
            }
            document.querySelector('#txtMA_LOAIHINH').onclick = function () {
                
                document.getElementById("txtSHKB").value = '';
                document.getElementById("txtTenKB").value = '';
                document.getElementById("txtCQQD").value = '';
                document.getElementById("txtTenCQQD").value = '';
                   document.getElementById("txtMaCQThu").value = '';
                   document.getElementById("txtMA_DBHC").value = '';
                   document.getElementById("txtTK_NSNN").value = '';
                   document.getElementById("txtMA_CHUONG").value = '';
                   document.getElementById("txtMA_TMUC").value = '';
                   document.getElementById("txtNOIDUNG").value = '';
            }
            document.querySelector('#txtSHKB').onclick = function () {
                document.getElementById("txtSHKB").value = '';
                document.getElementById("txtTenKB").value = '';
           
            }
           }, false)
     </script>

    <script type="text/jscript">

        //function clear() {
        //    document.getElementById("txtMaCQThu").value = '';
        //    document.getElementById("txtMA_DBHC").value = '';
        //    document.getElementById("txtTK_NSNN").value = '';
        //    document.getElementById("txtMA_CHUONG").value = '';
        //    document.getElementById("txtMA_TMUC").value = '';
        //    document.getElementById("txtNOIDUNG").value = '';
        //  }
        function getSoTK_TruyVanCore() {
            //jsGetTK_KH_NH(); jsGetTenTK_KH_NH();
            var pttt = $("#ddlMaHTTT option:selected").val();
            //console.log("PT_TT(CORE): " + pttt);
            if (pttt == "05") {
                PageMethods.getSoTK_TruyVanCore(document.getElementById("ddlMaHTTT").value, getSoTK_TruyVanCore_Complete, getSoTK_TruyVanCore_Error);
            }
            else {
                $get('txtTKGL').disabled = false;
            }
        }

        function getSoTK_TruyVanCore_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            if (result.length > 0) {
                $get('txtTKGL').value = result;
                $get('txtTKGL').disabled = true;
                $get('txtTenTKGL').value = '';
                //$get('txtSoDu_KH_NH').value = '';

            } else {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản tiền mặt ngân hàng" + error.get_message();
            }
        }
        function getSoTK_TruyVanCore_Error(error, userContext, methodName) {
            if (error !== null) {
                jsShowHideRowProgress(false);
                $get('txtTenTKGL').value = '';
                //$get('txtSoDu_KH_NH').value = '';
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản tiền mặt ngân hàng" + error.get_message();
            }
        }

        function checkTKGL() {
            for (var i = 0; i < arrTKGL.length; i++) {
                var arr = arrTKGL[i].split(';');
                if (arr[0] == $get('txtTKGL').value) {
                    return true;
                }
            }
            return false;
        }

        function ShowLov(strType) {



            if (strType == "SHKB") return ShowDMKB('KhoBac', 'txtSHKB', 'txtTenKB', 'txtSHKB');
            if (strType == "CQQD") return ShowDMKB('CQQD1', 'txtCQQD', 'txtTenCQQD', 'txtMA_LOAIHINH');
  
            if (strType == "LHTHU") return ShowDMKB('LHTHUBIENLAI', 'txtMA_LOAIHINH', 'txtTEN_LOAIHINH', 'txtMaNNT');




        }
        function jsIn_CT(isBS) {
            var width = screen.availWidth - 100;
            var height = screen.availHeight - 10;
            var left = 0;
            var top = 0;
            var params = 'width=' + width + ', height=' + height;
            params += ', top=' + top + ', left=' + left;
            params += ', directories=no';
            params += ', location=no';
            params += ', menubar=yes';
            params += ', resizable=no';
            params += ', scrollbars=yes';
            params += ', status=no';
            params += ', toolbar=no';

            var strSHKB, strSoCT, strSo_BT, MaNV, ngayLV;
            strSoCT = $('#txtSoCTU').val();

            window.open("../../pages/BaoCao/frmInBL.ashx?so_ct=" + strSoCT, "", params);
        }
        function jsSHKB_lostFocus(strSHKB) {

            //var strSHKB1 = $('#txtSHKB').val();
            //if (strSHKB1.length <= 0) {
            //    //$('#txtSHKB').focus();
            //   return
            //} 
            //$('#txtSHKB').focus();
            PageMethods.GetTen_SHKB(strSHKB, LoadDM_KB_Complete, LoadDM_KB_Error);
            
           
                //$('#txtTenKB').focus();
        }

        //function jsSHKB_lostFocus1(strSHKB) {

        //    //var strSHKB1 = $('#txtSHKB').val();
        //    //if (strSHKB1.length <= 0) {
        //    //    //$('#txtSHKB').focus();
        //    //   return
        //    //} 
        //    //$('#txtSHKB').focus();
        //    PageMethods.GetTen_SHKB(strSHKB, LoadDM_KB_Complete, LoadDM_KB_Error);
        //    $('#txtTenKB').focus();
        //}

        //function jsSHKB_lostFocus() {

        //    var strSHKB = $('#txtSHKB').val();
        //    //if (strSHKB1.length <= 0) {
        //    //    return
        //    //} else {
        //        PageMethods.GetTen_SHKB(strSHKB, LoadDM_KB_Complete, LoadDM_KB_Error);
        //    //}
        //}


        function LoadDM_KB_Complete(pData) {
            if (pData.length > 0) {
                $('#txtTenKB').val(pData);
                //clear cac  thong tin khac
                //$('#txtSHKB').focus();
                return;
            }

           
            //$('#txtSHKB').focus();
        }
        function LoadDM_KB_Error(pData) {
            alert('Mã kho bạc không đúng')
        }
        //load danh muc co quan quyet dinh
        function jsCQQD_lostFocus() {
     
            var strCQQD = $('#txtCQQD').val();
           
            if (strCQQD.length <= 0)
                //$('#txtTenCQQD').focus();
                return;

            //var strTenCQQD = $('#txtTenCQQD').val();
            //if (strTenCQQD.length > 0) {

            //    //  
            //    $('#txtCQQD').focus();
            //    return
            //}
       

            var strMA_LOAIHINH = "";
            if (document.getElementById('txtMA_LOAIHINH').value.length > 0) {
                strMA_LOAIHINH = document.getElementById('txtMA_LOAIHINH').value;
            }

            var strSHKB = $('#txtSHKB').val();

            PageMethods.GetDM_CQQD(strCQQD, strMA_LOAIHINH,strSHKB, LoadDM_CQQD_Complete, LoadDM_CQQD_Error);
 
        }
        function LoadDM_CQQD_Complete(pData) {
            var strCQQD = $('#txtCQQD').val();
            var strTenCQQD = ' ';
            if (pData.length > 0) {
                var strArr = pData.split('#');
                //if (strCQQD.length < 0) {
                $('#txtTK_NSNN').val(strArr[0]);
                $('#txtMA_CHUONG').val(strArr[1]);
                $('#txtMA_TMUC').val(strArr[2]);
                $('#txtMA_DBHC').val(strArr[3]);
                $('#txtMaCQThu').val(strArr[4]);
                $('#txtNOIDUNG').val(strArr[5]);
                $('#txtMA_LOAIHINH').val(strArr[6]);
                $('#txtSHKB').val(strArr[7]);
            //}

                jsComputeDIENGIAI();
                jsGetTenCQQD();

                //jsshkb_lostfocus($('#txtshkb').val());
                jsSHKB_lostFocus($('#txtSHKB').val());
                //$('#txtSHKB').focus();
                //$('#txtTenCQQD').focus();
                //var strTenSHKB1 = $('#txtTenKB').val();
                //if (strTenSHKB1.length > 0) {

                //    //  
                //    $('#txtSHKB').clearQueue();
                   
                //}
              
                return;
            }
            jsComputeDIENGIAI();
            //alert('Mã Loại hình thu không đúng');
            //alert('Mã Cơ quan quyết định  không đúng');
            //$('#txtMA_LOAIHINH').focus();
            alert('Mã Cơ quan quyết định không đúng')
        
            $('#txtTenCQQD').focus();
       
            //$('#txtCQQD').clearQueue();

        }
        function LoadDM_CQQD_Error(pData) {
            alert('Mã Cơ quan quyết định  không đúng');
        }
        //load loai hinh thu
        function fnc_LoadLHTHU() {
            var strSHKB = $('#txtSHKB').val();
            var strCQQD = $('#txtCQQD').val();
            if (strCQQD.length <= 0)
                return;
            if (strSHKB.length <= 0)
                return;

            PageMethods.jsLoadLHTHU(strSHKB, strCQQD, fnc_LoadLHTHU_Complete, fnc_LoadLHTHU_Error);


        }

        //load danh muc co quan quyet dinh
        function jsGetTenCQQD() {
            var strSHKB = $('#txtSHKB').val();
            var strMa_LH = $('#txtMA_LOAIHINH').val();
            var strCQQD = $('#txtCQQD').val();
            if (strCQQD.length <= 0)
           
                return;
    
            PageMethods.GetTen_CQQD(strCQQD, LoadTen_CQQD_Complete, LoadTen_CQQD_Error);
     
            //$('#txtTenCQQD').focus();
           
        }
        function LoadTen_CQQD_Complete(pData) {
            if (pData.length > 0) {
                $('#txtTenCQQD').val(pData);
                //fnc_LoadLHTHU();
                //clear cac  thong tin khac
               
                return;
                //} else {

                alert('Mã Cơ quan quyết định  không đúng');
                $('#txtTenCQQD').focus();
                //}
            }
        }
        function LoadTen_CQQD_Error(pData) {
            alert('Mã Cơ quan quyết định  không đúng');
        }

        function fnc_LoadLHTHU_Complete(pdata) {
            if (pdata.length > 0) {
                var strArr = pdata.split('#');
                var select = document.getElementById('txtMA_LOAIHINH');
                $('#txtMA_LOAIHINH').find('option').remove();
                for (var x = 0; x < strArr.length; x++) {
                    var opt = document.createElement('option');
                    opt.value = strArr[x].split('|')[0];
                    opt.innerHTML = strArr[x].split('|')[1];
                    select.appendChild(opt);
                    
                }
              
            }
          
        }
        function fnc_LoadLHTHU_Error(pData) {
            alert('Error load Danh muc KB');
        }

        //load cac thong tin  map  CQQD va Loai hinh
        function jsLoadMAP() {
            var strSHKB = $('#txtSHKB').val();
            var strCQQD = $('#txtCQQD').val();
            var strMALH = $('#txtMA_LOAIHINH').val();
            if (strCQQD.length <= 0)
                return;
            if (strMALH.length <= 0)
                return;
            //if (strSHKB.length <= 0) {
            //    alert('Chưa chọn kho bạc');
            //    $('#txtSHKB').focus();
            //}
            PageMethods.jsLOADMAPCQQD_LH(strSHKB, strCQQD, strMALH, jsLoadMAP_Complete, jsLoadMAP_Error);
          
        }
        function jsLoadMAP_Complete(pData) {
            if (pData.length > 0) {
                var strArr = pData.split('#');
                $('#txtTK_NSNN').val(strArr[0]);
                $('#txtMA_CHUONG').val(strArr[1]);
                $('#txtMA_TMUC').val(strArr[2]);
                $('#txtMA_DBHC').val(strArr[3]);
                $('#txtMaCQThu').val(strArr[4]);
                $('#txtNOIDUNG').val(strArr[5]);

                jsComputeDIENGIAI();
                //$('#txtCQQD').focus();
                return;
            }
            jsComputeDIENGIAI();
            alert('Mã Loại hình thu không đúng');
            //$('#txtCQQD').focus();
            //$('#txtCQQD').focus();
        }
        function jsLoadMAP_Error(pData) {
            alert('Mã Loại hình thu không đúng');
        }

    </script>
    <script type="text/javascript">
        function jsGetHDRObject2(sAction) {
            var objHdrHq = {};
            //console.log("1txtTongTien: " + $('#txtTongTien').val() + "---1tFee: " + $('#txtCharge').val() + "  -- tVAT: " + $('#txtVAT').val());
            var tongTien = $('#txtTongTien').val().replaceAll('.', '');
            var tFee = $('#txtCharge').val().replaceAll('.', '');
            var tVAT = $('#txtVAT').val().replaceAll('.', '');

            //console.log("2txtTongTien: " + tongTien + "---1tFee: " + tFee + "  -- tVAT: " + tVAT);
            var maTienTe = ($('#txtMA_NT').val());
            var tongTienNT = 0;
            if (maTienTe != 'VND')
                tongTienNT = accounting.unformat($('#txtTongTienNT').val(), ".");
            else
                tongTienNT = accounting.unformat($('#txtTongTienNT').val());

            // objHdrHq.SHKB = $("#txtSHKB").val();
            objHdrHq.SO_CT = $("#txtSoCTU").val();
            objHdrHq.NGAY_CT = $("#txtNgayCTU").val();
            objHdrHq.NGAY_CT = $("#txtNGAY_KH_NH").val();
            objHdrHq.SO_CT_NH = $("#txtSoRefTTSP").val();
            //objHdrHq.REF_CORE_TTSP = $("#txtSoRefTTSP").val();
            objHdrHq.SHKB = $("#txtSHKB").val();
            objHdrHq.TEN_KB = $("#txtTenKB").val();
            objHdrHq.CQQD = $("#txtCQQD").val();
            objHdrHq.TEN_CQQD = $("#txtTenCQQD").val();
            objHdrHq.MA_LHTHU = $("#txtMA_LOAIHINH").val();
            objHdrHq.TEN_LHTHU = $("#txtMA_LOAIHINH option:selected").text();// $("#txtMA_LOAIHINH").val();
            objHdrHq.TK_NS = $("#txtTK_NSNN").val();
            objHdrHq.MA_CHUONG = $("#txtMA_CHUONG").val();
            objHdrHq.MA_NDKT = $("#txtMA_TMUC").val();
            objHdrHq.NOI_DUNG = $("#txtNOIDUNG").val();
            objHdrHq.TTIEN = $("#txtTTIEN").val().replaceAll('.', '');
            objHdrHq.TTIEN_VPHC = $("#txtTTIEN_VPHC").val().replaceAll('.', '');
            objHdrHq.LY_DO = $("#txtLYDO").val();
            objHdrHq.TTIEN_NOP_CHAM = $("#txtTTIEN_CHAMNOP").val().replaceAll('.', '');
            if (objHdrHq.TTIEN_NOP_CHAM.length <= 0)
                objHdrHq.TTIEN_NOP_CHAM = '0';
            objHdrHq.LY_DO_NOP_CHAM = $("#txtLYDO_CHAMNOP").val();
            objHdrHq.SO_QD = $("#txtSO_QD").val();
            objHdrHq.NGAY_QD = $("#txtNGAY_QD").val();
            objHdrHq.KYHIEU_CT = "";
            objHdrHq.HINH_THUC = $("#ddlMaHTTT").val();
            objHdrHq.MA_DBHC = $("#txtMA_DBHC").val();
            objHdrHq.LOAIBIENLAI = $("#txtLoaiBienLai").val();
            //cho KS
            objHdrHq.TRANG_THAI = "02";
            objHdrHq.MA_NV = "";
            objHdrHq.MA_CN = "";
            objHdrHq.NGAY_HT = "";
            objHdrHq.MA_KS = "";
            objHdrHq.LY_DO_TUCHOI = "";
            objHdrHq.DIEN_GIAI = $("#txtGHICHU").val();
            objHdrHq.REMAX = "";
            objHdrHq.MA_NNTHUE = $("#txtMaNNT").val();
            objHdrHq.TEN_NNTHUE = $("#txtTenNNT").val();
            objHdrHq.TEN_NNTIEN = $("#txtTenNNTien").val();
            objHdrHq.DC_NNTIEN = $("#txtDIACHI").val();
            objHdrHq.GIAYTO_NNTIEN = $("#txtMaNNTien").val();
            objHdrHq.MA_CQTHU = $("#txtMaCQThu").val();
            objHdrHq.NGAY_KS = "";
            objHdrHq.MA_NV_TC = "";
            objHdrHq.NGAY_TC = "";
            objHdrHq.REF_NO = "";
            objHdrHq.RM_REF_NO = "";
            objHdrHq.SO_CT_NH = $("#txtSoRefTTSP").val();
            //objHdrHq.TK_KH_NH=
            //  objHdrHq.TEN_KH_NH
            if (objHdrHq.HINH_THUC == "01") {
                objHdrHq.TK_KH_NH = $('#txtTK_KH_NH').val();
                objHdrHq.TEN_KH_NH = $('#txtTenTK_KH_NH').val();
            } else {
                objHdrHq.TEN_KH_NH = $('#txtTenTKGL').val();
                objHdrHq.TK_KH_NH = $('#txtTKGL').val();
            }
            objHdrHq.TK_KH_NH = objHdrHq.TK_KH_NH.replace(/[-]/g, '');
            objHdrHq.MA_NH_A = "";
            objHdrHq.TEN_NH_A = "";
            objHdrHq.MA_NH_B = "";
            objHdrHq.TEN_NH_B = "";
            objHdrHq.MA_NH_TT = "";
            objHdrHq.TEN_NH_TT = "";
            objHdrHq.TTHAI_TTSP = "0"
            objHdrHq.PT_TINHPHI = "";
            //var tongTien = accounting.unformat($('#txtTongTien').val());
            //var tFee = accounting.unformat($('#txtCharge').val());
            //var tVAT = accounting.unformat($('#txtVAT').val());

            //console.log("tFee: " + tFee + "  -- tVAT: " + tVAT);
            objHdrHq.PHI_GD = tFee;
            objHdrHq.PHI_VAT = tVAT;
            objHdrHq.ACTION = sAction;
            objHdrHq.MA_NT = $('#txtMA_NT').val();

            if (objHdrHq.TTIEN.length <= 0) objHdrHq.TTIEN = '0';
            if (objHdrHq.TTIEN_VPHC.length <= 0) objHdrHq.TTIEN_VPHC = '0';
            if (objHdrHq.TTIEN_NOP_CHAM.length <= 0) objHdrHq.TTIEN_NOP_CHAM = '0';
            if (objHdrHq.PHI_GD.length <= 0) objHdrHq.PHI_GD = '0';
            if (objHdrHq.PHI_VAT.length <= 0) objHdrHq.PHI_VAT = '0';
            if (sAction == '2') {
                if (objHdrHq.SO_CT.length <= 0) {
                    objHdrHq.ACTION = "GHIKS";
                } else {
                    objHdrHq.ACTION = "UPDATE_KS";
                }
            }
            objHdrHq.CODE_PHI = $('#txtCodePhi').val();
            return objHdrHq;
        }
        function jsGhi_CTU(sAct) {
            var objHDR = jsGetHDRObject2(sAct);



            if (jsValidFormData2(objHDR)) {
                var checksodu = $("#ddlMaHTTT").val();
                if (checksodu != "05") {
                    var TTienTT = accounting.unformat($("#txtTongTrichNo").val().replaceAll('.', ''));
                    var SoDuKD = accounting.unformat($("#txtSoDu_KH_NH").val().replaceAll(',', ''));

                    if (TTienTT > SoDuKD) {
                        alert("Tổng tiền vượt quá số dư hiện tại, Vui lòng kiểm tra lại!");
                        return;
                    }
                }

                //So sánh tổng tiền vs số dư
                var TTienTT1 = accounting.unformat($("#txtTTIEN").val().replaceAll('.', ''));
                var SoDuKD1 = accounting.unformat($("#txtSoDu_KH_NH").val().replaceAll(',', ''));
                if (checksodu != "05") {
                    if (TTienTT1 > SoDuKD1) {
                        alert("Tổng tiền vượt quá số dư hiện tại, Vui lòng kiểm tra lại!");
                        return;
                    }
                }
                jsEnableDisableControlButton('0');
                $.ajax({
                    type: 'POST',
                    url: 'frmLapBienLaiGiaoThong.aspx/Ghi_CTu2',
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    data: JSON.stringify({
                        ctuHdr: objHDR
                    }),
                    success: function (reponse) {
                        var sRet = reponse.d.split(';');
                        if (sRet && sRet[0] == 'Success') {
                            var sOut = '';
                            if ((sRet[2]) && (sRet[2].indexOf('/') > 0) && (sRet[2].split('/').length > 0)) {
                                sOut = sRet[2].split('/');
                            }
                            if ((sOut instanceof Array) && (typeof sOut[0] !== 'undefined' && sOut[0] !== null) && (typeof sOut[1] !== 'undefined' && sOut[1] !== null)) {
                                alert('Hoàn thiện & gửi kiểm soát thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0]);
                            } else {
                                alert('Hoàn thiện & gửi kiểm soát thành công!');
                            }
                            //re-load lai chung tu len form (trong này đã có set hdfDescCTu)
                            if ((sOut) && (sOut.length > 1)) {
                                jsLoadCTList();
                                jsGetCTU(sOut[1]);

                            }
                            //} else {
                            //    jsGetCTU(null);
                            //}
                        } else {
                            alert('Cập nhật & gửi kiểm soát không thành công: ' + sRet[1]);
                            if ($('#txtSoCTU').val().length >= 0)
                                jsEnableDisableControlButton('2');
                            else
                                jsEnableDisableControlButton('3');


                        }
                        //   $('#hdfIsEdit').val(true);
                        // jsLoadCTList();
                        // resetTextBox_Mst_Ndk_Stk();
                    },
                    error: function (request, status, error) {
                        var err;
                        err = JSON.parse(request.responseText);
                        if (typeof err !== 'undefined' && err !== null) {
                            if (err.Message.indexOf(';') > -1) {
                                jsCheckUserSession(err.Message);
                            }
                            alert(err.Message);
                            if ($('#txtSoCTU').val().length >= 0)
                                jsEnableDisableControlButton('2');
                            else
                                jsEnableDisableControlButton('3');
                        }
                    }
                });
            }
        }

        function formatNumberVNĐ() {

            document.getElementById("txtTTIEN_VPHC").value = localeNumberFormat(document.getElementById("txtTTIEN_VPHC").value.replaceAll('.', ''), '.');
        }

        function formatNumberVNĐ1() {

            document.getElementById("txtTTIEN_CHAMNOP").value = localeNumberFormat(document.getElementById("txtTTIEN_CHAMNOP").value.replaceAll('.', ''), '.');
        }

        function jsValidFormData2(HDR) {
            if (HDR.SHKB.length != 4) {
                alert('Độ dài Kho bạc không  đúng!');
                $('#txtSHKB').focus();
                return false;
            }
            if (HDR.SHKB.length <= 0 || HDR.TEN_KB.length <= 0) {
                alert('Số hiệu KB không đúng!');
                $('#txtSHKB').focus();
                return false;
            }
            if (HDR.CQQD.length > 20) {
                alert('Độ dài Cơ quan quyết định vượt quá độ dài cho phép!');
                $('#txtCQQD').focus();
                return false;
            }
            if (HDR.CQQD.length <= 0 || HDR.TEN_CQQD.length <= 0) {
                alert('Cơ quan quyết định không đúng!');
                $('#txtCQQD').focus();
                return false;
            }
            if (HDR.TEN_CQQD.length > 200) {
                alert('Tên Cơ quan quyết định độ dài vượt quá 200 vui lòng kiểm tra lại danh mục!');
                $('#txtCQQD').focus();
                return false;
            }
            if (HDR.MA_CQTHU.length <= 0 || HDR.MA_CQTHU.length > 7) {
                alert('Mã CQThu không đúng hoặc độ dài vượt quá 7 ký tự vui lòng kiểm tra lại danh mục!');
                $('#txtCQQD').focus();
                return false;
            }
            if (HDR.MA_DBHC.length <= 0 || HDR.MA_DBHC.length > 5) {
                alert('Mã địa bàn hành chính không đúng hoặc độ dài vượt quá 5 ký tự vui lòng kiểm tra lại danh mục!');
                $('#txtCQQD').focus();
                return false;
            }
            if (HDR.MA_LHTHU.length <= 0 || HDR.MA_LHTHU.length > 3) {
                alert('Mã loại hình thu không đúng hoặc độ dài vượt quá 5 ký tự vui lòng kiểm tra lại danh mục!');
                $('#txtMA_LOAIHINH').focus();
                return false;
            }
            if (HDR.TEN_LHTHU.length <= 0 || HDR.TEN_LHTHU.length > 200) {
                alert('Mã loại hình thu không đúng hoặc Tên có độ dài vượt quá 200 ký tự vui lòng kiểm tra lại danh mục!');
                $('#txtMA_LOAIHINH').focus();
                return false;
            }
            if (HDR.TK_NS.length != 4) {
                alert('Tài khoản ngân sách nhà nước không đúng hoặc độ dài vượt quá 4 ký tự vui lòng kiểm tra lại danh mục!');
                $('#txtMA_LOAIHINH').focus();
                return false;
            }
            if (HDR.MA_CHUONG.length != 3) {
                alert('Mã chương khoản ngân sách nhà nước không đúng hoặc độ dài vượt quá 4 ký tự vui lòng kiểm tra lại danh mục!');
                $('#txtMA_LOAIHINH').focus();
                return false;
            }
            if (HDR.MA_NDKT.length != 4) {
                alert('Mã mục ngân sách nhà nước không đúng hoặc độ dài vượt quá 4 ký tự vui lòng kiểm tra lại danh mục!');
                $('#txtMA_LOAIHINH').focus();
                return false;
            }
            if (HDR.MA_NNTHUE.length < 10 || HDR.MA_NNTHUE.length > 14) {
                alert('Mã người nộp thuế chưa được nhập hoặc độ dài không đúng!');
                $('#txtMaNNT').focus();
                return false;
            }
            if (HDR.TEN_NNTHUE.length <= 0 || HDR.TEN_NNTHUE.length > 200) {
                alert('Chưa có Tên người nộp thuế hoặc độ dài > 200 ký tự. Vui lòng kiểm tra lại!');
                $('#txtTenNNT').focus();
                return false;
            }
            if (HDR.GIAYTO_NNTIEN.length <= 0 || HDR.GIAYTO_NNTIEN.length > 20) {
                alert('Chưa có thông tin CMT  người nộp tiền hoặc độ dài > 20 ký tự. Vui lòng kiểm tra lại!');
                $('#txtMaNNTien').focus();
                return false;
            }
            if (HDR.TEN_NNTIEN.length <= 0 || HDR.TEN_NNTIEN.length > 200) {
                alert('Chưa có Tên người nộp thuế hoặc độ dài > 200 ký tự. Vui lòng kiểm tra lại!');
                $('#txtTenNNTien').focus();
                return false;
            }
            if (HDR.DC_NNTIEN.length <= 0 || HDR.DC_NNTIEN.length > 200) {
                alert('Chưa nhập địa chỉ người nộp tiền hoặc độ dài > 200 ký tự. Vui lòng kiểm tra lại!');
                $('#txtDIACHI').focus();
                return false;
            }
            if (HDR.SO_QD.length <= 0 || HDR.SO_QD.length > 30) {
                alert('Chưa nhập số quyết định hoặc độ dài > 30 ký tự. Vui lòng kiểm tra lại!');
                $('#txtSO_QD').focus();
                return false;
            }
            if (HDR.NGAY_QD.length <= 0 || HDR.NGAY_QD.length > 10) {
                alert('Chưa nhập ngày quyết định hoặc độ dài > 10 ký tự. Vui lòng kiểm tra lại!');
                $('#txtNGAY_QD').focus();
                return false;
            }
            //if (!isValidDate(HDR.NGAY_QD)) {
            //    alert('Chưa nhập ngày quyết định hoặc định dạng không đúng. Vui lòng kiểm tra lại!');
            //    $('#txtNGAY_QD').focus();
            //    return false;
            //}
            if (HDR.TTIEN.length <= 0 || HDR.TTIEN.length > 20) {
                alert('Chưa số tiền cần nộp hoặc độ dài > 20 ký tự, Vui lòng kiểm tra lại!');
                $('#txtTTIEN_VPHC').focus();
                return false;
            }
            try {
                var x = parseFloat(HDR.TTIEN);
            } catch (e) {
                alert('Chưa nhập số tiền cần nộp hoặc tổng số tiền sai định dạng, Vui lòng kiểm tra lại!');
                $('#txtTTIEN_VPHC').focus();
                return false;
            }
            if (HDR.MA_NT.length <= 0 || HDR.MA_NT.length > 3) {
                alert('Chưa nhập mã nguyên tệ hoặc độ dài > 3 ký tự. hệ thống sẽ load lại màn hình!');
                document.location.reload();
                return false;
            }
            if (HDR.TTIEN_VPHC.length <= 0 || HDR.TTIEN_VPHC.length > 20) {
                alert('Chưa nhập số tiền cần nộp hoặc độ dài > 20 ký tự, Vui lòng kiểm tra lại!');
                $('#txtTTIEN_VPHC').focus();
                return false;
            }
            try {
                var x = parseFloat(HDR.TTIEN_VPHC);
            } catch (e) {
                alert('Chưa nhập số tiền cần nộp hoặc số tiền sai định dạng, Vui lòng kiểm tra lại!');
                $('#txtTTIEN_VPHC').focus();
                return false;
            }
            if (HDR.LY_DO.length <= 0 || HDR.LY_DO.length > 200) {
                alert('Chưa nhập lý do nộp hoặc độ dài > 200 ký tự. Vui lòng kiểm tra lại!');
                $('#txtLYDO').focus();
                return false;
            }
            if (HDR.TTIEN_NOP_CHAM.length > 20) {
                alert('Chưa tiền nộp chậm có độ dài > 20 ký tự, Vui lòng kiểm tra lại!');
                $('#txtTTIEN_CHAMNOP').focus();
                return false;
            }
            if (HDR.TTIEN_NOP_CHAM.length > 0)
                try {
                    var x = parseFloat(HDR.TTIEN_NOP_CHAM);
                } catch (e) {
                    alert('Chưa số tiền cần nộp chậm sai định dạng, Vui lòng kiểm tra lại!');
                    $('#txtTTIEN_CHAMNOP').focus();
                    return false;
                }
            if (HDR.LY_DO_NOP_CHAM.length > 200) {
                alert('Chưa  lý do nộp chậm có độ dài > 200 ký tự. Vui lòng kiểm tra lại!');
                $('#txtLYDO_CHAMNOP').focus();
                return false;
            }
            if (HDR.DIEN_GIAI.length > 1000) {
                alert('Diễn giải có độ dài > 1000 ký tự. Vui lòng kiểm tra lại!');
                $('#txtGHICHU').focus();
                return false;
            }

            //if (HDR.HINH_THUC != '01' && HDR.HINH_THUC != '03') {
            //    alert('Hình thức thanh toán không đúng . Vui lòng kiểm tra lại!');
            //    $('#ddlMaHTTT').focus();
            //    return false;
            //}
            //if (HDR.HINH_THUC == "05") {
            //    if (HDR.TK_KH_NH.length <= 0 || HDR.TK_KH_NH.length > 21) {
            //        alert('Vui lòng nhập số tài khoản GL hoặc độ dài > 21 ký tự!');
            //        $('#txtTKGL').focus();
            //        return false;
            //    }

            if (HDR.HINH_THUC != '01' && HDR.HINH_THUC != '05') {
                alert('Hình thức thanh toán không đúng . Vui lòng kiểm tra lại!');
                $('#ddlMaHTTT').focus();
                return false;
            }
            if (HDR.HINH_THUC == "03") {
                if (HDR.TK_KH_NH.length <= 0 || HDR.TK_KH_NH.length > 21) {
                    alert('Vui lòng nhập số tài khoản GL hoặc độ dài > 21 ký tự!');
                    $('#txtTKGL').focus();
                    return false;
                }
                if (HDR.TEN_KH_NH.length <= 0 || HDR.TEN_KH_NH.length > 200) {
                    alert('Tài khoản GL không đúng hoặc Tên GL có độ dài >200 ký tự!');
                    $('#txtTKGL').focus();
                    return false;
                }

            } else if (HDR.HINH_THUC == "01") {
                if (HDR.TK_KH_NH.length <= 0 || HDR.TK_KH_NH.length > 21) {
                    alert('Vui lòng nhập số tài khoản trích nợ hoặc độ dài > 21 ký tự!');
                    $('#txtTK_KH_NH').focus();
                    return false;
                }
                if (HDR.TEN_KH_NH.length <= 0 || HDR.TEN_KH_NH.length > 200) {
                    alert('Tài khoản trích nợ không đúng hoặc Tên tài khoản có độ dài >200 ký tự!');
                    $('#txtTK_KH_NH').focus();
                    return false;
                }
            }
            var nguyenTeTaiKhoan = document.getElementById('txtDesc_SoDu_KH_NH').value;
            var nguyenTeThanhToan = HDR.MA_NT;
            var pttt = $("#ddlMaHTTT").val();
            if (pttt != "05") {
                if (nguyenTeTaiKhoan !== nguyenTeThanhToan) {
                    alert('Nguyên tệ thanh toán và nguyên tệ tài khoản không khớp');
                    return false;
                } else {
                    var total = '';
                    if (nguyenTeThanhToan !== 'VND') {
                        total = accounting.unformat($('#txtTongTienNT').val());
                    }
                    else {
                        total = accounting.unformat($('#txtTongTrichNo').val());//txtTongTienNT
                    }
                    var accBalance = accounting.unformat($('#txtSoDu_KH_NH').val());

                    if (total > accBalance) {
                        alert('Tổng tiền trích nợ đang lớn hơn số tiền của khách hàng. Vui lòng kiểm tra lại.');
                        return false;
                    }
                }
            } else {


            }

            return true;
        }

        function ShowHideControlByPT_TT() {
            var intPP_TT = document.getElementById("ddlMaHTTT").value;
            var rTK_KH_NH = document.getElementById("rowTK_KH_NH");
            var rSoDuNH = document.getElementById("rowSoDuNH");
            var rTKGL = document.getElementById("rowTKGL");
            //07/06/2011-Kienvt:Them thong tin ngan hang nhan,chuyen
            //var rNHChuyen = document.getElementById("rowNHChuyen");
            //var rNHNhan = document.getElementById("rowNHNhan");
            //var rNHTT = document.getElementById("rowNHTT");
            //jsCalTotal();

            if (intPP_TT == '01') {//"NH_TKTH_KB"){ :chuyen khoan
                rTK_KH_NH.style.display = '';
                rSoDuNH.style.display = '';
                rTKGL.style.display = 'none';
                //07/06/2011-Kienvt:Them thong tin ngan hang nhan,chuyen
                //rNHChuyen.style.display = '';
                //rNHNhan.style.display = '';
                //rNHTT.style.display = '';
                //$get('txtTK_KH_NH').focus();
            }

            if (intPP_TT == '05') {//"NH_TKTG"){ :trung gian
                rTK_KH_NH.style.display = 'none';
                rSoDuNH.style.display = 'none';
                rTKGL.style.display = '';
                //rNHChuyen.style.display = '';
                //rNHNhan.style.display = '';
                //rNHTT.style.display = '';
                //07/06/2011-Kienvt:Them thong tin ngan hang nhan,chuyen

            }
            //  jsEnabledButtonByBDS();
        }
        function jsHuy_CTU() {


            var sSoCTu, sLyDoHuy;
            sSoCTu = $('#txtSoCTU').val();
            sLyDoHuy = $('#txtLyDoHuy').val();
            if (typeof sSoCTu !== 'undefined' && sSoCTu !== null && sSoCTu !== '') {
                if (typeof sLyDoHuy == 'undefined' || sLyDoHuy == null || sLyDoHuy == '') {

                    alert('Vui lòng nhập vào lý do hủy.');
                    return;


                }
                jsEnableDisableControlButton('0');
                $.ajax({
                    type: 'POST',
                    url: 'frmLapBienLai.aspx/Huy_CTU',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({
                        sSoCT: sSoCTu,
                        sLyDo: sLyDoHuy || null
                    }),
                    success: function (reponse) {
                        var sRet = reponse.d.split(';');
                        if (typeof sRet !== 'undefined' && sRet !== null && typeof sRet[1] !== 'undefined' && sRet[1] !== '') {
                            var sRetSplit = sRet[1].split('/');
                            if (typeof sRetSplit !== 'undefined' && sRetSplit !== null && sRetSplit.length > 0) {
                                alert(sRet[0] + ' - Số CT: ' + sRetSplit[0] + ' - SHKB: ' + sRetSplit[1]);
                                jsGetCTU(sRetSplit[1]);
                            } else {
                                alert('Hủy chứng từ không thành công. Vui lòng liên hệ quản lý hệ thống');

                            }
                        } else {
                            alert('Hủy chứng từ không thành công. Vui lòng liên hệ quản lý hệ thống');

                        }

                        jsLoadCTList();
                    },
                    error: function (request, status, error) {
                        var err;
                        err = JSON.parse(request.responseText);
                        if (err) {
                            if (err.Message.indexOf(';') > -1) {
                                jsCheckUserSession(err.Message);
                            }
                            alert(err.Message);
                            jsEnableDisableControlButton('2');
                        }
                    }
                });
            } else {
                alert('Không xác định được chứng từ cần hủy');
            }

        }
        function jsEnableDisableControlButton(sType) {
            //danh cho trang thai: 03:08:09
            if (sType == '1') {
                $('#cmdThemMoi').prop("disabled", false);
                $('#cmdGhiKS').prop("disabled", true);
                $('#cmdInCT').prop("disabled", false);
                $('#cmdHuyCT').prop("disabled", true);
                $('#cmdTinhPhi').prop("disabled", true);
                return;
            }
            //trang thai 06
            if (sType == '2') {
                $('#cmdThemMoi').prop("disabled", false);
                $('#cmdGhiKS').prop("disabled", false);
                $('#cmdInCT').prop("disabled", false);
                $('#cmdHuyCT').prop("disabled", false);
                $('#cmdTinhPhi').prop("disabled", false);
                return;
            }
            if (sType == '3') {
                $('#cmdThemMoi').prop("disabled", false);
                $('#cmdGhiKS').prop("disabled", false);
                $('#cmdInCT').prop("disabled", false);
                $('#cmdHuyCT').prop("disabled", false);
                $('#cmdTinhPhi').prop("disabled", false);
                return;
            }
            //trang thai 07
            if (sType == '4') {
                $('#cmdThemMoi').prop("disabled", false);
                $('#cmdGhiKS').prop("disabled", true);
                $('#cmdInCT').prop("disabled", true);
                $('#cmdHuyCT').prop("disabled", true);
                $('#cmdTinhPhi').prop("disabled", true);
                return;
            }
            //trang thai 02 va 04
            if (sType == '5') {
                $('#cmdThemMoi').prop("disabled", false);
                $('#cmdGhiKS').prop("disabled", true);
                $('#cmdInCT').prop("disabled", false);
                $('#cmdHuyCT').prop("disabled", false);
                $('#cmdTinhPhi').prop("disabled", true);
                return;
            }

            $('#cmdGhiKS').prop("disabled", true);
            $('#cmdInCT').prop("disabled", true);
            $('#cmdHuyCT').prop("disabled", true);
            $('#cmdThemMoi').prop("disabled", true);
            $('#cmdTinhPhi').prop("disabled", true);
        }
    </script>
    <script type="text/javascript">
        function jsComputeTTIEN() {
            var vtienphat = $get('txtTTIEN_VPHC').value.replaceAll('.', '');
            var vTienCham = $get('txtTTIEN_CHAMNOP').value.replaceAll('.', '');
            if (vTienCham.length <= 0) vTienCham = '0';
            if (vtienphat.length <= 0) vtienphat = '0';
            $get('txtTTIEN').value = parseFloat(vtienphat) + parseFloat(vTienCham);
            jsFormatNumber('txtTTIEN');
        }
        function jsComputeDIENGIAI() {
            jsComputeTTIEN();
            var tenloaihinh = $("#txtMA_LOAIHINH option:selected").text();
            var strDiengiai = tenloaihinh + ';LH:' + $get('txtMA_LOAIHINH').value + ';CQ:' + $get('txtCQQD').value + ';KB:' + $get('txtSHKB').value + ';QD:' + $get('txtSO_QD').value;
            strDiengiai += ';TKNS:' + $get('txtTK_NSNN').value + ';C:' + $get('txtMA_CHUONG').value + ';M:' + $get('txtMA_TMUC').value + ';T:' + $get('txtTTIEN').value + ';NNT:' + $get('txtTenNNTien').value;

            $get('txtGHICHU').value = strDiengiai;
        }
        String.prototype.replaceAll = function (strTarget, strSubString) {
            var strText = this;
            var intIndexOfMatch = strText.indexOf(strTarget);
            while (intIndexOfMatch != -1) {
                strText = strText.replace(strTarget, strSubString)
                intIndexOfMatch = strText.indexOf(strTarget);
            }
            return (strText);
        }
        function checkGLBeforGet(tkGL) {
            for (var i = 0; i < arrGL.length; i++) {
                var arr = arrGL[i].split(';');
                if (arr[0] == tkGL) {
                    isGL = true;
                    glName = arr[1];
                    return true;
                }
            }
            return false;
        }
        function loadXMLString(txt) {
            if (window.DOMParser) {
                parser = new DOMParser();
                xmlDoc = parser.parseFromString(txt, "text/xml");
            }
            else // code for IE
            {
                xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = false;
                xmlDoc.loadXML(txt);
            }
            return xmlDoc;
        }
        function jsShowHideRowProgress(blnShow) {
            if (blnShow) {
                document.getElementById("divProgress").style.display = '';
            } else {
                document.getElementById("divProgress").style.display = 'none';
            }
        }
        function jsGetTK_KH_NH() {
            // mask($get('txtTK_KH_NH').value, $get('txtTK_KH_NH'), '3,6,9,17', '-');
            if ($get('txtTK_KH_NH').value.length > 0) {
                if (checkGLBeforGet($get('txtTK_KH_NH').value) == false) {
                    document.getElementById('divStatus').innerHTML = '';
                    var strAccType = '';
                    var intPP_TT = document.getElementById("ddlMaHTTT").selectedIndex;
                    //console.log("intPP_TT: " + intPP_TT);
                    //if (intPP_TT == 0)
                    if (intPP_TT == 3) {
                        strAccType = 'TG';
                    } else {
                        strAccType = 'CK';
                    }

                    jsShowHideDivProgress(true);
                    PageMethods.GetTK_KH_NH($get('txtTK_KH_NH').value.trim(), GetTK_NH_Complete, GetTK_NH_Error);
                }
                else {
                    document.getElementById('divStatus').innerHTML = 'Định dạng tài khoản khách hàng không hợp lệ.Hãy nhập lại';
                }
            }
        }

        function GetTK_NH_Complete(result, methodName) {
            checkSS(result);
            jsShowHideRowProgress(false);

            if (result.length > 0) {
                var xmlDoc = loadXMLString(result);
                var rootCTU_HDR = xmlDoc.getElementsByTagName('PARAMETER')[0];
                if (rootCTU_HDR != null) {

                    if (xmlDoc.getElementsByTagName("RETCODE")[0].childNodes[0].nodeValue == "00000") {
                        if (xmlDoc.getElementsByTagName("BANKACCOUNT")[0].childNodes != null) $get('txtTK_KH_NH').value = xmlDoc.getElementsByTagName("BANKACCOUNT")[0].childNodes[0].nodeValue;
                        if (xmlDoc.getElementsByTagName("ACY_AVL_BAL")[0].childNodes != null) $get('txtSoDu_KH_NH').value = xmlDoc.getElementsByTagName("ACY_AVL_BAL")[0].childNodes[0].nodeValue;
                        $('#txtSoDu_KH_NH').val(accounting.formatNumber($get('txtSoDu_KH_NH').value));
                        if (xmlDoc.getElementsByTagName("CRR_CY_CODE")[0].childNodes != null) $get('txtDesc_SoDu_KH_NH').value = xmlDoc.getElementsByTagName("CRR_CY_CODE")[0].childNodes[0].nodeValue;
                        // $('#txtDesc_SoDu_KH_NH').val()
                        //jsFormatNumber('txtSoDu_KH_NH');
                        if (rootCTU_HDR.getElementsByTagName("CRR_CY_CODE")[0].childNodes.length != 0) $get('hdLimitAmout').value = rootCTU_HDR.getElementsByTagName("CRR_CY_CODE")[0].childNodes[0].nodeValue;
                        else $get('hdLimitAmout').value = "0";
                    } else {
                        $get('txtTK_KH_NH').value = '';
                        $get('txtTenTK_KH_NH').value = '';
                        $get('txtSoDu_KH_NH').value = '';
                        document.getElementById('divStatus').innerHTML = rootCTU_HDR.getElementsByTagName("ERRCODE")[0].firstChild.nodeValue;
                    }
                }
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Có lỗi trong quá trình truy vấn số dư tài khoản.';
                $get('txtTenTK_KH_NH').value = '';
                $get('txtSoDu_KH_NH').value = '';
            }

        }

        function GetTK_NH_Error(error, userContext, methodName) {
            if (error !== null) {
                $get('txtTenTK_KH_NH').value = '';
                $get('txtSoDu_KH_NH').value = '';
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản ngân hàng" + error.get_message();
            }
        }

        function jsGetTenTK_KH_NH() {
            if ($get('txtTK_KH_NH').value.length > 0) {
                //console.log("1");
                document.getElementById('divStatus').innerHTML = '';
                var strAccType = '';
                var intPP_TT = document.getElementById("ddlMaHTTT").selectedIndex;
                //console.log(intPP_TT);
                //var ma_NT = document.getElementById("txtTenNT").value;
                //console.log(ma_NT);
                if (intPP_TT == 3) {
                    strAccType = 'TG';
                } else {
                    strAccType = 'CK';
                }
                // console.log("2");
                jsShowHideRowProgress(true);
                PageMethods.GetTEN_KH_NH($get('txtTK_KH_NH').value.trim(), GetTEN_KH_NH_Complete, GetTEN_KH_NH_Error);
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Tài khoản khách hàng không được để trống';
            }
        }

        function GetTEN_KH_NH_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            if (result.length > 0) {
                $get('txtTenTK_KH_NH').value = result;
            } else {
                document.getElementById('divStatus').innerHTML = 'Không lấy được tên tài khoản khách hàng';
                $get('txtTenTK_KH_NH').value = '';
            }
        }
        function GetTEN_KH_NH_Error(error, userContext, methodName) {
            if (error !== null) {
                $get('txtTenTK_KH_NH').value = '';
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản ngân hàng" + error.get_message();
            }
        }
        function jsShowHideDivProgress(bShow) {
            if (bShow) {
                document.getElementById('divProgress').style.display = '';
            } else {
                document.getElementById('divProgress').innerHTML = 'Đang xử lý. Xin chờ một lát...';
                document.getElementById('divProgress').style.display = 'none';
            }
        }
        function checkSS(param) {

            var arrKQ = param.split(';');
            if (arrKQ[0] == "ssF") {
                window.open("../../pages/Warning.html", "_self");
                return;
            }
        }
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }
        function jsGet_TenTKGL() {

            if (arrTKGL.length > 0) {
                for (var i = 0; i < arrTKGL.length; i++) {

                    var arr = arrTKGL[i].split(';');
                    if (arr[0] == $('#txtTKGL').val()) {
                        $('#txtTenTKGL').val(arr[1]);
                        break;
                    }
                }
            }
        }
    </script>
    <script type="text/jscript">
        function jsThem_Moi() {
            document.location.reload();
        }
        function jsLoadCTList() {
            var sSoCtu = $('#txtTraCuuSoCtu').val();
            if (sSoCtu == 'undefined' || sSoCtu == '' || sSoCtu == null)
                sSoCtu = '';
            PageMethods.LoadCTList(curMa_NV, sSoCtu, LoadCTList_Complete, LoadCTList_Error);
        }

        function LoadCTList_Complete(result, methodName) {
            if (result.length > 0) {
                jsCheckUserSession(result);
                document.getElementById('divDSCT').innerHTML = result;
            }
            try {
                $('#grdDSCT').paging({ limit: 15 });
            } catch (err) {
                paginationList(15);
            }

        }

        function LoadCTList_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy danh sách chứng từ" + error.get_message();
            }
        }
        function jsCheckUserSession(param) {
            var bRet = true;
            if (param) {
                var arrKQ = param.split(';');
                if (arrKQ[0] == "errUserSession") {
                    window.open("../../pages/Warning.html", "_self");
                    bRet = false;
                }
            }
            return bRet;
        }
        function paginationList(listPaginationCount) {
            $('table#grdDSCT').each(function () {
                var currentPage = 0;
                var numPerPage = listPaginationCount;
                var $pager = $('.pager').remove();
                var $paging = $("#paging").remove();
                var $table = $(this);
                $table.bind('repaginate', function () {
                    $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager = $('<div id="pager"></div>');

                for (var page = 0; page < numPages; page++) {
                    $('<a class="page-number" style = "color:blue;margin-left:2% ;" href = "#" ></a>').text(page + 1).bind('click', {
                        newPage: page
                    }, function (event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');

                }

                $pager.insertAfter($table).find('a.page-number:first').addClass('active');
            });
        }
        function jsShowDivStatus(bShow, sMessage) {
            if (bShow) {
                document.getElementById('divStatus').style.display = '';
                document.getElementById('divStatus').innerHTML = sMessage;
            } else {
                document.getElementById('divStatus').style.display = 'none';
                document.getElementById('divStatus').innerHTML = sMessage;
            }
        }
        function jsClearData() {
            $("#txtSoCTU").val('');
            $("#txtNgayCTU").val('');
            $("#txtNGAY_KH_NH").val('');
            $("#txtSoRefTTSP").val('');
            $("#txtSHKB").val('');
            $("#txtTenKB").val('');
            $("#txtCQQD").val('');
            $("#txtTenCQQD").val('');
            var pdata = '|Chọn loại hinh thu';
            var strArr = pdata.split('#');
            var select = document.getElementById('txtMA_LOAIHINH');
            $('#txtMA_LOAIHINH').find('option').remove();
            for (var x = 0; x < strArr.length; x++) {
                var opt = document.createElement('option');
                opt.value = strArr[x].split('|')[0];
                opt.innerHTML = strArr[x].split('|')[1];
                select.appendChild(opt);
            }

            //$("#txtMA_LOAIHINH").val('');
            //objHdrHq.TEN_LHTHU = $("#txtMA_LOAIHINH option:selected").text();// $("#txtMA_LOAIHINH").val();
            $("#txtTK_NSNN").val('');
            $("#txtMA_CHUONG").val('');
            $("#txtMA_TMUC").val('');
            $("#txtNOIDUNG").val('');
            $("#txtTTIEN").val('');
            $("#txtTTIEN_VPHC").val('');
            $("#txtLYDO").val('');
            $("#txtTTIEN_CHAMNOP").val('');
            $("#txtLYDO_CHAMNOP").val();
            $("#txtSO_QD").val('');
            $("#txtNGAY_QD").val('');

            $("#ddlMaHTTT").val('01');
            $("#txtMA_DBHC").val('');

            $("#txtGHICHU").val('');

            $("#txtMaNNT").val('');
            $("#txtTenNNT").val('');
            $("#txtTenNNTien").val('');
            $("#txtDIACHI").val('');
            $("#txtMaNNTien").val('');
            $("#txtMaCQThu").val('');

            $('#txtTK_KH_NH').val('');
            $('#txtTenTK_KH_NH').val('');
            $('#txtTenTKGL').val('');
            $('#txtTKGL').val('');
            $('#txtTongTien').val('');
            $('#txtCharge').val('0');
            $('#txtVAT').val('0');
            $('#txtMA_NT').val('VND');
            ShowHideControlByPT_TT();

        }
        function jsGetCTU(sSoCT) {
            jsShowDivStatus(false, "");
            jsShowHideDivProgress(true);

            //clear data
            jsClearData();

            //NOT: null, undefined, 0, NaN, false, or ""
            if (sSoCT) {
                $.ajax({
                    type: "POST",
                    url: "frmLapBienLai.aspx/GetCTU",
                    cache: false,
                    data: JSON.stringify({
                        "sSoCT": sSoCT
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: GetCtuHQ_OnSuccess,
                    error: function (request, status, error) {
                        jsShowDivStatus(true, request.responseText);
                        var err;
                        err = JSON.parse(request.responseText);
                        if (err) {
                            if (err.Message.indexOf(';') > -1) {
                                jsCheckUserSession(err.Message);
                            }
                            jsShowDivStatus(true, err.Message);
                        }
                        jsShowHideDivProgress(false);
                    }
                });
            }
        }

        function GetCtuHQ_OnSuccess(response) {
            var txtChargeS = 0;
            if (response != null) {
                //Load data
                if (typeof response !== 'undefined' && response !== null) {
                    if (response.d) {
                        if (!jsCheckUserSession(response.d)) {
                            return;
                        }
                        var objCTu = JSON.parse(response.d);
                        if (objCTu) {
                            //Bind HDR panel
                            document.getElementById('lblSoCTu').innerHTML = "Số chứng từ:" + objCTu.SO_CT + "; " + objCTu.MOTA_TRANGTHAI;
                            $("#txtSoCTU").val(objCTu.SO_CT);
                            $("#txtNgayCTU").val(objCTu.NGAY_CT);
                            $("#txtNGAY_KH_NH").val(objCTu.NGAY_CT);
                            $("#txtSoRefTTSP").val(objCTu.SO_CT_NH)

                            //$("#txtSoRefTTSP").val(objCTu.REF_CORE_TTSP);
                            $("#txtSoBL").val(objCTu.SOBL);
                            $("#txtKH_BL").val(objCTu.KYHIEU_BL);


                            $("#txtSHKB").val(objCTu.SHKB);
                            $("#txtTenKB").val(objCTu.TEN_KB);
                            $("#txtCQQD").val(objCTu.CQQD);
                            $("#txtTenCQQD").val(objCTu.TEN_CQQD);
                            var pdata = objCTu.MA_LHTHU + '|' + objCTu.TEN_LHTHU
                            var strArr = pdata.split('#');
                            var select = document.getElementById('txtMA_LOAIHINH');
                            $('#txtMA_LOAIHINH').find('option').remove();
                            for (var x = 0; x < strArr.length; x++) {
                                var opt = document.createElement('option');
                                opt.value = strArr[x].split('|')[0];
                                opt.innerHTML = strArr[x].split('|')[1];
                                select.appendChild(opt);
                            }

                            $("#txtTK_NSNN").val(objCTu.TK_NS);
                            $("#txtMA_CHUONG").val(objCTu.MA_CHUONG);
                            $("#txtMA_TMUC").val(objCTu.MA_NDKT);
                            $("#txtNOIDUNG").val(objCTu.NOI_DUNG);
                            $("#txtTTIEN").val(objCTu.TTIEN);
                            $("#txtTTIEN_VPHC").val(objCTu.TTIEN_VPHC);
                            $("#txtLYDO").val(objCTu.LY_DO);
                            $("#txtTTIEN_CHAMNOP").val(objCTu.TTIEN_NOP_CHAM);

                            jsFormatNumber('txtTTIEN');
                            jsFormatNumber('txtTTIEN_VPHC');
                            jsFormatNumber('txtTTIEN_CHAMNOP');

                            $("#txtLYDO_CHAMNOP").val(objCTu.LY_DO_NOP_CHAM);
                            $("#txtSO_QD").val(objCTu.SO_QD);
                            $("#txtNGAY_QD").val(objCTu.NGAY_QD);

                            $("#ddlMaHTTT").val(objCTu.HINH_THUC);
                            $("#txtMA_DBHC").val(objCTu.MA_DBHC);

                            $("#txtGHICHU").val(objCTu.DIEN_GIAI);

                            $("#txtMaNNT").val(objCTu.MA_NNTHUE);
                            $("#txtTenNNT").val(objCTu.TEN_NNTHUE);
                            $("#txtTenNNTien").val(objCTu.TEN_NNTIEN);
                            $("#txtDIACHI").val(objCTu.DC_NNTIEN);
                            $("#txtMaNNTien").val(objCTu.GIAYTO_NNTIEN);
                            $("#txtMaCQThu").val(objCTu.MA_CQTHU);
                            if (objCTu.HINH_THUC == '01') {
                                $('#txtTK_KH_NH').val(objCTu.TK_KH_NH);
                                $('#txtTenTK_KH_NH').val(objCTu.TEN_KH_NH);
                            }
                            else {
                                $('#txtTenTKGL').val(objCTu.TEN_KH_NH);
                                $('#txtTKGL').val(objCTu.TK_KH_NH);
                            }


                            $('#txtTongTien').val(objCTu.TTIEN);
                            $('#txtCharge').val(objCTu.PHI_GD);
                            $('#txtVAT').val(objCTu.PHI_VAT);
                            $('#txtMA_NT').val(objCTu.MA_NT);
                            $('#txtLyDoHuy').val(objCTu.LY_DO_TUCHOI);
                            $('#txtLoaiBienLai').val(objCTu.LOAIBIENLAI);
                            $('#txtCodePhi').val(objCTu.CODE_PHI);
                            jsCalTotal();
                            ShowHideControlByPT_TT();
                            jsProcessTrangThai(objCTu.TRANG_THAI);
                            if (objCTu.TRANG_THAI == '03' || objCTu.TRANG_THAI == '08' || objCTu.TRANG_THAI == '09')
                                jsEnableDisableControlButton('1');
                            else if (objCTu.TRANG_THAI == '04')
                                jsEnableDisableControlButton('2');
                            else if (objCTu.TRANG_THAI == '05')
                                jsEnableDisableControlButton('4');
                            else if (objCTu.TRANG_THAI == '02')
                                jsEnableDisableControlButton('5');
                            else {
                                jsEnableDisableControlButton('3');
                            }
                        }
                    }
                }
            }
            //jsCalcTotalMoney();
            //  jsCalcTotalMoneyNew(txtChargeS);
            //jsCalCharacter();
            // jsConvertCurrToText();
            jsShowHideDivProgress(false);

        }
        function jsProcessTrangThai(sTrangThai) {
            //neu la cho kiem soa thi ko cho sua gi het
            if (sTrangThai == '02' || sTrangThai == '03' || sTrangThai == '08' || sTrangThai == '09' || sTrangThai == '05') {



                $("#txtSHKB").prop("disabled", true);

                $("#txtCQQD").prop("disabled", true);
                $('#txtMA_LOAIHINH').prop("disabled", true);

                $("#txtNOIDUNG").prop("disabled", true);

                $("#txtTTIEN_VPHC").prop("disabled", true);
                $("#txtLYDO").prop("disabled", true);
                $("#txtTTIEN_CHAMNOP").prop("disabled", true);
                $("#txtLYDO_CHAMNOP").prop("disabled", true);
                $("#txtSO_QD").prop("disabled", true);
                $("#txtNGAY_QD").prop("disabled", true);

                $("#ddlMaHTTT").prop("disabled", true);


                $("#txtGHICHU").prop("disabled", true);

                $("#txtMaNNT").prop("disabled", true);
                $("#txtTenNNT").prop("disabled", true);
                $("#txtTenNNTien").prop("disabled", true);
                $("#txtDIACHI").prop("disabled", true);
                $("#txtMaNNTien").prop("disabled", true);
                $('#txtTK_KH_NH').prop("disabled", true);
                $('#txtTenTKGL').prop("disabled", true);
                $('#txtLyDoHuy').prop("disabled", true);
                $('#txtLoaiBienLai').prop("disabled", true);
            }
        }


        //làm hàm tính phí mới ở đây
        function jsTinhPhi() {
            //ddlMaHTTT
            var dblTongTien = parseFloat($get('txtTongTien').value.replaceAll('.', ''));
            var tk_nsnn = $get('txtTK_NSNN').value.replaceAll('-', '')
            if (dblTongTien == "0") {
                alert("Không tính phí với số tiền bằng 0");
                return;
            }
            //console.log("1x");
            var tk_kh = "";
            var intPP_TT = document.getElementById("ddlMaHTTT").value;
            if (intPP_TT == '01') {

                tk_kh = $get('txtTK_KH_NH').value.replaceAll('-', '');
                if (tk_kh.trim().length <= 0) {
                    alert("Chưa có tài khoản trích nợ");
                    return;
                }
            } else if (intPP_TT == '03') {
                //tiền mặt GL thì không cần truyền số TK vào tính phí
                tk_kh = "";

            }
            //console.log("2x");
            PageMethods.TinhPhi(dblTongTien, tk_kh, tk_nsnn, intPP_TT, TinhPhi_Complete, TinhPhi_Error);
        }
        function TinhPhi_Complete(result, methodName) {
            checkSS(result);
            if (result.length > 0) {
                var arrkq = result.split(';');
                var errorcode = arrkq[0];
                var errorMess = arrkq[1];
                var codephi = arrkq[2];
                var feeAMT = arrkq[3];
                var feeVAT = arrkq[4];
                if (errorcode == "0") {
                    $("#txtCharge").val(feeAMT);
                    $("#txtVAT").val(feeVAT);
                    $("#txtCodePhi").val(codephi);
                    $get('txtTongTrichNo').value = parseFloat($get('txtTongTien').value.replaceAll('.', '')) + parseFloat(feeAMT) + parseFloat(feeVAT);
                    //console.log("codephi: " + codephi);
                    //console.log("CODE_PHI: " + $get('txtCodePhi').value);
                    jsFormatNumber('txtTongTien');
                    jsFormatNumber('txtVAT');
                    jsFormatNumber('txtTongTrichNo');
                    jsFormatNumber('txtCharge');
                    jsShowDivStatus(true, "Tính phí thành công!");
                } else {
                    $("#txtCharge").val("0");
                    $("#txtVAT").val("0");
                    $("#txtCodePhi").val(codephi);
                    $get('txtTongTrichNo').value = parseFloat($get('txtTongTien').value.replaceAll('.', ''));
                    jsFormatNumber('txtTongTrichNo');
                    jsShowDivStatus(true, "Lỗi trong quá trình tính phí. ErrorCode: " + errorcode + ". ErrorMess: " + errorMess);
                    //document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình tính phí. ERORR_CODE: " + errorcode + " .ERORR_MESS: " + errorMess;
                }

            } else {
                $("#txtCharge").val("0");
                $("#txtVAT").val("0");
                $("#txtCodePhi").val("-1");
                $get('txtTongTrichNo').value = parseFloat($get('txtTongTien').value.replaceAll('.', ''));
                jsFormatNumber('txtTongTrichNo');
                jsShowDivStatus(true, "Lỗi trong quá trình tính phí");
                //document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình tính phí";
            }
        }
        function TinhPhi_Error(error, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình tính phí" + error.get_message();
            }
        }

        function ResetTinhPhi() {
            $("#txtCharge").val("0");
            $("#txtVAT").val("0");
            $("#txtCodePhi").val("");
            $get('txtTongTrichNo').value = 0;
        };

        function jsFormatNumber(txtCtl) {
            document.getElementById(txtCtl).value = localeNumberFormat(document.getElementById(txtCtl).value.replaceAll('.', ''), '.');
        }

        function localeNumberFormat(amount, delimiter) {
            try {

                amount = parseFloat(amount);
            } catch (e) {
                throw ('localeNumberFormat caused INVALID FLOAT with value ' + amount)
                return null;
            }
            if (delimiter == null || delimiter == undefined) { delimiter = '.'; }

            // convert to string
            if (amount.match != 'function') { amount = amount.toString(); }

            // validate as numeric
            var regIsNumeric = /[^\d,\.-]+/igm;
            var results = amount.match(regIsNumeric);

            if (results != null) {
                outputText('INVALID NUMBER', eOutput)
                return null;
            }

            var minus = amount.indexOf('-') >= 0 ? '-' : '';
            amount = amount.replace('-', '');
            var amtLen = amount.length;
            var decPoint = amount.indexOf(',');
            var wholeNumberEnd = decPoint > 0 ? amtLen - (amtLen - decPoint) : amtLen;

            var wholeNumber = amount.substr(0, wholeNumberEnd);

            var numberEnd = amount.substr(decPoint, amtLen);
            var fraction = amount.substr(wholeNumberEnd, amtLen - wholeNumberEnd);

            var segments = (wholeNumberEnd - (wholeNumberEnd % 3)) / 3;
            var rvsNumber = wholeNumber.split('').reverse().join('');
            var output = '';

            for (i = 0; i < wholeNumberEnd; i++) {
                if (i % 3 == 0 && i != 0 && i != wholeNumberEnd) { output += delimiter; }
                output += rvsNumber.charAt(i);
            }
            output = minus + output.split('').reverse().join('') + fraction;
            return output;
        }

        function jsCalTotal() {
            var dblTongTien = 0;
            var dblPhi = 0;
            var dblVAT = 0;
            var dblTongTrichNo = 0;


            //console.log("1");
            dblPhi = parseFloat($get('txtCharge').value.replaceAll('.', ''));
            dblVAT = parseFloat($get('txtVAT').value.replaceAll('.', ''));
            //console.log("2");
            dblTongTien = parseFloat($get('txtTongTien').value.replaceAll('.', ''));
            dblTongTrichNo = dblPhi + dblVAT + dblTongTien;
            $get('txtTongTrichNo').value = dblTongTrichNo.toString().replace('.', ',');
            //console.log("3");
            jsFormatNumber('txtTongTien');
            jsFormatNumber('txtCharge');
            jsFormatNumber('txtVAT');
            jsFormatNumber('txtTongTrichNo');
            //console.log("4");
        };
        function jsCaltotalTongTien() {
            var vtienphat = $get('txtTTIEN_VPHC').value.replaceAll('.', '');
            var vTienCham = $get('txtTTIEN_CHAMNOP').value.replaceAll('.', '');
            if (vTienCham.length <= 0) vTienCham = '0';
            if (vtienphat.length <= 0) vtienphat = '0';
            $get('txtTongTien').value = parseFloat(vtienphat) + parseFloat(vTienCham);
            jsFormatNumber('txtTongTien');
        }
        //xxx)Lay thong tin tai ngan hang song phuong
        function jsValidNHSHB_SP(strSHKB) {
            var strSHKB1 = $('#txtSHKB').val();
            if (strSHKB1.length <= 0) {
                return
            }
            PageMethods.ValidNHSHB_SP(strSHKB, ValidNHSHB_SP_Complete, ValidNHSHB_SP_Error);
            
            $('#txtTenKB').focus();
            //$('#txtSHKB').clearQueue();
        }

        //function jsValidNHSHB_SP() {
        //    var strSHKB = $('#txtSHKB').val();
        //    //if (strSHKB.length <= 0) {
        //    //    return
        //    //}
        //    PageMethods.ValidNHSHB_SP(strSHKB, ValidNHSHB_SP_Complete, ValidNHSHB_SP_Error);
        //}

        function ValidNHSHB_SP_Complete(result, methodName) {
            if ( result.length < 8) {
                //jsShowDivStatus(true, "Kho bạc không mở tại MSB");
                alert("Kho bạc không mở tại SHB. Hãy kiểm tra lại");
                $('#txtTenKB').focus();
                //$('#txtSHKB').clearQueue();
                //$('#txtMaNNT').focus();
            }
            //$('#txtSHKB').clearQueue();
            $('#txtTenKB').focus();
        }
        function ValidNHSHB_SP_Error(error, userContext, methodName) {
            if (error !== null) {
                alert("Lỗi trong quá trình kiểm tra kho bạc mở tại SHB");
                //jsShowDivStatus(true, "Lỗi trong quá trình kiểm tra kho bạc mở tại MSB");

            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr valign="top">
            <td align="center" valign="top">
                <table width="100%" align="center">
                    <tr>
                        <td valign="top" width="190px" height="500px">
                            <asp:Panel ID="Panel2" runat="server" Height="450px" Width="180px" ScrollBars="Vertical">
                                <table class="grid_data" border="0" cellpadding="0" cellspacing="0" width="100%"
                                    style="padding: 10px 0; display: none;">
                                    <tr>
                                        <td colspan="2">
                                            <input type="text" id="txtTraCuuSoCtu" value="" style="width: 110px;" />
                                        </td>
                                        <td style="text-align: center;">
                                            <input type="button" id="btnTraCuuSoCtu" value="Tra cứu" onclick="jsLoadCTList()"
                                                style="width: 70px;" />
                                        </td>
                                    </tr>
                                </table>
                                <table class="grid_data" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr class='grid_header'>
                                        <td style="width: 10%">TT
                                        </td>
                                        <td style="width: 25%">Số CT
                                        </td>
                                        <td style="width: 65%">User lập
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <div id='divDSCT'>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                             <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/ChuyenKS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">02-Chờ kiểm soát
                        </td>
                    </tr>
                   
                     <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/ChuaNhapBDS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">03-Hạch toán thành công
                        </td>
                    </tr>
                                 <tr>
                        <td style="width: 10%">
                             <img src="../../images/icons/KSLoi.png" />
                        </td>
                        <td style="width: 90%" colspan="2">04-Từ chối - chuyển trả 
                        </td>
                    </tr>  
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/Huy.png" />
                        </td>
                        <td style="width: 90%" colspan="2">05- Đã hủy
                        </td>
                    </tr>
                   
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/TT_CThueLoi.gif" />
                        </td>
                        <td style="width: 90%" colspan="2">08-Gửi TTSP lỗi
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../../images/icons/DaKS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">09-Thành công
                        </td>
                    </tr>
                                      
                   
                   
                    <tr class="img">
                        <td align="left" style="width: 100%" colspan="3">
                            <hr style="width: 50%" />
                        </td>
                    </tr>
                </table>
                Alt + M : Thêm mới<br />
                <%--Alt + G : Ghi<br />--%>
                Alt + K : Ghi và chuyển KS<br />
                <%-- Alt + C : Chuyển KS<br />--%>
                Alt + I : In chứng từ<br />
                Alt + H : Hủy chứng từ<br />
                        </td>
                        <td valign="top">
                            <table width="100%">
                                <tr class="grid_header">
                                    <td align="left">
                                        <div id="divTTBDS">
                                        </div>
                                          <div id="lblSoCTu" style="float: left; display: inline;"></div>
                                    </td>
                                    <td height='15' align="right">Trường (*) là bắt buộc nhập
                                    </td>
                                </tr>
                            </table>
                            <table id="grdHeader" cellspacing="0" cellpadding="1" rules="all" border="1" style="border-width: 1px; border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse;">
                                <tr id="Tr1">
                                    <td width="24%">
                                        <b>Số/ Ngày chứng từ </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td width="25%">
                                        <input type="text" id="txtSoCTU" class="inputflat" style="width: 99%; background: #D6D3CE" disabled="disabled" />



                                    </td>
                                    <td width="51%">
                                        <input type="text" id="txtNgayCTU" class="inputflat" style="width: 99%; background: #D6D3CE" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="Tr2">
                                    <td width="24%">
                                        <b>Số/ Ký hiệu BL </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td width="25%">
                                        <input type="text" id="txtSoBL" class="inputflat" style="width: 99%; background: #D6D3CE" disabled="disabled" />



                                    </td>
                                    <td width="51%">
                                        <input type="text" id="txtKH_BL" class="inputflat" style="width: 99%; background: #D6D3CE" disabled="disabled" />
                                    </td>
                                </tr>
                                 <tr id="rowNgay_KH_NH">
                                    <td>
                                        <b>Ngày chuyển/ Số Ref SP</b>
                                    </td>
                                    <td>
                                        <input type="text" id="txtNGAY_KH_NH" class="inputflat" style="width: 99%; background: #D6D3CE" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtSoRefTTSP" class="inputflat" style="width: 99%; background:#D6D3CE" disabled="disabled" />
                                    </td>
                                </tr>
                                   <tr id="rowLHBLThu">
                                    <td>
                                        <b>Loại Hình thu</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td colspan="2">
                                        <select id="txtMA_LOAIHINH" class="inputflat_combo" style="width: 80%">
                                            <option value="">Chọn loại hình thu</option>
                                            <option value="981">Phat VPHC trong linh vuc giao thong duong bo</option>
                                            <option value="982">Phat VPHC trong linh vuc giao thong duong sat</option>
                                            <option value="983">Phat VPHC trong linh vuc giao thong duong thuy</option>
                                        </select>
                                    </td>
                                </tr>
                           
                                <tr id="rowCQQD">
                                    <td>
                                        <b>Cơ quan quyết định</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtCQQD" class="inputflat" style="width: 99%; background: Aqua" onkeypress="if (event.keyCode==13){ShowLov('CQQD');this.value = '';}" onblur="jsCQQD_lostFocus();jsGetTenCQQD();"  />
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenCQQD" onfocus="this.value=''" class="inputflat" style="width: 99%; background: #D6D3CE" readonly />
                                    </td>
                                </tr>

                                     <tr id="rowSHKB">
                                    <td width="24%">
                                        <b>Số hiệu KB </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td width="25%">
                                        <input type="text" id="txtSHKB" class="inputflat" style="width: 99%; background: Aqua"
                                       onkeypress="if (event.keyCode==13){ShowLov('SHKB');this.value = '';}" onblur='jsSHKB_lostFocus(this.value);jsValidNHSHB_SP(this.value);' />
                                          <%--onkeypress="if (event.keyCode==13){ShowLov('SHKB');}" onblur='jsSHKB_lostFocus();jsValidNHSHB_SP();' />--%>

                                        
                                    </td>
                                    <td width="51%">
                                       <input type="text" id="txtTenKB"  class="inputflat" style="width: 99%; background: #D6D3CE" readonly />
                                    </td>
                                </tr>

                                <tr id="rowCQThu">
                                    <td class="style1">
                                        <b>CQ Thu/DBHC </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td class="style1">
                                        <input type="text" id="txtMaCQThu" style="width: 99%;"
                                            class="inputflat" disabled="disabled" />
                                    </td>
                                    <td class="style1">
                                        <input type="text" id="txtMA_DBHC" class="inputflat" style="width: 99%; background: #D6D3CE" readonly />
                                    </td>
                                </tr>
                             
                                <tr id="rowTKNSNN">
                                    <td>
                                        <b>Tài Khoản NSNN / Chương</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtTK_NSNN" class="inputflat" style="width: 99%; background: #D6D3CE" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtMA_CHUONG" class="inputflat" style="width: 99%; background: #D6D3CE" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="rowNDKT">
                                    <td>
                                        <b>Mục NSNN</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtMA_TMUC" class="inputflat" style="width: 99%; background: #D6D3CE" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtNOIDUNG" class="inputflat" style="width: 99%;" />
                                    </td>
                                </tr>
                                <tr id="Tr8">
                                    <td>
                                        <b>Loại biên lai</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <select id="txtLoaiBienLai" class="inputflat_combo" style="width: 99%" disabled="disabled">
                                            <option value="1" selected="selected">Biên lai thu phạt</option>
                                            <option value="2">Biên lai thu phí, lệ phí</option>
                                        </select>
                                    </td>
                                    <td ></td>
                                    <!--<td >
																				<input type="text" id="txtMA_LOAIHINH" class="inputflat" style="width: 99%; background: Aqua" onkeypress="if (event.keyCode==13){ShowLov('LHTHU');}" onblur="jsLHTHU_lostFocus()" />
																			</td>
                                                            <td >
																				<input type="text" id="txtTEN_LOAIHINH" class="inputflat" style="width: 99%;background:#D6D3CE" disabled="disabled"  />
																			</td>
                                                            -->
                                </tr>
                                <tr id="rowMaNNT">
                                    <td width="24%">
                                        <b>Mã số thuế/Tên </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td width="25%">
                                        <input type="text" id="txtMaNNT" class="inputflat" style="width: 99%;" />
                                    </td>
                                    <td width="51%">
                                        <input type="text" id="txtTenNNT" class="inputflat" style="width: 92%;" />
                                        <img id="imgMaDB" alt="Chọn từ danh mục đặc biệt" src="../../images/icons/MaDB.png"
                                            onclick="jsGetMaDB();" style="width: 16px; height: 16px" />
                                    </td>
                                </tr>
                                <tr id="rowNNTien">
                                    <td>
                                        <b>Số CMT/Tên người nộp tiền</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtMaNNTien" class="inputflat" style="width: 99%;" maxlength="20" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenNNTien" class="inputflat" style="width: 99%;" onblur="jsComputeDIENGIAI();" />
                                    </td>
                                </tr>
                                <tr id="Tr6">
                                    <td>
                                        <b>Địa chỉ</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td colspan="2">
                                        <input type="text" id="txtDIACHI" class="inputflat" style="width: 99%;" maxlength="200" />
                                    </td>

                                </tr>
                                <tr id="rowSoQD">
                                    <td>
                                        <b>Số/ Ngày Quyết định</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSO_QD" class="inputflat" style="width: 99%;" maxlength="30" />
                                    </td>
                                    <td>
                                        <span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                                            <input type="text" id='txtNGAY_QD' maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                                                onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                                        </span>
                                    </td>
                                </tr>
                                <tr id="Tr4">
                                    <td>
                                        <b>Tổng tiền/ Nguyên tệ </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtTTIEN" class="inputflat" style="width: 99%; background: #D6D3CE" disabled="disabled""/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtMA_NT" class="inputflat" style="width: 99%; background: #D6D3CE" disabled="disabled" value="VND" />
                                    </td>

                                </tr>
                                <tr id="row">
                                    <td>
                                        <b>Số tiền/Lý do nộp VPHC </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtTTIEN_VPHC" class="inputflat" style="width: 99%;" onkeyup="formatNumberVNĐ();" onblur="jsComputeTTIEN();jsCaltotalTongTien();jsComputeDIENGIAI();"/>
                                    </td>
                                    <td>
                                        <input type="text" id="txtLYDO" class="inputflat" style="width: 99%;" />
                                    </td>

                                </tr>
                                <tr id="Tr3">
                                    <td>
                                        <b>Số tiền/Lý do chậm nộp </b>
                                    </td>
                                    <td>
                                        <input type="text" id="txtTTIEN_CHAMNOP" class="inputflat" style="width: 99%;" onkeyup="formatNumberVNĐ1();" onblur="jsComputeTTIEN();jsCaltotalTongTien();jsComputeDIENGIAI();" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtLYDO_CHAMNOP" class="inputflat" style="width: 99%;" />
                                    </td>

                                </tr>
                                <tr id="Tr5">
                                    <td>
                                        <b>Ghi chú</b>
                                    </td>
                                    <td colspan="2">
                                        <input type="text" id="txtGHICHU" class="inputflat" style="width: 99%;" />
                                    </td>
                                </tr>
                                <tr id="rowHTTT">
                                    <td>
                                        <b>Hình thức thanh toán</b>
                                    </td>
                                    <td>
                                        <select id="ddlMaHTTT" style="width: 99%" class="inputflat" onchange="ShowHideControlByPT_TT();getSoTK_TruyVanCore();">

                                            <option value="01" selected="selected">Chuyển khoản</option>

                                            <option value="05">Nộp tiền mặt</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenHTTT" class="inputflat" style="width: 99%; background: #D6D3CE" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="rowTK_KH_NH">
                                    <td>
                                        <b>TK Chuyển </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtTK_KH_NH" class="inputflat" style="width: 99%;" onblur="jsGetTK_KH_NH();jsGetTenTK_KH_NH();"
                                            onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'3,6,9,16','-');}"  maxlength="18" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenTK_KH_NH" class="inputflat" style="width: 95%; background: #D6D3CE" disabled="disabled" />
                                        <img id="imgSignature" alt="Hiển thị chữ ký khách hàng" src="../../images/icons/MaDB.png"
                                            onclick="jsShowCustomerSignature();" style="width: 16px; height: 16px" />
                                    </td>
                                </tr>
                                <tr id="rowSoDuNH">
                                    <td>
                                        <b>Số dư NH</b>
                                    </td>
                                    <td>
                                        <input type="text" id="txtSoDu_KH_NH" class="inputflat" style="width: 99%; background: #D6D3CE" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtDesc_SoDu_KH_NH" class="inputflat" style="width: 99%; background: #D6D3CE" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="rowTKGL" style="display: none;">
                                    <td>
                                        <b>TK chuyển </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <input type="text" id="txtTKGL" maxlength="12" class="inputflat" style="width: 99%;" onblur="jsGet_TenTKGL();" disabled="disabled" />
                                    </td>
                                    <td>
                                        <input type="text" id="txtTenTKGL" class="inputflat" style="width: 99%; background: #D6D3CE" disabled="disabled" />
                                    </td>
                                </tr>
                               
                                <tr id="Tr7">
                                    <td>
                                        <b>Lý do từ chối </b>
                                    </td>
                                    <td colspan="2">
                                        <input type="text" id="txtLYDO_TC" class="inputflat" style="width: 99%; background: #D6D3CE" disabled="disabled" />
                                    </td>

                                </tr>

                            </table>
                            <br />
                            <div class="errorMessage" style="width: 100%; margin: 0; padding: 0;">
                                <div id="divStatus" style="width: 100%; margin: 0; padding: 0; background-color: Aqua; font-weight: bold; font-size: 14pt; display: none;">
                                    <br />
                                </div>
                                <div id="divProgress" style="width: 100%; margin: 0; padding: 0; background-color: Aqua; font-weight: bold; font-size: 14pt; display: none;">
                                    <b style="">Đang xử lý. Xin chờ một lát...</b>
                                </div>
                            </div>
                            <div id="divTTinKhac" style="width: 100%; margin: 0; padding: 0;">
                                <table cellspacing="3" cellpadding="1" style="width: 100%;">
                                    <tr align="left">
                                        <td align="left" valign="middle" class="text" style="width: 50%; font-family: @Arial Unicode MS; font-weight: bold; color: red">
                                            <span style="">Số ký tự còn lại: </span>
                                            <label id="leftCharacter" style="font-weight: bold" visible="false">
                                            </label>
                                        </td>
                                        <td align="right" class="text" style="width: 50%;">
                                            <span style="font-family: @Arial Unicode MS; font-weight: bold;">Tổng Tiền: </span>
                                            <input type="text" id="txtTongTien" style="background-color: Yellow; text-align: right; font-weight: bold"
                                                class="inputflat" value="0" readonly="readonly" disabled="disabled" />
                                        </td>
                                    </tr>
                               <%--     <tr>
                                        <td align="right" class="text" style="width: 100%;" colspan="2">
                                            <span style="font-family: @Arial Unicode MS; font-weight: bold;">Tổng Tiền NT: </span>
                                            <input type="text" id="txtTongTienNT" style="background-color: Yellow; text-align: right; font-weight: bold"
                                                class="inputflat" value="0" readonly="readonly" disabled="disabled" />
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td align="right" colspan="2">
                                            <table width="600px">
                                                <tr>
                                                    <td>
                                                        <%--<input type="checkbox" id="chkChargeType" checked onclick="jsEnabledInputCharge();" />--%>
                                                        <input type="button" id="cmdTinhPhi"  value="Tính Phí" style="display:none" />
                                                    </td>
                                                    <td style="display:none">Phí
                                                    </td>
                                                    <td>
                                                        <input type="text" id="txtCharge" style="text-align: right; width: 100px; background-color: Yellow; font-weight: bold;display:none"
                                                            class="inputflat" value="0" disabled="disabled" onfocus="this.select()"
                                                            onkeyup="valInteger(this)" />
                                                        <input type="hidden" value="" id="txtChargeFlex" />

                                                    </td>
                                                    <td style="display:none">VAT:
                                                    </td>
                                                    <td>
                                                        <input type="text" id="txtVAT" style="text-align: right; width: 100px; background-color: Yellow; font-weight: bold;display:none"
                                                            class="inputflat" value="0" readonly="readonly" />
                                                        <input type="hidden" value="" id="txtVATFlex" />
                                                    </td>
                                                    <td style ="display:none ">Tổng trích nợ:
                                                    </td>
                                                    <td style ="display:none ">
                                                        <input type="text" id="txtTongTrichNo" style="text-align: right; width: 150px; background-color: Yellow; font-weight: bold"
                                                            class="inputflat" value="0" readonly="readonly" />
                                                        <input type="hidden" value="" id="txtTongTrichNoFlex" />
                                                    </td>
                                                    <td align="right" style="display:none">
                                                                            <input type="text" id="txtCodePhi" style="width: 100%; background-color: Yellow; font-weight: bold;
                                                                                text-align: right" class="inputflat" readonly="readonly" />
                                                                        </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="width: 99%" colspan="2">
                                            <div id='divBangChu' style="font-weight: bold;">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id="rowLyDoHuy" align="left">
                                        <td align="left" style="width: 99%" colspan="2" valign="middle">
                                            <span style="font-weight: bold;">Lý do hủy/điều chỉnh :</span>
                                            <input type="text" id="txtLyDoHuy" style="width: 99%; border-color: black; font-weight: bold"
                                                maxlength="100" class="inputflat" />
                                        </td>
                                    </tr>
                                </table>
                              
                                <input id="hdfLoaiCTu" type="hidden" value="T" />
                                <input id="hdfHQResult" type="hidden" />
                                <input id="hdfHDRSelected" type="hidden" />
                                <input id="hdfIsEdit" type="hidden" />
                                <input id="hdfDescCTu" type="hidden" />
                                <input id="hdfNgayKB" type="hidden" />
                                <input id="hdfMST" type="hidden" />
                                 <input type="hidden" id="hdLimitAmout" value="0" />
                                 <input type="hidden" id="hndNHSHB" />
                            </div>
                            <br />
                            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                <tr>
                                    <td width='100%' align="center" valign="top" class='text' colspan='2' align="right">
                                        <input type="button" id="cmdThemMoi" class="ButtonCommand" onclick="jsThem_Moi();"
                                            value="Tạo CT mới" accesskey="M" />&nbsp;
													<input type="button" id="cmdGhiKS" class="ButtonCommand" value="Chuyển duyệt" onclick="jsGhi_CTU('2');"
                                                        accesskey="K" />
                                        &nbsp;
												
													&nbsp;
													<input type="button" id="cmdInCT" value="In CT" class="ButtonCommand" onclick="jsIn_CT('1');" disabled="disabled"
                                                        accesskey="I" />
                                        &nbsp;
													<input type="button" id="cmdHuyCT" class="ButtonCommand" value="Hủy CT" onclick="jsHuy_CTU();" disabled="disabled"
                                                        accesskey="H" />

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <div id="dialog-popup" style="display: none;">
    </div>
    <div id="wapaler" style="display: none;">
    </div>
    <div class="msgBox" id="msgBox_SHKB" style="display: none; opacity: 1; min-height: 100px; width: 730px;">
        <div class="msgBoxTitle">DANH MỤC</div>
        <div>
            <div>
                <div class="msgBoxContent" id="msgBox_SHKB_CONTENT">

                    <table border="0" cellpadding="2" cellspacing="1" class="form_input" style="width: 100%">
                        <tr class="grid-heading">
                            <td align="center" class="style2" style="width: 30%">
                                <b>Mã danh mục</b>
                            </td>
                            <td align="center" class="style3">
                                <b>Tên danh mục</b>
                            </td>
                        </tr>
                        <tr>

                            <td>
                                <input type="text" id="msgBox_txtMaDanhMuc" onkeydown="if(event.keyCode==13) {msgBox_jsSearch(); event.keyCode=9;}" class="inputflat" />&nbsp;
                            </td>


                            <td align="center">
                                <input type="text" id="msgBox_txtTenDanhMuc" onkeydown="if(event.keyCode==13) {msgBox_jsSearch(); event.keyCode=9;}" class="inputflat" />&nbsp;
             <%-- <input type="button" id="msgBox_btnSearch" value=" ... " class="inputflat" onclick="msgBox_jsSearch();" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="style1">
                                <div id='msgBox_divDanhMuc' style="max-height: 300px; overflow: auto">
                                </div>
                                <div id='msgBox_divStatus'>
                                </div>

                            </td>
                        </tr>
                    </table>
                </div>

            </div>
            <div class="msgBoxButtons" id="msgBox_SHKB_ConfirmDivButton">
                <input type="hidden" id="msgBox_result" value="" />
                <input id="msgBox-btn-close" class="btn btn-primary" onclick="jsMsgBoxClose();" type="button" name="Ok" value="Đóng" />
            </div>

        </div>
    </div>
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function () {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtNGAY_QD").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
            jsLoadCTList();
        });
        function jsDiaglog() {
            $('#msgBox_result').val('');
            document.getElementById('msgBox_txtMaDanhMuc').value = "";
            document.getElementById('msgBox_txtTenDanhMuc').value = "";
            // console.log('Start');
            $('#wapaler').css("display", "block");
            $('#msgBox_SHKB').css("display", "block");

            jsDivAutoCenter('#msgBox_SHKB');
            msgBox_jsSearch();
        }


        function jsMsgBoxClose() {

            $('#wapaler').css("display", "none");
            $('#msgBox_SHKB').css("display", "none");
            //var strTenCQQD = $('#txtMA_LOAIHINH').val();
            //if (strTenCQQD.length > 0) {

            //    //  
            //    $('#txtCQQD').focus();
               
            //}
          

        }
        var msgBox_type = "";
        var msgBox_SHKB = "";
        var msgBox_txtID = "";
        var msgBox_txtTitle = "";
        var msgBox_txtFocus = "";

        function ShowDMKB(strPage, txtID, txtTitle, txtFocus) {

            document.getElementById('msgBox_result').value = "";

            msgBox_type = strPage;

            msgBox_txtID = txtID;
            msgBox_txtTitle = txtTitle;
            msgBox_txtFocus = txtFocus;
            jsDiaglog();


        }
        function jsDivAutoCenter(divName) {
            //      console.log('em chay phat' + divName);
            $(divName).css({
                position: 'fixed',
                left: ($(window).width() - $(divName).outerWidth()) / 2,
                top: ($(window).height() - $(divName).outerHeight()) / 2,
                height: 'auto'
            });
        }
        //pop-up
        $(window).resize(function () {

            jsDivAutoCenter('.msgBox');
        });
        //function msgBox_jsSearch() {
        //    var strKey = document.getElementById('msgBox_txtMaDanhMuc').value;
        //    var strTenDM = document.getElementById('msgBox_txtTenDanhMuc').value;
        //    var strPage = msgBox_type;
        //    var strSHKB = "";
        //    if (document.getElementById('txtSHKB').value.length > 0) {
        //        strSHKB = document.getElementById('txtSHKB').value;
        //    }
        //    var curMa_Dthu = '';
        //    var curMa_DBHC = '';
        //    PageMethods.SearchDanhMuc(curMa_Dthu, curMa_DBHC, strPage, strKey, strTenDM, strSHKB, SearchDanhMuc_Complete, SearchDanhMuc_Error);
        //}

        function msgBox_jsSearch() {
            var strKey = document.getElementById('msgBox_txtMaDanhMuc').value;
            var strTenDM = document.getElementById('msgBox_txtTenDanhMuc').value;
            var strMA_LOAIHINH = ""
            var strPage = msgBox_type;
            var strSHKB = "";
            if (document.getElementById('txtSHKB').value.length > 0) {
                strSHKB = document.getElementById('txtSHKB').value;
            }
            if (document.getElementById('txtMA_LOAIHINH').value.length > 0) {
                strMA_LOAIHINH = document.getElementById('txtMA_LOAIHINH').value;
            }
            var curMa_Dthu = '';
            var curMa_DBHC = '';
            PageMethods.SearchDanhMuc(curMa_Dthu, curMa_DBHC, strPage, strKey, strTenDM, strSHKB,strMA_LOAIHINH, SearchDanhMuc_Complete, SearchDanhMuc_Error);
        }
        function SearchDanhMuc_Complete(result, methodName) {
            //document.getElementById('divDanhMuc').innerHTML = result;          
            document.getElementById('msgBox_divDanhMuc').innerHTML = jsBuildResultTable(result);

        }
        function SearchDanhMuc_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('msgBox_divStatus').innerHTML = "Lỗi trong quá trình lấy danh sách chứng từ" + error.get_message();
            }
        }
        function SelectDanhMuc(strID, strTitle) {
            //var arr = new Array();
            //arr["ID"] = strID;
            //arr["Title"] = strTitle;
            //document.getElementById('msgBox_result').value = arr;
            jsMsgBoxClose();
            if (strID.length > 0) {
                document.getElementById(msgBox_txtID).value = strID;

                if (strTitle != null && msgBox_txtTitle != null) {
                    if (msgBox_txtTitle.length > 0)
                        document.getElementById('' + msgBox_txtTitle).value = strTitle;

                }
                if (msgBox_txtFocus != null) {
                    //console.log(msgBox_txtFocus);
                    document.getElementById(msgBox_txtFocus).focus();
                }
                if (msgBox_type == "KhoBac") {

                    jsSHKB_lostFocus(strID);
                    //PageMethods.GetTen_SHKB(strID, LoadDM_KB_Complete, LoadDM_KB_Error);
                } else if (msgBox_type == 'CQQD1') {
                    $('#txtCQQD').focus();
                    //fnc_LoadLHTHU();
                    
                }
            }
            //window.parent.returnValue = arr;
            //window.parent.close();
        }
        function getParam(param) {
            var query = window.location.search.substring(1);
            var parms = query.split('&');
            for (var i = 0; i < parms.length; i++) {
                var pos = parms[i].indexOf('=');
                if (pos > 0) {
                    var key = parms[i].substring(0, pos).toLowerCase();
                    var val = parms[i].substring(pos + 1);
                    if (key == param.toLowerCase())
                        return val;
                }
            }
            jsDivAutoCenter('.msgBox');
            return '';
        }

        function jsBuildResultTable(strInput) {
            var strRet = '';
            if (strInput.length > 0) {
                strRet += " <table border='0' cellpadding='0' cellspacing='0' width='100%'>";
                var arr = strInput.split("|");
                for (var i = 0; i < arr.length - 1; i++) {
                    var strID = arr[i].split(";")[0];
                    var strTitle = arr[i].split(";")[1];
                    strRet += "<tr onclick=\"SelectDanhMuc('" + strID + "','" + strTitle + "');\" class='PH'>" + "<td width='150' align='left'><b>" + strID + "</b></td><td  align='left'>" + strTitle + "</td></tr><tr><td colspan='2' height='8' ></td></tr> <tr><td colspan='2' class='line'></td></tr>";
                }
                strRet += "</table>";
            } else {
                strRet += " <table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td width='100%' align='left' class='nav'>Không tồn tại danh mục này !</td></tr></table>";
            }
            return strRet;
        }
    </script>
</asp:Content>

