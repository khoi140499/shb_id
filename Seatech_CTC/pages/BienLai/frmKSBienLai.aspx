﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmKSBienLai.aspx.vb" Inherits="pages_BienLai_frmKSBienLai" Title="Kiểm soát chứng từ biên lai" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../../javascript/jquery/jquery-1.3.2.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        function DG_changeBackColor(row, highlight) {
            if (highlight) {
                row.style.cursor = "hand";
                lastColor = row.style.backgroundColor;
                row.style.backgroundColor = '#C6D6FF';
            }
            else
                row.style.backgroundColor = lastColor;
        };

    </script>

    <script type="text/javascript" language="javascript">
        function ShowHideControlByPT_TT() {
            var intPP_TT = document.getElementById("ddlMaHTTT").value;
            var rTK_KH_NH = document.getElementById("rowToKhaiSo");
            var rSoDuNH = document.getElementById("rowLHXNK");
            var rTKGL = document.getElementById("rowTKGL");
            //07/06/2011-Kienvt:Them thong tin ngan hang nhan,chuyen
            //var rNHChuyen = document.getElementById("rowNHChuyen");
            //var rNHNhan = document.getElementById("rowNHNhan");
            //var rNHTT = document.getElementById("rowNHTT");
            //jsCalTotal();

            if (intPP_TT == '01') {//"NH_TKTH_KB"){ :chuyen khoan
                rTK_KH_NH.style.display = '';
                rSoDuNH.style.display = '';
                rTKGL.style.display = 'none';
                //07/06/2011-Kienvt:Them thong tin ngan hang nhan,chuyen
                //rNHChuyen.style.display = '';
                //rNHNhan.style.display = '';
                //rNHTT.style.display = '';
                //$get('txtTK_KH_NH').focus();
            }

            if (intPP_TT == '05') {//"NH_TKTG"){ :trung gian
                rTK_KH_NH.style.display = 'none';
                rSoDuNH.style.display = 'none';
                rTKGL.style.display = '';
                //rNHChuyen.style.display = '';
                //rNHNhan.style.display = '';
                //rNHTT.style.display = '';
                //07/06/2011-Kienvt:Them thong tin ngan hang nhan,chuyen

            }
            //  jsEnabledButtonByBDS();
        }
    </script>

    <style type="text/css">
        .auto-style1 {
            width: 183px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">KIỂM SOÁT CHỨNG TỪ BIÊN LAI</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr height="8px">
                        <td></td>
                    </tr>
                    <tr>
                        <td width='100%' align="center" valign="top" style="padding-left: 3; padding-right: 3">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width='900' align="left" valign="top" class="frame">
                                        <asp:HiddenField ID="hdfMaNNT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfSHKB" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfNgay_KB" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfSo_CT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfKyHieu_CT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfMa_NV" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfTrangThai_CT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfLoai_CT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfDienGiai" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfSoCTNH" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfMACN" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfSoRefCore" runat="server" Value="" />
                                        <table width="100%" align="center">
                                            <tr>
                                                <td valign="top" width="210px" height="300px" align="left">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr align="left">
                                                            <td>
                                                                <asp:GridView ID="grdDSCT" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                                                    ShowHeader="True" ShowFooter="True" BorderColor="#989898" CssClass="grid_data"
                                                                    PageSize="15" Width="100%">
                                                                    <PagerStyle CssClass="cssPager" />
                                                                    <AlternatingRowStyle CssClass="grid_item_alter" />
                                                                    <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header" HorizontalAlign="Left"></HeaderStyle>
                                                                    <RowStyle CssClass="grid_item" />
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Số CT">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <FooterStyle HorizontalAlign="Left"></FooterStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnSelectCT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SO_CT") %>'
                                                                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "KYHIEU_CT") & "," & DataBinder.Eval(Container.DataItem, "NGAY_KB") & "," & DataBinder.Eval(Container.DataItem, "SO_CT") & "," & DataBinder.Eval(Container.DataItem, "TRANG_THAI") & "," & DataBinder.Eval(Container.DataItem, "TTIEN") & "," & DataBinder.Eval(Container.DataItem, "MA_NV") & "," & DataBinder.Eval(Container.DataItem, "SHKB") & "," & Eval("TEN_DN").ToString() & "," & Eval("MA_KS").ToString()%>'
                                                                                    OnClick="btnSelectCT_Click"  />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Trạng thái">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <FooterStyle Font-Bold="true" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblMaNV" runat="server" Text='<%#Business.BienLai.buBienLai.Get_TEN_TrangThai_BL(Eval("TRANG_THAI").ToString())%>' />
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <%#grdDSCT.Rows.Count.ToString%>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <PagerStyle HorizontalAlign="Left" BackColor="#E4E5D7"></PagerStyle>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                <asp:Button ID="cmdRefresh" runat="server" Text="[Refresh]" AccessKey="K" CssClass="ButtonCommand"
                                                                    UseSubmitBehavior="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">Trạng thái chứng từ:
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td align="left" valign="top" width='100%'>
                                                                <asp:DropDownList ID="ddlTrangThai" runat="server" CssClass="inputflat" Width="100%"
                                                                    AutoPostBack="true">
                                                                    <asp:ListItem Value="99" Text="Tất cả"></asp:ListItem>

                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top" width="680px">
                                                    <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                                        <tr>
                                                            <td colspan="2" width='100%' align="left">
                                                                <table width="100%">
                                                                    <tr class="grid_header">
                                                                        <td height="15px" align="right">Trường (*) là bắt buộc nhập
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <div id="Panel1" style="height: 480px; width: 100%; overflow-y: scroll;">
                                                                    <table runat="server" id="grdHeader" cellspacing="0" cellpadding="1" rules="all"
                                                                        border="1" style="border-width: 1px; border-style: solid; font-family: Verdana; font-size: 8pt; width: 97%; border-collapse: collapse;">
                                                                        <tr id="rowNgayCT">
                                                                            <td width="24%">
                                                                                <b>Số/ Ngày chứng từ </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td width="25%">
                                                                                <asp:TextBox runat="server" ID="txtSoCTU" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="100%" />
                                                                            </td>
                                                                            <td width="51%">
                                                                                <asp:TextBox runat="server" ID="txtNgayCTU" CssClass="inputflat" ReadOnly="true" Width="100%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSoBL">
                                                                            <td width="24%">
                                                                                <b>Số/ Ký hiệu BL </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td width="25%">
                                                                                <asp:TextBox runat="server" ID="txtSoBL" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="100%" />
                                                                            </td>
                                                                            <td width="51%">
                                                                                <asp:TextBox runat="server" ID="txtKH_BL" CssClass="inputflat" ReadOnly="true" Width="100%" />
                                                                            </td>
                                                                        </tr>

                                                                        <tr id="rowRef">
                                                                            <td>
                                                                                <b>Ngày chuyển/ Số Ref SP</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtNGAY_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtSoRefTTSP" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                        </tr>


                                                                        <tr id="rowKhoBac">
                                                                            <td>
                                                                                <b>Số hiệu KB </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtSHKB" CssClass="inputflat" ReadOnly="true" Width="100%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenKB" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowCQQD">
                                                                            <td>
                                                                                <b>Cơ quan quyết định</b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtCQQD" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenCQQD" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowCQThu">
                                                                            <td>
                                                                                <b>CQ Thu/DBHC </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMaCQThu" CssClass="inputflat" ReadOnly="true" Width="100%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMA_DBHC" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowLHBLThu">
                                                                            <td>
                                                                                <b>Loại Hình thu</b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMA_LOAIHINH" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="99%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTEN_LOAIHINH" CssClass="inputflat" ReadOnly="true" Width="99%" />
                                                                            </td>
                                                                        </tr>

                                                                        <tr id="rowTKNSNN">
                                                                            <td>
                                                                                <b>Tài Khoản NSNN / Chương</b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTK_NSNN" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMA_CHUONG" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNDKT">
                                                                            <td>
                                                                                <b>Mục NSNN </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMA_TMUC" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtNOIDUNG" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="Tr9">
                                                                            <td width="24%">
                                                                                <b>Loại biên lai </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList runat="server" ID="txtLoaiBienLai" CssClass="inputflat" Enabled="false"
                                                                                    Width="100%">
                                                                                    <asp:ListItem Value="1" Text="Biên lai thu phạt" Selected="True"></asp:ListItem>
                                                                                    <asp:ListItem Value="2" Text="Biên lai thu phí, lệ phí"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowMaNNT">
                                                                            <td>
                                                                                <b>Mã số thuế/Tên </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMaNNT" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenNNT" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>

                                                                        </tr>
                                                                        <tr id="rowNNTien">
                                                                            <td>
                                                                                <b>Số CMT/Tên người nộp tiền</b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMaNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowDiaChi">
                                                                            <td>
                                                                                <b>Địa chỉ</b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:TextBox runat="server" ID="txtDIACHI" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>

                                                                        </tr>
                                                                        <tr id="rowSoQD">
                                                                            <td>
                                                                                <b>Số/ Ngày Quyết định</b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtSO_QD" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtNGAY_QD" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTT">
                                                                            <td>
                                                                                <b>Tổng tiền/ Nguyên tệ </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTongTien" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMA_NT" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSoTien">
                                                                            <td>
                                                                                <b>Số tiền/Lý do nộp VPHC </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTTIEN_VPHC" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtLYDO" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowMaNT">
                                                                            <td>
                                                                                <b>Số tiền/Lý do chậm nộp </b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTTIEN_CHAMNOP" CssClass="inputflat" ReadOnly="true" Width="100%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtLYDO_CHAMNOP" CssClass="inputflat" ReadOnly="true" Width="100%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowLoaiThue">
                                                                            <td>
                                                                                <b>Ghi chú</b>
                                                                            </td>

                                                                            <td colspan="2">
                                                                                <asp:TextBox runat="server" ID="txtGHICHU" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowHTTT">
                                                                            <td>
                                                                                <b>Hình thức thanh toán</b>
                                                                            </td>
                                                                            <td>
                                                                                <%-- <select id="ddlMaHTTT" style="width: 99%" class="inputflat" disabled="disabled">

                                                                                    <option value="01" selected="selected">Chuyển khoản</option>

                                                                                    <option value="03">Nộp qua GL</option>
                                                                                </select>--%>
                                                                                <asp:DropDownList runat="server" ID="ddlMaHTTT"  CssClass="inputflat" Enabled="false"
                                                                                    Width="100%">
                                                                                    <asp:ListItem Value="01" Text="Chuyển khoản" Selected="True"></asp:ListItem>
                                                                                    <asp:ListItem Value="05" Text="Nộp tiền mặt"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenHTTT" class="inputflat" Style="width: 99%; background: #D6D3CE" disabled="disabled" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowToKhaiSo" >
                                                                            <td>
                                                                                <b>TK Chuyển </b><span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTK_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtTenTK_KH_NH" runat="server" Width="100%" CssClass="inputflat">
                                                                                </asp:TextBox>
                                                                            </td>
                                                                        </tr>

                                                                        <tr id="rowLHXNK">
                                                                            <td>
                                                                                <b>Số dư NH</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtSoDu_KH_NH" CssClass="inputflat" ReadOnly="true" Width="100%" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtDesc_SoDu_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                        </tr>
                                                                             <tr id="rowTKGL" style ="display:none">
                                   
                                                                                  <td>
                                       
                                                                                       <b>TK chuyển </b><span class="requiredField">(*)</span>
                                    
                                                                                  </td>
                                    
                                                                                 <td>
                                        
                                                                                     <asp:TextBox runat="server" ID="txtTKGL" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                       
                                   
                                                                                      </td>
                                    
                                                                                 <td>
                                        
                                                                                     <asp:TextBox ID="txtTenTKGL" runat="server" Width="100%" CssClass="inputflat">
                                                                                </asp:TextBox>
                                        
                                   
                                                                                      </td>
                                 
                                                                             </tr>
                                                                        <tr id="rowSoKhung">
                                                                            <td>
                                                                                <b>Lý do từ chối </b>
                                                                            </td>
                                                                            <td colspan="2">
                                                                                <asp:TextBox runat="server" ID="txtLYDO_TC" CssClass="inputflat" 
                                                                                    Width="100%" />
                                                                            </td>

                                                                        </tr>



                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr style="visibility: hidden">
                                                            <td width="25%" style="height: 0px">
                                                                <asp:TextBox runat="server" ID="txtSo_BT" CssClass="inputflat" ReadOnly="true" Visible="false"
                                                                    Width="100%" />
                                                                <asp:TextBox runat="server" ID="txtSoCT" CssClass="inputflat" ReadOnly="true" Visible="false"
                                                                    Width="100%" />
                                                            </td>
                                                            <td width="51%" style="height: 0px">
                                                                <asp:TextBox runat="server" ID="txtTRANG_THAI" CssClass="inputflat" ReadOnly="true"
                                                                    Visible="false" Width="100%" />
                                                                <asp:TextBox runat="server" ID="txtMA_NV" CssClass="inputflat" ReadOnly="true" Visible="false"
                                                                    Width="100%" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='2' align="left" class="errorMessage">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left" width='100%'>
                                                                            <div id="divStatus" style="font-weight: bold" />
                                                                        </td>
                                                                        <td>
                                                                            <div id="divDescCTU" style="font-weight: bold">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" width='100%'>
                                                                            <asp:UpdateProgress ID="updProgress" runat="server">
                                                                                <ProgressTemplate>
                                                                                    <div style="background-color: Aqua;">
                                                                                        <b>Đang lấy dữ liệu tại server.Xin chờ một lát...</b>
                                                                                    </div>
                                                                                </ProgressTemplate>
                                                                            </asp:UpdateProgress>
                                                                            <b>
                                                                                <asp:Literal ID="lblStatus" runat="server"></asp:Literal></b>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" width="100%"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='2' height='5'></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" colspan="2">
                                                                <table width="500px" style="display: none">
                                                                    <tr align="right">
                                                                        <td></td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtRM_REF_NO" CssClass="inputflat" Visible="false" ReadOnly="true" Text="" />
                                                                            <asp:TextBox runat="server" ID="txtREF_NO" CssClass="inputflat" Visible="false" ReadOnly="true" Text="" />
                                                                        </td>
                                                                        <td>Tổng Tiền:
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtTTIEN" Style="text-align: right" CssClass="inputflat" ReadOnly="true" Text="0" />
                                                                        </td>
                                                                    </tr>

                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" colspan="2">
                                                                <table width="550px">
                                                                    <tr align="right">
                                                                        <td align="right"  colspan="6" style="display: none">
                                                                            <span style="font-family: @Arial Unicode MS; font-weight: bold;">Tổng Tiền: </span>
                                                                              <asp:TextBox runat="server" ID="txtTTienx" Width="100px" Style="text-align: right" CssClass="inputflat" ReadOnly="true"
                                                                                Text="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr  align="right"> 

                                                                        <td style="display: none">Phí
                                                                        </td>
                                                                        <td class="auto-style1">
                                                                            <asp:TextBox runat="server" ID="txtCharge" Width="100px" Style="text-align: right" CssClass="inputflat" ReadOnly="true"
                                                                                Text="0" Visible="false" />
                                                                        </td>
                                                                        <td style="display: none">VAT:
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtVAT" Width="100px" Style="text-align: right" CssClass="inputflat" ReadOnly="true"
                                                                                Text="0" Visible="false"/>
                                                                        </td>
                                                                        <td>Tổng trích nợ:
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtTongTrichNo" Style="text-align: right" Width="120px" CssClass="inputflat"
                                                                                ReadOnly="true" Text="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr align="right" style="display: none">
                                                                        <td align="right" valign="top" colspan="6" style="display: none">Lý do hủy: &nbsp;                                                                                                            
                                                                                    <asp:DropDownList ID="drpLyDoHuy" runat="server" Style="width: 60%" CssClass="inputflat">
                                                                                        <asp:ListItem Value="0">Không có trong dữ liệu HQ/NH </asp:ListItem>
                                                                                        <asp:ListItem Value="2">Điện thừa </asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='2' height='5'></td>
                                                        </tr>
                                                    </table>
                                                    <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                                        <tr align="left">
                                                           <%-- <td align="right">Mật khẩu Ks:
                                                                        <asp:TextBox runat="server" ID="txtMatKhau" CssClass="inputflat" Text="" TextMode="Password" />
                                                                &nbsp;
                                                            </td>--%>
                                                            <td align="left" valign="top" class='text'>

                                                                <asp:Button ID="cmdKS" runat="server" Text="Duyệt CT" AccessKey="K" CssClass="ButtonCommand" />
                                                                &nbsp;
                                                                <asp:Button ID="cmdChuyenTra" runat="server" Text="Chuyển trả" AccessKey="C" CssClass="ButtonCommand" />
                                                                &nbsp;
                                                                <asp:Button ID="cmdChuyenTTSP" runat="server" Text="Gửi lại TTSP" AccessKey="S" CssClass="ButtonCommand" />
                                                                &nbsp;
                                                           
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>


                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr height="5">
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
