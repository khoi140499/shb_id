﻿Imports System.Data
Imports VBOracleLib
Imports Business
Imports Business.Common
Imports Newtonsoft.Json
Imports CorebankServiceESB
Imports Business.SP

Partial Class pages_BienLai_frmLapBienLaiGiaoThong
    Inherits System.Web.UI.Page
    Private m_User As Business_HQ.CTuUser
    Private Shared cls_corebank As New CorebankServiceESB.clsCoreBank
    Private Shared cls_songphuong As New CorebankServiceESB.clsSongPhuong
    Private Shared CORE_VERSION As String = ConfigurationManager.AppSettings.Get("CORE.VERSION").ToString()
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    <System.Web.Services.WebMethod()>
    Public Shared Function Huy_CTU(ByVal sSoCT As String, ByVal sLyDo As String) As String
        Dim sRet As String = String.Empty
        Dim sMaNV As String = String.Empty
        Try
            If HttpContext.Current.Session("User") Is Nothing Then
                sRet = ""
                Throw New Exception("errUserSession;Bạn hãy thoát khỏi chương trình và đăng nhập lại")
            End If

            sMaNV = HttpContext.Current.Session("User").ToString()

            sRet = BienLai.buBienLai.HuyChungTu(sSoCT, sLyDo, sMaNV)
        Catch ex As Exception
            sRet = ""
            log.Error(ex.Message + ex.StackTrace + " execute by : " + HttpContext.Current.Session.Item("UserName").ToString())
            Throw New Exception(ex.Message)
        End Try

        Return sRet
    End Function

    Private Shared Function getUrlImageStatus(ByVal pvStrTrangThai As String) As String


        If pvStrTrangThai.Equals("02") Then
            Return "../../images/icons/ChuyenKS.png"
        End If
        If pvStrTrangThai.Equals("03") Then
            Return "../../images/icons/ChuaNhapBDS.png"
        End If
        If pvStrTrangThai.Equals("04") Then
            Return "../../images/icons/KSLoi.png"
        End If
        If pvStrTrangThai.Equals("05") Then
            Return "../../images/icons/Huy.png"
        End If
        If pvStrTrangThai.Equals("08") Then
            Return "../../images/icons/TT_CThueLoi.gif"
        End If
        If pvStrTrangThai.Equals("09") Then
            Return "../../images/icons/DaKS.png"
        End If
        Return ""
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetCTU(ByVal sSoCT As String) As String
        Dim myJavaScript As StringBuilder = New StringBuilder()
        Dim sPhi_VAT_hidden As String = ""
        Dim sRet As String = ""
        Dim sMaNV As String = ""
        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If
            sMaNV = HttpContext.Current.Session("User").ToString()
            Dim objCTU As Business.BienLai.infoCTUBIENLAI = Nothing
            objCTU = Business.BienLai.buBienLai.GetCTUDetail(sSoCT)
            If objCTU IsNot Nothing Then
                'modify something before serialize to json object
                sRet = Newtonsoft.Json.JsonConvert.SerializeObject(objCTU)
            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message + ex.StackTrace + " execute by : " + HttpContext.Current.Session.Item("UserName").ToString())
            Throw New Exception("Có lỗi trong quá trình lấy thông tin chứng từ")
        End Try

        Return sRet
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function LoadCTList(ByVal sMaNV As String, ByVal sSoCtu As String) As String
        Dim dt As DataTable = Nothing
        Dim sTable As String = ""
        Try
            If HttpContext.Current.Session("User") Is Nothing Then
                sTable = ""
                Throw New Exception("errUserSession;Bạn hãy thoát khỏi chương trình và đăng nhập lại")
            End If
            dt = Business.BienLai.buBienLai.GetSoCtu1(sMaNV, sSoCtu, clsCTU.TCS_GetNgayLV(sMaNV))

            sTable &= "<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdDSCT' style='width:100%;border-collapse:collapse;'>"

            Dim i As Integer = 0
            For Each dr As DataRow In dt.Rows
                sTable &= CType(("<tr onclick=jsGetCTU('" & dr("SO_CT").ToString & "') " & "class='" & IIf(i Mod 2 = 0, "grid_item", "grid_item_alter") & "'>"), String)
                sTable &= "<td align='center' style='width: 20%'><img src='" & getUrlImageStatus(dr("TRANG_THAI").ToString()) & "' alt='" & dr("TRANG_THAI").ToString() & "' /></td>"
                sTable &= "<td align='center' style='width: 30%'><a href='#'>" & dr("SO_CT").ToString & "</a></td>"
                sTable &= "<td align='center' style='width: 50%'>" & dr("TEN_DN").ToString() & "</td>"
                sTable &= "</tr>"
            Next

            sTable &= "<tr style='height: 24px;padding-right:2px;'><td align='right' colspan='3'>Tổng số chứng từ: <span style='font-weight:bold'>" & dt.Rows.Count & "</span></td></tr>"

            sTable &= "</table>"
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message + ex.StackTrace + " execute by : " + HttpContext.Current.Session.Item("UserName").ToString())
            sTable = String.Empty
        End Try

        Return sTable
    End Function


    '<System.Web.Services.WebMethod()>
    'Public Shared Function LoadCTList(ByVal sMaNV As String, ByVal sSoCtu As String) As String
    '    Dim dt As DataTable = Nothing
    '    Dim sTable As String = ""
    '    Try
    '        If HttpContext.Current.Session("User") Is Nothing Then
    '            sTable = ""
    '            Throw New Exception("errUserSession;Bạn hãy thoát khỏi chương trình và đăng nhập lại")
    '        End If
    '        dt = Business.BienLai.buBienLai.GetSoCtu(sMaNV, sSoCtu, clsCTU.TCS_GetNgayLV(sMaNV))

    '        sTable &= "<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdDSCT' style='width:100%;border-collapse:collapse;'>"

    '        Dim i As Integer = 0
    '        For Each dr As DataRow In dt.Rows
    '            sTable &= CType(("<tr onclick=jsGetCTU('" & dr("SO_CT").ToString & "') " & "class='" & IIf(i Mod 2 = 0, "grid_item", "grid_item_alter") & "'>"), String)
    '            sTable &= "<td align='center' style='width: 20%'><img src='" & getUrlImageStatus(dr("TRANG_THAI").ToString()) & "' alt='" & dr("TRANG_THAI").ToString() & "' /></td>"
    '            sTable &= "<td align='center' style='width: 30%'><a href='#'>" & dr("SO_CT").ToString & "</a></td>"
    '            sTable &= "<td align='center' style='width: 50%'>" & dr("TEN_DN").ToString() & "</td>"
    '            sTable &= "</tr>"
    '        Next

    '        sTable &= "<tr style='height: 24px;padding-right:2px;'><td align='right' colspan='3'>Tổng số chứng từ: <span style='font-weight:bold'>" & dt.Rows.Count & "</span></td></tr>"

    '        sTable &= "</table>"
    '    Catch ex As Exception
    '        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message + ex.StackTrace + " execute by : " + HttpContext.Current.Session.Item("UserName").ToString())
    '        sTable = String.Empty
    '    End Try

    '    Return sTable
    'End Function




    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTK_KH_NH(ByVal pv_strTKNH As String) As String
        Dim sRet As String = String.Empty
        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If

            Dim ma_loi As String = ""
            Dim so_du As String = ""
            Dim branch_tk As String = ""
            sRet = cls_corebank.GetTK_KH_NH(pv_strTKNH.Replace("-", ""), ma_loi, so_du, branch_tk)
            Return sRet
        Catch ex As Exception
            sRet = String.Empty
            Throw ex
        End Try

        Return sRet
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTEN_KH_NH(ByVal pv_strTKNH As String) As String
        Dim strReturn As String = ""
        Dim ma_loi As String = ""
        Dim so_du As String = ""
        'If CORE_ON_OFF = "1" Then
        strReturn = CorebankServiceESB.clsCoreBank.GetTen_TK_KH_NH(pv_strTKNH.Replace("-", ""))
        'Else
        '    strReturn = "Tài khoản test"
        'End If
        Return strReturn
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function jsLoadLHTHU(ByVal strSHKB As String, ByVal strMACQQD As String) As String
        Dim strReSult As String = ""
        Dim ds As DataSet = DataAccess.ExecuteReturnDataSet("SELECT B.MA_LH,B.TEN_LH FROM  TCS_MAP_CQQD_LHTHU A, TCS_DM_LH_THU_BIENLAI B  WHERE A.SHKB ='" & Business.CTuCommon.SQLInj(strSHKB) & "' AND A.MA_CQQD = '" & Business.CTuCommon.SQLInj(strMACQQD) & "' AND A.SHKB = B.SHKB AND A.MA_LH = B.MA_LH ORDER BY A.MA_LH ", CommandType.Text)
        If Not ds Is Nothing Then
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each vrow As DataRow In ds.Tables(0).Rows
                        If strReSult.Length > 0 Then
                            strReSult &= "#" & vrow("MA_LH").ToString() & "|" & vrow("TEN_LH").ToString()
                        Else
                            strReSult &= "" & vrow("MA_LH").ToString() & "|" & vrow("TEN_LH").ToString()
                        End If
                    Next
                End If
            End If
        End If

        Return strReSult

    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTen_LHThu(ByVal strSHKB As String, ByVal strMACQQD As String) As String
        Try
            Return mdlCommon.GetTen_LHThuBienLai(strMACQQD, strSHKB)
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            '  Throw ex
            Return String.Empty
        End Try
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTen_SHKB(ByVal strSHKB As String) As String
        Try


            Return mdlCommon.Get_TenKhoBac(strSHKB)
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            '  Throw ex
            Return String.Empty
        End Try
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetDM_CQQD(ByVal strMACQQD As String, ByVal strMA_LOAIHINH As String, ByVal strSHKB As String) As String
        Try
            Return mdlCommon.GetDM_CQQDBienLaiGT(strMACQQD, strMA_LOAIHINH, strSHKB)
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            '  Throw ex
            Return String.Empty
        End Try
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTen_CQQD(ByVal strMACQQD As String) As String
        Try

            'If (mdlCommon.Get_TenCQQDBienLaiGT(strMACQQD).Equals(" ") = True) Then
            '    Return String.Empty

            'Else
            '    Return mdlCommon.Get_TenCQQDBienLaiGT(strMACQQD)
            'End If
            Return mdlCommon.Get_TenCQQDBienLaiGT(strMACQQD)
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            '  Throw ex
            Return String.Empty
        End Try
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function jsLOADMAPCQQD_LH(ByVal strSHKB As String, ByVal strMACQQD As String, ByVal strMALH As String) As String
        Try
            Dim strSelect As String = "SELECT A.TK_NS,MA_CHUONG,MA_NDKT,MA_DBHC,MA_DVSDNS,(SELECT MAX(TEN) FROM TCS_DM_MUC_TMUC B WHERE  B.MA_TMUC =A.MA_NDKT ) NOIDUNGKT FROM TCS_MAP_CQQD_LHTHU A"
            strSelect &= " where a.shkb='" & Business.CTuCommon.SQLInj(strSHKB) & "' and a.ma_cqqd='" & Business.CTuCommon.SQLInj(strMACQQD) & "' and a.ma_lh='" & Business.CTuCommon.SQLInj(strMALH) & "' "
            Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSelect, CommandType.Text)

            If Not ds Is Nothing Then
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim strReSult As String = ""
                        strReSult &= "" & ds.Tables(0).Rows(0)("TK_NS").ToString()
                        strReSult &= "#" & ds.Tables(0).Rows(0)("MA_CHUONG").ToString()
                        strReSult &= "#" & ds.Tables(0).Rows(0)("MA_NDKT").ToString()
                        strReSult &= "#" & ds.Tables(0).Rows(0)("MA_DBHC").ToString()
                        strReSult &= "#" & ds.Tables(0).Rows(0)("MA_DVSDNS").ToString()
                        strReSult &= "#" & ds.Tables(0).Rows(0)("NOIDUNGKT").ToString()
                        Return strReSult
                    End If
                End If
            End If

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            '  Throw ex
            Return String.Empty
        End Try
        Return ""
    End Function
    '<System.Web.Services.WebMethod()> _
    'Public Shared Function SearchDanhMuc(ByVal strMa_DThu As String, ByVal strMa_DBHC As String, ByVal strPage As String, ByVal strKey As String, ByVal strTenDM As String, ByVal strSHKB As String, ByVal strMA_LOAIHINH As String) As String
    '    Try
    '        Dim strResult As String = ""
    '        Dim dt As DataTable
    '        Dim strMaDanhMuc As String = strKey.Trim
    '        Dim strTenDanhMuc As String = strTenDM
    '        Dim lovDM As New Business.lovDanhMuc(strMa_DThu, strSHKB)
    '        dt = lovDM.ShowDanhMuc(strPage, strMaDanhMuc, strTenDanhMuc, strMA_LOAIHINH)

    '        If Not Globals.IsNullOrEmpty(dt) Then
    '            Dim strID, strTitle As String
    '            Dim i As Integer = 0
    '            For i = 0 To dt.Rows.Count - 1
    '                If i <= 900 Then
    '                    strID = dt.Rows(i)(0).ToString
    '                    strTitle = dt.Rows(i)(1).ToString
    '                    strResult += strID & ";" & strTitle.Replace("'", " ") & "|"
    '                Else
    '                    Exit For
    '                End If
    '            Next
    '        Else
    '            strResult = ""
    '        End If

    '        Return strResult
    '    Catch ex As Exception
    '        log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
    '        '  Throw ex
    '        Return String.Empty
    '    End Try
    'End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function SearchDanhMuc(ByVal strMa_DThu As String, ByVal strMa_DBHC As String, ByVal strPage As String, ByVal strKey As String, ByVal strTenDM As String, ByVal strSHKB As String, ByVal strMA_LOAIHINH As String) As String
        Try
            Dim strResult As String = ""
            Dim dt As DataTable
            Dim strMaDanhMuc As String = strKey.Trim
            Dim strTenDanhMuc As String = strTenDM
            Dim lovDM As New Business.lovDanhMuc(strMa_DThu, strSHKB)
            dt = lovDM.ShowDanhMuc(strPage, strMaDanhMuc, strTenDanhMuc, strMA_LOAIHINH)

            If Not Globals.IsNullOrEmpty(dt) Then
                Dim strID, strTitle As String
                Dim i As Integer = 0
                For i = 0 To dt.Rows.Count - 1
                    If i <= 900 Then
                        strID = dt.Rows(i)(0).ToString
                        strTitle = dt.Rows(i)(1).ToString
                        strResult += strID & ";" & strTitle.Replace("'", " ") & "|"
                    Else
                        Exit For
                    End If
                Next
            Else
                strResult = ""
            End If

            Return strResult
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message & "-" & ex.StackTrace) '
            '  Throw ex
            Return String.Empty
        End Try
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim sSHKB As String = String.Empty
            Dim sMaDiemThu As String = String.Empty
            Dim sNgay_LV As String = ""

            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/Warning.html", False)
                Exit Sub
            End If
            If Session.Item("User").ToString.Length = 0 Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If Not Session.Item("User") Is Nothing Then
                m_User = New Business_HQ.CTuUser(Session.Item("User"))
                ' sSHKB = clsCTU.TCS_GetKHKB(CType(Session("User"), Integer))
                'sMaDiemThu = clsCTU.TCS_GetMaDT(CType(Session("User"), Integer))
            End If



            If Not IsPostBack Then
                If (Not ClientScript.IsStartupScriptRegistered(Me.GetType(), "InitDefValues")) Then

                    ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTKGL", Business_HQ.HaiQuan.HaiQuanController.PopulateArrTKGL(), True)
                    ClientScript.RegisterStartupScript(Me.GetType(), "InitMaNV", " var curMa_NV =""" & CType(Session("User"), Integer).ToString & """;", True)
                    ClientScript.RegisterStartupScript(Me.GetType(), "InitGL", GetGL(), True)
                End If


            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message + ex.StackTrace + " execute by : " + Session.Item("UserName").ToString())
        End Try
    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function Ghi_CTu2(ByVal ctuHdr As Business.BienLai.infoCTUBIENLAI) As String
        Dim sMaNV As String = ""
        Dim sMa_DThu As String = ""
        Dim sRet As String = String.Empty
        Try

            If HttpContext.Current.Session("User") Is Nothing Then
                sRet = ""
                Throw New Exception("errUserSession;Bạn hãy thoát khỏi chương trình và đăng nhập lại")
            End If

            sMaNV = HttpContext.Current.Session("User")
            sMa_DThu = Business_HQ.HaiQuan.HaiQuanController.GetMaDiemThu(sMaNV)

            If String.IsNullOrEmpty(sMaNV) Then
                sRet = "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
                Throw New Exception(sRet)
            End If

            If ctuHdr IsNot Nothing Then

                ctuHdr.MA_NV = CInt(sMaNV) 'get current user login
                ctuHdr.MA_CN = HttpContext.Current.Session("MA_CN_USER_LOGON")
                'kiem tra KB mo tai MSB, lay ky hieu va so bien lai
                'sinh so chung tu va ky hieu
                Dim vstrKH As String = cls_songphuong.ValiadSHBBienLai(ctuHdr.SHKB)
                If (vstrKH.Length < 8) Then
                    Return "Fail;Kho bạc không mở tài khoản tại SHB! vui lòng kiểm tra lại"
                End If


                Dim strResult = cls_songphuong.ValiadSHB(ctuHdr.SHKB)

                Dim strNHB As String = ""
                If strResult.Length > 8 Then
                    Dim strarr() As String
                    strarr = strResult.Split(";")
                    strNHB = strarr(0).ToString
                End If

                Dim strSql As String = ""

                If strNHB.Length = 8 Then
                    strSql = "Select MA_GIANTIEP  From TCS_DM_NH_GIANTIEP_TRUCTIEP where shkb = '" & Business.CTuCommon.SQLInj(ctuHdr.SHKB) & "' and ma_giantiep='" & strNHB & "'"
                    Dim dt = DataAccess.ExecuteToTable(strSql)
                    If VBOracleLib.Globals.IsNullOrEmpty(dt) Then

                        Return "Fail;kho bạc không mở tại shb. hãy kiểm tra lại"
                    End If
                End If

                Dim arrString As String() = vstrKH.Split(";")

                If ctuHdr.ACTION.Equals("GHIKS") Then

                    ctuHdr.SOBL = Business.SP.SongPhuongController.fnc_GetSoBienLai()

                    ctuHdr.KYHIEU_BL = SongPhuongController.GetKyHieuChungTu(ctuHdr.SHKB, ctuHdr.MA_NV, "04") & ctuHdr.SOBL
                    ctuHdr.MA_NH_B = arrString(0)

                    ' Số RM_REF_NO được trả ra từ coreank
                Else
                    ctuHdr.MA_NH_B = arrString(0)
                End If

                If String.IsNullOrEmpty(ctuHdr.MA_DTHU) Then
                    ctuHdr.MA_DTHU = sMa_DThu
                End If
                ctuHdr.MA_NV = CInt(sMaNV) 'get current user login
                ctuHdr.MA_CN = HttpContext.Current.Session("MA_CN_USER_LOGON")
                ctuHdr.NGAY_KH_NH = IIf(ctuHdr.NGAY_KH_NH Is Nothing, Date.Now.ToString("dd/MM/yyyy"), ctuHdr.NGAY_KH_NH)
                ctuHdr.PRODUCT = "ETAX"
                ctuHdr.KY_THUE = DateTime.Now.ToString("dd/MM/yyyy")
                'ctuHdr.NGAY_KB = Business.BienLai.buBienLai.GetNgayKhoBac(ctuHdr.SHKB)
                'ctuHdr.NGAY_KB = DateTime.Now.ToString("yyyyMMdd")

                'Dim ref_no As String = String.Empty
                'ctuHdr.RM_REF_NO = clsCTU.GenerateRMRef(clsCTU.TCS_GetMaChiNhanh(ctuHdr.MA_NV), ref_no)
                'ctuHdr.REF_NO = ref_no
                Dim objBL As Business.BienLai.buBienLai = New BienLai.buBienLai()

                sRet = objBL.GhiChungTu2(ctuHdr)
            End If

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message + ex.StackTrace + " execute by : " + HttpContext.Current.Session.Item("UserName").ToString())

            Throw ex

        End Try

        Return sRet
    End Function
    Private Function GetGL() As String
        Dim strSQL As String = "select so_tk,ten_tk from tcs_dm_tk_tg"
        Dim strProvice As String = DataAccess.ExecuteSQLScalar(strSQL)

        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrGL = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrGL[" & i & "] ='" & dt.Rows(i)("SO_TK") & ";" & dt.Rows(i)("TEN_TK") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty

    End Function
    Private Shared Function fncGetKyhieubienlai(ByVal pvStrSHKB As String) As String
        Try

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(ex.Message + ex.StackTrace + " execute by : " + HttpContext.Current.Session.Item("UserName").ToString())
        End Try
        Return ""
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function ValidNHSHB_SP(ByVal strSHKB As String) As String
        If (strSHKB.Equals(" ") = False) Then
            Dim strResult As String = ""
            Dim dt As New DataTable
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If

            strResult = cls_songphuong.ValiadSHB(strSHKB)

            Dim strNHB As String = ""
            If strResult.Length > 8 Then
                Dim strarr() As String
                strarr = strResult.Split(";")
                strNHB = strarr(0).ToString
            End If

            Dim strSql As String = ""

            If strNHB.Length = 8 Then
                strSql = "Select MA_GIANTIEP  From TCS_DM_NH_GIANTIEP_TRUCTIEP where shkb = '" & Business.CTuCommon.SQLInj(strSHKB) & "' and ma_giantiep='" & strNHB & "'"
                dt = DataAccess.ExecuteToTable(strSql)
                If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                    Return dt.Rows(0)(0).ToString()
                Else
                    Return ""
                End If
            End If

            Return strNHB
        Else
            Return False
        End If


    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function getSoTK_TruyVanCore(ByVal pv_strTienMat As String) As String
        Dim strReturn As String = ""
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If
        Dim objUser As CTuUser = New CTuUser(HttpContext.Current.Session("User"))
        Dim macn As String = objUser.MA_CN
        'strReturn = getSoTK_TienMat()
        Dim strSQL As String = "SELECT TAIKHOAN_TIENMAT FROM tcs_dm_chinhanh WHERE branch_code = '" & macn & "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Return dt.Rows(0)("TAIKHOAN_TIENMAT").ToString
        Else
            Return ""
        End If
        Return strReturn
    End Function
End Class
