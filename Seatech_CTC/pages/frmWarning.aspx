﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage01.master" AutoEventWireup="false" CodeFile="frmWarning.aspx.vb" Inherits="pages_frmWarning" title="Thông báo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg01_MainContent" Runat="Server">
    <table width="100%" height="470px">
        <tr>
            <td align="center">
                <asp:Label ID="lbThongBao" runat="server" CssClass="form_label" Font-Size="Larger"
                    Font-Bold="true" ForeColor="Red">
                </asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                
                <asp:Button ID="btnDNL" runat="server" Text="Quay trở lại màn hình Đăng nhập" />
                
            </td>
        </tr>
    </table>
</asp:Content>

