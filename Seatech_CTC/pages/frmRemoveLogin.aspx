﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false"
 CodeFile="frmRemoveLogin.aspx.vb" Inherits="pages_frmRemoveLogin" title="Quản lý user đăng nhập" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript" src="../../javascript/seatech_ctc/MyTab.js"></script>

    <link type="text/css" href="../../css/tabStyle.css" rel="stylesheet" />

    <script type="text/javascript">
    function valInteger (obj)
    {
        var i, strVal, blnChange;
        blnChange = false
        strVal = "";
        
        for (i = 0; i < (obj.value).length; i++)
        {
            switch (obj.value.charAt(i))
            {
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9": strVal = strVal + obj.value.charAt(i);
                    break;
                default: blnChange = true;
                    break;
            }
        }
        
        if (blnChange)
            obj.value = strVal;
    }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" runat="Server">
    <table id="Table1" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">QUẢN LÝ USER ĐĂNG NHẬP</asp:Label>
            </td>
        </tr>
        
    </table>
    <br />
   
    <div class="tab_current" id="tab1">
        <div id="DIVTT" runat="server">
            <table border="0" cellspacing="1" cellpadding="1" width="100%" style="text-align: left;"
                class="form_input">
                <tr>
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblUserName" runat="server" Text="Tên đăng nhập" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:TextBox ID="txtTenND" runat="server" Width="120px" CssClass="inputflat"></asp:TextBox>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="cmdTimKiem" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm" UseSubmitBehavior="false">
                </asp:Button>
                    </td>
                    <%--<td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblCustName" runat="server" Text="Tên người dùng" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                    
                        <asp:TextBox ID="txtHoTen" runat="server" Width="120px" CssClass="inputflat"></asp:TextBox>
                    </td>--%>
                </tr>
               <%-- <tr>
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblBranchNo" runat="server" Text="Mã chi nhánh" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label" colspan="3">
                        <asp:TextBox ID="txtMaCN" runat="server" Width="120px" CssClass="inputflat" ></asp:TextBox>
                        <asp:TextBox ID="txtTenCN" runat="server" Width="200" CssClass="inputflat"></asp:TextBox>
                    </td>
                   
                </tr>--%>
               
            </table>
        </div>
    </div>
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdHuy" runat="server" CssClass="ButtonCommand" Text="Hủy ĐN" TabIndex="5" UseSubmitBehavior="false">
                </asp:Button>&nbsp;&nbsp;
               
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:DataGrid ID="dgUserAccount" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="99%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle  CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="Ma_NV" HeaderText="Ma_NV " Visible="false" >
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                         <asp:BoundColumn DataField="Ten_DN" HeaderText=" Tên ĐN " >
                            <HeaderStyle Width="120"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="120"></ItemStyle>
                        </asp:BoundColumn>
                     <%--   <asp:TemplateColumn HeaderText="Tên ĐN" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ten_DN") %>'
                                    NavigateUrl='<%#"~/pages/frmRemoveLogin.aspx?ID=" & DataBinder.Eval(Container.DataItem, "Ten_DN") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle Width="70px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:TemplateColumn>--%>
                        <asp:BoundColumn DataField="Ten" HeaderText="Họ tên">
                            <HeaderStyle Width="150px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Ma_cn" HeaderText="Mã chi nhánh">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TEN_NH_CN" HeaderText="Tên chi nhánh">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>

