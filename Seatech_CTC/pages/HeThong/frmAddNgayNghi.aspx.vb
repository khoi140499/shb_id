﻿Imports Business.Common.mdlCommon
Imports Business.DanhMuc.NgayNghi

Imports System.Data

Partial Class pages_HeThong_frmAddNgayNghi
    Inherits System.Web.UI.Page
    Private objngay_nghi As New buNgayNghi

    Protected Sub cmdGhi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGhi.Click
        txtNN_Ghichu.Text = RemoveApostrophe(txtNN_Ghichu.Text)

        Dim objNgaynghi As New Business.DanhMuc.NgayNghi.infNgayNghi()
        If Request.Params("id") Is Nothing Then
            If txtNgayNghi.Value.Trim = "" Then
                clsCommon.ShowMessageBox(Me, "Ngày nghỉ không được để trống!")
                txtNgayNghi.Focus()
                Return
            End If
            
            'If txtNN_Ghichu.Text.Trim = "" Then
            '    'clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên điểm thu.")
            '    txtNN_Ghichu.Focus()
            '    Exit Sub
            'End If
            Try
                objNgaynghi.NgayNghi = txtNgayNghi.Value.ToString.Trim
                objNgaynghi.GhiChu = txtNN_Ghichu.Text.ToString.Trim

                If txtNgayNghi.Value.Trim <> "" Then
                    Dim a As Byte = objngay_nghi.checkExist(objNgaynghi)
                    If a = 1 Then
                        clsCommon.ShowMessageBox(Me, "Ngày nghỉ đã tồn tại trong hệ thống!")
                        txtNgayNghi.Focus()
                        Return
                    Else

                    End If
                End If
                Dim objInsert As New Business.DanhMuc.NgayNghi.buNgayNghi("ADD")
                If objInsert.Save(objNgaynghi) Then
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                Else
                    txtNgayNghi.Focus()
                    ' txtNgayNghi..SelectAll()
                End If
            Catch ex As Exception
                If objngay_nghi.Status = "EDIT" Then
                    clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình cập nhật ngày nghỉ !")
                Else
                    clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình tạo mới ngày nghỉ !")
                End If
                Throw ex
            Finally
            End Try
        Else
            'If txtNN_Ghichu.Text.Trim = "" Then
            '    clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên điểm thu.")
            '    txtNN_Ghichu.Focus()
            '    Exit Sub
            'End If
            Try
                objNgaynghi.NgayNghi = txtNgayNghi.Value.ToString.Trim
                objNgaynghi.GhiChu = txtNN_Ghichu.Text.ToString.Trim
                Dim objInsert As New Business.DanhMuc.NgayNghi.buNgayNghi("EDIT")
                If objInsert.Save(objNgaynghi) Then
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                Else
                    txtNgayNghi.Focus()
                End If
            Catch ex As Exception
                If objngay_nghi.Status = "ADD" Then
                    clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình cập nhật ngày nghỉ !")
                Else
                    clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình tạo mới ngày nghỉ !")
                End If
                Throw ex
            Finally
            End Try
        End If
        cmdCancel_Click(sender, e)
        load_dataGrid()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdGhi.Click, AddressOf cmdGhi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            load_dataGrid()
            cmdGhi.Enabled = True
            If Not Request.Params("id") Is Nothing Then
                txtNgayNghi.EnableTheming = False
                loadDetail(Request.Params("id").Trim)
            End If
        End If
        'cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn xóa dữ liệu đã chọn?') ")
    End Sub
    Private Sub load_dataGrid()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtNgayNghi.EnableTheming Then
                If txtNgayNghi.Value <> "" Then
                    WhereClause += " and ngay_nghi =to_date('" & txtNgayNghi.Value & "','dd/MM/yyyy')"
                End If
                If txtNN_Ghichu.Text <> "" Then
                    WhereClause += " AND ghi_chu like '%" + txtNN_Ghichu.Text + "%'"
                End If
            End If
            ds = objngay_nghi.GetDataSet(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgdata.DataSource = ds.Tables(0)
                dtgdata.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub

    Private Sub loadDetail(ByVal ngaynghi As String)
        Dim objNgayNghi As New Business.DanhMuc.NgayNghi.infNgayNghi()
        objNgayNghi.NgayNghi = ngaynghi
        Dim objLoad As New Business.DanhMuc.NgayNghi.buNgayNghi()
        Dim ds As DataSet
        ds = objLoad.loadDetail(objNgayNghi)
        If Not ds Is Nothing Then
            txtNgayNghi.Value = ds.Tables(0).Rows(0)("ngay_nghi").ToString
            txtNN_Ghichu.Text = ds.Tables(0).Rows(0)("ghi_chu").ToString
        End If
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.txtNgayNghi.Value = ""
        Me.txtNN_Ghichu.Text = ""
        cmdGhi.Enabled = True
        If Not Request.Params("id") Is Nothing Then
            Response.Redirect("~/pages/HeThong/frmAddNgayNghi.aspx")
        End If
    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim i As Integer
        Dim strngay_nghi As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True
        Dim objDel As New Business.DanhMuc.NgayNghi.buNgayNghi()
        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strngay_nghi = Trim(dtgdata.Items(i).Cells(0).Text)
                If strngay_nghi = "" Then
                    clsCommon.ShowMessageBox(Me, "Bạn chưa chọn ngày để xóa")
                    Return
                End If
                If (objDel.DelNgayNghi(strngay_nghi)) Then
                Else
                    clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                    Return
                End If
            v_count += 1
            End If
        Next
        load_dataGrid()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        load_dataGrid()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        dtgdata.CurrentPageIndex = 0
        load_dataGrid()
        cmdGhi.Enabled = False
    End Sub
    Protected Sub cmdDongBo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDongBo.Click
        dtgdata.CurrentPageIndex = 0

        Dim clscore As CorebankServiceESB.clsCoreBank = New CorebankServiceESB.clsCoreBank

        Dim result As String = clscore.DongBoNgayNghi()
        clsCommon.ShowMessageBox(Me, result)

        load_dataGrid()

    End Sub
End Class
