﻿Imports Business.Common.mdlCommon
Imports Business.DanhMuc
Imports System.Data
Imports VBOracleLib
Imports ProcMegService
Imports System.Diagnostics

Partial Class pages_HeThong_DanhMuc_frmDMCQT
    Inherits System.Web.UI.Page
    Dim objCQT As New CQThu.buCQThu()


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            EnableButton_Nhom(Session.Item("MA_NHOM"))
            hdfTenDN.Value = Session.Item("UserName")
            'load_dataGrid()
            load_cboDiemThu()
            cmdTaoMoi.Enabled = True
            If Not Request.Params("id") Is Nothing Then
                txtMaCQT.Enabled = False
                load_detail(Request.Params("id").Trim)
            End If
            'cmdTaoMoi.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn thực hiện không?')")
            'cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn thực hiện không?')")
        End If
    End Sub

    Protected Sub load_cboDiemThu()

        Dim ds As New DataSet
        Dim strSQL As String
        Try

            strSQL = "SELECT  a.shkb MA_DThu,a.TEN FROM TCS_DM_khobac a " & _
                        " ORDER BY shkb"
            ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(cboDiemThu, ds, 1, 0, "Tất cả")
            'cboDiemThu.DataSource = ds.Tables(0)
            'cboDiemThu.DataTextField = ds.Tables(0).Columns("Ten").ToString
            'cboDiemThu.DataValueField = ds.Tables(0).Columns("MA_DThu").ToString
            'cboDiemThu.DataBind()
        Catch ex As Exception
            'Lỗi kết nối cơ sở dữ liệu
            clsCommon.ShowMessageBox(Me, "Lỗi kết nối CSDL")
        Finally
            If Not ds Is Nothing Then
                ds.Dispose()
            End If
           
        End Try
    End Sub

    Protected Sub load_dataGrid()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtMaCQT.Text <> "" Then
                WhereClause += " AND upper(c.ma_cqthu) like upper('" + txtMaCQT.Text.Trim + "%')"
            End If
            If txtMaCQT.Enabled Then
                If txtTenCQT.Text <> "" Then
                    WhereClause += " AND UPPER(TRIM(c.ten)) LIKE UPPER('%" + txtTenCQT.Text.Trim + "%')"
                End If
            End If
            If txtMaHQ.Enabled Then
                If txtMaHQ.Text <> "" Then
                    WhereClause += " AND UPPER(TRIM(c.MA_HQ)) LIKE UPPER('%" + txtMaHQ.Text.Trim + "%')"
                End If
            End If
            If txtMaQLT.Enabled Then
                If txtMaQLT.Text <> "" Then
                    WhereClause += " AND UPPER(TRIM(c.MA_QLT)) LIKE UPPER('%" + txtMaQLT.Text.Trim + "%')"
                End If
            End If
            If txtMaTinh.Enabled Then
                If txtMaTinh.Text <> "" Then
                    WhereClause += " AND UPPER(TRIM(c.DBHC_TINH)) LIKE UPPER('%" + txtMaTinh.Text.Trim + "%')"
                End If
            End If
            If txtMaDBHC.Enabled Then
                If txtMaDBHC.Text <> "" Then
                    WhereClause += " AND UPPER(TRIM(c.DBHC_HUYEN)) LIKE UPPER('%" + txtMaDBHC.Text.Trim + "%')"
                End If
            End If
            If cboDiemThu.SelectedValue <> "" Then
                WhereClause += " AND c.shkb  = '" + cboDiemThu.SelectedValue + "'"
            End If
            ds = objCQT.Load(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgdata.DataSource = ds.Tables(0)
                dtgdata.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không tìm thấy dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub
    Protected Sub load_dataGrid_new()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtMaCQT.Text <> "" Then
                WhereClause += " AND upper(b.ma_cqthu) like upper('" + txtMaCQT.Text.Trim + "%')"
            End If
            If txtMaCQT.Enabled Then
                If txtTenCQT.Text <> "" Then
                    WhereClause += " AND UPPER(TRIM(b.ten)) LIKE UPPER('%" + txtTenCQT.Text.Trim + "%')"
                End If
            End If
            If txtMaHQ.Enabled Then
                If txtMaHQ.Text <> "" Then
                    WhereClause += " AND UPPER(TRIM(b.MA_HQ)) LIKE UPPER('%" + txtMaHQ.Text.Trim + "%')"
                End If
            End If
            If txtMaQLT.Enabled Then
                If txtMaQLT.Text <> "" Then
                    WhereClause += " AND UPPER(TRIM(b.MA_QLT)) LIKE UPPER('%" + txtMaQLT.Text.Trim + "%')"
                End If
            End If
            If cboDiemThu.SelectedValue <> "" Then
                WhereClause += " AND b.ma_dthu  = '" + cboDiemThu.SelectedValue + "'"
            End If
            If txtMaTinh.Enabled Then
                If txtMaTinh.Text <> "" Then
                    WhereClause += " AND UPPER(TRIM(b.DBHC_TINH)) LIKE UPPER('%" + txtMaTinh.Text.Trim + "%')"
                End If
            End If
            If txtMaDBHC.Enabled Then
                If txtMaDBHC.Text <> "" Then
                    WhereClause += " AND UPPER(TRIM(b.DBHC_HUYEN)) LIKE UPPER('%" + txtMaDBHC.Text.Trim + "%')"
                End If
            End If
            If drpTT_XU_LY.SelectedValue.ToString() <> "" Then
                WhereClause += " AND b.TT_XU_LY = '" + drpTT_XU_LY.SelectedValue.ToString() + "'"
            End If
            ds = objCQT.Load_new(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                grd_new.DataSource = ds.Tables(0)
                grd_new.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không tìm thấy dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub

    Protected Sub load_detail(ByVal str As String)
        Dim ds As DataSet
        ds = objCQT.Load(" AND c.ma_cqthu = '" + str + "'")
        If ds.Tables(0).Rows.Count > 0 Then
            txtMaCQT.Text = ds.Tables(0).Rows(0)("ma_cqthu").ToString
            txtTenCQT.Text = ds.Tables(0).Rows(0)("ten").ToString
            cboDiemThu.SelectedValue = ds.Tables(0).Rows(0)("ma_dthu").ToString
            txtMaHQ.Text = ds.Tables(0).Rows(0)("ma_hq").ToString
            txtMaQLT.Text = ds.Tables(0).Rows(0)("ma_qlt").ToString
            txtMaTinh.Text = ds.Tables(0).Rows(0)("dbhc_tinh").ToString
            txtMaDBHC.Text = ds.Tables(0).Rows(0)("dbhc_huyen").ToString
            txtTenTinh.Text = GetTenTinh(txtMaTinh.Text)
            txtTenDBHC.Text = GetTenDBHC(txtMaDBHC.Text)
            dtgdata.DataSource = ds.Tables(0)
            dtgdata.DataBind()
        End If
    End Sub

    Private Sub xoaText()
        txtMaCQT.Text = ""
        txtTenCQT.Text = ""
        txtMaDBHC.Text = ""
        txtMaTinh.Text = ""
        txtTenTinh.Text = ""
        txtTenDBHC.Text = ""
        txtMaQLT.Text = ""
        txtMaHQ.Text = ""
        txtMaCQT.Enabled = True
    End Sub

    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        If txtMaCQT.Text.Trim.Length = 0 Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã Cơ quan thu")
            txtMaCQT.Focus()
            Exit Sub

            '*****TYNK - Check ky tu dac biet*********07/02/2013
        Else
            If Regex.IsMatch(txtMaCQT.Text.Trim, "^[a-zA-Z0-9]*$") = False Then
                clsCommon.ShowMessageBox(Me, "Chuỗi bạn nhập không đúng - Có ký tự đặc biệt.")
                txtMaCQT.Focus()
                Exit Sub
            Else

            End If
            '***************************************************
        End If
        If txtTenCQT.Text.Trim.Length = 0 Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên Cơ quan thu")
            txtTenCQT.Focus()
            Exit Sub
        End If
        If cboDiemThu.SelectedValue.Equals("") Then
            clsCommon.ShowMessageBox(Me, "Điểm thu phải được chọn, vui lòng kiểm tra lại.!")
            Exit Sub
        End If
        Dim infCQT As New CQThu.infCQThu()
        infCQT.Ma_Dthu = cboDiemThu.SelectedValue
        infCQT.Ma_CQThu = txtMaCQT.Text.Trim
        infCQT.Ten = txtTenCQT.Text.Trim
        infCQT.Ma_HQ = txtMaHQ.Text.Trim
        infCQT.Ma_QLT = txtMaQLT.Text.Trim
        infCQT.MA_TINH = txtMaTinh.Text.Trim
        infCQT.MA_HUYEN = txtMaDBHC.Text.Trim
        infCQT.NGUOI_TAO = hdfTenDN.Value
        Try
            If Request.Params("id") Is Nothing Then
                infCQT.TYPE = "1" 'insert
                'Nếu có không có thay đổi chờ xử lý
                If Not Check_Insert(infCQT.Ma_CQThu) Then
                    If objCQT.Insert(infCQT, "10") = False Then
                        clsCommon.ShowMessageBox(Me, "Mã cơ quan thu đã tồn tại trong hệ thống.!")
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Thông tin mã chương này đã tồn tại và đang chờ xử lý!")
                End If
            Else
                infCQT.TYPE = "2" 'update
                'Nếu có không có thay đổi chờ xử lý
                If Not Check_ThayDoi(infCQT) Then
                    If objCQT.Update(infCQT, "10") = False Then
                        clsCommon.ShowMessageBox(Me, "Mã cơ quan thu đã tồn tại trong hệ thống.!")
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                End If
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Thực hiện không thành công !")
            Return
        End Try
        clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
        load_dataGrid()
        xoaText()
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        xoaText()
        cmdTaoMoi.Enabled = True
        txtMaCQT.Enabled = True
        load_dataGrid()
        If Not Request.Params("id") Then
            Response.Redirect("~/pages/HeThong/DanhMuc/frmDMCQT.aspx")
        End If
    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim i As Integer
        Dim strmaCQT As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn thực hiện không?')")
                strmaCQT = Trim(dtgdata.Items(i).Cells(0).Text)
                If objCQT.CheckDelete(strmaCQT) Then
                    clsCommon.ShowMessageBox(Me, "Dữ liệu đã sử dụng trong chức năng khác, không được xoá!")
                Else
                    Dim objMLNS As New CQThu.infCQThu
                    objMLNS.TYPE = "3" 'xóa dữ liệu
                    objMLNS.NGUOI_TAO = hdfTenDN.Value
                    objMLNS.Ma_CQThu = strmaCQT
                    If Not Check_ThayDoi(objMLNS) Then
                        'Get_Data_Item(objMLNS)
                        If objCQT.Delete(objMLNS, "10") Then
                            clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                        Else
                            clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình xóa dữ liệu!")
                        End If
                    Else
                        clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                    End If
                End If
                v_count += 1
            End If
        Next
        '***********TYNK - Load lại dữ liệu sau khi đã xóa bản ghi - 20/02/2013**************
        xoaText()
        load_dataGrid()
        '***********************************************************************
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub

    Protected Sub dtgdata_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgdata.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If Not MaNhom = "10" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim checkBox As HyperLink = DirectCast(e.Item.FindControl("Hyperlink1"), HyperLink)
                checkBox.Enabled = False
            End If
        End If
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        load_dataGrid()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        If drpDuLieu.SelectedIndex = 0 Then
            load_dataGrid()
            dtgdata.CurrentPageIndex = 0
            dtgdata.DataBind()
            grd_new.Visible = False
            dtgdata.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = False
                btnTuChoi.Visible = False
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = True
                'cmdIn.Visible = True
                cmdDeleteUser.Visible = True
                cmdTaoMoi.Visible = True
                cmdTruyvanThue.Visible = True
            End If
        Else
            load_dataGrid_new()
            grd_new.CurrentPageIndex = 0
            grd_new.DataBind()
            dtgdata.Visible = False
            grd_new.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = True
                btnTuChoi.Visible = True
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = False
                'cmdIn.Visible = False
                cmdDeleteUser.Visible = False
                cmdTaoMoi.Visible = False
                cmdTruyvanThue.Visible = False
            End If
        End If
        cmdTaoMoi.Enabled = False
    End Sub

    Protected Sub cmdTruyvanThue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTruyvanThue.Click
        'thêm aleert confirm trước khi làm
        Try
            ProcMegService.ProMsg.getMSG07(MessageType.strMSG009, "Trao đổi danh mục CQT")
            load_dataGrid()
            lblError.Text = "Đồng bộ danh mục thành công"
        Catch ex As Exception
            lblError.Text = ex.Message.ToString()
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Lỗi trong quá trình đồng bộ dữ liệu Cơ quan thu từ TC Thuế : Mô tả=" & ex.Message.ToString())
        End Try
    End Sub
    Protected Sub EnableButton_Nhom(ByVal Nhom As String)
        Nhom = clsCommon.Check_Maker_Checker(Nhom)
        hdfMa_Nhom.Value = Nhom
        If Nhom = "10" Then
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = True
            'cmdIn.Visible = True
            cmdDeleteUser.Visible = True
            cmdTaoMoi.Visible = True
            cmdTruyvanThue.Visible = True
            drpDuLieu.SelectedIndex = 0
            load_dataGrid()
            cmdTaoMoi.Enabled = True
        ElseIf Nhom = "11" Then
            btnDuyet.Visible = True
            btnTuChoi.Visible = True
            'rowTrangThai.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            cmdTruyvanThue.Visible = False
            drpDuLieu.SelectedIndex = 1
            load_dataGrid_new()
        Else
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            cmdTruyvanThue.Visible = False
            drpDuLieu.SelectedIndex = 0
            load_dataGrid()
        End If
    End Sub
    Protected Sub Get_Data_Item(ByRef objItem As CQThu.infCQThu)
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_dm_CQTHU WHERE ID=" & objItem.ID
        dt = DataAccess.ExecuteToTable(WhereClause)
        objItem.Ten = dt.Rows(0)("ten").ToString
        objItem.Ma_CQThu = dt.Rows(0)("Ma_CQThu").ToString
        objItem.Ma_Dthu = dt.Rows(0)("Ma_Dthu").ToString
        objItem.Ma_HQ = dt.Rows(0)("Ma_HQ").ToString
        objItem.Ma_QLT = dt.Rows(0)("Ma_QLT").ToString
        objItem.MA_TINH = dt.Rows(0)("DBHC_TINH").ToString
        objItem.MA_HUYEN = dt.Rows(0)("DBHC_HUYEN").ToString
    End Sub
    'Kiểm tra có thay đổi thông tin đang chờ xử lý ko
    'Nếu có thay đổi thì trả về True/False
    Protected Function Check_ThayDoi(ByRef objItem As CQThu.infCQThu) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_dm_CQTHU WHERE MA_CQTHU=" & objItem.Ma_CQThu & " AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function
    Protected Function Check_Insert(ByRef MA_CQTHU As String) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_dm_CQTHU WHERE MA_CQTHU=" & MA_CQTHU & " AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function

    Protected Sub grd_new_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_new.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If MaNhom = "10" Then
            e.Item.Cells(14).Visible = False
        ElseIf MaNhom = "11" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim tt_xu_ly As String = e.Item.Cells(12).Text.ToString
                If tt_xu_ly <> "0" Then
                    Dim checkBox As CheckBox = DirectCast(e.Item.FindControl("chkSelect"), CheckBox)
                    checkBox.Visible = False
                End If
            End If
        Else
            e.Item.Cells(14).Visible = False
        End If
    End Sub

    Protected Sub grd_new_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grd_new.PageIndexChanged
        load_dataGrid_new()
        grd_new.CurrentPageIndex = e.NewPageIndex
        grd_new.DataBind()
    End Sub

    Protected Sub btnDuyet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuyet.Click
        Dim i As Integer
        Dim strID As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                Dim objMLNS = New CQThu.infCQThu
                objMLNS.ID = strID
                objMLNS.NGUOI_DUYET = hdfTenDN.Value
                objMLNS.TT_XU_LY = "1"
                Dim objInsert As New CQThu.buCQThu
                Get_Data_Item(objMLNS)
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        load_dataGrid_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Duyệt thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi duyệt!")
        End If
    End Sub

    Protected Sub btnTuChoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTuChoi.Click
        Dim i As Integer
        Dim strID As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                Dim objMLNS = New CQThu.infCQThu
                objMLNS.ID = strID
                objMLNS.NGUOI_DUYET = hdfTenDN.Value
                objMLNS.TT_XU_LY = "2"
                Dim objInsert As New CQThu.buCQThu
                'Duyệt thêm mới
                If strType = "1" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        load_dataGrid_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Từ chối thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi từ chối!")
        End If
    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenTinh(ByVal strMaTinh As String) As String
        Dim strTenTinh As String = ""
        Try
            Dim strSQL As String = "SELECT ten from tcs_dm_xa where ma_xa = '" & strMaTinh & "'"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                strTenTinh = dt.Rows(0)("ten").ToString()
            End If
        Catch ex As Exception
            strTenTinh = ""

        End Try
        Return strTenTinh
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenDBHC(ByVal strMaDBHC As String) As String
        Dim strTenDBHC As String = ""
        Try
            Dim strSQL As String = "SELECT ten from tcs_dm_xa where ma_xa = '" & strMaDBHC & "'"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                strTenDBHC = dt.Rows(0)("ten").ToString()
            End If
        Catch ex As Exception
            strTenDBHC = ""

        End Try
        Return strTenDBHC
    End Function
End Class
