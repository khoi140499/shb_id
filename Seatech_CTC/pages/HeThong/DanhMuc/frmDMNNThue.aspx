﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmDMNNThue.aspx.vb" Inherits="pages_HeThong_DanhMuc_frmDMNNThue" title="Danh mục người nộp thuế" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
<table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">TRA CỨU THÔNG TIN NGƯỜI NỘP THUẾ</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <input type="hidden" runat="server" id="Hidden1" />
        </tr>
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%"
                    class="form_input">
                       <tr align="left">
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label1" runat="server" CssClass="label">Tra cứu theo</asp:Label>
                        </td>
                        <td class="form_control">
                        <asp:DropDownList runat ="server" ID ="ddlHTTT" CssClass="inputflat" AutoPostBack="true">
                        <asp:ListItem Value="MaSoThue" Text="Mã số thuế" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="ChungMinhThu" Text="Chứng minh thư" ></asp:ListItem>
                        </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="left">
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label3" runat="server" CssClass="label">Mã số thuế/Số CMT</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMaNNT" runat="server" MaxLength="14" Width="30%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>
                    </tr>
                     
                    <%-- <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label5" CssClass="label" runat="server">Loại thuế</asp:Label>
                        </td>
                        <td class="form_control">
                           <asp:DropDownList ID="cboLoaiThue" runat="server" CssClass="inputflat">
                                <asp:ListItem Text="--Tất cả--" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Thuế Nội địa" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Thuế Hải quan" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>--%>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdSearch" runat="server" CssClass="ButtonCommand" Text="Tra cứu">
                </asp:Button>&nbsp; &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
             <%--<div style="vertical-align :top; height: 300px; overflow: auto; width: 600px;">--%>
                <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True" 
                    AllowSorting="False" HorizontalAlign="Right" PageSize="50" >
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="ma_nnt" HeaderText="Mã NNT">
                            <HeaderStyle Width="40px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ten_nnt" HeaderText="Tên NNT">
                            <HeaderStyle Width="150px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"  Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="dia_chi" HeaderText="Địa chỉ NNT">
                            <HeaderStyle Width="100px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"  Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_tinh" HeaderText="Mã tỉnh">
                            <HeaderStyle Width="20px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"  Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_huyen" HeaderText="Mã huyện">
                            <HeaderStyle Width="20px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"  Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_xa" HeaderText="Mã xã">
                            <HeaderStyle Width="20px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"  Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_cqthu" HeaderText="Mã CQT">
                            <HeaderStyle Width="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"  Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_chuong" HeaderText="Mã chương">
                            <HeaderStyle Width="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="trang_thai" HeaderText="Trạng thái">
                            <HeaderStyle Width="20px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_loai_nnt" HeaderText="Mã loại NNT">
                            <HeaderStyle Width="30px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="so_cmt" HeaderText="Số CMT">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ngay_cap_mst" HeaderText="Ngày ĐK">
                            <HeaderStyle Width="100px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
                <%--</div>--%>
            </td>
        </tr>
    </table>
</asp:Content>

