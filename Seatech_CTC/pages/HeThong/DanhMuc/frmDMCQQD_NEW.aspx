﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmDMCQQD_NEW.aspx.vb" Inherits="pages_HeThong_DanhMuc_frmDMCQQD_NEW"
    Title="Danh mục mã cqqd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .tbl {
        }
            .tbl td {
                padding:3px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">

      <script language="javascript" type="text/javascript">
          function ShowLov(strType) {
              if (strType == "SHKB") return FindDanhMuc('KhoBac', '<%=txtMA_KB.ClientID%>', '<%=txtTen_KB.ClientID%>', '<%=txtMA_KB.ClientID%>');
         }
         function FindDanhMuc(strPage, txtID, txtTitle, txtFocus) {
             var strSHKB;
             var returnValue;
             if (document.getElementById('<%=txtMA_KB.ClientID%>').value.trim().length > 0) {
                 strSHKB = document.getElementById('<%=txtMA_KB.ClientID%>').value;
                 returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&SHKB=" + strSHKB + "&initParam=" + $get(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;", "_new");
             }
             else {
                 returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&initParam=" + document.getElementById(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;", "_new");
             }
             document.getElementById(txtID).value = returnValue["ID"];
             document.getElementById(txtTitle).value = returnValue["Title"];

         }

         function jsGet_TenKB() {
             var txt = document.getElementById('<%=txtMA_KB.ClientID%>').value;
            //txt.value ='ok';
            PageMethods.GetTenKB(txt, get_TenKB_Complete, get_TenKB__Error)
        }

        function get_TenKB_Complete(result, methodname) {
            var txt = document.getElementById('<%=txtTen_KB.ClientID%>');
        if (result.length > 0 && result.toString() != 'Null') {
            txt.value = result;
        }
        else {
            txt.value = '-Số hiệu kho bạc không đúng-';
        }
    }

    function get_TenKB__Error(error, context, methodname) {
        var txt = document.getElementById('<%=txtTen_KB.ClientID%>');
        txt.value = '';
    }
    function XacNhan() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("Bạn có chắc chắn muốn xóa nội dung này?")) {
            confirm_value.value = "Có";
        } else {
            confirm_value.value = "Không";
        }
        document.forms[0].appendChild(confirm_value);
    }
</script>

    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">DANH MỤC CƠ QUAN QUYẾT ĐỊNH</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <asp:HiddenField ID="hdfMa_Nhom" runat="server" />
            <asp:HiddenField ID="hdfTenDN" runat="server" />
        </tr>
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%"
                    class="tbl">
                    <tr align="left">
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label9" CssClass="label" runat="server">  Mã CQ quyết định</asp:Label>
                         
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMa_CQQD" runat="server" MaxLength="30" Width="30%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                            </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label8" CssClass="label" runat="server"> Tên CQ Quyết định</asp:Label>
                       
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtTen_CQQD" runat="server" MaxLength="200" Width="50%" CssClass="inputflat"
                                TabIndex="2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label7" CssClass="label" runat="server"> Mã CQ thu</asp:Label>
                         
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMa_CQTHU" runat="server" MaxLength="7" Width="20%" CssClass="inputflat"
                                TabIndex="3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label6" CssClass="label" runat="server">Mã kho bạc</asp:Label>
                          
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMa_KB" runat="server" MaxLength="4" Width="20%" CssClass="inputflat"
                                TabIndex="4"></asp:TextBox>
                             <input type="text" runat="server"  id="txtTen_KB" style="width:70%" disabled="disabled"/>
                        </td>
                    </tr>
                     <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label3" CssClass="label" runat="server">Mã địa bàn HC</asp:Label>
                          
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMa_DBHC" runat="server" MaxLength="5" Width="20%" CssClass="inputflat"
                                TabIndex="3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label2" CssClass="label" runat="server"> Ghi chú</asp:Label>
                           
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtGhiChu" runat="server" MaxLength="200" Width="90%" CssClass="inputflat"
                                TabIndex="5"></asp:TextBox>
                        </td>
                    </tr>

                      <tr align="left" id="rowTrangThai" runat="server">
                        <td class="form_label">
                            <asp:Label ID="lblPasswordCaption" CssClass="label" runat="server">Trạng thái</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID="cboTinh_trang" runat="server" CssClass="inputflat" Width="30%">
                                <asp:ListItem Text="Đang hoạt động" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Ngừng hoạt động" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>

                     <tr align="left" id="Tr1" runat="server">
                        <td class="form_label">
                            <asp:Label ID="Label4" CssClass="label" runat="server">Thông tin dữ liệu</asp:Label>
                        </td>
                        <td class="form_control">
                           <asp:DropDownList ID="drpDuLieu" runat="server" CssClass="inputflat" Width="30%">
                                <asp:ListItem Text="Dữ liệu hiện tại" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Dữ liệu thay đổi(thêm/sửa/xóa)" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>

                      <tr align="left" id="Tr2" runat="server">
                        <td class="form_label">
                            <asp:Label ID="Label5" CssClass="label" runat="server">Trạng thái xử lý</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID="drpTT_XU_LY" runat="server" CssClass="inputflat" Width="30%">
                                <asp:ListItem Text="Tất cả" Value=""></asp:ListItem>
                                <asp:ListItem Text="Chờ duyệt" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Đã duyệt" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Từ chối" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr align="left">
                        <td class="form_label">

                            <asp:Label ID="Label1" CssClass="label" runat="server">Thời gian/ người sửa</asp:Label>
                          
                        </td>
                        <td class="form_control">
                           <input type="text" runat="server"  id="txtMDate" style="width:30%"  disabled="disabled"/>
                             <input type="text" runat="server"  id="txtMAKER" style="width:60%" disabled="disabled"/>
                        </td>
                    </tr>
                    <tr align="left" style="display:none">
                        <td class="form_control">
                            <asp:TextBox ID="txtIDSE" runat="server" MaxLength="200" Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr> 

                     
                </table>
            </td>
        </tr>
        <tr>
             <td style="height: 40px;" align="center">
                    <asp:Button ID="cmdSearch" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdTaoMoi" runat="server" CssClass="ButtonCommand" Text="Ghi" UseSubmitBehavior="false">
                </asp:Button>
              
                <asp:Button ID="cmdDeleteUser" runat="server" CssClass="ButtonCommand" Text="Xóa"  OnClientClick="XacNhan();">
                </asp:Button>&nbsp;&nbsp;
                <asp:Button ID="btnDuyet" runat="server" CssClass="ButtonCommand" Text="Duyệt" UseSubmitBehavior="false">
                </asp:Button>
                 <asp:Button ID="btnTuChoi" runat="server" CssClass="ButtonCommand" Text="Từ chối" UseSubmitBehavior="false"></asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
             <%--<div class="inputflat" style="vertical-align :top; height: 300px; overflow: auto; width: 100%;">--%>
                  <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="ID" HeaderText="ID" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="ID" >
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ID")%>'
                                    NavigateUrl='<%#"~/pages/HeThong/DanhMuc/frmDMCQQD_NEW.aspx?ID=" & DataBinder.Eval(Container.DataItem, "ID")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="MA_CQQD" HeaderText="Mã CQQĐ" >
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TEN_CQQD" HeaderText="Tên CQQĐ">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_CQTHU" HeaderText="Mã CQTHU">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_KB" HeaderText="Kho Bạc">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                         <asp:BoundColumn DataField="MDATE" HeaderText="Ngày">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MAKER" HeaderText="User">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                     <%--     <asp:BoundColumn DataField="tinh_trang" HeaderText="Tình trạng">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>--%>
                         <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                 <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
                <asp:DataGrid ID="grd_new" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="id_cqqd" HeaderText="ID_CQQD" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="type" HeaderText="type" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ThayDoi" HeaderText="Thay đổi" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_cqqd_new" HeaderText="Mã cqqd mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ten_cqqd_new" HeaderText="Tên cqqd mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_cqthu_new" HeaderText="Tên cqthu mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_kb_new" HeaderText="Mã kb mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="NGAY_TAO" HeaderText="Ngày tạo mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="NGUOI_TAO" HeaderText="Người tạo mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tinh_trang_new" HeaderText="Tình trạng mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_cqqd" HeaderText="Mã cqqd hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ten_cqqd" HeaderText="Tên cqqd hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_cqthu" HeaderText="Tên cqthu hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_kb" HeaderText="Mã kb hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MDATE" HeaderText="Ngày tạo hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MAKER" HeaderText="Người tạo hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tinh_trang" HeaderText="Tình trạng hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tt_xu_ly" HeaderText="Trạng thái xử lý" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="xu_ly" HeaderText="Trạng thái xử lý"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
                <%--</div>--%>
            </td>
        </tr>
    </table>
</asp:Content>
