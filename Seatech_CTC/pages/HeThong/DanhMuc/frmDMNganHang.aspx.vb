﻿Imports System.Data
Imports Business.Common.mdlCommon
Imports Business.DanhMuc
Imports VBOracleLib

Partial Class pages_HeThong_DanhMuc_frmDMNganHang
    Inherits System.Web.UI.Page
    Private objNganHang As New NganHang.buNganHang()

    Private Sub GetBankList()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtMA_XA.Enabled Then
                If txtMA_XA.Text <> "" Then
                    WhereClause += " AND ma_xa like '%" + txtMA_XA.Text + "%'"
                End If
                If txtMA_NH.Text <> "" Then
                    WhereClause += " AND ma like '%" + txtMA_NH.Text + "%'"
                End If
                If txtTEN_NH.Text <> "" Then
                    WhereClause += " AND upper (ten) like upper('%" + txtTEN_NH.Text + "%')"
                End If
                If txtSHKB.Text <> "" Then
                    WhereClause += " AND shkb like '%" + txtSHKB.Text + "%'"
                End If
            End If

            ds = objNganHang.Load(WhereClause)
            If Not ds Is Nothing Then
                dtgdata.DataSource = ds.Tables(0)
                dtgdata.DataBind()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub GetDetailBank(ByVal id As String)
        Dim ds As DataSet
        ds = objNganHang.Load(" AND id = '" + id + "'")

        If ds.Tables(0).Rows.Count > 0 Then
            txtMA_XA.Text = ds.Tables(0).Rows(0)("ma_xa").ToString
            txtMA_NH.Text = ds.Tables(0).Rows(0)("ma").ToString
            txtTEN_NH.Text = ds.Tables(0).Rows(0)("ten").ToString
            txtSHKB.Text = ds.Tables(0).Rows(0)("shkb").ToString
        End If
       
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            GetBankList()
            cmdTaoMoi.Enabled = True
            If (Not Request.Params("id") Is Nothing) Then
                'txtMA_XA.Enabled = False
                GetDetailBank(Request.Params("id").Trim)
            End If
            cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn xóa dữ liệu đã chọn?') ")
        End If
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        GetBankList()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        GetBankList()
        cmdTaoMoi.Enabled = False
    End Sub

    Private Sub ClearFrom()
        txtMA_XA.Text = ""
        txtMA_NH.Text = ""
        txtTEN_NH.Text = ""
        txtSHKB.Text = ""
        txtMA_XA.Enabled = True
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        ClearFrom()

        Response.Redirect("~/pages/HeThong/DanhMuc/frmDMNganHang.aspx")
    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenKB(ByVal strSHKBM As String) As String
        Dim returnValue As String = ""
        If strSHKBM.Trim <> "" Then
            returnValue = Get_TenKhoBac(strSHKBM)
        End If
        Return returnValue
    End Function
    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        txtMA_XA.Text = RemoveApostrophe(txtMA_XA.Text)
        If txtMA_XA.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Mã địa bản hành chính.")
            txtMA_XA.Focus()
            Exit Sub
        End If

        If txtMA_XA.Text.Trim <> "" Then
            If "".Equals(Get_TenDBHC(txtMA_XA.Text.Trim)) Then
                txtMA_XA.Focus()
                clsCommon.ShowMessageBox(Me, "Mã địa bàn hành chính chưa tồn tại trong hệ thống.")
                Exit Sub
            End If
        End If
        If txtSHKB.Text.Trim <> "" Then
            If "".Equals(Get_TenKhoBac(txtSHKB.Text.Trim)) Then
                txtSHKB.Focus()
                clsCommon.ShowMessageBox(Me, "Số hiệu kho bạc không tồn tại trong hệ thống.")
                Exit Sub
            End If
        End If
        If txtMA_NH.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Mã ngân hàng.")
            txtMA_NH.Focus()
            Exit Sub
            '*****TYNK - Check ky tu dac biet*********19/02/2013
        Else
            If Regex.IsMatch(txtMA_NH.Text.Trim, "^[a-zA-Z0-9]*$") = False Then
                clsCommon.ShowMessageBox(Me, "Chuỗi bạn nhập không đúng - Có ký tự đặc biệt.")
                txtMA_NH.Focus()
                Exit Sub
            End If
            '***************************************************
        End If

        If txtTEN_NH.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Tên ngân hàng.")
            txtTEN_NH.Focus()
            Exit Sub
        End If

        If txtSHKB.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Số hiệu kho bạc.")
            txtSHKB.Focus()
            Exit Sub
        End If

        Dim infNH As New NganHang.infNganHang()
        Try

            infNH.MA_XA = txtMA_XA.Text.ToString.Trim
            infNH.MA_NH = txtMA_NH.Text.ToString.Trim
            infNH.TEN_NH = txtTEN_NH.Text.ToString.Trim
            infNH.SHKB = txtSHKB.Text.ToString.Trim

            If Not Request.Params("id") Is Nothing Then
                infNH.ID = Request.Params("id").Trim
                If objNganHang.Update(infNH) Then
                    'ClearFrom()
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                Else
                    clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                    txtMA_NH.Focus()
                End If
            Else
                If objNganHang.Insert(infNH) Then
                    'ClearFrom()
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                Else
                    clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                    txtMA_NH.Focus()
                End If
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình cập nhật danh mục ngân hàng")
        Finally
        End Try
        GetBankList()
        'Response.Redirect("~/pages/HeThong/DanhMuc/frmDMNganHang.aspx")
    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim i As Integer
        Dim strID As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True
        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(dtgdata.Items(i).Cells(0).Text)
                ' strMA_DTHU = Trim(dtgdata.Items(i).Cells(6).Text)
                If objNganHang.Delete(strID) Then
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                Else
                    clsCommon.ShowMessageBox(Me, "Dữ liệu đã sử dụng trong chức năng khác, không được xoá!")
                End If
            v_count += 1
            End If
        Next
        ClearFrom()
        GetBankList()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub

    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Try
            Dim frm As New Business.BaoCao.infBaoCao
            'Business.Common.mdlSystemVariables.gLoaiBaoCao = Report_Type.DM_TAIKHOAN
            Session("objBaoCao") = frm
            clsCommon.OpenNewWindow(Me, "../../BaoCao/frmShowReports.aspx&BC=" & Report_Type.DM_NGANHANG & "", "ManPower")
        Catch ex As Exception

        End Try
    End Sub
End Class
