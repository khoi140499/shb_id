﻿Imports Business.Common.mdlCommon
Imports Business.DanhMuc
Imports System.Data
Partial Class pages_HeThong_DanhMuc_frmDMDiemThu
    Inherits System.Web.UI.Page
    Private objDanhMuc As New buDanhMuc("DIEMTHU")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdGhi.Click, AddressOf cmdGhi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            load_dataGrid()
            cmdGhi.Enabled = True
            If Not Request.Params("id") Is Nothing Then
                txtMaDthu.Enabled = False
                loadDetail(Request.Params("id").Trim)
            End If
        End If
        cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn xóa dữ liệu đã chọn?') ")
    End Sub

    Private Sub load_dataGrid()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtMaDthu.Enabled Then
                If txtMaDthu.Text <> "" Then
                    WhereClause += " AND ma_dthu = '" + txtMaDthu.Text + "'"
                End If
                If txtTenDThu.Text <> "" Then
                    WhereClause += " AND upper( ten) like upper ('%" + txtTenDThu.Text + "%')"
                End If
            End If

            ds = objDanhMuc.GetDataSet(WhereClause)
            dtgdata.DataSource = ds.Tables(0)
            dtgdata.DataBind()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub loadDetail(ByVal ma_Dthu As String)
        Dim objDiemThu As New DiemThu.infDiemthu()
        objDiemThu.MaDThu = ma_Dthu
        Dim objLoad As New DiemThu.buDiemthu()
        Dim ds As DataSet
        ds = objLoad.loadDetail(objDiemThu)
        If Not ds Is Nothing Then
            txtMaDthu.Text = ds.Tables(0).Rows(0)("Ma_DThu").ToString
            txtTenDThu.Text = ds.Tables(0).Rows(0)("ten").ToString
        End If
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.txtMaDthu.Text = ""
        Me.txtTenDThu.Text = ""
        Me.txtMaDthu.Enabled = True
        cmdGhi.Enabled = True
        If Not Request.Params("id") Is Nothing Then
            Response.Redirect("~/pages/HeThong/DanhMuc/frmDMDiemThu.aspx")
        End If
    End Sub

    Protected Sub cmdGhi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdGhi.Click
        txtTenDThu.Text = RemoveApostrophe(txtTenDThu.Text)

        Dim objDiemthu As New DiemThu.infDiemthu()
        If Request.Params("id") Is Nothing Then
            If txtMaDthu.Text.Trim = "" Then
                clsCommon.ShowMessageBox(Me, "Mã điểm thu không được để trống!")
                txtMaDthu.Focus()
                Return
                '*****TYNK - Check ky tu dac biet*********07/02/2013
            Else
                If Regex.IsMatch(txtMaDthu.Text.Trim, "^[a-zA-Z0-9]*$") = False Then
                    clsCommon.ShowMessageBox(Me, "Chuỗi bạn nhập không đúng - Có ký tự đặc biệt.")
                    txtMaDthu.Focus()
                    Exit Sub
                End If
                '***************************************************
            End If
            If txtTenDThu.Text.Trim = "" Then
                clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên điểm thu.")
                txtTenDThu.Focus()
                Exit Sub
            End If
            Try
                objDiemthu.MaDThu = txtMaDthu.Text.ToString.Trim
                objDiemthu.TenDThu = txtTenDThu.Text.ToString.Trim
                Dim objInsert As New DiemThu.buDiemthu("ADD")
                If objInsert.Save(objDiemthu) Then
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                Else
                    txtMaDthu.Focus()
                    ' txtMaDthu..SelectAll()
                End If
            Catch ex As Exception
                If objDanhMuc.Status = "EDIT" Then
                    clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình cập nhật điểm thu !")
                Else
                    clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình tạo mới điểm thu !")
                End If
                Throw ex
            Finally
            End Try
        Else
            If txtTenDThu.Text.Trim = "" Then
                clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên điểm thu.")
                txtTenDThu.Focus()
                Exit Sub
            End If
            Try
                objDiemthu.MaDThu = txtMaDthu.Text.ToString.Trim
                objDiemthu.TenDThu = txtTenDThu.Text.ToString.Trim
                Dim objInsert As New DiemThu.buDiemthu("EDIT")
                If objInsert.Save(objDiemthu) Then
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                Else
                    txtMaDthu.Focus()
                End If
            Catch ex As Exception
                If objDanhMuc.Status = "ADD" Then
                    clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình cập nhật điểm thu !")
                Else
                    clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình tạo mới điểm thu !")
                End If
                Throw ex
            Finally
            End Try
        End If
        cmdCancel_Click(sender, e)
        load_dataGrid()
    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim i As Integer
        Dim checkSelect As Integer = 0
        Dim strma_Dthu As String
        Dim objDel As New DiemThu.buDiemthu()
        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strma_Dthu = Trim(dtgdata.Items(i).Cells(0).Text)
                If strma_Dthu = "0" Then
                    clsCommon.ShowMessageBox(Me, "Bạn không thể xoá mã điểm thu hệ thống")
                    Return
                End If
                If (objDel.DelDiemthu(strma_Dthu)) Then
                    checkSelect += 1
                Else
                    clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                    Return
                End If
            Else
                clsCommon.ShowMessageBox(Me, "Chưa có bản ghi nào được chọn để xóa.")
                Return
            End If
        Next
        If checkSelect > 0 Then
            clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
        Else
            clsCommon.ShowMessageBox(Me, "Bạn cần chọn Điểm thu muốn xóa!")
        End If
        load_dataGrid()
    End Sub

    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Try
            Dim frm As New Business.BaoCao.infBaoCao
            'Business.Common.mdlSystemVariables.gLoaiBaoCao = Report_Type.DM_DIEMTHU
            Session("objBaoCao") = frm
            clsCommon.OpenNewWindow(Me, "../../BaoCao/frmShowReports.aspx&BC=" & Report_Type.DM_DIEMTHU & "", "ManPower")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        load_dataGrid()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        load_dataGrid()
        cmdGhi.Enabled = False
    End Sub
End Class
