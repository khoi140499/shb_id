﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmDMMapCQQD_LHThu.aspx.vb"
 Inherits="pages_HeThong_DanhMuc_frmDMMapCQQD_LHThu" title="Danh mục map CQQD loại hình thu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" language="javascript">
  
    function FindDanhMuc(strPage, txtID, txtTitle) {
        var returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&initParam=" + document.getElementById(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;", "_new");

        document.getElementById(txtID).value = returnValue.ID;
        document.getElementById(txtTitle).value = returnValue.Title;

    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">DANH MỤC MAP CQQD LOẠI HÌNH THU</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <asp:HiddenField ID="hdfMa_Nhom" runat="server" />
            <asp:HiddenField ID="hdfTenDN" runat="server" />
        </tr>
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%"
                    class="form_input">
                    <tr align="left">
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label3" runat="server" CssClass="label">Số hiệu kho bạc</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtSHKB" runat="server" MaxLength="4" Width="71%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>

                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label1" runat="server" CssClass="label">Cơ quan quyết định</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtCQQD" runat="server"  Width="71%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                         <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label2" runat="server" CssClass="label">Mã loại hình</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMa_LH" runat="server" Width="71%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>

                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label6" runat="server" CssClass="label">Mã quỹ</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMaQuy" runat="server"  Width="71%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left" >
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label7" runat="server" CssClass="label">Tài khoản ngân sách</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtTK_NS" runat="server" Width="71%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>

                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label8" runat="server" CssClass="label">Mã chương</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMaChuong" runat="server"  Width="71%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>
                    </tr>
                      <tr align="left" >
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label9" runat="server" CssClass="label">Mã nội dung kinh tế</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMa_NDKT" runat="server" Width="71%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>

                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label10" runat="server" CssClass="label">Mã NKT</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMa_NKT" runat="server"  Width="71%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>
                    </tr>
                     <tr align="left" >
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label11" runat="server" CssClass="label">Mã địa bàn hành chính</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMaDBHC" runat="server" Width="71%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>

                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label12" runat="server" CssClass="label">Ký hiệu TK</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtKHTK" runat="server"  Width="71%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>
                    </tr>
                     <tr align="left" >
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label13" runat="server" CssClass="label">Mã DV sử dụng ngân sách</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMa_DVSDNS" runat="server" Width="71%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>

                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label14" runat="server" CssClass="label">Tên cơ quan</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtTenCQ" runat="server"  Width="71%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblPasswordCaption" CssClass="label" runat="server">Trạng thái</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID="cboTinh_trang" runat="server" CssClass="inputflat" Width="40%">
                                <asp:ListItem Text="Chọn" Value=""></asp:ListItem>
                                <asp:ListItem Text="Đang hoạt động" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Ngừng hoạt động" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                         <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label15" runat="server" CssClass="label">ID</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtID_HIS" runat="server"  Width="71%" CssClass="inputflat" TabIndex="1" ReadOnly></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left" id="Tr1" runat="server">
                        <td class="form_label">
                            <asp:Label ID="Label4" CssClass="label" runat="server">Thông tin dữ liệu</asp:Label>
                        </td>
                        <td class="form_control">
                           <asp:DropDownList ID="drpDuLieu" runat="server" CssClass="inputflat" Width="40%">
                                <asp:ListItem Text="Dữ liệu hiện tại" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Dữ liệu thay đổi(thêm/sửa/xóa)" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="left" id="Tr2" runat="server">
                        <td class="form_label">
                            <asp:Label ID="Label5" CssClass="label" runat="server">Trạng thái xử lý</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID="drpTT_XU_LY" runat="server" CssClass="inputflat" Width="40%">
                                <asp:ListItem Text="Tất cả" Value=""></asp:ListItem>
                                <asp:ListItem Text="Chờ duyệt" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Đã duyệt" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Từ chối" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdSearch" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdTaoMoi" runat="server" CssClass="ButtonCommand" Text="Ghi" TabIndex="5" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdDeleteUser" runat="server" CssClass="ButtonCommand" Text="Xóa" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdIn" Enabled="false" runat="server" CssClass="ButtonCommand" Text="In" Visible="false" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="btnDuyet" runat="server" CssClass="ButtonCommand" Text="Duyệt" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="btnTuChoi" runat="server" CssClass="ButtonCommand" Text="Từ chối" UseSubmitBehavior="false"></asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
             <%--<div style="vertical-align :top; height: 300px; overflow: auto; width: 600px;">--%>
                <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                         <asp:BoundColumn DataField="ID" HeaderText="ID" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="ID" > <%--HeaderStyle-Width="100"--%>
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ID")%>'
                                    NavigateUrl='<%#"~/pages/HeThong/DanhMuc/frmDMMapCQQD_LHThu.aspx?ID=" & DataBinder.Eval(Container.DataItem, "ID")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle Width="20%"></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="SHKB" HeaderText="Số hiệu kho bạc">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_CQQD" HeaderText="Mã cơ quan quyết định">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_LH" HeaderText="Mã loại hình">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_QUY" HeaderText="Mã quỹ">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                         <asp:BoundColumn DataField="TK_NS" HeaderText="Tài khoản thu ngân sách">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                         <asp:BoundColumn DataField="MA_CHUONG" HeaderText="Mã chương">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_NKT" HeaderText="Mã NKT">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_NDKT" HeaderText="Mã NDKT">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_DBHC" HeaderText="Mã DBHC">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                          <asp:BoundColumn DataField="KHTK" HeaderText="Ký hiệu TK">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_DVSDNS" HeaderText="Mã DVSDNS">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TEN_CQ" HeaderText="Tên cơ quan">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="tinhtrang" HeaderText="Tình trạng">
                            <HeaderStyle Width="120px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
                <asp:DataGrid ID="grd_new" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="id" HeaderText="ID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ID_HIS" HeaderText="ID_HIS" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="type" HeaderText="type" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ThayDoi" HeaderText="Thay đổi" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>

                        <asp:BoundColumn DataField="SHKB_new" HeaderText="SHKB mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_CQQD_new" HeaderText="Mã CQQD mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_LH_new" HeaderText="Mã loại hình mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_QUY_new" HeaderText="Mã quỹ mới"></asp:BoundColumn>
                       

                        <asp:BoundColumn DataField="TK_NS_new" HeaderText="TK ngân sách mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_CHUONG_new" HeaderText="Mã chương mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_NKT_new" HeaderText="Mã NKT mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_NDKT_new" HeaderText="Mã NDKT mới"></asp:BoundColumn>

                        <asp:BoundColumn DataField="MA_DBHC_new" HeaderText="Mã DBHC mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="KHTK_new" HeaderText="Ký hiệu TK mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_DVSDNS_new" HeaderText="Mã DVSDNS mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="TEN_CQ_new" HeaderText="Tên cơ quan mới"></asp:BoundColumn>

                         <asp:BoundColumn DataField="tinh_trang_new" HeaderText="Tình trạng mới"></asp:BoundColumn>

                        <asp:BoundColumn DataField="tinh_trang" HeaderText="Tình trạng hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tt_xu_ly" HeaderText="Trạng thái xử lý" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="xu_ly" HeaderText="Trạng thái xử lý"></asp:BoundColumn>

                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>

