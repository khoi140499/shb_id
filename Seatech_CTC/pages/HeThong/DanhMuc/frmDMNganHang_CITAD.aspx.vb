﻿Imports System.Data
Imports Business.Common.mdlCommon
Imports Business.DanhMuc
Imports VBOracleLib

Partial Class pages_HeThong_DanhMuc_frmDMNganHang_citad
    Inherits System.Web.UI.Page
    Private objNganHang As New NganHang.buNganHang_CITAD()

    Private Sub GetBankList()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtSHKB.Text.Trim <> "" Then
                WhereClause += " AND upper(a.shkb) like upper('%" + txtSHKB.Text.Trim + "%')"
            End If
            'If txtTenKB.Text.Trim <> "" Then
            '    WhereClause += " AND MA_TRUCTIEP like '%" + txtMA_NH.Text.Trim + "%'"
            'End If

            If txtMA_NH.Text.Trim <> "" Then
                WhereClause += " AND upper(a.MA_TRUCTIEP) like upper('%" + txtMA_NH.Text.Trim + "%')"
            End If
            If txtTEN_NH.Text.Trim <> "" Then
                WhereClause += " AND upper(a.TEN_TRUCTIEP) like upper('%" + txtTEN_NH.Text.Trim + "%')"
            End If
            If txtMa_NH_TH.Text.Trim <> "" Then
                WhereClause += " AND upper(a.MA_GIANTIEP) like upper('%" + txtMa_NH_TH.Text.Trim + "%')"
            End If
            If txtTen_NH_TH.Text.Trim <> "" Then
                WhereClause += " AND upper(a.TEN_GIANTIEP) like upper ('%" + txtTen_NH_TH.Text.Trim + "%')"
            End If
            'If ddlTrangThai.SelectedValue <> "" Then
            '    WhereClause += " AND tt_kb = '" + ddlTrangThai.SelectedValue + "'"
            'End If
            If drpTaiSHB.SelectedValue <> "" Then
                WhereClause += " AND a.TT_SHB = '" + drpTaiSHB.SelectedValue + "'"
            End If

            ds = objNganHang.Load(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgdata.DataSource = ds.Tables(0)
                dtgdata.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub
    Private Sub GetBankList_new()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtSHKB.Text.Trim <> "" Then
                WhereClause += " AND upper(b.shkb) like upper('%" + txtSHKB.Text.Trim + "%')"
            End If
            'If txtTenKB.Text.Trim <> "" Then
            '    WhereClause += " AND MA_TRUCTIEP like '%" + txtMA_NH.Text.Trim + "%'"
            'End If

            If txtMA_NH.Text.Trim <> "" Then
                WhereClause += " AND upper(b.MA_TRUCTIEP like upper('%" + txtMA_NH.Text.Trim + "%')"
            End If
            If txtTEN_NH.Text.Trim <> "" Then
                WhereClause += " AND upper(b.TEN_TRUCTIEP) like upper('%" + txtTEN_NH.Text.Trim + "%')"
            End If
            If txtMa_NH_TH.Text.Trim <> "" Then
                WhereClause += " AND upper(b.MA_GIANTIEP) like upper('%" + txtMa_NH_TH.Text.Trim + "%')"
            End If
            If txtTen_NH_TH.Text.Trim <> "" Then
                WhereClause += " AND upper(b.TEN_GIANTIEP) like upper ('%" + txtTen_NH_TH.Text.Trim + "%')"
            End If
            If drpTT_XU_LY.SelectedValue <> "" Then
                WhereClause += " AND b.TT_XU_LY = '" + drpTT_XU_LY.SelectedValue + "'"
            End If
            If drpTaiSHB.SelectedValue <> "" Then
                WhereClause += " AND b.TT_SHB = '" + drpTaiSHB.SelectedValue + "'"
            End If
            ds = objNganHang.Load_new(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                grd_new.DataSource = ds.Tables(0)
                grd_new.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub
    Private Sub GetDetailBank(ByVal id As String, ByVal mgt As String, ByVal mtt As String, ByVal idse As String)
        Dim ds As DataSet
        ds = objNganHang.Load(" and a.shkb ='" & id & "' and ma_giantiep='" & mgt & "' and ma_tructiep='" & mtt & "' ")
        'AND a.shkb = '" + id + "'
        If ds.Tables(0).Rows.Count > 0 Then
            txtMA_NH.Text = ds.Tables(0).Rows(0)("ma_tructiep").ToString
            txtTEN_NH.Text = ds.Tables(0).Rows(0)("TEN_TRUCTIEP").ToString
            txtSHKB.Text = ds.Tables(0).Rows(0)("shkb").ToString
            txtTenKB.Text = ds.Tables(0).Rows(0)("ten_kb").ToString
            txtMa_NH_TH.Text = ds.Tables(0).Rows(0)("MA_GIANTIEP").ToString
            txtTen_NH_TH.Text = ds.Tables(0).Rows(0)("TEN_GIANTIEP").ToString
            txtIDSE.Text = ds.Tables(0).Rows(0)("ID").ToString
            drpTaiSHB.SelectedValue = ds.Tables(0).Rows(0)("TT_SHB").ToString
            'ddlTrangThai.SelectedValue = ds.Tables(0).Rows(0)("TT_KB").ToString
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            EnableButton_Nhom(Session.Item("MA_NHOM"))
            hdfTenDN.Value = Session.Item("UserName")
            'GetBankList()
            cmdTaoMoi.Enabled = True
            ' txtSHKB.Enabled = True
            If (Not Request.Params("id") Is Nothing And Not Request.Params("mgt") Is Nothing And Not Request.Params("mtt") Is Nothing And Not Request.Params("idse") Is Nothing) Then
                txtSHKB.Enabled = False
                GetDetailBank(Request.Params("id").Trim, Request.Params("mgt"), Request.Params("mtt"), Request.Params("idse"))
            End If
        End If
        'cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn xóa dữ liệu đã chọn?') ")
    End Sub

    Protected Sub EnableButton_Nhom(ByVal Nhom As String)
        Nhom = clsCommon.Check_Maker_Checker(Nhom)
        hdfMa_Nhom.Value = Nhom
        If Nhom = "10" Then
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = True
            'cmdIn.Visible = True
            cmdDeleteUser.Visible = True
            cmdTaoMoi.Visible = True
            drpDuLieu.SelectedIndex = 0
            GetBankList()
            cmdTaoMoi.Enabled = True
        ElseIf Nhom = "11" Then
            btnDuyet.Visible = True
            btnTuChoi.Visible = True
            'rowTrangThai.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            drpDuLieu.SelectedIndex = 1
            GetBankList_new()
        Else
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            drpDuLieu.SelectedIndex = 0
            GetBankList()
        End If
    End Sub

    Protected Sub dtgdata_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgdata.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If Not MaNhom = "10" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim checkBox As HyperLink = DirectCast(e.Item.FindControl("Hyperlink1"), HyperLink)
                checkBox.Enabled = False
            End If
        End If
    End Sub
    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        GetBankList()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        If drpDuLieu.SelectedIndex = 0 Then
            GetBankList()
            dtgdata.CurrentPageIndex = 0
            dtgdata.DataBind()
            grd_new.Visible = False
            dtgdata.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = False
                btnTuChoi.Visible = False
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = True
                'cmdIn.Visible = True
                cmdDeleteUser.Visible = True
                cmdTaoMoi.Visible = True
            End If
        Else
            GetBankList_new()
            dtgdata.Visible = False
            grd_new.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = True
                btnTuChoi.Visible = True
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = False
                'cmdIn.Visible = False
                cmdDeleteUser.Visible = False
                cmdTaoMoi.Visible = False
            End If
        End If
        cmdTaoMoi.Enabled = False
    End Sub

    Private Sub ClearFrom()
        txtMA_NH.Text = ""
        txtTEN_NH.Text = ""
        txtSHKB.Text = ""
        txtTenKB.Text = ""
        txtMA_NH.Text = ""
        txtTen_NH_TH.Text = ""
        cmdTaoMoi.Enabled = True
        txtSHKB.Enabled = True
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        ClearFrom()

        Response.Redirect("~/pages/HeThong/DanhMuc/frmDMNganHang_CITAD.aspx")
    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenKB(ByVal strSHKBM As String) As String
        Dim returnValue As String = ""
        If strSHKBM.Trim <> "" Then
            returnValue = Get_TenKhoBac(strSHKBM)
        End If
        Return returnValue
    End Function
    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        If txtSHKB.Text.Trim <> "" Then
            If "".Equals(Get_TenKhoBac(txtSHKB.Text.Trim)) Then
                txtSHKB.Focus()
                clsCommon.ShowMessageBox(Me, "Số hiệu kho bạc không tồn tại trong hệ thống.")
                Exit Sub
            End If

        End If

        If txtMA_NH.Text.ToString.Trim.Length <> 8 Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Mã ngân hàng trực tiếp có 8 ký tự số.")
            txtMA_NH.Focus()
            Exit Sub
            '*****TYNK - Check ky tu dac biet*********19/02/2013
        Else
            If Regex.IsMatch(txtMA_NH.Text.Trim, "^[0-9]*$") = False Then
                clsCommon.ShowMessageBox(Me, "Hãy nhập mã ngân hàng trực tiếp kiểu số kiểu số.")
                txtMA_NH.Focus()
                Exit Sub
            End If
            '***************************************************
        End If

        If txtTEN_NH.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Tên ngân hàng trực tiếp.")
            txtTEN_NH.Focus()
            Exit Sub
        End If

        If txtSHKB.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Số hiệu kho bạc.")
            txtSHKB.Focus()
            Exit Sub
        Else
            If Regex.IsMatch(txtSHKB.Text.Trim, "^[0-9]*$") = False Then
                clsCommon.ShowMessageBox(Me, "Bạn nhập số hiệu kho bạc kiểu số")
                txtSHKB.Focus()
                Exit Sub
            End If
        End If
        If txtMa_NH_TH.Text.ToString.Trim.Length <> 8 Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Mã ngân hàng thụ hưởng có 8 ký tự số.")
            txtMa_NH_TH.Focus()
            Exit Sub
            '*****TYNK - Check ky tu dac biet*********19/02/2013
        Else
            If Regex.IsMatch(txtMa_NH_TH.Text.Trim, "^[0-9]*$") = False Then
                clsCommon.ShowMessageBox(Me, "Hãy nhập mã ngân hàng thụ hưởng kiểu số kiểu số.")
                txtMa_NH_TH.Focus()
                Exit Sub
            End If
            '***************************************************
        End If

        If txtTen_NH_TH.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Tên ngân hàng thụ hưởng.")
            txtTen_NH_TH.Focus()
            Exit Sub
        End If
        If drpTaiSHB.SelectedValue.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải chọn kho bạc và ngân hàng gián tiếp có mở tại SHB hay không.")
            Exit Sub
        End If
        Dim infNH As New NganHang.infNganHang_CITAD()
        Try

            infNH.MA_NH_TT = txtMA_NH.Text.ToString.Trim
            infNH.TEN_NH_TT = txtTEN_NH.Text.ToString.Trim
            infNH.SHKB = txtSHKB.Text.Trim
            infNH.Ten_KB = txtTenKB.Text.Trim
            infNH.MA_NH_TH = txtMa_NH_TH.Text.Trim
            infNH.TEN_NH_TH = txtTen_NH_TH.Text.Trim
            infNH.TT_KB = 0
            infNH.IDSE = txtIDSE.Text.Trim
            infNH.NGUOI_TAO = hdfTenDN.Value
            infNH.NH_SHB = drpTaiSHB.SelectedValue

            If Not Request.Params("id") Is Nothing Then
                infNH.TYPE = "2" 'UPDATE
                'infNH.SHKB = Request.Params("id").Trim
                'Nếu có không có thay đổi chờ xử lý
                If Not Check_ThayDoi(infNH) Then
                    If objNganHang.Update(infNH, "10") Then
                        ClearFrom()
                        clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                    Else
                        clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                        txtMA_NH.Focus()
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                End If

            Else
                If objNganHang.CheckExist_GT_TT(infNH) = True Then
                    infNH.TYPE = "1" 'INSERT
                    If Not Check_Insert(infNH.SHKB) Then
                        '10 thêm vào trong HIS_DM_CAP_CHUONG
                        If objNganHang.Insert(infNH, "10") Then
                            ClearFrom()
                            clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                        Else
                            clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                            txtMA_NH.Focus()
                        End If
                    Else
                        clsCommon.ShowMessageBox(Me, "Thông tin mã SHB này đã tồn tại và đang chờ xử lý!")
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Số hiệu kho bạc tương ứng mã ngân hàng đã tồn tại trong hệ thống")
                End If

            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình cập nhật danh mục ngân hàng")
        Finally
        End Try
        GetBankList()
    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim i As Integer
        Dim strID As String
        Dim strMa_GT As String
        Dim strMA_TT As String
        Dim strID_GT_TT As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(dtgdata.Items(i).Cells(0).Text)
                strMa_GT = Trim(dtgdata.Items(i).Cells(2).Text)
                strMA_TT = Trim(dtgdata.Items(i).Cells(4).Text)
                strID_GT_TT = Trim(dtgdata.Items(i).Cells(8).Text)
                Dim objMLNS As New NganHang.infNganHang_CITAD
                objMLNS.SHKB = strID
                objMLNS.TYPE = "3" 'xóa dữ liệu
                objMLNS.NGUOI_TAO = hdfTenDN.Value
                objMLNS.MA_NH_TT = strMA_TT
                objMLNS.MA_NH_TH = strMa_GT
                objMLNS.IDSE = strID_GT_TT
                If Not Check_ThayDoi(objMLNS) Then
                    If objNganHang.Delete(objMLNS, "10") Then
                        clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                    Else
                        clsCommon.ShowMessageBox(Me, "Dữ liệu đã sử dụng trong chức năng khác, không được xoá!")
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                End If
                v_count += 1
            End If
        Next

        ClearFrom()
        GetBankList()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub
    Protected Sub Get_Data_Item(ByRef objItem As NganHang.infNganHang_CITAD)
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT TEN_KB,SHKB,MA_TRUCTIEP,TEN_TRUCTIEP, MA_GIANTIEP,TEN_GIANTIEP,TT_SHB as NH_SHB   FROM his_dm_ngan_hang WHERE HIS_ID=" & objItem.HIS_ID
        dt = DataAccess.ExecuteToTable(WhereClause)
        objItem.Ten_KB = dt.Rows(0)("TEN_KB").ToString
        objItem.SHKB = dt.Rows(0)("SHKB").ToString
        objItem.MA_NH_TT = dt.Rows(0)("MA_TRUCTIEP").ToString
        objItem.TEN_NH_TT = dt.Rows(0)("TEN_TRUCTIEP").ToString
        objItem.MA_NH_TH = dt.Rows(0)("MA_GIANTIEP").ToString
        objItem.TEN_NH_TH = dt.Rows(0)("TEN_GIANTIEP").ToString
        objItem.NH_SHB = dt.Rows(0)("NH_SHB").ToString
    End Sub
    'Kiểm tra có thay đổi thông tin đang chờ xử lý ko
    'Nếu có thay đổi thì trả về True/False
    Protected Function Check_ThayDoi(ByRef objItem As NganHang.infNganHang_CITAD) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_dm_ngan_hang WHERE SHKB='" & objItem.SHKB & "' AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function
    Protected Function Check_Insert(ByRef strSHKB As String) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_dm_ngan_hang WHERE SHKB='" & strSHKB & "' AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function

    Protected Sub btnDuyet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuyet.Click
        Dim i As Integer
        Dim strID As String
        Dim strType As String
        Dim strIDSE As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                strIDSE = Trim(grd_new.Items(i).Cells(17).Text)
                Dim objMLNS = New NganHang.infNganHang_CITAD
                objMLNS.HIS_ID = strID
                objMLNS.NGUOI_DUYET = hdfTenDN.Value
                objMLNS.TT_XU_LY = "1"
                objMLNS.IDSE = strIDSE
                Dim objInsert As New NganHang.buNganHang_CITAD
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        GetBankList_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Duyệt thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi duyệt!")
        End If
    End Sub

    Protected Sub btnTuChoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTuChoi.Click
        Dim i As Integer
        Dim strID As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                Dim objMLNS = New NganHang.infNganHang_CITAD
                objMLNS.HIS_ID = strID
                objMLNS.NGUOI_DUYET = hdfTenDN.Value
                objMLNS.TT_XU_LY = "2"
                Dim objInsert As New NganHang.buNganHang_CITAD
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        GetBankList_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Từ chối thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi từ chối!")
        End If
    End Sub

    Protected Sub grd_new_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_new.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value
        If MaNhom = "10" Then
            e.Item.Cells(18).Visible = False
        ElseIf MaNhom = "11" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim tt_xu_ly As String = e.Item.Cells(15).Text.ToString
                If tt_xu_ly <> "0" Then
                    Dim checkBox As CheckBox = DirectCast(e.Item.FindControl("chkSelect"), CheckBox)
                    checkBox.Visible = False
                End If
            End If
        Else
            e.Item.Cells(17).Visible = False
        End If
    End Sub

    Protected Sub grd_new_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grd_new.PageIndexChanged
        GetBankList_new()
        grd_new.CurrentPageIndex = e.NewPageIndex
        grd_new.DataBind()
    End Sub
End Class
