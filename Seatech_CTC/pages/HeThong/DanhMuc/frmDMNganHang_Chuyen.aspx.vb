﻿Imports System.Data
Imports Business.Common.mdlCommon
Imports Business.DanhMuc
Imports VBOracleLib

Partial Class pages_HeThong_DanhMuc_frmDMNganHang_Chuyen
    Inherits System.Web.UI.Page
    Private objNHChuyen As New NHChuyen.buNHChuyen()

    Private Sub GetBankList()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtMA_NHChuyen.Enabled Then
                If txtMA_NHChuyen.Text <> "" Then
                    WhereClause += " AND upper(ma_nh_chuyen) like upper('%" + txtMA_NHChuyen.Text.Trim + "%')"
                End If
                If txtTEN_NHChuyen.Text <> "" Then
                    WhereClause += " AND upper(ten_nh_chuyen) like upper('%" + txtTEN_NHChuyen.Text.Trim + "%')"
                End If
            End If

            ds = objNHChuyen.Load(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgdata.DataSource = ds.Tables(0)
                dtgdata.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub
    Private Sub GetBankList_new()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtMA_NHChuyen.Enabled Then
                If txtMA_NHChuyen.Text <> "" Then
                    WhereClause += " AND upper(b.ma_nh_chuyen) like upper('%" + txtMA_NHChuyen.Text.Trim + "%')"
                End If
                If txtTEN_NHChuyen.Text <> "" Then
                    WhereClause += " AND upper(b.ten_nh_chuyen) like upper('%" + txtTEN_NHChuyen.Text.Trim + "%')"
                End If
            End If

            ds = objNHChuyen.Load_new(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                grd_new.DataSource = ds.Tables(0)
                grd_new.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub

    Private Sub GetDetailBank(ByVal id As String)
        Dim ds As DataSet
        ds = objNHChuyen.Load(" AND ma_nh_chuyen = '" + id + "'")

        If ds.Tables(0).Rows.Count > 0 Then
            txtMA_NHChuyen.Text = ds.Tables(0).Rows(0)("ma_nh_chuyen").ToString
            txtTEN_NHChuyen.Text = ds.Tables(0).Rows(0)("ten_nh_chuyen").ToString
        End If
        txtMA_NHChuyen.Enabled = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            EnableButton_Nhom(Session.Item("MA_NHOM"))
            hdfTenDN.Value = Session.Item("UserName")
            'GetBankList()
            cmdTaoMoi.Enabled = True
            If (Not Request.Params("id") Is Nothing) Then
                'txtMA_XA.Enabled = False
                GetDetailBank(Request.Params("id").Trim)
            End If
            'cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn xóa dữ liệu đã chọn?') ")
        End If
    End Sub
    Protected Sub EnableButton_Nhom(ByVal Nhom As String)
        Nhom = clsCommon.Check_Maker_Checker(Nhom)
        hdfMa_Nhom.Value = Nhom
        If Nhom = "10" Then
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = True
            'cmdIn.Visible = True
            cmdDeleteUser.Visible = True
            cmdTaoMoi.Visible = True
            drpDuLieu.SelectedIndex = 0
            GetBankList()
            cmdTaoMoi.Enabled = True
        ElseIf Nhom = "11" Then
            btnDuyet.Visible = True
            btnTuChoi.Visible = True
            'rowTrangThai.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            drpDuLieu.SelectedIndex = 1
            GetBankList_new()
        Else
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            drpDuLieu.SelectedIndex = 0
            GetBankList()
        End If
    End Sub
    Protected Sub dtgdata_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgdata.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If Not MaNhom = "10" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim checkBox As HyperLink = DirectCast(e.Item.FindControl("Hyperlink1"), HyperLink)
                checkBox.Enabled = False
            End If
        End If
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        GetBankList()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        If drpDuLieu.SelectedIndex = 0 Then
            GetBankList()
            dtgdata.CurrentPageIndex = 0
            dtgdata.DataBind()
            grd_new.Visible = False
            dtgdata.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = False
                btnTuChoi.Visible = False
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = True
                cmdDeleteUser.Visible = True
                cmdTaoMoi.Visible = True
            End If
        Else
            GetBankList_new()
            grd_new.CurrentPageIndex = 0
            grd_new.DataBind()
            dtgdata.Visible = False
            grd_new.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = True
                btnTuChoi.Visible = True
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = False
                cmdDeleteUser.Visible = False
                cmdTaoMoi.Visible = False
            End If
        End If
    End Sub

    Private Sub ClearFrom()
        txtMA_NHChuyen.Text = ""
        txtTEN_NHChuyen.Text = ""
        txtMA_NHChuyen.Enabled = True
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        ClearFrom()
        Response.Redirect("~/pages/HeThong/DanhMuc/frmDMNganHang_Chuyen.aspx")
    End Sub

    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        txtMA_NHChuyen.Text = RemoveApostrophe(txtMA_NHChuyen.Text)
        If txtMA_NHChuyen.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Mã ngân hàng Chuyển.")
            txtMA_NHChuyen.Focus()
            Exit Sub
        End If
        If txtTEN_NHChuyen.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Tên ngân hàng chuyển.")
            txtTEN_NHChuyen.Focus()
            Exit Sub
        End If
        Dim infNHChuyen As New NHChuyen.infNHChuyen()
        Try
            infNHChuyen.Ma_NHC = txtMA_NHChuyen.Text.ToString.Trim
            infNHChuyen.Ten_NHC = txtTEN_NHChuyen.Text.ToString.Trim
            infNHChuyen.NGUOI_TAO = hdfTenDN.Value
            If Not Request.Params("id") Is Nothing Then
                infNHChuyen.TYPE = "2" 'UPDATE
                infNHChuyen.Ma_NHC = Request.Params("id").Trim
                If Not Check_Insert(infNHChuyen.Ma_NHC) Then
                    If objNHChuyen.Update(infNHChuyen, "10") Then
                        ClearFrom()
                        clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                    Else
                        clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                        txtMA_NHChuyen.Focus()
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                End If
            Else
                infNHChuyen.TYPE = "1" 'INSERT
                If Not Check_Insert(infNHChuyen.Ma_NHC) Then
                    If objNHChuyen.Insert(infNHChuyen, "10") Then
                        ClearFrom()
                        clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                    Else
                        clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                        txtMA_NHChuyen.Focus()
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Thông tin mã NH chuyển này đã tồn tại và đang chờ xử lý!")
                End If
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình cập nhật danh mục ngân hàng chuyển")
        Finally
        End Try
        GetBankList()
        'Response.Redirect("~/pages/HeThong/DanhMuc/frmDMNganHang.aspx")
    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim i As Integer
        Dim strID As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True
        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(dtgdata.Items(i).Cells(0).Text)
                If objNHChuyen.CheckDelete(strID) Then
                    clsCommon.ShowMessageBox(Me, "Dữ liệu đã sử dụng trong chức năng khác, không được xoá!")
                Else
                    Dim objMLNS As New NHChuyen.infNHChuyen
                    objMLNS.TYPE = "3" 'xóa dữ liệu
                    objMLNS.NGUOI_TAO = hdfTenDN.Value
                    objMLNS.Ma_NHC = strID
                    If Not Check_ThayDoi(objMLNS) Then
                        If objNHChuyen.Delete(objMLNS, "10") Then
                            clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                        Else
                            clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình xóa dữ liệu!")
                        End If
                    Else
                        clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                    End If
                End If
                v_count += 1
            End If
        Next
        ClearFrom()
        GetBankList()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub
    Protected Sub Get_Data_Item(ByRef objItem As NHChuyen.infNHChuyen)
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_nh_chuyen WHERE HIS_ID=" & objItem.HIS_ID
        dt = DataAccess.ExecuteToTable(WhereClause)
        objItem.Ten_NHC = dt.Rows(0)("ten_nh_chuyen").ToString
        objItem.Ma_NHC = dt.Rows(0)("ma_nh_chuyen").ToString
    End Sub
    'Kiểm tra có thay đổi thông tin đang chờ xử lý ko
    'Nếu có thay đổi thì trả về True/False
    Protected Function Check_ThayDoi(ByRef objItem As NHChuyen.infNHChuyen) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_nh_chuyen WHERE ma_nh_chuyen='" & objItem.Ma_NHC & "' AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function
    Protected Function Check_Insert(ByRef strMaNHC As String) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_nh_chuyen WHERE ma_nh_chuyen='" & strMaNHC & "' AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function

    Protected Sub btnDuyet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuyet.Click
        Dim i As Integer
        Dim strID As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                Dim objMLNS = New NHChuyen.infNHChuyen
                objMLNS.HIS_ID = strID
                objMLNS.NGUOI_DUYET = hdfTenDN.Value
                objMLNS.TT_XU_LY = "1"
                Dim objInsert As New NHChuyen.buNHChuyen
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        GetBankList_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Duyệt thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi duyệt!")
        End If
    End Sub

    Protected Sub btnTuChoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTuChoi.Click
        Dim i As Integer
        Dim strID As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                Dim objMLNS = New NHChuyen.infNHChuyen
                objMLNS.HIS_ID = strID
                objMLNS.NGUOI_DUYET = hdfTenDN.Value
                objMLNS.TT_XU_LY = "2"
                Dim objInsert As New NHChuyen.buNHChuyen
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        GetBankList_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Từ chối thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi từ chối!")
        End If
    End Sub

    Protected Sub grd_new_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_new.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If MaNhom = "10" Then
            e.Item.Cells(9).Visible = False
        ElseIf MaNhom = "11" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim tt_xu_ly As String = e.Item.Cells(7).Text.ToString
                If tt_xu_ly <> "0" Then
                    Dim checkBox As CheckBox = DirectCast(e.Item.FindControl("chkSelect"), CheckBox)
                    checkBox.Visible = False
                End If
            End If
        Else
            e.Item.Cells(9).Visible = False
        End If
    End Sub

    Protected Sub grd_new_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grd_new.PageIndexChanged
        GetBankList_new()
        grd_new.CurrentPageIndex = e.NewPageIndex
        grd_new.DataBind()
    End Sub
End Class
