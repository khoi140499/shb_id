﻿Imports Business.Common.mdlCommon
Imports Business.DanhMuc
Imports System.Data
Imports VBOracleLib
Imports ProcMegService
Imports System.Diagnostics


Partial Class pages_HeThong_DanhMuc_frmDMDBHC
    Inherits System.Web.UI.Page
    Private objDBHC As New DBHC.daDBHC
    Dim MaTK As String
    Dim TenTK As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            EnableButton_Nhom(Session.Item("MA_NHOM"))
            hdfTenDN.Value = Session.Item("UserName")
            cmdTaoMoi.Enabled = True
            If Not Request.Params("id") Is Nothing Then
                txtMaDBHC.Enabled = False
                load_Detail(Request.Params("id").Trim)
            End If
            'cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn xóa dữ liệu đã chọn?') ")
        End If
    End Sub
    Protected Sub EnableButton_Nhom(ByVal Nhom As String)
        Nhom = clsCommon.Check_Maker_Checker(Nhom)
        hdfMa_Nhom.Value = Nhom
        If Nhom = "10" Then
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = True
            'cmdIn.Visible = True
            cmdDeleteUser.Visible = True
            cmdTaoMoi.Visible = True
            cmdTruyvanThue.Visible = True
            drpDuLieu.SelectedIndex = 0
            load_Datagrid()
            cmdTaoMoi.Enabled = True
        ElseIf Nhom = "11" Then
            btnDuyet.Visible = True
            btnTuChoi.Visible = True
            'rowTrangThai.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            cmdTruyvanThue.Visible = False
            drpDuLieu.SelectedIndex = 1
            load_Datagrid_new()
        Else
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            cmdTruyvanThue.Visible = False
            drpDuLieu.SelectedIndex = 0
            load_Datagrid()
        End If
    End Sub
    Private Sub load_Datagrid()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtMaDBHC.Text.Trim <> "" Then
                WhereClause += " AND upper(ma_xa) like upper('%" + txtMaDBHC.Text.Trim + "%')"
            End If
            If txtTenDBHC.Text.Trim <> "" Then
                WhereClause += " AND upper(ten) like upper ('%" + txtTenDBHC.Text.Trim + "%')"
            End If
            If txtMaCha.Text.Trim <> "" Then
                WhereClause += " AND upper(ma_cha) like upper('%" + txtMaCha.Text.Trim + "%')"
            End If
            If cboTinh_trang.SelectedValue.Trim <> "" Then
                WhereClause += " AND tinh_trang = '" + cboTinh_trang.SelectedValue.Trim + "'"
            End If
            ds = objDBHC.Load(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgdata.DataSource = ds.Tables(0)
                dtgdata.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub
    Private Sub load_Datagrid_new()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtMaDBHC.Text.Trim <> "" Then
                WhereClause += " AND upper(b.ma_xa) like upper('%" + txtMaDBHC.Text.Trim + "%')"
            End If
            If txtTenDBHC.Text.Trim <> "" Then
                WhereClause += " AND upper(b.ten) like upper ('%" + txtTenDBHC.Text.Trim + "%')"
            End If
            If txtMaCha.Text.Trim <> "" Then
                WhereClause += " AND upper(b.ma_cha) like upper('%" + txtMaCha.Text.Trim + "%')"
            End If
            If drpTT_XU_LY.SelectedValue.ToString <> "" Then
                WhereClause += " AND b.TT_XU_LY = '" + drpTT_XU_LY.SelectedValue.ToString + "'"
            End If
            ds = objDBHC.Load_new(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                grd_new.DataSource = ds.Tables(0)
                grd_new.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub

    Protected Sub Get_Data_Item(ByRef objItem As DBHC.infDBHC)
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_dm_dbhc WHERE ID=" & objItem.ID
        dt = DataAccess.ExecuteToTable(WhereClause)
        objItem.TEN_DBHC = dt.Rows(0)("ten").ToString
        objItem.TINH_TRANG = dt.Rows(0)("tinh_trang").ToString
        objItem.Ma_Cha = dt.Rows(0)("Ma_Cha").ToString
        objItem.Xa_ID = dt.Rows(0)("Xa_ID").ToString
    End Sub
    'Kiểm tra có thay đổi thông tin đang chờ xử lý ko
    'Nếu có thay đổi thì trả về True/False
    Protected Function Check_ThayDoi(ByRef objItem As DBHC.infDBHC) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_dm_dbhc WHERE XA_ID=" & objItem.Xa_ID & " AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function
    Protected Function Check_Insert(ByRef strMaDBHC As String) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_dm_dbhc WHERE MA_XA='" & strMaDBHC & "' AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function
    Private Sub load_Detail(ByVal ID As String)
        Dim ds As DataSet
        ds = objDBHC.Load(" AND xa_id = '" + ID + "'")
        If ds.Tables(0).Rows.Count > 0 Then
            txtMaDBHC.Text = ds.Tables(0).Rows(0)("ma_xa").ToString
            txtTenDBHC.Text = ds.Tables(0).Rows(0)("ten").ToString
            cboTinh_trang.SelectedValue = ds.Tables(0).Rows(0)("tinh_trang").ToString
            txtMaCha.Text = ds.Tables(0).Rows(0)("ma_cha").ToString
        End If
        load_Datagrid()
    End Sub

    Private Sub Xoa_text()
        txtTenDBHC.Text = ""
        txtMaDBHC.Text = ""
        txtMaCha.Text = ""
        cboTinh_trang.SelectedValue = ""
        txtMaDBHC.Enabled = True
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Xoa_text()
        If Not Request.Params("id") Then
            Response.Redirect("~/pages/HeThong/DanhMuc/frmDMDBHC.aspx")
        End If
    End Sub

    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        txtMaDBHC.Text = RemoveApostrophe(txtMaDBHC.Text)
        txtTenDBHC.Text = RemoveApostrophe(txtTenDBHC.Text)
        If txtMaDBHC.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã địa bàn hành chính.")
            txtMaDBHC.Focus()
            Exit Sub
            '*****TYNK - Check ky tu dac biet*********19/02/2013
        Else
            If Regex.IsMatch(txtMaDBHC.Text.Trim, "^[a-zA-Z0-9]*$") = False Then
                clsCommon.ShowMessageBox(Me, "Chuỗi bạn nhập không đúng - Có ký tự đặc biệt.")
                txtMaDBHC.Focus()
                Exit Sub
            End If
            If (txtMaDBHC.Text.Length <> 5) Then
                clsCommon.ShowMessageBox(Me, "Mã địa bàn hành chính phải là 5 ký tự")
                txtMaDBHC.Focus()
                Exit Sub
            End If
            '***************************************************
        End If

        If txtTenDBHC.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên địa bàn hành chính.")
            txtTenDBHC.Focus()
            Exit Sub
        End If
        If txtMaCha.Text.Trim.Equals("") Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã tỉnh thành.!")
            txtMaCha.Focus()
            Exit Sub
        End If
        Dim infDBHC As New DBHC.infDBHC
        Try
            infDBHC.Ma_DBHC = txtMaDBHC.Text.ToString.Trim.Replace(".", "")
            infDBHC.TEN_DBHC = txtTenDBHC.Text.ToString.Trim
            infDBHC.Ma_Cha = txtMaCha.Text.ToString.Trim
            infDBHC.TINH_TRANG = cboTinh_trang.SelectedValue
            If Not Request.Params("id") Is Nothing Then
                infDBHC.TYPE = "2" 'UPDATE
                infDBHC.Xa_ID = Request.Params("id").ToString
                'Nếu có không có thay đổi chờ xử lý
                If Not Check_ThayDoi(infDBHC) Then
                    If objDBHC.Update(infDBHC, "10") Then
                        Xoa_text()
                        clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                        load_Datagrid()
                    Else
                        clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                        txtMaDBHC.Focus()
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                End If
            Else
                infDBHC.TYPE = "1" 'INSERT
                infDBHC.Xa_ID = getDataKey("TCS_CCH_ID_SEQ.NEXTVAL")
                If objDBHC.check_maDBHC(infDBHC.Ma_DBHC) = True Then
                    If Not Check_Insert(infDBHC.Ma_DBHC) Then
                        If objDBHC.Insert(infDBHC, "10") Then
                            Xoa_text()
                            load_Datagrid()
                            clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                        Else
                            clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                            txtMaDBHC.Focus()
                        End If
                    Else
                        clsCommon.ShowMessageBox(Me, "Thông tin Mã ĐBHC này đã tồn tại và đang chờ xử lý!")
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Mã địa bàn hành chính đã tồn tại.!")
                End If

            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình cập nhật dữ liệu")
        Finally
        End Try

    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim i As Integer
        Dim strID As String
        Dim strMa_DBHC As String
        Dim ds As DataSet
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(dtgdata.Items(i).Cells(0).Text)
                strMa_DBHC = Trim(dtgdata.Items(i).Cells(1).Text)
                ds = objDBHC.CheckDelete(strID)
                If ds.Tables(0).Rows.Count > 0 Then
                    clsCommon.ShowMessageBox(Me, "Dữ liệu đã sử dụng trong chức năng khác, không được xoá!")
                Else
                    Dim objMLNS As New DBHC.infDBHC
                    objMLNS.Xa_ID = strID
                    objMLNS.TYPE = "3" 'xóa dữ liệu
                    objMLNS.NGUOI_TAO = hdfTenDN.Value
                    objMLNS.Ma_DBHC = strMa_DBHC
                    If Not Check_ThayDoi(objMLNS) Then
                        If objDBHC.Delete(objMLNS, "10") Then
                            clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                        Else
                            clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình xóa dữ liệu!")
                        End If
                    Else
                        clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                    End If
                End If
                v_count += 1
            End If
        Next
        load_Datagrid()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub

    Protected Sub dtgdata_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgdata.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If Not MaNhom = "10" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim checkBox As HyperLink = DirectCast(e.Item.FindControl("Hyperlink1"), HyperLink)
                checkBox.Enabled = False
            End If
        End If
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        load_Datagrid()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        If drpDuLieu.SelectedIndex = 0 Then
            load_Datagrid()
            dtgdata.CurrentPageIndex = 0
            dtgdata.DataBind()
            grd_new.Visible = False
            dtgdata.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = False
                btnTuChoi.Visible = False
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = True
                'cmdIn.Visible = True
                cmdDeleteUser.Visible = True
                cmdTaoMoi.Visible = True
                cmdTruyvanThue.Visible = True
            End If
        Else
            load_Datagrid_new()
            grd_new.CurrentPageIndex = 0
            grd_new.DataBind()
            dtgdata.Visible = False
            grd_new.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = True
                btnTuChoi.Visible = True
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = False
                'cmdIn.Visible = False
                cmdDeleteUser.Visible = False
                cmdTaoMoi.Visible = False
                cmdTruyvanThue.Visible = False
            End If
        End If
        cmdTaoMoi.Enabled = False
    End Sub

    Protected Sub cmdTruyvanThue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTruyvanThue.Click
        Try
            ProcMegService.ProMsg.getMSG07(MessageType.strMSG11, "Trao đổi danh mục DBHC")
            load_Datagrid()
            lblError.Text = "Đồng bộ danh mục thành công"
        Catch ex As Exception
            lblError.Text = ex.Message.ToString()
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Lỗi trong quá trình đồng bộ dữ liệu Danh mục Địa bàn hành chính từ TC Thuế : Mô tả=" & ex.Message.ToString())
        End Try
    End Sub

    Protected Sub grd_new_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_new.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If MaNhom = "10" Then
            e.Item.Cells(13).Visible = False
        ElseIf MaNhom = "11" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim tt_xu_ly As String = e.Item.Cells(11).Text.ToString
                If tt_xu_ly <> "0" Then
                    Dim checkBox As CheckBox = DirectCast(e.Item.FindControl("chkSelect"), CheckBox)
                    checkBox.Visible = False
                End If
            End If
        Else
            e.Item.Cells(13).Visible = False
        End If
    End Sub

    Protected Sub grd_new_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grd_new.PageIndexChanged
        load_Datagrid_new()
        grd_new.CurrentPageIndex = e.NewPageIndex
        grd_new.DataBind()
    End Sub

    Protected Sub btnDuyet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuyet.Click
        Dim i As Integer
        Dim strID As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                Dim objMLNS = New DBHC.infDBHC
                objMLNS.ID = strID
                objMLNS.NGUOI_DUYET = hdfTenDN.Value
                objMLNS.TT_XU_LY = "1"
                Dim objInsert As New DBHC.daDBHC
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        load_Datagrid_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Duyệt thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để duyệt!")
        End If
    End Sub

    Protected Sub btnTuChoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTuChoi.Click
        Dim i As Integer
        Dim strID As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                Dim objMLNS = New DBHC.infDBHC
                objMLNS.ID = strID
                objMLNS.NGUOI_DUYET = hdfTenDN.Value
                objMLNS.TT_XU_LY = "2"
                Dim objInsert As New DBHC.daDBHC
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        load_Datagrid_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Từ chối thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi từ chối!")
        End If
    End Sub
End Class
