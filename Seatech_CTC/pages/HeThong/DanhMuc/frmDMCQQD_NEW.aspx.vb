﻿Imports Business.DanhMuc
Imports System.Data
Imports Business
Imports Business.Common.mdlCommon
Imports VBOracleLib

Partial Class pages_HeThong_DanhMuc_frmDMCQQD_NEW
    Inherits System.Web.UI.Page
    Private mv_objUser As New CTuUser
    Private objDanhMuc As New buDanhMuc("CQQD")
    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    If Session.IsNewSession Then
    '        ' Force session to be created;
    '        ' otherwise the session ID changes on every request.
    '        Session("ForceSession") = DateTime.Now
    '    End If
    '    ' 'Sign' the viewstate with the current session.
    '    Me.ViewStateUserKey = Session.SessionID
    '    If Page.EnableViewState Then
    '        ' Make sure ViewState wasn't passed on the querystring.
    '        ' This helps prevent one-click attacks.
    '        If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
    '            Throw New Exception("Viewstate existed, but not on the form.")
    '        End If
    '    End If
    'End Sub
 

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        objDanhMuc = New buDanhMuc("CQQD")
        If Not IsPostBack Then
            EnableButton_Nhom(Session.Item("MA_NHOM"))
            hdfTenDN.Value = Session.Item("UserName")
            If Not Request.Params("id") Is Nothing Then
                'txtMa_CQQD.Enabled = False
                load_Detail(Request.Params("id").Trim)
            End If
            'cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn thực hiện không?') ")
        End If
    End Sub

    Protected Sub load_Detail(ByVal id As String)
        Dim ds As DataSet
        ds = objDanhMuc.GetDataSet(" AND id = '" + id + "'")
        If ds.Tables(0).Rows.Count > 0 Then
            txtMa_CQQD.Text = ds.Tables(0).Rows(0)("MA_CQQD").ToString
            txtTen_CQQD.Text = ds.Tables(0).Rows(0)("TEN_CQQD").ToString
            txtMa_KB.Text = ds.Tables(0).Rows(0)("MA_KB").ToString
            txtTen_KB.Value = Business.Common.mdlCommon.Get_TenKhoBac(ds.Tables(0).Rows(0)("MA_KB").ToString)
            txtMa_CQTHU.Text = ds.Tables(0).Rows(0)("MA_CQTHU").ToString
            txtMa_DBHC.Text = ds.Tables(0).Rows(0)("ma_dbhc").ToString
            txtGhiChu.Text = ds.Tables(0).Rows(0)("GHI_CHU").ToString
            txtIDSE.Text = ds.Tables(0).Rows(0)("ID").ToString
            txtMDate.Value = ds.Tables(0).Rows(0)("MDATE").ToString
            txtMAKER.Value = ds.Tables(0).Rows(0)("MAKER").ToString
            If ds.Tables(0).Rows(0)("tinh_trang").ToString = "Đang hoạt động" Then
                cboTinh_trang.SelectedValue = 1
            Else
                cboTinh_trang.SelectedValue = 0
            End If
        End If
     
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenKB(ByVal strSHKBM As String) As String
        Dim returnValue As String = ""
        If strSHKBM.Trim <> "" Then
            returnValue = Get_TenKhoBac(strSHKBM)
        End If
        Return returnValue
    End Function

   
    Private Sub GetInfoDetail(ByVal id As String)
        Dim ds As DataSet
        Dim buOBJ As buDM_CQQD = New buDM_CQQD()
        ds = buOBJ.GetInfoDetail(" and a.ID = '" & id & "'")
        If ds.Tables(0).Rows.Count > 0 Then
            'ma_lh,ten_lh,shkb,ma_chuong,ma_tmuc,ma_tkns,ghi_chu,mdate,maker
            txtMa_CQQD.Text = ds.Tables(0).Rows(0)("MA_CQQD").ToString
            txtTen_CQQD.Text = ds.Tables(0).Rows(0)("TEN_CQQD").ToString
            txtMa_KB.Text = ds.Tables(0).Rows(0)("MA_KB").ToString
            txtTen_KB.Value = Business.Common.mdlCommon.Get_TenKhoBac(ds.Tables(0).Rows(0)("MA_KB").ToString)
            txtMa_CQTHU.Text = ds.Tables(0).Rows(0)("MA_CQTHU").ToString
            txtMa_DBHC.Text = ds.Tables(0).Rows(0)("ma_dbhc").ToString
            txtGhiChu.Text = ds.Tables(0).Rows(0)("GHI_CHU").ToString
            txtIDSE.Text = ds.Tables(0).Rows(0)("ID").ToString
            txtMDate.Value = ds.Tables(0).Rows(0)("MDATE").ToString
            txtMAKER.Value = ds.Tables(0).Rows(0)("MAKER").ToString
            If ds.Tables(0).Rows(0)("tinh_trang").ToString = "Đang hoạt động" Then
                cboTinh_trang.SelectedValue = 1
            Else
                cboTinh_trang.SelectedValue = 0
            End If
        End If
    End Sub


    Private Sub load_dataGrid()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            
            'If txtMa_CQQD.Enabled Then
            If txtMa_CQQD.Text.Trim <> "" Then
                WhereClause += " AND a.ma_cqqd like '%" + FilterSQLi(txtMa_CQQD.Text.Trim) + "%'"
            End If
            If txtMa_KB.Text.Trim <> "" Then
                WhereClause += " AND upper(a.ma_kb) like upper('%" + FilterSQLi(txtMa_KB.Text.Trim) + "%')"
            End If
            If txtTen_CQQD.Text.Trim <> "" Then
                WhereClause += " AND upper(a.TEN_CQQD) like upper('%" + FilterSQLi(txtTen_CQQD.Text.Trim) + "%')"
            End If
            If txtGhiChu.Text.Trim <> "" Then
                WhereClause += " AND upper(a.GHI_CHU) like upper('%" + FilterSQLi(txtGhiChu.Text.Trim) + "%')"
            End If
            If txtMa_CQTHU.Text.Trim <> "" Then
                WhereClause += " AND a.MA_CQTHU like '%" + FilterSQLi(txtMa_CQTHU.Text.Trim) + "%'"
            End If
            If cboTinh_trang.SelectedItem.ToString() <> "" Then
                WhereClause += " AND tinh_trang = '" + cboTinh_trang.SelectedValue.ToString() + "'"
            End If
            'End If

            ds = objDanhMuc.GetDataSet(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgdata.DataSource = ds.Tables(0)
                dtgdata.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If

        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi:" & ex.Message)
        End Try
    End Sub

    Private Sub load_dataGrid_new()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try

            'If txtMa_CQQD.Enabled Then
            If txtMa_CQQD.Text.Trim <> "" Then
                WhereClause += " AND a.ma_cqqd like '%" + FilterSQLi(txtMa_CQQD.Text.Trim) + "%'"
            End If
            If txtMa_KB.Text.Trim <> "" Then
                WhereClause += " AND upper(a.ma_kb) like upper('%" + FilterSQLi(txtMa_KB.Text.Trim) + "%')"
            End If
            If txtTen_CQQD.Text.Trim <> "" Then
                WhereClause += " AND upper(a.TEN_CQQD) like upper('%" + FilterSQLi(txtTen_CQQD.Text.Trim) + "%')"
            End If
            If txtGhiChu.Text.Trim <> "" Then
                WhereClause += " AND upper(a.GHI_CHU) like upper('%" + FilterSQLi(txtGhiChu.Text.Trim) + "%')"
            End If
            If txtMa_CQTHU.Text.Trim <> "" Then
                WhereClause += " AND a.MA_CQTHU like '%" + FilterSQLi(txtMa_CQTHU.Text.Trim) + "%'"
            End If
            If drpTT_XU_LY.SelectedValue.ToString() <> "" Then
                WhereClause += " AND b.TT_XU_LY = '" + drpTT_XU_LY.SelectedValue.ToString() + "'"
            End If
            'End If

            ds = objDanhMuc.GetDataSet_new(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                grd_new.DataSource = ds.Tables(0)
                grd_new.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi:" & ex.Message)
        End Try
    End Sub

    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        Dim objCQQD As Object

        txtMa_CQQD.Text = RemoveApostrophe(txtMa_CQQD.Text)
        txtGhiChu.Text = RemoveApostrophe(txtGhiChu.Text)

        Dim regex_code As String = clsCTU.TCS_GETTS("SPECIAL_CHAR")
        If txtMa_KB.Text.Trim <> "" Then
            If clsCommon.checkContansChar(txtMa_KB.Text.Trim, regex_code) Then
                clsCommon.ShowMessageBox(Me, "Bạn nhập vào Số hiệu kho bạc không đúng - Có ký tự đặc biệt đã qui định.")
                txtMa_KB.Focus()
                Exit Sub
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Bạn nhập vào Số hiệu kho bạc.")
            txtMa_KB.Focus()
            Exit Sub
        End If

        If txtMa_CQQD.Text.ToString.Trim.Length <= 0 Then
            clsCommon.ShowMessageBox(Me, "Bạn nhập vào mã cơ quan quyết định.")
            txtMa_CQQD.Focus()
            Exit Sub
        End If

        If txtTen_CQQD.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên cơ quan quyết định.")
            txtTen_CQQD.Focus()
            Exit Sub
        End If


        Try
            objCQQD = New CQQD.infCQQD

            objCQQD.MA_CQQD = txtMa_CQQD.Text.ToString.Trim
            objCQQD.TEN_CQQD = txtTen_CQQD.Text.ToString.Trim
            objCQQD.MA_KB = txtMa_KB.Text.ToString.Trim
            objCQQD.MA_CQTHU = txtMa_CQTHU.Text.ToString.Trim
            objCQQD.MA_DBHC = txtMa_DBHC.Text.ToString.Trim
            objCQQD.TINH_TRANG = (1 - cboTinh_trang.SelectedIndex).ToString
            objCQQD.GHI_CHU = txtGhiChu.Text.ToString.Trim
            objCQQD.NGUOI_TAO = hdfTenDN.Value


            'Dim objInsert As buDM_CQQD = New buDM_CQQD()
            Dim objInsert As New CQQD.buDM_CQQD_NEW
            If Not Request.Params("id") Is Nothing Then
                objCQQD.TYPE = "2" 'UPDATE
                objCQQD.ID = Request.Params("id").Trim
                'Nếu có không có thay đổi chờ xử lý
                If Not Check_ThayDoi(objCQQD) Then
                    If objInsert.Update(objCQQD, "10") Then
                        ClearFrom()
                        clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                    Else
                        txtMa_CQQD.Focus()
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                End If
            Else
                objCQQD.TYPE = "1" 'INSERT
                objCQQD.ID = getDataKey("tcs_cqqd_bienlai_seq.NEXTVAL")
                'If Not Check_Insert(objCQQD.MA_CQQD) Then
                '10 thêm vào trong HIS_DM_CAP_CHUONG
                If objInsert.Insert(objCQQD, "10") Then
                    ClearFrom()
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                Else
                    txtMa_CQQD.Focus()
                End If
                '    Else
                '    clsCommon.ShowMessageBox(Me, "Thông tin mã chương này đã tồn tại và đang chờ xử lý!")
                'End If

            End If
        Catch ex As Exception
            If objDanhMuc.Status = "EDIT" Then
                'clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình cập nhật " &  & " !")
            Else
                'clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình tạo mới " & Ten & " !")
            End If
        Finally
        End Try
        load_dataGrid()
    End Sub

    Protected Sub Get_Data_Item(ByRef objItem As CQQD.infCQQD)
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM HIS_TCS_DM_CQQD WHERE ID_CQQD ='" & objItem.ID_CQQD & "'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        objItem.MA_CQQD = dt.Rows(0)("MA_CQQD").ToString
        objItem.TEN_CQQD = dt.Rows(0)("TEN_CQQD").ToString
        'objItem.CAP = dt.Rows(0)("CAP").ToString
        objItem.MA_CQTHU = dt.Rows(0)("MA_CQTHU").ToString
        objItem.MA_DBHC = dt.Rows(0)("MA_DBHC").ToString
        'objItem.MA_LH = dt.Rows(0)("MA_LH").ToString
        objItem.MA_KB = dt.Rows(0)("MA_KB").ToString
        objItem.TRANGTHAI = dt.Rows(0)("TRANGTHAI").ToString
        objItem.TINH_TRANG = dt.Rows(0)("tinh_trang").ToString
        objItem.GHI_CHU = dt.Rows(0)("ghi_chu").ToString
        objItem.ID = dt.Rows(0)("id").ToString
    End Sub

    Protected Sub btnDuyet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuyet.Click
        Dim i As Integer
        Dim strID_CQQD As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID_CQQD = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                Dim objCQQD = New CQQD.infCQQD

                objCQQD.ID_CQQD = strID_CQQD
                objCQQD.NGUOI_DUYET = hdfTenDN.Value
                objCQQD.TT_XU_LY = "1"
                Dim objInsert As New CQQD.buDM_CQQD_NEW
                'Dim objInsert As buDM_CQQD = New buDM_CQQD()
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objCQQD, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objCQQD)
                    If objInsert.Update(objCQQD, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    Get_Data_Item(objCQQD)
                    If objInsert.Delete(objCQQD, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        load_dataGrid_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Duyệt thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi duyệt!")
        End If
    End Sub

   

    Protected Function Check_ThayDoi(ByRef objItem As CQQD.infCQQD) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM HIS_TCS_DM_CQQD WHERE ID='" & objItem.ID & "' AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function
    Protected Function Check_Insert(ByRef strMA_CQQD As String) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM HIS_TCS_DM_CQQD WHERE MA_CQQD='" & strMA_CQQD & "' AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function

    Protected Sub grd_new_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_new.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If MaNhom = "10" Then
            e.Item.Cells(19).Visible = False
        ElseIf MaNhom = "11" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim tt_xu_ly As String = e.Item.Cells(17).Text.ToString
                If tt_xu_ly <> "0" Then
                    Dim checkBox As CheckBox = DirectCast(e.Item.FindControl("chkSelect"), CheckBox)
                    checkBox.Visible = False
                End If
            End If
        Else
            e.Item.Cells(19).Visible = False
        End If

    End Sub

    Protected Sub btnTuChoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTuChoi.Click
        Dim i As Integer
        Dim strID_CQQD As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID_CQQD = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                Dim objCQQD = New CQQD.infCQQD
                objCQQD.ID_CQQD = strID_CQQD
                objCQQD.NGUOI_DUYET = hdfTenDN.Value
                objCQQD.TT_XU_LY = "2"
                Dim objInsert As New CQQD.buDM_CQQD_NEW
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objCQQD, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objCQQD)
                    If objInsert.Update(objCQQD, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    If objInsert.Delete(objCQQD, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        load_dataGrid_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Từ chối thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi từ chối!")
        End If
    End Sub


    Protected Sub EnableButton_Nhom(ByVal Nhom As String)
        Nhom = clsCommon.Check_Maker_Checker(Nhom)
        hdfMa_Nhom.Value = Nhom
        If Nhom = "10" Then
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = True
            'cmdIn.Visible = True
            cmdDeleteUser.Visible = True
            cmdTaoMoi.Visible = True

            drpDuLieu.SelectedIndex = 0
            load_dataGrid()
            cmdTaoMoi.Enabled = True
        ElseIf Nhom = "11" Then
            btnDuyet.Visible = True
            btnTuChoi.Visible = True
            'rowTrangThai.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False

            drpDuLieu.SelectedIndex = 1
            load_dataGrid_new()
        Else
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False

            drpDuLieu.SelectedIndex = 0
            load_dataGrid()
        End If
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        If drpDuLieu.SelectedIndex = 0 Then
            load_dataGrid()
            dtgdata.CurrentPageIndex = 0
            dtgdata.DataBind()
            grd_new.Visible = False
            dtgdata.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = False
                btnTuChoi.Visible = False
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = True
                'cmdIn.Visible = True
                cmdDeleteUser.Visible = True
                cmdTaoMoi.Visible = True

            End If
        Else
            load_dataGrid_new()
            grd_new.CurrentPageIndex = 0
            grd_new.DataBind()
            dtgdata.Visible = False
            grd_new.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = True
                btnTuChoi.Visible = True
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = False
                'cmdIn.Visible = False
                cmdDeleteUser.Visible = False
                cmdTaoMoi.Visible = False

            End If
        End If
        cmdTaoMoi.Enabled = False
    End Sub

    

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim i As Integer
        Dim strMA_CQQD As String
        Dim strTEN_CQQD As String
        Dim strID As String
        Dim objDel As New CQQD.buDM_CQQD_NEW()
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(dtgdata.Items(i).Cells(0).Text)
                strMA_CQQD = Trim(dtgdata.Items(i).Cells(2).Text)
                strTEN_CQQD = Trim(dtgdata.Items(i).Cells(3).Text)
                'If objDel.CheckDelete(strMA_CQQD) Then
                '    clsCommon.ShowMessageBox(Me, "Dữ liệu đã sử dụng trong chức năng khác, không được xoá!")
                'Else
                Dim objCQQD As New CQQD.infCQQD
                objCQQD.ID = strID
                objCQQD.TYPE = "3" 'xóa dữ liệu
                objCQQD.NGUOI_TAO = hdfTenDN.Value
                objCQQD.MA_CQQD = strMA_CQQD
                objCQQD.TEN_CQQD = strTEN_CQQD
                If Not Check_ThayDoi(objCQQD) Then
                    'Get_Data_Item(objCQQD)
                    If objDel.Delete(objCQQD, "10") Then
                        clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                    Else
                        clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình xóa dữ liệu!")
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                End If

                'End If
                v_count += 1
            End If
        Next
        load_dataGrid()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        load_dataGrid()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        ClearFrom()
        cmdTaoMoi.Enabled = True
        If Not Request.Params("id") Then
            Response.Redirect("~/pages/HeThong/DanhMuc/frmDMCQQD_NEW.aspx")
        End If
    End Sub
    Private Sub ClearFrom()
        txtMa_CQQD.Text = ""
        txtTen_CQQD.Text = ""
        txtMa_KB.Text = ""
        txtTen_KB.Value = ""
        txtMa_CQTHU.Text = ""
        txtMa_DBHC.Text = ""
        txtGhiChu.Text = ""
        txtMDate.Value = ""
        txtMAKER.Value = ""
        txtIDSE.Text = ""
        cmdTaoMoi.Enabled = True

    End Sub

    Protected Sub grd_new_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grd_new.PageIndexChanged
        load_dataGrid_new()
        grd_new.CurrentPageIndex = e.NewPageIndex
        grd_new.DataBind()
    End Sub

    Protected Sub dtgdata_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgdata.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If Not MaNhom = "10" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim checkBox As HyperLink = DirectCast(e.Item.FindControl("Hyperlink1"), HyperLink)
                checkBox.Enabled = False
            End If
        End If
    End Sub

    'Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    'End Sub
End Class
