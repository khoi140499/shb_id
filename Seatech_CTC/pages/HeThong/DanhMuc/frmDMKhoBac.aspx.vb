﻿Imports Business.Common.mdlCommon
Imports Business.DanhMuc
Imports System.Data
Imports VBOracleLib
Imports ProcMegService
Imports System.Diagnostics

Partial Class pages_HeThong_DanhMuc_frmDMKhoBac
    Inherits System.Web.UI.Page
    Private objKhoBac As New KhoBac.daKhoBac
    Dim MaTK As String
    Dim TenTK As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            EnableButton_Nhom(Session.Item("MA_NHOM"))
            hdfTenDN.Value = Session.Item("UserName")
            cmdTaoMoi.Enabled = True
            If Not Request.Params("id") Is Nothing Then
                txtSHKB.Enabled = False
                load_Detail(Request.Params("id").Trim)
            End If
            'cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn thực hiện không?') ")
        End If
    End Sub
    Protected Sub EnableButton_Nhom(ByVal Nhom As String)
        Nhom = clsCommon.Check_Maker_Checker(Nhom)
        hdfMa_Nhom.Value = Nhom
        If Nhom = "10" Then
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = True
            'cmdIn.Visible = True
            cmdDeleteUser.Visible = True
            cmdTaoMoi.Visible = True
            cmdTruyvanThue.Visible = True
            drpDuLieu.SelectedIndex = 0
            load_Datagrid()
            cmdTaoMoi.Enabled = True
        ElseIf Nhom = "11" Then
            btnDuyet.Visible = True
            btnTuChoi.Visible = True
            'rowTrangThai.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            cmdTruyvanThue.Visible = False
            drpDuLieu.SelectedIndex = 1
            load_Datagrid_new()
        Else
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            cmdTruyvanThue.Visible = False
            drpDuLieu.SelectedIndex = 0
            load_Datagrid()
        End If
    End Sub
    Private Sub load_Datagrid()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtSHKB.Enabled Then
                If txtSHKB.Text <> "" Then
                    WhereClause += " AND upper(SHKB) like upper('%" + txtSHKB.Text.Trim + "%')"
                End If
                If txtTenKB.Text <> "" Then
                    WhereClause += " AND upper(ten) like upper('%" + txtTenKB.Text.Trim + "%')"
                End If
                If txtMaTinh.Text <> "" Then
                    WhereClause += " AND upper(MA_TINH) like upper('%" + txtMaTinh.Text.Trim + "%')"
                End If
                If txtMaDBHC.Text <> "" Then
                    WhereClause += " AND upper(MA_DB) like upper('%" + txtMaDBHC.Text.Trim + "%')"
                End If
                If cboTinh_trang.SelectedValue <> "" Then
                    WhereClause += " AND tinh_trang = '" + cboTinh_trang.SelectedValue + "'"
                End If
            End If
            ds = objKhoBac.Load(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgdata.DataSource = ds.Tables(0)
                dtgdata.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub
    Private Sub load_Datagrid_new()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtSHKB.Enabled Then
                If txtSHKB.Text <> "" Then
                    WhereClause += " AND upper(b.SHKB) like upper('%" + txtSHKB.Text.Trim + "%')"
                End If
                If txtTenKB.Text <> "" Then
                    WhereClause += " AND upper(b.ten) like upper('%" + txtTenKB.Text.Trim + "%')"
                End If
                If txtMaTinh.Text <> "" Then
                    WhereClause += " AND upper(b.MA_TINH) like upper('%" + txtMaTinh.Text.Trim + "%')"
                End If
                If txtMaDBHC.Text <> "" Then
                    WhereClause += " AND upper(b.MA_DB) like upper('%" + txtMaDBHC.Text.Trim + "%')"
                End If
                If drpTT_XU_LY.SelectedValue <> "" Then
                    WhereClause += " AND b.TT_XU_LY = '" + drpTT_XU_LY.SelectedValue + "'"
                End If
            End If
            ds = objKhoBac.Load_new(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                grd_new.DataSource = ds.Tables(0)
                grd_new.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub
    Protected Sub Get_Data_Item(ByRef objItem As KhoBac.infKhoBac)
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_dm_kho_bac WHERE ID=" & objItem.ID
        dt = DataAccess.ExecuteToTable(WhereClause)
        objItem.TEN_KB = dt.Rows(0)("ten").ToString
        objItem.TINH_TRANG = dt.Rows(0)("tinh_trang").ToString
        objItem.MaTinh = dt.Rows(0)("MA_TINH").ToString
        objItem.MaDBHC = dt.Rows(0)("MA_DB").ToString
        objItem.SHKB = dt.Rows(0)("SHKB").ToString
    End Sub
    'Kiểm tra có thay đổi thông tin đang chờ xử lý ko
    'Nếu có thay đổi thì trả về True/False
    Protected Function Check_ThayDoi(ByRef objItem As KhoBac.infKhoBac) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_dm_kho_bac WHERE shkb=" & objItem.SHKB & " AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function
    Protected Function Check_Insert(ByRef strSHKB As String) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_dm_kho_bac WHERE shkb=" & strSHKB & " AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function
    Private Sub load_Detail(ByVal ID As String)
        Dim ds As DataSet
        ds = objKhoBac.Load(" AND SHKB = '" + ID + "'")
        If ds.Tables(0).Rows.Count > 0 Then
            txtSHKB.Text = ds.Tables(0).Rows(0)("shkb").ToString
            txtTenKB.Text = ds.Tables(0).Rows(0)("ten").ToString
            txtMaTinh.Text = ds.Tables(0).Rows(0)("ma_tinh").ToString
            txtMaDBHC.Text = ds.Tables(0).Rows(0)("ma_db").ToString
            ' If ds.Tables(0).Rows(0)("tinh_trang").ToString = "Dang hoat dong" Then
            cboTinh_trang.SelectedValue = ds.Tables(0).Rows(0)("tinh_trang").ToString
            txtTen.Text = Get_TenDBHC(txtMaTinh.Text)
            txtTenDBHC.Text = Get_TenDBHC(txtMaDBHC.Text)
            'Else
            ' cboTinh_trang.SelectedValue = 1
            'End If
        End If
    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenTinh(ByVal strMaTinh As String) As String
        Dim strTenTinh As String = ""
        Try
            Dim strSQL As String = "SELECT ten from tcs_dm_xa where ma_xa = '" & strMaTinh & "'"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                strTenTinh = dt.Rows(0)("ten").ToString()
            End If
        Catch ex As Exception
            strTenTinh = ""

        End Try
        Return strTenTinh
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenDBHC(ByVal strMaDBHC As String) As String
        Dim strTenDBHC As String = ""
        Try
            Dim strSQL As String = "SELECT ten from tcs_dm_xa where ma_xa = '" & strMaDBHC & "'"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                strTenDBHC = dt.Rows(0)("ten").ToString()
            End If
        Catch ex As Exception
            strTenDBHC = ""

        End Try
        Return strTenDBHC
    End Function
    Private Sub Xoa_text()
        txtSHKB.Text = ""
        txtTenKB.Text = ""
        txtMaTinh.Text = ""
        txtMaDBHC.Text = ""
        cboTinh_trang.SelectedValue = ""
        txtSHKB.Enabled = True
    End Sub


    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Xoa_text()
        If Not Request.Params("id") Then
            Response.Redirect("~/pages/HeThong/DanhMuc/frmDMKhoBac.aspx")
        End If
    End Sub

    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        txtSHKB.Text = RemoveApostrophe(txtSHKB.Text)
        txtTenKB.Text = RemoveApostrophe(txtTenKB.Text)
        If txtSHKB.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào số hiệu kho bạc.")
            txtSHKB.Focus()
            Exit Sub
            '*****TYNK - Check độ dài mã SHKB*********25/02/2013
        ElseIf txtSHKB.Text.Trim.Length <> txtSHKB.MaxLength Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã Kho bạc có độ dài " & txtSHKB.MaxLength & " ký tự.")
            txtSHKB.Focus()
            Exit Sub
            '***************************************************
            '*****TYNK - Check ky tu dac biet*********19/02/2013
        Else
            If Regex.IsMatch(txtSHKB.Text.Trim, "^[a-zA-Z0-9]*$") = False Then
                clsCommon.ShowMessageBox(Me, "Chuỗi bạn nhập không đúng - Có ký tự đặc biệt.")
                txtSHKB.Focus()
                Exit Sub
            End If
            '***************************************************
        End If

        If txtTenKB.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên kho bạc.")
            txtTenKB.Focus()
            Exit Sub
        End If
        If cboTinh_trang.SelectedValue = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn hãy chọn tình trạng hoạt động.")
            txtTenKB.Focus()
            Exit Sub
        End If
        'If txtMaTinh.Text.ToString() = "" Then
        '    clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã Tỉnh.")
        '    txtMaTinh.Focus()
        '    Exit Sub
        'End If
        If txtMaDBHC.Text.ToString() = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã DBHC.")
            txtMaDBHC.Focus()
            Exit Sub
        End If
        Dim infKB As New KhoBac.infKhoBac
        Try
            infKB.SHKB = txtSHKB.Text.ToString.Trim.Replace(".", "")
            infKB.TEN_KB = txtTenKB.Text.ToString.Trim
            infKB.TINH_TRANG = cboTinh_trang.SelectedValue.ToString
            infKB.MaTinh = txtMaTinh.Text.ToString.Trim
            infKB.MaDBHC = txtMaDBHC.Text.ToString.Trim
            infKB.NGUOI_TAO = hdfTenDN.Value
            If Not Request.Params("id") Is Nothing Then
                infKB.TYPE = "2" 'UPDATE
                If Not Check_ThayDoi(infKB) Then
                    If objKhoBac.Update(infKB, "10") Then
                        Xoa_text()
                        clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                        load_Datagrid()
                    Else
                        clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                        txtSHKB.Focus()
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                End If
            Else
                If objKhoBac.CheckExist(infKB) = True Then
                    clsCommon.ShowMessageBox(Me, "Số hiệu kho bạc đã tồn tại trong hệ thống!")
                Else
                    If Not Check_Insert(infKB.SHKB) Then
                        infKB.TYPE = "1" 'INSERT
                        If objKhoBac.Insert(infKB, "10") Then
                            Xoa_text()
                            clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                            load_Datagrid()
                        Else
                            clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                            txtSHKB.Focus()
                        End If
                    Else
                        clsCommon.ShowMessageBox(Me, "Thông tin SHKB này đã tồn tại và đang chờ xử lý!")
                    End If
                End If
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình cập nhật dữ liệu")
        Finally
        End Try

    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim i As Integer
        Dim strMaTK As String
        Dim ds As DataSet
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True
        Try
            For i = 0 To dtgdata.Items.Count - 1
                Dim chkSelect As CheckBox
                chkSelect = dtgdata.Items(i).FindControl("chkSelect")
                If chkSelect.Checked Then
                    strMaTK = Trim(dtgdata.Items(i).Cells(0).Text)
                    ds = objKhoBac.CheckDelete(strMaTK)
                    If ds.Tables(0).Rows.Count > 0 Then
                        clsCommon.ShowMessageBox(Me, "Dữ liệu đã sử dụng trong chức năng khác, không được xoá!")
                    Else
                        Dim objMLNS As New KhoBac.infKhoBac
                        objMLNS.SHKB = strMaTK
                        objMLNS.TYPE = "3" 'xóa dữ liệu
                        objMLNS.NGUOI_TAO = hdfTenDN.Value
                        If Not Check_ThayDoi(objMLNS) Then
                            If objKhoBac.Delete(objMLNS, "10") Then
                                clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                                Xoa_text()
                                load_Datagrid()
                            End If
                        Else
                            clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                        End If
                    End If
                    v_count += 1
                End If
            Next
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình xóa dữ liệu kho bạc!" & ex.ToString())
        End Try
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub

    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Try
            Dim frm As New Business.BaoCao.infBaoCao
            'Business.Common.mdlSystemVariables.gLoaiBaoCao = Report_Type.DM_TAIKHOAN
            Session("objBaoCao") = frm
            clsCommon.OpenNewWindow(Me, "../../BaoCao/frmShowReports.aspx&BC=" & Report_Type.DM_TAIKHOAN & "", "ManPower")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dtgdata_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgdata.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If Not MaNhom = "10" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim checkBox As HyperLink = DirectCast(e.Item.FindControl("Hyperlink1"), HyperLink)
                checkBox.Enabled = False
            End If
        End If
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        load_Datagrid()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        If drpDuLieu.SelectedIndex = 0 Then
            load_Datagrid()
            dtgdata.CurrentPageIndex = 0
            dtgdata.DataBind()
            grd_new.Visible = False
            dtgdata.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = False
                btnTuChoi.Visible = False
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = True
                'cmdIn.Visible = True
                cmdDeleteUser.Visible = True
                cmdTaoMoi.Visible = True
                cmdTruyvanThue.Visible = True
            End If
        Else
            load_Datagrid_new()
            grd_new.CurrentPageIndex = 0
            grd_new.DataBind()
            dtgdata.Visible = False
            grd_new.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = True
                btnTuChoi.Visible = True
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = False
                'cmdIn.Visible = False
                cmdDeleteUser.Visible = False
                cmdTaoMoi.Visible = False
                cmdTruyvanThue.Visible = False
            End If
        End If
        cmdTaoMoi.Enabled = False
    End Sub
    Protected Sub cmdTruyvanThue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTruyvanThue.Click
        'thêm aleert confirm trước khi làm
        Try
            ProcMegService.ProMsg.getMSG07(MessageType.strMSG15, "Trao đổi dm Kho bạc")
            load_Datagrid()
            lblError.Text = "Đồng bộ danh mục thành công"
        Catch ex As Exception
            lblError.Text = ex.Message.ToString()
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Lỗi trong quá trình đồng bộ dữ liệu Danh mục Kho bạc từ TC Thuế : Mô tả=" & ex.Message.ToString())
        End Try
    End Sub

    Protected Sub grd_new_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_new.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If MaNhom = "10" Then
            e.Item.Cells(15).Visible = False
        ElseIf MaNhom = "11" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim tt_xu_ly As String = e.Item.Cells(13).Text.ToString
                If tt_xu_ly <> "0" Then
                    Dim checkBox As CheckBox = DirectCast(e.Item.FindControl("chkSelect"), CheckBox)
                    checkBox.Visible = False
                End If
            End If
        Else
            e.Item.Cells(15).Visible = False
        End If
    End Sub

    Protected Sub grd_new_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grd_new.PageIndexChanged
        load_Datagrid_new()
        grd_new.CurrentPageIndex = e.NewPageIndex
        grd_new.DataBind()
    End Sub

    Protected Sub btnDuyet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuyet.Click
        Dim i As Integer
        Dim strID As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                Dim objMLNS = New KhoBac.infKhoBac
                objMLNS.ID = strID
                objMLNS.NGUOI_DUYET = hdfTenDN.Value
                objMLNS.TT_XU_LY = "1"
                Dim objInsert As New KhoBac.daKhoBac
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        load_Datagrid_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Duyệt thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi duyệt!")
        End If
    End Sub

    Protected Sub btnTuChoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTuChoi.Click
        Dim i As Integer
        Dim strID As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                Dim objMLNS = New KhoBac.infKhoBac
                objMLNS.ID = strID
                objMLNS.NGUOI_DUYET = hdfTenDN.Value
                objMLNS.TT_XU_LY = "2"
                Dim objInsert As New KhoBac.daKhoBac
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        load_Datagrid_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Từ chối thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi từ chối!")
        End If
    End Sub
End Class
