﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmDMChiNhanh_NganHang.aspx.vb" Inherits="pages_HeThong_DanhMuc_frmDMCN_NganHang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
<script language="javascript" type="text/javascript">
function ShowLov(strType)
    {
        if (strType=="DBHC") return FindDanhMuc('DBHC','<%=txtMA_XA.ClientID %>'); 
    }
    function FindDanhMuc(strPage,txtID) {
        if (strPage=="DBHC")  strPage="DBHCAll";
        var returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&initParam=" + document.getElementById(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;","_new");
        if (returnValue != null) {
            document.getElementById(txtID).value = returnValue.ID;
        }
    }
    function ShowLov1(strType)
    {
        if (strType=="CNNganHang") return FindDanhMuc1('CNNganHang','<%=txtMa_CN.ClientID %>');         
    }
      function FindDanhMuc1(strPage,txtID) 
    {        
        var returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&initParam=" + document.getElementById(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;","_new");
        if (returnValue != null) {
            document.getElementById(txtID).value = returnValue.ID;
        }
    } 
   
</script>
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">DANH MỤC CHI NHÁNH NGÂN HÀNG</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <asp:HiddenField ID="hdfMa_Nhom" runat="server" />
            <asp:HiddenField ID="hdfTenDN" runat="server" />
        </tr>
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%" class="form_input">
                 <tr align="left">
                        <td class="form_label" style="width:15%">
                            <asp:Label ID="lblUserNameCaption" runat="server" CssClass="label">Mã phòng GD</asp:Label>
                        </td>
                        <td class="form_control"  style="width:35%">
                            <asp:TextBox ID="txtMA_PGD" runat="server" MaxLength="6" Width="90%"  CssClass="inputflat"></asp:TextBox>
                        </td>
                        <td class="form_label"  style="width:15%">
                            <asp:Label ID="Label1" runat="server" CssClass="label">Tên phòng GD</asp:Label>
                        </td>
                        <td class="form_control" >
                            <asp:TextBox ID="txtTEN_PGD" runat="server" MaxLength="100" Width="90%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                   
                <tr align="left">
                        <td class="form_label" >
                            <asp:Label ID="Label2" runat="server" CssClass="label">Mã chi nhánh</asp:Label>
                        </td>
                        <td class="form_control" >
                            <asp:TextBox ID="txtMa_CN"  runat="server" MaxLength="6" Width="90%" CssClass="inputflat" style="background: Aqua" onkeypress="if (event.keyCode==13){ShowLov1('CNNganHang');}" ></asp:TextBox>
                        </td>
                        <td class="form_label" >
                            <asp:Label ID="Label5" runat="server" CssClass="label">Mã ĐBHC</asp:Label>
                        </td>
                        <td class="form_control" >
                            <asp:TextBox ID="txtMA_XA" runat="server" MaxLength="5"  CssClass="inputflat"  Width="90%" style="background: Aqua"
                             onkeypress="if (event.keyCode==13){ShowLov('DBHC');}"></asp:TextBox>
                        </td>
                        
                    </tr>
                    <tr align="left">
                        <td  class="form_label">
                            <asp:Label ID="Label4" runat="server" CssClass="label">Phân cấp</asp:Label>
                        </td>
                        <td class="form_control"  >
                        <asp:DropDownList ID="cbo_PhanCap" runat="server" CssClass="inputflat" Width="91%">
                        <asp:ListItem Text="Tất cả" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Hội sở chính" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Chi nhánh" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Phòng giao dịch" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        </td> 
                        <td  class="form_label" >
                            <asp:Label ID="Label3" runat="server" CssClass="label">Tình trạng</asp:Label>
                        </td>
                        <td class="form_control" >
                        <asp:DropDownList ID="cbo_TinhTrang" runat="server" CssClass="inputflat" Width="91%">
                         <asp:ListItem Text="Tất cả" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Hoạt động" Value="1" ></asp:ListItem>
                        <asp:ListItem Text="Không doạt động" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        </td> 
                    </tr>
                   <tr align="left" id="Tr1" runat="server">
                        <td class="form_label">
                            <asp:Label ID="Label6" CssClass="label" runat="server">Thông tin dữ liệu</asp:Label>
                        </td>
                        <td class="form_control">
                           <asp:DropDownList ID="drpDuLieu" runat="server" CssClass="inputflat" Width="91%">
                                <asp:ListItem Text="Dữ liệu hiện tại" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Dữ liệu thay đổi(thêm/sửa/xóa)" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="form_label">
                            <asp:Label ID="Label7" CssClass="label" runat="server">Trạng thái xử lý</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID="drpTT_XU_LY" runat="server" CssClass="inputflat" Width="91%">
                                <asp:ListItem Text="Tất cả" Value=""></asp:ListItem>
                                <asp:ListItem Text="Chờ duyệt" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Đã duyệt" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Từ chối" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                     <tr align="left">
                        <td class="form_label" style="width:15%">
                            <asp:Label ID="Label8" runat="server" CssClass="label">Số TK trung gian thu NSNN bằng tiền mặt</asp:Label>
                        </td>
                        <td class="form_control"  style="width:35%">
                            <asp:TextBox ID="txtTaiKhoan_tienmat" runat="server" MaxLength="50" Width="90%"  CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdSearch" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdTaoMoi" runat="server" CssClass="ButtonCommand" Text="Ghi"  UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdDeleteUser" runat="server" CssClass="ButtonCommand" Text="Xóa" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="btnDuyet" runat="server" CssClass="ButtonCommand" Text="Duyệt" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="btnTuChoi" runat="server" CssClass="ButtonCommand" Text="Từ chối" UseSubmitBehavior="false"></asp:Button>

        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="id" HeaderText="id" Visible="false">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Mã PGD">
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ID") %>'
                                    NavigateUrl='<%#"~/pages/HeThong/DanhMuc/frmDMChiNhanh_NganHang.aspx?id=" & DataBinder.Eval(Container.DataItem, "id") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="NAME" HeaderText="Tên PGD">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="BRANCH_code" HeaderText="Mã CN">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_xa" HeaderText="Mã ĐBHC">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TINHTRANG" HeaderText="Tình trạng">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                         <asp:BoundColumn DataField="PHANCAP" HeaderText="Phân cấp">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TAIKHOAN_TIENMAT" HeaderText="Số TK trung gian thu NSNN bằng tiền mặt">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
                <asp:DataGrid ID="grd_new" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="HIS_ID" HeaderText="HIS_ID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="type" HeaderText="type" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ThayDoi" HeaderText="Thay đổi" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ID_new" HeaderText="Mã PGD mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ten_new" HeaderText="Tên PGD mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="BRANCH_ID_new" HeaderText="Mã chi nhánh mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_xa_new" HeaderText="Mã ĐBHC mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tinh_trang_new" HeaderText="Tình trạng mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="PHAN_CAP_new" HeaderText="Phân cấp mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ID" HeaderText="Mã PGD hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="name" HeaderText="Tên PGD hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="BRANCH_ID" HeaderText="Mã chi nhánh hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_xa" HeaderText="Mã ĐBHC hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tinh_trang" HeaderText="Tình trạng hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="PHAN_CAP" HeaderText="Phân cấp hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tt_xu_ly" HeaderText="Trạng thái xử lý" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="xu_ly" HeaderText="Trạng thái xử lý"></asp:BoundColumn>
                        <asp:BoundColumn DataField="taikhoan_tienmat" HeaderText="Tài khoản"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>