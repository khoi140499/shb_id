﻿Imports Business.Common.mdlCommon
Imports Business.DanhMuc.TieuMuc
Imports System.Data
Imports VBOracleLib
Imports ProcMegService
Imports System.Diagnostics


Partial Class pages_HeThong_DanhMuc_frmDMMucTieuMuc
    Inherits System.Web.UI.Page
    Dim objTIEUMUC As New buTieuMuc()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            hdfTenDN.Value = Session.Item("UserName")
            EnableButton_Nhom(Session.Item("MA_NHOM"))
            'nhóm thay đổi thông tin danh mục
            If Not Request.Params("id") Is Nothing Then
                txtMaTMuc.Enabled = False
                load_Detail(Request.Params("id"))
            End If
        End If
        'cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn xóa dữ liệu đã chọn?') ")
    End Sub
    Protected Sub EnableButton_Nhom(ByVal Nhom As String)
        Nhom = clsCommon.Check_Maker_Checker(Nhom)
        hdfMa_Nhom.Value = Nhom
        If Nhom = "10" Then
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = True
            'cmdIn.Visible = True
            cmdDeleteUser.Visible = True
            cmdTaoMoi.Visible = True
            cmdTruyvanThue.Visible = True
            drpDuLieu.SelectedIndex = 0
            load_DataGrid()
            cmdTaoMoi.Enabled = True
        ElseIf Nhom = "11" Then
            btnDuyet.Visible = True
            btnTuChoi.Visible = True
            'rowTrangThai.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            cmdTruyvanThue.Visible = False
            drpDuLieu.SelectedIndex = 1
            load_DataGrid_new()
        Else
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            cmdTruyvanThue.Visible = False
            drpDuLieu.SelectedIndex = 0
            load_DataGrid()
        End If
    End Sub
    Private Sub load_DataGrid()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtMaTMuc.Enabled Then
                If txtMaTMuc.Text <> "" Then
                    WhereClause += " AND upper(ma_tmuc) like upper('" + txtMaTMuc.Text.Trim + "%')"
                End If
                If txtTenTMuc.Text <> "" Then
                    WhereClause += " AND upper(ten) like upper('%" + txtTenTMuc.Text.Trim + "%')"
                End If
                If txtGhiChu.Text <> "" Then
                    WhereClause += " AND upper(ghi_chu) like upper('%" + txtGhiChu.Text.Trim + "%')"
                End If
                If cboTinh_trang.SelectedItem.ToString() <> "" Then
                    WhereClause += " AND tinh_trang = '" + cboTinh_trang.SelectedValue.ToString() + "'"
                End If
            End If

            ds = objTIEUMUC.Load(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgdata.DataSource = ds.Tables(0)
                dtgdata.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub
    Private Sub load_DataGrid_new()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtMaTMuc.Enabled Then
                If txtMaTMuc.Text <> "" Then
                    WhereClause += " AND upper(b.ma_tmuc) like upper('" + txtMaTMuc.Text.Trim + "%')"
                End If
                If txtTenTMuc.Text <> "" Then
                    WhereClause += " AND upper(b.ten) like upper ('%" + txtTenTMuc.Text.Trim + "%')"
                End If
                If txtGhiChu.Text <> "" Then
                    WhereClause += " AND upper(b.ghi_chu) like upper('%" + txtGhiChu.Text.Trim + "%')"
                End If
                If drpTT_XU_LY.SelectedValue.ToString() <> "" Then
                    WhereClause += " AND b.TT_XU_LY = '" + drpTT_XU_LY.SelectedValue.ToString() + "'"
                End If
            End If

            ds = objTIEUMUC.GetDataSet_new(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                grd_new.DataSource = ds.Tables(0)
                grd_new.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub

    Private Sub load_Detail(ByVal id As String)
        Dim ds As DataSet
        ds = objTIEUMUC.Load(" AND mtm_id = '" + id + "'")
        If ds.Tables(0).Rows.Count > 0 Then
            txtMaTMuc.Text = ds.Tables(0).Rows(0)("ma_tmuc").ToString
            txtTenTMuc.Text = ds.Tables(0).Rows(0)("ten").ToString
            txtGhiChu.Text = ds.Tables(0).Rows(0)("ghi_chu").ToString
            If ds.Tables(0).Rows(0)("tinh_trang").ToString = "Đang hoạt động" Then
                cboTinh_trang.SelectedValue = 1
            Else
                cboTinh_trang.SelectedValue = 0
            End If
            txtSacThue.Text = ds.Tables(0).Rows(0)("sac_thue").ToString
        End If
        'For i As Integer = 0 To dtgdata.Items.Count - 1
        '    If dtgdata.Items(i).Cells(0).Text = id Then
        '        txtMaTMuc.Text = dtgdata.Items(i).Cells(1).Text.ToString
        '        txtTenTMuc.Text = dtgdata.Items(i).Cells(3).Text.ToString
        '        txtGhiChu.Text = dtgdata.Items(i).Cells(4).Text.ToString.Trim
        '        cboTinh_trang.Text = dtgdata.Items(i).Cells(5).Text.ToString
        '    End If
        'Next
    End Sub
    Protected Sub Get_Data_Item(ByRef objItem As infTieuMuc)
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM HIS_DM_MUC_TMUC WHERE ID=" & objItem.ID
        dt = DataAccess.ExecuteToTable(WhereClause)
        objItem.Ten = dt.Rows(0)("ten").ToString
        objItem.TINH_TRANG = dt.Rows(0)("tinh_trang").ToString
        objItem.GHICHU = dt.Rows(0)("ghi_chu").ToString
        objItem.MTM_ID = dt.Rows(0)("MTM_ID").ToString
        objItem.MA_TMUC = dt.Rows(0)("MA_TMUC").ToString
        objItem.SAC_THUE = dt.Rows(0)("SAC_THUE").ToString
    End Sub
    'Kiểm tra có thay đổi thông tin đang chờ xử lý ko
    'Nếu có thay đổi thì trả về True/False
    Protected Function Check_ThayDoi(ByRef objItem As infTieuMuc) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM HIS_DM_MUC_TMUC WHERE MTM_ID=" & objItem.MTM_ID & " AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function

    Protected Function Check_Insert(ByRef strMaTmuc As String) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM HIS_DM_MUC_TMUC WHERE MA_TMUC=" & strMaTmuc & " AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function
    Private Sub Xoa_Text()
        txtGhiChu.Text = ""
        txtMaTMuc.Text = ""
        txtTenTMuc.Text = ""
        txtMaTMuc.Enabled = True
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Xoa_Text()
        cmdTaoMoi.Enabled = True
        If Not Request.Params("id") Then
            Response.Redirect("~/pages/HeThong/DanhMuc/frmDMMucTieuMuc.aspx")
        End If
    End Sub

    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        txtMaTMuc.Text = RemoveApostrophe(txtMaTMuc.Text)
        txtGhiChu.Text = RemoveApostrophe(txtGhiChu.Text)
        If Request.Params("id") Is Nothing Then
            If txtMaTMuc.Text.ToString.Trim = "" Then
                clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã tiểu mục.")
                txtMaTMuc.Focus()
                Exit Sub
            ElseIf txtMaTMuc.Text.Trim.Length <> txtMaTMuc.MaxLength Then
                clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã tiểu mục có độ dài " & txtMaTMuc.MaxLength & " ký tự.")
                txtMaTMuc.Focus()
                Exit Sub
                '*****TYNK - Check ky tu dac biet*********19/02/2013
            Else
                If Regex.IsMatch(txtMaTMuc.Text.Trim, "^[a-zA-Z0-9]*$") = False Then
                    clsCommon.ShowMessageBox(Me, "Chuỗi bạn nhập không đúng - Có ký tự đặc biệt.")
                    txtMaTMuc.Focus()
                    Exit Sub
                End If
                '***************************************************
            End If
        End If

        If txtTenTMuc.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên mã tiểu mục.")
            txtTenTMuc.Focus()
            Exit Sub
        End If

        Try
            Dim objInfTIEUMUC As New infTieuMuc

            objInfTIEUMUC.MA_TMUC = txtMaTMuc.Text.ToString.Trim
            objInfTIEUMUC.Ten = txtTenTMuc.Text.ToString.Trim
            objInfTIEUMUC.GHICHU = txtGhiChu.Text.ToString.Trim
            objInfTIEUMUC.SAC_THUE = txtSacThue.Text.ToString.Trim
            objInfTIEUMUC.TINH_TRANG = (1 - cboTinh_trang.SelectedIndex).ToString
            objInfTIEUMUC.NGUOI_TAO = hdfTenDN.Value

            If Not Request.Params("id") Is Nothing Then
                objInfTIEUMUC.MTM_ID = Request.Params("id").Trim
                objInfTIEUMUC.TYPE = "2" 'UPDATE
                If Not Check_ThayDoi(objInfTIEUMUC) Then
                    If objTIEUMUC.Update(objInfTIEUMUC, "10") Then
                        Xoa_Text()
                        clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                    Else
                        txtMaTMuc.Focus()
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                End If
            Else
                objInfTIEUMUC.TYPE = "1" 'INSERT
                objInfTIEUMUC.MTM_ID = getDataKey("TCS_MTM_ID_SEQ.NEXTVAL")
                If Not Check_Insert(objInfTIEUMUC.MA_TMUC) Then
                    If objTIEUMUC.Insert(objInfTIEUMUC, "10") Then
                        Xoa_Text()
                        clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                    Else
                        txtMaTMuc.Focus()
                        clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Thông tin mã tiểu mục này đã tồn tại và đang chờ xử lý!")
                End If

            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
        Finally
        End Try
        load_DataGrid()
    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim i As Integer
        Dim strMaTmuc As String
        Dim strMa_MTM_ID As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True
        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strMa_MTM_ID = Trim(dtgdata.Items(i).Cells(0).Text)
                strMaTmuc = Trim(dtgdata.Items(i).Cells(1).Text)
                If objTIEUMUC.CheckDelete(strMaTmuc) Then
                    clsCommon.ShowMessageBox(Me, "Dữ liệu đã sử dụng trong chức năng khác, không được xoá!")
                Else
                    Dim objMLNS As New infTieuMuc
                    objMLNS.MTM_ID = strMa_MTM_ID
                    objMLNS.TYPE = "3" 'xóa dữ liệu
                    objMLNS.NGUOI_TAO = hdfTenDN.Value
                    objMLNS.MA_TMUC = strMaTmuc
                    If Not Check_ThayDoi(objMLNS) Then
                        If objTIEUMUC.Delete(objMLNS, "10") Then
                            clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                        Else
                            clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình xóa mã tiểu mục!")
                        End If
                    Else
                        clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                    End If
                End If
                v_count += 1
            End If
        Next
        load_DataGrid()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub

    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Try
            Dim frm As New Business.BaoCao.infBaoCao
            'Business.Common.mdlSystemVariables.gLoaiBaoCao = Report_Type.DM_MUCTMUC
            Session("objBaoCao") = frm
            clsCommon.OpenNewWindow(Me, "../../BaoCao/frmShowReports.aspx&BC=" & Report_Type.DM_MUCTMUC & "", "ManPower")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        load_DataGrid()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        If drpDuLieu.SelectedIndex = 0 Then
            load_DataGrid()
            dtgdata.CurrentPageIndex = 0
            dtgdata.DataBind()
            grd_new.Visible = False
            dtgdata.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = False
                btnTuChoi.Visible = False
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = True
                cmdDeleteUser.Visible = True
                cmdTaoMoi.Visible = True
                cmdTruyvanThue.Visible = True
            End If
        Else
            load_DataGrid_new()
            grd_new.CurrentPageIndex = 0
            grd_new.DataBind()
            dtgdata.Visible = False
            grd_new.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = True
                btnTuChoi.Visible = True
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = False
                cmdDeleteUser.Visible = False
                cmdTaoMoi.Visible = False
                cmdTruyvanThue.Visible = False
            End If
        End If
        cmdTaoMoi.Enabled = False
    End Sub
    Protected Sub cmdTruyvanThue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTruyvanThue.Click
        'thêm aleert confirm trước khi làm
        Try
            ProcMegService.ProMsg.getMSG07(MessageType.strMSG19, "Trao đổi danh mục Tiểu Mục")
            load_DataGrid()
            lblError.Text = "Đồng bộ danh mục thành công"
        Catch ex As Exception
            lblError.Text = ex.Message.ToString()
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Lỗi trong quá trình đồng bộ dữ liệu Danh mục Nội dung kinh tế từ TC Thuế : Mô tả=" & ex.Message.ToString())
        End Try
    End Sub

    Protected Sub btnDuyet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuyet.Click
        Dim i As Integer
        Dim strID As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                Dim objMLNS = New infTieuMuc
                objMLNS.ID = strID
                objMLNS.NGUOI_DUYET = Session.Item("UserName")
                objMLNS.TT_XU_LY = "1"
                Dim objInsert As New buTieuMuc
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        load_dataGrid_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Duyệt thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi duyệt!")
        End If
    End Sub

    Protected Sub btnTuChoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTuChoi.Click
        Dim i As Integer
        Dim strID As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                Dim objMLNS = New infTieuMuc
                objMLNS.ID = strID
                objMLNS.NGUOI_DUYET = Session.Item("UserName")
                objMLNS.TT_XU_LY = "2"
                Dim objInsert As New buTieuMuc
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        load_DataGrid_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Từ chối thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi duyệt xóa!")
        End If
    End Sub

    Protected Sub grd_new_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_new.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value
        If MaNhom = "10" Then
            e.Item.Cells(15).Visible = False
        ElseIf MaNhom = "11" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim tt_xu_ly As String = e.Item.Cells(13).Text.ToString
                If tt_xu_ly <> "0" Then
                    Dim checkBox As CheckBox = DirectCast(e.Item.FindControl("chkSelect"), CheckBox)
                    checkBox.Visible = False
                End If
            End If
        Else
            e.Item.Cells(13).Visible = False
        End If
    End Sub

    Protected Sub grd_new_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grd_new.PageIndexChanged
        load_DataGrid_new()
        grd_new.CurrentPageIndex = e.NewPageIndex
        grd_new.DataBind()
    End Sub

    Protected Sub dtgdata_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgdata.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If Not MaNhom = "10" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim checkBox As HyperLink = DirectCast(e.Item.FindControl("Hyperlink1"), HyperLink)
                checkBox.Enabled = False
            End If
        End If
    End Sub
End Class
