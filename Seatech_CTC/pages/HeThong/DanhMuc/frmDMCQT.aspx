﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmDMCQT.aspx.vb" Inherits="pages_HeThong_DanhMuc_frmDMCQT" Title="Danh mục cơ quan thu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<script type="text/javascript" language="javascript">
    function ShowLov(strType)
    {
        if (strType=="DBHCTINH") return FindDanhMuc('DBHCTINH','<%=txtMaTinh.ClientID %>','<%=txtTenTinh.ClientID %>');         
        if (strType=="DBHCHUYEN") return FindDanhMuc('DBHCHUYEN','<%=txtMaDBHC.ClientID %>','<%=txtTenDBHC.ClientID %>');        
                          
    }
    function FindDanhMuc(strPage,txtID, txtTitle) 
    {        
        var returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&initParam=" + document.getElementById(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;","_new");
        if (returnValue !== null)
        {        
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;  
         }
    }
     function jsGet_TenKB(){
        if (document.getElementById ('<%= txtMaTinh.ClientID %>').value != ''){
            PageMethods.GetTenTinh($get('<%= txtMaTinh.ClientID %>').value,jsGet_TenKB_Success,jsGet_TenKB_Error);
        }
    }
    
    function jsGet_TenKB_Success(result){
        var url=result;
        if (url.length > 0){
            document.getElementById ('<%= txtTenTinh.ClientID %>').value = url ;
        }
        else {
            alert('Không tìm được chi nhánh!');
        }
        
    }
    function jsGet_TenKB_Error(result){
       //alert('Xảy ra lỗi trong quá trình lấy thông tin Chi nhánh.' );
    }
    
    //Ten DBHC
    function jsGet_TenDBHC(){
        if (document.getElementById ('<%= txtMaDBHC.ClientID %>').value != ''){
            PageMethods.GetTenDBHC($get('<%= txtMaDBHC.ClientID %>').value,jsTenDBHC_Success,jsTenDBHC_Error);
        }
    }
    
    function jsTenDBHC_Success(result){
        var url=result;
        if (url.length > 0){
            document.getElementById ('<%= txtTenDBHC.ClientID %>').value = url ;
        }
        else {
            alert('Không tìm được chi nhánh!');
        }
        
    }
    function jsTenDBHC_Error(result){
       //alert('Xảy ra lỗi trong quá trình lấy thông tin Chi nhánh.' );
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">DANH MỤC CƠ QUAN THU</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <asp:HiddenField ID="hdfMa_Nhom" runat="server" />
            <asp:HiddenField ID="hdfTenDN" runat="server" />
        </tr>
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%"
                    class="form_input">
                    <tr align="left">
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label3" runat="server" CssClass="label">Mã cơ quan thu</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMaCQT" runat="server" MaxLength="10" Width="71%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblUserNameCaption" runat="server" CssClass="label">Tên cơ quan thu</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtTenCQT" runat="server" MaxLength="50" Width="71%" CssClass="inputflat"
                                TabIndex="2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblPasswordCaption" CssClass="label" runat="server">Thuộc điểm thu</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID="cboDiemThu" runat="server" CssClass="inputflat" Width="40%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label1" CssClass="label" runat="server">Mã HQ</asp:Label>
                        </td>
                        <td class="form_control">
                             <asp:TextBox ID="txtMaHQ" runat="server" MaxLength="10" Width="71%" CssClass="inputflat"
                                TabIndex="2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label2" CssClass="label" runat="server">Mã QLT</asp:Label>
                        </td>
                        <td class="form_control">
                             <asp:TextBox ID="txtMaQLT" runat="server" MaxLength="5" Width="71%" CssClass="inputflat"
                                TabIndex="2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblMaTinh" runat="server" CssClass="label">Mã tỉnh</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMaTinh" runat="server" MaxLength="5" Width="20%" CssClass="inputflat" style="background: Aqua" 
                                 TabIndex="2" onkeypress="if (event.keyCode==13){ShowLov('DBHCTINH');}" onblur="jsGet_TenKB();"></asp:TextBox>
                            <asp:TextBox ID="txtTenTinh" runat="server" MaxLength="500" Width="50%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblDB" runat="server" CssClass="label">Mã huyện</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMaDBHC" runat="server" MaxLength="5" Width="20%" CssClass="inputflat" style="background: Aqua" 
                                TabIndex="2" onkeypress="if (event.keyCode==13){ShowLov('DBHCHUYEN');}" onblur="jsGet_TenDBHC();"></asp:TextBox>
                            <asp:TextBox ID="txtTenDBHC" runat="server" MaxLength="500" Width="50%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left" id="Tr1" runat="server">
                        <td class="form_label">
                            <asp:Label ID="Label4" CssClass="label" runat="server">Thông tin dữ liệu</asp:Label>
                        </td>
                        <td class="form_control">
                           <asp:DropDownList ID="drpDuLieu" runat="server" CssClass="inputflat" Width="40%">
                                <asp:ListItem Text="Dữ liệu hiện tại" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Dữ liệu thay đổi(thêm/sửa/xóa)" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="left" id="Tr2" runat="server">
                        <td class="form_label">
                            <asp:Label ID="Label5" CssClass="label" runat="server">Trạng thái xử lý</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID="drpTT_XU_LY" runat="server" CssClass="inputflat" Width="40%">
                                <asp:ListItem Text="Tất cả" Value=""></asp:ListItem>
                                <asp:ListItem Text="Chờ duyệt" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Đã duyệt" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Từ chối" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdSearch" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdTaoMoi" runat="server" CssClass="ButtonCommand" Text="Ghi" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdDeleteUser" runat="server" CssClass="ButtonCommand" Text="Xóa" UseSubmitBehavior="false">
                </asp:Button>
                 <asp:Button ID="cmdTruyvanThue" runat="server" CssClass="ButtonCommand" Text="Đồng bộ DM từ TCT" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="btnDuyet" runat="server" CssClass="ButtonCommand" Text="Duyệt" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="btnTuChoi" runat="server" CssClass="ButtonCommand" Text="Từ chối" UseSubmitBehavior="false"></asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="MA_CQTHU" HeaderText="Mã CQ thu" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Mã CQ thu" HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MA_CQTHU") %>'
                                    NavigateUrl='<%#"~/pages/HeThong/DanhMuc/frmDMCQT.aspx?ID=" & DataBinder.Eval(Container.DataItem, "MA_CQTHU") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="Ten" HeaderText="Tên CQ thu">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TEN_DIEMTHU" HeaderText="Tên điểm thu">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_dthu" HeaderText="Mã điểm thu" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_hq" HeaderText="Mã HQ">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_qlt" HeaderText="Mã QLT">
                            <ItemStyle HorizontalAlign="Left" ></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="dbhc_tinh" HeaderText="Mã tỉnh">
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ten_tinh" HeaderText="Tên tỉnh"></asp:BoundColumn>
                        <asp:BoundColumn DataField="dbhc_huyen" HeaderText="Mã huyện">
                            <ItemStyle HorizontalAlign="Left" ></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ten_huyen" HeaderText="Tên huyện"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
                <asp:DataGrid ID="grd_new" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="id" HeaderText="ID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="type" HeaderText="type" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ThayDoi" HeaderText="Thay đổi" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_cqthu" HeaderText="Mã CQThu"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ten" HeaderText="Tên CQThu"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_dthu" HeaderText="Mã điểm thu"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_HQ" HeaderText="Mã HQ"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_QLT" HeaderText="Mã QLT"></asp:BoundColumn>
                        <asp:BoundColumn DataField="dbhc_tinh" HeaderText="Mã tỉnh"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ten_tinh" HeaderText="Tên tỉnh"></asp:BoundColumn>
                        <asp:BoundColumn DataField="dbhc_huyen" HeaderText="Mã huyện"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ten_huyen" HeaderText="Tên huyện"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tt_xu_ly" HeaderText="Trạng thái xử lý" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="xu_ly" HeaderText="Trạng thái xử lý"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>
