﻿Imports Business.Common.mdlCommon
Imports Business.DanhMuc
Imports System.Data
Imports VBOracleLib
Imports ProcMegService
Imports System.Diagnostics

Partial Class pages_HeThong_DanhMuc_frmDMMapCQQD_LHThu
    Inherits System.Web.UI.Page
    Private objCQQD As New CQQD_LHThu.daCQQD_LHThu
    Dim MaTK As String
    Dim TenTK As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            EnableButton_Nhom(Session.Item("MA_NHOM"))
            hdfTenDN.Value = Session.Item("UserName")
            cmdTaoMoi.Enabled = True
            If Not Request.Params("id") Is Nothing Then
                'txtSHKB.Enabled = False
                load_Detail(Request.Params("id").Trim)
            End If
            'cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn thực hiện không?') ")
        End If
    End Sub
    Protected Sub EnableButton_Nhom(ByVal Nhom As String)
        Nhom = clsCommon.Check_Maker_Checker(Nhom)
        hdfMa_Nhom.Value = Nhom
        If Nhom = "10" Then
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = True
            'cmdIn.Visible = True
            cmdDeleteUser.Visible = True
            cmdTaoMoi.Visible = True
            drpDuLieu.SelectedIndex = 0
            load_Datagrid()
            cmdTaoMoi.Enabled = True
        ElseIf Nhom = "11" Then
            btnDuyet.Visible = True
            btnTuChoi.Visible = True
            'rowTrangThai.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            drpDuLieu.SelectedIndex = 1
            load_Datagrid_new()
        Else
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            drpDuLieu.SelectedIndex = 0
            load_Datagrid()
        End If
    End Sub
    Private Sub load_Datagrid()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtSHKB.Text <> "" Then
                WhereClause += " AND upper(SHKB) like upper('%" + txtSHKB.Text.Trim + "%')"
            End If
            If txtCQQD.Text <> "" Then
                WhereClause += " AND upper(ma_cqqd) like upper('%" + txtCQQD.Text.Trim + "%')"
            End If
            If txtMa_LH.Text <> "" Then
                WhereClause += " AND upper(ma_lh) like upper('%" + txtMa_LH.Text.Trim + "%')"
            End If
            If txtMaDBHC.Text <> "" Then
                WhereClause += " AND upper(ma_dbhc) like upper('%" + txtMaDBHC.Text.Trim + "%')"
            End If
            If cboTinh_trang.SelectedValue <> "" Then
                WhereClause += " AND tinh_trang = '" + cboTinh_trang.SelectedValue + "'"
            End If

            ds = objCQQD.Load(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgdata.DataSource = ds.Tables(0)
                dtgdata.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub
    Private Sub load_Datagrid_new()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try

            If txtSHKB.Text <> "" Then
                WhereClause += " AND upper(SHKB) like upper('%" + txtSHKB.Text.Trim + "%')"
            End If
            If txtCQQD.Text <> "" Then
                WhereClause += " AND upper(ma_cqqd) like upper('%" + txtCQQD.Text.Trim + "%')"
            End If
            If txtMa_LH.Text <> "" Then
                WhereClause += " AND upper(ma_lh) like upper('%" + txtMa_LH.Text.Trim + "%')"
            End If
            If txtMaDBHC.Text <> "" Then
                WhereClause += " AND upper(ma_dbhc) like upper('%" + txtMaDBHC.Text.Trim + "%')"
            End If
            If cboTinh_trang.SelectedValue <> "" Then
                WhereClause += " AND tinh_trang = '" + cboTinh_trang.SelectedValue + "'"
            End If


            ds = objCQQD.Load_new(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                grd_new.DataSource = ds.Tables(0)
                grd_new.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub
    Protected Sub Get_Data_Item(ByRef objItem As CQQD_LHThu.infCQQD_LHThu)
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM tcs_map_cqqd_lhthu_tmp WHERE ID=" & objItem.ID
        dt = DataAccess.ExecuteToTable(WhereClause)
        'ma_quy,tk_ns,ma_chuong,ma_nkt,ma_ndkt,ma_dbhc,khtk,ma_lh,shkb,ma_cqqd,ma_dvsdns,ten_cq,tinh_trang,id_his
        objItem.MA_QUY = dt.Rows(0)("ma_quy").ToString
        objItem.TK_NS = dt.Rows(0)("tk_ns").ToString
        objItem.MA_CHUONG = dt.Rows(0)("ma_chuong").ToString
        objItem.MA_NKT = dt.Rows(0)("ma_nkt").ToString
        objItem.MA_NDKT = dt.Rows(0)("ma_ndkt").ToString
        objItem.MA_DBHC = dt.Rows(0)("ma_dbhc").ToString
        objItem.KHTK = dt.Rows(0)("khtk").ToString
        objItem.MA_LH = dt.Rows(0)("ma_lh").ToString
        objItem.SHKB = dt.Rows(0)("shkb").ToString
        objItem.SHKB = dt.Rows(0)("SHKB").ToString
        objItem.MA_CQQD = dt.Rows(0)("ma_cqqd").ToString
        objItem.MA_DVSDNS = dt.Rows(0)("ma_dvsdns").ToString
        objItem.TEN_CQ = dt.Rows(0)("ten_cq").ToString
        objItem.ID_HIS = dt.Rows(0)("ID_HIS").ToString
        objItem.TINH_TRANG = dt.Rows(0)("tinh_trang").ToString

    End Sub
    'Kiểm tra có thay đổi thông tin đang chờ xử lý ko
    'Nếu có thay đổi thì trả về True/False
    Protected Function Check_ThayDoi(ByRef objItem As CQQD_LHThu.infCQQD_LHThu) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM tcs_map_cqqd_lhthu_tmp WHERE ID_HIS=" & objItem.ID_HIS & " AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function
    Protected Function Check_Insert(ByRef strID As String) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM tcs_map_cqqd_lhthu_tmp WHERE ID_HIS='" & strID & "' AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function
    Private Sub load_Detail(ByVal ID As String)
        Dim ds As DataSet
        ds = objCQQD.Load(" AND ID = '" + ID + "'")
        If ds.Tables(0).Rows.Count > 0 Then
            'ma_quy,tk_ns,ma_chuong,ma_nkt,ma_ndkt,ma_dbhc,khtk,ma_lh,shkb,ma_cqqd,ma_dvsdns,ten_cq
            txtMaQuy.Text = ds.Tables(0).Rows(0)("ma_quy").ToString
            txtTK_NS.Text = ds.Tables(0).Rows(0)("tk_ns").ToString
            txtMaChuong.Text = ds.Tables(0).Rows(0)("ma_chuong").ToString
            txtMa_NKT.Text = ds.Tables(0).Rows(0)("ma_nkt").ToString
            txtMa_NDKT.Text = ds.Tables(0).Rows(0)("ma_ndkt").ToString
            txtMaDBHC.Text = ds.Tables(0).Rows(0)("ma_dbhc").ToString
            txtKHTK.Text = ds.Tables(0).Rows(0)("khtk").ToString
            txtMa_LH.Text = ds.Tables(0).Rows(0)("ma_lh").ToString
            txtSHKB.Text = ds.Tables(0).Rows(0)("shkb").ToString
            txtCQQD.Text = ds.Tables(0).Rows(0)("ma_cqqd").ToString
            txtMa_DVSDNS.Text = ds.Tables(0).Rows(0)("ma_dvsdns").ToString
            txtTenCQ.Text = ds.Tables(0).Rows(0)("ten_cq").ToString
            txtID_HIS.Text = ID
            cboTinh_trang.SelectedValue = ds.Tables(0).Rows(0)("tinh_trang").ToString
         
            'Else
            ' cboTinh_trang.SelectedValue = 1
            'End If
        End If
    End Sub
  
   
    Private Sub Xoa_text()
        txtCQQD.Text = ""
        txtSHKB.Text = ""
        txtMa_LH.Text = ""
        txtMaQuy.Text = ""
        txtTK_NS.Text = ""
        txtMaChuong.Text = ""
        txtMa_NDKT.Text = ""
        txtMa_NKT.Text = ""
        txtMaDBHC.Text = ""
        txtKHTK.Text = ""
        txtMa_DVSDNS.Text = ""
        txtTenCQ.Text = ""
        txtID_HIS.Text = ""
        cboTinh_trang.SelectedValue = ""
        txtSHKB.Enabled = True
    End Sub


    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Xoa_text()
        If Not Request.Params("id") Then
            Response.Redirect("~/pages/HeThong/DanhMuc/frmDMMapCQQD_LHThu.aspx")
        End If
    End Sub

    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        txtSHKB.Text = RemoveApostrophe(txtSHKB.Text)
        If txtSHKB.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào số hiệu kho bạc.")
            txtSHKB.Focus()
            Exit Sub
            '*****TYNK - Check độ dài mã SHKB*********25/02/2013
        ElseIf txtSHKB.Text.Trim.Length <> txtSHKB.MaxLength Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã Kho bạc có độ dài " & txtSHKB.MaxLength & " ký tự.")
            txtSHKB.Focus()
            Exit Sub
            '***************************************************
            '*****TYNK - Check ky tu dac biet*********19/02/2013
        Else
            If Regex.IsMatch(txtSHKB.Text.Trim, "^[a-zA-Z0-9]*$") = False Then
                clsCommon.ShowMessageBox(Me, "Chuỗi bạn nhập không đúng - Có ký tự đặc biệt.")
                txtSHKB.Focus()
                Exit Sub
            End If
            '***************************************************
        End If

        If txtCQQD.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã cơ quan quyết định")
            txtCQQD.Focus()
            Exit Sub
        End If
      
        If txtMa_LH.Text.ToString() = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã loại hình.")
            txtMa_LH.Focus()
            Exit Sub
        End If
        If txtTK_NS.Text.ToString() = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tài khoản thu ngân sách.")
            txtTK_NS.Focus()
            Exit Sub
        End If
        If txtMaQuy.Text.ToString() = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã quỹ.")
            txtMaQuy.Focus()
            Exit Sub
        End If
        If txtMa_NDKT.Text.ToString() = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã nội dung kinh tế.")
            txtMa_NDKT.Focus()
            Exit Sub
        End If
        If txtMa_DVSDNS.Text.ToString() = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã đơn vị sử dụng ngân sách.")
            txtMa_DVSDNS.Focus()
            Exit Sub
        End If
     
        If txtMaDBHC.Text.ToString() = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã DBHC.")
            txtMaDBHC.Focus()
            Exit Sub
        End If

        If cboTinh_trang.SelectedValue = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn hãy chọn tình trạng hoạt động.")
            Exit Sub
        End If
        Dim infCQQD As New CQQD_LHThu.infCQQD_LHThu
        Try
            infCQQD.ID = txtID_HIS.Text.ToString.Trim
            infCQQD.ID_HIS = txtID_HIS.Text.ToString.Trim
            infCQQD.SHKB = txtSHKB.Text.ToString.Trim.Replace(".", "")
            infCQQD.MA_LH = txtMa_LH.Text.ToString
            infCQQD.MA_CQQD = txtCQQD.Text.ToString
            infCQQD.TK_NS = txtTK_NS.Text.ToString
            infCQQD.MA_QUY = txtMaQuy.Text.ToString
            infCQQD.MA_CHUONG = txtMaChuong.Text.ToString
            infCQQD.MA_NKT = txtMa_NKT.Text.ToString
            infCQQD.MA_NDKT = txtMa_NDKT.Text.ToString.Trim
            infCQQD.KHTK = txtKHTK.Text.ToString.Trim
            infCQQD.MA_DBHC = txtMaDBHC.Text.ToString.Trim
            infCQQD.MA_DVSDNS = txtMa_DVSDNS.Text.ToString.Trim
            infCQQD.TEN_CQ = txtTenCQ.Text.ToString.Trim
            infCQQD.NGUOI_TAO = hdfTenDN.Value

            infCQQD.TINH_TRANG = cboTinh_trang.SelectedValue.ToString

            If Not Request.Params("id") Is Nothing Then
                infCQQD.TYPE = "2" 'UPDATE
                If Not Check_ThayDoi(infCQQD) Then
                    If objCQQD.Update(infCQQD, "10") Then
                        Xoa_text()
                        clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                        load_Datagrid()
                    Else
                        clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                        txtSHKB.Focus()
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                End If
            Else
                If objCQQD.CheckExist(infCQQD) = True Then
                    clsCommon.ShowMessageBox(Me, "Thông tin ID đã tồn tại trong hệ thống!")
                Else
                    If Not Check_Insert(infCQQD.ID_HIS) Then
                        infCQQD.TYPE = "1" 'INSERT
                        If objCQQD.Insert(infCQQD, "10") Then
                            Xoa_text()
                            clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                            load_Datagrid()
                        Else
                            clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                            txtSHKB.Focus()
                        End If
                    Else
                        clsCommon.ShowMessageBox(Me, "Thông tin SHKB này đã tồn tại và đang chờ xử lý!")
                    End If
                End If
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình cập nhật dữ liệu")
        Finally
        End Try

    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim i As Integer
        Dim strID As String
        Dim ds As DataSet
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True
        Try
            For i = 0 To dtgdata.Items.Count - 1
                Dim chkSelect As CheckBox
                chkSelect = dtgdata.Items(i).FindControl("chkSelect")
                If chkSelect.Checked Then
                    strID = Trim(dtgdata.Items(i).Cells(0).Text)
                   
                    Dim objMLNS As New CQQD_LHThu.infCQQD_LHThu
                    objMLNS.ID = strID
                    objMLNS.ID_HIS = strID
                    objMLNS.TYPE = "3" 'xóa dữ liệu
                    objMLNS.NGUOI_TAO = hdfTenDN.Value

                    objMLNS.SHKB = Trim(dtgdata.Items(i).Cells(2).Text)
                    objMLNS.MA_LH = Trim(dtgdata.Items(i).Cells(4).Text)
                    objMLNS.MA_CQQD = Trim(dtgdata.Items(i).Cells(3).Text)
                    objMLNS.TK_NS = Trim(dtgdata.Items(i).Cells(6).Text)
                    objMLNS.MA_QUY = Trim(dtgdata.Items(i).Cells(5).Text)
                    objMLNS.MA_CHUONG = Trim(dtgdata.Items(i).Cells(7).Text)
                    objMLNS.MA_NKT = Trim(dtgdata.Items(i).Cells(8).Text)
                    objMLNS.MA_NDKT = Trim(dtgdata.Items(i).Cells(9).Text)
                    objMLNS.KHTK = Trim(dtgdata.Items(i).Cells(11).Text)
                    objMLNS.MA_DBHC = Trim(dtgdata.Items(i).Cells(10).Text)
                    objMLNS.MA_DVSDNS = Trim(dtgdata.Items(i).Cells(12).Text)
                    objMLNS.TEN_CQ = Trim(dtgdata.Items(i).Cells(13).Text)

                    'objMLNS.TINH_TRANG = cboTinh_trang.SelectedValue.ToString

                    If Not Check_ThayDoi(objMLNS) Then
                        If objCQQD.Delete(objMLNS, "10") Then
                            clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                            Xoa_text()
                            load_Datagrid()
                        End If
                    Else
                        clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                    End If

                    v_count += 1
                End If
            Next
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình xóa dữ liệu kho bạc!" & ex.ToString())
        End Try
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub

    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Try
            Dim frm As New Business.BaoCao.infBaoCao
            'Business.Common.mdlSystemVariables.gLoaiBaoCao = Report_Type.DM_TAIKHOAN
            Session("objBaoCao") = frm
            clsCommon.OpenNewWindow(Me, "../../BaoCao/frmShowReports.aspx&BC=" & Report_Type.DM_TAIKHOAN & "", "ManPower")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dtgdata_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgdata.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If Not MaNhom = "10" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim checkBox As HyperLink = DirectCast(e.Item.FindControl("Hyperlink1"), HyperLink)
                checkBox.Enabled = False
            End If
        End If
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        load_Datagrid()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        If drpDuLieu.SelectedIndex = 0 Then
            load_Datagrid()
            dtgdata.CurrentPageIndex = 0
            dtgdata.DataBind()
            grd_new.Visible = False
            dtgdata.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = False
                btnTuChoi.Visible = False
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = True
                'cmdIn.Visible = True
                cmdDeleteUser.Visible = True
                cmdTaoMoi.Visible = True
            End If
        Else
            load_Datagrid_new()
            grd_new.CurrentPageIndex = 0
            grd_new.DataBind()
            dtgdata.Visible = False
            grd_new.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = True
                btnTuChoi.Visible = True
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = False
                'cmdIn.Visible = False
                cmdDeleteUser.Visible = False
                cmdTaoMoi.Visible = False
            End If
        End If
        cmdTaoMoi.Enabled = False
    End Sub
  

    Protected Sub grd_new_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_new.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If MaNhom = "10" Then
            e.Item.Cells(20).Visible = False
        ElseIf MaNhom = "11" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim tt_xu_ly As String = e.Item.Cells(18).Text.ToString
                If tt_xu_ly <> "0" Then
                    Dim checkBox As CheckBox = DirectCast(e.Item.FindControl("chkSelect"), CheckBox)
                    checkBox.Visible = False
                End If
            End If
        Else
            e.Item.Cells(20).Visible = False
        End If
    End Sub

    Protected Sub grd_new_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grd_new.PageIndexChanged
        load_Datagrid_new()
        grd_new.CurrentPageIndex = e.NewPageIndex
        grd_new.DataBind()
    End Sub

    Protected Sub btnDuyet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuyet.Click
        Dim i As Integer
        Dim strID As String
        Dim strID_HIS As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strID_HIS = Trim(grd_new.Items(i).Cells(1).Text)
                strType = Trim(grd_new.Items(i).Cells(2).Text)
                Dim objMLNS = New CQQD_LHThu.infCQQD_LHThu
                objMLNS.ID = strID
                objMLNS.ID_HIS = strID_HIS
                objMLNS.NGUOI_DUYET = hdfTenDN.Value
                objMLNS.TT_XU_LY = "1"
                Dim objInsert As New CQQD_LHThu.daCQQD_LHThu
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        load_Datagrid_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Duyệt thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi duyệt!")
        End If
    End Sub

    Protected Sub btnTuChoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTuChoi.Click
        Dim i As Integer
        Dim strID As String
        Dim strID_HIS As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strID_HIS = Trim(grd_new.Items(i).Cells(1).Text)
                strType = Trim(grd_new.Items(i).Cells(2).Text)
                Dim objMLNS = New CQQD_LHThu.infCQQD_LHThu
                objMLNS.ID = strID
                objMLNS.ID_HIS = strID_HIS
                objMLNS.NGUOI_DUYET = hdfTenDN.Value
                objMLNS.TT_XU_LY = "2"
                Dim objInsert As New CQQD_LHThu.daCQQD_LHThu
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        load_Datagrid_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Từ chối thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi từ chối!")
        End If
    End Sub
End Class
