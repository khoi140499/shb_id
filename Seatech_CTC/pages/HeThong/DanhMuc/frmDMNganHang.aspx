﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false" CodeFile="frmDMNganHang.aspx.vb" Inherits="pages_HeThong_DanhMuc_frmDMNganHang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" Runat="Server">
<script language="javascript" type="text/javascript">
function ShowLov(strType)
    {
        if (strType=="DBHC") return FindDanhMuc('DBHC','<%=txtMA_XA.ClientID %>', '<%=txtTenDBHC.ClientID %>', '<%=txtMA_NH.ClientID %>'); 
        if (strType=="SHKB") return FindDanhMuc('KhoBac','<%=txtSHKB.ClientID %>', '<%=txtTenKB.ClientID%>','<%=txtSHKB.ClientID %>'); 
    }
    function FindDanhMuc(strPage,txtID, txtTitle,txtFocus) {
        var strSHKB;
        if (document.getElementById('<%=txtSHKB.ClientID %>').value.trim().length>0)
        {
                strSHKB=document.getElementById('<%=txtSHKB.ClientID %>').value;
             var returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&SHKB=" +strSHKB +"&initParam=" + $get(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;","_new");
        }
            else
        { 
            if (strPage=="DBHC")  strPage="DBHCAll";
            var returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&initParam=" + document.getElementById(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;","_new");
        }
        //alert('b');
       
        if (document.all) {
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                if (txtTitle!=null){
                    document.getElementById(txtTitle).value = returnValue.Title;                    
                }
                if (txtFocus!=null){
                    document.getElementById(txtFocus).focus();
                }
            }
        }
        //NextFocus();
    }
    
   // function FindDanhMuc(strPage,txtID,txtFocus) {
   //     var returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&initParam=" + document.getElementById(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;","_new");
   //     if (document.all) {
   //         if (returnValue != null) {
   //             document.getElementById(txtID).value = returnValue.ID;
    //            if (txtFocus!=null){
     //               document.getElementById(txtFocus).focus();
      //          }
       //     }
       // }
   // }
    function jsGet_TenKB()
        {
            var txt = document.getElementById('<%=txtSHKB.ClientID %>').value;
            //txt.value ='ok';
            PageMethods.GetTenKB(txt,get_TenKB_Complete,get_TenKB__Error)        
        }
    
    function get_TenKB_Complete(result, methodname)
    {
        var txt = document.getElementById('<%=txtTenKB.ClientID %>');
        if (result.length > 0 && result.toString() != 'Null'){            
            txt.value =result;
        }
        else
        {
            txt.value ='-Số hiệu kho bạc không đúng-';
        }
    }
    
    function get_TenKB__Error(error,context,methodname)
    {
        var txt = document.getElementById('<%=txtTenKB.ClientID %>');
        txt.value ='';
    }
</script>
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">DANH MỤC NGÂN HÀNG</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <input type="hidden" runat="server" id="Hidden1" />
        </tr>
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%" class="form_input">
                 <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblUserNameCaption" runat="server" CssClass="label">Mã ngân hàng</asp:Label>
                        </td>
                        <td class="form_control"  colspan="2">
                            <asp:TextBox ID="txtMA_NH" runat="server" MaxLength="10"  Width="148" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label1" runat="server" CssClass="label">Tên ngân hàng</asp:Label>
                        </td>
                        <td class="form_control" colspan="2">
                            <asp:TextBox ID="txtTEN_NH" runat="server" MaxLength="50" Width="350px" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label2" runat="server" CssClass="label">Số hiệu kho bạc</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtSHKB"  runat="server" MaxLength="20" style="background: Aqua" 
                             onblur='jsGet_TenKB();'  Width="100%" CssClass="inputflat"  onkeypress="if (event.keyCode==13){ShowLov('SHKB');}" ></asp:TextBox>
                        </td>
                        
                        <td class="form_control">
                            <asp:TextBox ID="txtTenKB" runat="server" CssClass="inputflat"  Enabled="false" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td style="width: 25%;" class="form_label">
                            <asp:Label ID="Label3" runat="server" CssClass="label">Mã địa bàn hành chính</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMA_XA" runat="server" MaxLength="5"  CssClass="inputflat"  Width="100%" style="background: Aqua"
                             onkeypress="if (event.keyCode==13){ShowLov('DBHC');}"></asp:TextBox>
                        </td>
                         <td class="form_control">
                            <asp:TextBox ID="txtTenDBHC" runat="server" CssClass="inputflat" Enabled="false" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                   
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdSearch" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm" UseSubmitBehavior="false">
                </asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới" UseSubmitBehavior="false">
                </asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdTaoMoi" runat="server" CssClass="ButtonCommand" Text="Ghi" TabIndex="5">
                </asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdDeleteUser" runat="server" CssClass="ButtonCommand" Text="Xóa">
                </asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdIn" Visible="false" Enabled="false" runat="server" CssClass="ButtonCommand" Text="In" UseSubmitBehavior="false" >
                </asp:Button>&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="id" HeaderText="id" Visible="false">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Mã ngân hàng">
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MA") %>'
                                    NavigateUrl='<%#"~/pages/HeThong/DanhMuc/frmDMNganHang.aspx?id=" & DataBinder.Eval(Container.DataItem, "id") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TEN" HeaderText="Tên ngân hàng">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_XA" HeaderText="Mã ĐBHC">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SHKB" HeaderText="SHKB">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
            </td>
        </tr>
    </table>
</asp:Content>