﻿Imports Business.Common.mdlCommon
Imports Business.DanhMuc
Imports System.Data
Imports VBOracleLib
Imports System.IO
'Imports iTextSharp.text.pdf
'Imports iTextSharp.text

Partial Class pages_HeThong_DanhMuc_frmDMLoaiNNT
    Inherits System.Web.UI.Page

    Private objDanhMuc As New buDanhMuc("LOAINNT")
    Dim Ma As String
    Dim Ten As String

    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click

        Dim objMLNS As Object
        'Dim regex_code As String = clsCTU.TCS_GETTS("SPECIAL_CHAR")
        'txtMaLH.Text = RemoveApostrophe(txtMaLH.Text)

        If txtMaLoaiNNT.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã loại NNT.")
            txtMaLoaiNNT.Focus()
            Exit Sub
        ElseIf txtMaLoaiNNT.Text.Trim.Length > txtMaLoaiNNT.MaxLength Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã loại NNT có độ dài tối đa" & txtMaLoaiNNT.MaxLength & " ký tự.")
            txtMaLoaiNNT.Focus()
            Exit Sub
            '*****Toanva - Check ky tu dac biet*********28/07/2015
            'Else
            '    If clsCommon.checkContansChar(txtMaLoaiNNT.Text.Trim, regex_code) Then
            '        clsCommon.ShowMessageBox(Me, "Bạn nhập vào Mã loại NNT không đúng - Có ký tự đặc biệt đã qui định.")
            '        txtMaLoaiNNT.Focus()
            '        Exit Sub
            '    End If
        End If

        If txtTenLoaiNNT.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên loại NNT.")
            txtTenLoaiNNT.Focus()
            Exit Sub
            'Else
            '    If clsCommon.checkContansChar(txtTenLoaiNNT.Text.Trim, regex_code) Then
            '        clsCommon.ShowMessageBox(Me, "Bạn nhập vào Tên loại NNT không đúng - Có ký tự đặc biệt đã qui định.")
            '        txtTenLoaiNNT.Focus()
            '        Exit Sub
            '    End If
        End If
        
        Dim Mota As String = ""
        Dim NewValue As String = ""
        Dim Action As String = ""
        Dim except As String = ""
        Try
            objMLNS = New LoaiNNT.infLoaiNNT

            objMLNS.MA_LOAI_NNT = RemoveApostrophe(txtMaLoaiNNT.Text.ToString.Trim)
            objMLNS.TEN_LOAI_NNT = RemoveApostrophe(txtTenLoaiNNT.Text.ToString.Trim)
            
            NewValue = "newvalue=""[DM_LOAI_NNT[Ma_Loai_NNT= " & objMLNS.MA_LOAI_NNT & ",Ten_Loai_NNT= " & objMLNS.Ten_Loai_NNT & "]]"" Description: " & ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) & " Exception:"

            Dim objInsert As New LoaiNNT.buLoaiNNT
            If Not Request.Params("id") Is Nothing Then
                Action = "DM_LOAI_NNT_UPDATE"
                objMLNS.MA_LOAI_NNT = Request.Params("id").Trim
                If objInsert.Update(objMLNS) Then
                    Mota &= " object: " & objMLNS.MA_LOAI_NNT & " result: Success  add_info: " & NewValue
                    xoa_Text()
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                Else
                    Mota &= " object: " & objMLNS.MA_LH & " result: Fail  add_info: " & NewValue
                    txtMaLoaiNNT.Focus()
                End If
            Else
                Action = "DM_LOAI_NNT_INSERT"
                If objInsert.Insert(objMLNS) Then
                    Mota &= " object: " & objMLNS.MA_LOAI_NNT & " result: Success  add_info: " & NewValue
                    xoa_Text()
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                Else
                    txtMaLoaiNNT.Focus()
                End If
            End If
            'Mota = " object: " & objMLNS.MA_CHUONG

        Catch ex As Exception
            except = ex.Message
            If objDanhMuc.Status = "EDIT" Then
                clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình cập nhật " & Ten & " !")
            Else
                clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình tạo mới " & Ten & " !")
            End If
        Finally
        End Try
        'LogApp.AddDebug(Action, Mota & except)
        load_dataGrid(0)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            RemoveHandler cmdSync.Click, AddressOf cmdSync_Click
            Exit Sub
        End If
        objDanhMuc = New buDanhMuc("LOAINNT")
        If Not IsPostBack Then
            load_dataGrid(0)
            cmdTaoMoi.Enabled = True
            If Not Request.Params("id") Is Nothing Then
                txtMaLoaiNNT.Enabled = False
                load_Detail(Request.Params("id").Trim)
            End If
            'cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn thực hiện không?') ")
        End If
    End Sub

    Private Sub load_dataGrid(ByVal page_index As Integer)
        Dim ds As DataSet
        Dim WhereClause As String = " AND 1=1"
        Try
            If txtMaLoaiNNT.Enabled Then
                If txtMaLoaiNNT.Text <> "" Then
                    WhereClause += " AND UPPER(MA_LOAI_NNT) like UPPER('%" + FilterSQLi(txtMaLoaiNNT.Text) + "%')"
                End If
                If txtTenLoaiNNT.Text <> "" Then
                    WhereClause += " AND upper(unvietnamese(TEN_LOAI_NNT)) like '%' || upper(unvietnamese('" + FilterSQLi(txtTenLoaiNNT.Text) + "')) || '%' "
                End If
            End If

            ds = objDanhMuc.GetDataSet(WhereClause)
            If hdnbox.Value.ToString <> WhereClause Or hdnbox.Value = "" Then
                hdnbox.Value = WhereClause
                dtgdata.CurrentPageIndex = 0
                dtgdata.DataSource = ds.Tables(0)
                dtgdata.DataBind()

                'gan vao datagrid de export excel
                GridView1.DataSource = ds.Tables(0)
                GridView1.AllowPaging = False
                GridView1.DataBind()
            Else
                dtgdata.CurrentPageIndex = page_index
                dtgdata.DataBind()
                dtgdata.DataSource = ds.Tables(0)
                dtgdata.DataBind()

                'gan vao datagrid de export excel
                GridView1.DataSource = ds.Tables(0)
                GridView1.AllowPaging = False
                GridView1.DataBind()
            End If

            If Not IsEmptyDataSet(ds) Then
            Else
                clsCommon.ShowMessageBox(Me, "Hệ thống không tìm thấy kết quả thỏa mãn điều kiện tra cứu")
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub load_Detail(ByVal id As String)
        Dim ds As DataSet
        ds = objDanhMuc.GetDataSet(" AND MA_LOAI_NNT = '" + id + "'")
        If ds.Tables(0).Rows.Count > 0 Then
            txtMaLoaiNNT.Text = ds.Tables(0).Rows(0)("MA_LOAI_NNT").ToString
            txtTenLoaiNNT.Text = ds.Tables(0).Rows(0)("TEN_LOAI_NNT").ToString
            
        End If
    End Sub

    Protected Sub xoa_Text()
        txtMaLoaiNNT.Text = ""
        txtTenLoaiNNT.Text = ""
        txtMaLoaiNNT.Enabled = True
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        xoa_Text()
        cmdTaoMoi.Enabled = True
        If Not Request.Params("id") Then
            Response.Redirect("~/pages/HeThong/DanhMuc/frmDMLoaiNNT.aspx")
        End If
    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim Mota As String = ""
        Dim NewValue As String = ""
        Dim Action As String = ""
        Dim except As String = ""

        Dim confirmValue As String = Request.Form("confirm_value")
        Dim i_char As Integer = confirmValue.LastIndexOf(",")
        i_char = i_char + 1
        confirmValue = confirmValue.Substring(i_char, confirmValue.Length - i_char)
        If confirmValue = "Có" Then
        Else
            Exit Sub
        End If
        Dim i As Integer
        Dim strMaLoaiNNT As String
        Dim objDel As New LoaiNNT.buLoaiNNT()
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strMaLoaiNNT = Trim(dtgdata.Items(i).Cells(0).Text)
                'If objDel.CheckDelete(strMaChuong) Then
                '    LogApp.AddDebug("DM_CHUONG_DELETE", " object: " & strMaChuong & " result: Fail  add_info: Ma chuong da su dung de lap chung tu Description: " & ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) & " Exception:")
                '    clsCommon.ShowMessageBox(Me, "Bạn không thể xoá mã chương đã sử dụng để lập chứng từ!")
                'Else
                If objDel.Delete(strMaLoaiNNT) Then
                    'LogApp.AddDebug("DM_LHINH_DELETE", " object: " & strMaLH & " result: Success  add_info:  Description: " & ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) & " Exception:")
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                Else
                    'LogApp.AddDebug("DM_CHUONG_DELETE", " object: " & strMaLH & " result: Fail  add_info: loi trong qua trinh xoa du lieu  Description: " & ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) & " Exception:")
                    clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình xóa dữ liệu!")
                End If
                'End If
                v_count += 1
            End If
        Next
        load_dataGrid(0)
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub

    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        'Try
        '    Dim frm As New Business.BaoCao.infBaoCao
        '    'Business.Common.mdlSystemVariables.gLoaiBaoCao = Report_Type.DM_CAPCHUONG
        '    Session("objBaoCao") = frm
        '    clsCommon.OpenNewWindow(Me, "../../BaoCao/frmShowReports.aspx?BC=" & Report_Type.DM_CAPCHUONG & "", "ManPower")
        'Catch ex As Exception
        'End Try
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        load_dataGrid(e.NewPageIndex)
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        load_dataGrid(0)
        dtgdata.CurrentPageIndex = 0
        dtgdata.DataBind()
        cmdTaoMoi.Enabled = False
    End Sub

    Protected Sub cmdTaoMoi_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Init
        If Session.IsNewSession Then
            ' Force session to be created;
            ' otherwise the session ID changes on every request.
            Session("ForceSession") = DateTime.Now
        End If
        ' 'Sign' the viewstate with the current session.
        Me.ViewStateUserKey = Session.SessionID
        If Page.EnableViewState Then
            ' Make sure ViewState wasn't passed on the querystring.
            ' This helps prevent one-click attacks.
            If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
                Throw New Exception("Viewstate existed, but not on the form.")
            End If
        End If
    End Sub

    Protected Sub cmdSync_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSync.Click

        'Dim confirmValue As String = Request.Form("confirm_value")
        'Dim i_char As Integer = confirmValue.LastIndexOf(",")
        'i_char = i_char + 1
        'confirmValue = confirmValue.Substring(i_char, confirmValue.Length - i_char)
        'If confirmValue = "Có" Then
        'Else
        '    Exit Sub
        'End If
        'Dim pv_ErrorNum As String = ""
        'Dim pv_ErrorMes As String = ""
        ''CustomsService.ProcessMSG.getMSG15("LH", "", pv_ErrorNum, pv_ErrorMes, Session.Item("MA_CN_USER_LOGON").ToString)
        'If pv_ErrorNum <> "0" Then
        '    If pv_ErrorMes <> "" Then
        '        clsCommon.ShowMessageBox(Me, pv_ErrorMes)
        '    Else
        '        clsCommon.ShowMessageBox(Me, "Đồng bộ thành công")
        '    End If
        'End If
    End Sub

    Protected Sub cmdExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExport.Click
        GridView1.Visible = True
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=DM_LOAINNT.xls")
        Response.Charset = "utf-8"
        Response.ContentType = "application/vnd.ms-excel"
        Using sw1 As New StringWriter()
            Dim hw1 As New HtmlTextWriter(sw1)
            GridView1.RenderControl(hw1)
            Response.Output.Write(sw1.ToString)
            Response.Flush()
            Response.End()
        End Using
    End Sub

    Protected Function ExportToExcel() As Boolean
        Dim ok As Boolean = True
        Try
            Dim sw As New StringWriter()
            Dim hw As New System.Web.UI.HtmlTextWriter(sw)
            Dim frm As HtmlForm = New HtmlForm()

            Page.Response.AddHeader("content-disposition", "attachment;filename=DanhMucLoaiNNT.xls")
            Page.Response.ContentType = "application/vnd.ms-excel"
            Page.Response.Charset = ""
            Page.EnableViewState = False
            frm.Attributes("runat") = "server"
            Controls.Add(frm)
            frm.Controls.Add(GridView1)
            frm.RenderControl(hw)
            Response.Write(sw.ToString())
            Response.Flush()
            Response.End()
        Catch ex As Exception
            ok = False
        End Try
        Return ok
    End Function

    Protected Sub btnExportPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportPDF.Click
        'GridView1.Visible = True
        'Dim bf As BaseFont = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + "\fonts\Arial.ttf", BaseFont.IDENTITY_H, True)
        'Dim table As New iTextSharp.text.pdf.PdfPTable(GridView1.Columns.Count)
        'Dim widths As Integer() = New Integer(GridView1.Columns.Count - 1) {}
        'For x As Integer = 0 To GridView1.Columns.Count - 1
        '    widths(x) = CInt(GridView1.Columns(x).ItemStyle.Width.Value)
        '    Dim cellText As String = Server.HtmlDecode(GridView1.HeaderRow.Cells(x).Text)
        '    'Set Font and Font Color
        '    Dim font As New iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.BOLD)
        '    Dim cell As New iTextSharp.text.pdf.PdfPCell(New Phrase(12, cellText, font))
        '    'Set Header Row BackGround Color
        '    'Important for Arabic, Persian or Urdu Text
        '    cell.RunDirection = PdfWriter.RUN_DIRECTION_DEFAULT
        '    cell.HorizontalAlignment = Element.ALIGN_CENTER
        '    table.AddCell(cell)
        'Next
        'table.SetWidths(widths)
        'For i As Integer = 0 To GridView1.Rows.Count - 1
        '    'If GridView1.Rows(i).RowType = DataControlRowType.DataRow Then
        '    For j As Integer = 0 To GridView1.Columns.Count - 1
        '        If j = 0 Then
        '            Dim lblmachuong As Label = GridView1.Rows(i).FindControl("lblMA_LOAINNT")
        '            Dim cellText As String = Server.HtmlDecode(lblmachuong.Text)
        '            'Set Font and Font Color
        '            Dim font As New iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL)
        '            Dim cell As New iTextSharp.text.pdf.PdfPCell(New Phrase(12, cellText, font))
        '            'Set Color of row
        '            If i Mod 2 = 0 Then
        '                'Set Row BackGround Color
        '            End If
        '            'Important for Arabic, Persian or Urdu Text
        '            cell.RunDirection = PdfWriter.RUN_DIRECTION_DEFAULT
        '            table.AddCell(cell)
        '        Else
        '            Dim cellText As String = Server.HtmlDecode(GridView1.Rows(i).Cells(j).Text)
        '            'Set Font and Font Color
        '            Dim font As New iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL)
        '            Dim cell As New iTextSharp.text.pdf.PdfPCell(New Phrase(12, cellText, font))
        '            'Set Color of row
        '            If i Mod 2 = 0 Then
        '                'Set Row BackGround Color
        '            End If
        '            'Important for Arabic, Persian or Urdu Text
        '            cell.RunDirection = PdfWriter.RUN_DIRECTION_DEFAULT
        '            table.AddCell(cell)
        '        End If

        '    Next
        '    'End If
        'Next

        ''Create the PDF Document
        'Dim pdfDoc As New Document(PageSize.A4, 10.0F, 10.0F, 10.0F, 0.0F)
        'PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
        'pdfDoc.Open()
        'pdfDoc.Add(table)
        'pdfDoc.Close()
        'Response.ContentType = "application/pdf"
        'Response.AddHeader("content-disposition", "attachment;filename=DM_LOAINNT.pdf")
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Write(pdfDoc)
        'Response.[End]()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
End Class
