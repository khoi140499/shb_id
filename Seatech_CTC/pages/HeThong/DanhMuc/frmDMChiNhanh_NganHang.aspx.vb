﻿Imports System.Data
Imports Business.Common.mdlCommon
Imports Business.DanhMuc
Imports VBOracleLib
Imports Business.HeThong

Partial Class pages_HeThong_DanhMuc_frmDMCN_NganHang
    Inherits System.Web.UI.Page
    Private objCNNganHang As New CNNganHang.buCNNganHang()

    Private Sub GetBranchBankList()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtMA_PGD.Enabled Then
                If txtMA_PGD.Text <> "" Then
                    WhereClause += " AND upper(id) like upper('" + txtMA_PGD.Text.Trim + "%')"
                End If
                If txtMa_CN.Text <> "" Then
                    WhereClause += " AND upper(BRANCH_code) like upper('" + txtMa_CN.Text.Trim + "%')"
                End If
                If txtTEN_PGD.Text <> "" Then
                    WhereClause += " AND upper(name) like upper('%" + txtTEN_PGD.Text.Trim + "%')"
                End If
                If cbo_TinhTrang.SelectedValue <> "" Then
                    WhereClause += " AND tinh_trang = '" + cbo_TinhTrang.SelectedValue.ToString + "'"
                End If
                If cbo_PhanCap.SelectedValue <> "" Then
                    WhereClause += " AND phan_cap = '" + cbo_PhanCap.SelectedValue.ToString + "'"
                End If
                If txtMA_XA.Text.Trim <> "" Then
                    WhereClause += " AND upper(ma_xa) like upper('" + txtMA_XA.Text.Trim + "%')"
                End If
            End If

            ds = objCNNganHang.Load(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgdata.DataSource = ds.Tables(0)
                dtgdata.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub
    Private Sub GetBranchBankList_new()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtMA_PGD.Enabled Then
                If txtMA_PGD.Text <> "" Then
                    WhereClause += " AND upper(b.id) like upper('" + txtMA_PGD.Text + "%')"
                End If
                If txtMa_CN.Text <> "" Then
                    WhereClause += " AND upper(b.BRANCH_code) like upper('" + txtMa_CN.Text + "%')"
                End If
                If txtTEN_PGD.Text <> "" Then
                    WhereClause += " AND upper(b.name) like upper('%" + txtTEN_PGD.Text + "%')"
                End If
                If drpTT_XU_LY.SelectedValue <> "" Then
                    WhereClause += " AND b.TT_XU_LY = '" + drpTT_XU_LY.SelectedValue.ToString + "'"
                End If
                If cbo_PhanCap.SelectedValue <> "" Then
                    WhereClause += " AND b.phan_cap = '" + cbo_PhanCap.SelectedValue.ToString + "'"
                End If
                If txtMA_XA.Text.Trim <> "" Then
                    WhereClause += " AND upper(b.ma_xa) like upper('" + txtMA_XA.Text + "%')"
                End If
            End If

            ds = objCNNganHang.Load_new(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                grd_new.DataSource = ds.Tables(0)
                grd_new.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub

    Private Sub GetDetailBranchBank(ByVal id As String)
        Dim ds As DataSet
        ds = objCNNganHang.Load(" AND id = '" + id + "'")

        If ds.Tables(0).Rows.Count > 0 Then
            txtMa_CN.Text = ds.Tables(0).Rows(0)("BRANCH_code").ToString
            txtMA_PGD.Text = ds.Tables(0).Rows(0)("id").ToString
            txtTEN_PGD.Text = ds.Tables(0).Rows(0)("name").ToString
            txtMA_XA.Text = ds.Tables(0).Rows(0)("ma_xa").ToString
            cbo_TinhTrang.SelectedValue = ds.Tables(0).Rows(0)("tinh_trang").ToString
            cbo_PhanCap.SelectedValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            txtTaiKhoan_tienmat.Text = ds.Tables(0).Rows(0)("taikhoan_tienmat").ToString
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            EnableButton_Nhom(Session.Item("MA_NHOM"))
            hdfTenDN.Value = Session.Item("UserName")
            cmdTaoMoi.Enabled = True
            If (Not Request.Params("id") Is Nothing) Then
                txtMA_PGD.Enabled = False
                GetDetailBranchBank(Request.Params("id").Trim)
            End If
            'GetBranchBankList()
            'cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn xóa dữ liệu đã chọn?') ")
        End If
    End Sub
    Protected Sub EnableButton_Nhom(ByVal Nhom As String)
        Nhom = clsCommon.Check_Maker_Checker(Nhom)
        hdfMa_Nhom.Value = Nhom
        If Nhom = "10" Then
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = True
            'cmdIn.Visible = True
            cmdDeleteUser.Visible = True
            cmdTaoMoi.Visible = True
            drpDuLieu.SelectedIndex = 0
            GetBranchBankList()
            cmdTaoMoi.Enabled = True
        ElseIf Nhom = "11" Then
            btnDuyet.Visible = True
            btnTuChoi.Visible = True
            'rowTrangThai.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            drpDuLieu.SelectedIndex = 1
            GetBranchBankList_new()
        Else
            btnDuyet.Visible = False
            btnTuChoi.Visible = False
            cmdCancel.Visible = False
            'cmdIn.Visible = False
            cmdDeleteUser.Visible = False
            cmdTaoMoi.Visible = False
            drpDuLieu.SelectedIndex = 0
            GetBranchBankList()
        End If
    End Sub

    Protected Sub dtgdata_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgdata.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If Not MaNhom = "10" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim checkBox As HyperLink = DirectCast(e.Item.FindControl("Hyperlink1"), HyperLink)
                checkBox.Enabled = False
            End If
        End If
    End Sub
    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        GetBranchBankList()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        If drpDuLieu.SelectedIndex = 0 Then
            GetBranchBankList()
            dtgdata.CurrentPageIndex = 0
            dtgdata.DataBind()
            grd_new.Visible = False
            dtgdata.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = False
                btnTuChoi.Visible = False
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = True
                'cmdIn.Visible = True
                cmdDeleteUser.Visible = True
                cmdTaoMoi.Visible = True
            End If
        Else
            GetBranchBankList_new()
            dtgdata.Visible = False
            grd_new.Visible = True
            If hdfMa_Nhom.Value = "11" Then
                btnDuyet.Visible = True
                btnTuChoi.Visible = True
            ElseIf hdfMa_Nhom.Value = "10" Then
                cmdCancel.Visible = False
                'cmdIn.Visible = False
                cmdDeleteUser.Visible = False
                cmdTaoMoi.Visible = False
            End If
        End If
        cmdTaoMoi.Enabled = False
    End Sub

    Private Sub ClearFrom()
        txtMA_PGD.Text = ""
        txtMa_CN.Text = ""
        txtTEN_PGD.Text = ""
        cbo_PhanCap.SelectedValue = ""
        cbo_TinhTrang.SelectedValue = ""
        txtMA_PGD.Enabled = True
        txtTaiKhoan_tienmat.Text = ""
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        ClearFrom()

        Response.Redirect("~/pages/HeThong/DanhMuc/frmDMChiNhanh_NganHang.aspx")
    End Sub

    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        If txtMA_PGD.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã phòng giao dịch.")
            txtMA_PGD.Focus()
            Exit Sub
            '*****TYNK - Check ky tu dac biet*********07/02/2013
        Else
            If Regex.IsMatch(txtMA_PGD.Text.Trim, "^[a-zA-Z0-9]*$") = False Then
                clsCommon.ShowMessageBox(Me, "Chuỗi bạn nhập không đúng - Có ký tự đặc biệt.")
                txtMA_PGD.Focus()
                Exit Sub
            End If
            '***************************************************
        End If

        If txtTEN_PGD.Text.Trim = "" Then
            txtTEN_PGD.Focus()
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập tên phòng giao dịch.")
        End If
        'If buUsers.check_MaCN(txtMa_CN.Text.ToString.Trim) = False Then
        '    clsCommon.ShowMessageBox(Me, "Mã chi nhánh không tồn tại hoặc sai định dạng (000000), vui lòng nhập lại:")
        '    txtMa_CN.Focus()
        '    Exit Sub
        '    '*****TYNK - Check ky tu dac biet*********07/02/2013
        'Else
        '    If Regex.IsMatch(txtMa_CN.Text.Trim, "^[a-zA-Z0-9]*$") = False Then
        '        clsCommon.ShowMessageBox(Me, "Chuỗi bạn nhập không đúng - Có ký tự đặc biệt.")
        '        txtMa_CN.Focus()
        '        Exit Sub
        '    End If
        '    '***************************************************
        'End If
        If Regex.IsMatch(txtMa_CN.Text.Trim, "^[a-zA-Z0-9]*$") = False Then
            clsCommon.ShowMessageBox(Me, "Chuỗi bạn nhập không đúng - Có ký tự đặc biệt.")
            txtMa_CN.Focus()
            Exit Sub
        End If
        If cbo_TinhTrang.SelectedValue = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn hãy chọn tình trạng hoạt động.")
            cbo_TinhTrang.Focus()
            Exit Sub
        End If

        If cbo_PhanCap.SelectedValue = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mức phân cấp.")
            cbo_PhanCap.Focus()
            Exit Sub
        End If
        If txtMA_XA.Text.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập mã ĐBHC.")
            cbo_PhanCap.Focus()
            Exit Sub
        End If
        If txtTaiKhoan_tienmat.Text.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập tài khoản tiền mặt.")
            txtTaiKhoan_tienmat.Focus()
            Exit Sub
        End If
        Dim infNH As New CNNganHang.infCNNganHang()
        Try

            infNH.MA_PGD = txtMA_PGD.Text.ToString.Trim
            infNH.MA_CN = txtMa_CN.Text.ToString.Trim
            infNH.TEN_PGD = txtTEN_PGD.Text.ToString.Trim
            infNH.TINH_TRANG = cbo_TinhTrang.SelectedValue.ToString
            infNH.PHAN_CAP = cbo_PhanCap.SelectedValue.ToString
            infNH.MA_XA = txtMA_XA.Text.Trim
            If infNH.MA_XA.Length > 2 Then
                infNH.MA_XA = infNH.MA_XA.Substring(0, 2)
            End If
            infNH.NGUOI_TAO = hdfTenDN.Value
            infNH.TAIKHOAN_TIENMAT = txtTaiKhoan_tienmat.Text.ToString.Trim
            If Not Request.Params("id") Is Nothing Then
                infNH.TYPE = "2" 'UPDATE
                infNH.ID = Request.Params("id").Trim
                'Nếu có không có thay đổi chờ xử lý
                If Not Check_ThayDoi(infNH) Then
                    If objCNNganHang.Update(infNH, "10") Then
                        'ClearFrom()
                        clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                    Else
                        clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                End If
            Else
                infNH.TYPE = "1" 'INSERT
                If Not Check_Insert(infNH.MA_PGD) Then
                    '10 thêm vào trong HIS_DM_CAP_CHUONG
                    If objCNNganHang.Insert(infNH, "10") Then
                        ClearFrom()
                        clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                    Else
                        clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Thông tin mã PGD này đã tồn tại và đang chờ xử lý!")
                End If
                
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình cập nhật danh mục ngân hàng")
        Finally
        End Try
        GetBranchBankList()
    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim i As Integer
        Dim strID As String
        Dim strPGD As String = ""
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            'strPGD = Trim(dtgdata.Items(i).Cells(1).Text)
            If chkSelect.Checked Then
                strID = Trim(dtgdata.Items(i).Cells(0).Text)
                'strPGD = Trim(dtgdata.Items(i).Cells(1).Text)
                ' strMA_DTHU = Trim(dtgdata.Items(i).Cells(6).Text)
                Dim objMLNS As New CNNganHang.infCNNganHang
                objMLNS.ID = strID
                objMLNS.TYPE = "3" 'xóa dữ liệu
                objMLNS.NGUOI_TAO = hdfTenDN.Value
                objMLNS.MA_PGD = strID
                If Not Check_ThayDoi(objMLNS) Then
                    If objCNNganHang.Delete(objMLNS, "10") Then
                        clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                    Else
                        clsCommon.ShowMessageBox(Me, "Dữ liệu đã sử dụng trong chức năng khác, không được xoá!")
                    End If
                Else
                    clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")
                End If
                v_count += 1
            End If
        Next
        ClearFrom()
        GetBranchBankList()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub
    Protected Sub Get_Data_Item(ByRef objItem As CNNganHang.infCNNganHang)
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_dm_chi_nhanh WHERE HIS_ID=" & objItem.HIS_ID
        dt = DataAccess.ExecuteToTable(WhereClause)
        objItem.TEN_PGD = dt.Rows(0)("name").ToString
        objItem.TINH_TRANG = dt.Rows(0)("tinh_trang").ToString
        objItem.PHAN_CAP = dt.Rows(0)("phan_cap").ToString
        objItem.MA_PGD = dt.Rows(0)("id").ToString
        objItem.MA_XA = dt.Rows(0)("MA_XA").ToString
        objItem.MA_CN = dt.Rows(0)("BRANCH_ID").ToString
    End Sub
    'Kiểm tra có thay đổi thông tin đang chờ xử lý ko
    'Nếu có thay đổi thì trả về True/False
    Protected Function Check_ThayDoi(ByRef objItem As CNNganHang.infCNNganHang) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_dm_chi_nhanh WHERE ID='" & objItem.MA_PGD & "' AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function
    Protected Function Check_Insert(ByRef strMaPGD As String) As Boolean
        Dim dt As DataTable
        Dim WhereClause As String = "SELECT * FROM his_dm_chi_nhanh WHERE ID='" & strMaPGD & "' AND TT_XU_LY='0'"
        dt = DataAccess.ExecuteToTable(WhereClause)
        If dt.Rows.Count > 0 Then
            'Có thay đổi
            Return True
        End If
        Return False
    End Function

    Protected Sub btnDuyet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuyet.Click
        Dim i As Integer
        Dim strID As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                Dim objMLNS = New CNNganHang.infCNNganHang
                objMLNS.HIS_ID = strID
                objMLNS.MA_PGD = strID
                objMLNS.NGUOI_DUYET = hdfTenDN.Value
                objMLNS.TT_XU_LY = "1"
                objMLNS.TAIKHOAN_TIENMAT = Trim(grd_new.Items(i).Cells(17).Text)
                Dim objInsert As New CNNganHang.buCNNganHang
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Duyệt thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        GetBranchBankList_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Duyệt thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi duyệt!")
        End If
    End Sub

    Protected Sub btnTuChoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTuChoi.Click
        Dim i As Integer
        Dim strID As String
        Dim strType As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To grd_new.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = grd_new.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(grd_new.Items(i).Cells(0).Text)
                strType = Trim(grd_new.Items(i).Cells(1).Text)
                Dim objMLNS = New CNNganHang.infCNNganHang
                objMLNS.HIS_ID = strID
                objMLNS.MA_PGD = strID
                objMLNS.NGUOI_DUYET = hdfTenDN.Value
                objMLNS.TT_XU_LY = "2"
                Dim objInsert As New CNNganHang.buCNNganHang
                'Duyệt thêm mới
                If strType = "1" Then
                    If objInsert.Insert(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt sửa thông tin
                ElseIf strType = "2" Then
                    Get_Data_Item(objMLNS)
                    If objInsert.Update(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                    'Duyệt xóa dữ liệu
                ElseIf strType = "3" Then
                    If objInsert.Delete(objMLNS, "11") Then
                        clsCommon.ShowMessageBox(Me, "Từ chối thành công")
                    End If
                End If
                v_count += 1
            End If
        Next
        GetBranchBankList_new()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Từ chối thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi từ chối!")
        End If
    End Sub

    Protected Sub grd_new_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grd_new.ItemDataBound
        Dim MaNhom As String = hdfMa_Nhom.Value 'Session.Item("MA_NHOM")
        If MaNhom = "10" Then
            e.Item.Cells(17).Visible = False
        ElseIf MaNhom = "11" Then
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim tt_xu_ly As String = e.Item.Cells(15).Text.ToString
                If tt_xu_ly <> "0" Then
                    Dim checkBox As CheckBox = DirectCast(e.Item.FindControl("chkSelect"), CheckBox)
                    checkBox.Visible = False
                End If
            End If
        Else
            e.Item.Cells(17).Visible = False
        End If
    End Sub

    Protected Sub grd_new_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grd_new.PageIndexChanged
        GetBranchBankList_new()
        grd_new.CurrentPageIndex = e.NewPageIndex
        grd_new.DataBind()
    End Sub
End Class
