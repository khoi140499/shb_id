﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmDMKhoBac.aspx.vb"
 Inherits="pages_HeThong_DanhMuc_frmDMKhoBac" title="Danh mục kho bạc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" language="javascript">
    function ShowLov(strType)
    {
        if (strType=="DBHCTINH") return FindDanhMuc('DBHCTINH','<%=txtMaTinh.ClientID %>','<%=txtTen.ClientID %>');         
        if (strType=="DBHCHUYEN") return FindDanhMuc('DBHCHUYEN','<%=txtMaDBHC.ClientID %>','<%=txtTenDBHC.ClientID %>');        
                          
    }
    function FindDanhMuc(strPage,txtID, txtTitle) 
    {        
        var returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&initParam=" + document.getElementById(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;","_new");

                
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;  
                //alert(document.getElementById(txtID).value & ' --- ' & document.getElementById(txtTitle).value);
                
    }
    
    function jsGet_TenKB(){
        if (document.getElementById ('<%= txtMaTinh.ClientID %>').value != ''){
            PageMethods.GetTenTinh($get('<%= txtMaTinh.ClientID %>').value,jsGet_TenKB_Success,jsGet_TenKB_Error);
        }
    }
    
    function jsGet_TenKB_Success(result){
        var url=result;
        if (url.length > 0){
            document.getElementById ('<%= txtTen.ClientID %>').value = url ;
        }
        else {
            alert('Không tìm được chi nhánh!');
        }
        
    }
    function jsGet_TenKB_Error(result){
       //alert('Xảy ra lỗi trong quá trình lấy thông tin Chi nhánh.' );
    }
    
    //Ten DBHC
    function jsGet_TenDBHC(){
        if (document.getElementById ('<%= txtMaDBHC.ClientID %>').value != ''){
            PageMethods.GetTenDBHC($get('<%= txtMaDBHC.ClientID %>').value,jsTenDBHC_Success,jsTenDBHC_Error);
        }
    }
    
    function jsTenDBHC_Success(result){
        var url=result;
        if (url.length > 0){
            document.getElementById ('<%= txtTenDBHC.ClientID %>').value = url ;
        }
        else {
            //alert('Không tìm được chi nhánh!');
        }
        
    }
    function jsTenDBHC_Error(result){
       //alert('Xảy ra lỗi trong quá trình lấy thông tin Chi nhánh.' );
    }
    //Ten DBHC
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">DANH MỤC KHO BẠC</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <asp:HiddenField ID="hdfMa_Nhom" runat="server" />
            <asp:HiddenField ID="hdfTenDN" runat="server" />
        </tr>
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%"
                    class="form_input">
                    <tr align="left">
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label3" runat="server" CssClass="label">Số hiệu kho bạc</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtSHKB" runat="server" MaxLength="4" Width="40%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblUserNameCaption" runat="server" CssClass="label">Tên kho bạc</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtTenKB" runat="server" MaxLength="500" Width="71%" CssClass="inputflat"
                                TabIndex="2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left" style="display:none">
                        <td class="form_label">
                            <asp:Label ID="lblMaTinh" runat="server" CssClass="label">Mã Tỉnh</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMaTinh" runat="server" MaxLength="5" Width="20%" CssClass="inputflat" style="background: Aqua" 
                                 TabIndex="2" onkeypress="if (event.keyCode==13){ShowLov('DBHCTINH');}" onblur="jsGet_TenKB();"></asp:TextBox>
                            <asp:TextBox ID="txtTen" runat="server" MaxLength="500" Width="50%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left" >
                        <td class="form_label">
                            <asp:Label ID="lblDB" runat="server" CssClass="label">Mã địa bàn</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMaDBHC" runat="server" MaxLength="5" Width="20%" CssClass="inputflat" style="background: Aqua" 
                                TabIndex="2" onkeypress="if (event.keyCode==13){ShowLov('DBHCHUYEN');}" onblur="jsGet_TenDBHC();"></asp:TextBox>
                            <asp:TextBox ID="txtTenDBHC" runat="server" MaxLength="500" Width="50%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblPasswordCaption" CssClass="label" runat="server">Trạng thái</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID="cboTinh_trang" runat="server" CssClass="inputflat" Width="40%">
                                <asp:ListItem Text="Chọn" Value=""></asp:ListItem>
                                <asp:ListItem Text="Đang hoạt động" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Ngừng hoạt động" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="left" id="Tr1" runat="server">
                        <td class="form_label">
                            <asp:Label ID="Label4" CssClass="label" runat="server">Thông tin dữ liệu</asp:Label>
                        </td>
                        <td class="form_control">
                           <asp:DropDownList ID="drpDuLieu" runat="server" CssClass="inputflat" Width="40%">
                                <asp:ListItem Text="Dữ liệu hiện tại" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Dữ liệu thay đổi(thêm/sửa/xóa)" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="left" id="Tr2" runat="server">
                        <td class="form_label">
                            <asp:Label ID="Label5" CssClass="label" runat="server">Trạng thái xử lý</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID="drpTT_XU_LY" runat="server" CssClass="inputflat" Width="40%">
                                <asp:ListItem Text="Tất cả" Value=""></asp:ListItem>
                                <asp:ListItem Text="Chờ duyệt" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Đã duyệt" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Từ chối" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdSearch" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdTaoMoi" runat="server" CssClass="ButtonCommand" Text="Ghi" TabIndex="5" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdDeleteUser" runat="server" CssClass="ButtonCommand" Text="Xóa" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdIn" Enabled="false" runat="server" CssClass="ButtonCommand" Text="In" Visible="false" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdTruyvanThue" runat="server" CssClass="ButtonCommand" Text="Đồng bộ DM từ TCT" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="btnDuyet" runat="server" CssClass="ButtonCommand" Text="Duyệt" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="btnTuChoi" runat="server" CssClass="ButtonCommand" Text="Từ chối" UseSubmitBehavior="false"></asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
             <%--<div style="vertical-align :top; height: 300px; overflow: auto; width: 600px;">--%>
                <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                         <asp:BoundColumn DataField="SHKB" HeaderText="Số hiệu kho bạc" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Số hiệu kho bạc" HeaderStyle-Width="100">
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SHKB") %>'
                                    NavigateUrl='<%#"~/pages/HeThong/DanhMuc/frmDMKhoBac.aspx?ID=" & DataBinder.Eval(Container.DataItem, "SHKB") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle Width="20%"></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TEN" HeaderText="Tên kho bạc">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_TINH" HeaderText="Mã tỉnh">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_DB" HeaderText="Mã địa bàn">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="tinhtrang" HeaderText="Tình trạng">
                            <HeaderStyle Width="120px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
                <asp:DataGrid ID="grd_new" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="id" HeaderText="ID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="type" HeaderText="type" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ThayDoi" HeaderText="Thay đổi" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="SHKB_new" HeaderText="SHKB mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ten_new" HeaderText="Tên SHKB mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_TINH_new" HeaderText="Mã tỉnh mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_DB_new" HeaderText="Mã địa bàn mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tinh_trang_new" HeaderText="Tình trạng mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="SHKB" HeaderText="SHKB hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ten" HeaderText="Tên SHKB hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_TINH" HeaderText="Mã tỉnh hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_DB" HeaderText="Mã địa bàn hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tinh_trang" HeaderText="Tình trạng hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tt_xu_ly" HeaderText="Trạng thái xử lý" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="xu_ly" HeaderText="Trạng thái xử lý"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>

