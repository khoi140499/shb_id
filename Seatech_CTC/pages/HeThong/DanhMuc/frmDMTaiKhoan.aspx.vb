﻿Imports Business.Common.mdlCommon
Imports Business.DanhMuc
Imports System.Data
Imports VBOracleLib

Partial Class pages_HeThong_DanhMuc_frmDMTaiKhoan
    Inherits System.Web.UI.Page
    Private objTaiKhoan As New TaiKhoan.buTaiKhoan()
    Dim MaTK As String
    Dim TenTK As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            load_Datagrid()
            cmdTaoMoi.Enabled = True
            dmKhoBac()
            If Not Request.Params("id") Is Nothing Then
                txtTK.Enabled = False
                load_Detail(Request.Params("id").Trim)
            End If
            cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn thực hiện không?') ")
        End If
    End Sub

    Private Sub load_Datagrid()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtTK.Enabled Then
                If txtTK.Text <> "" Then
                    WhereClause += " AND tk = '" + txtTK.Text + "'"
                End If
                If txtTenTK.Text <> "" Then
                    WhereClause += " AND ten_tk = '" + txtTenTK.Text + "'"
                End If
                If txtMaDBHC.Text <> "" Then
                    WhereClause += " AND dbhc = '" + txtMaDBHC.Text + "'"
                End If
                If txtMaCQT.Text <> "" Then
                    WhereClause += " AND ma_cqthu = '" + txtMaCQT.Text + "'"
                End If
                If cboTinh_trang.SelectedItem.ToString() <> "" Then
                    WhereClause += " AND tcs_dm_taikhoan.tinh_trang = '" + cboTinh_trang.SelectedValue.ToString() + "'"
                End If
            End If
            ds = objTaiKhoan.Load(WhereClause)
            If Not ds Is Nothing Then
                dtgdata.DataSource = ds.Tables(0)
                dtgdata.DataBind()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub load_Detail(ByVal ID As String)
        Dim ds As DataSet
        ds = objTaiKhoan.Load(" AND tk = '" + ID + "'")
        If ds.Tables(0).Rows.Count > 0 Then
            txtTK.Text = ds.Tables(0).Rows(0)("tk").ToString
            txtTenTK.Text = ds.Tables(0).Rows(0)("ten_tk").ToString
            txtMaDBHC.Text = ds.Tables(0).Rows(0)("dbhc").ToString
            txtMaCQT.Text = ds.Tables(0).Rows(0)("ma_cqthu").ToString
            If ds.Tables(0).Rows(0)("tinh_trang").ToString = "Dang hoat dong" Then
                cboTinh_trang.SelectedValue = 0
            Else
                cboTinh_trang.SelectedValue = 1
            End If

            '*****TYNK - Hiển thị TK nợ / có - Hiển thị SHKB*********25/02/2013
            If ds.Tables(0).Rows(0)("tk_kb_nh").ToString = "Co" Then
                cboTK_KB_NH.SelectedValue = 0
            Else
                cboTK_KB_NH.SelectedValue = 1
            End If
            cboKhoBac.SelectedItem.Text = ds.Tables(0).Rows(0)("ten").ToString
            '***************************************************
        End If
        'For i As Integer = 0 To dtgdata.Items.Count - 1
        '    If dtgdata.Items(i).Cells(0).Text = ID Then
        '        txtTK.Text = dtgdata.Items(i).Cells(0).Text.ToString
        '        txtTenTK.Text = dtgdata.Items(i).Cells(2).Text.ToString
        '        txtMaDBHC.Text = dtgdata.Items(i).Cells(3).Text.ToString
        '        txtMaCQT.Text = dtgdata.Items(i).Cells(4).Text.ToString

        '        If dtgdata.Items(i).Cells(5).Text.ToString = "Ngung hoat dong" Then
        '            cboTinh_trang.SelectedValue = "1"
        '        Else
        '            cboTinh_trang.SelectedValue = "0"
        '        End If
        '    End If
        'Next
    End Sub

    Private Sub Xoa_text()
        txtTenTK.Text = ""
        txtTK.Text = ""
        txtMaDBHC.Text = ""
        txtMaCQT.Text = ""
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Xoa_text()
        If Not Request.Params("id") Then
            Response.Redirect("~/pages/HeThong/DanhMuc/frmDMTaiKhoan.aspx")
        End If
    End Sub

    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        Dim objMLNS As Object

        txtTK.Text = RemoveApostrophe(txtTK.Text)
        txtTenTK.Text = RemoveApostrophe(txtTenTK.Text)
        If txtTK.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã tài khoản.")
            txtTK.Focus()
            Exit Sub
            '*****TYNK - Sửa nhập mã từ 4 đến 12 kí tự thay vì 4 hoặc 12 kí tự *********25/02/2013
        ElseIf txtTK.Enabled = True And (txtTK.Text.ToString.Trim.Length < 4 Or txtTK.Text.ToString.Trim.Length > 12) Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã tài khoản có độ dài từ 4 đến 12 ký tự. ")
            txtTK.Focus()
            Exit Sub
            '***************************************************
            '*****TYNK - Check ky tu dac biet*********19/02/2013
        Else
            If Regex.IsMatch(txtTK.Text.Trim, "^[a-zA-Z0-9]*$") = False Then
                clsCommon.ShowMessageBox(Me, "Chuỗi bạn nhập không đúng - Có ký tự đặc biệt.")
                txtTK.Focus()
                Exit Sub
            End If
            '***************************************************
        End If

        If txtTenTK.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên Tài khoản.")
            txtTenTK.Focus()
            Exit Sub
        End If
        If "".Equals(txtMaDBHC.Text.Trim) Then
            clsCommon.ShowMessageBox(Me, "Mã địa bàn hành chính không được để trống.!")
            txtMaDBHC.Focus()
            Exit Sub
        End If
        If txtMaDBHC.Text.Trim <> "" Then
            If "".Equals(Get_TenDBHC(txtMaDBHC.Text.Trim)) Then
                txtMaDBHC.Focus()
                clsCommon.ShowMessageBox(Me, "Mã địa bàn hành chính chưa tồn tại trong hệ thống.")
                Exit Sub
            End If
        End If
        If "".Equals(txtMaCQT.Text.Trim) Then
            clsCommon.ShowMessageBox(Me, "Mã cơ quan thu không được để trống.!")
            txtMaCQT.Focus()
            Exit Sub
        End If
        If txtMaCQT.Text.ToString.Trim <> "" Then
            Dim objCQThu As New CQThu.daCQThu
            If objCQThu.Load(txtMaCQT.Text.ToString.Trim).Ma_CQThu = "" Then
                txtMaCQT.Focus()
                clsCommon.ShowMessageBox(Me, "Mã cơ quan thu chưa tồn tại trong hệ thống.")
                Exit Sub
            End If
        End If
        If txtTenTK.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên tài khoản.")
            txtTenTK.Focus()
            Exit Sub
        End If
        Dim infTK As New TaiKhoan.infTaiKhoan()
        Try
            infTK.TK = txtTK.Text.ToString.Trim.Replace(".", "")
            infTK.TEN_TK = txtTenTK.Text.ToString.Trim

            infTK.MA_DBHC = txtMaDBHC.Text.Replace(".", "").Trim()
            infTK.MA_CQTHU = txtMaCQT.Text.ToString.Trim
            infTK.TINH_TRANG = (1 - cboTinh_trang.SelectedIndex).ToString
            infTK.TK_KB_NH = cboTK_KB_NH.SelectedValue.ToString
            infTK.SHKB = cboKhoBac.SelectedValue.ToString
            If Not Request.Params("id") Is Nothing Then
                If objTaiKhoan.Update(infTK) Then
                    Xoa_text()
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                Else
                    clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                    txtTK.Focus()
                End If
            Else
                If objTaiKhoan.Insert(infTK) Then
                    Xoa_text()
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                Else
                    clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                    txtTK.Focus()
                End If
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình cập nhật tài khoản")
        Finally
        End Try
        load_Datagrid()
    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim i As Integer
        Dim strMaTK As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True
        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strMaTK = Trim(dtgdata.Items(i).Cells(0).Text)
                If objTaiKhoan.Delete(strMaTK) Then
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                Else
                    clsCommon.ShowMessageBox(Me, "Dữ liệu đã sử dụng trong chức năng khác, không được xoá!")
                End If
                v_count += 1

            End If
        Next
        Xoa_text()
        load_Datagrid()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub

    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Try
            Dim frm As New Business.BaoCao.infBaoCao
            'Business.Common.mdlSystemVariables.gLoaiBaoCao = Report_Type.DM_TAIKHOAN
            Session("objBaoCao") = frm
            clsCommon.OpenNewWindow(Me, "../../BaoCao/frmShowReports.aspx&BC=" & Report_Type.DM_TAIKHOAN & "", "ManPower")
        Catch ex As Exception
            
        End Try
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        load_Datagrid()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        load_Datagrid()
        cmdTaoMoi.Enabled = False
    End Sub
    Protected Sub dmKhoBac()
        Dim objKhoBac As New KhoBac.daKhoBac()
        Dim ds As DataSet
        ds = objKhoBac.Load("and Tinh_trang=1")
        cboKhoBac.DataSource = ds.Tables(0)
        cboKhoBac.DataValueField = ds.Tables(0).Columns("SHKB").ToString()
        cboKhoBac.DataTextField = ds.Tables(0).Columns("Ten").ToString()
        cboKhoBac.DataBind()
    End Sub
End Class
