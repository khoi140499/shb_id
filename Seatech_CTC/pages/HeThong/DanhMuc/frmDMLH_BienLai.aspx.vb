﻿Imports Business.DanhMuc
Imports System.Data
Imports Business.Common.mdlCommon
Imports Business

Partial Class pages_HeThong_DanhMuc_frmDMLH_BienLai
    Inherits System.Web.UI.Page
    Private mv_objUser As New CTuUser
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Session.IsNewSession Then
            ' Force session to be created;
            ' otherwise the session ID changes on every request.
            Session("ForceSession") = DateTime.Now
        End If
        ' 'Sign' the viewstate with the current session.
        Me.ViewStateUserKey = Session.SessionID
        If Page.EnableViewState Then
            ' Make sure ViewState wasn't passed on the querystring.
            ' This helps prevent one-click attacks.
            If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
                Throw New Exception("Viewstate existed, but not on the form.")
            End If
        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            GetAllData()
            cmdTaoMoi.Enabled = True
            ' txtSHKB.Enabled = True
            If (Not Request.Params("id") Is Nothing) Then
                GetInfoDetail(Business.CTuCommon.SQLInj(Request.Params("id").Trim))
            End If
        End If
    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenKB(ByVal strSHKBM As String) As String
        Dim returnValue As String = ""
        If strSHKBM.Trim <> "" Then
            returnValue = Get_TenKhoBac(strSHKBM)
        End If
        Return returnValue
    End Function
    Protected Sub cmdTaoMoi_Click(sender As Object, e As EventArgs) Handles cmdTaoMoi.Click
        Try
            Dim buOBJ As buTCS_DM_LH_THU_BIENLAI = New buTCS_DM_LH_THU_BIENLAI()
            mv_objUser = New CTuUser(Session.Item("User").ToString())
            Dim Mota As String = ""
            Dim NewValue As String = ""
            Dim Action As String = ""
            Dim regex_code As String = clsCTU.TCS_GETTS("SPECIAL_CHAR")
            If txtMA_KB.Text.Trim <> "" Then
                If clsCommon.checkContansChar(txtMA_KB.Text.Trim, regex_code) Then
                    clsCommon.ShowMessageBox(Me, "Bạn nhập vào Số hiệu kho bạc không đúng - Có ký tự đặc biệt đã qui định.")
                    txtMA_KB.Focus()
                    Exit Sub
                End If
            Else
                clsCommon.ShowMessageBox(Me, "Bạn nhập vào Số hiệu kho bạc.")
                txtMA_KB.Focus()
                Exit Sub
            End If

            If txtMA_LH.Text.ToString.Trim.Length <= 0 Then
                clsCommon.ShowMessageBox(Me, "Bạn nhập vào mã loại hình.")
                txtMA_LH.Focus()
                Exit Sub
            End If

            If txtTEN_LH.Text.ToString.Trim = "" Then
                clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên loại hình thu.")
                txtTEN_LH.Focus()
                Exit Sub
            End If


            Dim strErr As String = ""
            Try
                If Not Request.Params("id") Is Nothing Then
                    'update
                    strErr = buOBJ.UPDATE_DM_LHTHU(txtMA_LH.Text.Trim, txtTEN_LH.Text.Trim, txtMA_KB.Text.Trim, txtTK_NSNN.Text.Trim, txtMA_CHUONG.Text.Trim, txtMA_TMUC.Text.Trim, txtGhiChu.Text.Trim, mv_objUser.Ten_DN, txtIDSE.Text.Trim)
                    If strErr.Equals("0") Then
                        clsCommon.ShowMessageBox(Me, "Cập nhật dữ liệu thành công!")
                    End If
                Else
                    'insert
                    strErr = buOBJ.INSERT_DM_LHTHU(txtMA_LH.Text.Trim, txtTEN_LH.Text.Trim, txtMA_KB.Text.Trim, txtTK_NSNN.Text.Trim, txtMA_CHUONG.Text.Trim, txtMA_TMUC.Text.Trim, txtGhiChu.Text.Trim, mv_objUser.Ten_DN)
                    If strErr.Equals("0") Then
                        clsCommon.ShowMessageBox(Me, "Thêm mới dữ liệu thành công!")
                    End If
                End If
            Catch ex As Exception
                strErr = ex.Message
                clsCommon.ShowMessageBox(Me, "Lỗi khi cập nhật danh mục!")
            End Try
            If strErr <> "0" Then
                clsCommon.ShowMessageBox(Me, "Lỗi khi cập nhật danh mục!")
            End If

            Reset()
            GetAllData()
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    Private Sub GetAllData()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Dim buOBJ As buTCS_DM_LH_THU_BIENLAI = New buTCS_DM_LH_THU_BIENLAI()
        Try
            If txtMA_LH.Text.Trim <> "" Then
                WhereClause += " AND a.ma_lh like '%" + FilterSQLi(txtMA_LH.Text.Trim) + "%'"
            End If
            If txtMA_KB.Text.Trim <> "" Then
                WhereClause += " AND upper(a.shkb) like upper('%" + FilterSQLi(txtMA_KB.Text.Trim) + "%')"
            End If
            If txtTK_NSNN.Text.Trim <> "" Then
                WhereClause += " AND upper(a.ma_tkns) like upper('%" + FilterSQLi(txtTK_NSNN.Text.Trim) + "%')"
            End If
            If txtMA_CHUONG.Text.Trim <> "" Then
                WhereClause += " AND upper(a.ma_chuong) like upper('%" + FilterSQLi(txtMA_CHUONG.Text.Trim) + "%')"
            End If
            If txtMA_TMUC.Text.Trim <> "" Then
                WhereClause += " AND upper(a.ma_tmuc) like upper ('%" + FilterSQLi(txtMA_TMUC.Text.Trim) + "%')"
            End If

            ds = buOBJ.Load_DM_LOAIHINH_THU_BIENLAI(WhereClause)
            dtgdata.DataSource = ds.Tables(0)
            dtgdata.DataBind()

            If Not IsEmptyDataSet(ds) Then
            Else
                clsCommon.ShowMessageBox(Me, "Hệ thống không tìm thấy kết quả thỏa mãn điều kiện tra cứu")
            End If
        Catch ex As Exception
            Dim yyy As String = ex.Message
        End Try
    End Sub
    Private Sub GetInfoDetail(ByVal id As String)
        Dim ds As DataSet
        Dim buOBJ As buTCS_DM_LH_THU_BIENLAI = New buTCS_DM_LH_THU_BIENLAI()
        ds = buOBJ.GetInfoDetail(" and a.ID = '" & Business.CTuCommon.SQLInj(id) & "'")
        If ds.Tables(0).Rows.Count > 0 Then
            'ma_lh,ten_lh,shkb,ma_chuong,ma_tmuc,ma_tkns,ghi_chu,mdate,maker
            txtMA_LH.Text = ds.Tables(0).Rows(0)("ma_lh").ToString
            txtTEN_LH.Text = ds.Tables(0).Rows(0)("ten_lh").ToString
            txtMA_KB.Text = ds.Tables(0).Rows(0)("shkb").ToString
            txtTen_KB.Value = Business.Common.mdlCommon.Get_TenKhoBac(ds.Tables(0).Rows(0)("shkb").ToString)
            txtMA_CHUONG.Text = ds.Tables(0).Rows(0)("ma_chuong").ToString
            txtMA_TMUC.Text = ds.Tables(0).Rows(0)("ma_tmuc").ToString
            txtTK_NSNN.Text = ds.Tables(0).Rows(0)("ma_tkns").ToString
            txtGhiChu.Text = ds.Tables(0).Rows(0)("ghi_chu").ToString
            txtIDSE.Text = ds.Tables(0).Rows(0)("ID").ToString
            txtMDate.Value = ds.Tables(0).Rows(0)("MDATE").ToString
            txtMAKER.Value = ds.Tables(0).Rows(0)("MAKER").ToString
        End If
    End Sub
    Protected Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        'Try
        '    Dim buOBJ As buTCS_DM_LH_THU_BIENLAI = New buTCS_DM_LH_THU_BIENLAI()
        '    Dim ds As DataSet = buOBJ.Search_DM_LOAIHINH_THU_BIENLAI(txtMA_LH.Text.Trim(), txtTEN_LH.Text.Trim(), txtMA_KB.Text.Trim(), txtTK_NSNN.Text.Trim(), txtMA_TMUC.Text.Trim())
        '    If Not ds Is Nothing Then
        '        If ds.Tables.Count > 0 Then
        '            If ds.Tables(0).Rows.Count > 0 Then
        '                dtgdata.DataSource = ds.Tables(0)
        '                dtgdata.DataBind()
        '                Return
        '            End If
        '        End If
        '    End If
        '    dtgdata.DataSource = Nothing
        '    dtgdata.DataBind()
        '    lblError.Text = "Không có dữ liệu"
        'Catch ex As Exception

        'End Try
        dtgdata.CurrentPageIndex = 0
        GetAllData()
        cmdTaoMoi.Enabled = False
    End Sub
    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim i_char As Integer = confirmValue.LastIndexOf(",")
        i_char = i_char + 1
        confirmValue = confirmValue.Substring(i_char, confirmValue.Length - i_char)
        If confirmValue = "Có" Then
        Else
            Exit Sub
        End If
        Dim i As Integer
        Dim strID As String

        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True
        Dim buOBJ As buTCS_DM_LH_THU_BIENLAI = New buTCS_DM_LH_THU_BIENLAI()
        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(dtgdata.Items(i).Cells(0).Text)
                buOBJ.DELETE_DM_LHTHU(strID)
            End If
            v_count = v_count + 1
        Next

        ClearFrom()
        GetAllData()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub
    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        Try
            GetAllData()
            dtgdata.CurrentPageIndex = e.NewPageIndex
            dtgdata.DataBind()
        Catch ex As Exception
            dtgdata.CurrentPageIndex = 0
            dtgdata.DataBind()
        End Try
    End Sub
    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        'LogApp.AddDebug("cmdCancel_Click", "Lam moi DM NH truc tiep gian tiep")
        ClearFrom()

        Response.Redirect("~/pages/HeThong/DanhMuc/frmDMLH_BienLai.aspx")
    End Sub
    Private Sub ClearFrom()
        txtMA_LH.Text = ""
        txtTEN_LH.Text = ""
        txtMA_KB.Text = ""
        txtTen_KB.Value = ""
        txtTK_NSNN.Text = ""
        txtMA_CHUONG.Text = ""
        txtMA_TMUC.Text = ""
        txtGhiChu.Text = ""
        txtMDate.Value = ""
        txtMAKER.Value = ""
        txtIDSE.Text = ""
        cmdTaoMoi.Enabled = True

    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
End Class
