﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmDMNganHang_CITAD.aspx.vb" Inherits="pages_HeThong_DanhMuc_frmDMNganHang_citad" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <script language="javascript" type="text/javascript">
        function ShowLov(strType) {
            if (strType == "SHKB") return FindDanhMuc('KhoBac', '<%=txtSHKB.ClientID %>', '<%=txtTenKB.ClientID%>', '<%=txtSHKB.ClientID %>');
}
function FindDanhMuc(strPage, txtID, txtTitle, txtFocus) {
    var strSHKB;
    var returnValue;
    if (document.getElementById('<%=txtSHKB.ClientID %>').value.trim().length > 0) {
            strSHKB = document.getElementById('<%=txtSHKB.ClientID %>').value;
            returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&SHKB=" + strSHKB + "&initParam=" + $get(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;", "_new");
        }
        else {
            returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&initParam=" + document.getElementById(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;", "_new");
        }
        document.getElementById(txtID).value = returnValue["ID"];
        document.getElementById(txtTitle).value = returnValue["Title"];

    }

    // function FindDanhMuc(strPage,txtID,txtFocus) {
    //     var returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&initParam=" + document.getElementById(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;","_new");
    //     if (document.all) {
    //         if (returnValue != null) {
    //             document.getElementById(txtID).value = returnValue.ID;
    //            if (txtFocus!=null){
    //               document.getElementById(txtFocus).focus();
    //          }
    //     }
    // }
    // }
    function jsGet_TenKB() {
        var txt = document.getElementById('<%=txtSHKB.ClientID %>').value;
        //txt.value ='ok';
        PageMethods.GetTenKB(txt, get_TenKB_Complete, get_TenKB__Error)
    }

    function get_TenKB_Complete(result, methodname) {
        var txt = document.getElementById('<%=txtTenKB.ClientID %>');
        if (result.length > 0 && result.toString() != 'Null') {
            txt.value = result;
        }
        else {
            txt.value = '-Số hiệu kho bạc không đúng-';
        }
    }

    function get_TenKB__Error(error, context, methodname) {
        var txt = document.getElementById('<%=txtTenKB.ClientID %>');
        txt.value = '';
    }
    </script>
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">DANH MỤC NGÂN HÀNG TRỰC TIẾP GIÁN TIẾP </asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <asp:HiddenField ID="hdfMa_Nhom" runat="server" />
            <asp:HiddenField ID="hdfTenDN" runat="server" />
        </tr>

        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%" class="form_input">
                    <tr align="left">
                        <td class="form_label" style="width: 18%">
                            <asp:Label ID="Label2" runat="server" CssClass="label">Số hiệu kho bạc</asp:Label>
                        </td>
                        <td class="form_control" style="width: 30%">
                            <asp:TextBox ID="txtSHKB" runat="server" MaxLength="4" Style="background: Aqua"
                                onblur='jsGet_TenKB();' Width="100%" CssClass="inputflat" onkeypress="if (event.keyCode==13){ShowLov('SHKB');}"></asp:TextBox>
                        </td>

                        <td class="form_control" colspan="2">
                            <asp:TextBox ID="txtTenKB" runat="server" CssClass="inputflat" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label1" runat="server" CssClass="label">Mã NH TH</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMa_NH_TH" runat="server" MaxLength="8" Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>

                        <td class="form_control" colspan="2">
                            <asp:TextBox ID="txtTen_NH_TH" runat="server" MaxLength="200" Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblUserNameCaption" runat="server" CssClass="label">Mã NH TT</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMA_NH" runat="server" MaxLength="8" Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>

                        <td class="form_control" colspan="2">
                            <asp:TextBox ID="txtTEN_NH" runat="server" MaxLength="200" Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left" style="display: none">
                        <td class="form_control">
                            <asp:TextBox ID="txtIDSE" runat="server" MaxLength="200" Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left" style="display: none">
                        <td class="form_label">
                            <asp:Label ID="Label3" runat="server" CssClass="label">Tình trạng kho bạc</asp:Label>
                        </td>
                        <td class="form_control" colspan="2">
                            <asp:DropDownList ID="ddlTrangThai" runat="server">
                                <asp:ListItem Text="Liên ngân hàng" Value="0" Selected></asp:ListItem>
                                <asp:ListItem Text="Ngân hàng chuyên thu" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="left" id="Tr1" runat="server">
                        <td class="form_label">
                            <asp:Label ID="Label6" CssClass="label" runat="server">Thông tin dữ liệu</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID="drpDuLieu" runat="server" CssClass="inputflat" Width="95%">
                                <asp:ListItem Text="Dữ liệu hiện tại" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Dữ liệu thay đổi(thêm/sửa/xóa)" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="form_label" style="width: 18%;">
                            <asp:Label ID="Label7" CssClass="label" runat="server">Trạng thái xử lý</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID="drpTT_XU_LY" runat="server" CssClass="inputflat" Width="95%">
                                <asp:ListItem Text="Tất cả" Value=""></asp:ListItem>
                                <asp:ListItem Text="Chờ duyệt" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Đã duyệt" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Từ chối" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                     <tr align="left" id="Tr2" runat="server">
                        <td class="form_label" style="width: 18%;">
                            <asp:Label ID="Label5" CssClass="label" runat="server">Kho bạc tại SHB</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID="drpTaiSHB" runat="server" CssClass="inputflat" Width="95%">
                                <asp:ListItem Text="Tất cả" Value=""></asp:ListItem>
                                <asp:ListItem Text="Có" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Không" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdSearch" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm" UseSubmitBehavior="false"></asp:Button>
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới" UseSubmitBehavior="false"></asp:Button>
                <asp:Button ID="cmdTaoMoi" runat="server" CssClass="ButtonCommand" Text="Ghi" UseSubmitBehavior="false"></asp:Button>
                <asp:Button ID="cmdDeleteUser" runat="server" CssClass="ButtonCommand" Text="Xóa" UseSubmitBehavior="false"></asp:Button>
                <asp:Button ID="cmdIn" Visible="false" Enabled="false" runat="server" CssClass="ButtonCommand" Text="In" UseSubmitBehavior="false"></asp:Button>
                <asp:Button ID="btnDuyet" runat="server" CssClass="ButtonCommand" Text="Duyệt" UseSubmitBehavior="false"></asp:Button>
                <asp:Button ID="btnTuChoi" runat="server" CssClass="ButtonCommand" Text="Từ chối" UseSubmitBehavior="false"></asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True" PageSize='15'>
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="SHKB" HeaderText="SHKB" Visible="false">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="SHKB">
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SHKB") %>' NavigateUrl='<%#"~/pages/HeThong/DanhMuc/frmDMNganHang_CITAD.aspx?id=" & DataBinder.Eval(Container.DataItem, "SHKB") & "&mgt=" & DataBinder.Eval(Container.DataItem, "MA_GIANTIEP") & "&mtt=" & DataBinder.Eval(Container.DataItem, "MA_TRUCTIEP")  & "&idse=" & DataBinder.Eval(Container.DataItem, "ID")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="MA_GIANTIEP" HeaderText="Mã NH thụ hưởng">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TEN_GIANTIEP" HeaderText="Tên NH thụ hưởng">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_TRUCTIEP" HeaderText="Mã NH trực tiếp">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="70px"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TEN_TRUCTIEP" HeaderText="Tên NH trực tiếp">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                          <asp:BoundColumn DataField="NH_SHB" HeaderText="NH SHB">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="ID" HeaderText="ID" Visible="False">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle"></PagerStyle>
                </asp:DataGrid><br />
                <asp:DataGrid ID="grd_new" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="HIS_ID" HeaderText="HIS_ID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="type" HeaderText="type" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ThayDoi" HeaderText="Thay đổi" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="SHKB_new" HeaderText="SHKB mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_GIANTIEP_new" HeaderText="Mã NH TH mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="TEN_GIANTIEP_new" HeaderText="Tên NH TH mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_TRUCTIEP_new" HeaderText="Mã NH TT mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="TEN_TRUCTIEP_new" HeaderText="Tên NH TT mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="NH_SHB_new" HeaderText="NH SHB mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="SHKB" HeaderText="SHKB hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_GIANTIEP" HeaderText="Mã NH TH hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="TEN_GIANTIEP" HeaderText="Tên NH TH hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_TRUCTIEP" HeaderText="Mã NH TT hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="TEN_TRUCTIEP" HeaderText="Tên NH TT hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="NH_SHB" HeaderText="NH SHB hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tt_xu_ly" HeaderText="Trạng thái xử lý" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="xu_ly" HeaderText="Trạng thái xử lý"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ID" HeaderText="ID" Visible="false"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>
