﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmDMLH_BienLai.aspx.vb" Inherits="pages_HeThong_DanhMuc_frmDMLH_BienLai" Title="Danh mục loại hình thu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .tbl {
        }
            .tbl td {
                padding:3px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <script language="javascript" type="text/javascript">
        function ShowLov(strType) {
            if (strType == "SHKB") return FindDanhMuc('KhoBac', '<%=txtMA_KB.ClientID%>', '<%=txtTen_KB.ClientID%>', '<%=txtMA_KB.ClientID%>');
        }
        function FindDanhMuc(strPage, txtID, txtTitle, txtFocus) {
            var strSHKB;
            var returnValue;
            if (document.getElementById('<%=txtMA_KB.ClientID%>').value.trim().length > 0) {
            strSHKB = document.getElementById('<%=txtMA_KB.ClientID%>').value;
            returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&SHKB=" + strSHKB + "&initParam=" + $get(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;", "_new");
        }
        else {
            returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&initParam=" + document.getElementById(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;", "_new");
        }
        document.getElementById(txtID).value = returnValue["ID"];
        document.getElementById(txtTitle).value = returnValue["Title"];

    }

    function jsGet_TenKB() {
        var txt = document.getElementById('<%=txtMA_KB.ClientID%>').value;
        //txt.value ='ok';
        PageMethods.GetTenKB(txt, get_TenKB_Complete, get_TenKB__Error)
    }

    function get_TenKB_Complete(result, methodname) {
        var txt = document.getElementById('<%=txtTen_KB.ClientID%>');
    if (result.length > 0 && result.toString() != 'Null') {
        txt.value = result;
    }
    else {
        txt.value = '-Số hiệu kho bạc không đúng-';
    }
}

function get_TenKB__Error(error, context, methodname) {
    var txt = document.getElementById('<%=txtTen_KB.ClientID%>');
        txt.value = '';
    }
    function XacNhan() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("Bạn có chắc chắn muốn xóa nội dung này?")) {
            confirm_value.value = "Có";
        } else {
            confirm_value.value = "Không";
        }
        document.forms[0].appendChild(confirm_value);
    }
</script>
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">DANH MỤC LOẠI HÌNH BIÊN LAI THU</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <input type="hidden" runat="server" id="Hidden1" />
        </tr>
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="2" cellspacing="2" border="0" width="100%"
                    class="tbl">
                    <tr align="left">
                        <td style="width: 20%;" class="form_label">
                           Mã loai hình
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMA_LH" runat="server" MaxLength="3" Width="30%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                            </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                           Tên loại hình
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtTEN_LH" runat="server" MaxLength="200" Width="50%" CssClass="inputflat"
                                TabIndex="2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                          Mã kho bạc
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMA_KB" runat="server" MaxLength="4" style="background: Aqua" 
                             onblur='jsGet_TenKB();' Width="100%"   CssClass="inputflat"  onkeypress="if (event.keyCode==13){ShowLov('SHKB');}"></asp:TextBox>
                             <input type="text" runat="server"  id="txtTen_KB" style="width:70%" disabled="disabled"/>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                          Tài khoản NSNN
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtTK_NSNN" runat="server" MaxLength="4" Width="20%" CssClass="inputflat"
                                TabIndex="3"></asp:TextBox>
                        </td>
                    </tr>
                     <tr align="left">
                        <td class="form_label">
                          Mã Chương
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMA_CHUONG" runat="server" MaxLength="3" Width="20%" CssClass="inputflat"
                                TabIndex="3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                          Mã Tiểu mục
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMA_TMUC" runat="server" MaxLength="4" Width="20%" CssClass="inputflat"
                                TabIndex="3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            Ghi chú
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtGhiChu" runat="server" MaxLength="200" Width="90%" CssClass="inputflat"
                                TabIndex="5"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                          Thời gian/ người sửa
                        </td>
                        <td class="form_control">
                           <input type="text" runat="server"  id="txtMDate" style="width:30%" disabled="disabled"/>
                             <input type="text" runat="server"  id="txtMAKER" style="width:60%" disabled="disabled"/>
                        </td>
                    </tr>
                    <tr align="left" style="display:none">
                        <td class="form_control">
                            <asp:TextBox ID="txtIDSE" runat="server" MaxLength="200" Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr> 
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdSearch" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm">
                </asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới">
                </asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdTaoMoi" runat="server" CssClass="ButtonCommand" Text="Ghi" TabIndex="5">
                </asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdDeleteUser" runat="server" CssClass="ButtonCommand" Text="Xóa" OnClientClick="XacNhan();">
                </asp:Button>&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" AllowPaging="True" BorderColor="#989898" CssClass="grid_data">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                         <asp:BoundColumn DataField="ID" HeaderText="ID" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="ID" >
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ID")%>'
                                    NavigateUrl='<%#"~/pages/HeThong/DanhMuc/frmDMLH_BienLai.aspx?ID=" & DataBinder.Eval(Container.DataItem, "ID")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="MA_LH" HeaderText="Mã loại hình" >
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TEN_LH" HeaderText="Tên loại hình">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SHKB" HeaderText="Mã Kho bạc">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_TKNS" HeaderText="TK ngân sách">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                         <asp:BoundColumn DataField="MDATE" HeaderText="Ngày">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MAKER" HeaderText="User" Visible="false">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
            </td>
        </tr>
    </table>
</asp:Content>
