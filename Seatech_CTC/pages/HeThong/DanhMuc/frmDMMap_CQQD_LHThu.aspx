﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02_1.master" AutoEventWireup="false" CodeFile="frmDMMap_CQQD_LHThu.aspx.vb" Inherits="pages_HeThong_DanhMuc_frmDMMap_CQQD_LHThu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_1_MainContent" Runat="Server" >
<script language="javascript" type="text/javascript">
    function ShowLov(strType) {
        if (strType == "SHKB") return FindDanhMuc('KhoBac', '<%=txtSHKB.ClientID %>', '<%=txtTenKB.ClientID%>', '<%=txtSHKB.ClientID %>');
    }
    function FindDanhMuc(strPage, txtID, txtTitle, txtFocus) {
        var strSHKB;
        var returnValue;
        if (document.getElementById('<%=txtSHKB.ClientID %>').value.trim().length > 0) {
            strSHKB = document.getElementById('<%=txtSHKB.ClientID %>').value;
            returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&SHKB=" + strSHKB + "&initParam=" + $get(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;", "_new");
        }
        else {
            returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&initParam=" + document.getElementById(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;", "_new");
        }
        document.getElementById(txtID).value = returnValue["ID"];
        document.getElementById(txtTitle).value = returnValue["Title"];

    }

    function jsGet_TenKB() {
        var txt = document.getElementById('<%=txtSHKB.ClientID %>').value;
    //txt.value ='ok';
    PageMethods.GetTenKB(txt, get_TenKB_Complete, get_TenKB__Error)
}

function get_TenKB_Complete(result, methodname) {
    var txt = document.getElementById('<%=txtTenKB.ClientID %>');
        if (result.length > 0 && result.toString() != 'Null') {
            txt.value = result;
        }
        else {
            txt.value = '-Số hiệu kho bạc không đúng-';
        }
    }

    function get_TenKB__Error(error, context, methodname) {
        var txt = document.getElementById('<%=txtTenKB.ClientID %>');
        txt.value = '';
    }
    function XacNhan() {
        var confirm_value = document.createElement("INPUT");
        confirm_value.type = "hidden";
        confirm_value.name = "confirm_value";
        if (confirm("Bạn có chắc chắn muốn xóa nội dung này?")) {
            confirm_value.value = "Có";
        } else {
            confirm_value.value = "Không";
        }
        document.forms[0].appendChild(confirm_value);
    }
</script>
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%" >
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">DANH MỤC MAP CQQD - LOẠI HÌNH THU</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <input type="hidden" runat="server" id="Hidden1" />
        </tr>
        
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="2" cellspacing="2" border="0" width="100%" class="form_input">
                 <tr align="left">
                        <td class="form_label" style="width:15%">
                            <asp:Label ID="Label2" runat="server" CssClass="">Số hiệu kho bạc</asp:Label>
                        </td>
                        <td class="form_control" style="width:30%">
                            <asp:TextBox ID="txtSHKB"  runat="server" MaxLength="4" style="background: Aqua" 
                             onblur='jsGet_TenKB();' Width="100%"   CssClass="inputflat"  onkeypress="if (event.keyCode==13){ShowLov('SHKB');}" ></asp:TextBox>
                        </td>
                        
                        <td class="form_control">
                            <asp:TextBox ID="txtTenKB" runat="server" CssClass="inputflat"  Width="100%" ReadOnly="true" ></asp:TextBox>
                        </td>
                    </tr>
                     <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label1" runat="server" CssClass="">Mã CQQD</asp:Label>
                        </td>
                        <td class="form_control" >
                            <asp:TextBox ID="txtMa_CQQD" runat="server" MaxLength="30"   Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>
                   
                        <td class="form_control">
                            <asp:TextBox ID="txtTen_CQQD" runat="server" MaxLength="200" Width="100%" CssClass="inputflat" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                 <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label3" runat="server" CssClass="">Mã LH Thu</asp:Label>
                        </td>
                        <td class="form_control" >
                            <asp:TextBox ID="txtMA_LHThu" runat="server" MaxLength="5"   Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtTEN_LHThu" runat="server" MaxLength="200" Width="100%" CssClass="inputflat" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                     <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label4" runat="server" CssClass="">Mã cơ quan thu</asp:Label>
                        </td>
                        <td class="form_control" >
                            <asp:TextBox ID="txtMaCQThu" runat="server" MaxLength="7"   Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtTenCQThu" runat="server" MaxLength="200" Width="100%" CssClass="inputflat" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                     <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label6" runat="server" CssClass="">Mã ĐBHC</asp:Label>
                        </td>
                        <td class="form_control" >
                            <asp:TextBox ID="txtMaDBHC" runat="server" MaxLength="5"   Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                     <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label5" runat="server" CssClass="">TK NSNN/Chương</asp:Label>
                        </td>
                        <td class="form_control" >
                            <asp:TextBox ID="txtTKNSNN" runat="server" MaxLength="4"   Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtChuong" runat="server" MaxLength="3" Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                     <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label7" runat="server" CssClass="">Mã NDKT</asp:Label>
                        </td>
                        <td class="form_control" >
                            <asp:TextBox ID="txtMa_NDKT" runat="server" MaxLength="4"   Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                     <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label8" runat="server" CssClass="">Mã Quỹ</asp:Label>
                        </td>
                        <td class="form_control" >
                            <asp:TextBox ID="txtMaQuy" runat="server" MaxLength="2"   Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>  
                     <tr align="left" style="display:none">
                        <td class="form_control">
                            <asp:TextBox ID="txtIDSE" runat="server" MaxLength="200" Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr> 
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdSearch" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm" UseSubmitBehavior="false">
                </asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới" UseSubmitBehavior="false">
                </asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdTaoMoi" runat="server" CssClass="ButtonCommand" Text="Ghi" UseSubmitBehavior="false">
                </asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdDeleteUser" runat="server" CssClass="ButtonCommand" Text="Xóa" UseSubmitBehavior="false" OnClientClick="XacNhan();">
                </asp:Button>&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
              <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                         <asp:BoundColumn DataField="ID" HeaderText="ID" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="ID" >
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ID")%>'
                                    NavigateUrl='<%#"~/pages/HeThong/DanhMuc/frmDMMap_CQQD_LHThu.aspx?ID=" & DataBinder.Eval(Container.DataItem, "ID")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="SHKB" HeaderText="Số hiệu kho bạc">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_CQQD" HeaderText="Mã cơ quan quyết định">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_LH" HeaderText="Mã loại hình">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_QUY" HeaderText="Mã quỹ">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                         <asp:BoundColumn DataField="TK_NS" HeaderText="Tài khoản thu ngân sách">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                         <asp:BoundColumn DataField="MA_CHUONG" HeaderText="Mã chương">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_NDKT" HeaderText="Mã NDKT">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_DBHC" HeaderText="Mã DBHC">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_DVSDNS" HeaderText="Mã DVSDNS">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="tinh_trang" HeaderText="Tình trạng" Visible="false">
                            <HeaderStyle Width="120px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
            </td>
        </tr>
    </table>
</asp:Content>