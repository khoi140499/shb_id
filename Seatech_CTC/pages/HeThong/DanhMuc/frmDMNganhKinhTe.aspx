﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false" 
CodeFile="frmDMNganhKinhTe.aspx.vb" Inherits="pages_HeThong_DanhMuc_frmDMNganhKinhTe" title="Danh mục ngành kinh tế" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" Runat="Server">
<table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">DANH MỤC NGÀNH KINH TẾ</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <input type="hidden" runat="server" id="Hidden1" />
        </tr>
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%"
                    class="form_input">
                    <tr align="left">
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label3" runat="server" CssClass="label">Mã ngành</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMaNganh" runat="server" MaxLength="2" Width="30%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblUserNameCaption" runat="server" CssClass="label">Tên ngành</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtTenNganh" runat="server" MaxLength="50" Width="50%" CssClass="inputflat"
                                TabIndex="2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblPasswordCaption" CssClass="label" runat="server">Trạng thái</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID="cboTinh_trang" runat="server" CssClass="inputflat">
                                <asp:ListItem Text="Đang hoạt động" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Ngừng hoạt động" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label2" runat="server" CssClass="label">Ghi chú</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtGhiChu" runat="server" MaxLength="50" Width="50%" CssClass="inputflat"
                                TabIndex="2"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdSearch" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm">
                </asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới">
                </asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdTaoMoi" runat="server" CssClass="ButtonCommand" Text="Ghi" TabIndex="5">
                </asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdDeleteUser" runat="server" CssClass="ButtonCommand" Text="Xóa">
                </asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdIn" runat="server" Enabled="false" CssClass="ButtonCommand" Text="In" Visible="false">
                </asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <%--<div style="vertical-align :top; height: 300px; overflow: auto; width: 100%;">--%>
                <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    
                    <Columns>
                     <asp:BoundColumn DataField="LKH_ID" HeaderText="ID" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"  Width="0"></ItemStyle>
                        </asp:BoundColumn>
                         <asp:BoundColumn DataField="MA_KHOAN" HeaderText="Mã ngành" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Mã ngành" HeaderStyle-Width="80px">
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MA_KHOAN") %>'
                                    NavigateUrl='<%#"~/pages/HeThong/DanhMuc/frmDMNganhKinhTe.aspx?ID=" & DataBinder.Eval(Container.DataItem, "LKH_ID") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>

<HeaderStyle Width="80px"></HeaderStyle>
                        </asp:TemplateColumn>
                       
                        <asp:BoundColumn DataField="Ten" HeaderText="Tên ngành">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ghi_chu" HeaderText="Ghi chú">
                            <HeaderStyle Width="120px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="tinh_trang" HeaderText="Tình trạng">
                            <HeaderStyle Width="100px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                       
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
                <%--</div>--%>
            </td>
        </tr>
    </table>
</asp:Content>

