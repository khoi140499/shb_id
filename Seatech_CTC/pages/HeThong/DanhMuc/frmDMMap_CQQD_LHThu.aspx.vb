﻿Imports System.Data
Imports Business.Common.mdlCommon
Imports Business.DanhMuc
Imports VBOracleLib
Imports System.IO
Imports iTextSharp.text.pdf
Imports iTextSharp.text

Partial Class pages_HeThong_DanhMuc_frmDMMap_CQQD_LHThu
    Inherits System.Web.UI.Page
    Dim buBL As Business.DanhMuc.buTCS_MAP_CQQD_LHTHU = New buTCS_MAP_CQQD_LHTHU
    Private Sub GetAllData()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtSHKB.Text.Trim <> "" Then
                WhereClause += " AND a.shkb like '%" + FilterSQLi(txtSHKB.Text.Trim) + "%'"
            End If
            If txtMa_CQQD.Text.Trim <> "" Then
                WhereClause += " AND upper(a.ma_cqqd) like upper('%" + FilterSQLi(txtMa_CQQD.Text.Trim) + "%')"
            End If
            If txtMA_LHThu.Text.Trim <> "" Then
                WhereClause += " AND upper(a.ma_lh) like upper('%" + FilterSQLi(txtMA_LHThu.Text.Trim) + "%')"
            End If
            If txtMaCQThu.Text.Trim <> "" Then
                WhereClause += " AND upper(a.ma_dvsdns) like upper('%" + FilterSQLi(txtMaCQThu.Text.Trim) + "%')"
            End If
            If txtMaDBHC.Text.Trim <> "" Then
                WhereClause += " AND upper(a.ma_dbhc) like upper ('%" + FilterSQLi(txtMaDBHC.Text.Trim) + "%')"
            End If
            If txtTKNSNN.Text.Trim <> "" Then
                WhereClause += " AND upper(a.tk_ns) like upper ('%" + FilterSQLi(txtTKNSNN.Text.Trim) + "%')"
            End If
            If txtChuong.Text.Trim <> "" Then
                WhereClause += " AND upper(a.ma_chuong) like upper ('%" + FilterSQLi(txtChuong.Text.Trim) + "%')"
            End If
            If txtMa_NDKT.Text.Trim <> "" Then
                WhereClause += " AND upper(a.ma_ndkt) like upper ('%" + FilterSQLi(txtMa_NDKT.Text.Trim) + "%')"
            End If
            If txtMaQuy.Text.Trim <> "" Then
                WhereClause += " AND upper(a.ma_quy) like upper ('%" + FilterSQLi(txtMaQuy.Text.Trim) + "%')"
            End If
            ds = buBL.Search_DM_TCS_MAP_CQQD_LHTHU(WhereClause)
            dtgdata.DataSource = ds.Tables(0)
            dtgdata.DataBind()

            If Not IsEmptyDataSet(ds) Then
            Else
                clsCommon.ShowMessageBox(Me, "Hệ thống không tìm thấy kết quả thỏa mãn điều kiện tra cứu")
            End If
        Catch ex As Exception
            Dim yyy As String = ex.Message
        End Try
    End Sub

    Private Sub GetInfoDetail(ByVal id As String)
        Dim ds As DataSet
        ds = buBL.GetInfoDetail(" and a.ID = '" & Business.CTuCommon.SQLInj(id) & "'")
        If ds.Tables(0).Rows.Count > 0 Then
            txtMaQuy.Text = ds.Tables(0).Rows(0)("MA_QUY").ToString
            txtTKNSNN.Text = ds.Tables(0).Rows(0)("TK_NS").ToString
            txtChuong.Text = ds.Tables(0).Rows(0)("MA_CHUONG").ToString
            txtMa_NDKT.Text = ds.Tables(0).Rows(0)("MA_NDKT").ToString
            txtMaDBHC.Text = ds.Tables(0).Rows(0)("MA_DBHC").ToString
            txtMA_LHThu.Text = ds.Tables(0).Rows(0)("MA_LH").ToString

            Dim strSHKB As String = ds.Tables(0).Rows(0)("SHKB").ToString
            txtSHKB.Text = strSHKB

            If strSHKB.Length > 0 Then
                txtTenKB.Text = Business.Common.mdlCommon.Get_TenKhoBac(strSHKB)
            End If
            Dim strMaCQQD As String = ds.Tables(0).Rows(0)("MA_CQQD").ToString
            txtMa_CQQD.Text = strMaCQQD
            If strMaCQQD.Length > 0 Then
                txtTen_CQQD.Text = Business.Common.mdlCommon.Get_TenCQQDBienLai(strMaCQQD, strSHKB)
            End If

            If strMaCQQD.Length > 0 And strSHKB.Length > 0 Then
                txtTEN_LHThu.Text = Business.Common.mdlCommon.GetTen_LHThuBienLai(strMaCQQD, strSHKB)
            End If

            Dim ma_cqthu As String = ds.Tables(0).Rows(0)("MA_DVSDNS").ToString
            txtMaCQThu.Text = ds.Tables(0).Rows(0)("MA_DVSDNS").ToString
            If ma_cqthu.Length > 0 Then
                txtTenCQThu.Text = Business.Common.mdlCommon.TCS_GETTENCQTHU(ma_cqthu, "")
            End If

            txtIDSE.Text = ds.Tables(0).Rows(0)("ID").ToString
            'ddlTrangThai.SelectedValue = ds.Tables(0).Rows(0)("TT_KB").ToString
        End If
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Session.IsNewSession Then
            ' Force session to be created;
            ' otherwise the session ID changes on every request.
            Session("ForceSession") = DateTime.Now
        End If
        ' 'Sign' the viewstate with the current session.
        Me.ViewStateUserKey = Session.SessionID
        If Page.EnableViewState Then
            ' Make sure ViewState wasn't passed on the querystring.
            ' This helps prevent one-click attacks.
            If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
                Throw New Exception("Viewstate existed, but not on the form.")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            GetAllData()
            cmdTaoMoi.Enabled = True
            ' txtSHKB.Enabled = True
            If (Not Request.Params("id") Is Nothing) Then
                GetInfoDetail(Business.CTuCommon.SQLInj(Request.Params("id").Trim))
            End If
        End If
        'cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn xóa dữ liệu đã chọn?') ")
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        Try
            GetAllData()
            dtgdata.CurrentPageIndex = e.NewPageIndex
            dtgdata.DataBind()
        Catch ex As Exception
            dtgdata.CurrentPageIndex = 0
            dtgdata.DataBind()
        End Try
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        dtgdata.CurrentPageIndex = 0
        'LogApp.AddDebug("cmdSearch_Click", "Tim kiem DM NH truc tiep gian tiep")
        GetAllData()
        cmdTaoMoi.Enabled = False
    End Sub

    Private Sub ClearFrom()
        txtMaQuy.Text = ""
        txtTKNSNN.Text = ""
        txtChuong.Text = ""
        txtMa_NDKT.Text = ""
        txtMaDBHC.Text = ""
        txtMA_LHThu.Text = ""
        txtSHKB.Text = ""
        txtTenKB.Text = ""
        txtMa_CQQD.Text = ""
        txtTen_CQQD.Text = ""
        txtTEN_LHThu.Text = ""
        txtMaCQThu.Text = ""
        txtTenCQThu.Text = ""

        txtIDSE.Text = ""
        txtSHKB.Enabled = True
        cmdTaoMoi.Enabled = True

    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        'LogApp.AddDebug("cmdCancel_Click", "Lam moi DM NH truc tiep gian tiep")
        ClearFrom()

        Response.Redirect("~/pages/HeThong/DanhMuc/frmDMMap_CQQD_LHThu.aspx")
    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenKB(ByVal strSHKBM As String) As String
        Dim returnValue As String = ""
        If strSHKBM.Trim <> "" Then
            returnValue = Get_TenKhoBac(strSHKBM)
        End If
        Return returnValue
    End Function
    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        Dim Mota As String = ""
        Dim NewValue As String = ""
        Dim Action As String = ""
        Dim regex_code As String = clsCTU.TCS_GETTS("SPECIAL_CHAR")
        If txtSHKB.Text.Trim <> "" Then
            If clsCommon.checkContansChar(txtSHKB.Text.Trim, regex_code) Then
                clsCommon.ShowMessageBox(Me, "Bạn nhập vào Số hiệu kho bạc không đúng - Có ký tự đặc biệt đã qui định.")
                txtSHKB.Focus()
                Exit Sub
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Bạn nhập vào Số hiệu kho bạc.")
            txtSHKB.Focus()
            Exit Sub
        End If

        If txtMa_CQQD.Text.ToString.Trim.Length <= 0 Then
            clsCommon.ShowMessageBox(Me, "Bạn nhập vào mã cơ quan quyết định.")
            txtMa_CQQD.Focus()
            Exit Sub
        End If

        If txtMA_LHThu.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã loại hình thu.")
            txtMA_LHThu.Focus()
            Exit Sub
        End If

        If txtTKNSNN.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tài khoản thu NSNN.")
            txtTKNSNN.Focus()
            Exit Sub
        End If

        If txtMa_NDKT.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Mã nội dung kinh tế.")
            txtMa_NDKT.Focus()
            Exit Sub

        End If
        Dim strErr As String = ""
        Try
            If Not Request.Params("id") Is Nothing Then
                'update
                strErr = buBL.UPDATE_DM_CQQD_LHTHU(txtMa_CQQD.Text.Trim, txtSHKB.Text.Trim, txtMA_LHThu.Text.Trim, txtMaDBHC.Text.Trim, txtMaCQThu.Text.Trim, txtTKNSNN.Text.Trim, txtChuong.Text.Trim, txtMa_NDKT.Text.Trim, txtMaQuy.Text.Trim, txtIDSE.Text.Trim)
                If strErr.Equals("0") Then
                    clsCommon.ShowMessageBox(Me, "Cập nhật dữ liệu thành công!")
                End If
            Else
                'insert
                strErr = buBL.INSERT_DM_CQQD_LHTHU(txtMa_CQQD.Text.Trim, txtSHKB.Text.Trim, txtMA_LHThu.Text.Trim, txtMaDBHC.Text.Trim, txtMaCQThu.Text.Trim, txtTKNSNN.Text.Trim, txtChuong.Text.Trim, txtMa_NDKT.Text.Trim, txtMaQuy.Text.Trim)
                If strErr.Equals("0") Then
                    clsCommon.ShowMessageBox(Me, "Thêm mới dữ liệu thành công!")
                End If
            End If
        Catch ex As Exception
            strErr = ex.Message
            clsCommon.ShowMessageBox(Me, "Lỗi khi cập nhật danh mục!")
        End Try
        If strErr <> "0" Then
            clsCommon.ShowMessageBox(Me, "Lỗi khi cập nhật danh mục!")
        End If

        Reset()
        GetAllData()
    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim i_char As Integer = confirmValue.LastIndexOf(",")
        i_char = i_char + 1
        confirmValue = confirmValue.Substring(i_char, confirmValue.Length - i_char)
        If confirmValue = "Có" Then
        Else
            Exit Sub
        End If
        Dim i As Integer
        Dim strID As String

        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True

        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strID = Trim(dtgdata.Items(i).Cells(0).Text)
                buBL.DELETE_DM_CQQD_LHTHU(strID)
                v_count = v_count + 1
            End If
        Next

        ClearFrom()
        GetAllData()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub


    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
End Class
