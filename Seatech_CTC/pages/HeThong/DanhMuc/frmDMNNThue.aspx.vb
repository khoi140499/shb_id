﻿Imports Business.Common.mdlCommon
Imports Business.DanhMuc
Imports System.Data
Imports VBOracleLib
Imports ProcMegService
Imports CustomsService
Imports System.Diagnostics

Partial Class pages_HeThong_DanhMuc_frmDMNNThue
    Inherits System.Web.UI.Page
    Private objNNT As New NNT.buNNT()

    Private Sub load_Datagrid()
        Dim ds As DataSet
        Dim strSql As String = "SELECT ma_nnt, ngay_cap_mst, ten_nnt, dia_chi, ma_tinh, ma_huyen, " & _
                                " ma_xa, ma_cqthu, ma_chuong, trang_thai, ma_loai_nnt,so_cmt " & _
                                " FROM tcs_dm_nnt WHERE 1 = 1 "
        Dim WhereClause As String = ""
        Try
            If txtMaNNT.Enabled Then
                If txtMaNNT.Text <> "" Then
                    If ddlHTTT.SelectedValue = "MaSoThue" Then
                        WhereClause += " AND ma_nnt = '" + txtMaNNT.Text + "'"
                    Else
                        WhereClause += " AND so_cmt = '" + txtMaNNT.Text + "'"
                    End If
                End If
            End If
            strSql &= WhereClause
            ds = DataAccess.ExecuteReturnDataSet(strSql, CommandType.Text)
            If Not ds Is Nothing Then
                dtgdata.DataSource = ds.Tables(0)
                dtgdata.DataBind()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        '  load_Datagrid()
        Try
            If ddlHTTT.SelectedValue = "MaSoThue" Then
                For i As Integer = 0 To 100
                    ProcMegService.ProMsg.getMSG03(txtMaNNT.Text.Trim)
                Next
            Else
                For i As Integer = 0 To 100
                    ProcMegService.ProMsg.getMSG01(txtMaNNT.Text.Trim)
                Next

            End If
        Catch ex As Exception
            lblError.Text = "Lỗi trong quá trình tra cứu thông tin NNT : Error Messages = " & ex.ToString()
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Lỗi trong quá trình đồng bộ dữ liệu Người nộp thuế từ TC Thuế : Mô tả=" & ex.Message.ToString())
        End Try
        load_Datagrid()
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        load_Datagrid()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub ddlHTTT_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlHTTT.SelectedIndexChanged
        SetLength()
    End Sub
    Protected Sub SetLength()
        If ddlHTTT.SelectedValue = "MaSoThue" Then
            txtMaNNT.MaxLength = 14
            txtMaNNT.Text = ""
        Else
            txtMaNNT.MaxLength = 60
            txtMaNNT.Text = ""
        End If
    End Sub
End Class
