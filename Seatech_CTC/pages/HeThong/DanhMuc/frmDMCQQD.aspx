﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmDMCQQD.aspx.vb" Inherits="pages_HeThong_DanhMuc_frmDMCQQD" Title="Danh mục cơ quan quyết định" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .tbl {
        }
            .tbl td {
                padding:3px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
     <script language="javascript" type="text/javascript">
         function ShowLov(strType) {
             if (strType == "SHKB") return FindDanhMuc('KhoBac', '<%=txtMA_KB.ClientID%>', '<%=txtTen_KB.ClientID%>', '<%=txtMA_KB.ClientID%>');
         }
         function FindDanhMuc(strPage, txtID, txtTitle, txtFocus) {
             var strSHKB;
             var returnValue;
             if (document.getElementById('<%=txtMA_KB.ClientID%>').value.trim().length > 0) {
                strSHKB = document.getElementById('<%=txtMA_KB.ClientID%>').value;
                returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&SHKB=" + strSHKB + "&initParam=" + $get(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;", "_new");
            }
            else {
                returnValue = window.showModalDialog("../../../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&initParam=" + document.getElementById(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;", "_new");
            }
            document.getElementById(txtID).value = returnValue["ID"];
            document.getElementById(txtTitle).value = returnValue["Title"];

        }

        function jsGet_TenKB() {
            var txt = document.getElementById('<%=txtMA_KB.ClientID%>').value;
        //txt.value ='ok';
        PageMethods.GetTenKB(txt, get_TenKB_Complete, get_TenKB__Error)
    }

    function get_TenKB_Complete(result, methodname) {
        var txt = document.getElementById('<%=txtTen_KB.ClientID%>');
        if (result.length > 0 && result.toString() != 'Null') {
            txt.value = result;
        }
        else {
            txt.value = '-Số hiệu kho bạc không đúng-';
        }
    }

    function get_TenKB__Error(error, context, methodname) {
        var txt = document.getElementById('<%=txtTen_KB.ClientID%>');
    txt.value = '';
}
function XacNhan() {
    var confirm_value = document.createElement("INPUT");
    confirm_value.type = "hidden";
    confirm_value.name = "confirm_value";
    if (confirm("Bạn có chắc chắn muốn xóa nội dung này?")) {
        confirm_value.value = "Có";
    } else {
        confirm_value.value = "Không";
    }
    document.forms[0].appendChild(confirm_value);
}
</script>
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">DANH MỤC CƠ QUAN QUYẾT ĐỊNH</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <input type="hidden" runat="server" id="Hidden1" />
        </tr>
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="2" cellspacing="2" border="0" width="100%"
                    class="tbl">
                    <tr align="left">
                        <td style="width: 20%;" class="form_label">
                           Mã CQ quyết định
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMa_CQQD" runat="server" MaxLength="30" Width="30%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                            </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                           Tên CQ Quyết định
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtTen_CQQD" runat="server" MaxLength="200" Width="50%" CssClass="inputflat"
                                TabIndex="2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                          Mã CQ thu
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMa_CQTHU" runat="server" MaxLength="7" Width="20%" CssClass="inputflat"
                                TabIndex="3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                          Mã kho bạc
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMa_KB" runat="server" MaxLength="4" Width="20%" CssClass="inputflat"
                                TabIndex="4"></asp:TextBox>
                             <input type="text" runat="server"  id="txtTen_KB" style="width:70%" disabled="disabled"/>
                        </td>
                    </tr>
                     <tr align="left">
                        <td class="form_label">
                          Mã địa bàn HC
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMa_DBHC" runat="server" MaxLength="5" Width="20%" CssClass="inputflat"
                                TabIndex="3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            Ghi chú
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtGhiChu" runat="server" MaxLength="200" Width="90%" CssClass="inputflat"
                                TabIndex="5"></asp:TextBox>
                        </td>
                    </tr>
                       <tr align="left" id="rowTrangThai" runat="server">
                        <td class="form_label">
                            <asp:Label ID="lblPasswordCaption" CssClass="label" runat="server">Trạng thái</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID="cboTinh_trang" runat="server" CssClass="inputflat" Width="30%">
                                <asp:ListItem Text="Đang hoạt động" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Ngừng hoạt động" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                          Thời gian/ người sửa
                        </td>
                        <td class="form_control">
                           <input type="text" runat="server"  id="txtMDate" style="width:30%"  disabled="disabled"/>
                             <input type="text" runat="server"  id="txtMAKER" style="width:60%" disabled="disabled"/>
                        </td>
                    </tr>
                    <tr align="left" style="display:none">
                        <td class="form_control">
                            <asp:TextBox ID="txtIDSE" runat="server" MaxLength="200" Width="100%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr> 
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdSearch" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm">
                </asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới">
                </asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdTaoMoi" runat="server" CssClass="ButtonCommand" Text="Ghi" TabIndex="5">
                </asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdDeleteUser" runat="server" CssClass="ButtonCommand" Text="Xóa"  OnClientClick="XacNhan();">
                </asp:Button>&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" AllowPaging="True" BorderColor="#989898" CssClass="grid_data">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="ID" HeaderText="ID" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="ID" >
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ID")%>'
                                    NavigateUrl='<%#"~/pages/HeThong/DanhMuc/frmDMCQQD.aspx?ID=" & DataBinder.Eval(Container.DataItem, "ID")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="MA_CQQD" HeaderText="Mã CQQĐ" >
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TEN_CQQD" HeaderText="Tên CQQĐ">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_CQTHU" HeaderText="Mã CQTHU">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MA_KB" HeaderText="Kho Bạc">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                         <asp:BoundColumn DataField="MDATE" HeaderText="Ngày">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MAKER" HeaderText="User">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                         <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
            </td>
        </tr>
    </table>
</asp:Content>
