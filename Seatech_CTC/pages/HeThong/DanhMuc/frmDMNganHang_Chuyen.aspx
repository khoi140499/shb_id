﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmDMNganHang_Chuyen.aspx.vb" Inherits="pages_HeThong_DanhMuc_frmDMNganHang_Chuyen" title="Danh mục Ngân hàng chuyển" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">DANH MỤC CHI NHÁNH NGÂN HÀNG CHUYỂN</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <asp:HiddenField ID="hdfMa_Nhom" runat="server" />
            <asp:HiddenField ID="hdfTenDN" runat="server" />
        </tr>
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%" class="form_input">
                 <tr align="left">
                        <td class="form_label" style="width:15%">
                            <asp:Label ID="lblNHChuyen" runat="server" CssClass="label">Mã Ngân hàng Chuyển</asp:Label>
                        </td>
                        <td class="form_control"  >
                            <asp:TextBox ID="txtMA_NHChuyen" runat="server" MaxLength="8" Width="49%"  CssClass="inputflat"></asp:TextBox>
                        </td>
                        
                    </tr>
                    <tr align="left">
                        <td class="form_label"  style="width:15%">
                            <asp:Label ID="lblTen_NHChuyen" runat="server" CssClass="label">Tên Ngân hàng Chuyển</asp:Label>
                        </td>
                        <td class="form_control" >
                            <asp:TextBox ID="txtTEN_NHChuyen" runat="server" MaxLength="100" Width="49%" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left" id="Tr1" runat="server">
                        <td class="form_label">
                            <asp:Label ID="Label4" CssClass="label" runat="server">Thông tin dữ liệu</asp:Label>
                        </td>
                        <td class="form_control">
                           <asp:DropDownList ID="drpDuLieu" runat="server" CssClass="inputflat" Width="50%">
                                <asp:ListItem Text="Dữ liệu hiện tại" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Dữ liệu thay đổi(thêm/sửa/xóa)" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="left" id="Tr2" runat="server">
                        <td class="form_label">
                            <asp:Label ID="Label5" CssClass="label" runat="server">Trạng thái xử lý</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:DropDownList ID="drpTT_XU_LY" runat="server" CssClass="inputflat" Width="50%">
                                <asp:ListItem Text="Tất cả" Value=""></asp:ListItem>
                                <asp:ListItem Text="Chờ duyệt" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Đã duyệt" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Từ chối" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdSearch" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdTaoMoi" runat="server" CssClass="ButtonCommand" Text="Ghi" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdDeleteUser" runat="server" CssClass="ButtonCommand" Text="Xóa" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="btnDuyet" runat="server" CssClass="ButtonCommand" Text="Duyệt" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="btnTuChoi" runat="server" CssClass="ButtonCommand" Text="Từ chối" UseSubmitBehavior="false"></asp:Button>
        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="ma_nh_chuyen" HeaderText="id" Visible="false">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Mã NH chuyển">
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MA_NH_CHUYEN") %>'
                                    NavigateUrl='<%#"~/pages/HeThong/DanhMuc/frmDMNganHang_Chuyen.aspx?id=" & DataBinder.Eval(Container.DataItem, "ma_nh_chuyen") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="ten_nh_chuyen" HeaderText="Tên NH chuyển">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
                <asp:DataGrid ID="grd_new" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="his_id" HeaderText="his_ID" Visible="true"></asp:BoundColumn>
                        <asp:BoundColumn DataField="type" HeaderText="type" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ThayDoi" HeaderText="Thay đổi" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_nh_chuyen_new" HeaderText="Mã NH chuyển mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ten_nh_chuyen_new" HeaderText="Tên NH chuyển mới"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ma_nh_chuyen" HeaderText="Mã NH chuyển hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ten_nh_chuyen" HeaderText="Tên NH chuyển hiện tại"></asp:BoundColumn>
                        <asp:BoundColumn DataField="tt_xu_ly" HeaderText="Trạng thái xử lý" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="xu_ly" HeaderText="Trạng thái xử lý"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>

