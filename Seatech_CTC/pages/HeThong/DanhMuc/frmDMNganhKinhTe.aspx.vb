﻿Imports Business.Common.mdlCommon
Imports Business.DanhMuc.LoaiKhoan
Imports System.Data
Imports VBOracleLib

Partial Class pages_HeThong_DanhMuc_frmDMNganhKinhTe
    Inherits System.Web.UI.Page
    Dim objNGanhKT As New buLoaiKhoan()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            load_DataGrid()
            cmdTaoMoi.Enabled = True
            If Not Request.Params("id") Is Nothing Then
                txtMaNganh.Enabled = False
                load_Detail(Request.Params("id"))
            End If
        End If
        cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn xóa dữ liệu đã chọn?') ")
    End Sub

    Private Sub load_DataGrid()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtMaNganh.Enabled Then
                If txtMaNganh.Text <> "" Then
                    WhereClause += " AND ma_khoan = '" + txtMaNganh.Text + "'"
                End If
                If txtTenNganh.Text <> "" Then
                    WhereClause += " AND upper(ten) like upper('%" + txtTenNganh.Text + "%')"
                End If
                If txtGhiChu.Text <> "" Then
                    WhereClause += " AND upper(ghi_chu) like upper('%" + txtGhiChu.Text + "%')"
                End If
            End If

            ds = objNGanhKT.Load(WhereClause)
            dtgdata.DataSource = ds.Tables(0)
            dtgdata.DataBind()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub load_Detail(ByVal id As String)
        Dim ds As DataSet
        ds = objNGanhKT.Load(" AND lkh_id = '" + id + "'")
        If ds.Tables(0).Rows.Count > 0 Then
            txtMaNganh.Text = ds.Tables(0).Rows(0)("ma_khoan").ToString
            txtTenNganh.Text = ds.Tables(0).Rows(0)("ten").ToString
            txtGhiChu.Text = ds.Tables(0).Rows(0)("ghi_chu").ToString
            If ds.Tables(0).Rows(0)("tinh_trang").ToString = "Dang hoat dong" Then
                cboTinh_trang.SelectedValue = 1
            Else
                cboTinh_trang.SelectedValue = 0
            End If
        End If
        'For i As Integer = 0 To dtgdata.Items.Count - 1
        '    If dtgdata.Items(i).Cells(0).Text = id Then
        '        txtMaNganh.Text = dtgdata.Items(i).Cells(1).Text.ToString
        '        txtTenNganh.Text = dtgdata.Items(i).Cells(3).Text.ToString
        '        txtGhiChu.Text = dtgdata.Items(i).Cells(4).Text.ToString.Trim
        '        cboTinh_trang.Text = dtgdata.Items(i).Cells(5).Text.ToString
        '    End If
        'Next
    End Sub

    Private Sub Xoa_Text()
        txtGhiChu.Text = ""
        txtMaNganh.Text = ""
        txtTenNganh.Text = ""
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Xoa_Text()
        cmdTaoMoi.Enabled = True
        If Not Request.Params("id") Then
            Response.Redirect("~/pages/HeThong/DanhMuc/frmDMNganhKinhTe.aspx")
        End If
    End Sub

    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        txtMaNganh.Text = RemoveApostrophe(txtMaNganh.Text)
        txtGhiChu.Text = RemoveApostrophe(txtGhiChu.Text)

        If txtMaNganh.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã ngành.")
            txtMaNganh.Focus()
            Exit Sub
        ElseIf txtMaNganh.Text.Trim.Length <> txtMaNganh.MaxLength Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã Ngành có độ dài " & txtMaNganh.MaxLength & " ký tự.")
            txtMaNganh.Focus()
            Exit Sub
            '*****TYNK - Check ky tu dac biet*********19/02/2013
        Else
            If Regex.IsMatch(txtMaNganh.Text.Trim, "^[a-zA-Z0-9]*$") = False Then
                clsCommon.ShowMessageBox(Me, "Chuỗi bạn nhập không đúng - Có ký tự đặc biệt.")
                txtMaNganh.Focus()
                Exit Sub
            End If
            '***************************************************
        End If

        If txtTenNganh.Text.ToString.Trim = "" Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên mã ngành.")
            txtTenNganh.Focus()
            Exit Sub
        End If

        Try
            Dim objInfKT As New infLoaiKhoan

            objInfKT.MA_KHOAN = txtMaNganh.Text.ToString.Trim
            objInfKT.Ten = txtTenNganh.Text.ToString.Trim
            objInfKT.GHICHU = txtGhiChu.Text.ToString.Trim
            objInfKT.TINH_TRANG = (1 - cboTinh_trang.SelectedIndex).ToString
            If Not Request.Params("id") Is Nothing Then
                objInfKT.LKH_ID = Request.Params("id").Trim
                If objNGanhKT.Update(objInfKT) Then
                    Xoa_Text()
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                Else
                    txtMaNganh.Focus()
                End If
            Else
                objInfKT.LKH_ID = getDataKey("TCS_CCH_ID_SEQ.NEXTVAL")
                If objNGanhKT.Insert(objInfKT) Then
                    Xoa_Text()
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
                Else
                    txtMaNganh.Focus()
                    clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
                End If
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Thực hiện không thành công")
        Finally
        End Try
        load_DataGrid()
    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim i As Integer
        Dim strMaChuong As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True
        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strMaChuong = Trim(dtgdata.Items(i).Cells(0).Text)
                If objNGanhKT.Delete(strMaChuong) Then
                    clsCommon.ShowMessageBox(Me, "Thực hiện thành công!")
                Else
                    clsCommon.ShowMessageBox(Me, "Dữ liệu đã sử dụng trong chức năng khác, không được xoá!")
                End If
                v_count += 1

            End If
        Next
        load_DataGrid()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Xoá dữ liệu thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub

    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Try
            Dim frm As New Business.BaoCao.infBaoCao
            'Business.Common.mdlSystemVariables.gLoaiBaoCao = Report_Type.DM_LOAIKHOAN
            Session("objBaoCao") = frm
            clsCommon.OpenNewWindow(Me, "../../BaoCao/frmShowReports.aspx&BC=" & Report_Type.DM_LOAIKHOAN & "", "ManPower")
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        load_DataGrid()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        load_DataGrid()
        cmdTaoMoi.Enabled = False
    End Sub
End Class
