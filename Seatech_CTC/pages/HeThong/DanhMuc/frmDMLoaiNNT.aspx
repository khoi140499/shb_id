﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmDMLoaiNNT.aspx.vb" Inherits="pages_HeThong_DanhMuc_frmDMLoaiNNT"
    Title="Danh mục loại NNT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<script type="text/javascript" language="javascript">
    function XacNhan(msg) {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm(msg)) {
                confirm_value.value = "Có";
            } else {
                confirm_value.value = "Không";
            }
            document.forms[0].appendChild(confirm_value);
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">DANH MỤC LOẠI NGƯỜI NỘP THUẾ</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <input type="hidden" runat="server" id="Hidden1" />
        </tr>
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%"
                    class="form_input">
                    <tr align="left">
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="lblMaLoaiNNT" runat="server" CssClass="label">Mã loại NNT</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMaLoaiNNT" runat="server" MaxLength="20" Width="30%" CssClass="inputflat" TabIndex="1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblTenLoaiNNT" runat="server" CssClass="label">Tên loại NNT</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtTenLoaiNNT" runat="server" MaxLength="100" Width="50%" CssClass="inputflat" TabIndex="2"></asp:TextBox>
                        </td>
                    </tr>
                    
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdSearch" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm" UseSubmitBehavior="false">
                </asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới" UseSubmitBehavior="false">
                </asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdTaoMoi" runat="server" CssClass="ButtonCommand" Text="Ghi" UseSubmitBehavior="false">
                </asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdDeleteUser" runat="server" CssClass="ButtonCommand" Text="Xóa" UseSubmitBehavior="false" OnClientClick="XacNhan('Bạn có chắc chắn muốn xóa nội dung này?')">
                </asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdIn" Enabled="false" runat="server" CssClass="ButtonCommand" Text="In" Visible="false" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="cmdSync" runat="server" Visible="false" CssClass="ButtonCommand" Text="Đồng bộ" UseSubmitBehavior="false" OnClientClick="XacNhan('Bạn có chắc chắn muốn đồng bộ nội dung này?')">
                </asp:Button>
                <asp:Button ID="cmdExport" runat="server" CssClass="ButtonCommand" Text="Export" UseSubmitBehavior="false">
                </asp:Button>
                <asp:Button ID="btnExportPDF" runat="server" CssClass="ButtonCommand" Text="Export PDF" Visible="false" UseSubmitBehavior="false">
                </asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
             <%--<div class="inputflat" style="vertical-align :top; height: 300px; overflow: auto; width: 100%;">--%>
                <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="MA_LOAI_NNT" HeaderText="Mã loại NNT" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Mã loại NNT" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MA_LOAI_NNT") %>'
                                    NavigateUrl='&nbsp;<%#"~/pages/HeThong/DanhMuc/frmDMLoaiNNT.aspx?ID=" & DataBinder.Eval(Container.DataItem, "MA_LOAI_NNT") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle Width="20%"></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TEN_LOAI_NNT" HeaderText="Tên loại NNT">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Center" Mode="NumericPages" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
                <%--</div>--%>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" TabIndex="9" Visible="false" Font-Names="Arial"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="false">
                    <AlternatingRowStyle CssClass="grid_item_alter"></AlternatingRowStyle>
                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                    <PagerStyle CssClass="grid_item" />
                        <Columns>
                            <asp:TemplateField HeaderText="Mã loại NNT">
                                <ItemTemplate>
                                    &nbsp;<asp:Label ID="lblMA_LOAINNT" runat= "server" Text='<%#Eval("MA_LOAI_NNT")%>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Width="120px"></HeaderStyle>
                                <ItemStyle Width="120px" HorizontalAlign="Left" ></ItemStyle>
                             </asp:TemplateField>
                            <asp:BoundField DataField="TEN_LOAI_NNT"  HeaderText="Tên loại NNT" >
                                <HeaderStyle Width="300px"></HeaderStyle>
                                <ItemStyle Width="300px" HorizontalAlign="Left" ></ItemStyle>
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
