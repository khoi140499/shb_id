﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmGroupUsers.aspx.vb" Inherits="pages_HeThong_frmGroupUsers" Title="Thông tin nhóm người sử dụng" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table id="tbl_GroupUser" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">THÔNG TIN NHÓM NGƯỜI SỬ DỤNG</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table id="tbl_GroupUserDtl" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%"
                    class="form_input">
                    <tr align="left">
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="lblMaNhom" runat="server" CssClass="label">Mã nhóm</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMaNhom" runat="server" MaxLength="10" Width="50%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblTenNhom" runat="server" CssClass="label">Tên nhóm</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtTenNhom" runat="server" MaxLength="50" Width="50%" CssClass="inputflat" TabIndex="2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="form_label" style="text-align:left">
                            <asp:Label ID="Label1" runat="server" CssClass="label">Quyền hạn</asp:Label>
                        </td>
                        <td class="form_control" style="text-align:left">
                            <asp:RadioButton runat="server" ForeColor="Black" Text="Maker" ID="rdMaker" GroupName="rdPermission" />
                            <asp:RadioButton runat="server" ForeColor="Black" Text="Checker" ID="rdChecker" GroupName="rdPermission" />
                            <asp:RadioButton runat="server" ForeColor="Black" Text="Viewer" ID="rdViewer" GroupName="rdPermission" />
                        </td>
                    </tr>
                    <tr>
                        <td class="form_label" style="text-align:left">
                            <asp:Label ID="Label2" runat="server" CssClass="label">Trạng thái dữ liệu</asp:Label>
                        </td>
                        <td class="form_control" style="text-align:left">
                            <asp:DropDownList runat="server" ID="drlDataStatus" CssClass="inputflat">
                                <asp:ListItem Text="Dữ liệu hiện tại" Value="CurrentData" Selected="True" />
                                <asp:ListItem Text="Dư liệu thay đổi" Value="ChangeData" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdSearch" runat="server" CssClass="" Text="Tìm kiếm" UseSubmitBehavior="false"></asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdCancel" runat="server" CssClass="" Text="Làm mới" UseSubmitBehavior="false"></asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdTaoMoi" runat="server" CssClass="" Text="Ghi" UseSubmitBehavior="false" Visible="false"></asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdDeleteUser" runat="server" CssClass="" Text="Xóa" UseSubmitBehavior="false" Visible="false"></asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdDuyet" runat="server" CssClass="" Text="Duyệt" UseSubmitBehavior="false" Visible="false"></asp:Button>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#1367B0" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="MA_NHOM" HeaderText="Mã nhóm" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Mã nhóm" HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MA_NHOM") %>'
                                    NavigateUrl='<%#"~/pages/HeThong/frmGroupUsers.aspx?ID=" & DataBinder.Eval(Container.DataItem, "MA_NHOM") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle Width="100px"></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TEN_NHOM" HeaderText="Tên nhóm">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
                <asp:DataGrid ID="dgApproveAction" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#1367B0" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="ACTION_ID" HeaderText="ACTION_ID" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Hành động">
                            <ItemTemplate>
                                <%#GetActonString(Eval("ACTION").ToString())%>
                            </ItemTemplate>
                        </asp:TemplateColumn>

                        <asp:BoundColumn DataField="MA_NHOM" HeaderText="Mã nhóm" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Mã nhóm" HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MA_NHOM") %>'
                                    NavigateUrl='<%#"~/pages/HeThong/frmGroupUsers.aspx?ID=" & DataBinder.Eval(Container.DataItem, "MA_NHOM") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle Width="100px"></HeaderStyle>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TEN_NHOM" HeaderText="Tên nhóm">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>
