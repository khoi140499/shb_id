﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false"
    CodeFile="frmAddNgayNghi.aspx.vb" Inherits="pages_HeThong_frmAddNgayNghi" Title="Quản lý ngày nghỉ" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript">
        function jsTracuu()
        {
            var strngaynghi;
            strngaynghi=document.getElementById("txtNgayNghi").value ;
            
        }
     
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
                for (var k = 0; k <= str.length; k++){
                    if (k == locs[i]){
                    if (str.substring(k, k+1) != delim){
                            str = str.substring(0,k) + delim + str.substring(k,str.length)
                        }
                    }
                }
                }
                textbox.value = str
        }
    </script>
    <script src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../../javascript/DatePicker/jquery.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" runat="Server">
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">THÊM NGÀY NGHỈ HỆ THỐNG</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <input type="hidden" runat="server" id="Hidden1" />
        </tr>
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%"
                    class="form_input">
                    <tr align="left">
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="lblNgayNghi" runat="server" CssClass="label">Chọn ngày</asp:Label>
                        </td>
                        <td class="form_control">
                            <span style="display:block; width:24%; position:relative;vertical-align:middle;">
                                <input type="text" id='txtNgayNghi' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                                onblur="CheckDate(this);" onfocus="this.select()" /></span>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblNN_Ghichu" runat="server" CssClass="label">Ghi chú</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtNN_Ghichu" runat="server" MaxLength="50" Width="50%" CssClass="inputflat"
                                TabIndex="2"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdSearch" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm" UseSubmitBehavior="false">
                </asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới" UseSubmitBehavior="false">
                </asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdGhi" runat="server" CssClass="ButtonCommand" Text="Ghi" TabIndex="5" UseSubmitBehavior="false">
                </asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdDeleteUser" runat="server" CssClass="ButtonCommand" Text="Xóa" UseSubmitBehavior="false">
                </asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdDongBo" runat="server" CssClass="ButtonCommand" Text="Đồng bộ ngày nghỉ" UseSubmitBehavior="false">
                </asp:Button>
                <%--<asp:Button ID="cmdIn" Enabled="false" runat="server" CssClass="ButtonCommand" Text="In"
                    Visible="false"></asp:Button>--%>
            </td>
        </tr>
        <tr>
            <td>
                <input type="hidden" runat="server" id="hdnbox" />
                <input type="hidden" runat="server" id="hdnbox1" />
            </td>
        </tr>
        <tr>
            <td align="center">
                <%--<div style="vertical-align :top; height: 300px; overflow: auto; width: 100%;">--%>
                <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="ngay_nghi" HeaderText="ID" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Ngày nghỉ" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ngay_nghi","{0:dd/MM/yyyy}") %>'
                                    NavigateUrl='<%#"~/pages/HeThong/frmAddNgayNghi.aspx?ID=" & DataBinder.Eval(Container.DataItem, "ngay_nghi") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle Width="20%"></HeaderStyle>
                        </asp:TemplateColumn>
                        <%--<asp:BoundColumn DataField="Ma_DThu" HeaderText="Mã điểm thu">
                            <HeaderStyle Width="50"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" Width="50"></ItemStyle>
                        </asp:BoundColumn>--%>
                        <asp:BoundColumn DataField="ghi_chu" HeaderText="Ghi chú">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid><br />
                <%--</div>--%>
            </td>
        </tr>
    </table>
    <!-- required plugins -->
    <script src="../../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
        });
    </script>		
</asp:Content>
