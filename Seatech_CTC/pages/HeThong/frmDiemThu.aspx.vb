﻿Imports System
Imports System.Drawing
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports Business.HeThong
Imports System.Data
Partial Class pages_HeThong_frmDiemThu
    Inherits System.Web.UI.Page

    Private Sub load_Datagrid()
        Dim ds As New DataSet
        Try

            ds = buUsers.TCS_LST_DM_NHANVIEN()
            dgUserAccount.DataSource = ds.Tables(0)
            dgUserAccount.DataBind()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu")
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdSave.Click, AddressOf cmdSave_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            load_Datagrid()
            load_DiemThu()
            If Request.Params("ID") <> "" Then
                load_dtgChucNang(Request.Params("ID").Trim())
            End If
        End If
    End Sub
    Private Sub load_DiemThu()

        Dim ds As New DataSet
        Dim strSQL As String
        ' Trong trường hợp đang load dữ liệu thì không load quyền
        ' If mblnMove Then Return
        Try

            strSQL = "SELECT  a.MA_DThu,a.TEN FROM TCS_DM_DIEMTHU a " & _
                        " ORDER BY MA_DThu"
            ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            dtgDiemthu.DataSource = ds
            dtgDiemthu.DataBind()

        Catch ex As Exception
            'Lỗi kết nối cơ sở dữ liệu
            clsCommon.ShowMessageBox(Me, "Lỗi kết nối CSDL")
        Finally
            If Not ds Is Nothing Then
                ds.Dispose()
            End If
           
        End Try
    End Sub

    Private Sub load_dtgChucNang(ByVal mstrTmpMa_NV As String)

        Dim ds As New DataSet
        Dim strSQL As String
        ' Trong trường hợp đang load dữ liệu thì không load quyền
        ' If mblnMove Then Return
        Try

            strSQL = "SELECT 1 AS ING, a.MA_DThu,a.TEN FROM TCS_DM_DIEMTHU a,tcs_dm_nhanvien b " & _
                        "where instr( b.ban_lv,a.MA_DThu)>0 and b.ma_nv=" & CInt(mstrTmpMa_NV) & _
                        " UNION " & _
                        "SELECT 0 AS ING, a.MA_DThu,a.TEN FROM TCS_DM_DIEMTHU a,tcs_dm_nhanvien b " & _
                        "where instr( b.ban_lv,a.MA_DThu)<=0  and b.ma_nv=" & CInt(mstrTmpMa_NV) & _
                        " ORDER BY MA_DThu"
            ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            dtgDiemthu.DataSource = ds
            dtgDiemthu.DataBind()
            For i As Integer = 0 To ds.Tables(0).Rows.Count() - 1
                Dim obj As CheckBox
                obj = dtgDiemthu.Items(i).FindControl("chkSelect")
                If ds.Tables(0).Rows(i)("ING") = 1 Then
                    obj.Checked = True
                End If
            Next

        Catch ex As Exception
            'Lỗi kết nối cơ sở dữ liệu
            clsCommon.ShowMessageBox(Me, "Lỗi kết nối CSDL")
        Finally
            If Not ds Is Nothing Then
                ds.Dispose()
            End If
          
        End Try
    End Sub

    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        '-----------------------------------------------------
        ' Mục đích: Cập nhật những thay đổi nhóm NSD cho NSD
        '           cho những dùng được lựa chọn ở listBox
        ' Tham số: 
        ' Giá trị trả về: 
        ' Ngày viết: 16/10/2007
        ' Người viết: Nguyễn Bá Cường
        ' ----------------------------------------------------
        'Dim cnNv As DataAccess
        Dim strItem As String = ""
        Dim strSQL As String
        Dim i As Integer
        Dim obj As CheckBox
        Dim mstrMa_NV As String
        Try
            obj = New CheckBox()
            mstrMa_NV = Request.Params("id")

            For i = 0 To dtgDiemthu.Items.Count - 1
                obj = dtgDiemthu.Items(i).FindControl("chkSelect")
                If obj.Checked Then
                    If strItem <> "" Then strItem = strItem & "/"
                    strItem = strItem & dtgDiemthu.Items(i).Cells(1).Text.Trim
                    If "".Equals(strItem) Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải chọn ít nhất một nhóm.")
                        Exit Sub
                    End If


                    'Kienvt : Thay die thu =ban_lv
                    'strSQL = "Update TCS_DM_NHANVIEN Set MA_DThu= '" & strItem & _
                    '"' where Ma_NV =" & mstrMa_NV

                    strSQL = "Update TCS_DM_NHANVIEN Set Ban_LV= '" & strItem & _
                    "' where Ma_NV =" & mstrMa_NV

                    DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                End If

            Next

            clsCommon.ShowMessageBox(Me, "Cập nhật người sử dụng cho nhóm thành công !")
        Catch ex As Exception
            ' Lỗi xoá dữ liệu không thành công
            clsCommon.ShowMessageBox(Me, gstrLoiCapNhatDL)
            'Finally
            '    If Not cnNv Is Nothing Then
            '        cnNv.Dispose()
            '    End If
        End Try

    End Sub

End Class
