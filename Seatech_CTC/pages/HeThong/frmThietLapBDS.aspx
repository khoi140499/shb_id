﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false"
    CodeFile="frmThietLapBDS.aspx.vb" Inherits="pages_HeThong_frmThietLapBDS" Title="Thiết lập tham số chi nhánh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" runat="Server">
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">THIẾT LẬP THAM SỐ CHI NHÁNH</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            <input type="hidden" runat="server" id="Hidden1" />
        </tr>
        <tr>
            <td align="center">
                <table id="table2" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%"
                    class="form_input">
                    <tr align="left">
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label3" runat="server" CssClass="label">Mã chi nhánh</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtMaCN" runat="server" MaxLength="20" Width="50%" CssClass="inputflat"
                                TabIndex="1"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td style="width: 20%;" class="form_label">
                            <asp:Label ID="Label5" runat="server" CssClass="label">URLBDS</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtURLBDS" runat="server" MaxLength="20" Width="50%" CssClass="inputflat"
                                TabIndex="2"></asp:TextBox>
                        </td>
                    </tr>
                   
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label6" runat="server" CssClass="label">BDS_Servername</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtSERVERNAME" runat="server" MaxLength="50" Width="50%" CssClass="inputflat"
                                TabIndex="4"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label1" runat="server" CssClass="label">BDS_Username</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtUserName" runat="server" MaxLength="50" Width="50%" CssClass="inputflat"
                                TabIndex="4"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label2" runat="server" CssClass="label">BDS_Password</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtPassWord" TextMode="Password" runat="server" MaxLength="50" Width="50%"
                                CssClass="inputflat" TabIndex="4"></asp:TextBox>
                        </td>
                    </tr>
                     <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblUserNameCaption" runat="server" CssClass="label">URLBDSVB</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtURLBDSVB" runat="server" MaxLength="50" Width="50%" CssClass="inputflat"
                                TabIndex="3"></asp:TextBox>
                        </td>
                    </tr>
                     <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label7" runat="server" CssClass="label">BDSVB_SERVERNAME</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtBDSVB_SERVERNAME" runat="server" MaxLength="50" Width="50%" CssClass="inputflat"
                                TabIndex="3"></asp:TextBox>
                        </td>
                    </tr>
                     <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label8" runat="server" CssClass="label">BDSVB_USER_NAME</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtBDSVB_USER_NAME" runat="server" MaxLength="50" Width="50%" CssClass="inputflat"
                                TabIndex="3"></asp:TextBox>
                        </td>
                    </tr>
                     <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="Label9" runat="server" CssClass="label">BDSVB_PASSWORD</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtBDSVB_PASSWORD" TextMode="Password" runat="server" MaxLength="50" Width="50%" CssClass="inputflat"
                                TabIndex="3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left" style="display:none">
                        <td class="form_label">
                            <asp:Label ID="Label4" runat="server" CssClass="label">DBName</asp:Label>
                        </td>
                        <td class="form_control">
                            <asp:TextBox ID="txtDBName" runat="server" MaxLength="50" Width="50%" CssClass="inputflat"
                                TabIndex="4"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới">
                </asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdTaoMoi" runat="server" CssClass="ButtonCommand" Text="Ghi" TabIndex="5">
                </asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdDelete" runat="server" CssClass="ButtonCommand" Text="Xóa"></asp:Button>&nbsp;&nbsp;
                <%--  <asp:Button ID="cmdTimKiem" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm">
                </asp:Button>--%>
            </td>
        </tr>
        <tr>
            <td align="center">
                <%--<div id="mainFrame" style="vertical-align: top; height: 300px; overflow: auto; width: 100%;">--%>
                <asp:DataGrid ID="grid_data" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="99%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="Ma_CN" HeaderText="Ma_CN" Visible="false">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Mã CN" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ma_CN") %>'
                                    NavigateUrl='<%#"~/pages/HeThong/frmThietLapBDS.aspx?ID=" & DataBinder.Eval(Container.DataItem, "Ma_CN") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle Width="20%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="URLBDS" HeaderText="UrlBDS">
                            <HeaderStyle Width="150px"></HeaderStyle>
                            <ItemStyle Width="150px" HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        
                        <asp:BoundColumn DataField="SERVERNAME" HeaderText="BDS_ServerName">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle Width="100px" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="User_Name" HeaderText="BDS_Username">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle Width="100px" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="false" DataField="Password" HeaderText="BDS_Password">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle Width="100px" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="URLBDSVB" HeaderText="UrlBDSVB">
                            <HeaderStyle Width="150px"></HeaderStyle>
                            <ItemStyle Width="150px" HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="BDSVB_SERVERNAME" HeaderText="BDSVB_SERVERNAME">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle Width="100px" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn  DataField="BDSVB_USER_NAME" HeaderText="BDSVB_USER_NAME">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle Width="100px" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="false" DataField="BDSVB_PASSWORD" HeaderText="BDSVB_PASSWORD">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle Width="100px" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DBName" HeaderText="DBName" Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle Width="0px" HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid>
                <%--</div>--%>
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
