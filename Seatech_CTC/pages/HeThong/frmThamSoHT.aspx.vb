﻿Imports System.Data
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports Business.HeThong
Imports Business.Common
Imports VBOracleLib.Security

Partial Class pages_HeThong_frmThamSoHT
    Inherits System.Web.UI.Page
    Private Sub frmThamSoHT_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'AllowEditTSDC()
        If Not IsPostBack Then
            'TCS_FillGrTEN_TSTSDR()
            LoadDDLDiemThu()
            TCS_FillGrTEN_TSTSDC()
        End If
    End Sub   

    Private Sub TCS_FillGrTEN_TSTSDC()
        '--------------------------------------------------------------
        ' Mục đích: Lấy các giá trị tham số đã đọc được trong bảng 
        '           TCS_GIATRI_TS để đưa ra grTEN_TS.
        ' Tham số: 
        ' Giá trị trả về: 
        ' Ngày viết: 21/11/2007
        ' Người viết: Lê Anh Ngọc
        ' -------------------------------------------------------------
        Dim strSQL As String
        Try

            strSQL = "Select TEN_TS,GIATRI_TS,MO_TA " & _
                " From TCS_THAMSO " & _
                " Where LOAI_TS = 'NSD' AND MA_DTHU='" & cboDSDiemThu.SelectedValue & "'" & _
                " Order by SAP_XEP"
            Dim ds As New DataSet
            ds = buThamso.ThamsoChung(strSQL)
            grdTsRieng.DataSource = ds
            grdTsRieng.DataBind()

        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, gstrLoiKetNoiCSDL)
        Finally
        End Try
    End Sub
    'Private Sub TCS_FillGrTEN_TSTSDR()
    '    '--------------------------------------------------------------
    '    ' Mục đích: Lấy các giá trị tham số đã được lấy khi đăng nhập
    '    '           hệ thống. - Khởi tạo dữ liệu cho tham số dùng riêng
    '    ' Tham số: 
    '    ' Giá trị trả về: 
    '    ' Ngày viết: 21/11/2007
    '    ' Người viết: Lê Anh Ngọc
    '    ' -------------------------------------------------------------
    '    Dim i As Integer = 1
    '    Dim dt As New DataTable()
    '    Dim dtCo As DataColumn
    '    Dim dtRow As DataRow

    '    dtCo = New DataColumn()
    '    dtCo.ColumnName = "TEN_TS"
    '    dt.Columns.Add(dtCo)
    '    dtCo = New DataColumn()
    '    dtCo.ColumnName = "GIATRI_TS"
    '    dt.Columns.Add(dtCo)
    '    dtCo = New DataColumn()
    '    dtCo.ColumnName = "MO_TA"
    '    dt.Columns.Add(dtCo)

    '    ' Ký hiệu chứng từ
    '    dtRow = dt.NewRow()
    '    dtRow("TEN_TS") = "KHCT"
    '    dtRow("MO_TA") = "Ký hiệu chứng từ (in phôi)"
    '    dtRow("GIATRI_TS") = gstrKHCT
    '    dt.Rows.Add(dtRow)


    '    ' Số chứng từ
    '    dtRow = dt.NewRow()
    '    dtRow("TEN_TS") = "SOCT"
    '    dtRow("MO_TA") = "Số chứng từ (in phôi)"
    '    dtRow("GIATRI_TS") = gstrSoCT
    '    dt.Rows.Add(dtRow)

    '    ' Ký hiệu biên lai

    '    dtRow = dt.NewRow()
    '    dtRow("TEN_TS") = "KHBL"
    '    dtRow("MO_TA") = "Ký hiệu biên lai (in phôi)"
    '    dtRow("GIATRI_TS") = gstrKHCT
    '    dt.Rows.Add(dtRow)

    '    ' Số biên lai
    '    dtRow = dt.NewRow()
    '    dtRow("TEN_TS") = "SOBL"
    '    dtRow("MO_TA") = "Số biên lai (in phôi)"
    '    dtRow("GIATRI_TS") = gstrSoBL
    '    dt.Rows.Add(dtRow)

    '    ' Mẫu in phôi
    '    dtRow = dt.NewRow()
    '    dtRow("TEN_TS") = "MAU_GNT"
    '    dtRow("MO_TA") = "Mẫu in phôi (0:Mới; 1:Cũ(HPH); 2:Cũ(HCM); 3:Cũ(HCM-TB); 4:Cũ(BRV))"
    '    dtRow("GIATRI_TS") = gintMauInPhoi.ToString()
    '    dt.Rows.Add(dtRow)

    '    ' Mẫu in A4
    '    dtRow = dt.NewRow()
    '    dtRow("TEN_TS") = "MAU_CTU"
    '    dtRow("MO_TA") = "Mẫu in chứng từ trên A4 (0:Laser; 1:In kim)"
    '    dtRow("GIATRI_TS") = gintMauInCTUA4.ToString()
    '    dt.Rows.Add(dtRow)

    '    ' Mẫu in kim biên lai A5
    '    dtRow = dt.NewRow()
    '    dtRow("TEN_TS") = "MAU_BLT_IK"
    '    dtRow("MO_TA") = "Mẫu in kim biên lai thu (0:A4; 1:A5)"
    '    dtRow("GIATRI_TS") = gintMauInKimBLT.ToString()
    '    dt.Rows.Add(dtRow)

    '    ' Ưu tiên tìm NNT trong danh mục
    '    dtRow = dt.NewRow()
    '    dtRow("TEN_TS") = "DM_UU_TIEN"
    '    dtRow("MO_TA") = "Ưu tiên thu thuế (0:mặc định; 1:trước bạ,đất; 2:thuế xuất, nhập khẩu; 3:thuế nội địa)"
    '    dtRow("GIATRI_TS") = gbytUutien
    '    dt.Rows.Add(dtRow)

    '    ' Tham số connect KT38i
    '    dtRow = dt.NewRow()
    '    dtRow("TEN_TS") = "KT38i"
    '    dtRow("MO_TA") = "Tên database KTKB (Alias trong file TNSNAME.ORA):"
    '    dtRow("GIATRI_TS") = gstrKT38i
    '    dt.Rows.Add(dtRow)
    '    grdTsRieng.DataSource = dt
    '    grdTsRieng.DataBind()       
    'End Sub

    Private Function TCS_GhiTSDC() As Boolean
        '--------------------------------------------------------------
        ' Mục đích: Ghi sự thay đổi vào bảng TCS_GIATRI_TS
        ' Tham số: 
        ' Giá trị trả về: 
        ' Ngày viết: 20/11/2007
        ' Người viết:Vietnq
        ' Nguoi sua: Ngocla
        ' -------------------------------------------------------------       
        Dim cnUGIATRI_TS As New DataAccess
        Dim strSQL1 As String
        Dim strSQL2 As String
        Dim tmpstrCellText As String
        Dim tmpstrTen_TS As String
        Dim blnResult As Boolean = True
        Dim i As Integer = 1
        Dim textP As New TextBox()
        For i = 0 To grdTsRieng.Items.Count - 1
            tmpstrTen_TS = grdTsRieng.Items(i).Cells(0).Text ' ten Tham so
            textP = grdTsRieng.Items(i).FindControl("GIATRI_TS")
            tmpstrCellText = Trim(textP.Text)
            'Kiểm tra giá trị hợp lệ
            Select Case tmpstrTen_TS
                Case "MA_DT"
                    If (tmpstrCellText = "") Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã điểm thu.")
                        blnResult = False
                        Exit For
                    Else
                        'If Not IsNumeric(tmpstrCellText) Then
                        '    clsCommon.ShowMessageBox(Me, "Mã điểm thu phải là số.")
                        '    blnResult = False
                        '    Exit For
                        'End If
                        If Len(tmpstrCellText) > 7 Then
                            clsCommon.ShowMessageBox(Me, "Mã điểm thu có chiều dài lớn nhất là 7.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "MA_QH"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã quận huyện.")
                        blnResult = False
                        Exit For
                    Else
                        'If Not IsNumeric(tmpstrCellText) Then
                        '    clsCommon.ShowMessageBox(Me, "Mã quận huyện phải là số.")
                        '    blnResult = False
                        '    Exit For
                        'End If
                        If Len(tmpstrCellText) > 2 Then
                            clsCommon.ShowMessageBox(Me, "Mã quận huyện có chiều dài lớn nhất là 2.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "MA_TTP"
                    If (tmpstrCellText = "") Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã tỉnh TP.")
                        blnResult = False
                        Exit For
                    Else
                        'If Not IsNumeric(tmpstrCellText) Then
                        '    clsCommon.ShowMessageBox(Me, "Mã tỉnh TP phải là số.")
                        '    blnResult = False
                        '    Exit For
                        'End If
                        'If Len(tmpstrCellText) > 3 Then
                        '    clsCommon.ShowMessageBox(Me, "Mã tỉnh TP có chiều dài lớn nhất là 3.")
                        '    blnResult = False
                        '    Exit For
                        'End If
                    End If
                Case "MA_DBHC"
                    If (tmpstrCellText = "") Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã địa bàn hành chính.")
                        blnResult = False
                        Exit For
                    Else
                        If Len(tmpstrCellText) <> 5 Then
                            clsCommon.ShowMessageBox(Me, "Mã địa bàn hành chính phải có chiều dài là 5.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "KHCT"
                    If (tmpstrCellText = "") Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập thông tin Ký hiệu chứng từ.")
                        blnResult = False
                        Exit For
                    Else
                        If Len(tmpstrCellText) > 20 Then
                            clsCommon.ShowMessageBox(Me, "Thông tin Ký hiệu chứng từ phải có chiều dài <= 20.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "KHBL"
                    If (tmpstrCellText = "") Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập thông tin Ký hiệu biên lai.")
                        blnResult = False
                        Exit For
                    Else
                        If Len(tmpstrCellText) > 20 Then
                            clsCommon.ShowMessageBox(Me, "Thông tin Ký hiệu biên lai phải có chiều dài <= 20.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "SLCT"
                    If (tmpstrCellText = "") Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Số liên in chứng từ.")
                        blnResult = False
                        Exit For
                    Else
                        If Not IsNumeric(tmpstrCellText) Then
                            clsCommon.ShowMessageBox(Me, "Số liên in chứng từ phải là số.")
                            blnResult = False
                            Exit For
                        Else
                            If Len(tmpstrCellText) > 2 Then
                                clsCommon.ShowMessageBox(Me, "Số liên in chứng từ có chiều dài lớn nhất là 2.")
                                blnResult = False
                                Exit For
                            End If
                        End If
                    End If
                Case "SLBL"
                    If (tmpstrCellText = "") Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Số liên in biên lai.")
                        blnResult = False
                        Exit For
                    Else
                        If Not IsNumeric(tmpstrCellText) Then
                            clsCommon.ShowMessageBox(Me, "Số liên in biên lai phải là số.")
                            blnResult = False
                            Exit For
                        Else
                            If Len(tmpstrCellText) > 2 Then
                                clsCommon.ShowMessageBox(Me, "Số liên in biên lai có chiều dài lớn nhất là 2.")
                                blnResult = False
                                Exit For
                            End If
                        End If
                    End If
                Case "TEN_DTHU" ' Bỏ nợ tài khoản
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên điểm thu.")
                        blnResult = False
                        Exit For
                    Else
                        If Len(tmpstrCellText) > 255 Then
                            clsCommon.ShowMessageBox(Me, "Tham số tên điểm thu có độ dài tối đa là 255.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "SO_BANTHU"
                    If (tmpstrCellText = "") Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào số bàn thu.")
                        blnResult = False
                        Exit For
                    Else
                        If Not IsNumeric(tmpstrCellText) Then
                            clsCommon.ShowMessageBox(Me, "Bàn thu phải là số.")
                            blnResult = False
                            Exit For
                        Else
                            If Len(tmpstrCellText) > 2 Then
                                clsCommon.ShowMessageBox(Me, "Tham sô Bàn thu có chiều dài lớn nhất là 2.")
                                blnResult = False
                                Exit For
                            End If
                        End If
                    End If
                Case "SUA_MLNS"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào kiểu sửa/không được sửa MLNS (C/K).")
                        blnResult = False
                        Exit For
                    Else
                        If (UCase(tmpstrCellText) <> "C") And (UCase(tmpstrCellText) <> "K") _
                            And (UCase(tmpstrCellText) <> "TRUE") And (UCase(tmpstrCellText) <> "FALSE") Then
                            clsCommon.ShowMessageBox(Me, "Giá trị tham số cho phép sửa MLNS phải là C hoặc K")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "MA_CQTHUE"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã cơ quan Thuế.")
                        blnResult = False
                        Exit For
                    Else
                        If Len(tmpstrCellText) > 5 Then
                            clsCommon.ShowMessageBox(Me, "Mã cơ quan Thuế có chiều dài lớn nhất là 5.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "TEN_KB"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên Kho bạc.")
                        blnResult = False
                        Exit For
                    Else
                        If Len(tmpstrCellText) > 255 Then
                            clsCommon.ShowMessageBox(Me, "Tên Kho bạc có chiều dài lớn nhất là 255.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "TEN_TINH"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên Tỉnh/TP.")
                        blnResult = False
                        Exit For
                    Else
                        If Len(tmpstrCellText) > 255 Then
                            clsCommon.ShowMessageBox(Me, "Tên Tỉnh/TP có chiều dài lớn nhất là 255.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "SAI_THUCTHU"
                    If (tmpstrCellText = "") Or (tmpstrCellText = "0") Then
                        clsCommon.ShowMessageBox(Me, "Giá trị sai số thực thu phải lớn hơn 0.")
                        blnResult = False
                        Exit For
                    Else
                        If Not IsNumeric(tmpstrCellText) Then
                            clsCommon.ShowMessageBox(Me, "Giá trị sai số thực thu phải là số.")
                            blnResult = False
                            Exit For
                        Else
                            If Len(tmpstrCellText) > 255 Then
                                clsCommon.ShowMessageBox(Me, "Tham số sai số thực thu có chiều dài lớn nhất là 255.")
                                blnResult = False
                                Exit For
                            End If
                        End If
                    End If
                Case "SUA_SOCT"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập giá trị cho phép sửa số CT hay không (C/K).")
                        blnResult = False
                        Exit For
                    Else
                        If (UCase(tmpstrCellText) <> "C") And (UCase(tmpstrCellText) <> "K") _
                            And (UCase(tmpstrCellText) <> "TRUE") And (UCase(tmpstrCellText) <> "FALSE") Then
                            clsCommon.ShowMessageBox(Me, "Giá trị tham số cho phép sửa số CT phải là C hoặc K")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "IN_PHOI"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập giá trị cho phép in chứng từ ra phôi hay không (C/K).")
                        blnResult = False
                        Exit For
                    Else
                        If (UCase(tmpstrCellText) <> "C") And (UCase(tmpstrCellText) <> "K") _
                            And (UCase(tmpstrCellText) <> "TRUE") And (UCase(tmpstrCellText) <> "FALSE") Then
                            clsCommon.ShowMessageBox(Me, "Giá trị tham số cho phép in chứng từ ra phôi phải là C hoặc K")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "KSCT_NVP"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập giá trị (0 hoặc 1) cho phần kiểm soát chứng từ ngoài văn phòng.")
                        blnResult = False
                        Exit For
                    Else
                        If (UCase(tmpstrCellText) <> "0") And (UCase(tmpstrCellText) <> "1") Then
                            clsCommon.ShowMessageBox(Me, "Giá trị tham số kiểm soát chứng từ ngoài văn phòng phải là 0 hoặc 1")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "KT_MATIN"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập giá trị xác định có kiểm tra mã ĐTNT khi lập GNT hay không (C/K).")
                        blnResult = False
                        Exit For
                    Else
                        If (UCase(tmpstrCellText) <> "C") And (UCase(tmpstrCellText) <> "K") _
                            And (UCase(tmpstrCellText) <> "TRUE") And (UCase(tmpstrCellText) <> "FALSE") Then
                            clsCommon.ShowMessageBox(Me, "Giá trị tham số kiểm tra mã ĐTNT khi lập GNT phải là C hoặc K")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "THU_TB"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập giá trị xác định có thu trước bạ hay không (C/K).")
                        blnResult = False
                        Exit For
                    Else
                        If (UCase(tmpstrCellText) <> "C") And (UCase(tmpstrCellText) <> "K") _
                            And (UCase(tmpstrCellText) <> "TRUE") And (UCase(tmpstrCellText) <> "FALSE") Then
                            clsCommon.ShowMessageBox(Me, "Giá trị tham số cho thu trước bạ phải là C hoặc K")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "EXP_PATH"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải chỉ ra đường dẫn kết xuất dữ liệu về văn phòng Kho bạc.")
                        blnResult = False
                        Exit For
                    Else
                        If Len(tmpstrCellText) > 255 Then
                            clsCommon.ShowMessageBox(Me, "Tham số đường dẫn kết xuất dữ liệu có chiều dài lớn nhất là 255.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "FTP_EXP_PATH"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải chỉ ra đường dẫn FTP kết xuất dữ liệu về văn phòng Kho bạc.")
                        blnResult = False
                        Exit For
                    Else
                        If Len(tmpstrCellText) > 255 Then
                            clsCommon.ShowMessageBox(Me, "Tham số đường dẫn kết xuất dữ liệu có chiều dài lớn nhất là 255.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "IMP_PATH"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải chỉ ra đường dẫn nhận dữ liệu của cơ quan Thuế gửi.")
                        blnResult = False
                        Exit For
                    Else
                        If Len(tmpstrCellText) > 255 Then

                            clsCommon.ShowMessageBox(Me, "Tham số đường dẫn FTP nhận dữ liệu có chiều dài lớn nhất là 255.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "FTP_IMP_PATH"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải chỉ ra đường dẫn FTP nhận dữ liệu của cơ quan Thuế gửi.")
                        blnResult = False
                        Exit For
                    Else
                        If Len(tmpstrCellText) > 255 Then
                            clsCommon.ShowMessageBox(Me, "Tham số đường dẫn FTP nhận dữ liệu có chiều dài lớn nhất là 255.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "BK_PATH"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải chỉ ra đường dẫn sao lưu và khôi phục dữ liệu.")
                        blnResult = False
                        Exit For
                    Else
                        If Len(tmpstrCellText) > 255 Then
                            clsCommon.ShowMessageBox(Me, "Tham số đường dẫn sao lu và khôi phục dữ liệu có chiều dài lớn nhất là 255.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "TB_PATH"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải chỉ ra đường dẫn đến file của chương trình thu trước bạ.")
                        blnResult = False
                        Exit For
                    Else
                        If Len(tmpstrCellText) > 255 Then
                            clsCommon.ShowMessageBox(Me, "Tham số đường dẫn chứa file của chương trình thu thuế trước bạ có chiều dài lớn nhất là 255.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "FTP_TB_PATH"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải chỉ ra đường dẫn FTP đến file của chương trình thu trước bạ.")
                        blnResult = False
                        Exit For
                    Else
                        If Len(tmpstrCellText) > 255 Then
                            clsCommon.ShowMessageBox(Me, "Tham số đường dẫn FTP chứa file của chương trình trước bạ có chiều dài lớn nhất là 255.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "NHAP_BKPLT"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào kiểu nhập số thực thu theo BKPLT/Tổng số(B/T).")
                        blnResult = False
                        Exit For
                    Else
                        If (UCase(tmpstrCellText) <> "B") And (UCase(tmpstrCellText) <> "T") Then
                            clsCommon.ShowMessageBox(Me, "Giá trị tham số nhập số thực thu phải là B hoặc T")
                            blnResult = False
                            Exit For
                        End If
                    End If
                    tmpstrCellText = UCase(tmpstrCellText)
                Case "IN_KYTHUE"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập giá trị xác định có in kỳ thuế hay không(C/K)?")
                        blnResult = False
                        Exit For
                    Else
                        If (UCase(tmpstrCellText) <> "C") And (UCase(tmpstrCellText) <> "K") Then
                            clsCommon.ShowMessageBox(Me, "Giá trị tham số cho phép in kỳ thuế phải là C hoặc K")
                            blnResult = False
                            Exit For
                        End If
                    End If
                Case "KSCT"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập giá trị xác định có kiểm soát chứng từ(C/K)?")
                        blnResult = False
                        Exit For
                    Else
                        If (UCase(tmpstrCellText) <> "C") And (UCase(tmpstrCellText) <> "K") Then
                            clsCommon.ShowMessageBox(Me, "Giá trị tham số cho phép kiểm soát phải là C hoặc K")
                            blnResult = False
                            Exit For
                        End If
                        tmpstrCellText = UCase(tmpstrCellText)
                    End If
                Case "A5_TOP"
                    If (tmpstrCellText = "") Then
                        clsCommon.ShowMessageBox(Me, "Giá trị điều chỉnh phải lớn hơn hoặc bằng 0.")
                        blnResult = False
                        Exit For
                    Else
                        If Not IsNumeric(tmpstrCellText) Then
                            clsCommon.ShowMessageBox(Me, "Giá trị điều chỉnh phải là số.")
                            blnResult = False
                            Exit For
                        Else
                            If Len(tmpstrCellText) > 255 Then
                                clsCommon.ShowMessageBox(Me, "Tham số điều chỉnh có chiều dài lớn nhất là 255.")
                                blnResult = False
                                Exit For
                            End If
                        End If
                    End If
                Case "A5_LEFT"
                    If (tmpstrCellText = "") Then
                        clsCommon.ShowMessageBox(Me, "Giá trị điều chỉnh phải lớn hơn hoặc bằng 0.")
                        blnResult = False
                        Exit For
                    Else
                        If Not IsNumeric(tmpstrCellText) Then
                            clsCommon.ShowMessageBox(Me, "Giá trị điều chỉnh phải là số.")
                            blnResult = False
                            Exit For
                        Else
                            If Len(tmpstrCellText) > 255 Then
                                clsCommon.ShowMessageBox(Me, "Tham số điều chỉnh có chiều dài lớn nhất là 255.")
                                blnResult = False
                                Exit For
                            End If
                        End If
                    End If
                Case "PASS_KTKB"
                    If tmpstrCellText = "" Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập mật khẩu của user TDTT trong KTKB.")
                        blnResult = False
                        Exit For
                    Else
                        If Len(tmpstrCellText) > 20 Then
                            clsCommon.ShowMessageBox(Me, "Tham số mật khẩu có chiều dài lớn nhất là 20.")
                            blnResult = False
                            Exit For
                        End If
                    End If
                    If (Not "".Equals(gstrPassKTKB) And gstrPassKTKB.Equals(VBOracleLib.Security.DescryptStr(tmpstrCellText))) Then
                        tmpstrCellText = gstrPassKTKB
                    End If
                    tmpstrCellText = VBOracleLib.Security.EncryptStr(tmpstrCellText)
                Case "TEN_KTT"
                    If (tmpstrCellText = "") Then
                        tmpstrCellText = " "
                    End If
            End Select

            If blnResult Then

                Try
                    strSQL1 = "Update tcs_thamso " & _
                        "Set GIATRI_TS = '" & tmpstrCellText & "' " & _
                        "Where MA_DTHU='" & cboDSDiemThu.SelectedValue & "' and TEN_TS = '" & tmpstrTen_TS & "'"
                    Select Case tmpstrTen_TS
                        Case "EXP_PATH"
                            strSQL2 = "Update XML_PATH " & _
                                "Set Path_Name = '" & tmpstrCellText & "' " & _
                                "Where Path_Type = 'Y'"
                        Case "IMP_PATH"
                            strSQL2 = "Update XML_PATH " & _
                                "Set Path_Name = '" & tmpstrCellText & "' " & _
                                "Where Path_Type = 'N'"
                    End Select
                    'If gDB_Type = "ORA8i" Or gDB_Type = "ORA9i" Then
                    strSQL2 = ""
                    'End If
                    buThamso.ThamsoRieng(strSQL1, strSQL2)
                    ' Bỏ trạng thái bận
                Catch ex As Exception
                    'Lỗi thao tác vừa thực hiện
                    clsCommon.ShowMessageBox(Me, gstrLoiThaoTacThucHien)
                    'LogDebug.Writelog(gstrLoiThaoTacThucHien & " - " & ex.ToString & vbCrLf & "Mã lỗi: " & Err.Number & vbCrLf & "-----------------------------------" & vbCrLf)
                    blnResult = False
                    Exit For

                End Try
            End If
        Next
        clsCommon.ShowMessageBox(Me, "Cập nhật thành công")
        'If blnResult Then
        '    mblnSuaDL = False
        '    cmdGhiTSDC.Enabled = False
        'End If
        ' Trả lại kết quả cho hàm
        Return blnResult
    End Function
    'Private Sub cmdGhiTSDC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGhiTSDC.Click
    '    If TCS_GhiTSDC() Then ReadDBVars()
    'End Sub
    'Private Function TCS_GhiTSDR() As Boolean
    '    '--------------------------------------------------------------
    '    ' Mục đích: Ghi sự thay đổi vào các biến hệ thống của bàn làm
    '    '           việc hiện thời- Tham số dùng riêng
    '    ' Tham số: 
    '    ' Giá trị trả về: 
    '    ' Ngày viết: 20/11/2007
    '    ' Người viết:Vietnq
    '    ' Nguoi sua: Ngocla
    '    ' -------------------------------------------------------------
    '    Dim tmpstrCellText As String
    '    Dim tmpstrTen_TS As String
    '    Dim tmpstrKT38i As String
    '    Dim tmpbytUutien As Byte

    '    Dim tmpstrSoCT As String
    '    Dim tmpstrSoBL As String
    '    Dim tmpstrKHCT As String
    '    Dim tmpstrKHBL As String
    '    Dim tmpbytMauGNT As Byte
    '    Dim tmpbytMauCTUA4 As Byte
    '    Dim tmpbytMau_BLT_IK As Byte

    '    Dim blnResult As Boolean = True
    '    Dim i As Integer

    '    Dim textP As New TextBox()
    '    For i = 0 To grdTsRieng.Items.Count - 1
    '        tmpstrTen_TS = grdTsRieng.Items(i).Cells(0).Text ' ten Tham so
    '        textP = grdTsRieng.Items(i).FindControl("GIATRI_TS")
    '        tmpstrCellText = Trim(textP.Text)
    '        Select Case tmpstrTen_TS
    '            Case "KHCT"
    '                If tmpstrCellText = "" Then
    '                    clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào ký hiệu chứng từ.")
    '                    blnResult = False
    '                    Exit For
    '                Else
    '                    If Len(tmpstrCellText) > 15 Then
    '                        clsCommon.ShowMessageBox(Me, "Ký hiệu chứng từ có chiều dài lớn nhất là 15.")
    '                        blnResult = False
    '                        Exit For
    '                    End If
    '                    tmpstrKHCT = tmpstrCellText
    '                End If
    '            Case "SOCT"
    '                If tmpstrCellText = "" Then
    '                    clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào số chứng từ.")
    '                    blnResult = False
    '                    Exit For
    '                Else
    '                    If Len(tmpstrCellText) > 7 Then
    '                        clsCommon.ShowMessageBox(Me, "Số chứng từ có chiều dài lớn nhất là 7.")
    '                        blnResult = False
    '                        Exit For
    '                    End If
    '                    If Not IsNumeric(tmpstrCellText) Then
    '                        clsCommon.ShowMessageBox(Me, "Số chứng từ phải là số.")
    '                        blnResult = False
    '                        Exit For
    '                    End If
    '                    tmpstrSoCT = tmpstrCellText
    '                End If
    '            Case "KHBL"
    '                If tmpstrCellText = "" Then
    '                    clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào ký hiệu biên lai.")
    '                    blnResult = False
    '                    Exit For
    '                Else
    '                    If Len(tmpstrCellText) > 15 Then
    '                        clsCommon.ShowMessageBox(Me, "Ký hiệu biên lai có chiều dài lớn nhất là 15.")
    '                        blnResult = False
    '                        Exit For
    '                    End If
    '                    tmpstrKHBL = tmpstrCellText
    '                End If
    '            Case "SOBL"
    '                If tmpstrCellText = "" Then
    '                    clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào số biên lai.")
    '                    blnResult = False
    '                    Exit For
    '                Else
    '                    If Len(tmpstrCellText) > 7 Then
    '                        clsCommon.ShowMessageBox(Me, "Số biên lai có chiều dài lớn nhất là 7.")
    '                        blnResult = False
    '                        Exit For
    '                    End If
    '                    If Not IsNumeric(tmpstrCellText) Then
    '                        clsCommon.ShowMessageBox(Me, "Số biên lai phải là số.")
    '                        blnResult = False
    '                        Exit For
    '                    End If
    '                    tmpstrSoBL = tmpstrCellText
    '                End If
    '            Case "MAU_GNT"
    '                If Not IsNumeric(tmpstrCellText) Then
    '                    clsCommon.ShowMessageBox(Me, "Mẫu in phôi (0-Mới; 1-Cũ(HPH)); 2-Cũ(HCM); 3-Cũ(HCM-TB); 4-Cũ(BRV)).")
    '                    blnResult = False
    '                    Exit For
    '                End If
    '                Select Case CInt(tmpstrCellText)
    '                    Case 0, 1, 2, 3, 4

    '                    Case Else
    '                        clsCommon.ShowMessageBox(Me, "Mẫu in phôi (0-Mới; 1-Cũ(HPH)); 2-Cũ(HCM); 3-Cũ(HCM-TB); 4-Cũ(BRV)).")
    '                        blnResult = False
    '                        Exit For
    '                End Select
    '                tmpbytMauGNT = CByte(tmpstrCellText)
    '            Case "MAU_CTU"
    '                If Not IsNumeric(tmpstrCellText) Then
    '                    clsCommon.ShowMessageBox(Me, "Mẫu in chứng từ trên A4 (0:Laser; 1:In kim)")
    '                    blnResult = False
    '                    Exit For
    '                End If
    '                Select Case CInt(tmpstrCellText)
    '                    Case 0, 1

    '                    Case Else
    '                        clsCommon.ShowMessageBox(Me, "Mẫu in chứng từ trên A4 (0:Laser; 1:In kim)")
    '                        blnResult = False
    '                        Exit For
    '                End Select
    '                tmpbytMauCTUA4 = CInt(tmpstrCellText)
    '            Case "MAU_BLT_IK"
    '                If Not IsNumeric(tmpstrCellText) Then
    '                    clsCommon.ShowMessageBox(Me, "Mẫu in kim biên lai thu (0:A4; 1:A5)")
    '                    blnResult = False
    '                    Exit For
    '                End If
    '                Select Case CInt(tmpstrCellText)
    '                    Case 0, 1

    '                    Case Else
    '                        clsCommon.ShowMessageBox(Me, "Mẫu in kim biên lai thu (0:A4; 1:A5)")
    '                        blnResult = False
    '                        Exit For
    '                End Select
    '                tmpbytMau_BLT_IK = CInt(tmpstrCellText)
    '            Case "DM_UU_TIEN"
    '                If tmpstrCellText = "" Then
    '                    clsCommon.ShowMessageBox(Me, "Ưu tiên thu thuế (0- mặc định; 1 - thuế trước bạ, thuế đất; 2 – thu thuế xuất, nhập khẩu; 3 - Thu thuế nội địa)")
    '                    blnResult = False
    '                    Exit For
    '                Else
    '                    If Not IsNumeric(tmpstrCellText) Then
    '                        clsCommon.ShowMessageBox(Me, "Ưu tiên thu thuế (0- mặc định; 1 - thuế trước bạ, thuế đất; 2 – thu thuế xuất, nhập khẩu; 3 - Thu thuế nội địa).")
    '                        blnResult = False
    '                        Exit For
    '                    End If
    '                    If Not (CInt(tmpstrCellText) = 0 Or CInt(tmpstrCellText) = 1 Or CInt(tmpstrCellText) = 2 Or CInt(tmpstrCellText) = 3) Then
    '                        clsCommon.ShowMessageBox(Me, "Ưu tiên thu thuế (0- mặc định; 1 - thuế trước bạ, thuế đất; 2 – thu thuế xuất, nhập khẩu; 3 - Thu thuế nội địa).")
    '                        blnResult = False
    '                        Exit For
    '                    End If
    '                    tmpbytUutien = tmpstrCellText
    '                End If
    '            Case "KT38i"
    '                If tmpstrCellText = "" Then
    '                    clsCommon.ShowMessageBox(Me, "Tên database KTKB không được để trống.")
    '                    blnResult = False
    '                    Exit For
    '                Else
    '                    tmpstrKT38i = tmpstrCellText
    '                End If
    '        End Select
    '    Next
    '    If blnResult Then
    '        gbytUutien = tmpbytUutien
    '        gstrKT38i = tmpstrKT38i

    '        gstrSoCT = ChuanHoa_SoCT(Trim(tmpstrSoCT))
    '        gstrSoBL = ChuanHoa_SoCT(Trim(tmpstrSoBL))
    '        gstrKHCT = tmpstrKHCT
    '        gstrKHBL = tmpstrKHBL

    '        gintMauInPhoi = tmpbytMauGNT
    '        gintMauInCTUA4 = tmpbytMauCTUA4
    '        gintMauInKimBLT = tmpbytMau_BLT_IK

    '        'mblnSuaDL = False
    '        'cmdGhiTSDR.Enabled = False
    '        UpdateIniFile()
    '    Else
    '        grdTsRieng.Focus()
    '    End If
    '    ' Trả lại kết quả cho hàm
    '    Return blnResult
    '    clsCommon.ShowMessageBox(Me, "Cập nhật thành công")
    'End Function
   
    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        If Not Session.Item("User") Is Nothing Then
            If TCS_GhiTSDC() Then ReadDBVars(clsCTU.TCS_GetMaDT(CType(Session("User"), Integer)))
            TCS_FillGrTEN_TSTSDC()
        End If
    End Sub

    'Protected Sub cboLoaiTS_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoaiTS.SelectedIndexChanged
    '    If cboLoaiTS.SelectedValue = 1 Then
    '        ' Khởi tạo dữ liệu tham số dùng riêng
    '        TCS_FillGrTEN_TSTSDR()
    '    Else
    '        ' Khởi tạo dữ liệu tham số dùng chung
    '        TCS_FillGrTEN_TSTSDC()
    '    End If
    'End Sub
    Private Sub LoadDDLDiemThu()
        Try
            Dim strSQL As String = "SELECT a.ma_dthu ID, a.ten TITLE  FROM tcs_dm_diemthu a  ORDER BY a.ma_dthu"
            cboDSDiemThu.DataSource = DatabaseHelp.ExecuteToTable(strSQL)
            cboDSDiemThu.DataTextField = "TITLE"
            cboDSDiemThu.DataValueField = "ID"
            cboDSDiemThu.DataBind()
            cboDSDiemThu.SelectedIndex = 0            
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub cboDSDiemThu_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDSDiemThu.SelectedIndexChanged
        'Load danh sach cac tham so dung chung cho tung diem thu
        TCS_FillGrTEN_TSTSDC()
    End Sub
End Class
