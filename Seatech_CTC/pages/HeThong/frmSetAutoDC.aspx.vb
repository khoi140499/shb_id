﻿Imports System.Data
Imports Business
Imports VBOracleLib
Partial Class pages_HeThong_frmSetAutoDC
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            SetControl()
            cmdChuyen.Attributes.Add("onClick", "return confirm('Bạn có thực sự muốn chuyển trạng thái đối chiếu tự động không?')")
        End If
    End Sub
    Private Sub SetControl()
        Dim v_strStatus As String = ""


        If Not Application("AUTO_RUN") Is Nothing Then
            v_strStatus = Application("AUTO_RUN").ToString()
        End If


        If v_strStatus <> "" Then

            If v_strStatus = "0" Then
                lbHienTai.Text = "Chạy tự động"
                cboTrangThai.SelectedIndex = 1
                cmdChuyen.Enabled = True
            ElseIf v_strStatus = "1" Then
                lbHienTai.Text = "Dừng chạy tự động"
                cboTrangThai.SelectedIndex = 0
                cmdChuyen.Enabled = True
            ElseIf v_strStatus = "2" Then
                lbHienTai.Text = "Đang chạy đối chiếu"
                cmdChuyen.Enabled = False
            End If
            cboTrangThai.Enabled = False
            
        End If

    End Sub

    Protected Sub cmdChuyen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyen.Click
        Dim str As String
        Try

            If Not cboTrangThai.SelectedValue Is Nothing Then
                Application("AUTO_RUN") = cboTrangThai.SelectedValue.ToString()
                clsCTU.Update_DC_TS_AUTO_RUN(cboTrangThai.SelectedValue.ToString())
            End If

        Catch ex As Exception

            'Lỗi xoá dữ liệu không thành công
            clsCommon.ShowMessageBox(Me, "Không chuyển được trạng thái đối chiếu tự động")
            Return
        End Try
        SetControl()
        clsCommon.ShowMessageBox(Page, "Thực hiện thành công")
    End Sub
End Class
