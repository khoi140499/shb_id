﻿Imports System
Imports System.Drawing
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports Business.HeThong
Imports System.Data
Partial Class pages_HeThong_frmThietLapBDS
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.Params("ID") <> "" Then
                txtMaCN.Enabled = False
                load_Detail()
            End If
            Load_Datagrid()
        End If
        cmdDelete.Attributes.Add("onclick", "return confirm('Bạn có thực sự muốn xóa không?')")
    End Sub
    Private Sub load_Detail()
        Dim Ma_CN As String
        Dim dsDetail As DataSet
        If Not Request.Params("ID") Is Nothing Then
            Ma_CN = Request.Params("ID").ToString.Trim
            dsDetail = LayDSCN(Ma_CN)
            If Not dsDetail Is Nothing And dsDetail.Tables(0).Rows.Count > 0 Then
                txtMaCN.Text = dsDetail.Tables(0).Rows(0)("Ma_CN").ToString
                txtURLBDS.Text = dsDetail.Tables(0).Rows(0)("URLBDS").ToString
                txtURLBDSVB.Text = dsDetail.Tables(0).Rows(0)("URLBDSVB").ToString
                txtSERVERNAME.Text = dsDetail.Tables(0).Rows(0)("SERVERNAME").ToString
                txtUserName.Text = dsDetail.Tables(0).Rows(0)("User_Name").ToString
                txtPassWord.Attributes.Add("value", dsDetail.Tables(0).Rows(0)("password").ToString)
                txtDBName.Text = dsDetail.Tables(0).Rows(0)("dbname").ToString
                txtBDSVB_SERVERNAME.Text = dsDetail.Tables(0).Rows(0)("BDSVB_SERVERNAME").ToString
                txtBDSVB_USER_NAME.Text = dsDetail.Tables(0).Rows(0)("BDSVB_USER_NAME").ToString
                txtBDSVB_PASSWORD.Attributes.Add("value", dsDetail.Tables(0).Rows(0)("BDSVB_PASSWORD").ToString)
            End If
        End If
    End Sub
    Private Function LayDSCN(Optional ByVal ma_CN As String = "") As DataSet
        Dim strSQL As String
        Dim ds As DataSet
        Dim where As String = ""
        If ma_CN <> "" Then
            where = " AND MA_CN='" & ma_CN & "'"
        End If
        strSQL = " select * from tcs_bank_thamso WHERE 1=1" & where
        ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Return ds
    End Function
    Private Sub Load_Datagrid()
        Dim dsChung As DataSet
        dsChung = LayDSCN()
        If Not dsChung Is Nothing And dsChung.Tables(0).Rows.Count > 0 Then
            grid_data.DataSource = dsChung
            grid_data.DataBind()
        End If
    End Sub
    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        XoaText()
        If Not Request.Params("id") Is Nothing Then
            Response.Redirect("~/pages/HeThong/frmThietLapBDS.aspx", False)
        End If
    End Sub
    Private Sub XoaText()
        txtMaCN.Text = ""
        txtSERVERNAME.Text = ""
        txtURLBDS.Text = ""
        txtURLBDSVB.Text = ""
        txtDBName.Text = ""
        txtPassWord.Attributes.Add("value", "")
        txtUserName.Text = ""
        txtBDSVB_PASSWORD.Attributes.Add("value", "")
        txtBDSVB_SERVERNAME.Text = ""
        txtBDSVB_USER_NAME.Text = ""
    End Sub

    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        Dim strInsert As String
        If txtMaCN.Text.Trim.Length = 0 Then
            clsCommon.ShowMessageBox(Page, "Mã chi nhánh không được để trống")
            Exit Sub
        End If
        'If txtURLBDS.Text.Trim.Length = 0 Then
        '    clsCommon.ShowMessageBox(Page, "URLBDS không được để trống")
        '    Exit Sub
        'End If
        'If txtURLBDSVB.Text.Trim.Length = 0 Then
        '    clsCommon.ShowMessageBox(Page, "URLBDSVB không được để trống")
        '    Exit Sub
        'End If
        If txtSERVERNAME.Text.Trim.Length = 0 Then
            clsCommon.ShowMessageBox(Page, "Servername không được để trống")
            Exit Sub
        End If
        If txtUserName.Text.Trim.Length = 0 Then
            clsCommon.ShowMessageBox(Page, "Username không được để trống")
            Exit Sub
        End If
        If txtPassWord.Text.Trim.Length = 0 Then
            clsCommon.ShowMessageBox(Page, "Password không được để trống")
            Exit Sub
        End If
        'If txtDBName.Text.Trim.Length = 0 Then
        '    clsCommon.ShowMessageBox(Page, "DBName không được để trống")
        '    Exit Sub
        'End If
        If txtBDSVB_SERVERNAME.Text.Trim.Length = 0 Then
            clsCommon.ShowMessageBox(Page, "BDSVB_SERVERNAME không được để trống")
            Exit Sub
        End If
        If txtBDSVB_USER_NAME.Text.Trim.Length = 0 Then
            clsCommon.ShowMessageBox(Page, "BDSVB_USER_NAME không được để trống")
            Exit Sub
        End If
        If txtBDSVB_PASSWORD.Text.Trim.Length = 0 Then
            clsCommon.ShowMessageBox(Page, "BDSVB_PASSWORD không được để trống")
            Exit Sub
        End If
        If Not Request.Params("id") Is Nothing Then
            strInsert = " UPDATE TCS_BANK_THAMSO SET URLBDS=  '" & txtURLBDS.Text.Trim & "', " & _
            " URLBDSVB= '" & txtURLBDSVB.Text.Trim & "'," & _
            " SERVERNAME='" & txtSERVERNAME.Text.Trim & "'," & _
            " User_name= '" & txtUserName.Text.Trim & "'," & _
            " Password='" & txtPassWord.Text.Trim & "'," & _
            " BDSVB_SERVERNAME='" & txtBDSVB_SERVERNAME.Text & "'," & _
            " BDSVB_USER_NAME='" & txtBDSVB_USER_NAME.Text & "'," & _
            " BDSVB_PASSWORD='" & txtBDSVB_PASSWORD.Text & "'" & _
            " WHERE MA_CN='" & Request.Params("id").ToString.Trim & "'"
        Else
            strInsert = "INSERT INTO TCS_BANK_THAMSO(MA_CN,URLBDS,URLBDSVB,SERVERNAME,user_name,password,dbname,BDSVB_SERVERNAME,BDSVB_USER_NAME,BDSVB_PASSWORD)" & _
                        " VALUES('" & txtMaCN.Text.Trim & "','" & txtURLBDS.Text.Trim & "','" & txtURLBDSVB.Text.Trim & "','" & txtSERVERNAME.Text.Trim & "'," & _
                        " '" & txtUserName.Text.Trim & " ','" & txtPassWord.Text.Trim & "','" & txtURLBDS.Text.Trim & "','" & txtBDSVB_SERVERNAME.Text.Trim & "','" & txtBDSVB_USER_NAME.Text.Trim & "','" & txtBDSVB_PASSWORD.Text.Trim & "')"
        End If
        Dim kq As Integer
        kq = DatabaseHelp.Execute(strInsert)
        If kq <= 0 Then
            clsCommon.ShowMessageBox(Me, "Cập nhật không thành công!")
            Exit Sub
        Else
            clsCommon.ShowMessageBox(Me, "Cập nhật thành công!")
            XoaText()
            Load_Datagrid()
        End If
    End Sub

    Protected Sub cmdDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim strMaCN As String
        Dim objUser As New infUser
        Dim strDel As String
        For i As Integer = 0 To grid_data.Items.Count() - 1
            Dim chkSelect As CheckBox
            chkSelect = grid_data.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strMaCN = Trim(grid_data.Items(i).Cells(0).Text)
                Try
                    strDel = "Delete from TCS_BANK_THAMSO WHERE MA_CN='" & strMaCN & "'"
                    Dim kq As Integer
                    kq = DatabaseHelp.Execute(strDel)
                    If kq <= 0 Then
                        clsCommon.ShowMessageBox(Me, "Xóa không thành công!")
                        Exit Sub
                    Else

                    End If
                    Exit For
                Catch ex As Exception
                    'Lỗi xoá dữ liệu không thành công
                    clsCommon.ShowMessageBox(Me, gstrLoiXoaDuLieu)
                    Return
                End Try
            End If
        Next
        clsCommon.ShowMessageBox(Me, "Xóa thành công!")
        Load_Datagrid()
    End Sub

    Protected Sub grid_data_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grid_data.PageIndexChanged
        Load_Datagrid()
        grid_data.CurrentPageIndex = e.NewPageIndex
        grid_data.DataBind()
    End Sub
End Class
