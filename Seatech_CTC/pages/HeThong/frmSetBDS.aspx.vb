﻿Imports System.Data
Imports Business
Imports VBOracleLib
Partial Class pages_HeThong_frmSetBDS
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            SetControl()
            cmdChuyen.Attributes.Add("onClick", "return confirm('Bạn có thực sự muốn chuyển trạng thái BDS không?')")
        End If
    End Sub
    Private Sub SetControl()
        Dim str As String
        Dim dt As DataTable
        str = "SELECT TEN_TS,  GIATRI_TS FROM TCS_THAMSO WHERE MA_DTHU='00' AND TEN_TS='MODE'"
        dt = DatabaseHelp.ExecuteToTable(str)
        If Not dt Is Nothing Then
            dt.Rows(0)("GIATRI_TS").ToString()
            If dt.Rows(0)("GIATRI_TS").ToString().Trim().ToUpper = "O" Then
                lbHienTai.Text = "Online"
                cboTrangThai.SelectedIndex = 1
            Else
                lbHienTai.Text = "Offline"
                cboTrangThai.SelectedIndex = 0
            End If
            cboTrangThai.Enabled = False
        End If
    End Sub

    Protected Sub cmdChuyen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyen.Click
        Dim str As String
        Try
            str = "Update TCS_THAMSO SET GIATRI_TS='" & cboTrangThai.SelectedValue.ToString & "' WHERE TEN_TS='MODE' AND MA_DTHU='00'"
            DataAccess.ExecuteNonQuery(Str, CommandType.Text)
        Catch ex As Exception

            'Lỗi xoá dữ liệu không thành công
            clsCommon.ShowMessageBox(Me, "Không chuyển được trạng thái BDS")
            Return
        End Try
        SetControl()
        clsCommon.ShowMessageBox(Page, "Thực hiện thành công")
    End Sub
End Class
