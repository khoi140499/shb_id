﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmGroups.aspx.vb" Inherits="pages_HeThong_frmGroups" Title="Quản lý chức năng nhóm người dùng" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table id="Table1" cellspacing="0" align="center" cellpadding="0" bgcolor="#ffffff" style="width: 100%">
        <tr>
            <td valign="top" align="center">
                <table id="Content" cellspacing="1" cellpadding="1" border="0" style="width: 100%"
                    bgcolor="#ffffff">
                    <tr>
                        <td colspan="2" class="pageTitle" style="height: 30px">
                            <asp:Label ID="Label1" runat="server" Font-Bold="True">QUẢN LÝ CHỨC NĂNG NHÓM NGƯỜI DÙNG</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left">
                            <table id="Table2" width="100%" border="0" cellpadding="0" cellspacing="1" class="form_input">
                                <tr>
                                    <td valign="top">
                                        <table width="100%">
                                            <tr align="left">
                                                <td class="form_label">
                                                    <asp:Label ID="lblNhom" runat="server" CssClass="label">Nhóm</asp:Label>
                                                </td>
                                                <td class="form_control" colspan="3">
                                                    <asp:DropDownList ID="cboNhom" runat="server" Width="200px" AutoPostBack="True" CssClass="inputflat">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td class="form_label">
                                                    <asp:Label ID="Label4" runat="server" CssClass="label">Danh sách chức năng</asp:Label>
                                                </td>
                                                <td class="form_control" colspan="3">
                                                    <div style="vertical-align:middle; height:350px; overflow: auto; width: 600px;">
                                                        <asp:DataGrid ID="dtgChucNang" Visible="false" runat="server" AutoGenerateColumns="False" TabIndex="9"
                                                            Width="100%" BorderColor="#989898" CssClass="grid_data" Height="100">
                                                            <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                                                            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
                                                            <ItemStyle CssClass="grid_item" />
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderText="Chọn">
                                                                    <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="Ma_CN" Visible="false" HeaderText="Mã chức năng">
                                                                    <HeaderStyle Width="0px"></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left" Width="0px"></ItemStyle>
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="Ten_CN_Con" HeaderText="Chức năng">
                                                                    <HeaderStyle></HeaderStyle>
                                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                </asp:BoundColumn>
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                                                                Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                                                                Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                                                            </PagerStyle>
                                                        </asp:DataGrid>
                                                        <table id="tblPhanQuyen" style="width: 600px;">
                                                            <asp:Repeater runat="server" ID="rptParentNode">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td style="color:Black;background:#99CCCC;padding:2px">
                                                                        <asp:HiddenField ID="hdParentNodeId" runat="server" Value='<%#Eval("nodeid")%>' />
                                                                        <asp:HiddenField ID="hdTenCN" runat="server" Value='<%#Eval("text")%>' />
                                                                        
                                                                        <asp:Label ID="Label1" Text='<%#Eval("text")%>' runat="server" Font-Bold="true" />
                                                                        <br />
                                                                            <asp:DataGrid AutoGenerateColumns="false" Width="100%" runat="server" ID="childNode" DataSource='<%# LoadChildNode(Eval("nodeid").ToString()) %>'>
                                                                                <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                                                                                <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
                                                                                <ItemStyle CssClass="grid_item" />
                                                                                <Columns>
                                                                                    <asp:TemplateColumn ItemStyle-Width="25" HeaderText="Chọn">
                                                                                        <ItemTemplate>
                                                                                            <asp:HiddenField ID="hdChildNodeId" runat="server" Value='<%#Eval("nodeid")%>' />
                                                                                            <asp:CheckBox ID="chkSelectItem" runat="server" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="ten_cn_con" HeaderText="Tên chức năng" />
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </table>
                                                    </div>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 40px">
                        <td colspan="2" align="center">
                            <asp:Button ID="cmdSave" runat="server" CssClass="ButtonCommand" Text="Ghi" TabIndex="10">
                            </asp:Button>&nbsp;&nbsp;
                            <asp:Button ID="btnExport" runat="server" Text="Export" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
