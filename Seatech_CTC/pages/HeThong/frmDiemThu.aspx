﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false" CodeFile="frmDiemThu.aspx.vb" Inherits="pages_HeThong_frmDiemThu" title="Phân quyền điểm thu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" Runat="Server">
<table id="Table1" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tr>
            <td style="width: 10px;">
                <img src="">
            </td>
            <td style="width: 620px; height: 351px;" valign="top" align="center">
                <table id="Content" cellspacing="1" cellpadding="1" border="0" style="width: 100%"
                    bgcolor="#ffffff">
                    <tr>
                        <td colspan="2" class="pageTitle" style="height: 30px">
                            <asp:Label ID="Label1" runat="server" Font-Bold="True">PHÂN QUYỀN THEO ĐỊA BÀN THU</asp:Label>
                        </td>
                    </tr>
                    <tr style="height:40px">
                    <td align="left">
                    <asp:Label runat="server" ID="Label3"  Font-Bold="True" Font-Italic="true" ForeColor="#003399"  >Danh sách người sử dụng</asp:Label>
                    </td>
                    <td align="center" class="pageTitle">
                    <asp:Label runat="server" ID="Label2"  Font-Bold="True" >Danh sách điểm thu</asp:Label>
                    </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 60%" valign="top">
                            <asp:DataGrid ID="dgUserAccount" runat="server" AutoGenerateColumns="False" TabIndex="9"
                                Width="100%" AllowPaging="True" BorderColor="#989898" CssClass="grid_data" Height="100">
                                <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                                <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
                                <ItemStyle CssClass="grid_item" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="T&#234;n đăng nhập" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ma_NV") %>'
                                                NavigateUrl='<%#"~/pages/HeThong/frmDiemThu.aspx?ID=" & DataBinder.Eval(Container.DataItem, "Ma_NV") %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Ma_NV" Visible="false" HeaderText="Tên đăng nhập">
                                        <HeaderStyle Width="0px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="0px"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Ten" HeaderText="Họ tên">
                                        <HeaderStyle></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Chuc_Danh" HeaderText="Chức danh">
                                        <HeaderStyle Width="80px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <%-- <asp:BoundColumn DataField="Ban_LV" HeaderText="Bàn số">
                                            <HeaderStyle Width="80px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="local_ip" HeaderText="Địa chỉ IP">
                                            <HeaderStyle Width="80px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        </asp:BoundColumn>--%>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                                    Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                                </PagerStyle>
                            </asp:DataGrid>
                        </td>
                        <td align="left" style="width: 60%" valign="top">
                            <asp:DataGrid ID="dtgDiemthu" runat="server" AutoGenerateColumns="False" TabIndex="9"
                                Width="100%" BorderColor="#989898" CssClass="grid_data" Height="100">
                                <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                                <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
                                <ItemStyle CssClass="grid_item" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Chọn">
                                        <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="MA_DThu" Visible="false" HeaderText="Mã điểm thu">
                                        <HeaderStyle Width="0"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="TEN" HeaderText="Tên điểm thu">
                                        <HeaderStyle Width="80px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="center" Width="80px"></ItemStyle>
                                    </asp:BoundColumn>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                                    Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                                </PagerStyle>
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="cmdSave" runat="server" CssClass="ButtonCommand" Text="Ghi" TabIndex="10">
                            </asp:Button>&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 5px;">
                <img src="" width="10px" />
            </td>
        </tr>
    </table>
</asp:Content>

