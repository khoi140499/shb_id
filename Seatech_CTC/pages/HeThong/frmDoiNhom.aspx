﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmDoiNhom.aspx.vb" Inherits="pages_HeThong_frmDoiNhom" Title=" Gán nhóm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table id="Table1" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
        <tr>
            <td style="width: 10px;">
                <img src="">
            </td>
            <td style="width: 620px; height: 351px;" valign="top" align="center">
                <table id="Content" cellspacing="1" cellpadding="1" border="0" style="width: 100%"
                    bgcolor="#ffffff">
                    <tr>
                        <td  class="pageTitle" style="height: 30px">
                            <asp:Label ID="Label1" runat="server" Font-Bold="True">PHÂN QUYỀN THEO NHÓM</asp:Label>
                        </td>
                    </tr>
                    <tr>
                            <td>
                                <table width="100%" border="0" cellpadding="2" cellspacing="1" class="form_input">
                                
                              
                    <tr style="height:40px">
                    <td align="left" class="form_label">
                    <asp:Label runat="server" ID="Label3"  Font-Bold="True" Font-Italic="true" ForeColor="#003399"  >Danh sách người sử dụng</asp:Label>
                    </td>
                    <td align="center" class="form_label">
                    <asp:Label runat="server" ID="Label2"  Font-Bold="True" Font-Italic="true" ForeColor="#003399"  >Danh sách nhóm người sử dụng</asp:Label>
                    </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 60%" valign="top">
                            <asp:DataGrid ID="dgUserAccount" runat="server" AutoGenerateColumns="False" TabIndex="9"
                                Width="100%" AllowPaging="True" BorderColor="#989898" CssClass="grid_data" Height="100">
                                <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                                <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
                                <ItemStyle CssClass="grid_item" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="T&#234;n đăng nhập" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ten_DN") %>'
                                                NavigateUrl='<%#"~/pages/HeThong/frmDoinhom.aspx?ID=" & DataBinder.Eval(Container.DataItem, "Ten_DN") %>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Ten_DN" Visible="false" HeaderText="Tên đăng nhập">
                                        <HeaderStyle Width="0px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="0px"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Ten" HeaderText="Họ tên">
                                        <HeaderStyle></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Chuc_Danh" HeaderText="Chức danh">
                                        <HeaderStyle Width="80px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    </asp:BoundColumn>
                                    <%-- <asp:BoundColumn DataField="Ban_LV" HeaderText="Bàn số">
                                            <HeaderStyle Width="80px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="local_ip" HeaderText="Địa chỉ IP">
                                            <HeaderStyle Width="80px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                                        </asp:BoundColumn>--%>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                                    Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                                </PagerStyle>
                            </asp:DataGrid>
                        </td>
                        <td valign="top" align="left" class="form_label">
                            <asp:RadioButtonList ID="raoGroups" runat="Server" DataTextField="TEN_NHOM" DataValueField="MA_NHOM" />
                            <%--<asp:DataGrid ID="dtgChucNang" runat="server" AutoGenerateColumns="False" TabIndex="9"
                                Width="100%" BorderColor="#989898" CssClass="grid_data" Height="100">
                                <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                                <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
                                <ItemStyle CssClass="grid_item" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Chọn">
                                        <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="MA_NHOM" Visible="false" HeaderText="Mã nhóm">
                                        <HeaderStyle Width="0"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="TEN_NHOM" HeaderText="Tên nhóm">
                                        <HeaderStyle Width="80px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="80px"></ItemStyle>
                                    </asp:BoundColumn>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                                    Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                                    Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                                </PagerStyle>
                            </asp:DataGrid>--%>
                        </td>
                    </tr>
                      </table>
                            </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="cmdSave" runat="server" CssClass="ButtonCommand" Text="Ghi" TabIndex="10">
                            </asp:Button>&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 5px;">
                <img src="" width="10px" />
            </td>
        </tr>
    </table>
</asp:Content>
