﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmThamSoHT.aspx.vb" Inherits="pages_HeThong_frmThamSoHT" Title="Tham số hệ thống" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr align="center">
            <td class="pageTitle">
                <asp:Label ID="Label2" runat="server">THAM SỐ HỆ THỐNG</asp:Label>
            </td>
        </tr>
        <tr style="height: 80">
            <td align="right" valign="top" style="height: 30">
                <asp:DropDownList ID="cboDSDiemThu" AutoPostBack="true" Width="150px" runat="server"
                    CssClass="inputflat" TabIndex="28">
                </asp:DropDownList>
            </td>
        </tr>
        <tr align="center">
            <td valign="top">
                <asp:DataGrid ID="grdTsRieng" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                    Width="100%" CssClass="grid_data">
                    <HeaderStyle CssClass="grid_header"></HeaderStyle>
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="TEN_TS" HeaderText="Ten TS" Visible="false">
                            <HeaderStyle HorizontalAlign="Center" Width="0"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="MO_TA" HeaderText="Tham số" ItemStyle-HorizontalAlign="Left">
                            <HeaderStyle HorizontalAlign="Center" Width="30%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Gi&#225; trị ">
                            <HeaderStyle HorizontalAlign="Center" Width="40%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                                <asp:TextBox ID="GIATRI_TS" runat="server" Width="99%" BorderColor="white" CssClass="inputflat"
                                    Text='<%# DataBinder.Eval(Container.DataItem, "GIATRI_TS") %>'></asp:TextBox></ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Right" Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" height="30">
                    <tr>
                        <td align="left">
                            &nbsp;
                        </td>
                        <td align="right">
                            <asp:Button ID="cmdSave" TabIndex="0" runat="server" Text="Ghi" CssClass="ButtonCommand">
                            </asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
