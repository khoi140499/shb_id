﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmDM_LHXNK.aspx.vb" Inherits="pages_HeThong_frmDM_LHXNK" title="Danh mục loại hình XNK" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
     <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">DANH MỤC LOẠI HÌNH XNK</asp:Label>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="btnKetXuat" runat="server" Text="Kết xuất" UseSubmitBehavior="false">
                </asp:Button>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:GridView ID="grdChungTu" runat="server" Width="100%"  CssClass="grid_data" HeaderStyle-CssClass="grid_header"
                    RowStyle-CssClass="grid_item" AlternatingRowStyle-CssClass="grid_item_alter"
                    AutoGenerateColumns="false"
                    EmptyDataText="Không có dữ liệu">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>STT</HeaderTemplate>
                            <ItemTemplate>
                                <%#Container.DataItemIndex + 1%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>Mã loại hình XNK</HeaderTemplate>
                            <ItemTemplate>
                                &nbsp;<%#Eval("MA_LH")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TEN_LH" HeaderText="Tên loại hình XNK"/>
                        <asp:BoundField DataField="NGAY_ANHAN" HeaderText="Ngày bảo lãnh tối đa"/>
                    </Columns>
                    <HeaderStyle CssClass="grid_header" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>

