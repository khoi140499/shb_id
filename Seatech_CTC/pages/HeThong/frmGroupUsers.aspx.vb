﻿Imports Business.Common.mdlCommon
Imports Business
Imports System.Data
Imports VBOracleLib

Partial Class pages_HeThong_frmGroupUsers
    Inherits System.Web.UI.Page
    Dim infGUser As New HeThong.infGroupUsers
    Dim buGUser As New HeThong.buGroupUsers
    Dim buThamSo As New HeThong.buThamso

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            ' Duc Anh : Hiện nút theo quyền maker/checker đã gán : Start
            Dim strChecker As String = HeThong.buThamso.ThamSoHT_GetValue("NHOM_CHECKER")
            Dim strMaker As String = HeThong.buThamso.ThamSoHT_GetValue("NHOM_MAKER")
            Dim strViewer As String = HeThong.buThamso.ThamSoHT_GetValue("NHOM_VIEWER")

            If strMaker.Contains(Session.Item("MA_NHOM").ToString()) Then
                cmdTaoMoi.Visible = True
                cmdDeleteUser.Visible = True
            End If

            If strChecker.Contains(Session.Item("MA_NHOM").ToString()) Then
                cmdDuyet.Visible = True
                cmdDeleteUser.Visible = False
            End If

            If (strChecker.Contains(Session.Item("MA_NHOM").ToString()) = False And strMaker.Contains(Session.Item("MA_NHOM").ToString()) = False) Or (strViewer.Contains(Session.Item("MA_NHOM").ToString())) Then
                cmdDeleteUser.Visible = False
                cmdTaoMoi.Visible = False
                cmdDuyet.Visible = False
            End If

            If Session.Item("UserName").ToString() = "admin" Then
                cmdTaoMoi.Visible = True
                cmdDeleteUser.Visible = True
                cmdDuyet.Visible = True
            End If
            ' Duc Anh : Hiện nút theo quyền maker/checker đã gán : End

            LoadGrid()
            cmdTaoMoi.Enabled = True
            If Not Request.Params("id") Is Nothing Then
                txtMaNhom.Enabled = False
                infGUser.MaNhom = Request.Params("id").Trim
                Load_Detail()
            End If
            'cmdTaoMoi.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn thực hiện không?')")
            'cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn thực hiện không?')")
        End If
    End Sub

    Protected Function GetActonString(ByVal strAction As String) As String
        Dim strResult As String = ""
        Select Case strAction
            Case "INSERT"
                strResult = "Thêm mới"
            Case "UPDATE"
                strResult = "Cập nhật"
            Case "DELETE"
                strResult = "Xóa"
        End Select
        Return strResult
    End Function

    Protected Sub LoadGrid()
        Dim dt As DataTable
        Try
            If txtMaNhom.Text.Trim <> "" Then
                infGUser.MaNhom = txtMaNhom.Text.Trim
            End If

            If Not txtTenNhom.Text.Trim.Equals("") Then
                infGUser.TenNhom = txtTenNhom.Text.Trim
            End If

            If drlDataStatus.SelectedValue = "CurrentData" Then
                dtgdata.Visible = True
                dgApproveAction.Visible = False
                cmdDuyet.Visible = False

                dt = buGUser.Search(infGUser)

                If dt.Rows.Count > 0 Then
                    dtgdata.DataSource = dt
                    dtgdata.DataBind()
                Else
                    dgApproveAction.DataSource = Nothing
                    dgApproveAction.DataBind()
                    clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
                End If
            Else
                dtgdata.Visible = False
                dgApproveAction.Visible = True
                cmdDuyet.Visible = True

                dt = buGUser.SearchTempData(infGUser)

                If dt.Rows.Count > 0 Then
                    dgApproveAction.DataSource = dt
                    dgApproveAction.DataBind()
                Else
                    dgApproveAction.DataSource = Nothing
                    dgApproveAction.DataBind()
                    clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
                End If
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub

    Protected Sub Load_Detail()
        ' Duc Anh : Hiển thị radio button tương ứng với quyền được gán : Start

        Dim strChecker As String = HeThong.buThamso.ThamSoHT_GetValue("NHOM_CHECKER")
        Dim strMaker As String = HeThong.buThamso.ThamSoHT_GetValue("NHOM_MAKER")
        Dim strViewer As String = HeThong.buThamso.ThamSoHT_GetValue("NHOM_VIEWER")
        Dim strMa_Nhom As String = Session.Item("MA_NHOM").ToString()
        If strMaker.Contains(Request.Params("id").Trim) Then
            rdMaker.Checked = True
        End If

        If strChecker.Contains(Request.Params("id").Trim) Then
            rdChecker.Checked = True
        End If

        If (strChecker.Contains(Request.Params("id").Trim) = False And strMaker.Contains(Request.Params("id").Trim) = False) Or (strViewer.Contains(Request.Params("id").Trim)) Then
            rdViewer.Checked = True
        End If
        ' Duc Anh : Hiển thị radio button tương ứng với quyền được gán : End

        Dim dt As DataTable
        dt = buGUser.Search(infGUser)
        If dt.Rows.Count > 0 Then
            txtMaNhom.Text = dt.Rows(0)("MA_NHOM").ToString
            txtTenNhom.Text = dt.Rows(0)("TEN_NHOM").ToString

            dtgdata.DataSource = dt
            dtgdata.DataBind()
        End If
    End Sub

    Private Sub xoaText()
        infGUser = New HeThong.infGroupUsers
        txtMaNhom.Text = ""
        txtTenNhom.Text = ""
    End Sub
    Private Function CheckExist_MaNhom(ByVal ma_nhom As String) As Boolean
        Dim blnResult As Boolean = True
        Dim conn As DataAccess
        Dim dr As IDataReader
        Dim strSql As String
        Try
            conn = New DataAccess
            strSql = "SELECT * FROM TCS_DM_NHOM " & _
                    "WHERE ma_nhom ='" & ma_nhom & "' "
            dr = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
            If dr.Read Then
                blnResult = True
            Else
                blnResult = False
            End If
        Catch ex As Exception
            blnResult = False
            Throw ex
        Finally
            If Not dr Is Nothing Then
                dr.Dispose()
            End If
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
        End Try
        Return blnResult
    End Function

    Protected Sub cmdTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        ' Duc Anh : Gán quyền Checker, Maker, Viewer cho nhóm : Start
        Dim strPermission As String = ""

        If rdMaker.Checked Then
            strPermission += "Maker;"
        End If

        If rdChecker.Checked Then
            strPermission += "Checker;"
        End If

        If rdViewer.Checked Then
            strPermission += "Viewer;"
        End If
        ' Duc Anh : Gán quyền Checker, Maker, Viewer cho nhóm : End 

        If txtMaNhom.Text.Trim.Length = 0 Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào mã nhóm")
            txtMaNhom.Focus()
            Exit Sub
        Else
            If Regex.IsMatch(txtMaNhom.Text.Trim, "^[a-zA-Z0-9]*$") = False Then
                clsCommon.ShowMessageBox(Me, "Chuỗi bạn nhập không đúng - Có ký tự đặc biệt.")
                txtMaNhom.Focus()
                Exit Sub
            End If
        End If
        If CheckExist_MaNhom(txtMaNhom.Text.Trim) And Request.Params("ID") Is Nothing Then
            clsCommon.ShowMessageBox(Me, "Mã nhóm đã tồn tại, vui lòng kiểm tra lại.!")
            Exit Sub
        End If
        If txtTenNhom.Text.Trim.Length = 0 Then
            clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào tên nhóm")
            txtTenNhom.Focus()
            Exit Sub
        End If

        infGUser.MaNhom = txtMaNhom.Text.Trim
        infGUser.TenNhom = txtTenNhom.Text.Trim
        infGUser.GroupPermission = strPermission

        Try
            If Not Request.Params("ID") Is Nothing Then
                'If Not buGUser.Update(infGUser) Then
                '    clsCommon.ShowMessageBox(Me, "Có lỗi khi sửa nhóm người sử dụng")
                'Else
                '    If HeThong.buThamso.ThamSoHT_CheckExist(strTen_TS_Nhom) Then
                '        HeThong.buThamso.ThamSoHT_UpdateValue(strTen_TS_Nhom, strPermission)
                '    Else
                '        HeThong.buThamso.ThamSoHT_Insert(strTen_TS_Nhom, strPermission, "", "", "")
                '    End If
                'End If

                If buGUser.InsertTempAction(infGUser, Session.Item("User").ToString(), "UPDATE") Then
                    clsCommon.ShowMessageBox(Me, "Cập nhật thành công và đang chờ xử lý")
                Else
                    clsCommon.ShowMessageBox(Me, "Thao tác thất bại. Vui lòng thử lại sau")
                End If
            Else
                If buGUser.InsertTempAction(infGUser, Session.Item("User").ToString(), "INSERT") Then
                    clsCommon.ShowMessageBox(Me, "Cập nhật thành công và đang chờ xử lý")
                Else
                    clsCommon.ShowMessageBox(Me, "Thao tác thất bại. Vui lòng thử lại sau")
                End If
                'If Not buGUser.Insert(infGUser) Then
                '    clsCommon.ShowMessageBox(Me, "Có lỗi khi thêm mới nhóm người sử dụng")
                'Else
                '    HeThong.buThamso.ThamSoHT_Insert(strTen_TS_Nhom, strPermission, "", "", "")
                'End If
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Thực hiện không thành công !")
            Return
        End Try
        'clsCommon.ShowMessageBox(Me, "Thực hiện thành công")

        xoaText()
        LoadGrid()
        txtMaNhom.Enabled = True

        rdChecker.Checked = False
        rdMaker.Checked = False
        rdViewer.Checked = False
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        xoaText()
        cmdTaoMoi.Enabled = True
        txtMaNhom.Enabled = True
        If Not Request.Params("ID") Is Nothing Then
            Response.Redirect("~/pages/HeThong/frmGroupUsers.aspx", False)
        End If
        LoadGrid()
    End Sub

    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim i As Integer
        Dim v_flag As Boolean = True
        Dim v_count As Integer = 0
        Dim success_Count As Integer = 0
        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                infGUser.MaNhom = Trim(dtgdata.Items(i).Cells(0).Text)
                infGUser.TenNhom = Trim(dtgdata.Items(i).Cells(2).Text)

                If buGUser.InsertTempAction(infGUser, Session.Item("User").ToString(), "DELETE") Then
                    success_Count += 1
                End If
                v_count += 1
            End If
        Next
        xoaText()
        LoadGrid()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Đã gửi yêu cầu xử lý xóa " & success_Count.ToString() & " nhóm đã chọn")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        LoadGrid()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub

    Protected Sub cmdSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        LoadGrid()
        cmdTaoMoi.Enabled = False
    End Sub

    Protected Sub cmdDuyet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDuyet.Click
        Dim success_count As Integer = 0
        Dim checked_count As Integer = 0

        For i = 0 To dgApproveAction.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dgApproveAction.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                Dim strAction_ID = Trim(dgApproveAction.Items(i).Cells(0).Text)
                If buGUser.ApproveTempData(strAction_ID, Session.Item("User").ToString()) Then
                    success_count += 1
                End If
                checked_count += 1
            End If
        Next

        If checked_count > 0 Then
            If success_count > 0 Then
                clsCommon.ShowMessageBox(Me, "Bạn đã duyệt thành công " & success_count.ToString() + " hành động")
            Else
                clsCommon.ShowMessageBox(Me, "Duyệt hành động thất bại. Vui lòn thử lại sau")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Bạn phải chọn ít nhất một hành động để duyệt")
        End If
        LoadGrid()
    End Sub
End Class
