﻿Imports System
Imports System.Drawing
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports Business.HeThong
Imports System.Data
Partial Class pages_HeThong_frmDoiNhom
    Inherits System.Web.UI.Page
    Private mblnMove As Boolean = False
    Private mstrMa_NV As String = ""
    Public Property TCS_Ma_NV() As String
        Get
            Return mstrMa_NV
        End Get
        Set(ByVal Value As String)
            mstrMa_NV = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdSave.Click, AddressOf cmdSave_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            load_Datagrid()
            mblnMove = True
            load_dtgChucNang(Session.Item("User").ToString)
            mblnMove = False
            If Request.Params("ID") <> "" Then
                load_dtgChucNang(Request.Params("ID").Trim())
            End If
        End If

    End Sub
    Private Sub load_Datagrid()
        Dim ds As New DataSet
        Try
            ds = buUsers.TCS_LST_DM_NHANVIEN()
            dgUserAccount.DataSource = ds.Tables(0)
            dgUserAccount.DataBind()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu")
        End Try
    End Sub

    Private Sub load_dtgChucNang(ByVal mstrTmpMa_NV As String)
        Dim cnQnhom As DataAccess
        Dim ds As New DataSet
        Dim strSQL As String
        ' Trong trường hợp đang load dữ liệu thì không load quyền
        If mblnMove Then Return
        Try
            cnQnhom = New DataAccess
            strSQL = "SELECT 1 AS ING, a.MA_NHOM,TEN_NHOM  FROM TCS_DM_NHOM a,tcs_dm_nhanvien b " & _
                        "where instr(b.ma_nhom ,a.ma_nhom)>0 and b.ten_dn='" & mstrTmpMa_NV & "'" & _
                        " UNION " & _
                        "SELECT 0 AS ING, a.MA_NHOM,TEN_NHOM  FROM TCS_DM_NHOM a,tcs_dm_nhanvien b " & _
                        "where instr(b.ma_nhom,a.ma_nhom)<=0  and b.ten_dn='" & mstrTmpMa_NV & "'" & _
                        " ORDER BY ma_nhom"
            ds = cnQnhom.ExecuteReturnDataSet(strSQL, CommandType.Text)
            raoGroups.DataSource = ds
            raoGroups.DataBind()
            For i As Integer = 0 To ds.Tables(0).Rows.Count() - 1
                If ds.Tables(0).Rows(i)("ING") = 1 Then
                    raoGroups.Items(i).Selected = True
                End If
            Next
        Catch ex As Exception
            'Lỗi kết nối cơ sở dữ liệu
            clsCommon.ShowMessageBox(Me, "Lỗi kết nối CSDL")
        Finally
            If Not ds Is Nothing Then
                ds.Dispose()
            End If
            If Not cnQnhom Is Nothing Then
                cnQnhom.Dispose()
            End If
        End Try
    End Sub

    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        '-----------------------------------------------------
        ' Mục đích: Cập nhật những thay đổi nhóm NSD cho NSD
        '           cho những dùng được lựa chọn ở listBox
        ' Tham số: 
        ' Giá trị trả về: 
        ' Ngày viết: 16/10/2007
        ' Người viết: Nguyễn Bá Cường
        ' ----------------------------------------------------
        Dim cnNv As DataAccess
        Dim strItem As String = ""
        Dim strSQL As String
        Dim i As Integer
        Dim obj As CheckBox
        Try
            obj = New CheckBox()
            mstrMa_NV = Request.Params("id")

            For i = 0 To raoGroups.Items.Count - 1
                If raoGroups.Items(i).Selected Then
                    If strItem <> "" Then strItem = strItem & "/"
                    strItem = strItem & raoGroups.Items(i).Value.Trim
                    If "".Equals(strItem) Then
                        clsCommon.ShowMessageBox(Me, "Bạn phải chọn ít nhất một nhóm.")
                        Exit Sub
                    End If
                    strSQL = "Update TCS_DM_NHANVIEN Set MA_NHOM= '" & strItem & _
                    "' where ten_DN ='" & mstrMa_NV & "'"
                    cnNv = New DataAccess
                    cnNv.ExecuteNonQuery(strSQL, CommandType.Text)
                End If
            Next

            clsCommon.ShowMessageBox(Me, "Cập nhật người sử dụng cho nhóm thành công !")
        Catch ex As Exception
            ' Lỗi xoá dữ liệu không thành công
            clsCommon.ShowMessageBox(Me, gstrLoiCapNhatDL)
        Finally
            If Not cnNv Is Nothing Then
                cnNv.Dispose()
            End If
        End Try
    End Sub

    Protected Sub dgUserAccount_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgUserAccount.PageIndexChanged
        load_Datagrid()
        dgUserAccount.CurrentPageIndex = e.NewPageIndex
        dgUserAccount.DataBind()
    End Sub
End Class
