﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmUsers.aspx.vb" Inherits="pages_HeThong_frmUsers" Title="Thông tin người dùng" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>

    <script language="javascript" src="../../images/javascript/jquery/script/default/jquery-1.4.3.min.js"
        type="text/javascript"></script>
    <script type="text/javascript" src="../../javascript/seatech_ctc/MyTab.js"></script>

    <link type="text/css" href="../../css/tabStyle.css" rel="stylesheet" />

    <script type="text/javascript">
    function valInteger (obj)
    {
        var i, strVal, blnChange;
        blnChange = false
        strVal = "";
        
        for (i = 0; i < (obj.value).length; i++)
        {
            switch (obj.value.charAt(i))
            {
                case "0":
                case "1":
                case "2":
                case "3":
                case "4":
                case "5":
                case "6":
                case "7":
                case "8":
                case "9": strVal = strVal + obj.value.charAt(i);
                    break;
                default: blnChange = true;
                    break;
            }
        }
        
        if (blnChange)
            obj.value = strVal;
    }
//    function removeEventHandler(elem,eventType,handler) {
//         if (elem.removeEventListener) 
//            elem.removeEventListener (eventType,handler,false);
//         if (elem.detachEvent)
//            elem.detachEvent ('on'+eventType,handler); 
//    }
//    var b1 = document.getElementById('<%=cmdTaoMoi.ClientID %>');
//    removeEventHandler(b1,'click',handlerFunction1);
     function ShowLov(strType)
    {
        if (strType=="CNNganHang") return FindDanhMuc('CNNganHang','<%=txtMaCN.ClientID %>','<%=txtTenCN.ClientID %>');         
                          
    }
      function FindDanhMuc(strPage,txtID,txtTenDM) 
    {        
        var returnValue = window.showModalDialog("../../Find_DM/Find_DanhMuc.aspx?page=" + strPage +"&initParam=" + document.getElementById(txtID).value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;","_new");
        
                
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTenDM).value = returnValue.Title;
                
            
        
        //NextFocus();
    }
    function jsGet_TenCN_NH(){
        if (document.getElementById ('<%= txtMaCN.ClientID %>').value != ''){
            PageMethods.GetURLSignature($get('<%= txtMaCN.ClientID %>').value,FSuccess,FError);
        }
    }
    
    function FSuccess(result){
        var url=result;
        if (url.length > 0){
            document.getElementById ('<%= txtTenCN.ClientID %>').value = url ;
        }
        else {
            alert('Không tìm được chi nhánh!');
        }
        
    }
    function FError(result){
       //alert('Xảy ra lỗi trong quá trình lấy thông tin Chi nhánh.' );
    }
    
    function jsCalTotal(){
    
        if ($get('<%= txtDenHanMuc.ClientID %>').value.length > 0) {            
                    jsFormatNumber('<%= txtDenHanMuc.ClientID %>');
                } 
        if ($get('<%= txtTuHanMuc.ClientID %>').value.length > 0) {            
                    jsFormatNumber('<%= txtTuHanMuc.ClientID %>');
                } 
    }
    function jsFormatNumber(txtCtl){
        document.getElementById(txtCtl).value = localeNumberFormat(document.getElementById(txtCtl).value.replaceAll('.',''),'.');
    }
         
    function localeNumberFormat(amount,delimiter) {
	    try {
	   
		    amount = parseFloat(amount);
	    }catch (e) {
		    throw ('localeNumberFormat caused INVALID FLOAT with value ' + amount)
		    return null;
	    }
	    if (delimiter == null || delimiter == undefined) { delimiter = '.'; }
	    
	    // convert to string
	    if (amount.match != 'function') { amount = amount.toString(); }

	    // validate as numeric
	    var regIsNumeric = /[^\d,\.-]+/igm;
	    var results = amount.match(regIsNumeric);

	    if (results != null) {
		    outputText('INVALID NUMBER', eOutput)
		    return null;
	    }

	    var minus = amount.indexOf('-') >= 0 ? '-' : '';
	    amount = amount.replace('-', '');
	    var amtLen = amount.length;
	    var decPoint = amount.indexOf(',');
	    var wholeNumberEnd = decPoint > 0 ? amtLen - (amtLen - decPoint) : amtLen;

	    var wholeNumber = amount.substr(0, wholeNumberEnd);
	    
	    var numberEnd=amount.substr(decPoint,amtLen);
	    var fraction = amount.substr(wholeNumberEnd, amtLen - wholeNumberEnd);

	    var segments = (wholeNumberEnd - (wholeNumberEnd % 3)) / 3;
	    var rvsNumber = wholeNumber.split('').reverse().join('');
	    var output = '';

	    for (i = 0; i < wholeNumberEnd; i++) {
		    if (i % 3 == 0 && i != 0 && i != wholeNumberEnd) { output += delimiter; }
		    output += rvsNumber.charAt(i);
	    }
	    output = minus +  output.split('').reverse().join('') + fraction ;
	    return output;
    }    
       
    String.prototype.replaceAll = function(strTarget,strSubString)
    {
        var strText = this;
        var intIndexOfMatch = strText.indexOf( strTarget );
        while (intIndexOfMatch != -1){
            strText = strText.replace( strTarget, strSubString )
            intIndexOfMatch = strText.indexOf( strTarget );
        }
        return( strText );
    } 
 
    function mask(str,textbox,loc,delim){
        var locs = loc.split(',');
        for (var i = 0; i <= locs.length; i++){
	        for (var k = 0; k <= str.length; k++){
	            if (k == locs[i]){
	            if (str.substring(k, k+1) != delim){
	                    str = str.substring(0,k) + delim + str.substring(k,str.length)
	                }
	            }
	        }
        }
        textbox.value = str
    }
    </script>

    <style type="text/css">
        .ButtonCommand
        {
            height: 26px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table id="Table1" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">QUẢN LÝ NGƯỜI DÙNG</asp:Label>
            </td>
        </tr>
        
        <%--<tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
            
        </tr>--%>
        <%--<tr>
            <td>
                <table id="table3" runat="server" cellpadding="1" cellspacing="1" border="0" width="100%" class="form_input">
                    <tr align="right">
                        <td class="form_control" colspan="2">--%>
        <%--</td>
                    </tr>
                    <tr></tr>
                </table>
            </td>
        </tr>--%>
    </table>
    <br />
    <ul class="tabs" id="tabs">
        <li><a href="#" class="current" id="tab1_link">
            <asp:Label ID="tabCustomer" runat="server" Text="Thông tin tài khoản"></asp:Label></a></li>
        <%--<li><a href="#" id="tab2_link">
            <asp:Label ID="tabPassword" runat="server" Text="Mật khẩu"></asp:Label></a></li>--%>
    </ul>
    <div class="tab_current" id="tab1">
        <div id="DIVTT" runat="server">
            <table border="0" cellspacing="1" cellpadding="1" width="100%" style="text-align: left;"
                class="form_input">
                <tr style="display:none">
                    <td >
                        <asp:Label ID="lblCustNo" runat="server" Text="Mã người dùng" CssClass="label"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtMaND" runat="server" Width="300px" CssClass="inputflat"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblUserName" runat="server" Text="Tên đăng nhập" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:TextBox ID="txtTenND" runat="server" Width="300px" CssClass="inputflat"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblCustName" runat="server" Text="Tên người dùng" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                    
                        <asp:TextBox ID="txtHoTen" runat="server" Width="300px" CssClass="inputflat"></asp:TextBox>
                    </td>
                </tr>
                <tr style="visibility:hidden;display:none">
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblBdsNo" runat="server" Text="Mã BDS" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:TextBox ID="txtMaBDS" runat="server" Width="300px" CssClass="inputflat"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblBranchNo" runat="server" Text="Chi nhánh" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:TextBox ID="txtMaCN" runat="server" Width="150px" CssClass="inputflat" style="background: Aqua" 
                        onkeypress="if (event.keyCode==13){ShowLov('CNNganHang');}" onblur="jsGet_TenCN_NH();"></asp:TextBox>
                        <asp:TextBox ID="txtTenCN" runat="server" Width="300px" CssClass="inputflat" ReadOnly="true" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblPosition" runat="server" Text="Chức danh" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:TextBox ID="txtChucdanh" runat="server" Width="300px" CssClass="inputflat"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblGroup" runat="server" Text="Thuộc nhóm" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:DropDownList ID="cboNhomND" runat="server" Width="300px" CssClass="inputflat" AutoPostBack="true" >
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr runat="server" id="trHMTu">
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblHanMucTu" runat="server" Text="Hạn mức từ" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:TextBox ID="txtTuHanMuc" runat="server" Width="300px" CssClass="inputflat" onblur="jsCalTotal();"></asp:TextBox>
                    </td>
                </tr>
                <tr runat="server" id="trHMDen">
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblHanMucDen" runat="server" Text="Hạn mức đến" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:TextBox ID="txtDenHanMuc" runat="server" Width="300px" CssClass="inputflat" onblur="jsCalTotal();"></asp:TextBox>
                    </td>
                </tr>
                <tr style="visibility:hidden;display:none">
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblCollectionPoint" runat="server" Text="Kho bạc mặc định" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:DropDownList ID="cboDiemThu" runat="server" Width="300px" CssClass="inputflat">
                        </asp:DropDownList>
                    </td>
                </tr>
                
                <tr>
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="Label2" runat="server" Text="Kiểu đăng nhập" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:DropDownList ID="ddlKieuDN" runat="server" Width="300px" CssClass="inputflat" AutoPostBack="true" >
                        <asp:ListItem Text="<< Tất cả >>" Value="" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Thông qua LDAP" Value="0" ></asp:ListItem>
                        <asp:ListItem Text="Đăng nhập bình thường" Value="1" ></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr runat="server" id="UserDomain">
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="Label3" runat="server" Text="Domain" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:TextBox ID="txtUserDomain" runat="server" Width="300px" CssClass="inputflat" ></asp:TextBox>
                    </td>
                </tr>
                <tr id="PassWord" runat="server">
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblPassword" runat="server" Text="Mật khẩu" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:TextBox ID="txtMatKhau" runat="server" Width="300px" CssClass="inputflat" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr id="RePassWord" runat="server">
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblRetPassword" runat="server" Text="Xác nhận mật khẩu" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:TextBox ID="txtXacNhanMK" runat="server" Width="300px" CssClass="inputflat"
                            TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="Label1" runat="server" Text="Trạng thái" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:DropDownList ID="cboTrangThai" runat="server" Width="300px" CssClass="inputflat">
                            <asp:ListItem Text="<< Tất cả >>" Value="" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Đang hoạt động" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Ngừng hoạt động" Value="1" ></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="Label4" runat="server" Text="Trạng thái dữ liệu" CssClass="label"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="cboTrangThaiDuLieu" runat="server" Width="300px" CssClass="inputflat">
                            <asp:ListItem Text="Dữ liệu hiện tại" Value="CurrentData"></asp:ListItem>
                            <asp:ListItem Text="Dữ liệu thay đổi" Value="DataChanged" ></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <%--<div class="tab" id="tab2">
        <div id="DIVMatkhau" runat="server">
            <table border="0" cellspacing="1" cellpadding="1" width="100%" style="text-align: left;"
                class="form_input">
                <tr>
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblPassword" runat="server" Text="Mật khẩu" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:TextBox ID="txtMatKhau" runat="server" Width="300px" CssClass="inputflat" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width: 20%;" class="form_label">
                        <asp:Label ID="lblRetPassword" runat="server" Text="Xác nhận mật khẩu" CssClass="label"></asp:Label>
                    </td>
                    <td class="form_label">
                        <asp:TextBox ID="txtXacNhanMK" runat="server" Width="300px" CssClass="inputflat"
                            TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
    </div>--%>
    <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="cmdCancel" runat="server" CssClass="ButtonCommand" Text="Làm mới" UseSubmitBehavior="false"></asp:Button>&nbsp; &nbsp;
                <asp:Button ID="cmdTaoMoi" Visible="true" runat="server" CssClass="ButtonCommand" Text="Ghi" UseSubmitBehavior="false"></asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdDeleteUser" runat="server" Visible="true" CssClass="ButtonCommand" Text="Xóa" UseSubmitBehavior="false"></asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdKhoiPhuc" runat="server" Visible="false" CssClass="ButtonCommand" Text="Khôi phục" UseSubmitBehavior="false"></asp:Button>
                <asp:Button ID="cmdDuyet" runat="server" Visible="true" CssClass="ButtonCommand" Text="Duyệt" UseSubmitBehavior="false"></asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdTimKiem" runat="server" CssClass="ButtonCommand" Text="Tìm kiếm" UseSubmitBehavior="false"></asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox Style="visibility: hidden" runat="server" ID="hdnbox"></asp:TextBox>
                <asp:TextBox Style="visibility: hidden" runat="server" ID="hdnbox1"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:DataGrid ID="dgUserAccount" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="99%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle  CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="Ma_NV" HeaderText="Ma_NV " Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Tên ĐN" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ten_DN") %>'
                                    NavigateUrl='<%#"~/pages/HeThong/frmUsers.aspx?ID=" & DataBinder.Eval(Container.DataItem, "Ten_DN") & "&MANV=" & DataBinder.Eval(Container.DataItem, "Ma_NV") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <HeaderStyle Width="70px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="Ten_DN" Visible="false" HeaderText="Tên ĐN">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Ten" HeaderText="Họ tên">
                            <HeaderStyle Width="150px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                       <%-- <asp:BoundColumn DataField="TELLER_ID" HeaderText="Mã Teller">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>--%>
                        <asp:BoundColumn DataField="Chuc_Danh" HeaderText="Chức danh">
                            <HeaderStyle Width="70px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <%-- <asp:BoundColumn DataField="Ban_LV" HeaderText="Điểm thu">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>--%>
                        <asp:BoundColumn DataField="Ma_CN" HeaderText="Mã chi nhánh">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TINH_TRANG" HeaderText="Trạng thái">
                            <HeaderStyle Width="80px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center"></ItemStyle>
                        </asp:BoundColumn>
                        
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid>
                <input type="hidden" runat="server" id="Hidden1" />
                <asp:Label ID="lbSua" Visible="false" CssClass="label" runat="server">Sửa</asp:Label>
                &nbsp;&nbsp;
                <asp:DropDownList ID="cboLoaiSua" Visible="false" runat="server" AutoPostBack="true"
                    TabIndex="10" CssClass="inputflat">
                    <asp:ListItem Value="0" Text="Tất cả"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Mật Khẩu"></asp:ListItem>
                    <asp:ListItem Value="2" Text="Thông tin"></asp:ListItem>
                </asp:DropDownList>
                <asp:DataGrid ID="dgApproveAction" runat="server" AutoGenerateColumns="False" TabIndex="9"
                    Width="99%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True" Visible="false">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle  CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:BoundColumn DataField="ACTION_ID" HeaderText="Hành động" Visible="False">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Hành động">
                            <ItemTemplate>
                                <%#GetActonString(Eval("ACTION").ToString())%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="NGUOI_TAO" HeaderText="Người thực hiện" Visible="True">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Ma_NV" HeaderText="Ma_NV " Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Ten_DN" HeaderText="Ten_DN " Visible="false">
                            <HeaderStyle Width="0"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="0"></ItemStyle>
                        </asp:BoundColumn>
                        
                        <asp:BoundColumn DataField="Ten_DN" Visible="false" HeaderText="Tên ĐN">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Ten" HeaderText="Họ tên">
                            <HeaderStyle Width="150px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Chuc_Danh" HeaderText="Chức danh">
                            <HeaderStyle Width="70px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>

                        <asp:BoundColumn DataField="Ma_CN" HeaderText="Mã chi nhánh">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Chọn">
                            <ItemStyle HorizontalAlign="Center" Width="30px"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                        Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                        Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
                    </PagerStyle>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>
