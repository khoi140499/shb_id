﻿Imports System.Data
Imports VBOracleLib
Imports System.IO
Partial Class pages_HeThong_frmDM_LHXNK
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            loadGrid()
        End If
    End Sub
    Protected Sub loadGrid()
        Try
            Dim ds As DataTable
            Dim sql As String = "select * from tcs_dm_lhinh order by ma_lh desc"
            ds = DataAccess.ExecuteToTable(sql)
            If ds.Rows.Count > 0 Then
                grdChungTu.DataSource = ds
                grdChungTu.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub

    Protected Sub btnKetXuat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKetXuat.Click
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=DM_LoaiHinh_XNK.xls")
        Response.Charset = "utf-8"
        Response.ContentType = "application/vnd.ms-excel"
        Using sw1 As New StringWriter()
            Dim hw1 As New HtmlTextWriter(sw1)
            grdChungTu.RenderControl(hw1)
            Response.Output.Write(sw1.ToString)
            Response.Flush()
            Response.End()
        End Using
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
End Class
