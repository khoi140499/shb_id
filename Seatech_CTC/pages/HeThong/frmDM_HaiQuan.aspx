﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmDM_HaiQuan.aspx.vb" Inherits="pages_HeThong_frmDM_HaiQuan" title="Danh mục hải quan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
     <table id="Table4" cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="lblTitle" runat="server">DANH MỤC CHI HẢI QUAN</asp:Label>
            </td>
        </tr>
        <tr>
            <td style="height: 40px;" align="center">
                <asp:Button ID="btnKetXuat" runat="server" Text="Kết xuất" UseSubmitBehavior="false">
                </asp:Button>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:GridView ID="grdChungTu" runat="server" Width="100%"  BorderColor="#989898" CssClass="grid_data"  AutoGenerateColumns="false"  EmptyDataText="Không có dữ liệu">
                    <AlternatingRowStyle CssClass="grid_item_alter" />
                    <HeaderStyle CssClass="grid_header" />
                    <RowStyle CssClass="grid_item"/>
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>STT</HeaderTemplate>
                            <ItemTemplate>
                                <%#Container.DataItemIndex + 1%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>Mã hải quan</HeaderTemplate>
                            <ItemTemplate>
                                &nbsp;<%#Eval("MA_HQ")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="TEN" HeaderText="Tên hải quan"/>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>

