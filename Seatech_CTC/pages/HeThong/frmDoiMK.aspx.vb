﻿Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports Business.HeThong

Partial Class pages_HeThong_frmDoiMK
    Inherits System.Web.UI.Page

    Private Sub frmDoiMK_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdDoiMK.Click, AddressOf cmdDoiMK_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            If Not Session.Item("User") Is Nothing Then ' Hiển thị tên NSD hiện thời
                lblUserName.Text = Session.Item("User").ToString()
            End If
        End If
    End Sub

    Private Sub cmdDoiMK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDoiMK.Click      
        Try
            Dim objUser As infUser

            ' Kiểm tra không cho phép mật khẩu trống
            If "".Equals(txtMKMoi.Text.Trim) Then
                clsCommon.ShowMessageBox(Me, "Bạn phải nhập mật khẩu tên truy cập !")
                txtMKMoi.Focus()
                Exit Sub
            End If

            ' Kiểm tra người đổi mật khẩu phải có mật khẩu của người dùng hiện thời
            If Trim(Me.txtMKCu.Text) <> Trim(VBOracleLib.Security.DescryptStr(clsCTU.TCS_GetMatKhau(Session.Item("User").ToString()))) Then
                clsCommon.ShowMessageBox(Me, "Mật khẩu cũ không hợp lệ.")
                txtMKCu.Text = ""
                txtMKCu.Focus()
                Return
            End If

            ' Kiểm tra mật khẩu mới và mật khẩu xác nhận phải giống nhau
            If Trim(txtMKMoi.Text) <> Trim(txtXacNhanMKM.Text) Then
                clsCommon.ShowMessageBox(Me, "Bạn chưa nhập đúng đúng mật khẩu xác nhận.")
                txtMKMoi.Text = ""
                txtXacNhanMKM.Text = ""
                txtXacNhanMKM.Focus()
                Return
            End If

            'Thực hiện đổi mật khẩu
            objUser = New infUser(Session.Item("user").ToString())
            objUser.Mat_Khau = VBOracleLib.Security.EncryptStr(Trim(txtMKMoi.Text))
            objUser.Ma_NV = Session.Item("User").ToString()
            If buAddUser.DoiMK(objUser) Then
                clsCommon.ShowMessageBox(Me, "Đổi mật khẩu thành công !")
            Else
                clsCommon.ShowMessageBox(Me, "Lỗi khi đổi mật khẩu của NSD !")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    

    Protected Sub cmdThoat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdThoat.Click
        Me.txtXacNhanMKM.Text = ""
        Me.txtMKMoi.Text = ""
        Me.txtMKCu.Text = ""
        Response.Redirect("../frmHomeNV.aspx", False)
    End Sub

End Class


