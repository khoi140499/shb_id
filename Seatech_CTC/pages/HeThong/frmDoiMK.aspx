﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false"
    CodeFile="frmDoiMK.aspx.vb" Inherits="pages_HeThong_frmDoiMK" Title="Đổi mật khẩu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" runat="Server">
    <table id="tblTitle" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">ĐỔI MẬT KHẨU</asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left" class="form_label">
                <asp:Label ID="lblError" Font-Bold="True" Font-Size="8pt" ForeColor="red" Font-Names="Arial"
                    runat="server" CssClass="label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table width="100%" class="form_input">
                    <tr align="left">
                        <td width="25%" class="form_label">
                            <asp:Label ID="lblCurUserCaption" runat="server" CssClass="label">Mã nhân viên</asp:Label>
                        </td>
                        <td class="form_label">
                            <asp:Label ID="lblUserName" runat="server" Width="250px" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lb1" runat="server" CssClass="label">Mật khẩu cũ</asp:Label>
                        </td>
                        <td class="form_label">
                            <asp:TextBox ID="txtMKCu" runat="server" TextMode="Password" MaxLength="50" Width="250px"
                                CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblNewPassCaption" CssClass="label" runat="server">Mật khẩu mới</asp:Label>
                        </td>
                        <td class="form_label">
                            <asp:TextBox ID="txtMKMoi" TextMode="Password" MaxLength="50" runat="server" Width="250px"
                                CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblConfirmPassCaption" CssClass="label" runat="server">Xác nhận mật khẩu</asp:Label>
                        </td>
                        <td class="form_label">
                            <asp:TextBox ID="txtXacNhanMKM" TextMode="Password" MaxLength="50" runat="server"
                                Width="250px" CssClass="inputflat"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
        <tr align="center">
            <td align="center">
                <asp:Button ID="cmdDoiMK" runat="server" Text="Đổi MK" CssClass="ButtonCommand" AccessKey="M">
                </asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdThoat" runat="server" Text="Hủy" CssClass="ButtonCommand" AccessKey="H" ></asp:Button>
            </td>
        </tr>
    </table>
</asp:Content>
