﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage02.master" AutoEventWireup="false"
    CodeFile="frmSetAutoDC.aspx.vb" Inherits="pages_HeThong_frmSetAutoDC" Title="Cấu hình tự động đối chiếu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" runat="Server">
    <table id="tblTitle" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">CẤU HÌNH TỰ ĐỘNG ĐỐI CHIẾU</asp:Label>&nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" class="form_label">
                <asp:Label ID="lblError" Font-Bold="True" Font-Size="8pt" ForeColor="red" Font-Names="Arial"
                    runat="server" CssClass="label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table width="100%" class="form_input">
                    <col width='30%' />
                    <col width='70%' />
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lb1" runat="server" CssClass="label">Trạng thái hiện tại</asp:Label>
                        </td>
                        <td class="form_label">
                            <asp:Label ID="lbHienTai" runat="server" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left">
                        <td class="form_label">
                            <asp:Label ID="lblNewPassCaption" CssClass="label" runat="server">Trạng thái mới</asp:Label>
                        </td>
                        <td class="form_label">
                            <asp:DropDownList ID="cboTrangThai" runat="server" Width="60%" CssClass="inputflat">
                                <asp:ListItem Text="Chạy tự động" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Dừng chạy tự động" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr align="center">
            <td align="center" colspan="2">
                <asp:Button ID="cmdChuyen" runat="server" Text="Chuyển" CssClass="ButtonCommand"
                    AccessKey="H"></asp:Button>
            </td>
        </tr>
    </table>
</asp:Content>
