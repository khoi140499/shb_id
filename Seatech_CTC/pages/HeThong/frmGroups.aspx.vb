﻿Imports System
Imports System.Drawing
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports Business.HeThong
Imports System.Data
Imports System.IO

Partial Class pages_HeThong_frmGroups
    Inherits System.Web.UI.Page
    Private mstroldMaNhom As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdSave.Click, AddressOf cmdSave_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            'Load nhom nguoi dung
            load_Nhom()
            'Load chuc nang nguoi dung
            'load_ChucNang()
            LoadParentNode()
            LoadCheckBox()
            'Load nhom mac dinh
            TCS_DS_NHOM_CN(cboNhom.SelectedValue.ToString)
            'mstroldMaNhom = IIf(Not Request.Params("id") Is Nothing, Request.Params("id").Trim, "")

            'If Not Request.Params("id") Is Nothing Then
            '    mstroldMaNhom = Request.Params("id").Trim
            'Else
            '    mstroldMaNhom = ""
            'End If
            'If mstroldMaNhom <> "" Then
            '    TCS_DS_NHOM_CN(mstroldMaNhom)
            'End If
        End If
    End Sub

    Protected Function LoadChildNode(ByVal strParentNode As String) As DataTable
        Dim strSqlQuery As String = "select a.nodeid,b.ma_cn,a.parentnodeid,a.text,b.ten_cn_con from tcs_site_map a inner join tcs_dm_chucnang b " & _
        " on a.nodeid=b.ma_cn and (a.parentnodeid='" & strParentNode & "' or a.rootnodeid='" & strParentNode & "') and b.ghi_chu='1' and a.parentnodeid is not null order by a.menu_order,a.nodeid "
        'Dim dtChildMenu As DataTable = DataAccess.ExecuteToTable("select nodeid,text from tcs_site_map where parentnodeid ='" & strParentNode & "' and status='1' and nodeid in(select ma_cn from tcs_dm_chucnang where ghi_chu='1') order by menu_order")
        Dim dtChildMenu As DataTable = DataAccess.ExecuteToTable(strSqlQuery)
        Return dtChildMenu
    End Function

    Sub LoadParentNode()
        Dim dtParentMenu As DataTable = DataAccess.ExecuteToTable("select nodeid,text from tcs_site_map where parentnodeid is null order by parent_order")
        rptParentNode.DataSource = dtParentMenu
        rptParentNode.DataBind()
    End Sub

    Private Sub load_Nhom()
        Dim cnQnhom As DataAccess
        Dim ds As New DataSet
        Dim strSQL As String
        ' Trong trường hợp đang load dữ liệu thì không load quyền
        Try
            cnQnhom = New DataAccess
            strSQL = "Select Ma_Nhom,Ten_Nhom " & "From TCS_DM_NHOM ORDER BY Ma_Nhom "
            ds = cnQnhom.ExecuteReturnDataSet(strSQL, CommandType.Text)
            'dtgNhom.DataSource = ds
            'dtgNhom.DataBind()

            cboNhom.Items.Clear()

            cboNhom.DataSource = ds.Tables(0).DefaultView
            cboNhom.DataTextField = "TEN_NHOM"
            cboNhom.DataValueField = "MA_NHOM"
            cboNhom.DataBind()
            cboNhom.SelectedIndex = 0
        Catch ex As Exception
            'Lỗi kết nối cơ sở dữ liệu
            clsCommon.ShowMessageBox(Me, "Lỗi kết nối CSDL")
        Finally
            If Not ds Is Nothing Then
                ds.Dispose()
            End If
            If Not cnQnhom Is Nothing Then
                cnQnhom.Dispose()
            End If
        End Try
    End Sub

    Private Sub load_ChucNang()
        Dim cnQnhom As DataAccess
        Dim ds As New DataSet
        Dim strSQL As String
        Try
            cnQnhom = New DataAccess
            strSQL = "SELECT Ma_CN,replace(Ten_CN_Con,'&','')  Ten_CN_Con FROM TCS_DM_CHUCNANG WHERE GHI_CHU='1' ORDER BY Ma_CN"
            ds = cnQnhom.ExecuteReturnDataSet(strSQL, CommandType.Text)
            dtgChucNang.DataSource = ds.Tables(0)
            dtgChucNang.DataBind()
        Catch ex As Exception
            'Lỗi kết nối cơ sở dữ liệu
            clsCommon.ShowMessageBox(Me, gstrLoiKetNoiCSDL)
            Return
        Finally
            If Not ds Is Nothing Then
                ds.Dispose()
            End If
            If Not cnQnhom Is Nothing Then
                cnQnhom.Dispose()
            End If
        End Try
    End Sub

    Private Sub LoadCheckBox()
        For Each rptItem As RepeaterItem In rptParentNode.Items
            Dim dtgChildNode As DataGrid = rptItem.FindControl("childNode")
            Dim hdNodeId As HiddenField = rptItem.FindControl("hdNodeId")
            Dim strMa_Nhom As String = cboNhom.SelectedValue
            For Each dtgItem As DataGridItem In dtgChildNode.Items

                Dim hdChildNodeId As HiddenField = dtgItem.FindControl("hdChildNodeId")
                Dim dtSelectQuyen As DataTable = DataAccess.ExecuteToTable("select * from tcs_phanquyen where ma_nhom='" & strMa_Nhom & "' and ma_cn='" & hdChildNodeId.Value & "'")
                Dim chkSelect As CheckBox = dtgItem.FindControl("chkSelectItem")

                If dtSelectQuyen.Rows.Count > 0 Then
                    chkSelect.Checked = True
                Else
                    chkSelect.Checked = False
                End If

            Next
        Next
    End Sub

    Private Sub ExportData()
        Dim dtCurrentPermission As New DataTable
        dtCurrentPermission.Columns.Add("Ma_CN")
        dtCurrentPermission.Columns.Add("Ten_Cn")
        For Each rptItem As RepeaterItem In rptParentNode.Items
            Dim dtr As DataRow = dtCurrentPermission.NewRow

            Dim dtgChildNode As DataGrid = rptItem.FindControl("childNode")
            Dim hdTenCN As HiddenField = rptItem.FindControl("hdTenCN")
            Dim hdNodeId As HiddenField = rptItem.FindControl("hdParentNodeId")
            Dim strMa_Nhom As String = cboNhom.SelectedValue

            dtr("Ma_CN") = hdNodeId.Value
            dtr("Ten_Cn") = hdTenCN.Value
            dtCurrentPermission.Rows.Add(dtr)
            For Each dtgItem As DataGridItem In dtgChildNode.Items

                Dim chkSelectItem As CheckBox = dtgItem.FindControl("chkSelectItem")

                If chkSelectItem.Checked Then
                    Dim dtrChild As DataRow = dtCurrentPermission.NewRow
                    Dim hdChildNodeId As HiddenField = dtgItem.FindControl("hdChildNodeId")
                    Dim strTenCN As String = dtgItem.Cells(1).Text
                    dtrChild("Ma_CN") = hdChildNodeId.Value
                    dtrChild("Ten_Cn") = strTenCN
                    dtCurrentPermission.Rows.Add(dtrChild)
                End If
            Next
        Next

        Dim dtgPermission As New DataGrid
        dtgPermission.DataSource = dtCurrentPermission
        dtgPermission.DataBind()

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=Permission.xls")
        Response.Charset = "utf-8"
        Response.ContentEncoding = System.Text.Encoding.Unicode
        Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble())
        Response.ContentType = "application/vnd.ms-excel"
        Using sw1 As New StringWriter()
            Dim hw1 As New HtmlTextWriter(sw1)
            dtgPermission.RenderControl(hw1)
            Response.Output.Write(sw1.ToString)
            Response.Flush()
            Response.End()
        End Using
    End Sub

    Private Sub SavePermission()
        For Each rptItem As RepeaterItem In rptParentNode.Items
            Dim dtgChildNode As DataGrid = rptItem.FindControl("childNode")
            Dim hdNodeId As HiddenField = rptItem.FindControl("hdNodeId")
            Dim strMa_Nhom As String = cboNhom.SelectedValue

            Dim strSQL As String = ""

            For Each dtgItem As DataGridItem In dtgChildNode.Items
                Dim hdChildNodeId As HiddenField = dtgItem.FindControl("hdChildNodeId")
                Dim chkSelect As CheckBox = dtgItem.FindControl("chkSelectItem")

                If chkSelect.Checked Then
                    strSQL = "Insert Into TCS_PHANQUYEN (MA_NHOM,MA_CN) " & _
                            "Values ('" & strMa_Nhom & "','" & Trim(hdChildNodeId.Value) & "')"
                    DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                Else
                    strSQL = "Delete From TCS_PHANQUYEN Where MA_NHOM = '" & cboNhom.SelectedValue.ToString & "' and ma_cn='" & hdChildNodeId.Value & "'"
                    DataAccess.ExecuteNonQuery(strSQL, CommandType.Text)
                End If
            Next
        Next
    End Sub

    Private Sub TCS_DS_NHOM_CN(ByVal mstrMa_Nhom As String)
        '-----------------------------------------------------
        ' Mục đích: Thực hiện đánh đấu checkBox, tương ứng với
        '           những quyền của người dùng không được phép
        '           sử dụng
        ' Tham số: mstrMa_Nhom
        ' Giá trị trả về: 
        ' Ngày viết: 16/10/2007
        ' Người viết: Nguyễn Bá Cường
        ' ----------------------------------------------------
        Dim cnQNhom As DataAccess
        Dim drNhom As IDataReader
        Dim strItem As String
        Dim strSQL As String
        Dim i As Integer

        Try
            cnQNhom = New DataAccess
            strSQL = "Select MA_CN " & _
                "From TCS_PHANQUYEN Where MA_NHOM = '" & UCase(Trim(mstrMa_Nhom)) & "'"

            drNhom = cnQNhom.ExecuteDataReader(strSQL, CommandType.Text)
            Dim obj As CheckBox
            ' Khởi tạo lai cac check box
            For i = 0 To dtgChucNang.Items.Count - 1
                obj = dtgChucNang.Items(i).FindControl("chkSelect")
                obj.Checked = False
            Next
            Do While drNhom.Read
                For i = 0 To dtgChucNang.Items.Count - 1
                    strItem = Trim(dtgChucNang.Items(i).Cells(1).Text)
                    If Trim(strItem) = Trim(drNhom.GetValue(0)) Then
                        obj = dtgChucNang.Items(i).FindControl("chkSelect")
                        obj.Checked = True
                    End If
                Next
            Loop
        Catch ex As Exception
            'Lỗi kết nối cơ sở dữ liệu
            clsCommon.ShowMessageBox(Me, gstrLoiKetNoiCSDL)
        Finally
            If Not drNhom Is Nothing Then
                drNhom.Close()
            End If
            If Not cnQNhom Is Nothing Then
                cnQNhom.Dispose()
            End If
        End Try
    End Sub

    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        SavePermission()
        'LogApp.AddDebug("cmdSave_Click", "Luu thong tin phan quyen cua nhom nguoi su dung")
        ''-----------------------------------------------------
        '' Mục đích: Cập nhật những thay đổi quyền cho người
        ''           cho những dùng được lựa chọn ở listBox
        '' Tham số: 
        '' Giá trị trả về: 
        '' Ngày viết: 18/10/2007
        '' Người viết: Nguyễn Bá Cường
        '' ----------------------------------------------------
        'Dim cnUNhom As DataAccess
        'Dim strItem As String
        'Dim strSQL As String
        'Dim i As Integer
        'Dim obj As CheckBox
        ''If Not Request.Params("id") Is Nothing Then
        'Try
        '    'mstroldMaNhom = Request.Params("id").Trim
        '    '  End If
        '    cnUNhom = New DataAccess
        '    strSQL = "Delete From TCS_PHANQUYEN Where MA_NHOM = '" & cboNhom.SelectedValue.ToString & "'"
        '    cnUNhom.ExecuteNonQuery(strSQL, CommandType.Text)
        '    'For i = 0 To lstChucNang.Items.Count - 1
        '    '    If Not (lstChucNang.GetItemCheckState(i) = CheckState.Checked) Then
        '    '        strItem = Mid(lstChucNang.Items(i), 101)
        '    '        strSQL = "Insert Into TCS_PHANQUYEN (MA_NHOM,MA_CN) " & _
        '    '                "Values ('" & mstroldMaNhom & "','" & Trim(strItem) & "')"
        '    '        cnUNhom.ExecuteNonQuery(strSQL, CommandType.Text)
        '    '    End If
        '    'Next
        '    For i = 0 To dtgChucNang.Items.Count - 1
        '        obj = dtgChucNang.Items(i).FindControl("chkSelect")
        '        If obj.Checked Then
        '            strItem = Trim(dtgChucNang.Items(i).Cells(1).Text)
        '            strSQL = "Insert Into TCS_PHANQUYEN (MA_NHOM,MA_CN) " & _
        '                    "Values ('" & cboNhom.SelectedValue.ToString & "','" & Trim(strItem) & "')"
        '            cnUNhom.ExecuteNonQuery(strSQL, CommandType.Text)
        '        End If
        '    Next

        '    clsCommon.ShowMessageBox(Me, "Gán quyền chức năng cho nhóm thành công !")
        'Catch ex As Exception
        '    'Lỗi xoá dữ liệu không thành công
        '    clsCommon.ShowMessageBox(Me, gstrLoiXoaDuLieu)
        '    Return
        'Finally
        '    If Not cnUNhom Is Nothing Then
        '        cnUNhom.Dispose()
        '    End If
        'End Try
        ''End If
    End Sub

    Protected Sub cboNhom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNhom.SelectedIndexChanged
        'TCS_DS_NHOM_CN(cboNhom.SelectedValue.ToString)
        LoadCheckBox()
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        ExportData()
    End Sub
End Class
