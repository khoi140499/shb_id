﻿Imports System
Imports System.Drawing
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports Business.HeThong
Imports System.Data
Imports Business

Partial Class pages_HeThong_frmUsers
    Inherits System.Web.UI.Page
    Private mstrMa_NV As String = ""
    Private mstrHoTen As String = ""
    Private mstrChuc_Danh As String = ""
    Private mstrMatKhau As String = ""
    Private strMa_CN As String
    Private strTrang_thai As String
    Private maxWrongLogin As Integer = 3

    Public ReadOnly Property TCS_Ma_NV() As String
        Get
            Return mstrMa_NV
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim tmpstrCol_1 As String
        'Dim tmpstrCol_2 As String
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            RemoveHandler cmdTaoMoi.Click, AddressOf cmdSave_Click
            RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If

        If Not IsPostBack Then
            Dim ds As New DataSet
            Try
                ds = buAddUser.SelectNhom()
                'cboNhomND.Items.Clear()

                'cboNhomND.DataSource = ds.Tables(0).DefaultView
                'cboNhomND.DataTextField = "TEN_NHOM"
                'cboNhomND.DataValueField = "MA_NHOM"
                'cboNhomND.DataBind()
                'cboNhomND.SelectedIndex = 0
                'ds.Dispose()
                clsCommon.ComboBoxWithValue(cboNhomND, ds, 1, 0, "<< Tất cả >>")
                PassWord.Visible = False
                RePassWord.Visible = False
                trHMDen.Visible = False
                trHMTu.Visible = False

                ' Duc Anh : Hiện nút theo quyền maker/checker đã gán : Start
                Dim strChecker As String = HeThong.buThamso.ThamSoHT_GetValue("NHOM_CHECKER")
                Dim strMaker As String = HeThong.buThamso.ThamSoHT_GetValue("NHOM_MAKER")
                Dim strViewer As String = HeThong.buThamso.ThamSoHT_GetValue("NHOM_VIEWER")

                'If strMaker.Contains(Session.Item("MA_NHOM").ToString()) Then
                '    cmdTaoMoi.Visible = True
                '    cmdDeleteUser.Visible = True
                'End If

                'If strChecker.Contains(Session.Item("MA_NHOM").ToString()) Then
                '    cmdDuyet.Visible = True
                '    cmdDeleteUser.Visible = False
                'End If

                'If (strChecker.Contains(Session.Item("MA_NHOM").ToString()) = False And strMaker.Contains(Session.Item("MA_NHOM").ToString()) = False) Or (strViewer.Contains(Session.Item("MA_NHOM").ToString())) Then
                '    cmdDeleteUser.Visible = False
                'End If
                ' Duc Anh : Hiện nút theo quyền maker/checker đã gán : End
            Catch ex As Exception
                'Lỗi kết nối cơ sở dữ liệu
                clsCommon.ShowMessageBox(Me, gstrLoiKetNoiCSDL)
            End Try
            load_DiemThu()
            load_Datagrid()
           
            If Request.Params("MANV") <> "" Then
                load_Detail()
            End If
            ViewState("Pwd") = txtMatKhau.Text
            txtMatKhau.Attributes.Add("value", ViewState("Pwd").ToString())
            ViewState("Pwd") = txtXacNhanMK.Text
            txtXacNhanMK.Attributes.Add("value", ViewState("Pwd").ToString())
        End If
        'cmdDeleteUser.Attributes.Add("onclick", "return confirm('Bạn có thực sự muốn xóa không?')")
        ' txtMaBDS.Attributes.Add("onkeyup", "return valInteger(this);")
        ' txtMaCN.Attributes.Add("onkeyup", "return valInteger(this);")
    End Sub

    Protected Function GetActonString(ByVal strAction As String) As String
        Dim strResult As String = ""
        Select Case strAction
            Case "INSERT"
                strResult = "Thêm mới"
            Case "UPDATE"
                strResult = "Cập nhật"
            Case "DELETE"
                strResult = "Xóa"
        End Select
        Return strResult
    End Function

    <System.Web.Services.WebMethod()> _
   Public Shared Function GetURLSignature(ByVal pv_strTK As String) As String
        Dim ten_cn As String = ""
        Dim dt_NAME_CN As New DataTable
        dt_NAME_CN = DataAccess.ExecuteToTable("Select NAME from tcs_dm_chinhanh where ID=" & pv_strTK)
        If Not dt_NAME_CN Is Nothing And dt_NAME_CN.Rows.Count > 0 Then
            ten_cn = dt_NAME_CN.Rows(0)("NAME").ToString()
        Else
            ten_cn = ""
        End If
        Return ten_cn
    End Function
    Protected Sub load_DiemThu()

        Dim ds As New DataSet
        Dim strSQL As String
        Try

            strSQL = "SELECT  a.MA_DThu,a.TEN FROM TCS_DM_DIEMTHU a " & _
                        " ORDER BY MA_DThu"
            ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            cboDiemThu.DataSource = ds.Tables(0)
            cboDiemThu.DataTextField = ds.Tables(0).Columns("Ten").ToString
            cboDiemThu.DataValueField = ds.Tables(0).Columns("MA_DThu").ToString
            cboDiemThu.DataBind()
            'clsCommon.ComboBoxWithValue(cboDiemThu, ds, 1, 0, "<< Tất cả >>")
        Catch ex As Exception
            'Lỗi kết nối cơ sở dữ liệu
            clsCommon.ShowMessageBox(Me, "Lỗi kết nối CSDL")
        Finally
            If Not ds Is Nothing Then
                ds.Dispose()
            End If

        End Try
    End Sub
    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTaoMoi.Click
        If Request.Params("MANV") = "" Then
            'If "".Equals(txtMaND.Text.Trim) Or Len(txtMaND.Text) < 3 Then
            '    clsCommon.ShowMessageBox(Me, "Bạn phải nhập tên truy cập đủ 3 ký tự !")
            '    txtMaND.Focus()
            '    Return
            'End If
            If "".Equals(txtTenND.Text.Trim) Then
                clsCommon.ShowMessageBox(Me, "Tên đăng nhập không được để trống!")
                txtTenND.Focus()
                Return
            End If
            If CheckExits(txtTenND.Text.Trim) = True Then
                clsCommon.ShowMessageBox(Me, "Tên đăng nhập đã tồn tại, vui lòng kiểm tra lại.!")
                txtTenND.Focus()
                Return
            End If

            Dim dtCheckTemAvalable As DataTable = DataAccess.ExecuteToTable("select * from tcs_dm_nhanvien_temp where ten_dn='" & txtTenND.Text.Trim & "' and action='INSERT' and APPROVE_TIME is null")

            'If "".Equals(txtMaBDS.Text.Trim) Then
            '    clsCommon.ShowMessageBox(Me, "Mã Teller không được để trống!")
            '    txtTenND.Focus()
            '    Return
            'End If
            If cboNhomND.SelectedValue = "" Then
                clsCommon.ShowMessageBox(Me, "Bạn phải chọn nhóm sử dụng để ghi.!")
                cboNhomND.Focus()
                Return
            End If
            If "".Equals(txtMaCN.Text.Trim) Then
                clsCommon.ShowMessageBox(Me, "Mã chi nhánh không được để trống!")
                txtTenND.Focus()
                Return
            End If
            'Kiểm tra điều kiện khác rỗng của mã người dùng
            If "".Equals(txtHoTen.Text.Trim) Then
                clsCommon.ShowMessageBox(Me, "Bạn phải nhập họ tên của tên truy cập !")
                txtHoTen.Focus()
                Return
            End If
            If buUsers.check_MaCN(txtMaCN.Text.Trim) = False Then
                clsCommon.ShowMessageBox(Me, "Mã chi nhánh không tồn tại hoặc sai định dạng (000000), vui lòng nhập lại:")
                txtMaCN.Focus()
                Return
            End If

            If ddlKieuDN.SelectedValue.ToString = "" Then
                clsCommon.ShowMessageBox(Me, "Bạn phải chọn Kiểu đăng nhập !")
                Return
            End If
            ' Kiểm tra điều kiện khác rỗng của pass
            If ddlKieuDN.SelectedValue.ToString = "1" Then
                If "".Equals(txtMatKhau.Text.Trim) Then
                    clsCommon.ShowMessageBox(Me, "Bạn phải nhập mật khẩu của tên truy cập !")
                    txtMatKhau.Focus()
                    Return
                End If
                ' Kiểm tra điều kiện khác rỗng của xác nhận pass
                If "".Equals(txtXacNhanMK.Text.Trim) Then
                    clsCommon.ShowMessageBox(Me, "Bạn phải nhập xác nhận mật khẩu của tên truy cập !")
                    txtXacNhanMK.Focus()
                    Return
                End If
                ' Kiểm tra điều kiện hai mật khẩu phải giống nhau 
                If Trim(txtMatKhau.Text) <> Trim(txtXacNhanMK.Text) Then
                    clsCommon.ShowMessageBox(Me, "Mật khẩu và mật khẩu xác nhận không giống nhau.")
                    txtXacNhanMK.Focus()
                    Return
                End If
            ElseIf ddlKieuDN.SelectedValue.ToString = "0" Then
                If "".Equals(txtUserDomain.Text.Trim) Then
                    clsCommon.ShowMessageBox(Me, "Bạn phải nhập User Domain !")
                    txtUserDomain.Focus()
                    Return
                End If
            Else
                txtUserDomain.Text = ""
                txtMatKhau.Text = ""
                txtXacNhanMK.Text = ""
            End If

            If cboNhomND.Text = "" Then
                clsCommon.ShowMessageBox(Me, "Nhóm người dùng phải khác trống.")
                Return
            End If

            'Hạn mức
            If cboNhomND.SelectedValue.ToString() = "04" Or cboNhomND.SelectedValue.ToString() = "15" Then
                If "".Equals(txtTuHanMuc.Text) Then
                    txtTuHanMuc.Text = "0"
                End If
                If "".Equals(txtDenHanMuc.Text) Then
                    txtDenHanMuc.Text = "0"
                End If
                If Convert.ToDecimal(txtDenHanMuc.Text.Replace(".", "")) < Convert.ToDecimal(txtTuHanMuc.Text.Replace(".", "")) Then
                    clsCommon.ShowMessageBox(Me, "Hạn mức đến phải lớn hơn hạn mức từ.")
                    txtDenHanMuc.Focus()
                    Return
                End If
            Else
                txtTuHanMuc.Text = "0"
                txtDenHanMuc.Text = "0"
            End If

            Dim objUser As New infUser
            ' Đặt trạng thái bận

            Try
                'objUser.Ma_NV = txtMaND.Text
                objUser.TEN_ND = txtTenND.Text.Trim.ToLower()
                objUser.Ten = Trim(txtHoTen.Text)
                objUser.Mat_Khau = VBOracleLib.Security.EncryptStr(Trim(txtMatKhau.Text))
                objUser.Chuc_Danh = Trim(txtChucdanh.Text)
                objUser.Ma_Nhom = cboNhomND.SelectedValue.ToString
                objUser.MaCN = txtMaCN.Text.Trim
                ' chua co ban lam viec
                objUser.LDAP = ddlKieuDN.SelectedValue
                objUser.Ban_Lv = cboDiemThu.SelectedValue
                If cboTrangThai.SelectedValue <> "" Then
                    objUser.Trang_thai = cboTrangThai.SelectedValue
                Else
                    clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Trạng thái hoạt động !")
                    Exit Sub
                End If

                objUser.Teller_Id = Trim(txtMaBDS.Text)
                objUser.HanMuc_Tu = txtTuHanMuc.Text.Trim.Replace(".", "")
                objUser.HanMuc_Den = txtDenHanMuc.Text.Trim.Replace(".", "")
                objUser.User_Domain = txtUserDomain.Text.Trim

                'sonmt
                objUser.MA_NV_TAO = Session.Item("User").ToString()

                ' ktra tên đang nhập còn đang hoạt động ko
                If CheckExits(objUser.TEN_ND, "0") Then
                    clsCommon.ShowMessageBox(Me, "User đăng nhập này đã có và đang hoạt động !")
                    txtMaND.Focus()
                    Exit Sub
                End If
                ' ktra tên đang nhập ở chi nhánh này đã tồn tai chưa
                If CheckExits(objUser.TEN_ND, "1", objUser.MaCN) Then
                    clsCommon.ShowMessageBox(Me, "User đăng nhập đã có ở chi nhánh và đang ở trạng thái ngừng hoạt động.")
                    txtMaND.Focus()
                    Exit Sub
                End If

                If dtCheckTemAvalable.Rows.Count > 0 Then
                    clsCommon.ShowMessageBox(Me, "Tên đăng nhập này đang chờ duyệt để được hoạt động")
                    Exit Sub
                End If
                If Not CheckExits_UserTemp(objUser.TEN_ND, "0") Then
                    buAddUser.InsertUserTemp(objUser, "INSERT")
                    clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt !")
                Else
                    clsCommon.ShowMessageBox(Me, "Dữ liệu tương tự đã tồn tại trong hệ thống nhưng chưa được duyệt. Vui lòng thao tác với dữ liệu đã tồn tại !")
                    Exit Sub
                End If

                'clsCommon.ShowMessageBox(Me, "Tạo mới thành công mã nhân viên !")

                ''mstrMa_NV = txtMaND.Text
                '' Tạo mới NSD thành công
                'Else

                'End If

            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình tạo mới NSD ! " + ex.Message)
                Return
            End Try
        Else
            ' txtMaND.ReadOnly = True
            ' txtTenND.ReadOnly = True
            ' Đổi tên
            Dim objUser As New infUser()
            If cboLoaiSua.SelectedValue = "2" Or cboLoaiSua.SelectedValue = "0" Then
                If "".Equals(txtHoTen.Text.Trim) Then
                    clsCommon.ShowMessageBox(Me, "Bạn phải nhập họ tên của tên truy cập !")
                    txtHoTen.Focus()
                    Return
                End If
                Try
                    objUser.Ma_NV = Trim(txtMaND.Text)
                    objUser.Ten = Trim(txtHoTen.Text)
                    objUser.Chuc_Danh = Trim(txtChucdanh.Text)
                    objUser.Ma_Nhom = cboNhomND.SelectedValue
                    objUser.Ban_Lv = cboDiemThu.SelectedValue
                    objUser.Teller_Id = Trim(txtMaBDS.Text)
                    objUser.MaCN = Trim(txtMaCN.Text)
                    objUser.Ma_Nhom = cboNhomND.SelectedValue

                    objUser.Ma_NV = Trim(txtMaND.Text)
                    objUser.TEN_ND = txtTenND.Text.Trim.ToLower()
                    objUser.LDAP = ddlKieuDN.SelectedValue

                    'sonmt
                    objUser.MA_NV_SUA = Session.Item("User").ToString()

                    'Kiểm tra lại mật khẩu
                    If ddlKieuDN.SelectedValue.ToString = "1" Then
                        If "".Equals(txtMatKhau.Text.Trim) Then
                            clsCommon.ShowMessageBox(Me, "Bạn phải nhập mật khẩu của tên truy cập !")
                            txtMatKhau.Focus()
                            Return
                        End If
                        ' Kiểm tra điều kiện khác rỗng của xác nhận pass
                        If "".Equals(txtXacNhanMK.Text.Trim) Then
                            clsCommon.ShowMessageBox(Me, "Bạn phải nhập xác nhận mật khẩu của tên truy cập !")
                            txtXacNhanMK.Focus()
                            Return
                        End If
                        ' Kiểm tra điều kiện hai mật khẩu phải giống nhau 
                        If Trim(txtMatKhau.Text) <> Trim(txtXacNhanMK.Text) Then
                            clsCommon.ShowMessageBox(Me, "Mật khẩu và mật khẩu xác nhận không giống nhau.")
                            txtXacNhanMK.Focus()
                            Return
                        End If
                    ElseIf ddlKieuDN.SelectedValue.ToString = "0" Then
                        If "".Equals(txtUserDomain.Text.Trim) Then
                            clsCommon.ShowMessageBox(Me, "Bạn phải nhập mật khẩu của tên truy cập !")
                            txtUserDomain.Focus()
                            Return
                        End If
                    End If
                    objUser.User_Domain = txtUserDomain.Text.Trim
                    objUser.Mat_Khau = VBOracleLib.Security.EncryptStr(Trim(txtMatKhau.Text))
                    'Check Han muc
                    If cboNhomND.SelectedValue.ToString() = "04" Or cboNhomND.SelectedValue.ToString() = "15" Then
                        If "".Equals(txtTuHanMuc.Text) Then
                            txtTuHanMuc.Text = "0"
                        End If
                        If "".Equals(txtDenHanMuc.Text) Then
                            txtDenHanMuc.Text = "0"
                        End If
                        If Convert.ToDecimal(txtDenHanMuc.Text.Replace(".", "")) < Convert.ToDecimal(txtTuHanMuc.Text.Replace(".", "")) Then
                            clsCommon.ShowMessageBox(Me, "Hạn mức đến phải lớn hơn hạn mức từ.")
                            txtDenHanMuc.Focus()
                            Return
                        End If
                    Else
                        txtTuHanMuc.Text = "0"
                        txtDenHanMuc.Text = "0"
                    End If

                    objUser.HanMuc_Tu = txtTuHanMuc.Text.Trim.Replace(".", "")
                    objUser.HanMuc_Den = txtDenHanMuc.Text.Trim.Replace(".", "")
                    If cboTrangThai.SelectedValue <> "" Then
                        objUser.Trang_thai = cboTrangThai.SelectedValue
                    Else
                        clsCommon.ShowMessageBox(Me, "Bạn phải nhập vào Trạng thái hoạt động !")
                        Exit Sub
                    End If

                    If Not CheckExits_UserTemp(objUser.TEN_ND, "0") Then
                        If Trim(txtMaCN.Text) <> strMa_CN Then
                            'If CheckExits(objUser.TEN_ND, "", objUser.MaCN) Then
                            '    buAddUser.DoiCN_EXITS(objUser)
                            'Else
                            '    buAddUser.DoiTen(objUser, True)
                            'End If
                            objUser.MA_NV_TAO = Session.Item("User")
                            If Not buAddUser.InsertUserTemp(objUser, "UPDATE") Then
                                clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình thay đổi thông tin User!")
                                Exit Sub
                            End If
                        Else
                            If objUser.Trang_thai <> strTrang_thai And objUser.Trang_thai = "0" And CheckExits(objUser.TEN_ND, "0") = True Then
                                clsCommon.ShowMessageBox(Me, "Không thể sửa trạng thái user đang hoạt động ở chi nhánh khác!")
                                Return
                            End If
                            'buAddUser.DoiTen(objUser)
                            objUser.MA_NV_TAO = Session.Item("User")
                            If Not buAddUser.InsertUserTemp(objUser, "UPDATE") Then
                                clsCommon.ShowMessageBox(Me, "Lỗi trong quá trình thay đổi thông tin User!")
                                Exit Sub
                            End If
                        End If
                    Else
                        clsCommon.ShowMessageBox(Me, "Dữ liệu tương tự đã tồn tại trong hệ thống nhưng chưa được duyệt. Vui lòng thao tác với dữ liệu đã tồn tại !")
                        Exit Sub
                    End If

                    'buAddUser.DoiTen(objUser)
                    mstrHoTen = txtHoTen.Text
                    'clsCommon.ShowMessageBox(Me, "Cập nhật thành công!")
                    clsCommon.ShowMessageBox(Me, "Ghi dữ liệu thành công và chờ KSV phê duyệt!")

                Catch ex As Exception
                    'Lỗi không thể cập nhật dữ liệu
                    clsCommon.ShowMessageBox(Me, gstrLoiCapNhatDL)
                End Try
            End If
        End If
        XoaText()
        load_Datagrid()
    End Sub

    Private Sub XoaText()
        txtMaND.Text = ""
        txtTenND.Text = ""
        txtHoTen.Text = ""
        txtMatKhau.Text = ""
        txtXacNhanMK.Text = ""
        txtChucdanh.Text = ""
        txtMaBDS.Text = ""
        txtMaCN.Text = ""
        txtTenCN.Text = ""
        txtMaND.ReadOnly = False
        txtTenND.ReadOnly = False
        txtTenND.Enabled = True
        txtTuHanMuc.Text = ""
        txtDenHanMuc.Text = ""
        ddlKieuDN.SelectedValue = ""
        cboTrangThai.SelectedValue = ""
        cboNhomND.SelectedValue = ""
        PassWord.Visible = False
        RePassWord.Visible = False
        trHMDen.Visible = False
        trHMTu.Visible = False
        UserDomain.Visible = False
        txtUserDomain.Text = ""
    End Sub

    Private Sub load_Datagrid(Optional ByVal objUser As infUser = Nothing)
        Dim ds As New DataSet
        Try
            If Not objUser Is Nothing Then
                ds = buUsers.TCS_LST_DM_NHANVIEN_DIEMTHU(objUser)
            Else
                ds = buUsers.TCS_LST_DM_NHANVIEN_DIEMTHU()
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                'dgUserAccount.CurrentPageIndex = 0
                dgUserAccount.DataSource = ds.Tables(0)
                dgUserAccount.DataBind()
            Else
                If Not Request.Params("MANV") <> "" Then
                    clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
                End If
                'dgUserAccount.DataSource = Nothing
                'dgUserAccount.DataBind()
                'Return
            End If

        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình lấy dữ liệu. Lỗi:" & ex.Message)
        End Try
    End Sub

    Private Sub load_Detail()
        Dim dtTableMaxLoginWrong As DataTable = DataAccess.ExecuteToTable("select giatri_ts from tcs_thamso_ht where ten_ts='MAX_LOGIN_WRONG'")

        If Not dtTableMaxLoginWrong Is Nothing And dtTableMaxLoginWrong.Rows.Count > 0 Then
            maxWrongLogin = Integer.Parse(dtTableMaxLoginWrong.Rows(0)("giatri_ts").ToString())
        End If

        Dim Ma_NV As String
        'Dim Ten_DN As String
        Dim objUser As New infUser
        If Not Request.Params("MANV") Is Nothing Then
            Ma_NV = Request.Params("MANV").ToString.Trim
            objUser = buUsers.NHANVIEN_DETAIL(Ma_NV)
            txtMaND.Text = objUser.Ma_NV
            txtHoTen.Text = objUser.Ten
            txtChucdanh.Text = objUser.Chuc_Danh
            cboNhomND.SelectedValue = objUser.Ma_Nhom
            cboDiemThu.SelectedValue = objUser.Ban_Lv
            txtTenND.Text = objUser.TEN_ND
            txtMaBDS.Text = objUser.Teller_Id
            txtMaCN.Text = objUser.MaCN
            strMa_CN = objUser.MaCN
            txtTenCN.Text = Get_TenCN(strMa_CN)
            'txtTenCN.Enabled = False
            cboTrangThai.SelectedValue = objUser.Trang_thai
            strTrang_thai = objUser.Trang_thai
            'txtTenND.ReadOnly = True
            txtTenND.Enabled = False
            ddlKieuDN.SelectedValue = objUser.LDAP
            txtUserDomain.Text = objUser.User_Domain

            If ddlKieuDN.SelectedValue = "" Then
                PassWord.Visible = False
                RePassWord.Visible = False
                UserDomain.Visible = False
            ElseIf ddlKieuDN.SelectedValue = "0" Then
                If Not objUser.User_Domain Is Nothing Then
                    UserDomain.Visible = True
                    txtUserDomain.Text = objUser.User_Domain
                End If
            Else
                If Not objUser.Mat_Khau Is Nothing Then
                    PassWord.Visible = True
                    RePassWord.Visible = True
                    txtMatKhau.Text = VBOracleLib.Security.DescryptStr(objUser.Mat_Khau)
                    txtXacNhanMK.Text = VBOracleLib.Security.DescryptStr(objUser.Mat_Khau)
                End If
            End If

            If objUser.Ma_Nhom.ToString = "04" Or objUser.Ma_Nhom.ToString = "15" Then
                trHMDen.Visible = True
                trHMTu.Visible = True
                txtTuHanMuc.Text = VBOracleLib.Globals.Format_Number(objUser.HanMuc_Tu, ".")
                txtDenHanMuc.Text = VBOracleLib.Globals.Format_Number(objUser.HanMuc_Den, ".")
            Else
                trHMDen.Visible = False
                trHMTu.Visible = False
            End If
            If objUser.LDAP = 0 Then
                'DIVMatkhau.Visible = False
            Else
                'DIVMatkhau.Visible = True
            End If
            'If Session.Item("User") <> objUser.MA_NV_SUA Then
            '    cmdDuyet.Visible = True
            '    cmdDeleteUser.Visible = True
            '    cmdTaoMoi.Visible = True
            'Else
            '    cmdDuyet.Visible = False
            '    cmdDeleteUser.Visible = False
            '    cmdTaoMoi.Visible = False
            'End If
            load_Datagrid(objUser)
        End If
    End Sub

    Protected Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        XoaText()
        lbSua.Visible = False
        cboLoaiSua.Visible = False
        If Not Request.Params("MANV") Is Nothing Then
            Response.Redirect("~/pages/HeThong/frmUsers.aspx", False)
        End If
        cmdTimKiem_Click(sender, e)
    End Sub

    Private Function CheckExits(ByVal strTen_DN As String, Optional ByVal strTrangThai As String = "", Optional ByVal strMaCN As String = "") As Boolean
        Dim blnResult As Boolean = True
        Dim conn As DataAccess
        Dim dr As IDataReader
        Dim strSql As String
        Try
            conn = New DataAccess
            strSql = "SELECT * FROM TCS_DM_NHANVIEN " & _
                    "WHERE ten_DN ='" & strTen_DN & "' "
            If strTrangThai <> "" Then
                strSql = strSql & "and TINH_TRANG = '" & strTrangThai & "'"
            End If
            If strMaCN <> "" Then
                strSql = strSql & " and ma_cn ='" & strMaCN & "'"
            End If
            dr = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
            If dr.Read Then
                blnResult = True
            Else
                blnResult = False
            End If
        Catch ex As Exception
            blnResult = False
            Throw ex
        Finally
            If Not dr Is Nothing Then
                dr.Dispose()
            End If
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
        End Try
        Return blnResult
    End Function
    Private Function CheckExits_UserTemp(ByVal strTen_DN As String, ByVal strDaDuyet As String) As Boolean
        Dim blnResult As Boolean = True
        Dim conn As DataAccess
        Dim dr As IDataReader
        Dim strSql As String
        Try
            conn = New DataAccess
            strSql = "SELECT * FROM TCS_DM_NHANVIEN_temp " & _
                    "WHERE ten_DN ='" & strTen_DN & "' "
            If strDaDuyet <> "" Then
                strSql = strSql & "and da_duyet = '" & strDaDuyet & "'"
            End If
            dr = DataAccess.ExecuteDataReader(strSql, CommandType.Text)
            If dr.Read Then
                blnResult = True
            Else
                blnResult = False
            End If
        Catch ex As Exception
            blnResult = False
            Throw ex
        Finally
            If Not dr Is Nothing Then
                dr.Dispose()
            End If
            If Not conn Is Nothing Then
                conn.Dispose()
            End If
        End Try
        Return blnResult
    End Function
    Protected Sub cmdDeleteUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDeleteUser.Click
        Dim strTenND As String
        Dim strMaND As String
        Dim objUser As New infUser
        Dim strUser_GD As String = ""
        Dim v_count As Integer = 0
        Dim succsess_Count As Integer = 0
        Dim v_flag As Boolean = True
        For i As Integer = 0 To dgUserAccount.Items.Count() - 1
            Dim chkSelect As CheckBox
            chkSelect = dgUserAccount.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strMaND = Trim(dgUserAccount.Items(i).Cells(0).Text)
                strTenND = Trim(dgUserAccount.Items(i).Cells(1).Text)
                If strTenND = "admin" Then
                    clsCommon.ShowMessageBox(Me, "Bạn không thể xoá NSD là QTHT.")
                    Return
                End If
                If Session.Item("User").ToString.PadLeft(3, "0") = strMaND Then
                    clsCommon.ShowMessageBox(Me, "Bạn không thể xoá NSD hiện thời được.")
                    Return
                End If
                Try
                    objUser.Ma_NV = strMaND
                    Dim conn As New DataAccess
                    Dim dtUserDetail As DataTable = DataAccess.ExecuteToTable("SELECT * FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMaND & "'")

                    If dtUserDetail.Rows.Count > 0 Then
                        objUser.Ma_NV = dtUserDetail.Rows(0)("ma_nv").ToString()
                        objUser.Ten = dtUserDetail.Rows(0)("TEN").ToString()
                        objUser.Mat_Khau = dtUserDetail.Rows(0)("MAT_KHAU").ToString()
                        objUser.Trang_thai = dtUserDetail.Rows(0)("TINH_TRANG").ToString()
                        objUser.Ban_Lv = dtUserDetail.Rows(0)("BAN_LV").ToString()
                        objUser.Chuc_Danh = dtUserDetail.Rows(0)("CHUC_DANH").ToString()
                        objUser.TEN_ND = dtUserDetail.Rows(0)("TEN_DN").ToString()
                        objUser.Teller_Id = dtUserDetail.Rows(0)("TELLER_ID").ToString()
                        objUser.MaCN = dtUserDetail.Rows(0)("MA_CN").ToString()
                        objUser.LDAP = dtUserDetail.Rows(0)("LDAP").ToString()
                        objUser.Ma_Nhom = dtUserDetail.Rows(0)("MA_NHOM").ToString()
                        objUser.HanMuc_Tu = dtUserDetail.Rows(0)("HANMUC_TU").ToString()
                        objUser.HanMuc_Den = dtUserDetail.Rows(0)("HANMUC_DEN").ToString()
                        objUser.User_Domain = dtUserDetail.Rows(0)("USERDOMAIN").ToString()
                        objUser.MA_NV_TAO = Session.Item("User").ToString()

                        If dtUserDetail.Rows(0)("LAN_DN_SAI").ToString() <> "" Then
                            objUser.Lan_DN_SAI = dtUserDetail.Rows(0)("LAN_DN_SAI").ToString()
                        Else
                            objUser.Lan_DN_SAI = 0
                        End If

                        objUser.Da_Duyet = dtUserDetail.Rows(0)("DA_DUYET").ToString()
                    End If

                    'buAddUser.Delete(objUser, "1")
                    Dim dtCheckUserTem As DataTable = DataAccess.ExecuteToTable("select 1 from TCS_DM_NHANVIEN_TEMP where MA_NV='" & objUser.Ma_NV & "' and APPROVE_TIME is not null and ACTION='DELETE'")

                    If dtCheckUserTem.Rows.Count < 1 Then
                        If buAddUser.InsertUserTemp(objUser, "DELETE") Then
                            succsess_Count += 1
                        End If
                    End If

                Catch ex As Exception
                    'Lỗi xoá dữ liệu không thành công
                    clsCommon.ShowMessageBox(Me, gstrLoiXoaDuLieu)
                    Return
                End Try
                v_count += 1
            End If
        Next
        XoaText()
        load_Datagrid()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Thao tác xóa thành công và đang chờ xử lý!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để xóa!")
        End If
    End Sub

    Protected Sub An_text()
        If cboLoaiSua.SelectedValue = "1" Then
            'DIVMatkhau.Visible = True
            DIVTT.Visible = False
        Else
            If cboLoaiSua.SelectedValue = "2" Then
                'DIVMatkhau.Visible = False
                DIVTT.Visible = True
            Else
                'DIVMatkhau.Visible = True
                DIVTT.Visible = True
            End If
        End If
    End Sub

    Protected Sub cboLoaiSua_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoaiSua.SelectedIndexChanged
        An_text()
    End Sub

    Protected Sub TimKiem()
        Dim objInf As New infUser()
        objInf.TEN_ND = Trim(txtTenND.Text.Trim.ToLower())
        objInf.Ten = Trim(txtHoTen.Text)
        objInf.MaCN = Trim(txtMaCN.Text)
        If cboTrangThai.SelectedValue <> "" Then
            objInf.Trang_thai = cboTrangThai.SelectedValue
        Else
            objInf.Trang_thai = ""
        End If
        If ddlKieuDN.SelectedValue <> "" Then
            objInf.LDAP = ddlKieuDN.SelectedValue
            If objInf.LDAP = "0" Then
                objInf.User_Domain = Trim(txtUserDomain.Text)
            End If
        Else
            objInf.LDAP = ""
        End If

        objInf.Chuc_Danh = Trim(txtChucdanh.Text)
        objInf.Ma_Nhom = Trim(cboNhomND.SelectedValue.ToString)
        objInf.MA_NV_TAO = Session.Item("User").ToString()
        dgUserAccount.CurrentPageIndex = 0

        If cboTrangThaiDuLieu.SelectedValue = "CurrentData" Then
            load_Datagrid(objInf)
            dgUserAccount.Visible = True
            dgApproveAction.Visible = False
        Else
            dgUserAccount.Visible = False
            dgApproveAction.Visible = True
            LoadGridApprove(objInf)
        End If
    End Sub

    Protected Sub LoadGridApprove(ByVal objUser As infUser)
        Dim dt As New DataTable
        dt = HeThong.buUsers.TCS_LST_DM_NHANVIEN_TEMP(objUser)
        If dt.Rows.Count > 0 Then
            cmdDuyet.Visible = True
            dgApproveAction.DataSource = dt
            dgApproveAction.DataBind()
        Else
            'cmdDuyet.Visible = False
            dgApproveAction.DataSource = dt
            dgApproveAction.DataBind()
        End If
        
    End Sub

    Protected Sub cmdTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTimKiem.Click
        TimKiem()
    End Sub

    Protected Sub dgUserAccount_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgUserAccount.PageIndexChanged
        Dim objInf As New infUser()
        objInf.TEN_ND = Trim(txtTenND.Text.Trim.ToLower())
        objInf.Ten = Trim(txtHoTen.Text)
        objInf.MaCN = Trim(txtMaCN.Text)
        If cboTrangThai.SelectedValue <> "" Then
            objInf.Trang_thai = cboTrangThai.SelectedValue
        Else
            objInf.Trang_thai = ""
        End If
        If ddlKieuDN.SelectedValue <> "" Then
            objInf.LDAP = ddlKieuDN.SelectedValue
            If objInf.LDAP = "0" Then
                objInf.User_Domain = Trim(txtUserDomain.Text)
            End If
        Else
            objInf.LDAP = ""
        End If

        objInf.Chuc_Danh = Trim(txtChucdanh.Text)
        objInf.Ma_Nhom = Trim(cboNhomND.SelectedValue.ToString)
        load_Datagrid(objInf)
        dgUserAccount.CurrentPageIndex = e.NewPageIndex
        dgUserAccount.DataBind()
    End Sub

    Protected Sub cmdKhoiPhuc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdKhoiPhuc.Click
        Dim strTenND As String
        Dim objUser As New infUser

        For i As Integer = 0 To dgUserAccount.Items.Count() - 1
            Dim chkSelect As CheckBox
            chkSelect = dgUserAccount.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                strTenND = Trim(dgUserAccount.Items(i).Cells(0).Text)
                Try
                    objUser.Ma_NV = strTenND
                    buAddUser.Delete(objUser, "0")
                    Exit For
                Catch ex As Exception
                    'Lỗi xoá dữ liệu không thành công
                    clsCommon.ShowMessageBox(Me, "Khoi phục NSD không thành công")
                    Return
                End Try
            End If
        Next
        clsCommon.ShowMessageBox(Me, "Thực hiện thành công")
        load_Datagrid()
    End Sub

    Protected Sub ddlKieuDN_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlKieuDN.SelectedIndexChanged
        If ddlKieuDN.SelectedValue.ToString() = "1" Then
            'DIVMatkhau.Visible = True
            UserDomain.Visible = False
            PassWord.Visible = True
            RePassWord.Visible = True
            PassWord.Focus()
        ElseIf ddlKieuDN.SelectedValue.ToString() = "0" Then
            UserDomain.Visible = True
            UserDomain.Focus()
            PassWord.Visible = False
            RePassWord.Visible = False
        Else
            UserDomain.Visible = False
            PassWord.Visible = False
            RePassWord.Visible = False
        End If
        txtTenCN.Text = Get_TenCN(txtMaCN.Text)
    End Sub

    Protected Sub cboNhomND_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNhomND.SelectedIndexChanged
        If cboNhomND.SelectedValue.ToString() = "04" Or cboNhomND.SelectedValue.ToString() = "15" Or cboNhomND.SelectedValue.ToString() = "0" Then
            trHMDen.Visible = True
            trHMTu.Visible = True
            txtTuHanMuc.Focus()
        Else
            trHMDen.Visible = False
            trHMTu.Visible = False
        End If
        txtTenCN.Text = Get_TenCN(txtMaCN.Text)
    End Sub

    Protected Sub cmdDuyet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDuyet.Click
        Dim action_ID As String = ""
        Dim success_count As Integer = 0
        Dim checked_count As Integer = 0

        For i As Integer = 0 To dgApproveAction.Items.Count() - 1
            Dim chkSelect As CheckBox
            chkSelect = dgApproveAction.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                action_ID = Trim(dgApproveAction.Items(i).Cells(0).Text)
                If HeThong.buUsers.ApproveUserTemp(action_ID, Session.Item("User").ToString()) Then
                    success_count += 1
                End If
                checked_count += 1
            End If
        Next

        If checked_count > 0 Then
            If success_count > 0 Then
                clsCommon.ShowMessageBox(Me, "Bạn đã duyệt thành công " & success_count.ToString() & " hành động")
            Else
                clsCommon.ShowMessageBox(Me, "Duyệt hành động thất bại. Vui lòng thử lại sau")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Bạn phải chọn ít nhất một hành động để duyệt")
        End If

        TimKiem()
    End Sub
End Class
