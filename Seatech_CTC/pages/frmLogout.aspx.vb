﻿Imports Business
Imports VBOracleLib
Imports System.Data
Imports System.Diagnostics
Partial Class pages_HeThong_frmLogout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim strIP_Local = Request.ServerVariables("REMOTE_HOST").ToString()
            If Not Session.Item("User") Is Nothing Then
                'Dim user As String = Session.Item("User").ToString()
                'HeThong.buUsers.UpdateTTLogin(User, "0")
                'Dim ht As Hashtable
                'ht = DirectCast(Application("SESSION_LIST"), Hashtable)
                'If ht.Count > 0 And ht.ContainsKey(user) = True Then
                '    ht.Remove(user)
                'End If
                HeThong.buUsers.Insert_Log_DN(Session.Item("User").ToString().Trim(), Session.Item("UserName").ToString().Trim(), strIP_Local.ToString(), 0, Session.Item("MA_CN_USER_LOGON").ToString(), "")
                HeThong.buUsers.UpdateTTLogin(Session.Item("User").ToString().Trim(), "0", strIP_Local)
                'Session.RemoveAll()

                Session.Clear()
                Session.Abandon()
                Session.RemoveAll()

                If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                    Response.Cookies("ASP.NET_SessionId").Value = String.Empty
                    Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
                End If

                'If Request.Cookies("AuthToken") IsNot Nothing Then
                '    Response.Cookies("AuthToken").Value = String.Empty
                '    Response.Cookies("AuthToken").Expires = DateTime.Now.AddMonths(-20)
                'End If


            End If
            Response.Redirect("frmLogin.aspx", False)
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình logout ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())
        End Try
    End Sub
End Class
