﻿Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports Business
Imports System.Data
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Diagnostics
Partial Class pages_frmRemoveLogin
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            RemoveHandler cmdHuy.Click, AddressOf cmdHuy_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            load_datagrid("",Session.Item("MA_CN_USER_LOGON").ToString())
        End If
    End Sub
    Private Sub load_datagrid(Optional ByVal strTenDN As String = "", Optional ByVal strMaCN As String = "")
        Try
            Dim ds As DataSet = Nothing
            Dim en As IDictionaryEnumerator
            If strTenDN.Trim <> "" Then
                ds = HeThong.buUsers.TKNHANVIEN_MADN(strTenDN, strMaCN)
            Else
                ds = HeThong.buUsers.TKNHANVIEN_MADN("", strMaCN)
            End If
            dgUserAccount.DataSource = ds
            dgUserAccount.DataBind()
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình load dữ liệu user đăng nhập")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())
        End Try
    End Sub

    Protected Sub cmdHuy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdHuy.Click
        Try
            Dim strIP_Local = Request.ServerVariables("REMOTE_HOST").ToString()
            Dim strTenND As String
            Dim en As IDictionaryEnumerator
            For i As Integer = 0 To dgUserAccount.Items.Count() - 1
                Dim chkSelect As CheckBox
                chkSelect = dgUserAccount.Items(i).FindControl("chkSelect")
                If chkSelect.Checked Then
                    strTenND = Trim(dgUserAccount.Items(i).Cells(0).Text)
                    If strTenND = "001" Then
                        clsCommon.ShowMessageBox(Me, "Bạn không thể xoá NSD là QTHT.")
                        Return
                    End If
                    If strTenND = Session.Item("User") Then
                        clsCommon.ShowMessageBox(Me, "Bạn không thể xoá chính user của mình.")
                        Return
                    End If
                    HeThong.buUsers.Insert_Log_DN(strTenND, strTenND, strIP_Local.ToString(), 2, Session.Item("MA_CN_USER_LOGON").ToString(), Session.Item("UserName").ToString().Trim())
                    HeThong.buUsers.UpdateTTLogin(strTenND, "0", "")
                    Dim ht As Hashtable

                    ht = DirectCast(Application("SESSION_LIST"), Hashtable)
                    'en = ht.GetEnumerator
                    ''Session.Remove(strTenND)
                    'While en.MoveNext
                    '    Dim session As HttpSessionState = DirectCast(en.Value, HttpSessionState)
                    '    If Not session Is Nothing Then
                    '        If session.Item("User").ToString() = strTenND Then
                    '            ht.Remove(strTenND)
                    '            session.RemoveAll()
                    '            Exit While
                    '        End If
                    '    End If
                    'End While
                End If
            Next
            clsCommon.ShowMessageBox(Me, "Hủy đăng nhập cho User thành công.")
            load_datagrid("", Session.Item("MA_CN_USER_LOGON").ToString())
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình hủy user đăng nhập")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())
            load_datagrid()
        End Try
    End Sub

    Protected Sub cmdTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTimKiem.Click
        If txtTenND.Text.Trim <> "" Then
            load_datagrid(txtTenND.Text.Trim, Session.Item("MA_CN_USER_LOGON").ToString())
        End If
    End Sub
End Class
