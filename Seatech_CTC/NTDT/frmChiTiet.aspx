﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmChiTiet.aspx.vb" Inherits="NTDT_frmChiTiet" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Chi tiết thông tin</title>
    <link href="../css/myStyles.css" type="text/css" rel="stylesheet" />
    <link href="../css/menuStyle.css" type="text/css" rel="stylesheet" />
     <style type="text/css">
        .txtHoanThien{width:90%;}
        .container{width:98%;}
    </style>
</head>
<body>
    <form id="form1" runat="server">
     <div class="container">
        <p class="pageTitle"> Chi tiết đăng ký sử dụng dịch vụ nộp thuế điện tử </p>
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
            <tr>
                <td align="left" style="width:25%" class="form_label">Mã số thuế<span class="requiredField">(*)</span></td>
                <td class="form_control"><asp:TextBox ID="txtMST" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên NNT<span class="requiredField">(*)</span></td>
                <td class="form_control"><asp:TextBox ID="txtTenNNT" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Địa chỉ NNT</td>
                <td class="form_control"><asp:TextBox ID="txtDCNNT" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Mã cơ quan thu<span class="requiredField">(*)</span></td>
                <td class="form_control"><asp:TextBox ID="txtMaCQT" runat="server" CssClass="inputflat txtHoanThien" MaxLength="5" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Email</td>
                <td class="form_control"><asp:TextBox ID="txtEmail" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số điện thoại</td>
                <td class="form_control"><asp:TextBox ID="txtSDT" runat="server" CssClass="inputflat txtHoanThien" MaxLength="15" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên người liên hệ</td>
                <td class="form_control"><asp:TextBox ID="txtNguoiLH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số serial chứng thư số<span class="requiredField">(*)</span></td>
                <td class="form_control"><asp:TextBox ID="txtSoSerial" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="200"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Nhà cung cấp chứng thư số</td>
                <td class="form_control"><asp:TextBox ID="txtNhaCC" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Mã ngân hàng</td>
                <td class="form_control"><asp:TextBox ID="txtMaNH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="10" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên ngân hàng</td>
                <td class="form_control"><asp:TextBox ID="txtTenNH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Mã TVAN</td>
                <td class="form_control"><asp:TextBox ID="txtMaTVAN" runat="server" CssClass="inputflat txtHoanThien" MaxLength="6" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên TVAN</td>
                <td class="form_control"><asp:TextBox ID="txtTenTVAN" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Ngày gửi</td>
                <td class="form_control"><asp:TextBox ID="txtNgayGui" runat="server" CssClass="inputflat txtHoanThien" MaxLength="10" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <%--<tr>
                <td align="left" style="width:25%" class="form_label">Số tài khoản</td>
                <td class="form_control"><asp:TextBox ID="txtSoTKNH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true"></asp:TextBox></td>
            </tr>--%>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên tài khoản</td>
                <td class="form_control"><asp:TextBox ID="txtTenTKNH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true"></asp:TextBox>
                    <asp:HiddenField ID="hdfNNTDT_ID" runat="server" />
                    <asp:HiddenField ID="hdrTRANG_THAI" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số tài khoản sử dụng NTDT</td>
                <td>
                    <asp:GridView ID="grdTKNH" runat="server" Width="80%" DataKeyNames="ID" ShowHeader="false"
                        EmptyDataText="Chưa có" EmptyDataRowStyle-CssClass="form_label"   AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="SO_TK_NH" Visible="true"/>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
