﻿Imports System.Data
Imports VBOracleLib
Imports System.Data.OracleClient

Partial Class pages_GuiLaiCtu
    Inherits System.Web.UI.Page

    Protected Function LayTTGip(ByVal tt As String)
        If tt.Equals("1") Then
            Return "Thành công"
        Else
            Return "Chưa thành công"
        End If
    End Function
    Protected Sub loaddata()
        Try
            Dim oCmd As OracleCommand = New OracleCommand()

            ' tuanda - 05/02/24
            Dim sql As String = "select so_ctu, so_gnt,tt_chuyen,MST_nNoP MST,tt_gip,ngay_chuyen from tcs_ctu_ntdt_hdr a"
            Dim where As String = " where ma_gd_phanhoi is null order by so_ctu desc"

            If txtSO_CT.Text.Trim.Length > 0 Then
                'where &= " and a.so_ctu like '%" & txtSO_CT.Text & "%'"
                where &= " and a.so_ctu like '%' || :so_ctu || '%'"
                oCmd.Parameters.AddWithValue("so_ctu", txtSO_CT.Text.Trim())
            End If
            If txtMST.Text.Trim.Length > 0 Then
                'where &= " and a.mst like '%" & txtMST.Text & "%'"
                where &= " and a.mst_nnop like '%' || :mst_nnop || '%'"
                oCmd.Parameters.AddWithValue("mst_nnop", txtMST.Text.Trim())
            End If
            If txtGNT.Text.Trim.Length > 0 Then
                'where &= " and a.so_gnt like '%" & txtGNT.Text & "%'"
                where &= " and a.so_gnt like '%' || :so_gnt || '%'"
                oCmd.Parameters.AddWithValue("so_gnt", txtGNT.Text.Trim())
            End If
            sql &= where

            oCmd.CommandText = sql
            Dim dt As DataTable = New DataTable()
            dt = DataAccess.ExecuteToTable(CommandType.Text, oCmd)
            dtgdata.DataSource = dt
            dtgdata.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        loaddata()
    End Sub

    Protected Sub btnGuiLai_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuiLai.Click

        Dim i As Integer
        Dim strSoCT As String
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True
        Dim thongbao As ETAX_GDT.ProcessXuLyTB = New ETAX_GDT.ProcessXuLyTB()
        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                Dim loai As String = Trim(dtgdata.Items(i).Cells(0).Text)
                strSoCT = Trim(dtgdata.Items(i).Cells(3).Text)
                Try
                    Dim err_code As String = ""
                    Dim err_msg As String = ""
                    thongbao.GuiThongBaoTungCT(strSoCT, err_code, err_msg)

                    If err_code = "0" Then
                        Dim sql As String = "update tcs_ctu_ntdt_hdr set tt_chuyen = (tt_chuyen+1), ngay_chuyen = TO_DATE('" & DateTime.Now.ToString("dd/MM/yyyy") & "', 'dd/MM/yyyy') where so_ctu='" & strSoCT & "'"
                        DataAccess.ExecuteNonQuery(sql, CommandType.Text)
                        'sql = "update ntdt_baocao_ctiet_r3 set tt_chuyen = (tt_chuyen+1), ngay_chuyen = TO_DATE('" & DateTime.Now.ToString("dd/MM/yyyy") & "', 'dd/MM/yyyy') where so_ctu='" & strSoCT & "'"
                        'DataAccess.ExecuteNonQuery(sql, CommandType.Text)
                    Else
                        clsCommon.ShowMessageBox(Me, "Gửi lại chứng từ lệch thất bại! " & err_msg)
                    End If
                Catch ex As Exception
                    clsCommon.ShowMessageBox(Me, "Gửi lại chứng từ lệch thất bại! " & ex.Message)
                End Try
                v_count += 1
            End If
        Next
        loaddata()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Gửi lại chứng từ lệch thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để gửi lại!")
        End If


    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        loaddata()
        dtgdata.CurrentPageIndex = e.NewPageIndex
        dtgdata.DataBind()
    End Sub


    Protected Sub btnGIP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGIP.Click
        Dim i As Integer
        Dim strSoCT As String = ""
        Dim strMST As String = ""
        Dim v_count As Integer = 0
        Dim v_flag As Boolean = True
        For i = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox
            chkSelect = dtgdata.Items(i).FindControl("chkSelect")
            If chkSelect.Checked Then
                Dim loai As String = Trim(dtgdata.Items(i).Cells(0).Text)
                strSoCT = Trim(dtgdata.Items(i).Cells(3).Text)
                strMST = Trim(dtgdata.Items(i).Cells(4).Text)
                Try
                    Dim err_code As String = ""
                    Dim err_msg As String = ""
                    ProcMegService.ProMsg.getMSG21(strMST, "", strSoCT, "", "", "00", err_code, err_msg)
                    If err_code.Equals("01") Then
                        DataAccess.ExecuteNonQuery("update tcs_ctu_ntdt_hdr set tt_gip='1', ngay_chuyen = TO_DATE('" & DateTime.Now.ToString("dd/MM/yyyy") & "', 'dd/MM/yyyy') where so_ctu='" + strSoCT + "'", CommandType.Text)
                    Else
                        clsCommon.ShowMessageBox(Me, "Gửi lại chứng từ qua GIP thất bại " & err_msg)
                    End If
                Catch ex As Exception
                    clsCommon.ShowMessageBox(Me, "Gửi lại chứng từ qua GIP thất bại! " & ex.Message)
                End Try
                v_count += 1
            End If
        Next
        loaddata()
        If v_count > 0 Then
            If v_flag Then
                clsCommon.ShowMessageBox(Me, "Gửi lại chứng từ qua GIP thành công!")
            End If
        Else
            clsCommon.ShowMessageBox(Me, "Hãy chọn ít nhất một bản ghi để gửi lại!")
        End If
    End Sub
End Class
