﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmCapNhatTrangThaiGNT.aspx.vb" Inherits="NTDT_frmCapNhatTrangThaiGNT" Title="Cập nhật trạng thái GNT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../jsdropdowlist/jquery.js" type="text/javascript"></script>
    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }
        function jsShowChiTietCT(_soct) {
            window.showModalDialog("../../NTDT/frmChiTietCTNTDT.aspx?soct=" + _soct, "", "dialogWidth:800px;dialogHeight:650px;help:0;status:0;", "_new");
        }
        function jsShowChiTietCT_HQ(_soct) {
            var tab = window.open("../../pages/ChungTu/frmChiTietCT_HQ.aspx?soct=" + _soct, "_blank");
            tab.focus();
        }
        $(document).ready(function () {


        });
        function SelectAllCheckboxes(spanChk) {

            // Added as ASPX uses SPAN for checkbox
            var oItem = spanChk.children;
            var theBox = (spanChk.type == "checkbox") ?
                spanChk : spanChk.children.item[0];
            xState = theBox.checked;
            elm = theBox.form.elements;

            for (i = 0; i < elm.length; i++)
                if (elm[i].type == "checkbox" &&
                         elm[i].id != theBox.id) {
                    //elm[i].click();
                    if (elm[i].checked != xState)
                        elm[i].click();
                    //elm[i].checked=xState;
                }
        }
    </script>
    <style type="text/css">
        .dataView
        {
            padding-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <p class="pageTitle">CẬP NHẬT TRẠNG THÁI GNT</p>
            </td>
        </tr>
        <tr>
            <td>
                <div style="width: 100%; margin-bottom: 30px">
                    <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">

                        <tr>
                            <td align="left" style="width: 15%" class="form_label">Từ ngày</td>
                            <td class="form_control" style="width: 35%"><span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                                <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                                    onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                            </span></td>
                            <td align="left" style="width: 15%" class="form_label">Đến ngày</td>
                            <td class="form_control" style="width: 35%">
                                <span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                                    <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                                        onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="form_label">Từ Số CT: </td>
                            <td class="form_control">
                                <asp:TextBox ID="txtTuSo_CT" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                            <td align="left" class="form_label">Đến Số CT: </td>
                            <td class="form_control">
                                <asp:TextBox ID="txtDenSo_CT" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                        </tr>
                       
                      <%--  <td align="left" class="form_label"></td>
                        <td class="form_control"></td>
                        <td class="form_control"></td>--%>
                        <tr style="display:none">
                        <td class="form_control">
                         <%--   <asp:DropDownList ID="txtMaCN" runat="server" CssClass="inputflat"
                                AutoPostBack="false" Style="width: 95%; border-color: White; font-weight: bold; text-align: left;">
                            </asp:DropDownList>--%>
                            <asp:HiddenField ID="hdfMA_CN" runat="server" />
                        </td>
                        </tr>
                        <tr>
                            <td align="left" class="form_label">Mã số thuế</td>
                            <td class="form_control">
                                <asp:TextBox ID="txtMST" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                            <td align="left" class="form_label">Tài khoản trích nợ</td>
                            <td class="form_control">
                                <asp:TextBox ID="txtTaiKhoanTrichNo" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                        </tr>
                       <tr style="display:none;">
                           
                            <td align="left" class="form_label">Số Tiền: </td>
                            <td class="form_control">
                                <asp:TextBox ID="txtSoTien" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                            <%-- <td align="left" class="form_label">Loại thuế</td>
                            <td class="form_control">
                                <asp:DropDownList ID="ddlLoaiThue" runat="server"  AppendDataBoundItems="true"> 
                                    <asp:ListItem Text="Thuế tại quầy" Value="1" Selected="True" />
                                    <asp:ListItem Text="Phí bộ ban ngành" Value="2" />
                                </asp:DropDownList>

                            </td>--%>
                        </tr>
                        
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Button ID="btnTimKiem" runat="server" Text="Tìm kiếm" Font-Bold="true" />
                                <asp:Button ID="btnCapNhat" runat="server" Text="Cập nhật" Font-Bold="true" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="dataView">
                    
                    <asp:GridView ID="dtgrd" runat="server" AutoGenerateColumns="False" ShowFooter="true"
                        Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True" PageSize='15'>
                        <AlternatingRowStyle CssClass="grid_item_alter"></AlternatingRowStyle>
                        <HeaderStyle CssClass="grid_header"></HeaderStyle>
                        <RowStyle CssClass="grid_item" />
                        <Columns>
                             <asp:TemplateField HeaderText="Select">
                                <ItemTemplate>
                                   <asp:CheckBox ID="chkSelect" runat="server" />
                                </ItemTemplate>
                                <HeaderTemplate>
                                     <input id="chkAll" onclick="javascript: SelectAllCheckboxes(this);" runat="server" type="checkbox" />
                                </HeaderTemplate>
                           </asp:TemplateField>
                            <asp:TemplateField HeaderText="STT">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex + 1%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <HeaderStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="so_ctu" HeaderText="Số chứng từ">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle VerticalAlign="Top" />
                            </asp:BoundField>
                             <asp:BoundField DataField="mst_nnop" HeaderText="Mã số thuế">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle VerticalAlign="Top" />
                            </asp:BoundField>
                             <asp:BoundField DataField="stk_nhang_nop" HeaderText="Số tài khoản">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%#Replace(Eval("tong_tien", "{0:n2}"), ",", "")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <HeaderTemplate>
                                    Số tiền
                                </HeaderTemplate>
                                <HeaderStyle VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Số CT Corebank" SortExpression="SoCTCorebank">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtSoCTCorebank" runat="server" Text='' Style="width: 95%" MaxLength="20"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Trạng thái hạch toán" SortExpression="TTHachToan">
                                <ItemTemplate>
                                   <asp:DropDownList ID="txtTrangThaiHT" runat="server" CssClass="inputflat"
                                        AutoPostBack="false" Style="width: 95%; border-color: White; font-weight: bold; text-align: left;">
                                        <asp:ListItem Text="Hạch toán thành công" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="Xử lý chứng từ không thành công" Value="13"></asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:BoundField DataField="ma_nguyente" HeaderText="Loại tiền">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle VerticalAlign="Top" />
                            </asp:BoundField>
                             <asp:BoundField DataField="ngay_lap" HeaderText="Ngày lập">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle VerticalAlign="Top" />
                            </asp:BoundField>
                         </Columns>
                        <PagerStyle HorizontalAlign="Right" BackColor="#E4E5D7" Font-Bold="False"
                            Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                            Font-Strikeout="False" Font-Underline="False" ForeColor="#000" VerticalAlign="Middle"></PagerStyle>
                    </asp:GridView>
                </div>
            </td>
        </tr>
    </table>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function () {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>
</asp:Content>
