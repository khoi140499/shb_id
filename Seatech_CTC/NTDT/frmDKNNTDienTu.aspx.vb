﻿Imports System.Data
'Imports ETAX_GDT

Partial Class NTDT_frmDKNNTDienTu
    Inherits System.Web.UI.Page

    Private objNNTDT As New Business.NTDT.NNTDT.buNNTDT

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        'Try
        '    Dim mq As clsMQHelper = New clsMQHelper()
        '    mq.PutMQMessage("<msg>hello</msg>")
        'Catch ex As Exception
        'End Try
        

        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            'RemoveHandler cmdTaoMoi.Click, AddressOf cmdTaoMoi_Click
            'RemoveHandler cmdDeleteUser.Click, AddressOf cmdDeleteUser_Click
            Exit Sub
        End If
        If Not IsPostBack Then
            If Not Request.Params("id") Is Nothing Then
                txtMST.Enabled = False
                load_Detail(Request.Params("id").Trim)
            End If
            'cmdDeleteUser.Attributes.Add("onclick", "return confirm ('Bạn thực sự muốn thực hiện không?') ")
        End If
    End Sub


    Protected Sub btnTiepTuc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTiepTuc.Click

    End Sub

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        load_dataGrid()
    End Sub

    Private Sub load_dataGrid()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtMST.Enabled Then
                If txtMST.Text <> "" Then
                    WhereClause += " AND MST like '%" & txtMST.Text.Trim & "%'"
                End If
                If txtDT.Text <> "" Then
                    WhereClause += " AND SDT_NNT = like '%" & txtDT.Text.Trim + "%'"
                End If
                If txtEmail.Text <> "" Then
                    WhereClause += " AND EMAIL_NNT like '%" + txtEmail.Text + "%'"
                End If
                If txtSerial.Text <> "" Then
                    WhereClause += " AND SERIAL_CERT_NTHUE like '%" + txtSerial.Text + "%'"
                End If
                If txtTCCTS.Text <> "" Then
                    WhereClause += " AND ISSUER_CERT_NTHUE like '%" + txtTCCTS.Text + "%'"
                End If
                If ddlNH.Text <> "" Then
                    WhereClause += " AND MA_NHANG = '" + ddlNH.SelectedValue + "'"
                End If
                If txtTKNH1.Text <> "" Then
                    WhereClause += " AND STK_NHANG1 like '%" + txtTKNH1.Text + "%' OR STK_NHANG2 like '%" + txtTKNH1.Text + "%' OR STK_NHANG3 like '%" + txtTKNH1.Text + "%' OR STK_NHANG4 like '%" + txtTKNH1.Text + "%' OR STK_NHANG5 like '%" + txtTKNH1.Text + "%'"
                End If

            End If

            ds = objNNTDT.GetDataSet(WhereClause)
            dtgrd.DataSource = ds.Tables(0)
            dtgrd.DataBind()
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub load_Detail(ByVal id As String)
        Dim ds As DataSet
        ds = objNNTDT.GetDataSet(" AND MST = '" + id + "'")
        If ds.Tables(0).Rows.Count > 0 Then
            txtMST.Text = ds.Tables(0).Rows(0)("MST").ToString
            txtDT.Text = ds.Tables(0).Rows(0)("SDT_NNT").ToString
            txtEmail.Text = ds.Tables(0).Rows(0)("EMAIL_NNT").ToString
            txtSerial.Text = ds.Tables(0).Rows(0)("SERIAL_CERT_NTHUE").ToString
            txtTCCTS.Text = ds.Tables(0).Rows(0)("ISSUER_CERT_NTHUE").ToString
            txtTKNH1.Text = ds.Tables(0).Rows(0)("STK_NHANG1").ToString
            txtTKNH2.Text = ds.Tables(0).Rows(0)("STK_NHANG2").ToString
            txtTKNH3.Text = ds.Tables(0).Rows(0)("STK_NHANG3").ToString
            txtTKNH4.Text = ds.Tables(0).Rows(0)("STK_NHANG4").ToString
            txtTKNH5.Text = ds.Tables(0).Rows(0)("STK_NHANG5").ToString
        End If
        
    End Sub

    Protected Sub xoa_Text()
        txtDT.Text = ""
        txtEmail.Text = ""
        txtTKNH1.Text = ""
        txtTKNH2.Text = ""
        txtTKNH3.Text = ""
        txtTKNH4.Text = ""
        txtTKNH5.Text = ""
    End Sub

    Protected Sub btnNhapLai_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNhapLai.Click
        xoa_Text()
    End Sub
End Class
