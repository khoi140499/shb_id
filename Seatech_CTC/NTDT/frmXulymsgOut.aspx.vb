﻿Imports System.Data
Imports System.Data.OracleClient
Imports VBOracleLib
Imports ETAX_GDT
Imports Business.Common.mdlCommon

Partial Class NTDT_frmXulymsgOut
    Inherits System.Web.UI.Page


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Search()
    End Sub

    Protected Function ProcessSelectedMsg(ByVal strID_Gdich As String) As Boolean
        Dim result As Boolean = True
        Dim strMess_Content As String = ""
        Dim processStatus As Boolean = True

        Dim strMA_NV As String = Session.Item("User").ToString()

        Try
            strMess_Content = DataAccess.ExecuteSQLScalar("select content_mess from tcs_dm_msg_out_ntdt where id_gdich ='" & strID_Gdich & "'").ToString()
        Catch ex As Exception
            result = False
        End Try

        Try
            Dim prsMsg As ETAX_GDT.clsMQHelper = New ETAX_GDT.clsMQHelper()
            prsMsg.PutMQMessage(strMess_Content)
        Catch ex As Exception
            result = False
            processStatus = False
        End Try
        Dim strSql As String = ""
        If processStatus Then
            strSql = "update tcs_dm_msg_out_ntdt set NHANVIEN_VL='" & strMA_NV & "',TTHAI_XL_LAI='SUCCESS',NGAY_XL_LAI=SYSDATE where id_gdich='" & strID_Gdich & "'"
        Else
            strSql = "update tcs_dm_msg_out_ntdt set NHANVIEN_VL='" & strMA_NV & "',TTHAI_XL_LAI='FAIL',NGAY_XL_LAI=SYSDATE where id_gdich='" & strID_Gdich & "'"
        End If
        DataAccess.ExecuteNonQuery(strSql, CommandType.Text)
        Return result
    End Function

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim succsessCount As Integer
        Dim selectedCount As Integer
        For i As Integer = 0 To dtgdata.Items.Count - 1

            Dim chkSelect As CheckBox = dtgdata.Items(i).FindControl("chkSelectItem")
            If chkSelect.Checked Then
                selectedCount += 1
                Dim strId_Gdich As String = dtgdata.Items(i).Cells(1).Text.Trim()

                If ProcessSelectedMsg(strId_Gdich) Then
                    succsessCount += 1
                End If
            End If
        Next

        If selectedCount = 0 Then
            clsCommon.ShowMessageBox(Me, "Bạn chưa chọn message nào để xử lý")
            Return
        Else
            If succsessCount > 0 Then
                clsCommon.ShowMessageBox(Me, "Đã xử lý thành công " & succsessCount.ToString() & " message trên " & selectedCount.ToString() & " message đã chọn")
            Else
                clsCommon.ShowMessageBox(Me, "Xử lý message thất bại. Vui lòng thử lại sau")
            End If
        End If
    End Sub

    Protected Function Search() As DataTable
        'If txtMsgId.Text.Trim() = "" And txtTranCode.Text.Trim() = "" Then
        '    clsCommon.ShowMessageBox(Me, "Bạn cần nhập một trong hai thông tin MSG_ID và TRAN_CODE.")
        '    Return New DataTable
        'End If

        Dim strSql = "select n.id_gdich,n.tran_code,n.msg_id,n.msg_refid,n.send_date,n.ngay_gdich,n.content_mess,h.so_ctu " & _
                    "from tcs_dm_msg_out_ntdt n, tcs_ctu_ntdt_hdr h" & _
                    " where n.id_link = h.ma_gd (+) "
        Dim oCmd As OracleCommand = New OracleCommand()

        If txtMsgId.Text.Trim() <> "" Then
            strSql += " and n.msg_refid = :msg_refid"
            oCmd.Parameters.AddWithValue("msg_refid", txtMsgId.Text.Trim())
        End If

        If txtTranCode.Text.Trim() <> "" Then
            strSql += " and n.tran_code = :tran_code"
            oCmd.Parameters.AddWithValue("tran_code", txtTranCode.Text.Trim())
        End If
        Dim strMst As String = txtMST.Text.Trim()
        If strMst <> "" Then
            strSql += " and (n.content_mess LIKE '%' || :content_mess || '%' or n.content_mess LIKE '%' || :content_mess1 || '%')"
            oCmd.Parameters.AddWithValue("content_mess", "<MST_NNOP>" & txtMST.Text.Trim())
            oCmd.Parameters.AddWithValue("content_mess1", "<MST>" & txtMST.Text.Trim())
        End If

        If txtTuNgay.Value.Trim() = "" And txtDenNgay.Value.Trim() <> "" Then
            strSql += " and to_date(n.ngay_gdich,'dd/MM/yyyy HH24:MI:SS')<=to_date('" & txtDenNgay.Value.Trim & " 00:00:00','dd/MM/yyyy HH24:MI:SS') "
        ElseIf txtTuNgay.Value.Trim() <> "" And txtDenNgay.Value.Trim() <> "" Then
            strSql += " and to_date(n.ngay_gdich,'dd/MM/yyyy HH24:MI:SS')>=to_date('" & txtTuNgay.Value.Trim() & " 00:00:00','dd/MM/yyyy HH24:MI:SS') and to_date(ngay_gdich,'dd/MM/yyyy HH24:MI:SS')<=to_date('" & txtDenNgay.Value.Trim() & " 23:59:59','dd/MM/yyyy HH24:MI:SS')"
        ElseIf txtTuNgay.Value.Trim() <> "" And txtDenNgay.Value.Trim() <> "" Then
            strSql += " and to_date(n.ngay_gdich,'dd/MM/yyyy HH24:MI:SS')<=to_date('" & txtDenNgay.Value.Trim() & " 23:59:59','dd/MM/yyyy HH24:MI:SS') "
        End If

        oCmd.CommandText = strSql + " order by n.ngay_gdich desc "
        Dim dtSearchResult As DataTable = DataAccess.ExecuteToTable(CommandType.Text, oCmd)

        If dtSearchResult.Rows.Count > 0 Then
            dtgdata.DataSource = dtSearchResult
            dtgdata.DataBind()
        Else
            clsCommon.ShowMessageBox(Me, "Không có dữ liệu.")
        End If

        Return dtSearchResult
    End Function

    Protected Sub ExportXML(ByVal strXML_Content As String, ByVal strID_Gdich As String)
        Dim data As Byte() = System.Text.Encoding.UTF8.GetBytes(strXML_Content)
        Dim strFileName As String = "GDICH_" + strID_Gdich
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.ContentType = "text/xml"
        HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename= " + strFileName + ".xml")
        HttpContext.Current.Response.AddHeader("Content-Length", data.Length.ToString())
        HttpContext.Current.Response.ContentType = "application/octet-stream"
        HttpContext.Current.Response.BinaryWrite(data)
        HttpContext.Current.Response.[End]()
    End Sub

    Protected Sub dtgdata_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgdata.ItemCommand
        Dim commandName = e.CommandName.ToString()
        Dim strID_Gdich = e.CommandArgument.ToString()
        Select Case commandName
            Case "ExportMsg"
                Dim strMess_Content As String = DataAccess.ExecuteSQLScalar("select content_mess from tcs_dm_msg_out_ntdt where id_gdich ='" & strID_Gdich & "'").ToString()
                ExportXML(strMess_Content, strID_Gdich)
        End Select
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = "Xử lý message out"

        If Not IsPostBack Then
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        Try
            dtgdata.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception

        End Try
        Search()
    End Sub
End Class
