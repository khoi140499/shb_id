﻿Imports VBOracleLib
Imports System.Data
Imports System.Diagnostics
Imports Business

Partial Class NTDT_frmCapNhatTrangThaiGNT
    Inherits System.Web.UI.Page
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private mv_objUser As New CTuUser
    Private Shared strMaNhom_GDV As String = ConfigurationManager.AppSettings.Get("GROUP_TELLER").ToString()
    Dim total As Decimal = 0
    Dim totalRow As Integer = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            txtTuNgay.Value = Business.Common.mdlCommon.ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = Business.Common.mdlCommon.ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            'DM_DiemThu()
            'DM_User()
            hdfMA_CN.Value = Session.Item("MA_CN_USER_LOGON").ToString
        End If
    End Sub

    Private Sub load_dataGrid()
        total = 0
        totalRow = 0
        Dim strTuSo As String = txtTuSo_CT.Text.Trim
        Dim strDenSo As String = txtDenSo_CT.Text.Trim
        Dim pv_strSo_FCC As String = ""
        Dim strTuSoFT As String = ""
        Dim strDenSoFT As String = ""
        Dim pv_strSo_refCITAD As String = ""
        Dim strTuNgay As String = txtTuNgay.Value
        Dim strDenNgay As String = txtDenNgay.Value
        Dim strMaNNT As String = txtMST.Text.Trim
        Dim strTaiKhoanTrichNo As String = txtTaiKhoanTrichNo.Text.Trim
        Dim strTKCo As String = ""
        Dim strTKNo As String = ""
        Dim strKHCT As String = ""
        Dim strSoTien As String = txtSoTien.Text.Trim
        Dim WhereClause As String = ""
        Try
            If strTuSo <> "" Then
                WhereClause += " AND UPPER(a.so_ctu)>=" & strTuSo & ""
            End If
            If strDenSo <> "" Then
                WhereClause += " AND  UPPER(a.so_ctu)<=" & strDenSo & ""
            End If
            '''''''''''''
            Dim strNgayStart As String
            strNgayStart = Business.Common.mdlCommon.ConvertDateToNumber(strTuNgay.ToString())
            Dim strNgayFinish As String = Business.Common.mdlCommon.ConvertDateToNumber(strDenNgay.ToString())
            'bo tim kiem theo ngay ngay kb

            If strNgayFinish = "0" And strNgayStart <> "0" Then
                strNgayFinish = DateTime.Now.ToString("yyyyMMdd")
                WhereClause += " and ( (TO_CHAR(a.ngay_nhang,'yyyymmdd') >=" & strNgayStart & " and TO_CHAR(a.ngay_nhang,'yyyymmdd') <=" & strNgayFinish & "))"
            ElseIf strNgayFinish <> "0" And strNgayStart = "0" Then
                WhereClause += " and (TO_CHAR(a.ngay_nhang,'yyyymmdd') <=" & strNgayFinish & ")"
            ElseIf strNgayStart <> "0" And strNgayFinish <> "0" Then
                WhereClause += " and ( (TO_CHAR(a.ngay_nhang,'yyyymmdd') >=" & strNgayStart & " and TO_CHAR(a.ngay_nhang,'yyyymmdd') <=" & strNgayFinish & ") )"
            End If

            If strMaNNT <> "" Then
                WhereClause += " AND a.mst_nnop LIKE '%" & strMaNNT & "%'"
            End If

            If strTKCo <> "" Then
                WhereClause += " AND  a.stk_nhang_nop LIKE '%" & strTaiKhoanTrichNo & "%'"
            End If

            If strSoTien <> "" Then
                WhereClause += " AND a.tong_tien=" & CInt(strSoTien) & ""

            End If

            Dim strSQL As String = ""

            'vinhvv
            strSQL = "SELECT a.so_ctu, a.mst_nnop, a.stk_nhang_nop, a.tong_tien, a.ma_nguyente," & _
                " a.ngay_lap " & _
                "  FROM tcs_ctu_ntdt_hdr a" & _
                " WHERE 1 = 1 AND tthai_ctu='15' " & WhereClause & _
                " ORDER BY a.so_ctu DESC "

            Dim dt = DatabaseHelp.ExecuteToTable(strSQL)
            If dt.Rows.Count > 0 Then
                totalRow = dt.Rows.Count
                For Each vidu In dt.Rows
                    total += Decimal.Parse(vidu("tong_tien").ToString)
                Next
                dtgrd.DataSource = dt
                dtgrd.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub

    Protected Sub dtgrd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dtgrd.PageIndexChanging
        load_dataGrid()
        dtgrd.PageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub


    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        load_dataGrid()
        dtgrd.PageIndex = 0
        dtgrd.DataBind()
    End Sub
  
    Protected Sub btnCapNhat_Click(sender As Object, e As EventArgs) Handles btnCapNhat.Click

        Dim strSo_CT As String = ""
        Dim strMST As String = ""
        Dim strSo_TK As String = ""
        Dim strSo_Tien As String = ""
        Dim strSo_CT_NH As String = ""
        Dim strTrangThaiHT As String = ""
        Dim strMoTa_LoiHT As String = ""

        Dim strTen_TrangThai As String = ""

        Dim c As String = dtgrd.Rows.Count
        Dim strMA_NV As String = Session.Item("User").ToString
        For i As Integer = 0 To dtgrd.Rows.Count - 1
            Dim isChecked As CheckBox = DirectCast(dtgrd.Rows(i).FindControl("chkSelect"), CheckBox)
            If isChecked.Checked Then
                Dim strSoCTCorebank As TextBox = dtgrd.Rows(i).FindControl("txtSoCTCorebank")
                Dim strtxtTrangThaiHT As DropDownList = dtgrd.Rows(i).FindControl("txtTrangThaiHT")

                strSo_CT = dtgrd.Rows(i).Cells(2).Text.Trim

                strTrangThaiHT = strtxtTrangThaiHT.SelectedValue
                strSo_CT_NH = strSoCTCorebank.Text.Trim

             
                If strSo_CT_NH.Length > 50 Then
                    clsCommon.ShowMessageBox(Me, "Số CT Corebank vượt quá 50 ký tự")
                ElseIf strTrangThaiHT.Equals("11") And strSo_CT_NH.Length <= 0 Then
                    clsCommon.ShowMessageBox(Me, "Số CT Corebank không được để trống")
                Else
                    If strTrangThaiHT.Equals("11") Then
                        strTen_TrangThai = "Hạch toán thành công"
                    ElseIf strtxtTrangThaiHT.Equals("13") Then
                        strTen_TrangThai = "Xử lý chứng từ không thành công"
                    End If
                    'Update du lieu va trang thai vao bang hdr va bang chung tu(Thanh cong)
                    If UpdateHDR_ChungTu(strSo_CT, strSo_CT_NH, strTrangThaiHT, strTen_TrangThai) Then
                        clsCommon.ShowMessageBox(Me, "Cập nhật dữ liệu thành công")
                    Else
                        clsCommon.ShowMessageBox(Me, "Cập nhật dữ liệu thất bại")
                    End If
                End If
            End If

        Next

        'Redirect to same page again
        Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
    End Sub

    Private Sub DM_DiemThu()

        Dim dsDiemThu As DataSet
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        If strcheckHSC = "1" Then
            'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
            ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
            strSQL = "SELECT a.id, a.id ||'-'||a.name nam FROM tcs_dm_chinhanh a order by a.id "

        Else

            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' or a.id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' order by a.id"
        End If
        Try
            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Có lỗi trong quá trình lẫy thông tin chi nhánh", Session.Item("TEN_DN"))
            Throw ex
        Finally

        End Try

    End Sub
    'Private Sub DM_User()
    '    Dim cnUser As New DataAccess
    '    Dim dsUser As DataSet
    '    Dim strSQL As String = ""
    '    strSQL = "SELECT DISTINCT (CASE  WHEN HDR.MA_NV=0 THEN '' ELSE (SELECT TO_CHAR(NV1.MA_NV) FROM TCS_DM_NHANVIEN NV1 WHERE NV1.MA_NV=HDR.MA_NV) END) AS MA_NV,(CASE  WHEN HDR.MA_NV=0 THEN '' ELSE (SELECT NV1.TEN_DN FROM TCS_DM_NHANVIEN NV1 WHERE NV1.MA_NV=HDR.MA_NV) END) AS NGUOI_LAP " & _
    '             " FROM TCS_CTU_HDR hdr,tcs_dm_nhanvien nv,tcs_dm_chinhanh cn,tcs_dm_lthue lt " & _
    '             " WHERE hdr.ma_nv=nv.ma_nv and nv.ma_cn = cn.id "
    '    Try
    '        dsUser = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
    '    Catch ex As Exception
    '        clsCommon.WriteLog(ex, "Có lỗi trong quá trình lẫy thông tin user", Session.Item("TEN_DN"))
    '        Throw ex
    '    Finally
    '        If (Not IsNothing(cnUser)) Then DataAccess.Dispose()
    '    End Try

    'End Sub

    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = BaoCao.buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub


    Protected Function layTenCN(ByVal ma_cn As String) As String
        Dim tencn As String = ""
        Try
            tencn = DataAccess.ExecuteReturnDataSet("select name from tcs_dm_chinhanh where branch_code = '" + ma_cn + "'", CommandType.Text).Tables(0).Rows(0)("name").ToString
        Catch ex As Exception

        End Try

        Return tencn
    End Function


    Public Function LOAD_MN() As String
        Dim str_return As String = ""
        Dim strSQL_MaNhom As String = "select ma_nhom from tcs_dm_nhanvien where ma_nv='" & Session.Item("User").ToString & "'"
        Dim dt As New DataTable
        dt = DataAccess.ExecuteToTable(strSQL_MaNhom)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            str_return = dt.Rows(0)("ma_nhom").ToString
        End If
        Return str_return
    End Function
    Private Function DM_NV() As String
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable
        Dim dk As String = ""
        Dim strcheckHS As String = checkHSC()
        Dim str_Ma_Nhom As String = ""
        Dim strSQL As String = ""
        'If cboDiemThu.Text.Equals("") Then
        If strcheckHS <> "1" Then
            If strcheckHS = "2" Then
                dk = "and (b.branch_id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "' or  b.id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "') "
            Else
                dk = "and b.id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "' "
            End If
        End If
        'Else
        'dk = "And ma_cn ='" & cboDiemThu.SelectedValue.ToString & "'"
        ' End If
        str_Ma_Nhom = LOAD_MN()
        If str_Ma_Nhom = strMaNhom_GDV Then
            strSQL = "SELECT a.Ma_NV,a.ten_DN FROM tcs_dm_NhanVien a,tcs_dm_chinhanh b Where a.ma_cn=b.id and a.ma_nv='" & Session.Item("User").ToString & "' and a.ma_nhom='" & strMaNhom_GDV & "' " & dk
        Else
            strSQL = "SELECT a.Ma_NV,a.ten_DN FROM tcs_dm_NhanVien a,tcs_dm_chinhanh b Where a.ma_cn=b.id and a.ma_nhom='" & strMaNhom_GDV & "' " & dk
        End If
        If strcheckHS = "1" Then
            strSQL = "SELECT a.Ma_NV,a.ten_DN FROM tcs_dm_NhanVien a"
        End If
        'strSQL = "SELECT a.Ma_NV,a.ten_DN FROM tcs_dm_NhanVien a,tcs_dm_chinhanh b Where a.ma_cn=b.id and a.ma_nhom='02' " & dk
        'huynt
        'ma_nhom='03': Nhóm Giao Dịch Viên
        strSQL = "SELECT a.Ma_NV,a.ten_DN FROM tcs_dm_NhanVien a,tcs_dm_chinhanh b Where a.ma_cn=b.id and a.ma_nhom='03' " & dk
        'end huynt
        dt = DataAccess.ExecuteToTable(strSQL)
        Dim noOfItems As Integer = dt.Rows.Count
        Dim myJavaScript As StringBuilder = New StringBuilder()
        myJavaScript.Append(" var arrMaNV= new Array(" & noOfItems.ToString & ");")
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrMaNV[" & i & "] ='" & dt.Rows(i)("Ma_NV") & ";" & dt.Rows(i)("ten_DN") & "';")
            Next
            'Return myJavaScript.ToString()
        End If
        Return myJavaScript.ToString()
        'Return String.Empty
    End Function

    Private Function DM_KenhCtu() As String

        Dim dsKCTu As DataTable
        Dim dk As String = ""
        'dk = "And ma_cn like'%" & cboDiemThu.SelectedValue.ToString & "%'"

        Dim strSQL As String = "SELECT a.ma_kenh,a.ten_kenh FROM tcs_kenh_ct a " ' & dk
        Try
            dsKCTu = DataAccess.ExecuteToTable(strSQL)
            'clsCommon.ComboBoxWithValue(cboKenhCtu, dsKCTu, 1, 0, "Tất cả")
            Dim noOfItems As Integer = dsKCTu.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrKenhCT= new Array(" & noOfItems.ToString & ");")
            If Not VBOracleLib.Globals.IsNullOrEmpty(dsKCTu) Then
                For i As Integer = 0 To dsKCTu.Rows.Count - 1
                    myJavaScript.Append(" arrKenhCT[" & i & "] ='" & dsKCTu.Rows(i)("ma_kenh") & ";" & dsKCTu.Rows(i)("ten_kenh") & "';")
                Next
                'Return myJavaScript.ToString()
            End If
            Return myJavaScript.ToString()
            'Return String.Empty
        Catch ex As Exception
            Return String.Empty
            Throw ex
        Finally

        End Try
    End Function

    Private Function DM_LOAITHUE() As String

        Dim dt As DataTable

        Dim strSQL As String = "SELECT * from TCS_DM_LTHUE ORDER BY MA_LTHUE "
        dt = DataAccess.ExecuteToTable(strSQL)
        Dim noOfItems As Integer = dt.Rows.Count
        Dim myJavaScript As StringBuilder = New StringBuilder()
        myJavaScript.Append(" var arrMaLTHUE= new Array(" & noOfItems.ToString & ");")
        myJavaScript.Append(" arrMaLTHUE[0] ='00;<< Tất cả >>';")
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrMaLTHUE[" & i + 1 & "] ='" & dt.Rows(i)("MA_LTHUE") & ";" & dt.Rows(i)("TEN") & "';")
            Next
            'Return myJavaScript.ToString()
        End If
        Return myJavaScript.ToString()
        'Return String.Empty
    End Function

   
    Private Function UpdateHDR_ChungTu(ByVal strSo_Ct As String, ByVal strSo_CT_NH As String, ByVal strTrang_Thai As String, ByVal strTen_TrangThai As String) As Boolean

        Dim transCT As IDbTransaction = Nothing
        Dim conn As OracleClient.OracleConnection = Nothing
        'transCT = DataAccess.BeginTransaction()

        Dim check As Boolean = True
        Dim strSQL As String = ""
      
        Try
            conn = DataAccess.GetConnection()
            transCT = conn.BeginTransaction()
            'Update bang tcs_ctu_hdr
            strSQL = "UPDATE TCS_CTU_NTDT_HDR SET SO_CT_NH='" & strSo_CT_NH & "', tthai_ctu='" & strTrang_Thai & "',ten_tthai_ctu='" & strTen_TrangThai & "', MA_GD_PHANHOI='' " & _
                               " WHERE SO_CTU='" & strSo_Ct & "' AND tthai_ctu='15'"

            DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

            transCT.Commit()
        Catch ex As Exception
            transCT.Rollback()
            Throw ex
        Finally
            If Not transCT Is Nothing Then
                transCT.Dispose()
            End If
            If Not conn Is Nothing Then
                conn.Close()
                conn.Dispose()
            End If
        End Try


        Return check
    End Function

End Class
