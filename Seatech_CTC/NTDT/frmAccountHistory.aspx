﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmAccountHistory.aspx.vb" Inherits="NTDT_frmAccountHistory" title="Tra cứu thông tin thay đổi tài khoản - MST" %>
<%@ Import Namespace="Business.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
    </script>
    <style type="text/css">
        .container{width:100%; margin-bottom:30px}
        .dataView{padding-bottom:10px}
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle"> Tra cứu thông tin thay đổi tài khoản - MST </p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
            <tr>
                <td align="left" style="width:15%" class="form_label">Từ ngày</td>
                <td class="form_control"><span style="display:block; width:40%; position:relative;vertical-align:middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                    onblur="CheckDate(this);" onfocus="this.select()" />
                </span></td>
                <td align="left" style="width:15%" class="form_label">Đến ngày</td>
                <td class="form_control">
                <span style="display:block; width:40%; position:relative; vertical-align:middle;">
                    <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                     onblur="CheckDate(this);" onfocus="this.select()" />
                </span></td>
            </tr>
            <tr>
                <td align="left" class="form_label">Mã số thuế</td>
                <td class="form_control"><asp:TextBox ID="txtMST" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                 <td align="left" class="form_label">Số tài khoản</td>
                <td class="form_control"><asp:TextBox ID="txtTKNH" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnTimKiem" runat="server" Text="Tìm kiếm" />
                    <asp:Button ID="btnExport" runat="server" Text="Export" />
                </td>
            </tr>
        </table>
    </div>
    <div class="dataView">
        <asp:DataGrid ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="100%" BorderColor="#000" CssClass="grid_data" AllowPaging="True">
            <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
            <ItemStyle CssClass="grid_item" />
            <Columns>
                <asp:BoundColumn DataField="MST" HeaderText="Mã số thuế">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="SO_TK_NH" HeaderText="Số TK">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <%--<asp:BoundColumn DataField="COMPANY" HeaderText="Tên TK">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>--%>
                <asp:BoundColumn DataField="NGAY_TH" HeaderText="Ngày thay đổi">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
            </Columns>
            <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                Font-Strikeout="False" Font-Underline="False" ForeColor="#000" VerticalAlign="Middle">
            </PagerStyle>
        </asp:DataGrid>
        <asp:GridView ID="grdExport" runat="server" AutoGenerateColumns="False" ShowFooter="true" Visible="false"
            Width="100%" BorderColor="#000" CssClass="grid_data" AllowPaging="false" >
            <AlternatingRowStyle CssClass="grid_item_alter"></AlternatingRowStyle>
            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
            <RowStyle CssClass="grid_item" />
            <Columns>
                <asp:BoundField DataField="MST" HeaderText="Mã số thuế">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="SO_TK_NH" HeaderText="Số TK">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <%--<asp:BoundField DataField="COMPANY" HeaderText="Tên TK">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>--%>
                
                <asp:BoundField DataField="NGAY_TH" HeaderText="Ngày thay đổi">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
            </Columns>
            <PagerStyle HorizontalAlign="Right" BackColor="#E4E5D7" Font-Bold="False"
                Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                Font-Strikeout="False" Font-Underline="False" ForeColor="#000" VerticalAlign="Middle">
            </PagerStyle>
        </asp:GridView>
    </div>
        
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>		
</asp:Content>

