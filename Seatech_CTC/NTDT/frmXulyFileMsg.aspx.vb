﻿Imports System.IO
Imports System.Data

Partial Class NTDT_frmXulyFileMsg
    Inherits System.Web.UI.Page

    Dim prsMsg As ETAX_GDT.ProcessMSG = New ETAX_GDT.ProcessMSG

    Private Function GetFilesFromFolder(ByVal extension As String) As FileInfo()
        Dim strMessageFolder As String = "c:\\"
        Try
            strMessageFolder = System.Configuration.ConfigurationManager.AppSettings.Get("MESSAGE_FOLDER")

            If strMessageFolder.EndsWith("\") = False Then
                strMessageFolder = strMessageFolder.Trim + "\"
            End If

            If Directory.Exists(strMessageFolder) = False Then
                Directory.CreateDirectory(strMessageFolder)
            End If
        Catch ex As Exception

        End Try

        Dim directoryInfo As New DirectoryInfo(strMessageFolder)
        Dim internalExtension As String = String.Concat("*.", extension)
        Dim fileInfo As FileInfo() = directoryInfo.GetFiles(internalExtension, SearchOption.TopDirectoryOnly)
        Return fileInfo
    End Function

    Private Sub BindFileNamesToList()
        Dim extension As String = "txt"
        Dim fileInfo As FileInfo() = GetFilesFromFolder(If((extension = ""), "txt", extension))

        Dim dtFileList As New DataTable
        dtFileList.Columns.Add("FileName")
        dtFileList.Columns.Add("CreateTime")
        dtFileList.Columns.Add("FullPath")

        For Each fileInfoTemp As FileInfo In fileInfo
            Dim dtr As DataRow = dtFileList.NewRow
            dtr("FileName") = fileInfoTemp.Name
            dtr("CreateTime") = fileInfoTemp.CreationTime
            dtr("FullPath") = fileInfoTemp.FullName
            dtFileList.Rows.Add(dtr)
        Next

        Dim dv As DataView = dtFileList.DefaultView
        dv.Sort = "CreateTime desc"
        dtFileList = dv.ToTable()

        dtgFileList.DataSource = dtFileList
        dtgFileList.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = "Xử lý message từ file"
        If Not IsPostBack Then
            BindFileNamesToList()
        End If
    End Sub

    Protected Sub btnProcessMsg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessMsg.Click
        Dim checkItemCount As Integer = 0

        For Each dtgItem As DataGridItem In dtgFileList.Items
            Dim hdFilePath As HiddenField = dtgItem.FindControl("hdFilePath")
            Dim chkSelectItem As CheckBox = dtgItem.FindControl("chkSelectItem")

            If chkSelectItem.Checked Then
                checkItemCount += 1
                Dim strFullPath As String = hdFilePath.Value
                Dim strMess_Content As String = System.IO.File.ReadAllText(strFullPath)
                Try
                    prsMsg.process_Msg(strMess_Content)
                    File.Delete(strFullPath)
                Catch ex As Exception

                End Try
            End If
        Next
        BindFileNamesToList()

        If checkItemCount = 0 Then
            clsCommon.ShowMessageBox(Me, "Bạn phải chọn ít nhất một file để xử lý")
        Else
            clsCommon.ShowMessageBox(Me, "Xử lý thành công")
        End If


    End Sub

    Protected Sub btnDeleteMsg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteMsg.Click
        Dim checkedItemCount As Integer = 0
        For Each dtgItem As DataGridItem In dtgFileList.Items
            Dim hdFilePath As HiddenField = dtgItem.FindControl("hdFilePath")
            Dim chkSelectItem As CheckBox = dtgItem.FindControl("chkSelectItem")

            If chkSelectItem.Checked Then
                Dim strFullPath As String = hdFilePath.Value
                checkedItemCount += 1
                Try
                    File.Delete(strFullPath)
                Catch ex As Exception

                End Try
            End If
        Next
        BindFileNamesToList()

        If checkedItemCount = 0 Then
            clsCommon.ShowMessageBox(Me, "Bạn phải chọn ít nhất 1 file để xóa")
        Else
            clsCommon.ShowMessageBox(Me, "Xử lý thành công")
        End If
    End Sub

    Protected Sub dtgFileList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgFileList.PageIndexChanged
        dtgFileList.CurrentPageIndex = e.NewPageIndex
        BindFileNamesToList()
    End Sub
End Class
