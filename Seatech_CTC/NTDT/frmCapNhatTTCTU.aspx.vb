﻿Imports System.Data
Imports Business.NTDT.NNTDT
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao
Imports Newtonsoft.Json

Partial Class pages_NTDT_frmCapNhatTTCTU
    Inherits System.Web.UI.Page
    Shared total As Decimal = 0
    Shared totalRow As Integer = 0
    Private Shared user As String = ""
    Private Shared folderFile As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            DM_TrangThai()
            user = Session.Item("User")
        End If
    End Sub
    Protected Function GetMoTa_TrangThai(ByVal strTrang_Thai As String) As String
        Try
            Dim result As String = ""
            Dim sql As String = "select mota from tcs_dm_trangthai_ntdt where trang_thai='" & strTrang_Thai & "' and GHICHU='CT'"
            Dim dr As IDataReader = DataAccess.ExecuteDataReader(sql, CommandType.Text)
            If dr.Read Then
                result = dr("MOTA").ToString
            End If
            Return result
        Catch ex As Exception
            Return strTrang_Thai
        End Try
    End Function
    Private Shared Function ReadHistory(ByVal soct As String) As String
        Dim x As String = ""
        Try
            folderFile = System.Web.HttpContext.Current.Server.MapPath("~/NTDT_Action/" & soct & ".txt")
            x = ETAX_GDT.Libxxx.ReadFromFile(folderFile)
        Catch ex As Exception
            Return ""
        End Try
        Return x
    End Function
    Private Shared Function GetTTUser(ByVal strUS As String) As String
        Try
            Dim sql As String = "select ten, ten_dn from tcs_dm_nhanvien where ma_nv='" & strUS & "'"
            Dim ds As New DataTable()
            ds = DataAccess.ExecuteReturnDataTable(sql, CommandType.Text, (New List(Of IDataParameter)).ToArray())
            Return ds.Rows(0)(0).ToString() & "[" & ds.Rows(0)(1).ToString() & "]"
        Catch ex As Exception
            Return ""
        End Try
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function UpdateCtu(ByVal soct As String, ByVal ghichu As String,
                                         ByVal trangthai As String, ByVal tentrangthai As String,
                                         ByVal tentrangthaiold As String) As String
        Try
            Dim sql As String = "UPDATE tcs_ctu_ntdt_hdr " &
                                "SET tthai_ctu = '" & trangthai & "', ten_tthai_ctu = N'" & tentrangthai & "',ma_gd_phanhoi = '' " &
                                "WHERE so_ctu = '" & soct & "'"
            DataAccess.ExecuteNonQuery(sql, CommandType.Text)

            'Thanh con ghi log action ra file 
            Dim fileLog As String = "NTDT_Action"
            Dim actionName As String = "- " & Date.Now.ToString("dd/MM/yyyy HH:mm:ss") & ": User " & GetTTUser(user) & " vừa thực hiện cập nhật trạng thái chứng từ " & soct & "[" & tentrangthaiold & "] sang " & tentrangthai + " - lý do: " & ghichu
            ETAX_GDT.Libxxx.WriteFile(fileLog, actionName, soct)
        Catch ex As Exception
            Return "Cập nhật trạng thái không thành công, vui lòng thử lại"
        End Try
        Return "Cập nhật trạng thái chứng từ thành công"
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function LoadCTList(ByVal strTuNgay As String, ByVal strDenNgay As String,
                                         ByVal strMaNNT As String, ByVal strGNT As String, ByVal strSoCT As String) As String
        total = 0
        totalRow = 0
        Dim objNNTDT As New buNNTDT
        Dim WhereClause As String = ""
        Dim lsObj As New List(Of Newtonsoft.Json.Linq.JObject)
        Try
            If strTuNgay <> "" Then
                WhereClause += " AND (TO_CHAR(A.NGAY_NHANG,'yyyymmdd') >= " & ConvertDateToNumber(strTuNgay) & ") "
            End If
            If strDenNgay <> "" Then
                WhereClause += " AND (TO_CHAR(A.NGAY_NHANG,'yyyymmdd') <= " & ConvertDateToNumber(strDenNgay) & ") "
            End If
            If strSoCT <> "" Then
                WhereClause += " AND A.SO_CT = '" & strSoCT.Trim & "' "
            End If

            If strMaNNT <> "" Then
                WhereClause += " AND A.MST_NNOP like '%" & strMaNNT & "%' "
            End If
            If strGNT <> "" Then
                WhereClause += " AND A.so_gnt like '%" + strGNT + "%' "
            End If

            Dim strSQL As String =
                                    "SELECT a.so_ctu so_ct," &
                                    "       a.id_ctu id," &
                                    "       a.mst_nnop mst," &
                                    "       a.so_gnt," &
                                    "       TO_CHAR (ngay_chuyen, 'dd/MM/rrrr') AS ngay_chuyen," &
                                    "       TO_CHAR (ngay_ht, 'dd/MM/rrrr') AS ngay_hachtoan," &
                                    "       a.ten_nnop," &
                                    "       a.stk_nhang_nop," &
                                    "       a.ten_tthai_ctu," &
                                    "       a.tong_tien," &
                                    "       a.so_ct_nh AS ref_no" &
                                    "  FROM tcs_ctu_ntdt_hdr a " &
                                     "WHERE 1 = 1 " & WhereClause & " order by so_ct desc"

            Dim dt = DatabaseHelp.ExecuteToTable(strSQL)
            Dim dem As Decimal = 1
            If dt.Rows.Count > 0 Then
                For Each item In dt.Rows
                    Dim obj As New Newtonsoft.Json.Linq.JObject()
                    Dim id As String = item(1).ToString()
                    Dim so_ct As String = item(0).ToString()
                    Dim mst As String = item(2).ToString()
                    Dim so_gnt As String = item(3).ToString()
                    Dim ngay_chuyen As String = item(4).ToString()
                    Dim ngay_hachtoan As String = item(5).ToString()
                    Dim ten_nnop As String = item(6).ToString()
                    Dim ten_tthai_ctu As String = item(8).ToString()
                    Dim tong_tien As String = item(9).ToString()
                    Dim ref_no As String = item(10).ToString()
                    obj("STT") = dem
                    obj("id") = id
                    obj("so_ct") = so_ct
                    obj("mst") = mst
                    obj("so_gnt") = so_gnt
                    obj("ngay_chuyen") = ngay_chuyen
                    obj("ngay_hachtoan") = ngay_hachtoan
                    obj("ten_nnop") = ten_nnop
                    obj("ten_tthai_ctu") = ten_tthai_ctu
                    obj("tong_tien") = tong_tien
                    obj("ref_no") = ref_no
                    obj("his") = ReadHistory(so_ct)
                    lsObj.Add(obj)
                    dem += 1
                Next
            Else
                Return ""
            End If
            Return JsonConvert.SerializeObject(lsObj)
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Sub DM_TrangThai()
        Dim cnDiemThu As New DataAccess
        Dim dsTrangThai As DataSet
        Dim strSQL As String = ""
        strSQL = "SELECT a.TRANG_THAI, a.MOTA  FROM tcs_dm_trangthai_ntdt a where a.GHICHU='CT' and a.TRANG_THAI in ('11','13','15') order by mota"
        Try
            dsTrangThai = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            clsCommon.WriteLog(ex, "Có lỗi trong quá trình lấy thông tin trạng thái chứng từ", Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Sub

End Class
