﻿<%@ Page Language="VB" MasterPageFile="../shared/MasterPage02.master" AutoEventWireup="false" CodeFile="frmDKNNTDienTu.aspx.vb" Inherits="NTDT_frmDKNNTDienTu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .DKNNTDT input[type="text"]{width:200px}
        .container{width:450px; border:1px solid #000; padding:10px; margin-top:30px; margin-bottom:30px}
        .dataView{padding-bottom:10px}
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MstPg02_MainContent" runat="Server">
    <div class="container">
        <table cellpadding="0" cellspacing="10" border="0" class="DKNNTDT">
            <tr>
                <td align="left" style="width:200px">Mã số thuế</td>
                <td><asp:TextBox ID="txtMST" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:200px">Điện thoại</td>
                <td><asp:TextBox ID="txtDT" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:200px">Thư điện tử</td>
                <td><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:200px">Số serial chứng thư số</td>
                <td><asp:TextBox ID="txtSerial" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:200px">Tổ chức cấp chứng thư số</td>
                <td><asp:TextBox ID="txtTCCTS" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:200px">Ngân hàng</td>
                <td><asp:DropDownList ID="ddlNH" Width="200px" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td align="left" style="width:200px">
                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                            <td align="left">Số TK Ngân hàng</td>
                            <td align="right">1</td>
                        </tr>
                    </table>
                </td>
                <td><asp:TextBox ID="txtTKNH1" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="right" style="width:200px">&nbsp; 2</td>
                <td><asp:TextBox ID="txtTKNH2" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="right" style="width:200px">&nbsp; 3</td>
                <td><asp:TextBox ID="txtTKNH3" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="right" style="width:200px">&nbsp; 4</td>
                <td><asp:TextBox ID="txtTKNH4" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="right" style="width:200px">&nbsp; 5</td>
                <td><asp:TextBox ID="txtTKNH5" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:200px">&nbsp;</td>
                <td align="left"><asp:CheckBox ID="cbxNTDT" runat="server" Text="Nộp thuế điện tử" /></td>
            </tr>
            <tr>
                <td style="width:200px" align="right"><asp:Button ID="btnNhapLai" runat="server" Text="Nhập lại thông tin CKS" /></td>
                <td align="left">
                    &nbsp;&nbsp;&nbsp;<asp:Button ID="btnTiepTuc" runat="server" Text="Tiếp tục" />
                    &nbsp;&nbsp;<asp:Button ID="btnTimKiem" runat="server" Text="Tìm kiếm" />
                </td>
            </tr>
        </table>
    </div>
    <div class="dataView">
        <asp:DataGrid ID="dtgrd" runat="server" AutoGenerateColumns="False" TabIndex="9"
            Width="100%" BorderColor="#000" CssClass="grid_data" AllowPaging="True">
            <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
            <ItemStyle CssClass="grid_item" />
            <Columns>
                <asp:BoundColumn DataField="MST" HeaderText="ID" Visible="false">
                    <HeaderStyle Width="0"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"  Width="0"></ItemStyle>
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Mã số thuế" HeaderStyle-Width="20%">
                    <ItemTemplate>
                        <asp:HyperLink ID="Hyperlink1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MST") %>'
                            NavigateUrl='<%#"~/NTDT/frmDKNNTDienTu.aspx?ID=" & DataBinder.Eval(Container.DataItem, "MST") %>'>
                        </asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle Width="20%"></HeaderStyle>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="SDT_NNT" HeaderText="Điện thoại">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="EMAIL_NNT" HeaderText="Thư điện tử">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="SERIAL_CERT_NTHUE" HeaderText="Serial CTS">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ISSUER_CERT_NTHUE" HeaderText="Tổ chức cấp CTS">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="TEN_NHANG" HeaderText="Ngân hàng">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="STK_NHANG1" HeaderText="Số TK NH">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
            </Columns>
            <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                Font-Strikeout="False" Font-Underline="False" ForeColor="#000" VerticalAlign="Middle">
            </PagerStyle>
        </asp:DataGrid>
    </div>
</asp:Content>