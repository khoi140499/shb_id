﻿Imports Business.Common.mdlCommon
Imports Business.DanhMuc
Imports System.Data
Imports VBOracleLib
Imports System.IO
Imports ProcMegService
Imports System.Data.OleDb
Imports Business
Imports ETAX_GDT
Imports Business.HeThong
Imports Newtonsoft.Json.Linq

Partial Class NTDT_frmTraCuuXuLyTT_NTDT
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)

            Exit Sub
        End If

    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function Search(ByVal js As String) As String
        Dim objRes As New JObject()
        Dim strErrorCode As String = ""
        Dim strErroMess As String = ""
        Dim wclause As String = ""
        Try
            If js.Length > 2 Then
                Dim obj As JObject = JObject.Parse(js)
                Dim tuNgay As String = obj("tuNgay").ToString()
                Dim denNgay As String = obj("denNgay").ToString()
                Dim SoCT As String = obj("SoCT").ToString()
                Dim MST As String = obj("MST").ToString()
                Dim SctKore As String = obj("SctKore").ToString()
                Dim SoTC As String = obj("SoTC").ToString()
                Dim soien As String = obj("soien").ToString()
                Dim tinhtrang As String = obj("tinhtrang").ToString()
                wclause = WhereClause(tinhtrang, MST, SctKore, SoCT, SoTC, soien, tuNgay, denNgay)
            End If
            Dim ds As DataSet = Load1(wclause)
            If Not IsEmptyDataSet(ds) Then
                strErrorCode = "0"
                strErroMess = BuildHtmlData(ds)
            Else
                strErrorCode = "1"
                strErroMess = "Không có dữ liệu thỏa mãn điều kiện tìm kiếm"
            End If

        Catch ex As Exception
            strErrorCode = "-1"
            strErroMess = "Lỗi hệ thống " & ex.Message
        End Try
        objRes("errorCode") = strErrorCode
        objRes("errorMess") = strErroMess
        Return Newtonsoft.Json.JsonConvert.SerializeObject(objRes)
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Update(ByVal js As String) As String
        Dim objRes As New JObject()
        Dim strErrorCode As String = ""
        Dim strErroMess As String = ""
        Dim wclause As String = ""
        Dim strDem As Int16 = 0
        Try
            If js.Length > 0 Then
                For Each item As String In js.Split(",")
                    If UpdateCTU(item.Split("-")(0), item.Split("-")(1)) Then
                        strDem += 1
                    End If
                Next
                strErrorCode = "0"
                strErroMess = "Cập nhật thành công " & strDem & "/" & js.Split(",").Length & " chứng từ"
            Else
                strErrorCode = "1"
                strErroMess = "Vui lòng chọn tối thiểu 1 số chứng từ để cập nhật"
            End If


        Catch ex As Exception
            strErrorCode = "-1"
            strErroMess = "Lỗi hệ thống " & ex.Message
        End Try
        objRes("errorCode") = strErrorCode
        objRes("errorMess") = strErroMess
        Return Newtonsoft.Json.JsonConvert.SerializeObject(objRes)
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function Process(ByVal js As String) As String
        Dim objRes As New JObject()
        Dim strErrorCode As String = ""
        Dim strErroMess As String = ""
        Dim wclause As String = ""
        Dim strDem As Int16 = 0
        Try
            If js.Length > 0 Then
                For Each item As String In js.Split(",")
                    If ProcessCitad.Build06048(item.Split("-")(0)) Then
                        strDem += 1
                    End If
                Next
                strErrorCode = "0"
                strErroMess = "Xử lý thành công " & strDem & "/" & js.Split(",").Length & " chứng từ"
            Else
                strErrorCode = "1"
                strErroMess = "Vui lòng chọn tối thiểu 1 số chứng từ để xử lý"
            End If


        Catch ex As Exception
            strErrorCode = "-1"
            strErroMess = "Lỗi hệ thống " & ex.Message
        End Try
        objRes("errorCode") = strErrorCode
        objRes("errorMess") = strErroMess
        Return Newtonsoft.Json.JsonConvert.SerializeObject(objRes)
    End Function


    Private Shared Function UpdateCTU(ByVal soCt As String, ByVal trangThai As String) As Boolean
        Try
            Dim Sql As String = "update tcs_ctu_ntdt_hdr set trangthai_06048='" & trangThai & "' where so_ctu='" & soCt & "'"
            DataAccess.ExecuteNonQuery(Sql, CommandType.Text)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Shared Function WhereClause(ByVal tinhTrang As String, ByVal mst As String, ByVal soKore As String,
                                 ByVal soCT As String, ByVal soTC As String, ByVal soTien As String,
                                 ByVal tuNgay As String, ByVal denNgay As String) As String
        Dim sqlWhereClause As String = ""
        Try

            If tinhTrang <> "" Then
                sqlWhereClause += " AND a.TRANGTHAI_SENDKB = '" + tinhTrang + "'"
            End If

            If mst <> "" Then
                sqlWhereClause += " AND a.mst_nnop like '%" + mst + "%'"
            End If

            If soKore <> "" Then
                sqlWhereClause += " AND a.so_ct_nh like '%" + soKore + "%'"
            End If

            If soCT <> "" Then
                sqlWhereClause += " AND a.so_ctu like '%" + soCT + "%'"
            End If

            If soTC <> "" Then
                sqlWhereClause += " AND a.MA_THAMCHIEU like '%" + soTC + "%'"
            End If
            If soTien <> "" Then
                sqlWhereClause += " AND a.tong_tien like '%" + soTien + "%'"
            End If
            If tuNgay <> "" Then
                sqlWhereClause += " AND TO_CHAR(a.ngay_nhang,'yyyymmdd') >= " & ConvertDateToNumber(tuNgay)
            End If
            If denNgay <> "" Then
                sqlWhereClause += " AND TO_CHAR(a.ngay_nhang,'yyyymmdd') <= " & ConvertDateToNumber(denNgay)
            End If
        Catch ex As Exception
        End Try
        Return sqlWhereClause
    End Function

    Private Shared Function Load1(ByVal WhereClause As String) As Object
        Dim strSQL As String
        Dim TT_NTDT As DataAccess
        Dim dsTT_NTDT As DataSet

        Try
            strSQL = "SELECT a.so_ct_nh,a.so_ctu, a.tong_tien,a.tt_sp, a.MA_THAMCHIEU,a.mst_nnop,a.ten_nnop, to_char(a.ngay_nhang, 'DD/MM/YYYY hh24:mm:ss') " &
           "         as ngay_nhang ,a.tthai_ctu,a.trangthai_06048,(select c.mota from tcs_dm_trangthai c where c.trangthai=a.trangthai_06048 and c.loai='06048') ten_tthai06048 " &
           "   FROM TCS_CTU_NTDT_HDR a " &
           "   WHERE 1 = 1 and a.tthai_ctu ='11' " & WhereClause &
           "   ORDER BY so_ctu "

            TT_NTDT = New DataAccess
            dsTT_NTDT = TT_NTDT.ExecuteReturnDataSet(strSQL, CommandType.Text)
        Catch ex As Exception
            CTuCommon.WriteLog(ex, "Lỗi khi lấy thông tin khach hang uu dai phi")
            Throw ex
        Finally
            If Not TT_NTDT Is Nothing Then TT_NTDT.Dispose()
        End Try

        Return dsTT_NTDT
    End Function

    Private Shared Function BuildHtmlData(ByVal ds As DataSet) As String
        Dim str As String = ""
        Try
            For Each item As DataRow In ds.Tables(0).Rows
                str &= "<tr><td style='width: 3%'><input type='checkbox' onchange='fncEn(this)' name='vehicle1' value='Bike'></td><td style='width: 10%'><span> " & item("so_ct_nh") & "</span></td><td style='width: 10%'><span> " & item("so_ctu") & "</span></td><td style='width: 10%'><span> " & item("tong_tien") & "</span></td><td style='width: 10%'><span> " & item("MA_THAMCHIEU") & "</span></td> <td style='width: 12%'><span> " & item("mst_nnop") & "</span></td><td style='width: 13%'><span> " & item("ten_nnop") & "</span></td><td style='width: 12%'><span> " & item("ngay_nhang") & "</span></td><td style='width: 20%'> " & SetSelectDataStatus(item("trangthai_06048"), item("tt_sp"), item("so_ctu")) & "</td></tr>"
            Next
        Catch ex As Exception

        End Try
        Return str
    End Function


    Private Shared Function SetSelectDataStatus(ByVal trangThai06048 As String, ByVal tt_sp As String, ByVal soCT As String) As String
        Dim str As String = "<select id = 'sl" & soCT & "' disabled onchange='fncUpdateSelect(this)'>"
        If trangThai06048.Equals("00") And tt_sp = "0" Then
            str &= "<option value='00' selected>Chưa gửi UNT/KBNN</option><option value = '00' >Đã gửi thành công đến NH UNT</Option> <option value = '03' > Gửi lỗi đến NH UNT</Option></select>"
        ElseIf trangThai06048.Equals("00") And tt_sp <> "0" Then
            str &= "<option value='00' selected>Chưa gửi UNT/KBNN</option><option value = '02' > Đã gửi thành công đến KBNN</Option><option value = '04' > Gửi lỗi đến KBNN</Option></select>"
        ElseIf trangThai06048 = "01" Then
            str &= "<option value='01' selected>Đã gửi thành công đến NH UNT</option><option value = '03' > Gửi lỗi đến NH UNT</Option></select>"
        ElseIf trangThai06048 = "03" Then
            str &= "<option value='01' >Đã gửi thành công đến NH UNT</option><option value = '03' selected> Gửi lỗi đến NH UNT</Option></select>"
        ElseIf trangThai06048 = "02" Then
            str &= "<option value = '02' selected> Đã gửi thành công đến KBNN</Option><option value = '04' > Gửi lỗi đến KBNN</Option></select>"
        ElseIf trangThai06048 = "04" Then
            str &= "<option value = '02'> Đã gửi thành công đến KBNN</Option><option value = '04' selected> Gửi lỗi đến KBNN</Option></select>"
        End If
        Return str

    End Function

    Private Function CheckCitad(ByVal strSoCTU As String) As Boolean
        Try
            Dim str As String = "Select * from tcs_ctu_ntdt_hdr where so_ct_nh='" + strSoCTU + "' and tt_sp is null"
            Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(str, CommandType.Text)
            If Not ds Is Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function

End Class
