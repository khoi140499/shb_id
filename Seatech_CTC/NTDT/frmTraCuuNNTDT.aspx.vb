﻿Imports System.Data
Imports Business.NTDT.NNTDT
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao

Partial Class NTDT_frmTraCuuNNTDT
    Inherits System.Web.UI.Page
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Load_TrangThai()
            DM_DiemThu()
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            hdfMA_CN.Value = Session.Item("MA_CN_USER_LOGON").ToString
            btnExport.Enabled = False
        End If
    End Sub
    Private Sub load_dataGrid()
        Dim ds As DataSet
        Dim objNNTDT As New buNNTDT
        Dim WhereClause As String = ""
        Try
            If txtMST.Enabled Then
                If txtMST.Text <> "" Then
                    WhereClause += " AND upper(a.MST) like upper('%" & txtMST.Text.Trim & "%')"
                End If
                If txtTuNgay.Value <> "" Then
                    WhereClause += " AND a.NGAY_TAO >= " & ConvertDateToNumber(txtTuNgay.Value)
                End If
                If txtDenNgay.Value <> "" Then
                    WhereClause += " AND a.NGAY_TAO <= " & ConvertDateToNumber(txtDenNgay.Value)
                End If
                If txtTenNNT.Text <> "" Then
                    WhereClause += " AND upper(a.TEN_NNT) like upper('%" + txtTenNNT.Text + "%')"
                End If
                If drpTrangThai.SelectedIndex <> 0 Then
                    WhereClause += " AND a.TRANG_THAI = '" + drpTrangThai.SelectedValue + "'"
                End If
                If txtSDT.Text <> "" Then
                    WhereClause += " AND upper(a.SDT_NNT) like upper('%" + txtSDT.Text + "%')"
                End If
                If txtTKNH.Text <> "" Then
                    WhereClause += " AND upper(B.SO_TK_NH) like upper('%" + txtTKNH.Text + "%')"
                End If

                'sonmt: ma chi nhanh
                If txtMaCN.SelectedValue.Trim.Length > 0 Then
                    WhereClause += " AND a.ma_cn = '" & txtMaCN.SelectedValue & "' "
                End If


            End If
            ds = objNNTDT.GetDataSet(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgrd.DataSource = ds.Tables(0)
                dtgrd.DataBind()

                grdExport.DataSource = ds.Tables(0)
                grdExport.DataBind()

                btnExport.Enabled = True
            Else
                btnExport.Enabled = False
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub
    Private Sub Load_TrangThai()
        Try
            Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_NTDT WHERE GHICHU='NNT'"


            Dim dsDiemThu As DataSet

            dsDiemThu = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)

            drpTrangThai.DataSource = dsDiemThu
            drpTrangThai.DataTextField = "MOTA"
            drpTrangThai.DataValueField = "TRANG_THAI"
            drpTrangThai.DataBind()
            drpTrangThai.Items.Insert(0, "Tất cả")

            'Dim dr As IDataReader
            'dr = DataAccess.ExecuteDataReader(sql, CommandType.Text)
            'dr.Read()
            'drpTrangThai.DataSource = dr
            'drpTrangThai.DataTextField = "MOTA"
            'drpTrangThai.DataValueField = "TRANG_THAI"
            'drpTrangThai.DataBind()
            'dr.Close()
            'drpTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái NNTDT. Lỗi: " & ex.Message)
        End Try
        'Try
        '    Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_NTDT WHERE GHICHU='NNT'"
        '    Dim dr As OracleClient.OracleDataReader
        '    dr = DataAccess.ExecuteDataReader(sql, CommandType.Text)
        '    drpTrangThai.DataSource = dr
        '    drpTrangThai.DataTextField = "MOTA"
        '    drpTrangThai.DataValueField = "TRANG_THAI"
        '    drpTrangThai.DataBind()
        '    dr.Close()
        '    drpTrangThai.Items.Insert(0, "Tất cả")
        'Catch ex As Exception
        '    clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái NNTDT. Lỗi: " & ex.Message)
        'End Try
    End Sub


    Private Sub DM_DiemThu()

        Dim dsDiemThu As DataSet
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        If strcheckHSC = "1" Then
            'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
            ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
            strSQL = "SELECT a.id, a.id ||'-'||a.name nam FROM tcs_dm_chinhanh a  "

        Else

            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        End If
        Try
            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(txtMaCN, dsDiemThu, 1, 0, "Tất cả")
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Có lỗi trong quá trình lẫy thông tin chi nhánh", Session.Item("TEN_DN"))
            Throw ex
        Finally

        End Try

    End Sub

    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        load_dataGrid()
        dtgrd.CurrentPageIndex = 0
        dtgrd.DataBind()
    End Sub

    Protected Sub dtgrd_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgrd.PageIndexChanged
        load_dataGrid()
        dtgrd.CurrentPageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        grdExport.Visible = True
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=ExportNNTDT.xls")
        Response.Charset = "utf-8"
        Response.ContentType = "application/vnd.ms-excel"
        Using sw1 As New StringWriter()
            Dim hw1 As New HtmlTextWriter(sw1)
            grdExport.RenderControl(hw1)
            Response.Output.Write(sw1.ToString)
            Response.Flush()
            Response.End()
        End Using
        grdExport.Visible = False
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
End Class
