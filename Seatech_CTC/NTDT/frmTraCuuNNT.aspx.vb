﻿Imports System.Data
Imports Business.NTDT.NNTDT
Imports Business.Common.mdlCommon
Imports VBOracleLib

Partial Class NTDT_frmTraCuuNNT
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Load_TrangThai()
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            hdfMA_CN.Value = Session.Item("MA_CN_USER_LOGON").ToString
        End If
    End Sub
    Private Sub load_dataGrid()
        Dim ds As DataSet
        Dim objNNTDT As New buNNTDT
        Dim WhereClause As String = ""
        Try
            If txtMST.Text <> "" Then
                WhereClause += " AND upper(a.MST) like upper('%" & txtMST.Text.Trim & "%')"
            End If
            If txtTuNgay.Value <> "" Then
                WhereClause += " AND a.NGAY_TAO >= " & ConvertDateToNumber(txtTuNgay.Value)
            End If
            If txtDenNgay.Value <> "" Then
                WhereClause += " AND a.NGAY_TAO <= " & ConvertDateToNumber(txtDenNgay.Value)
            End If
            If txtTenNNT.Text <> "" Then
                WhereClause += " AND upper(a.TEN_NNT) like upper('%" + txtTenNNT.Text + "%')"
            End If
            If drpTrangThai.SelectedIndex <> 0 Then
                WhereClause += " AND a.TRANG_THAI = '" + drpTrangThai.SelectedValue + "'"
            End If
            If txtSDT.Text <> "" Then
                WhereClause += " AND upper(a.SDT_NNT) like upper('%" + txtSDT.Text + "')"
            End If
            If txtTKNH.Text <> "" Then
                WhereClause += " AND upper(b.SO_TK_NH) like upper('%" + txtTKNH.Text + "%')"
            End If
            'If hdfMA_CN.Value <> "" Then
            '    WhereClause += " AND ((a.MA_CN is null) or (a.MA_CN= '" + hdfMA_CN.Value + "')) "
            'Else
            '    WhereClause += " AND (a.MA_CN is null) "
            'End If
            ds = objNNTDT.GetDataSet(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgrd.DataSource = ds.Tables(0)
                dtgrd.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub
  
    Private Sub Load_TrangThai()
        Try
            Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_NTDT WHERE GHICHU='NNT'"

            Dim cnDiemThu As New DataAccess
            Dim dsDiemThu As DataSet

            dsDiemThu = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)

            drpTrangThai.DataSource = dsDiemThu
            drpTrangThai.DataTextField = "MOTA"
            drpTrangThai.DataValueField = "TRANG_THAI"
            drpTrangThai.DataBind()
            drpTrangThai.Items.Insert(0, "Tất cả")

            'Dim dr As IDataReader
            'dr = DataAccess.ExecuteDataReader(sql, CommandType.Text)
            'dr.Read()
            'drpTrangThai.DataSource = dr
            'drpTrangThai.DataTextField = "MOTA"
            'drpTrangThai.DataValueField = "TRANG_THAI"
            'drpTrangThai.DataBind()
            'dr.Close()
            'drpTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái NNTDT. Lỗi: " & ex.Message)
        End Try
        'Try
        '    Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_NTDT WHERE GHICHU='NNT'"
        '    Dim dr As OracleClient.OracleDataReader
        '    dr = DataAccess.ExecuteDataReader(sql, CommandType.Text)
        '    drpTrangThai.DataSource = dr
        '    drpTrangThai.DataTextField = "MOTA"
        '    drpTrangThai.DataValueField = "TRANG_THAI"
        '    drpTrangThai.DataBind()
        '    dr.Close()
        '    drpTrangThai.Items.Insert(0, "Tất cả")
        'Catch ex As Exception
        '    clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái NNTDT. Lỗi: " & ex.Message)
        'End Try
    End Sub

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        load_dataGrid()
        dtgrd.CurrentPageIndex = 0
        dtgrd.DataBind()
    End Sub

    Protected Sub dtgrd_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgrd.PageIndexChanged
        load_dataGrid()
        dtgrd.CurrentPageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub
End Class
