﻿Imports System.Data
Imports Business.NTDT.NNTDT
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports System.IO

Partial Class NTDT_frmAccountHistory
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            btnExport.Enabled = False
        End If
    End Sub
    Private Sub load_dataGrid()
        Dim ds As DataSet
        Dim objNNTDT As New buNNTDT
        Dim WhereClause As String = ""
        Try
            If txtMST.Text <> "" Then
                WhereClause += " AND upper(a.MST) like upper('%" & txtMST.Text.Trim & "%')"
            End If

            If txtTuNgay.Value <> "" Then
                'WhereClause += " AND TO_char(a.ngay_th,'dd/MM/rrrr') >= '" & (txtTuNgay.Value) & "'"
                'Nhamlt_20180506
                WhereClause += " AND TO_char(a.ngay_th,'RRRRMMDD')  >= " & ConvertDateToNumber(txtTuNgay.Value)
            End If
            If txtDenNgay.Value <> "" Then
                'WhereClause += " AND TO_char(a.ngay_th,'dd/MM/rrrr') <= '" & txtDenNgay.Value & "'"
                WhereClause += " AND TO_char(a.ngay_th,'RRRRMMDD')  <= " & ConvertDateToNumber(txtDenNgay.Value)
            End If

            If txtTKNH.Text <> "" Then
                WhereClause += " AND upper(b.SO_TK_NH) like upper('%" + txtTKNH.Text + "%')"
            End If

            ds = objNNTDT.GetAccountHistory(WhereClause)

            If Not IsEmptyDataSet(ds) Then
                dtgrd.DataSource = ds.Tables(0)
                dtgrd.DataBind()

                grdExport.DataSource = ds.Tables(0)
                grdExport.DataBind()
                btnExport.Enabled = True
            Else
                btnExport.Enabled = False
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        load_dataGrid()
        dtgrd.CurrentPageIndex = 0
        dtgrd.DataBind()

    End Sub

    Protected Sub dtgrd_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgrd.PageIndexChanged
        load_dataGrid()
        dtgrd.CurrentPageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        grdExport.Visible = True
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=ExportAccountHistory.xls")
        Response.Charset = "utf-8"
        Response.ContentType = "application/vnd.ms-excel"
        Using sw1 As New StringWriter()
            Dim hw1 As New HtmlTextWriter(sw1)
            grdExport.RenderControl(hw1)
            Response.Output.Write(sw1.ToString)
            Response.Flush()
            Response.End()
        End Using
        grdExport.Visible = False
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

End Class
