﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmChiTietThongTinNNTDT.aspx.vb" 
Inherits="NTDT_frmChiTietThongTinNNTDT" title="Chi tiết thông tin NTDT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   <style type="text/css">
        .txtHoanThien{width:80%;}
        .container{width:100%;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle"> Chi tiết đăng ký sử dụng dịch vụ NTDT </p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
            <tr>
                <td align="left" style="width:25%" class="form_label">Mã số thuế<span class="requiredField">(*)</span></td>
                <td class="form_control"><asp:TextBox ID="txtMST" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên NNT<span class="requiredField">(*)</span></td>
                <td class="form_control"><asp:TextBox ID="txtTenNNT" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Địa chỉ NNT</td>
                <td class="form_control"><asp:TextBox ID="txtDCNNT" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Mã cơ quan thu<span class="requiredField">(*)</span></td>
                <td class="form_control"><asp:TextBox ID="txtMaCQT" runat="server" CssClass="inputflat txtHoanThien" MaxLength="5" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Email</td>
                <td class="form_control"><asp:TextBox ID="txtEmail" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số điện thoại</td>
                <td class="form_control"><asp:TextBox ID="txtSDT" runat="server" CssClass="inputflat txtHoanThien" MaxLength="15" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên người liên hệ</td>
                <td class="form_control"><asp:TextBox ID="txtNguoiLH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số serial chứng thư số<span class="requiredField">(*)</span></td>
                <td class="form_control"><asp:TextBox ID="txtSoSerial" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="200"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Nhà cung cấp chứng thư số</td>
                <td class="form_control"><asp:TextBox ID="txtNhaCC" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Mã ngân hàng</td>
                <td class="form_control"><asp:TextBox ID="txtMaNH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="10" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên ngân hàng</td>
                <td class="form_control"><asp:TextBox ID="txtTenNH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Mã TVAN</td>
                <td class="form_control"><asp:TextBox ID="txtMaTVAN" runat="server" CssClass="inputflat txtHoanThien" MaxLength="6" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên TVAN</td>
                <td class="form_control"><asp:TextBox ID="txtTenTVAN" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Ngày gửi</td>
                <td class="form_control"><asp:TextBox ID="txtNgayGui" runat="server" CssClass="inputflat txtHoanThien" MaxLength="10" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên tài khoản</td>
                <td class="form_control"><asp:TextBox ID="txtTenTKNH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số tài khoản sử dụng NTDT <span class="requiredField">(*)</span></td>
                <td>
                    <asp:GridView ID="grdTKNH" runat="server"  Width="80%" DataKeyNames="ID" ShowHeader="false"
                        EmptyDataText="Chưa có" EmptyDataRowStyle-CssClass="form_label"   AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="ID" Visible="false"/>
                            <asp:TemplateField HeaderText="Tài khoản ngân hàng">
                                <ItemTemplate><asp:Label ID="lblSoTKNH" runat="server" Text='<%#Eval("SO_TK_NH") %>'></asp:Label></ItemTemplate>
                                <ItemStyle Width="80%" CssClass="form_label"/>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;
                <asp:HiddenField ID="hdfNNTDT_ID" runat="server" />
                <asp:HiddenField ID="hdrTRANG_THAI" runat="server" /></td>
            </tr>
         <%--   <tr>
                <td align="center" colspan="2">
                    <asp:Button ID="btnIn" runat="server" Text="In" Width="90px"/>
                </td>
            </tr>--%>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
        </table>
    </div>
</asp:Content>

