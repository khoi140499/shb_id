﻿Imports VBOracleLib
Imports System.Data
Imports Business.NTDT.NNTDT
Imports Business.Common.mdlCommon
Imports CorebankServiceESB
Imports Business.BaoCao
Imports Business.Common

Partial Class NTDT_frmHoanThienNNTDT
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            hdfMA_NV.Value = Session.Item("User").ToString
            Try
                If Not Request.Params("ID") Is Nothing Then
                    Dim NNTDT_ID As String = Request.Params("ID").ToString
                    Dim strErrMsg As String = ""
                    If NNTDT_ID <> "" Then
                        Dim obj As New infNNTDT
                        obj.NNTDT_ID = NNTDT_ID
                        If Get_Data_DB(obj, strErrMsg) Then
                            If Not Get_DataToForm(obj, strErrMsg) Then
                                clsCommon.ShowMessageBox(Me, strErrMsg)
                            End If
                        Else
                            clsCommon.ShowMessageBox(Me, strErrMsg)
                        End If
                        If hdfMA_CN.Value = "" Then
                            hdfMA_CN.Value = Session.Item("MA_CN_USER_LOGON").ToString
                        End If
                        Load_Grid_DS_TKNH()
                        Get_Max_So_TK_NH()
                        'nếu là tạo mới thì check xem mã số thuế đã tồn tại chưa
                        If hdrTRANG_THAI.Value = "00" Or hdrTRANG_THAI.Value = "" Then
                            If Check_MST(obj.MST) Then
                                btnChuyenKS.Enabled = False
                                btnDieuChinh.Enabled = False
                                btnHuy.Enabled = False
                                btnIn.Enabled = False
                                btnCheckTK.Enabled = False
                                clsCommon.ShowMessageBox(Me, "MST đang hoạt động trên hệ thống nên không cho phép tạo mới")
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Lỗi: " & ex.Message)
            End Try
        End If
    End Sub
    'Lấy dữ liệu từ DB theo NNTDT_ID
    Private Function Get_Data_DB(ByRef item As infNNTDT, ByRef strErrMsg As String) As Boolean
        Try
            Dim obj As New daNNTDT
            item = obj.Load(item.NNTDT_ID)
            Return True
        Catch ex As Exception
            strErrMsg = "Không lấy được thông tin người nộp thuế. Lỗi: " & ex.Message
            Return False
        End Try
    End Function
    'Lấy dữ liệu cho form
    Private Function Get_DataToForm(ByRef item As infNNTDT, ByRef strErrMsg As String) As Boolean
        Try
            txtMST.Text = item.MST
            txtTenNNT.Text = item.TEN_NNT
            txtDCNNT.Text = item.DIACHI_NNT
            txtMaCQT.Text = item.MA_CQT
            txtEmail.Text = item.EMAIL_NNT
            txtSDT.Text = item.SDT_NNT
            txtNguoiLH.Text = item.TEN_LHE_NTHUE
            txtSoSerial.Text = item.SERIAL_CERT_NTHUE
            txtNhaCC.Text = item.ISSUER_CERT_NTHUE
            txtMaNH.Text = item.MA_NHANG
            txtTenNH.Text = item.TEN_NHANG
            txtMaTVAN.Text = item.VAN_ID
            txtTenTVAN.Text = item.TEN_TVAN
            txtNgayGui.Text = item.NGAY_GUI
            'txtSoTKNH.Text = item.STK_NHANG
            txtTenTKNH.Text = item.TENTK_NHANG
            hdfNNTDT_ID.Value = item.NNTDT_ID
            hdrTRANG_THAI.Value = item.TRANG_THAI
            'hdfMA_CN.Value = item.MA_CN
            txtLyDo.Text = item.LDO_TCHOI
            'Enable các nút trên màn hình hoàn thiện theo trạng thái
            Valid_Button_Enable(item.TRANG_THAI)
            Return True
        Catch ex As Exception
            strErrMsg = "Lỗi không đưa được thông tin lên form. Lỗi: " & ex.Message
            Return False
        End Try
    End Function
    'Gán dữ liệu từ form vài obj
    Private Function SetDataToDB(ByRef item As infNNTDT, ByRef strErrMsg As String) As Boolean
        Try
            item.MST = txtMST.Text
            item.TEN_NNT = txtTenNNT.Text
            item.DIACHI_NNT = txtDCNNT.Text
            item.MA_CQT = txtMaCQT.Text
            item.EMAIL_NNT = txtEmail.Text
            item.SDT_NNT = txtSDT.Text
            item.TEN_LHE_NTHUE = txtNguoiLH.Text
            item.SERIAL_CERT_NTHUE = txtSoSerial.Text
            item.ISSUER_CERT_NTHUE = txtNhaCC.Text
            item.MA_NHANG = txtMaNH.Text
            item.TEN_NHANG = txtTenNH.Text
            item.VAN_ID = txtMaTVAN.Text
            item.TEN_TVAN = txtTenTVAN.Text
            item.NGAY_GUI = txtNgayGui.Text
            'item.STK_NHANG = txtSoTKNH.Text
            item.TENTK_NHANG = txtTenTKNH.Text
            item.NNTDT_ID = hdfNNTDT_ID.Value
            item.MA_CN = hdfMA_CN.Value
            item.NV_TT = hdfMA_NV.Value
            item.LDO_TCHOI = txtLyDo.Text.Trim
            'item.TRANG_THAI = hdrTRANG_THAI.Value
            Return True
        Catch ex As Exception
            strErrMsg = "Không lấy được dữ liệu từ form vào DB. Mô tả: " & ex.Message
            Return False
        End Try
    End Function
    'Lấy thông tin tài khoản ngân hàng của khách hàng
    Private Sub Load_Grid_DS_TKNH()
        Dim ds As New DataSet
        Try
            Dim sql = "Select * from ntdt_map_mst_tknh where NNTDT_ID=" & hdfNNTDT_ID.Value & " and MST='" & txtMST.Text.Trim & "'"
            ds = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            grdTKNH.DataSource = ds
            grdTKNH.DataBind()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách tài khoản ngân hàng của khách hàng. Lỗi: " & ex.Message)
        End Try
    End Sub

    Protected Sub btnChuyenKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChuyenKS.Click
        'Valid dữ liệu bắt buộc nhập
        If txtMST.Text = "" Then
            clsCommon.ShowMessageBox(Me, "Mã số thuế không được để trống")
            Exit Sub
        End If
        If txtTenNNT.Text = "" Then
            clsCommon.ShowMessageBox(Me, "Tên người nộp thuế không được để trống")
            Exit Sub
        End If
        If txtMaCQT.Text = "" Then
            clsCommon.ShowMessageBox(Me, "Mã cơ quan thu không được để trống")
            Exit Sub
        End If
        If txtSoSerial.Text = "" Then
            clsCommon.ShowMessageBox(Me, "Số serial chứng thư số không được để trống")
            Exit Sub
        End If
        If grdTKNH.Rows.Count = 0 Then
            clsCommon.ShowMessageBox(Me, "Bạn cần check và lưu lại số tài khoản ngân hàng")
            txtSoTKNH.Focus()
            Exit Sub
        End If
        Try
            Dim strErrMsg As String = ""
            Dim objCur As New infNNTDT
            objCur.NNTDT_ID = hdfNNTDT_ID.Value
            'Kiểm tra trạng thái của thông tin hiện tại có thay đổi gì không
            If Get_Data_DB(objCur, strErrMsg) Then
                If objCur.TRANG_THAI <> hdrTRANG_THAI.Value Then
                    clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát không thành công. Trạng thái bản ghi đã thay đổi")
                    Valid_Button_Enable(objCur.TRANG_THAI)
                    Exit Sub
                End If
            Else
                clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát không thành công." & strErrMsg)
                Exit Sub
            End If

            Dim objUpdate As New infNNTDT
            If Not SetDataToDB(objUpdate, strErrMsg) Then
                clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát không thành công. Lỗi: " & strErrMsg)
                Exit Sub
            End If
            'Trạng thái chờ kiểm soát = 01
            objUpdate.TRANG_THAI = "01"
            Dim bu As New buNNTDT
            If Not bu.Update_HoanThien(objUpdate) Then
                clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát không thành công")
            Else
                hdrTRANG_THAI.Value = "01"
                Valid_Button_Enable("01")
                Load_Grid_DS_TKNH()
                clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát thành công. Đã gửi thông tin tới KSV")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát không thành công. Lỗi:" & ex.Message)
        End Try
    End Sub

    Protected Sub btnHuy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHuy.Click
        Try
            If txtLyDo.Text.Trim = "" Then
                clsCommon.ShowMessageBox(Me, "Bạn cần nhập lý do hủy")
                txtLyDo.Focus()
                Exit Sub
            End If
            Dim strErrMsg As String = ""
            Dim objCur As New infNNTDT
            objCur.NNTDT_ID = hdfNNTDT_ID.Value
            'Kiểm tra trạng thái của thông tin hiện tại có thay đổi gì không
            If Get_Data_DB(objCur, strErrMsg) Then
                If objCur.TRANG_THAI <> hdrTRANG_THAI.Value Then
                    clsCommon.ShowMessageBox(Me, "Hủy không thành công. Thông tin này đã thay đổi")
                    Exit Sub
                End If
            Else
                clsCommon.ShowMessageBox(Me, "Hủy không thành công." & strErrMsg)
                Exit Sub
            End If

            Dim objUpdate As New infNNTDT
            objUpdate.NNTDT_ID = hdfNNTDT_ID.Value
            objUpdate.NV_TT = hdfMA_NV.Value
            objUpdate.LDO_TCHOI = txtLyDo.Text.Trim
            'Trạng thái hủy - chờ duyệt GDV = 02
            objUpdate.TRANG_THAI = "02"
            objUpdate.MA_CN = hdfMA_CN.Value
            Dim bu As New buNNTDT
            If Not bu.Update_TrangThai(objUpdate) Then
                clsCommon.ShowMessageBox(Me, "Hủy không thành công")
            Else
                hdrTRANG_THAI.Value = "02"
                Valid_Button_Enable("02")
                Load_Grid_DS_TKNH()
                clsCommon.ShowMessageBox(Me, "Hủy thành công. Đã gửi thông tin tới KSV")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Hủy không thành công. Lỗi:" & ex.Message)
        End Try
    End Sub

    Private Sub Valid_Button_Enable(ByVal strTrangThai As String)
        Select Case strTrangThai
            Case "00" 'đăng ký mới
                btnChuyenKS.Enabled = True
                btnDieuChinh.Enabled = False
                btnHuy.Enabled = True
                'btnIn.Enabled = False
                btnCheckTK.Enabled = True
            Case "01" 'chờ kiểm soát
                btnChuyenKS.Enabled = False
                btnDieuChinh.Enabled = False
                btnHuy.Enabled = False
                'btnIn.Enabled = True
                btnCheckTK.Enabled = False
            Case "02" 'hủy - chờ duyệt
                btnChuyenKS.Enabled = False
                btnDieuChinh.Enabled = False
                btnHuy.Enabled = False
                'btnIn.Enabled = False
                btnCheckTK.Enabled = False
            Case "03" 'thành công
                btnChuyenKS.Enabled = False
                btnDieuChinh.Enabled = True
                btnHuy.Enabled = False
                'btnIn.Enabled = True
                btnCheckTK.Enabled = False
            Case "04" 'hủy
                btnChuyenKS.Enabled = False
                btnDieuChinh.Enabled = False
                btnHuy.Enabled = False
                'btnIn.Enabled = False
                btnCheckTK.Enabled = False
            Case "05" 'điều chỉnh
                btnChuyenKS.Enabled = True
                btnDieuChinh.Enabled = False
                btnHuy.Enabled = False
                'btnIn.Enabled = False
                btnCheckTK.Enabled = True
            Case "06" 'ngừng
                btnChuyenKS.Enabled = False
                btnDieuChinh.Enabled = False
                btnHuy.Enabled = False
                'btnIn.Enabled = False
                btnCheckTK.Enabled = False
            Case "07" 'chuyển trả
                btnChuyenKS.Enabled = True
                btnDieuChinh.Enabled = False
                btnHuy.Enabled = True
                'btnIn.Enabled = False
                btnCheckTK.Enabled = True
            Case "08" 'chuyen thanh cong nhung chua nhan duoc phan hoi 999
                btnChuyenKS.Enabled = False
                btnDieuChinh.Enabled = False
                btnHuy.Enabled = False
                'btnIn.Enabled = False
                btnCheckTK.Enabled = False
            Case "09" 'huy thanh cong nhung chua nhan duoc phan hoi 999
                btnChuyenKS.Enabled = False
                btnDieuChinh.Enabled = False
                btnHuy.Enabled = False
                'btnIn.Enabled = False
                btnCheckTK.Enabled = False
        End Select
    End Sub

    Protected Sub btnDieuChinh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDieuChinh.Click
        Try
            Dim strErrMsg As String = ""
            Dim objCur As New infNNTDT
            objCur.NNTDT_ID = hdfNNTDT_ID.Value
            'Kiểm tra trạng thái của thông tin hiện tại có thay đổi gì không
            If Get_Data_DB(objCur, strErrMsg) Then
                If objCur.TRANG_THAI <> hdrTRANG_THAI.Value Then
                    clsCommon.ShowMessageBox(Me, "Thay đổi thông tin không thành công. Dữ liệu này đã thay đổi")
                    Exit Sub
                End If
            Else
                clsCommon.ShowMessageBox(Me, "Điều chỉnh không thành công." & strErrMsg)
                Exit Sub
            End If

            Dim objUpdate As New infNNTDT
            'Trạng thái điều chỉnh = 05
            objUpdate.TRANG_THAI = "05"
            objUpdate.NNTDT_ID = hdfNNTDT_ID.Value
            objUpdate.NV_TT = hdfMA_NV.Value
            Dim bu As New buNNTDT
            If Not bu.Update_TrangThai(objUpdate) Then
                clsCommon.ShowMessageBox(Me, "Điều chỉnh không thành công")
            Else
                hdrTRANG_THAI.Value = "05"
                Valid_Button_Enable("05")
                Load_Grid_DS_TKNH()
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Điều chỉnh không thành công. Lỗi:" & ex.Message)
        End Try
    End Sub

    Protected Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Response.Redirect("~/NTDT/frmTraCuuNNT.aspx")
    End Sub

    Protected Sub grdTKNH_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles grdTKNH.RowCancelingEdit
        grdTKNH.EditIndex = -1
        Load_Grid_DS_TKNH()
    End Sub

    Protected Sub grdTKNH_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdTKNH.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim stor_id As String = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ID"))
            Dim _so_tk_nh As String = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SO_TK_NH"))
            Dim lnkbtnresult As Button = DirectCast(e.Row.FindControl("ButtonDelete"), Button)
            If lnkbtnresult IsNot Nothing Then
                lnkbtnresult.Attributes.Add("onclick", (Convert.ToString("javascript:return deleteConfirm('" & _so_tk_nh.Trim & "')")))
            End If
        End If
        If hdrTRANG_THAI.Value = "00" Or hdrTRANG_THAI.Value = "05" Or hdrTRANG_THAI.Value = "07" Then
            e.Row.Cells(2).Visible = True
        Else
            e.Row.Cells(2).Visible = False
        End If
    End Sub

    Protected Sub grdTKNH_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdTKNH.RowDeleting
        Try
            Dim _id As String = grdTKNH.DataKeys(e.RowIndex).Values("ID").ToString()
            Dim sqlupdate As String = "Delete ntdt_map_mst_tknh where ID=" & _id
            DataAccess.ExecuteNonQuery(sqlupdate, CommandType.Text)
            'grdTKNH.EditIndex = -1
            Load_Grid_DS_TKNH()
            txtTenTKNH.Text = ""
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không xóa được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub

    Protected Sub grdTKNH_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles grdTKNH.RowEditing
        grdTKNH.EditIndex = e.NewEditIndex
        Load_Grid_DS_TKNH()
    End Sub

    Protected Sub grdTKNH_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles grdTKNH.RowUpdating
        Try
            Dim _id As String = grdTKNH.DataKeys(e.RowIndex).Values("ID").ToString()
            Dim _so_tk_nh As TextBox = DirectCast(grdTKNH.Rows(e.RowIndex).FindControl("txtSoTKNH_Edit"), TextBox)
            Dim sqlupdate As String = "Update ntdt_map_mst_tknh set SO_TK_NH ='" & _so_tk_nh.Text.Trim & "' where ID=" & _id
            DataAccess.ExecuteNonQuery(sqlupdate, CommandType.Text)
            grdTKNH.EditIndex = -1
            Load_Grid_DS_TKNH()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không sửa được dữ liệu. Lỗi: " & ex.Message)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If txtSoTKNH.Text.Trim = "" Then
                clsCommon.ShowMessageBox(Me, "Hãy nhập số tài khoản ngân hàng")
                Exit Sub
            End If
            If txtTenTKNH.Text.Trim = "" Then
                clsCommon.ShowMessageBox(Me, "Số tài khoản ngân hàng không hợp lệ")
                Exit Sub
            End If
            'Nếu giới hạn số tk lớn hơn ds số tk_nh hiện có thì 
            If hdrMax_SoTKNH.Value <= grdTKNH.Rows.Count Then
                clsCommon.ShowMessageBox(Me, "Mỗi người chỉ cho phép tối đa số tài khoản ngân hàng đăng ký là: " & hdrMax_SoTKNH.Value)
                Exit Sub
            End If
            Dim sql_check As String = "Select * from ntdt_map_mst_tknh a, tcs_dm_nntdt b where a.mst=b.mst and a.nntdt_id=b.nntdt_id and b.trang_thai not in ('04','06') and a.MST='" & txtMST.Text.Trim & "' and a.so_tk_nh='" & txtSoTKNH.Text.Trim & "'"
            Dim dt As DataTable = DataAccess.ExecuteToTable(sql_check)
            If dt.Rows.Count > 0 Then
                clsCommon.ShowMessageBox(Me, "Tài khoản ngân hàng này đã tồn tại")
                Exit Sub
            End If
            Dim sql As String = "Insert into ntdt_map_mst_tknh(nntdt_id,MST,SO_TK_NH,ID) values('" & hdfNNTDT_ID.Value & "','" & txtMST.Text.Trim & "','" & txtSoTKNH.Text.Trim & "',NTDT_ID_MAP_TK_MST.nextval)"
            DataAccess.ExecuteNonQuery(sql, CommandType.Text)
            Load_Grid_DS_TKNH()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không thêm được thông tin tài khoản khách hàng. Lỗi: " & ex.Message)
        End Try
    End Sub

    Protected Sub btnCheckTK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckTK.Click
        If txtSoTKNH.Text = "" Then
            txtSoTKNH.Focus()
            clsCommon.ShowMessageBox(Me, "Số tài khoản ngân hàng không được để trống")
            Exit Sub
        End If
        Try
            Dim ten_tk As String
            Dim core As New CorebankServiceESB.clsCoreBank
            ten_tk = core.GetTen_TK_KH_NH(txtSoTKNH.Text.Trim)
            txtTenTKNH.Text = ten_tk
            If ten_tk <> "" Then
                btnAdd.Enabled = True
            Else
                btnAdd.Enabled = False
            End If
        Catch ex As Exception
            btnAdd.Enabled = False
            clsCommon.ShowMessageBox(Me, "Không lấy được tên khách hàng. Lỗi: " & ex.Message)
        End Try
    End Sub
    Private Sub Get_Max_So_TK_NH()
        Dim sql = "Select GIATRI_TS from TCS_THAMSO_HT where TEN_TS='MAX_SO_TK_NH'"
        Dim dt As DataTable = DataAccess.ExecuteToTable(sql)
        If dt.Rows.Count > 0 Then
            hdrMax_SoTKNH.Value = dt.Rows(0)("GIATRI_TS")
        Else
            hdrMax_SoTKNH.Value = "1"
        End If
    End Sub
    Private Function Check_MST(ByVal MST As String) As Boolean
        Dim sql = "Select * from tcs_dm_nntdt where MST='" & MST & "' and trang_thai in ('02','03') "
        Dim dt As DataTable = DataAccess.ExecuteToTable(sql)
        If dt.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Sub btnIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIn.Click
        Dim sql As String = "SELECT A.MST,A.TEN_NNT TEN_KH,A.DIACHI_NNT DIA_CHI,A.EMAIL_NNT EMAIL,A.SDT_NNT SDT,B.NAME TEN_CN,C.so_tk_nh,A.UPDATE_TT  " & _
                           " FROM TCS_DM_NNTDT A, tcs_dm_chinhanh B, ntdt_map_mst_tknh C " & _
                           " WHERE A.MA_CN = B.branch_id(+) AND A.MST = C.MST(+) AND A.nntdt_id = C.NNTDT_ID(+) AND A.NNTDT_ID=" & hdfNNTDT_ID.Value
        Try
            Dim ds = New DataSet
            Dim frm = New infBaoCao
            ds = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If
            frm.TCS_DS = ds
            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../BaoCao/frmShowReports.aspx?BC=" & Report_Type.BC_NTDT, "ManPower")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình Lập bảng kê chứng từ!")
        Finally
            btnIn.Enabled = True
        End Try
    End Sub
End Class
