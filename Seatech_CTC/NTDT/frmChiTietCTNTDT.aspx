﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage06.master" AutoEventWireup="false"
    CodeFile="frmChiTietCTNTDT.aspx.vb" Inherits="NTDT_frmChiTietCT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<script src="../../jsdropdowlist/jquery.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/popcalendar.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/CheckDate.js" type="text/javascript"></script>

    <script language="javascript" src="../../javascript/popup.js" type="text/javascript"></script>
    <script language="javascript" src="../../images/javascript/jquery/script/default/jquery-1.4.3.min.js" type="text/javascript"></script>
    <script language="javascript" src="../../javascript/jquery/jquery-1.3.2.js" type="text/javascript"></script>
    <script src="../../javascript/jquery/script/default/jquery.validationEngine.js" type="text/javascript"></script>
    <script src="../../javascript/jquery/script/default/jquery.validationEngine-en.js" type="text/javascript"></script>
    <link href="../../javascript/jquery/css/validation/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <script src="../../javascript/jquery/script/default/jquery.validationengine-vn.js" type="text/javascript"></script>
    <script src="../../javascript/google/js/jquery-1.8.0.min.js" type="text/javascript"></script>
    <script src="../../javascript/google/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
    <link href="../../javascript/google/css/ui-lightness/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />
    <%--    <link type="text/css" href="../../css/tabStyle.css" rel="stylesheet" />--%>

    <script type="text/javascript" language="javascript">
        function jsIn_CT(isBS) {

            var width = screen.availWidth - 100;
            var height = screen.availHeight - 10;
            var left = 0;
            var top = 0;
            var params = 'width=' + width + ', height=' + height;
            params += ', top=' + top + ', left=' + left;
            params += ', directories=no';
            params += ', location=no';
            params += ', menubar=yes';
            params += ', resizable=no';
            params += ', scrollbars=yes';
            params += ', status=no';
            params += ', toolbar=no';
            var strSHKB = $get('<%=hdnSHKB.clientID%>').value;
	 	var strSoCT = $get('<%=hdnSO_CTU.clientID%>').value;
        var strSo_BT = $get('<%=hdnSO_BT.clientID%>').value;
        var MaNV = $get('<%=hdnMA_NV.clientID%>').value;
        var ngayLV = $get('<%=hdnNGAYLV.clientID%>').value;
        window.showModalDialog("../../pages/Baocao/frmInGNT.aspx?SoCT=" + strSoCT + "&SHKB=" + strSHKB + "&SoBT=" + strSo_BT + "&NgayKB=" + ngayLV + "&MaNV=" + MaNV + "&BS=" + isBS);

    }

    function pageLoad() {
        //window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    }
    function EndRequestHandler() {
        var v_inCTU = $get('<%=hdfInCTU.clientID%>').value;

        if (v_inCTU == "1") {
            $get('<%=hdfInCTU.clientID%>').value = "0";

            var v_soCT, v_shKB, v_soBT, v_ngayKB, v_maNV;
            v_soCT = $get('<%=hdnSO_CTU.clientID%>').value;
            v_shKB = $get('<%=hdnSHKB.clientID%>').value;
            v_soBT = $get('<%=hdnSO_BT.clientID%>').value;
            v_ngayKB = $get('<%=hdnNGAYLV.clientID%>').value;
            v_maNV = $get('<%=hdnMA_NV.clientID%>').value;
            window.open("../../pages/Baocao/frmInGNT.aspx?SoCT=" + v_soCT + "&SHKB=" + v_shKB + "&SoBT=" + v_soBT + "&NgayKB=" + v_ngayKB + "&MaNV=" + v_maNV + "&BS=1");
        }
    }

    </script>

    <style type="text/css">
        .style1
        {
            height: 24px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg06_MainContent" runat="Server">
    <div id="popupDiv" title="Thông tin chi tiết" style="">
        <table border='0' cellpadding='0' cellspacing='0' width='100%'>
            <tr>
                <td colspan="2" width='100%' align="left">
                    <table width="100%">
                        <tr class="grid_header">
                            <td height="15px" align="right">
                                Trường (*) là bắt buộc nhập
                            </td>
                        </tr>
                    </table>
                    <div id="Panel1" style="height: 400px; width: 100%; overflow-y: scroll;">
                        <table runat="server" id="grdHeader" cellspacing="0" cellpadding="1" rules="all"
                            border="1" style="border-width: 1px; border-style: solid; font-family: Verdana;
                            font-size: 8pt; width: 97%; border-collapse: collapse;">
                            <tr id="rowMaNNT">
                                <td width="24%">
                                    <b>Mã số thuế/Tên </b><span class="requiredField">(*)</span>
                                </td>
                                <td width="25%">
                                    <asp:TextBox runat="server" ID="txtMaNNT" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                        Width="98%" />
                                </td>
                                <td width="51%">
                                    <asp:TextBox runat="server" ID="txtTenNNT" CssClass="inputflat" ReadOnly="true" Width="99%" />
                                </td>
                            </tr>
                            <tr id="rowDChiNNT">
                                <td>
                                    <b>Địa chỉ người nộp thuế</b>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox runat="server" ID="txtDChiNNT" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowQuanNNThue">
                                <td>
                                    <b>Quận(Huyện)</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtQuanNNThue" CssClass="inputflat" ReadOnly="true"
                                        Width="99%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTenQuanNNThue" CssClass="inputflat" ReadOnly="true"
                                        Width="99%" />
                                </td>
                            </tr>
                            <tr id="rowTinhNNThue">
                                <td>
                                    <b>Thành phố(Tỉnh)</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTinhNNThue" CssClass="inputflat" ReadOnly="true"
                                        Width="99%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTenTinhNNThue" CssClass="inputflat" ReadOnly="true"
                                        Width="99%" />
                                </td>
                            </tr>
                            <tr id="rowSHKB">
                                <td width="24%">
                                    <b>Số hiệu KB </b><span class="requiredField">(*)</span>
                                </td>
                                <td width="25%">
                                    <asp:TextBox runat="server" ID="txtSHKB" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                        Width="98%" />
                                </td>
                                <td width="51%">
                                    <asp:TextBox runat="server" ID="txtTenKB" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowKHCT">
                                <td width="24%">
                                    <b>Mã hiệu CT/Số CT </b><span class="requiredField">(*)</span>
                                </td>
                                <td width="25%">
                                    <asp:TextBox runat="server" ID="txtKHCT" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                        Width="98%" />
                                </td>
                                <td width="51%">
                                    <asp:TextBox runat="server" ID="txtSoCT" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowGNT_REFCORE">
                                <td width="24%">
                                    <b>Số GNT/ số Refcore: </b>
                                </td>
                                <td width="25%">
                                    <asp:TextBox runat="server" ID="txtGNT" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                        Width="98%" />
                                </td>
                                <td width="51%">
                                    <asp:TextBox runat="server" ID="txtREFCORE" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowQuan_HuyenNNTien">
                                <td>
                                    <b>Quận(huyện)</b>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox runat="server" ID="TXT1" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowTinhNNTien" style="display: none">
                                <td>
                                    <b>Thành phố(Tỉnh)</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTinh_TphoNNTien" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtQuan_HuyenNNTien" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowDBHC">
                                <td>
                                    <b>Mã tỉnh/thành Kho bạc </b><span class="requiredField">(*)</span>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtMaDBHC" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTenDBHC" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowCQThu">
                                <td>
                                    <b>CQ Thu </b><span class="requiredField">(*)</span>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtMaCQThu" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTenCQThu" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowTKNo" style="display: none">
                                <td style="background-color: Aqua">
                                    <b>TK Nợ NSNN </b><span class="requiredField">(*)</span>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTKNo" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTenTKNo" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowTKCo">
                                <td>
                                    <b>TK Có NSNN </b><span class="requiredField">(*)</span>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTenTKCo" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTKCo" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowHTTT">
                                <td>
                                    <b>Hình thức thanh toán</b>
                                </td>
                                <td>
                                    <%--<asp:DropDownList runat="server" ID="ddlHTTT" CssClass="inputflat" Enabled="false"
                                        Width="100%">
                                        <asp:ListItem Value="00" Text="Nộp tiền mặt" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="01" Text="Chuyển khoản"></asp:ListItem>
                                        <asp:ListItem Value="02" Text="Điện chuyển tiền"></asp:ListItem>
                                        <asp:ListItem Value="03" Text="Chuyển khoản nội bộ"></asp:ListItem>
                                    </asp:DropDownList>--%>
                                    <asp:TextBox runat="server" ID="txtHTTT" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTenHTTT" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowTK_KH_NH">
                                <td>
                                    <b>TK Chuyển </b><span class="requiredField">(*)</span>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTK_KH_NH" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTenTK_KH_NH" CssClass="inputflat" ReadOnly="true" Width="90%" />
                                </td>
                            </tr>
                            <tr id="rowSoDuNH">
                                <td>
                                    <b>Số dư NH</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtSoDu_KH_NH" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtDesc_SoDu_KH_NH" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowTKGL">
                                <td>
                                    <b>TK Chuyển </b><span class="requiredField">(*)</span>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTKGL" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTenTKGL" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowNNTien">
                                <td>
                                    <b>MST người nộp tiền/Tên</b>
                                </td>
                                <td >
                                    <asp:TextBox runat="server" ID="txtMaNNTien" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTenNNTien" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowDChiNNTien">
                                <td>
                                    <b>Địa chỉ người nộp tiền</b>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox runat="server" ID="txtDChiNNTien" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowQuanNNTien" style="display:none;">
                                <td>
                                    <b>Quận(Huyện)</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtQuanNNTien" CssClass="inputflat" ReadOnly="true"
                                        Width="99%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTenQuanNNTien" CssClass="inputflat" ReadOnly="true"
                                        Width="99%" />
                                </td>
                            </tr>
                            <tr id="rowTPNNTien"  style="display:none;">
                                <td>
                                    <b>Thành phố(Tỉnh)</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTinhNNTien" CssClass="inputflat" ReadOnly="true"
                                        Width="99%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTenTinhNNTien" CssClass="inputflat" ReadOnly="true"
                                        Width="99%" />
                                </td>
                            </tr>
                            <tr id="rowSoCMND">
                                <td>
                                    <b>Số chứng minh</b><span class="requiredField">(*)</span>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox runat="server" ID="txtCK_SoCMND" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowSoDT">
                                <td>
                                    <b>Số điện thoại</b>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox runat="server" ID="txtCK_SoDT" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowNgay_KH_NH">
                                <td>
                                    <b>Ngày lập CT</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtNGAY_KH_NH" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtDesNGAY_KH_NH" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowNganHangChuyen">
                                <td>
                                    <b>Mã NH Chuyển </b><span class="requiredField">(*)</span>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtMA_NH_A" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTen_NH_A" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowNganHangTT">
                                <td>
                                    <b>Mã NH trực tiếp </b><span class="requiredField">(*)</span>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtMA_NH_TT" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTEN_NH_TT" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowNganHangNhan">
                                <td>
                                    <b>Mã NH thụ hưởng </b><span class="requiredField">(*)</span>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtMA_NH_B" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTen_NH_B" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowMaNT">
                                <td>
                                    <b>Nguyên tệ</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtMaNT" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtTenNT" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowLoaiThue">
                                <td>
                                    <b>Loại thuế</b>
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlLoaiThue" CssClass="inputflat" Enabled="false"
                                        Width="98%">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtDescLoaiThue" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowToKhaiSo">
                                <td>
                                    <b>Tờ khai số</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtToKhaiSo" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDescToKhaiSo" runat="server" Width="98%" CssClass="inputflat">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr id="rowNgayDK">
                                <td>
                                    <b>Ngày đăng ký</b>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox runat="server" ID="txtNgayDK" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowLHXNK">
                                <td>
                                    <b>Loại hình XNK</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtLHXNK" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtDescLHXNK" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowSoKhung">
                                <td>
                                    <b>Số khung</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtSoKhung" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtDescSoKhung" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowSoMay">
                                <td>
                                    <b>Số máy</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtSoMay" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtDescSoMay" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <tr id="rowSO_QD">
                                <td>
                                    <b>Số quyết định</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtSO_QD" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="TextBox1" CssClass="inputflat" ReadOnly="true" Width="98%"
                                        Visible="true" />
                                </td>
                            </tr>
                            <tr id="rowCQ_QD">
                                <td>
                                    <b>Cơ quan quyết định</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtCQ_QD" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="TextBox2" CssClass="inputflat" ReadOnly="true" Width="98%"
                                        Visible="true" />
                                </td>
                            </tr>
                            <tr id="rowNgay_QD">
                                <td>
                                    <b>Ngày quyết định</b>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtNGAY_QD" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="TextBox3" CssClass="inputflat" ReadOnly="true" Width="98%"
                                        Visible="true" />
                                </td>
                            </tr>
                             <tr id="rowDienGiaiHQ">
                                <td>
                                    <b>Diễn giải sai lệch</b>
                                </td>
                                <td colspan="2">
                                    <asp:TextBox runat="server" ID="txtDienGiaiHQ" CssClass="inputflat" ReadOnly="true"
                                        Width="98%" />
                                </td>
                            </tr>
                            <%--<tr id="rowSoBK">
                                                                                    <td>
                                                                                        <b>Số bảng kê</b>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox runat="server" ID="txtSo_BK" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="rowNgayBK">
                                                                                    <td>
                                                                                        <b>Ngày bảng kê</b>
                                                                                    </td>
                                                                                    <td colspan="2">
                                                                                        <asp:TextBox runat="server" ID="txtNgay_BK" CssClass="inputflat" ReadOnly="true"
                                                                                            Width="98%" />
                                                                                    </td>
                                                                                </tr>--%>
                        </table>
                    </div>
                </td>
            </tr>
            <tr style="visibility: hidden">
                <td width="25%" style="height: 0px">
                    <asp:TextBox runat="server" ID="txtSo_BT" CssClass="inputflat" ReadOnly="true" Visible="false"
                        Width="99%" />
                    <asp:TextBox runat="server" ID="txtSoCT_cu" CssClass="inputflat" ReadOnly="true"
                        Visible="false" Width="99%" />
                </td>
                <td width="51%" style="height: 0px">
                    <asp:TextBox runat="server" ID="txtTRANG_THAI" CssClass="inputflat" ReadOnly="true"
                        Visible="false" Width="99%" />
                    <asp:TextBox runat="server" ID="txtMA_NV" CssClass="inputflat" ReadOnly="true" Visible="false"
                        Width="99%" />
                </td>
            </tr>
            <tr>
                <td colspan='2' align="left" class="errorMessage" width='100%'>
                    <table width='100%'>
                        <tr>
                            <td width='100%' align="right">
                                <asp:Label ID="lbSoFT" Text="" runat="server" Style="font-weight: bold;"></asp:Label>
                                <asp:Label ID="lbSoCT" runat="server" Style="font-weight: bold;" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width='100%'>
                                <asp:UpdateProgress ID="updProgress" runat="server">
                                    <ProgressTemplate>
                                        <div style="background-color: Aqua;">
                                            <b style="font-size: 15pt">Đang lấy dữ liệu tại server.Xin chờ một lát...</b>
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <b>
                                    <asp:Literal ID="lblStatus" runat="server"></asp:Literal></b>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" width="100%">
                    <asp:DataGrid ID="grdChiTiet" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                        ShowHeader="true" Width="100%" CssClass="grid_data">
                        <HeaderStyle CssClass="grid_header"></HeaderStyle>
                        <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                        <ItemStyle CssClass="grid_item" />
                        <Columns>
                            <asp:TemplateColumn Visible="false">
                                <HeaderStyle HorizontalAlign="Center" Width="30px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkRemove" runat="server" Enabled="false" Checked="true" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Mã quỹ" Visible="false">
                                <HeaderStyle HorizontalAlign="Center" Width="0px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="MA_CAP" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                        ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MAQUY") %>'
                                        Font-Bold="true" MaxLength="2" ToolTip="Ấn phím Enter để chọn mã cấp"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                            <%--tuanda
                            thêm cột mã ID khoản nộp và mã tham chiếu khoản nộp--%>
                            <asp:TemplateColumn HeaderText="ID khoản nộp">
                                <HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="ID_KNOP" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                        Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "ID_KNOP") %>'
                                        Font-Bold="true" MaxLength="30" ToolTip="Ấn phím Enter để chọn mã chương" onkeyup="return valInteger(this)"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <%--tuanda--%>
                           <%-- <asp:TemplateColumn HeaderText="Mã tham chiếu">
                                <HeaderStyle HorizontalAlign="Center" Width="150px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="MA_THAMCHIEU_KNOP" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                        Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_THAMCHIEU") %>'
                                        Font-Bold="true" MaxLength="20" ToolTip="Ấn phím Enter để chọn mã chương" onkeyup="return valInteger(this)"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>--%>
                            <asp:TemplateColumn HeaderText="Chương">
                                <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="MA_CHUONG" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                        Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_CHUONG") %>'
                                        Font-Bold="true" MaxLength="3" ToolTip="Ấn phím Enter để chọn mã chương" onkeyup="return valInteger(this)"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Mã ngành" Visible="false">
                                <HeaderStyle HorizontalAlign="Center" Width="0px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="MA_KHOAN" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                        ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_KHOAN") %>'
                                        Font-Bold="true" MaxLength="3" ToolTip="Ấn phím Enter để chọn mã khoản" onkeyup="return valInteger(this)"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NDKT">
                                <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="MA_MUC" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                        Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_TMUC") %>'
                                        Font-Bold="true" MaxLength="4" ToolTip="Ấn phím Enter để chọn mã tiểu mục" onkeyup="return valInteger(this)"
                                        AutoPostBack="true"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Mã DBHC">
                                <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="MA_DBHC_THU" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                        Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_DBHC_THU") %>'
                                        Font-Bold="true" MaxLength="4" ToolTip="Ấn phím Enter để chọn mã tiểu mục" onkeyup="return valInteger(this)"
                                        AutoPostBack="true"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Nội dung">
                                <HeaderStyle HorizontalAlign="Center" Width="230px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="NOI_DUNG" runat="server" Width="98%" BorderColor="white" CssClass="inputflat"
                                        ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "NOI_DUNG") %>'
                                        MaxLength="200" Font-Bold="true"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Số tiền thuế">
                                <HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="SOTIEN" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                        Style="text-align: right" Text='<%# DataBinder.Eval(Container.DataItem, "SODATHUTK") %>'
                                        ReadOnly="true" Font-Bold="true"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Số đã thu">
                                <HeaderStyle HorizontalAlign="Center" Width="80px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="SODATHU" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                        Style="text-align: right" ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "TTIEN") %>'
                                        Font-Bold="true">
                                    </asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="Kỳ thuế">
                                <HeaderStyle HorizontalAlign="Center" Width="70px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="KY_THUE" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                        ReadOnly="true" Style="text-align: center" Text='<%# DataBinder.Eval(Container.DataItem, "KY_THUE") %>'
                                        Font-Bold="true"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td colspan='2' height='5'>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <table width="500px">
                        <tr align="right">
                            <td>
                                <asp:HiddenField id="hdnSO_CTU" runat="server"/>
                                <asp:HiddenField id="hdnSHKB" runat="server"/>
                                <asp:HiddenField id="hdnMA_NV" runat="server"/>
                                <asp:HiddenField id="hdnSO_BT" runat="server"/>
                                <asp:HiddenField id="hdnNGAYLV" runat="server"/>
                                <asp:HiddenField id="hdfInCTU" runat="server"/>
                                <asp:Button ID="btnInCTU" runat="server" CssClass="ButtonCommand" Text="In Chứng từ"/>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtRM_REF_NO" CssClass="inputflat" Visible="false"
                                    ReadOnly="true" Text="" />
                                <asp:TextBox runat="server" ID="txtREF_NO" CssClass="inputflat" Visible="false" ReadOnly="true"
                                    Text="" />
                                <asp:TextBox runat="server" ID="txtSoCT_NH" CssClass="inputflat" Visible="false"
                                    ReadOnly="true" Text="" />
                                <asp:TextBox runat="server" ID="txtTimeLap" CssClass="inputflat" Visible="false"
                                    ReadOnly="true" Text="" />
                            </td>
                            <td align="left" style="display: none">
                                PT tính phí
                                <asp:RadioButton ID="rdTypeMP" Text="Miễn phí" GroupName="RadioGroup" runat="server"
                                    Enabled="false" />
                                <asp:RadioButton ID="rdTypePT" Text="Phí trong" Visible="false" GroupName="RadioGroup"
                                    runat="server" Enabled="false" />
                                <asp:RadioButton ID="rdTypePN" Text="Phí ngoài" GroupName="RadioGroup" runat="server"
                                    Enabled="false" />
                            </td>
                            <td align="right">
                                Tổng Tiền
                                <asp:TextBox runat="server" ID="txtTTIEN" Style="text-align: right" CssClass="inputflat"
                                    ReadOnly="true" Text="0" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="4">
                                Bằng chữ
                                <asp:Label runat="server" ID="lblTTien_Chu" Style="text-align: right" CssClass="inputflat"
                                    ReadOnly="true" />
                            </td>
                        </tr>
                        <%--<tr align="right">
                                                                                <td align="right" valign="top">
                                                                                    &nbsp;
                                                                                </td>
                                                                                <td align="right" valign="top">
                                                                                    Số đã thu: &nbsp;
                                                                                    <asp:TextBox runat="server" ID="txtTT_TTHU" CssClass="inputflat" ReadOnly="true"
                                                                                        Text="0" />
                                                                                </td>
                                                                            </tr>--%>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%">
                        <tr align="left">
                            <td align="right" colspan="3">
                                Sản phẩm
                                <asp:TextBox ID="txtSanPham" Text="" runat="server" Width="80%" CssClass="inputflat"
                                    ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr align="right">
                            <td align="right">
                                CoreBank: Phí
                                <asp:TextBox runat="server" ID="txtCharge" Width="100px" CssClass="inputflat" ReadOnly="true"
                                    Text="0" Style="text-align: right" />
                            </td>
                            <td align="right">
                                VAT
                                <asp:TextBox runat="server" ID="txtVAT" Width="100px" CssClass="inputflat" ReadOnly="true"
                                    Text="0" Style="text-align: right" />
                            </td>
                        </tr>
                        <tr align="right">
                            <td align="right" style="display:none;">
                                eTax : Phí
                                <asp:TextBox runat="server" ID="txtCharge2" Width="100px" CssClass="inputflat" ReadOnly="true"
                                    Text="0" Style="text-align: right" />
                            </td>
                            <td align="right" style="display:none;">
                                VAT
                                <asp:TextBox runat="server" ID="txtVat2" Width="100px" CssClass="inputflat" ReadOnly="true"
                                    Text="0" Style="text-align: right" />
                            </td>
                            <td align="right">
                                Tổng trích nợ
                                <asp:TextBox runat="server" ID="txtTongTrichNo" Width="120px" CssClass="inputflat"
                                    ReadOnly="true" Text="0" Style="text-align: right" />
                            </td>
                        </tr>
                        <tr align="right" style="display: none">
                            <td align="left" colspan="3">
                                Đi Citad
                                <asp:RadioButton ID="RadioButton1" Text="Kiểu mới" GroupName="RGCitad" runat="server"
                                    Enabled="false" />
                                <asp:RadioButton ID="RadioButton2" Text="Kiểu cũ" GroupName="RGCitad" runat="server"
                                    Enabled="false" />
                            </td>
                        </tr>
                        <tr align="right" style="display:none;">
                            <td align="right" valign="top" colspan="4">
                                Lý do hủy: &nbsp;
                                <asp:TextBox ID="txtLyDoHuy" runat="server" CssClass="inputflat" Width="90%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
