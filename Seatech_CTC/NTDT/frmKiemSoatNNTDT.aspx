﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmKiemSoatNNTDT.aspx.vb" 
    Inherits="NTDT_frmKiemSoatNNTDT" title="Kiểm soát thông tin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .txtHoanThien{width:80%;}
        .container{width:100%;padding-bottom:50px;}
    </style>
    <script type="text/javascript" language="javascript">
//     function DisableButton() {
//                document.forms[0].submit();
//                window.setTimeout("disableButton('" + 
//                   window.event.srcElement.id + "')", 200);
//            }
    function disableButton() {
                document.getElementById('<%=btnDuyetHuy.clientID%>').disabled=true;
                document.getElementById('<%=btnKS.clientID%>').disabled=true;
                document.getElementById('<%=btnChuyenTra.clientID%>').disabled=true;
            }
   
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle"> Kiểm soát thông tin đăng ký dịch vụ NTDT </p>
    <div class="container">
        <table width="100%">
            <tr>
                <td style="width:20%" valign="top">
                    <asp:GridView ID="grdDSCT" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        ShowHeader="True" ShowFooter="True" BorderColor="#989898" CssClass="grid_data"
                        PageSize="15" Width="100%">
                        <PagerStyle CssClass="cssPager" />
                        <AlternatingRowStyle CssClass="grid_item_alter" />
                        <HeaderStyle CssClass="grid_header" HorizontalAlign="Left"></HeaderStyle>
                        <RowStyle CssClass="grid_item" />
                        <Columns>
                            <asp:TemplateField HeaderText="TT">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                     <%#Eval("TRANG_THAI").ToString()%>
                                </ItemTemplate>
                                 <FooterTemplate>
                                    Tổng số:
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="MST" FooterText="Tổng số:">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnSelectCT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MST") %>'
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "NNTDT_ID")%>'
                                        OnClick="btnSelectCT_Click" />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <%#grdDSCT.Rows.Count.ToString%>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Left" BackColor="#E4E5D7"></PagerStyle>
                    </asp:GridView>
                    <p class="form_label">Trạng thái:<br />
                    <asp:DropDownList ID="drpTrangThai" runat="server" Width="97%" CssClass="inputflat" AutoPostBack="true" >
                       <%-- <asp:ListItem Value="99" Text="Tất cả"></asp:ListItem>
                        <asp:ListItem Value="00" Text="Đăng ký mới"></asp:ListItem>
                        <asp:ListItem Value="01" Selected="True" Text="Chờ kiểm soát"></asp:ListItem>
                        <asp:ListItem Value="02" Text="Hủy - chờ duyệt"></asp:ListItem>
                        <asp:ListItem Value="03" Text="Thành công"></asp:ListItem>
                        <asp:ListItem Value="04" Text="Hủy"></asp:ListItem>
                        <asp:ListItem Value="05" Text="Điều chỉnh"></asp:ListItem>
                        <asp:ListItem Value="06" Text="Ngừng"></asp:ListItem>      
                        <asp:ListItem Value="07" Text="Chuyển trả"></asp:ListItem>--%>                                                                       
                    </asp:DropDownList></p>
                    <p><asp:Button ID="btnRefresh" runat="server" Text="Refresh" /></p>
                </td>
                <td>
                    <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Mã số thuế <span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtMST" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="false"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Tên NNT <span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtTenNNT" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="200"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Địa chỉ NNT</td>
                            <td class="form_control"><asp:TextBox ID="txtDCNNT" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="200"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Mã cơ quan thu <span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtMaCQT" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="5"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Email</td>
                            <td class="form_control"><asp:TextBox ID="txtEmail" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="200"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Số điện thoại</td>
                            <td class="form_control"><asp:TextBox ID="txtSDT" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="15"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Tên người liên hệ</td>
                            <td class="form_control"><asp:TextBox ID="txtNguoiLH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="200"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Số serial chứng thư số <span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtSoSerial" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="false" MaxLength="200"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Nhà cung cấp chứng thư số</td>
                            <td class="form_control"><asp:TextBox ID="txtNhaCC" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="200"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Mã ngân hàng</td>
                            <td class="form_control"><asp:TextBox ID="txtMaNH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="10"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Tên ngân hàng</td>
                            <td class="form_control"><asp:TextBox ID="txtTenNH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="200"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Mã TVAN</td>
                            <td class="form_control"><asp:TextBox ID="txtMaTVAN" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="6"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Tên TVAN</td>
                            <td class="form_control"><asp:TextBox ID="txtTenTVAN" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="200"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Ngày gửi</td>
                            <td class="form_control"><asp:TextBox ID="txtNgayGui" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Tên tài khoản</td>
                            <td class="form_control"><asp:TextBox ID="txtTenTKNH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Số tài khoản sử dụng NTDT <span class="requiredField">(*)</span></td>
                            <td>
                                <asp:GridView ID="grdTKNH" runat="server"  Width="80%" DataKeyNames="ID" ShowHeader="false"
                                    EmptyDataText="Chưa có" EmptyDataRowStyle-CssClass="form_label"   AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:BoundField DataField="ID" Visible="false"/>
                                        <asp:TemplateField HeaderText="Tài khoản ngân hàng">
                                            <ItemTemplate><asp:Label ID="lblSoTKNH" runat="server" Text='<%#Eval("SO_TK_NH") %>'></asp:Label></ItemTemplate>
                                            <ItemStyle Width="80%" CssClass="form_label"/>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Lý do hủy/chuyển trả</td>
                            <td class="form_control"><asp:TextBox ID="txtLyDo" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;
                            <asp:HiddenField ID="hdfNNTDT_ID" runat="server" />
                            <asp:HiddenField ID="hdrTRANG_THAI" runat="server" />
                            <asp:HiddenField ID="hdfMA_NV" runat="server" />
                            <asp:HiddenField ID="hdfSUBJECT_CERT_NTHUE" runat="server" />
                            <asp:HiddenField ID="hdfMA_GDICH" runat="server" />
                            <asp:HiddenField ID="hdfUpdate_TT" runat="server" />
                            </td>
                        </tr>
                         <tr>
                        <td align="left" width='100%'colspan="2">
                            <b>
                                <asp:Literal ID="lblStatus" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnKS" runat="server" Text="Kiểm soát" Width="90px" Enabled="false"/>&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnDuyetHuy" runat="server" Text="Duyệt hủy" Width="90px" Enabled="false"/>&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnChuyenTra" runat="server" Text="Chuyển trả" Width="90px" Enabled="false"/>&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnExit" runat="server" Text="Thoát" Width="90px"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        
    </div>
</asp:Content>

