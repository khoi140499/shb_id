﻿Imports VBOracleLib
Imports System.Data
Imports Business.NTDT.NNTDT
Imports Business.Common.mdlCommon

Partial Class NTDT_frmChiTiet
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                If Not Request.Params("ID") Is Nothing Then
                    Dim NNTDT_ID As String = Request.Params("ID").ToString
                    Dim strErrMsg As String = ""
                    If NNTDT_ID <> "" Then
                        Dim obj As New infNNTDT
                        obj.NNTDT_ID = NNTDT_ID
                        If Get_Data_DB(obj, strErrMsg) Then
                            If Not Get_DataToForm(obj, strErrMsg) Then
                                clsCommon.ShowMessageBox(Me, strErrMsg)
                            End If
                        Else
                            clsCommon.ShowMessageBox(Me, strErrMsg)
                        End If
                    End If
                End If
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Lỗi: " & ex.Message)
            End Try
        End If
    End Sub
    'Lấy dữ liệu từ DB theo NNTDT_ID
    Private Function Get_Data_DB(ByRef item As infNNTDT, ByRef strErrMsg As String) As Boolean
        Try
            Dim obj As New daNNTDT
            item = obj.Load(item.NNTDT_ID)
            Return True
        Catch ex As Exception
            strErrMsg = "Không lấy được thông tin người nộp thuế. Lỗi: " & ex.Message
            Return False
        End Try
    End Function
    'Lấy dữ liệu cho form
    Private Function Get_DataToForm(ByRef item As infNNTDT, ByRef strErrMsg As String) As Boolean
        Try
            txtMST.Text = item.MST
            txtTenNNT.Text = item.TEN_NNT
            txtDCNNT.Text = item.DIACHI_NNT
            txtMaCQT.Text = item.MA_CQT
            txtEmail.Text = item.EMAIL_NNT
            txtSDT.Text = item.SDT_NNT
            txtNguoiLH.Text = item.TEN_LHE_NTHUE
            txtSoSerial.Text = item.SERIAL_CERT_NTHUE
            txtNhaCC.Text = item.ISSUER_CERT_NTHUE
            txtMaNH.Text = item.MA_NHANG
            txtTenNH.Text = item.TEN_NHANG
            txtMaTVAN.Text = item.VAN_ID
            txtTenTVAN.Text = item.TEN_TVAN
            txtNgayGui.Text = item.NGAY_GUI
            'txtSoTKNH.Text = item.STK_NHANG
            txtTenTKNH.Text = item.TENTK_NHANG
            hdfNNTDT_ID.Value = item.NNTDT_ID
            hdrTRANG_THAI.Value = item.TRANG_THAI
            Load_Grid_DS_TKNH()
            Return True
        Catch ex As Exception
            strErrMsg = "Lỗi không đưa được thông tin lên form. Lỗi: " & ex.Message
            Return False
        End Try
    End Function
    'Lấy thông tin tài khoản ngân hàng của khách hàng
    Private Sub Load_Grid_DS_TKNH()
        Dim ds As New DataSet
        Try
            Dim sql = "Select * from ntdt_map_mst_tknh where NNTDT_ID=" & hdfNNTDT_ID.Value & " and MST='" & txtMST.Text.Trim & "'"
            ds = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            grdTKNH.DataSource = ds
            grdTKNH.DataBind()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách tài khoản ngân hàng của khách hàng. Lỗi: " & ex.Message)
        End Try
    End Sub
End Class
