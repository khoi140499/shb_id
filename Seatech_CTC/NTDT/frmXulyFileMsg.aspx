﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmXulyFileMsg.aspx.vb" Inherits="NTDT_frmXulyFileMsg" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../javascript/jquery-1.7.2.min.js" type="text/javascript"></script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle"> Xử lý file message </p>
    <div class="container">
        <asp:DataGrid runat="server" ID="dtgFileList" AutoGenerateColumns="False" 
            Width="100%" BorderColor="#000" CssClass="grid_data" AllowPaging="True" PageSize="20">
            <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
            <ItemStyle CssClass="grid_item" />
            <PagerStyle Mode="NumericPages" HorizontalAlign="Right" />
            <Columns>
                <asp:TemplateColumn ItemStyle-VerticalAlign="Middle" HeaderText="Chọn file">
                    <ItemTemplate>
                        <asp:HiddenField ID="hdFilePath" runat="server" Value='<%# Eval("FullPath") %>' />
                        <asp:CheckBox ID="chkSelectItem" CssClass="chkSelectItem" runat="server" />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="FileName" HeaderText="Tên file" />
                <asp:BoundColumn DataField="CreateTime" HeaderText="Ngày tạo" DataFormatString="{0:dd/MM/yyyy hh:mm:ss}"/>
            </Columns>
        </asp:DataGrid>
        <div style="text-align:center">
            <asp:Button ID="btnProcessMsg" runat="server" Text="Xử lý message đã chọn" OnClientClick="return confirm('Bạn có muốn xử lý các message đã chọn?')" />
            <asp:Button ID="btnDeleteMsg" runat="server" Text="Xóa message đã chọn" OnClientClick="return confirm('Bạn có muốn xóa các message đã chọn?')" />
        </div>
    </div>
</asp:Content>

