﻿Imports System.Data
Imports System.Diagnostics
Imports System.Xml
Imports System.Xml.XmlDocument
Imports Business
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports Business.ChungTu
Imports Business.NewChungTu
Imports Business.BaoCao
Imports System.Collections.Generic
Imports System.Xml.Linq
Imports ConcertCurToText
Imports System.IO
Imports CorebankServiceESB

Partial Class NTDT_frmChiTietCT
    Inherits System.Web.UI.Page
    Private illegalChars As String = "[&]"
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LogApp.AddDebug("Chon chung tu xem chi tiet", "TRA CUU CHUNG TU: Xem chi tiet chung tu")
        If Not IsPostBack Then
            If Not Request.QueryString.Item("soct") Is Nothing Then
                Dim so_ct As String = Request.QueryString.Item("soct").ToString
                GetChungTu(so_ct)
            End If
        End If
    End Sub
    Protected Sub GetChungTu(ByVal soct As String)
        LoadCTChiTietAll(soct)
        hdnSO_CTU.Value = soct
        lbSoCT.Text = "Số CT: " & soct
        lbSoCT.Visible = True
    End Sub
    Private Sub LoadCTChiTietAll(ByVal so_ct As String)
        Try
            Dim v_strTT As String = String.Empty
            Dim v_strTTThue As String = String.Empty
            Dim v_strLoaiThue As String = String.Empty
            Dim v_strPhuongThucTT As String = String.Empty
            Dim v_str_Ma_NNT As String = String.Empty

            'Load chứng từ header
            Dim dsCT As New DataSet
            btnInCTU.Visible = False
            dsCT = ChungTu.buChungTu.CTUNTDT_Header_Set(so_ct)
            If (dsCT.Tables(0).Rows.Count > 0) Then
                ShowHideCTRow(True)
                FillDataToCTHeader(dsCT)
                v_strTT = dsCT.Tables(0).Rows(0)("TRANG_THAI").ToString
                v_strLoaiThue = dsCT.Tables(0).Rows(0)("MA_LTHUE").ToString
                v_strPhuongThucTT = dsCT.Tables(0).Rows(0)("PT_TT").ToString
                v_str_Ma_NNT = dsCT.Tables(0).Rows(0)("Ma_NNThue").ToString
                v_strTTThue = dsCT.Tables(0).Rows(0)("TT_CTHUE").ToString
            Else
                lblStatus.Text = "Không tìm thấy chứng từ cần tìm.Hãy thử lại"
                Exit Sub
            End If
            'Load thông tin chi tiết
            Dim dsCTChiTiet As New DataSet
            dsCTChiTiet = ChungTu.buChungTu.CTUNTDT_ChiTiet_Set(so_ct)
            If dsCTChiTiet.Tables(0).Rows.Count > 0 Then
                FillDataToCTDetail(dsCTChiTiet, v_str_Ma_NNT)
                CaculateTongTien(dsCTChiTiet.Tables(0))
            Else
                lblStatus.Text = "Không lấy được thông tin chi tiết của chứng từ.Hãy kiểm tra lại"
                Exit Sub
            End If
            ShowHideControlByLoaiThue(v_strLoaiThue)
            ShowHideControlByPT_TT(v_strPhuongThucTT, v_strTT)
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)
            log.Error(sbErrMsg.ToString())
            
        Finally
        End Try
    End Sub

    Private Sub FillDataToCTHeader(ByVal dsCT As DataSet)
        Try
            Dim strTenVT = String.Empty
            Dim strTenLH As String = String.Empty
            Dim strMaLH As String = String.Empty
            Dim dr As DataRow = dsCT.Tables(0).Rows(0)
            Dim v_strTrangThai As String = dr("TRANG_THAI").ToString()
            'Dim v_strHTTT As String = GetString(dr("PT_TT")).ToString
            txtSHKB.Text = dr("SHKB").ToString()
            txtTenKB.Text = dr("TEN_KB").ToString()
            txtNGAY_KH_NH.Text = ConvertNumbertoString(dr("NGAY_KB").ToString())
            txtTKCo.Text = dr("TK_Co").ToString()
            txtTKNo.Text = dr("TK_No").ToString()
            txtMaDBHC.Text = dr("MA_XA").ToString()
            txtTenDBHC.Text = Get_TenTinh(dr("MA_XA").ToString())
            txtMaNNT.Text = dr("MA_NNTHUE").ToString()
            txtTenNNT.Text = dr("TEN_NNTHUE").ToString()
            txtDChiNNT.Text = dr("DC_NNTHUE").ToString()
            txtMaNNTien.Text = dr("ma_nntien").ToString 'dr("Ma_NNTien").ToString()
            txtTenNNTien.Text = dr("ten_nntien").ToString 'dr("TEN_NNTIEN").ToString()
            txtDChiNNTien.Text = dr("DC_NNTIEN").ToString()
            txtMaCQThu.Text = dr("MA_CQTHU").ToString()
            txtTenCQThu.Text = dr("TEN_CQTHU").ToString()
            Dim dt As New DataTable
            dt.Columns.Add("ID_HQ", Type.GetType("System.String"))
            dt.Columns.Add("Name", Type.GetType("System.String"))
            dt.Columns.Add("Value", Type.GetType("System.String"))
            dt.Columns(0).AutoIncrement = True
            Dim R As DataRow = dt.NewRow
            R("ID_HQ") = dr("MA_LTHUE").ToString()
            R("Name") = GetTenLoaiThue(dr("MA_LTHUE").ToString()) 'dr("MA_LTHUE").ToString() & "-" & GetTenLoaiThue(dr("MA_LTHUE").ToString())
            R("Value") = dr("MA_LTHUE").ToString()
            dt.Rows.Add(R)
            ddlLoaiThue.DataSource = dt
            ddlLoaiThue.DataTextField = "Name"
            ddlLoaiThue.DataValueField = "Value"
            ddlLoaiThue.DataBind()

            'txtDescLoaiThue.Text = GetTenLoaiThue(dr("MA_LTHUE").ToString())
            strMaLH = GetString(dr("LH_XNK").ToString())
            Get_LHXNK_ALL(strMaLH, strTenLH, strTenVT)
            txtLHXNK.Text = strMaLH
            txtDescLHXNK.Text = strTenVT & "-" & strTenLH
            txtSoKhung.Text = GetString(dr("SO_KHUNG").ToString())
            txtSoMay.Text = GetString(dr("SO_MAY").ToString())
            txtToKhaiSo.Text = GetString(dr("SO_TK").ToString())
            txtNgayDK.Text = GetString(dr("NGAY_TK"))
            'txtSo_BK.Text = GetString(dr("So_BK"))
            'txtNgay_BK.Text = GetString(dr("Ngay_BK"))
            rowTKGL.Visible = False
            'If v_strHTTT = "03" Then
            '    rowTKGL.Visible = True
            '    rowSoDuNH.Visible = False
            '    rowTK_KH_NH.Visible = False
            '    txtTKGL.Text = GetString(dr("TK_KH_NH"))
            '    txtTenTKGL.Text = dr("TEN_KH_NH").ToString
            'End If
            If dr("ma_ntk").ToString = "1" Then
                txtTenTKCo.Text = "Tài khoản thu NSNN"
            ElseIf dr("ma_ntk").ToString = "2" Then
                txtTenTKCo.Text = "Tài khoản tạm thu"
            Else
                txtTenTKCo.Text = "Thu hoàn thuế GTGT"
            End If
            rowLHXNK.Visible = False
            txtTK_KH_NH.Text = GetString(dr("TK_KH_NH"))
            txtTenTK_KH_NH.Text = dr("TEN_KH_NH").ToString
            txtNGAY_KH_NH.Text = GetString(dr("NGAY_KH_NH"))
            txtMA_NH_A.Text = GetString(dr("MA_NH_A"))
            txtTen_NH_A.Text = GetString(dr("TEN_NH_A"))
            txtMA_NH_B.Text = dr("MA_NHANG_UNT").ToString 'GetString(dr("MA_NH_B"))
            txtTen_NH_B.Text = dr("TEN_NHANG_UNT").ToString 'GetString(dr("TEN_NH_B"))
            'txtDienGiaiNH.Text = GetString(dr("NH_HUONG"))
            Try
                txtDienGiaiHQ.Text = GetString(dr("DIENGIAI_HQ"))
            Catch ex As Exception
                txtDienGiaiHQ.Text = ""
            End Try
            Dim dt1 As DataTable = getNHTrucTiepGianTiepKB_SHB(dr("shkb").ToString(), dr("ma_nhang_unt").ToString())
            txtMA_NH_TT.Text = dt1.Rows(0)("ma_tructiep").ToString
            txtTEN_NH_TT.Text = dt1.Rows(0)("ten_tructiep").ToString
            txtMaNT.Text = dr("MA_NGUYENTE").ToString 'GetString(dr("Ma_NT"))
            txtTenNT.Text = dr("MA_NGUYENTE").ToString 'GetString(dr("Ten_NT"))
            txtTimeLap.Text = GetString(dr("ngay_ht"))
            txtSO_QD.Text = GetString(dr("so_qd"))
            txtCQ_QD.Text = GetString(dr("cq_qd"))
            txtNGAY_QD.Text = GetString(dr("ngay_qd"))
            ' txtTenTKNo.Text = dr("TEN_TKNO").ToString()
            'txtTenTKCo.Text = ""
            'tynk-comment phí và VAT - 08-04-2013*********
            Dim strPTTP As String = ""
            strPTTP = dr("pt_tinhphi").ToString()
            If strPTTP = "O" Then
                rdTypePN.Checked = True
            ElseIf strPTTP = "B" Then
                rdTypePT.Checked = True
            Else
                rdTypeMP.Checked = True
            End If
            If txtTKCo.Text.Length >= 3 Then
                If txtTKCo.Text.Substring(0, 3) = "711" Then
                    txtTenTKCo.Text = "Tài khoản thu NSNN"
                Else
                    txtTenTKCo.Text = "Thu hoàn thuế GTGT"
                End If
            End If
            Dim strNHA As String = Get_NH_A()
            txtMA_NH_A.Text = dr("MA_NHANG_NOP").ToString 'strNHA.Split(";")(0).ToString()
            txtTen_NH_A.Text = dr("TEN_NHANG_NOP").ToString 'strNHA.Split(";")(1).ToString()
            'v_strHTTT = "01"
            'ddlHTTT.SelectedValue = v_strHTTT
            'ddlHTTT.SelectedItem.Text = Get_TenPTTT(v_strHTTT)
            txtGNT.Text = dr("SO_GNT").ToString()
            txtREFCORE.Text = dr("SO_CT_NH").ToString()
            txtHTTT.Text = dr("PT_TT").ToString
            Dim strTT_CITAD As String = ""
            strTT_CITAD = dr("TT_CITAD").ToString()
            If strTT_CITAD = "1" Then
                RadioButton1.Checked = True
            Else
                RadioButton2.Checked = True
            End If
            Try
                txtCharge.Text = VBOracleLib.Globals.Format_Number(dr("PHI_GD").ToString(), ".")
                txtVAT.Text = VBOracleLib.Globals.Format_Number(dr("PHI_VAT").ToString(), ".")
            Catch ex As Exception
                txtCharge.Text = ""
                txtVAT.Text = ""
            End Try
            Try
                txtCharge2.Text = VBOracleLib.Globals.Format_Number(dr("PHI_GD2").ToString(), ".")
                txtVat2.Text = VBOracleLib.Globals.Format_Number(dr("PHI_VAT2").ToString(), ".")
            Catch ex As Exception
                txtCharge2.Text = ""
                txtVat2.Text = ""
            End Try
            Try
            Catch ex As Exception
                txtCharge2.Text = ""
                txtVat2.Text = ""
            End Try
            '*********************************************
            txtRM_REF_NO.Text = dr("RM_REF_NO").ToString()
            txtREF_NO.Text = dr("REF_NO").ToString()
            txtSoCT_NH.Text = dr("So_CT_NH").ToString()
            txtQuan_HuyenNNTien.Text = dr("QUAN_HUYENNNTIEN").ToString()
            txtTinh_TphoNNTien.Text = dr("TINH_TPNNTIEN").ToString()
            txtKHCT.Text = dr("KyHieu_CT").ToString()
            txtSoCT.Text = dr("So_CT").ToString()
            If dr("LY_DO_HUY").ToString().Trim <> "" Then
                txtLyDoHuy.Text = dr("LY_DO_HUY").ToString().Trim
            Else
                txtLyDoHuy.Text = ""
            End If
            'hdfTrangThai_CT.Value = dr("trang_thai").ToString().Trim
            'hdfTrangThai_CThue.Value = dr("tt_cthue").ToString().Trim
            Dim strTenTinhTP As String = ""
            ''vinh
            Try
                txtQuanNNThue.Text = dr("MA_HUYEN_NNOP").ToString ' dr("ma_huyen_nnthue").ToString().Trim
                txtTenQuanNNThue.Text = dr("TEN_HUYEN_NNOP").ToString 'clsCTU.Get_TenQuanHuyen(txtQuanNNThue.Text.ToString())
                'strTenTinhTP = clsCTU.Get_TenTinhTP(txtQuanNNThue.Text.ToString())
                txtTinhNNThue.Text = dr("MA_TINH_NNOP").ToString 'strTenTinhTP.Split(";")(0).ToString()
                txtTenTinhNNThue.Text = dr("TEN_TINH_NNOP").ToString 'strTenTinhTP.Split(";")(1).ToString()
            Catch ex As Exception
                txtQuanNNThue.Text = "" ' dr("Quan_huyennntien").ToString().Trim
                txtTenQuanNNThue.Text = ""
                txtTinhNNThue.Text = ""
                txtTenTinhNNThue.Text = ""
            End Try
            Try
                If dr("ma_tinh").ToString().Trim <> "" Then
                    txtQuanNNTien.Text = dr("ma_quan_huyennntien").ToString().Trim
                    txtTenQuanNNTien.Text = clsCTU.Get_TenQuanHuyen(txtQuanNNTien.Text.ToString())
                    strTenTinhTP = clsCTU.Get_TenTinhTP(txtQuanNNTien.Text.ToString())
                    txtTinhNNTien.Text = strTenTinhTP.Split(";")(0).ToString()
                    txtTenTinhNNTien.Text = strTenTinhTP.Split(";")(1).ToString()
                Else
                    txtQuanNNTien.Text = ""
                    txtTenQuanNNTien.Text = ""
                    txtTinhNNTien.Text = ""
                    txtTenTinhNNTien.Text = ""
                End If
            Catch ex As Exception
                txtQuanNNTien.Text = ""
                txtTenQuanNNTien.Text = ""
                txtTinhNNTien.Text = ""
                txtTenTinhNNTien.Text = ""
            End Try
            txtSanPham.Text = dr("ma_sanpham").ToString()
            txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number(dr("tt_tthu").ToString().Trim, ".")
            If dr("SEQ_NO").ToString() <> "" Then
                lbSoFT.Text = "Số FT:" & dr("SEQ_NO").ToString()
                If dr("so_xcard").ToString() <> "" Then
                    lbSoFT.Text &= " Số XCARD :" & dr("so_xcard").ToString()
                End If
            Else
                lbSoFT.Text = ""
            End If
            Try
                txtCK_SoCMND.Text = dr("SO_CMND").ToString()
                txtCK_SoDT.Text = dr("SO_FONE").ToString()
                txtDienGiaiHQ.Text = dr("DIENGIAI_HQ")
            Catch ex As Exception
                txtCK_SoCMND.Text = ""
                txtCK_SoDT.Text = ""
                txtDienGiaiHQ.Text = ""
            End Try
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình FillDataToCTHeader")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)
            log.Error(sbErrMsg.ToString())
          
            ' Throw ex
        Finally
        End Try
    End Sub
    Private Sub FillDataToCTDetail(ByVal pv_dsCT As DataSet, ByVal v_strMa_NNT As String)
        Try
            Dim v_dt As DataTable = pv_dsCT.Tables(0)
            Dim dr As DataRow
            Dim sodathu As String
            grdChiTiet.DataSource = v_dt
            grdChiTiet.DataBind()
            For i As Integer = 0 To v_dt.Rows.Count - 1
                dr = v_dt.Rows(i)
                sodathu = clsCTU.TCS_CalSoDaThu(v_strMa_NNT, dr("MaQuy").ToString(), dr("Ma_Chuong").ToString(), dr("Ma_Khoan").ToString(), dr("Ma_TMuc").ToString(), dr("Ky_Thue").ToString())
                grdChiTiet.Items(i).Cells(6).Text = sodathu
                grdChiTiet.Items(i).Cells(6).Enabled = False
                'CType(grdChiTiet.Items(i).Controls(5).Controls(1), TextBox).Text = VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(5).Controls(1), TextBox).Text, ".")
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CaculateTongTien(ByVal pv_dt As DataTable)
        'Tính toán và cập nhật lại trường tổng tiền
        Dim dblTongTien As Double = 0
        Dim dblTongTrichNo As Double = 0
        For i As Integer = 0 To pv_dt.Rows.Count - 1
            dblTongTien += pv_dt.Rows(i)("TTIEN")
        Next
        'tynk-commennt phí và VAT-08-04-2013*******
        Dim dblCharge As Double = CDbl(IIf((txtCharge2.Text.Replace(".", "").Length > 0), txtCharge2.Text.Replace(".", ""), "0"))
        Dim dblChargeET As Double = CDbl(IIf((txtCharge.Text.Replace(".", "").Length > 0), txtCharge.Text.Replace(".", ""), "0"))
        Dim dblVAT As Double = CDbl(IIf((txtVat2.Text.Replace(".", "").Length > 0), txtVat2.Text.Replace(".", ""), "0"))
        Dim dblVATeT As Double = CDbl(IIf((txtVAT.Text.Replace(".", "").Length > 0), txtVAT.Text.Replace(".", ""), "0"))
        '******************************************
        txtTTIEN.Text = VBOracleLib.Globals.Format_Number(dblTongTien, ".")
        dblTongTrichNo = dblTongTien + dblCharge + dblVAT + dblChargeET + dblVATeT
        txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number(dblTongTrichNo, ".")
        lblTTien_Chu.Text = ConcertCurToText.ConvertCurr.So_chu(txtTTIEN.Text.Trim.Replace(".", ""))
        'lbTongTrichNoBangChu.Text = ConcertCurToText.ConvertCurr.So_chu(txtTongTrichNo.Text.Trim.Replace(".", ""))
    End Sub
    Private Function GetTenLoaiThue(ByVal pv_strMaLoaiThue As String) As String
        Select Case pv_strMaLoaiThue
            Case "01"
                Return "Thuế nội địa"
            Case "02"
                Return "Thuế thu nhập cá nhân"
            Case "03"
                Return "Thuế trước bạ"
            Case "04"
                Return "Thuế Hải Quan"
            Case "05"
                Return "Thu khác"
            Case "06"
                Return "Thu tài chính"
            Case Else '05'
                Return "Phạt vi phạm hành chính"
        End Select
    End Function

    Private Sub ShowHideCTRow(ByVal pv_blnDisplayRow As Boolean)
        rowDChiNNT.Visible = pv_blnDisplayRow
        rowNNTien.Visible = pv_blnDisplayRow
        rowDChiNNTien.Visible = False
        rowQuan_HuyenNNTien.Visible = False
        rowTinhNNTien.Visible = pv_blnDisplayRow
        rowDBHC.Visible = pv_blnDisplayRow
        rowCQThu.Visible = pv_blnDisplayRow
        rowTKNo.Visible = False
        rowTKCo.Visible = pv_blnDisplayRow
        rowHTTT.Visible = pv_blnDisplayRow
        rowTK_KH_NH.Visible = pv_blnDisplayRow
        rowSoDuNH.Visible = False
        rowNgay_KH_NH.Visible = pv_blnDisplayRow
        rowMaNT.Visible = pv_blnDisplayRow
        rowLoaiThue.Visible = pv_blnDisplayRow
        rowToKhaiSo.Visible = pv_blnDisplayRow
        rowNgayDK.Visible = pv_blnDisplayRow
        rowLHXNK.Visible = pv_blnDisplayRow
        rowSoKhung.Visible = pv_blnDisplayRow
        rowSoMay.Visible = pv_blnDisplayRow
        'rowSoBK.Visible = pv_blnDisplayRow
        'rowNgayBK.Visible = pv_blnDisplayRow
        rowNganHangChuyen.Visible = pv_blnDisplayRow
        rowNganHangNhan.Visible = pv_blnDisplayRow
        rowTKGL.Visible = pv_blnDisplayRow
        rowNNTien.Visible = pv_blnDisplayRow
        rowSoCMND.Visible = pv_blnDisplayRow
        rowDChiNNTien.Visible = pv_blnDisplayRow
        rowSoDT.Visible = pv_blnDisplayRow
        rowTinhNNThue.Visible = pv_blnDisplayRow
        rowTinhNNTien.Visible = pv_blnDisplayRow
        rowQuanNNThue.Visible = pv_blnDisplayRow
        rowQuanNNTien.Visible = pv_blnDisplayRow
        rowSO_QD.Visible = False
        rowCQ_QD.Visible = False
        rowNgay_QD.Visible = False
    End Sub

    Private Sub ShowHideControlByLoaiThue(ByVal pv_strLoaiThue As String)
        Select Case pv_strLoaiThue
            Case "01" 'NguonNNT.NNThue_CTN, NguonNNT.NNThue_VL, NguonNNT.SoThue_CTN, NguonNNT.VangLai
                'An thong tin To khai
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False

                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case "02"
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case "03" 'NguonNNT.TruocBa
                'Show thong tin Truoc ba
                rowSoKhung.Visible = True
                rowSoMay.Visible = True

                'an thong tin to khai
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
            Case "04" 'NguonNNT.HaiQuan
                'Show thong tin to khai
                rowLHXNK.Visible = True
                rowToKhaiSo.Visible = True
                rowNgayDK.Visible = True

                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case "05"
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case "06"
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case Else '"05" Khong xac dinh
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
                rowSO_QD.Visible = True
                rowCQ_QD.Visible = True
                rowNgay_QD.Visible = True
        End Select
    End Sub

    Private Sub ShowHideControlByPT_TT(ByVal pv_strPT_TT As String, ByVal pv_strTT As String)
        If pv_strPT_TT = "01" Or pv_strPT_TT = "03" Or pv_strPT_TT = "CK" Then 'Chuyển khoản
            'If pv_strPT_TT = "03" Then ' Chỉ GL mới lấy ra số dư vì chuyển khoản đã trừ tiền trước rồi
            rowTK_KH_NH.Visible = True
            rowNganHangNhan.Visible = True
            rowNganHangChuyen.Visible = True
            rowNNTien.Visible = True
            rowSoCMND.Visible = False
            rowDChiNNTien.Visible = True
            rowSoDT.Visible = False
            rowTKGL.Visible = False

            rowSoDuNH.Visible = True
            rowTK_KH_NH.Visible = True
            Dim v_strSoTKKH As String = txtTK_KH_NH.Text.Trim
            Dim v_strLoaiTien As String = txtMaNT.Text
            If v_strSoTKKH.Length > 0 Then
                If (txtTK_KH_NH.Text.Trim.Length > 0) Then
                    Dim tentk As String = GetTEN_KH_NH(v_strSoTKKH)
                    txtTenTK_KH_NH.Text = tentk
                    Dim Err As String = ""
                    Dim so_du As String = GetSoduTK_KH_NH(v_strSoTKKH, Err)
                    If Err.Trim = "" Then
                        txtSoDu_KH_NH.Text = so_du
                    Else
                        clsCommon.ShowMessageBox(Me, Err)
                        txtSoDu_KH_NH.Text = ""
                    End If
                End If
            Else
                rowSoDuNH.Visible = False
            End If
        ElseIf pv_strPT_TT = "00" Then  'Tiền mặt
            rowTK_KH_NH.Visible = False
            'rowNganHangNhan.Visible = False
            'rowNganHangChuyen.Visible = False
            rowSoDuNH.Visible = False
            rowTKGL.Visible = False

            rowNNTien.Visible = True
            rowSoCMND.Visible = True
            rowDChiNNTien.Visible = True
            rowSoDT.Visible = True
            'ElseIf pv_strPT_TT = "01" Then  'chuyên khoan KH
            '    rowSoDuNH.Visible = False
        Else 'Báo có
            rowTK_KH_NH.Visible = False
            rowNganHangNhan.Visible = False
            rowNganHangChuyen.Visible = False
            rowSoDuNH.Visible = False
            rowTKGL.Visible = True
            rowNNTien.Visible = False
            rowSoCMND.Visible = False
            rowDChiNNTien.Visible = False
            rowSoDT.Visible = False
        End If
    End Sub

    Protected Sub btnInCTU_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInCTU.Click
        Try
            Dim ngayLV As String = hdnNGAYLV.Value
            Dim strSoCT As String = hdnSO_CTU.Value
            Dim strSo_BT As String = hdnSO_BT.Value
            Dim strSHKB As String = hdnSHKB.Value
            Dim MaNV As String = hdnMA_NV.Value
            Response.Cookies.Add(New HttpCookie("User", Session.Item("User").ToString()))
            'Response.Redirect(("../../pages/Baocao/frmInGNT.aspx?SoCT=" + strSoCT + "&SHKB=" + strSHKB + "&SoBT=" + strSo_BT + "&NgayKB=" + ngayLV + "&MaNV=" + MaNV + "&BS=1"), False)
            Response.Redirect(("../../pages/Baocao/InGNTpdf.ashx?SoCT=" + strSoCT + "&SHKB=" + strSHKB + "&SoBT=" + strSo_BT + "&NgayKB=" + ngayLV + "&MaNV=" + MaNV + "&BS=1"), False)
            'hdfInCTU.Value = "1"
        Catch ex As Exception

        End Try
    End Sub
    '------------------------
    Private Function GetTEN_KH_NH(ByVal pv_strTKNH As String) As String
        Dim strReturn As String = ""
        Dim crBank As New clsCoreBank
        strReturn = clsCoreBank.GetTen_TK_KH_NH(pv_strTKNH.Replace("-", ""))
        Return strReturn
    End Function
    Private Function GetTK_KH_NH(ByVal pv_strTKNH As String, ByRef err As String) As String
        Dim sRet As String = ""
        Try
            Dim ma_loi As String = ""
            Dim so_du As String = ""
            Dim branch_tk As String = ""
            err = ""
            Dim crB As New clsCoreBank
            Dim result As String = crB.GetTK_KH_NH(pv_strTKNH.Replace("-", ""), ma_loi, so_du, branch_tk)
            If ma_loi = "00000" Or ma_loi Is Nothing Then
                sRet = branch_tk
            End If
        Catch ex As Exception
            err = ex.Message
        End Try
        Return sRet
    End Function
    Private Function GetSoduTK_KH_NH(ByVal pv_strTKNH As String, ByRef err As String) As String
        Dim sRet As String = ""
        Try
            Dim ma_loi As String = ""
            Dim so_du As String = ""
            Dim branch_tk As String = ""
            err = ""
            Dim crB As New clsCoreBank
            Dim result As String = crB.GetTK_KH_NH(pv_strTKNH.Replace("-", ""), ma_loi, so_du, branch_tk)
            If ma_loi = "00000" Or ma_loi Is Nothing Then
                sRet = VBOracleLib.Globals.Format_Number(so_du, ".")
            End If
        Catch ex As Exception
            err = ex.Message
        End Try
        Return sRet
    End Function
    Public Shared Function getNHTrucTiepGianTiepKB_SHB(ByVal pStrSHKB As String, ByVal pStrMa_GT As String) As DataTable
        Dim dt As DataTable = New DataTable()
        Try
            Dim strSQL As String = "Select * from TCS_DM_NH_GIANTIEP_TRUCTIEP where SHKB='" & pStrSHKB & "' and ma_giantiep='" & pStrMa_GT & "'"
            dt = DataAccess.ExecuteToTable(strSQL)
        Catch ex As Exception

        End Try
        Return dt
    End Function
End Class
