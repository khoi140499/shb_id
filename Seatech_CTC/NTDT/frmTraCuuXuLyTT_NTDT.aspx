﻿

<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmTraCuuXuLyTT_NTDT.aspx.vb" Inherits="NTDT_frmTraCuuXuLyTT_NTDT" title="Tra cứu và xử lý trạng thái chứng từ nộp thuế điện tử" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
        var soct = [];
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }


        function check_and_confirm() {
            if ($("input:checked").length <= 0) {
                alert("Phải chọn ít nhất 1 message");
                return false;
            }
            else {
                return confirm("Bạn có muốn xử lý chứng từ đã chọn?")
            }
        }
        function CheckUpdate() {
            if ($("input:checked").length <= 0) {
                alert("Phải chọn ít nhất 1 message");
                return false;
            }
            else {
                return confirm("Bạn có muốn xử lý chứng từ đã chọn?")
            }
        }
        function fncSearch() {
            var tuNgay = $('#txtTuNgay').val();
            var denNgay = $('#txtDenNgay').val();
            var SoCT = $('#txtSoCT').val();
            var MST = $('#txtMST').val();
            var SctKore = $('#txtSoCoreBank').val();
            var SoTC = $('#txtSoTC').val();
            var soien = $('#txtSoTien').val();
            var tinhtrang = $('#cboTinh_trang').find(":selected").val();
            var obj = {};
            obj.tuNgay = tuNgay;
            obj.denNgay = denNgay;
            obj.SoCT = SoCT;
            obj.MST = MST;
            obj.SctKore = SctKore;
            obj.SoTC = SoTC;
            obj.soien = soien;
            obj.tinhtrang = tinhtrang;
            var json = JSON.stringify(obj);
            $.ajax({
                type: "POST",
                url: "frmTraCuuXuLyTT_NTDT.aspx/Search",
                cache: false,
                data: JSON.stringify({
                    "js": json
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status, xhr) {// success callback function
                    var kq = data.d;
                    var x = $.parseJSON(kq);
                    if (x.errorCode != '0') {
                        alert(x.errorMess);
                    } else if (x.errorCode == '0') {
                        $('#tbDL tbody').html('');
                        $('#tbDL tbody').append(x.errorMess);
                    }
                },
                error: function (data, status, xhr) {
                    console.log(data);
                }
            });
        }
        function fncEn(e) {
            var row = e.closest("tr");
            var id = "#sl" + $(row).find('td:eq(2)').text().trim();
            var cs = e.checked;
            if (e.checked) {
                $(id).removeAttr('disabled');
                soct.push($(row).find('td:eq(2)').text().trim() + "-" + $(id).find(":selected").val());
            } else {
                $(id).attr('disabled', 'disabled');
                let evens = soct.filter(v => v != ($(row).find('td:eq(2)').text().trim() + "-" + $(id).find(":selected").val()));
                soct = evens;
            }
            alert(soct);
        }

        function frnUpdate() {
            frnAction("Update");
        }
        function frnAction(funcName) {
            $.ajax({
                type: "POST",
                url: "frmTraCuuXuLyTT_NTDT.aspx/" + funcName,
                cache: false,
                data: JSON.stringify({
                    "js": soct.toString()
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status, xhr) { //success callback function
                    var kq = data.d;
                    var x = $.parseJSON(kq);
                    if (x.errorCode == '0') {
                        soct = [];
                    }
                    alert(x.errorMess);
                    fncSearch();
                },
                error: function (data, status, xhr) {
                    console.log(data);
                }
            });
        }

        function fncUpdateSelect(e) {
            var row = e.closest("tr");
            var id = "#sl" + $(row).find('td:eq(2)').text().trim();
            let evens = soct.filter(v => v.split('-')[0] != ($(row).find('td:eq(2)').text().trim()));
            soct = evens;
            soct.push($(row).find('td:eq(2)').text().trim() + "-" + $(id).find(":selected").val());
        }

        function fncProcess() {
            frnAction("Process");
        }

    </script>
    <style type="text/css">
        .container{width:100%; margin-bottom:30px}
        .dataView{padding-bottom:10px}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle">Tra cứu và xử lý trạng thái chứng từ nộp thuế điện tử </p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
            <tr>
                <td align="left" style="width:20%" class="form_label">Từ ngày</td>
                <td style="width:30%" class="form_control" align="left">
                    <span style="display:block; width:57%; position:relative;vertical-align:middle;">
                        <asp:TextBox runat="server" ID="txtTuNgay" CssClass="inputflat date-pick dp-applied" Width="84%" />
                    </span>
                </td>
                <td align="left" style="width:20%" class="form_label">Đến ngày</td>
                <td style="width:30%" class="form_control" align="left">
                    <span style="display:block; width:57%; position:relative;vertical-align:middle;">
                        <asp:TextBox runat="server" ID="txtDenNgay" CssClass="inputflat date-pick dp-applied" Width="84%" />
                    </span>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:20%" class="form_label">
                    Số chứng từ
                </td>
                <td align="left" class="form_control">
                    <asp:TextBox ID="txtSoCT" runat="server" CssClass="inputflat" />
                </td>
                <td align="left" style="width:20%" class="form_label">Mã số thuế</td>
                <td  align="left" class="form_control"><asp:TextBox runat="server" ID="txtMST" CssClass="inputflat" /></td>
            </tr>
            
             <tr>
                <td align="left" style="width:20%" class="form_label">
                    Số chứng từ core bank
                </td>
                <td align="left" class="form_control">
                    <asp:TextBox ID="txtSoCoreBank" runat="server" CssClass="inputflat" />
                </td>
                <td align="left" style="width:20%" class="form_label">Số tham chiếu</td>
                <td  align="left" class="form_control"><asp:TextBox runat="server" ID="txtSoTC" CssClass="inputflat" /></td>
            </tr>

                 <tr>
                <td align="left" style="width:20%" class="form_label">
                    Số tiền
                </td>
                <td align="left" class="form_control">
                    <asp:TextBox ID="txtSoTien" runat="server" CssClass="inputflat" />
                </td>
                   <td align="left" class="form_label" style="width:20%">
                           Trạng thái
                       </td>
                        <td align="left" class="form_control">
                            <asp:DropDownList ID="cboTinh_trang" runat="server" CssClass="inputflat">
                                <asp:ListItem Text="Tất cả" Value="" Selected ="True"></asp:ListItem>
                                <asp:ListItem Text="Gửi thành công đến ngân hàng UNT" Value="01"></asp:ListItem>
                                <asp:ListItem Text="Lỗi gửi đến ngân hàng UNT" Value="02"></asp:ListItem>
                                <asp:ListItem Text="Gửi thành công đến KBNN" Value="03"></asp:ListItem>
                                <asp:ListItem Text="Lỗi gửi đến KBNN" Value="04"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
            </tr>
        
            <tr align="center">
                <td colspan="4" align="center">
                    <input type="button" value="Tra cứu" id="btnSearch" onclick="fncSearch()"/> &nbsp; &nbsp
                    <input type="button" value="Xử lý" id="btnProcess"onclick="fncProcess()"/> &nbsp; &nbsp
                    <input type="button" value="Cập nhật" id="btnUpdate" onclick="frnUpdate()"/>
                </td>
            </tr>
        </table>


         <div class="dataView">
             <table style="width: 100%" id ="tbDL" class='grid_data' cellspacing='0' rules='all' border='0.5' style='width: 100%;
                            border-collapse: collapse;'>
                 <thead style="height: 37px;background-color: #5ec9cd;">
                     <tr>
                         <th style="width: 3%">
                             <b>Chọn</b>
                         </th>
                         <th style="width: 10%">
                            <b>Số chứng từ core bank</b>
                         </th>
                         <th style="width: 10%">
                            <b>Số chứng từ</b>
                         </th>
                         <th style="width: 10%">
                            <b>Số tiền</b>
                         </th>
                         <th style="width: 10%">
                             <b>Số tham chiếu</b>
                         </th>
                         <th style="width: 12%">
                            <b>Mã số thuế</b>
                         </th>
                         <th style="width: 13%">
                            <b>Tên người nộp thuế</b>
                         </th>
                         <th style="width: 12%">
                            <b>Ngày giao dịch</b>
                         </th>
                        <th style="width: 20%">
                            <b>Trạng thái</b>
                         </th>
                     </tr>
                 </thead>
                 <tbody>

                 </tbody>
             </table>
                 <asp:HiddenField id="ListCT_NoiDia" runat="server" />
             <br />
             </div>

    </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function () {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>
</asp:Content>

