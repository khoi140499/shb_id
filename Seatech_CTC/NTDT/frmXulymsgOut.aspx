﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmXulymsgOut.aspx.vb" Inherits="NTDT_frmXulymsgOut" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }

        function jsChiTietCTU(_ID) {
            window.open("../NTDT/frmChiTietChungTu.aspx?ID=" + _ID, "", "dialogWidth:800px;dialogHeight:520px;help:0;status:0;", "_new");
        }
    </script>
    
    <style type="text/css">
        .container{width:100%; margin-bottom:30px}
        .dataView{padding-bottom:10px}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">

<div class="container" style="vertical-align:middle">
        <p class="pageTitle"> Tra cứu và xử lý message gửi tới TCT </p>
        <table cellpadding="2" cellspacing="1" border="0" width="80%" class="form_input">
            <tr>
                <td align="left" style="width:15%" class="form_label">Từ ngày</td>
                <td class="form_control" style="width:35%"><span style="display:block; width:40%; position:relative;vertical-align:middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                    onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span></td>
                <td align="left" style="width:15%" class="form_label">Đến ngày</td>
                <td class="form_control" style="width:35%">
                <span style="display:block; width:40%; position:relative; vertical-align:middle;">
                    <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                     onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span></td>
            </tr>
            <tr>
                <td class="form_label" align="left">
                    MSG_REFID
                </td>
                <td class="form_control">
                    <asp:TextBox ID="txtMsgId" runat="server" CssClass="inputflat" />
                </td>
                <td class="form_label" align="left">TRAN_CODE</td>
                <td class="form_control"><asp:TextBox runat="server" ID="txtTranCode" CssClass="inputflat" /></td>
            </tr>
            <tr>
                <td class="form_label" align="left">MST</td>
                <td class="form_control">
                    <asp:TextBox runat="server" ID="txtMST" CssClass="inputflat" />
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center" class="form_control">
                    <asp:Button runat="server" ID="btnSearch" Text="Tra cứu" />
                    <asp:Button runat="server" ID="btnProcess" Text="Xử lý" OnClientClick="return confirm('Bạn có muốn xử lý message đã chọn?')" />
                </td>
            </tr>
            
        </table>
        <div class="dataView">
            <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
                Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True" 
                PageSize="20">
                <PagerStyle Mode="NumericPages" />
                <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                <ItemStyle CssClass="grid_item" />
                <Columns>
                    <asp:TemplateColumn HeaderText="Chọn">
                        <ItemTemplate>
                            <asp:CheckBox runat="server" ID="chkSelectItem" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Số CTU">
                        <ItemTemplate>
                            <a href="#" onclick='jsChiTietCTU(<%#Eval("SO_CTU").ToString %>)'><%#Eval("SO_CTU").ToString%></a>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="id_gdich" HeaderText="ID giao dịch"></asp:BoundColumn>
                    <asp:BoundColumn DataField="tran_code" HeaderText="tran_code"></asp:BoundColumn>
                    <asp:BoundColumn DataField="msg_id" HeaderText="msg_id"></asp:BoundColumn>
                    <asp:BoundColumn DataField="msg_refid" HeaderText="msg_refid"></asp:BoundColumn>
                    <asp:BoundColumn DataField="send_date" HeaderText="send_date"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ngay_gdich" HeaderText="ngay_gdich"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Export">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" CommandArgument='<%# Eval("ID_GDICH") %>' Text="Export message" CommandName="ExportMsg" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
        });
    </script>		
</asp:Content>

