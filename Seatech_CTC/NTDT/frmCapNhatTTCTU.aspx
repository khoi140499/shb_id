﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmCapNhatTTCTU.aspx.vb" 
Inherits="pages_NTDT_frmCapNhatTTCTU" title="Cập nhật trạng thái chứng từ" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
        <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function () {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>	
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }

        function updateCtu(e) {
            $("#divBlack").css("display", "block");
            $("#divUp").css("display", "block");
            var row = e.closest("tr");
            $("#txtID").text($(row).find('td:eq(4)').text().trim());
            $("#txtGNT").text($(row).find('td:eq(2)').text().trim());
            $("#txtSCT").text($(row).find('td:eq(1)').text().trim());
            $("#txtTrangThai").text($(row).find('td:eq(8)').text().trim());
        }
        function jsUpdate() {
            var soct = $('#txtSCT').text();
            var ghichu = $('#txtGhiChu').val();
            var trangthai = $('#cboTrangthai').find(":selected").val();
            var tentrangthai = $('#cboTrangthai').find(":selected").text();
            var tentrangthaiold = $('#txtTrangThai').text();
            if (ghichu.length == 0) {
                alert('Vui lòng nhập ghi chú');
                return;
            }
            if (trangthai.length == 0) {
                alert('Vui lòng chọn trạng thái cập nhật');
                return;
            }
            $.ajax({
                type: "POST",
                url: "frmCapNhatTTCTU.aspx/UpdateCtu",
                cache: false,
                data: JSON.stringify({
                    "soct": soct,
                    "ghichu": ghichu,
                    "trangthai": trangthai,
                    "tentrangthai": tentrangthai,
                    "tentrangthaiold": tentrangthaiold
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status, xhr) {// success callback function
                    alert(data.d);
                    jsTraCuu();
                    updateCtuClose();
                },
                error: function (data, status, xhr) {
                    console.log(data);
                }
            });

        }
        function updateCtuClose() {
            $("#divBlack").css("display", "none");
            $("#divUp").css("display", "none");
        }
        function jsTraCuu() {
            var strTuNgay, strDenNgay, strMaNNT, strGNT, strSoCT;
            strTuNgay = document.getElementById("txtTuNgay").value;
            strDenNgay = document.getElementById("txtDenNgay").value;
            strGNT = document.getElementById("txtSoGNT").value;
            strMaNNT = document.getElementById("txtMaNNT").value;
            strSoCT = document.getElementById("txtSoCT").value;
            $.ajax({
                type: "POST",
                url: "frmCapNhatTTCTU.aspx/LoadCTList",
                cache: false,
                data: JSON.stringify({
                    "strTuNgay": strTuNgay,
                    "strDenNgay": strDenNgay,
                    "strMaNNT": strMaNNT,
                    "strGNT": strGNT,
                    "strSoCT": strSoCT
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status, xhr) {// success callback function
                    LoadCTList_Complete(data.d)
                },
                error: function (data, status, xhr) {
                    console.log(data);
                }
            });
        }

        function LoadCTList_Complete(data) {
            $('#grdDSCT_ToDay tbody').html('');
            if (data.length > 0) {
                var obj = JSON.parse(data);
                for (let i = 0; i < obj.length; i++) {
                    var tr = "<tr>" +
                        "    <td style='widtd:2%;text-align: center'>" +
                        "        " + obj[i].STT +
                        "    </td>" +
                        "    <td style='widtd:7%;text-align: center'>" +
                        "        " + obj[i].so_ct +
                        "    </td>" +
                        "    <td style='widtd:7%;text-align: left'>" +
                        "        " + obj[i].so_gnt +
                        "    </td>" +
                        "    <td style='widtd:7%;text-align: center'>" +
                        "         " + obj[i].ref_no +
                        "    </td>" +
                        "    <td style='widtd:7%;text-align: left'>" +
                        "        " + obj[i].mst +
                        "    </td>" +
                        "    <td style='widtd:10%'>" +
                        "         " + obj[i].ten_nnop +
                        "    </td>" +
                        "    <td style='widtd:7%;text-align: center'>" +
                        "        " + obj[i].ngay_chuyen +
                        "    </td>" +
                        "    <td style='widtd:7%;text-align: center'>" +
                        "        " + obj[i].ngay_hachtoan +
                        "    </td>" +
                        "    <td style='widtd:10%;text-align: left'>" +
                        "        " + obj[i].ten_tthai_ctu +
                        "    </td>" +
                        "    <td style='widtd:29%'>" +
                        "        " + obj[i].his +
                        "    </td>" +
                        "    <td style='widtd:7%'>" +
                        "    <input type=\"button\" class=\"ButtonCommand\" value=\"Cập nhật\" id=\"cmdCheck\" onclick=\"updateCtu(this)\" />" +
                        "    </td>" +
                        "</tr>";
                    $('#grdDSCT_ToDay tbody').append(tr);
                }

            }
            else {
                alert('Không có dữ liệu!')
            }
        }
        function getLichSu(gnt) {
            var kq = "";
            $.ajax({
                type: "POST",
                url: "frmCapNhatTTCTU.aspx/ReadHistory",
                cache: false,
                data: JSON.stringify({
                    "soct": gnt
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data, status, xhr) {// success callback function
                    kq = data.d;
                },
                error: function (data, status, xhr) {
                    console.log(data);
                }
            });
            return kq;
        }
    </script>
    <style type="text/css">
        .dataView{padding-bottom:10px}
        .style1
        {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <div id="divUp" style="display:none;width: 600px;height: 400px;position: absolute;box-shadow: 0px 0px 11px #000000;border-radius: 4px;z-index: 10001;background-color: white;margin: auto;inset: 0px;">
        <div style="padding: 5px 0 5px 0;padding: 10px;font-size: 16pt;color: #00335e;font-weight: bold;border-bottom: 1px solid #002c5f;text-align: left;cursor: move;">Cập nhật trạng thái chứng từ NTDT</div>
            <div>
                <div>
                    <div id="msgBox_SHKB_CONTENT" style="font-size: 12pt;margin: 10px 3px 0px 0px;display: inline-block;text-align: left;/* width: 99%; */padding: 5px;">
                        <table border="0" cellpadding="2" cellspacing="1" class="form_input" style="width:100%;font-size: 12pt;">
                            <tbody>
                                <tr class="grid-heading tdDetail">
                                    <td class="tdDetail" style="width:15%">
                                        <b>Mã số thuế</b> 
                                    </td>
                                    <td class="tdDetail" id="txtID" style="width:35%"></td>
                                </tr>
                                <tr>
                                    <td class="tdDetail" style="width:15%">
                                        <b>Số GNT</b> 
                                    </td>
                                    <td class="tdDetail" id="txtGNT" style="width:35%"></td>
                                </tr>
                               <tr>
                                    <td class="tdDetail">
                                        <b>Số chứng từ</b> 
                                    </td>
                                    <td class="tdDetail" id="txtSCT" style="width:35%"></td>
                                   </tr>
                                <tr>
                                    <td class="tdDetail" style="width:15%">
                                        <b>Trạng thái</b> 
                                    </td>
                                    <td class="tdDetail" id="txtTrangThai" style="width:35%"></td>
                                </tr>
                               <tr id="trCapNhat1">
                                    <td class="tdDetail">
                                        <b>Trạng thái cập nhật</b> 
                                    </td>
                                    <td class="tdDetail" id="td_UpdateTT">
                                        <select id="cboTrangthai">
                                            <option value="" selected="selected">Chọn trạng thái cập nhật</option>
                                            <option value="11">Nộp thuế thành công</option>
                                            <option value="13">Hạch toán không thành công</option>
                                            <option value="14">Hoàn trả giấy nộp tiền</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="trCapNhat2">
                                    <td class="tdDetail">
                                        <b>Ghi chú<span style="color:red">*</span></b> 
                                    </td>
                                    <td class="tdDetail">
                                        <textarea id="txtGhiChu" style="width:99%" rows="5"></textarea>
                                    </td>
                                </tr>
                            </tbody>
                        </table>           
                    </div>   
                </div>
                <div class="msgBoxButtons" id="msgBox_SHKB_ConfirmDivButton">
                    <input id="msgBox-btn-update" class="btn btn-tertiary" onclick="jsUpdate()" type="button" value="Cập nhật">
                    <input id="msgBox-btn-close" class="btn btn-primary" onclick="updateCtuClose()" type="button" name="Ok" value="Đóng">
                </div>
            </div>
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">CẬP NHẬT TRẠNG THÁI CHỨNG TỪ NTDT</asp:Label>
            </td>
        </tr>
        <tr>
            <td width='100%' align="left" valign="top" class='nav'>
                <div id="divTracuu">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width='90%' align="left" valign="top" >
                                <table width="100%" border="0" cellspacing="1" cellpadding="2" class="form_input">
                                    <tr align="left">
                                        <td width='15%' class="form_label">
                                            Từ ngày
                                        </td>
                                        <td width='35%' class="form_control">
                                            <span style="display:block; width:35%; position:relative;vertical-align:middle;">
                                                <input type="text" id='txtTuNgay' maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                                                onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                                            </span>
                                        </td>
                                        <td width='15%' class="form_label">
                                            Đến ngày
                                        </td>
                                        <td width='35%' class="form_control">
                                            <span style="display:block; width:35%; position:relative;vertical-align:middle;">
                                                <input type="text" id='txtDenNgay' maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                                                onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width='15%' align="left" valign="top" class="form_label">
                                            Số CT:
                                        </td>
                                        <td width='35%' align="left" valign="top" class="form_control">
                                            <input type="text" id='txtSoCT' class="inputflat" style="width:80%" />
                                        </td>
                                        <td width='15%' align="left" valign="top" class="form_label">
                                            Số GNT:
                                        </td>
                                        <td align="left" valign="top" class="form_control">
                                            <input type="text" id='txtSoGNT' class="inputflat" style="width:80%" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width='100' align="left" valign="top" class="form_label">
                                            Mã số thuế:
                                        </td>
                                        <td align="left" valign="top" class="form_control">
                                            <input type="text" id='txtMaNNT' class="inputflat" style="width: 80%" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr >
                            <td align="center">
                                <input style="margin-top: 15px;" type="button" class="ButtonCommand" value="Tra Cứu" id="cmdTC" onclick="jsTraCuu()" />
                                
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="divKQToDay" style="margin-top: 15px;">
                    <asp:Panel ID="Panel3" runat="server" Height="450px" Width="100%" ScrollBars="Vertical" BorderWidth="1px" BorderColor="Black">
                        <table class='grid_data' cellspacing='0' rules='all' border='0.5' id='grdDSCT_ToDay' style='width: 100%;
                            border-collapse: collapse;'>
                            <thead style="height: 37px;background-color: #5ec9cd;">
                                <tr>
                                    <th style='width:2%'>
                                        STT
                                    </th>
                                    <th style='width:7%'>
                                        Số chứng từ
                                    </th>
                                    <th style='width:7%'>
                                        Số GNT
                                    </th>
                                    <th style='width:7%'>
                                        Số giao dịch Core
                                    </th>
                                    <th style='width:7%'>
                                        Mã số thuế
                                    </th>
                                    <th style='width:10%'>
                                        Tên NNT
                                    </th>
                                    <th style='width:7%'>
                                        Ngày gửi
                                    </th>
                                    <th style='width:7%'>
                                        Ngày hạch toán
                                    </th>
                                    <th style='width:10%'>
                                        Trạng thái
                                    </th>
                                    <th style='width:29%'>
                                        Lịch sử cập nhật
                                    </th>
                                    <th style='width:7%'>
                    
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </asp:Panel>
                </div>
            </td>
        </tr>
    </table>

</asp:Content>

