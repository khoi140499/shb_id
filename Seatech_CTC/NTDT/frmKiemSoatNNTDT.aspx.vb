﻿Imports VBOracleLib
Imports System.Data
Imports Business.NTDT.NNTDT
Imports Business.Common.mdlCommon
Imports ETAX_GDT.ProcessMSG

Partial Class NTDT_frmKiemSoatNNTDT
    Inherits System.Web.UI.Page
    'Toanva create
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Load_TrangThai()
            Load_grid_DSNNTDT()
            hdfMA_NV.Value = Session.Item("User").ToString
        End If
        btnKS.Attributes.Add("onclick", "javascript:disableButton() ;" + Page.ClientScript.GetPostBackEventReference(btnKS, "").ToString())
        btnDuyetHuy.Attributes.Add("onclick", "javascript:disableButton() ;" + Page.ClientScript.GetPostBackEventReference(btnDuyetHuy, "").ToString())
        btnChuyenTra.Attributes.Add("onclick", "this.disabled=true;" + Page.ClientScript.GetPostBackEventReference(btnChuyenTra, "").ToString())
    End Sub
#Region "Data process"
    'Lấy dữ liệu cho gird người nộp thuế
    Private Sub Load_grid_DSNNTDT(Optional ByVal strTrangThai As String = "99")
        Dim ds As DataSet
        Dim objNNTDT As New buNNTDT
        Dim WhereClause As String = ""
        Try
            Dim ngay_lv_number As String = clsCTU.TCS_GetNgayLV()
            Dim ngay_lv As String = ConvertNumberToDate(ngay_lv_number).ToString("dd/MM/yyyy")
            Dim ma_cn As String = Session.Item("MA_CN_USER_LOGON").ToString
            If drpTrangThai.SelectedIndex <> 0 Then
                If strTrangThai = "01" Or strTrangThai = "02" Or strTrangThai = "08" Or strTrangThai = "09" Then
                    WhereClause += " and  a.trang_thai='" & strTrangThai & "'"
                ElseIf strTrangThai = "03" Then
                    WhereClause += " and  a.trang_thai='" & strTrangThai & "' and TO_CHAR(a.ngay_ks,'dd/MM/yyyy')='" & ngay_lv & "'"
                Else
                    WhereClause += " and  a.trang_thai='" & strTrangThai & "' and a.NGAY_TAO='" & ngay_lv_number & "'"
                End If

            Else
                WhereClause += " and (a.trang_thai='01' or a.trang_thai='02' or a.trang_thai='08' or a.trang_thai='09' or(( a.trang_thai='00' or a.trang_thai='04'or a.trang_thai='05' or a.trang_thai='06'or a.trang_thai='07') and a.NGAY_TAO='" & ngay_lv_number & "' ) or (a.trang_thai='03'  and TO_CHAR(a.ngay_ks,'dd/MM/yyyy')='" & ngay_lv & "'))"
            End If
            WhereClause += " and a.Ma_CN='" & ma_cn & "'"

            'ds = objNNTDT.GetDataSet(WhereClause)
            ds = objNNTDT.GetData_Header(WhereClause)
            'If Not IsEmptyDataSet(ds) Then
            grdDSCT.DataSource = ds.Tables(0)
            grdDSCT.DataBind()
            'Else
            'clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            'End If
            ' Valid_Button_Enable(strTrangThai)
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách người nộp thuế. Lỗi: " & ex.Message)
        End Try
    End Sub
    'Lấy dữ liệu cho dataset
    Private Function Load_Data_DSNNTDT(Optional ByVal strTrangThai As String = "99") As DataSet
        Dim ds As New DataSet
        Dim objNNTDT As New buNNTDT
        Dim WhereClause As String = ""
        Try
            If drpTrangThai.SelectedIndex <> 0 Then
                WhereClause = strTrangThai
            End If
            Dim ngay_lv_number As String = clsCTU.TCS_GetNgayLV()
            Dim ngay_lv As String = ConvertNumberToDate(ngay_lv_number).ToString("dd/MM/yyyy")
            Dim ma_cn As String = Session.Item("MA_CN_USER_LOGON").ToString
            If drpTrangThai.SelectedIndex <> 0 Then
                If strTrangThai = "01" Or strTrangThai = "02" Or strTrangThai = "08" Or strTrangThai = "09" Then
                    WhereClause += " and  trang_thai='" & strTrangThai & "'"
                ElseIf strTrangThai = "03" Then
                    WhereClause += " and  trang_thai='" & strTrangThai & "' and TO_CHAR(ngay_ks,'dd/MM/yyyy')='" & ngay_lv & "'"
                Else
                    WhereClause += " and  trang_thai='" & strTrangThai & "' and NGAY_TAO='" & ngay_lv_number & "'"
                End If

            Else
                WhereClause += " and (trang_thai='01' or trang_thai='02' or trang_thai='08' or trang_thai='09' or(( trang_thai='00' or trang_thai='04'or trang_thai='05'or trang_thai='06'or trang_thai='07') and NGAY_TAO='" & ngay_lv_number & "' ) or (trang_thai='03'  and TO_CHAR(ngay_ks,'dd/MM/yyyy')='" & ngay_lv & "'))"
            End If
            WhereClause += " and Ma_CN='" & ma_cn & "'"

            ds = objNNTDT.GetDataSet(WhereClause)
            Return ds
        Catch ex As Exception
            Return ds
        End Try
    End Function
    'Lấy dữ liệu từ DB theo NNTDT_ID
    Private Function Get_Data_DB(ByRef item As infNNTDT, ByRef strErrMsg As String) As Boolean
        Try
            Dim obj As New daNNTDT
            item = obj.Load(item.NNTDT_ID)
            Return True
        Catch ex As Exception
            strErrMsg = "Không lấy được thông tin người nộp thuế. Lỗi: " & ex.Message
            Return False
        End Try
    End Function
    'Lấy dữ liệu cho form
    Private Function Get_DataToForm(ByRef item As infNNTDT, ByRef strErrMsg As String) As Boolean
        Try
            txtMST.Text = item.MST
            txtTenNNT.Text = item.TEN_NNT
            txtDCNNT.Text = item.DIACHI_NNT
            txtMaCQT.Text = item.MA_CQT
            txtEmail.Text = item.EMAIL_NNT
            txtSDT.Text = item.SDT_NNT
            txtNguoiLH.Text = item.TEN_LHE_NTHUE
            txtSoSerial.Text = item.SERIAL_CERT_NTHUE
            txtNhaCC.Text = item.ISSUER_CERT_NTHUE
            txtMaNH.Text = item.MA_NHANG
            txtTenNH.Text = item.TEN_NHANG
            txtMaTVAN.Text = item.VAN_ID
            txtTenTVAN.Text = item.TEN_TVAN
            txtNgayGui.Text = item.NGAY_GUI
            txtTenTKNH.Text = item.TENTK_NHANG
            hdfNNTDT_ID.Value = item.NNTDT_ID
            hdrTRANG_THAI.Value = item.TRANG_THAI
            txtLyDo.Text = item.LDO_TCHOI
            hdfSUBJECT_CERT_NTHUE.Value = item.SUBJECT_CERT_NTHUE
            hdfMA_GDICH.Value = item.MA_GDICH
            hdfUpdate_TT.Value = item.UPDATE_TT
            'Enable các nút trên màn hình hoàn thiện theo trạng thái
            'Valid_Button_Enable(item.TRANG_THAI)
            Return True
        Catch ex As Exception
            strErrMsg = "Lỗi không đưa được thông tin lên form. Lỗi: " & ex.Message
            Return False
        End Try
    End Function
    'Gán dữ liệu từ form vài obj
    Private Sub SetDataToDB(ByRef item As infNNTDT)
        item.MST = txtMST.Text
        item.TEN_NNT = txtTenNNT.Text
        item.DIACHI_NNT = txtDCNNT.Text
        item.MA_CQT = txtMaCQT.Text
        item.EMAIL_NNT = txtEmail.Text
        item.SDT_NNT = txtSDT.Text
        item.TEN_LHE_NTHUE = txtNguoiLH.Text
        item.SERIAL_CERT_NTHUE = txtSoSerial.Text
        item.ISSUER_CERT_NTHUE = txtNhaCC.Text
        item.MA_NHANG = txtMaNH.Text
        item.TEN_NHANG = txtTenNH.Text
        item.VAN_ID = txtMaTVAN.Text
        item.TEN_TVAN = txtTenTVAN.Text
        item.NGAY_GUI = txtNgayGui.Text
        item.TENTK_NHANG = txtTenTKNH.Text
        item.NNTDT_ID = hdfNNTDT_ID.Value
        item.NV_TT = hdfMA_NV.Value
        item.LDO_TCHOI = txtLyDo.Text.Trim
        item.SUBJECT_CERT_NTHUE = hdfSUBJECT_CERT_NTHUE.Value
        item.STK_NHANG = GetSo_TK_NH(hdfNNTDT_ID.Value, txtMST.Text.Trim)
        item.MA_GDICH = hdfMA_GDICH.Value
        item.UPDATE_TT = hdfUpdate_TT.Value
        'item.TRANG_THAI = hdrTRANG_THAI.Value
    End Sub
    'Lấy thông tin tài khoản ngân hàng của khách hàng
    Private Sub Load_Grid_DS_TKNH()
        Dim ds As New DataSet
        Try
            Dim sql = "Select * from ntdt_map_mst_tknh where NNTDT_ID=" & hdfNNTDT_ID.Value & " and MST='" & txtMST.Text.Trim & "'"
            ds = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            grdTKNH.DataSource = ds
            grdTKNH.DataBind()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách tài khoản ngân hàng của khách hàng. Lỗi: " & ex.Message)
        End Try
    End Sub
    'Ẩn hiện button
    Private Sub Valid_Button_Enable(ByVal strTrangThai As String)
        Select Case strTrangThai
            Case "00" 'đăng ký mới
                btnKS.Enabled = False
                btnDuyetHuy.Enabled = False
                btnChuyenTra.Enabled = False
            Case "01" 'chờ ks
                btnKS.Enabled = True
                btnKS.Text = "Kiểm soát"
                btnDuyetHuy.Enabled = False
                btnChuyenTra.Enabled = True
            Case "02" 'Hủy - chờ duyệt
                btnKS.Enabled = False
                btnDuyetHuy.Enabled = True
                btnDuyetHuy.Text = "Duyệt hủy"
                btnChuyenTra.Enabled = True
            Case "03" 'thành công
                btnKS.Enabled = False
                btnDuyetHuy.Enabled = False
                btnChuyenTra.Enabled = False
            Case "04" 'Hủy
                btnKS.Enabled = False
                btnDuyetHuy.Enabled = False
                btnChuyenTra.Enabled = False
            Case "05" 'điều chỉnh
                btnKS.Enabled = False
                btnDuyetHuy.Enabled = False
                btnChuyenTra.Enabled = False
            Case "06" 'ngừng
                btnKS.Enabled = False
                btnDuyetHuy.Enabled = False
                btnChuyenTra.Enabled = False
            Case "07" 'chuyển trả
                btnKS.Enabled = False
                btnDuyetHuy.Enabled = False
                btnChuyenTra.Enabled = False
            Case "08" 'Thành công - chờ xác nhận từ tct
                btnKS.Enabled = True
                btnKS.Text = "Kiểm soát lại"
                btnDuyetHuy.Enabled = False
                btnChuyenTra.Enabled = False
            Case "09" 'Hủy - chờ xác nhận từ tct
                btnKS.Enabled = False
                btnDuyetHuy.Enabled = True
                btnDuyetHuy.Text = "Duyệt hủy lại"
                btnChuyenTra.Enabled = False
        End Select
    End Sub
#End Region
#Region "Control button"
    'Button: Kiểm soát
    Protected Sub btnKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnKS.Click
        Try
            Valid_Button_Enable("00")
            Dim strErrMsg As String = ""
            Dim objCur As New infNNTDT
            objCur.NNTDT_ID = hdfNNTDT_ID.Value
            'Kiểm tra trạng thái của thông tin hiện tại có thay đổi gì không
            If Get_Data_DB(objCur, strErrMsg) Then
                If objCur.TRANG_THAI <> hdrTRANG_THAI.Value Then
                    clsCommon.ShowMessageBox(Me, "Kiểm soát không thành công. Thông tin này đã thay đổi")
                    Exit Sub
                End If
            Else
                clsCommon.ShowMessageBox(Me, "Kiểm soát không thành công." & strErrMsg)
                Exit Sub
            End If
            Dim objUpdate As New infNNTDT
            SetDataToDB(objUpdate)
            objUpdate.NNTDT_ID = hdfNNTDT_ID.Value
            objUpdate.NV_TT = hdfMA_NV.Value
            'Trạng thái Kiểm soát gửi thành công và chờ xác nhận từ tct= 08
            objUpdate.TRANG_THAI = "08"
            Dim sendMSG As New ETAX_GDT.ProcessThongBaoDangKyNNTDT_NH_TTXL
            Dim sendMSG_TT As New ETAX_GDT.ProcessThayDoiTTTK_NH_TTXL
            Dim error_code As String = ""
            Dim error_msg As String = ""
            'sửa thông tin
            If hdfUpdate_TT.Value = "00" Then
                sendMSG_TT.sendmsg06013(objUpdate, error_code, error_msg)
            Else
                'nhập mới vào hệ thống
                sendMSG.sendMSG06007(objUpdate, error_code, error_msg)
            End If

            If error_code = "00" Then
                Dim bu As New buNNTDT
                If Not bu.Update_TrangThai(objUpdate) Then
                    clsCommon.ShowMessageBox(Me, "Kiểm soát không thành công")
                Else
                    'Load_grid_DSNNTDT(drpTrangThai.SelectedValue)
                    'Valid_Button_Enable(objUpdate.TRANG_THAI)
                    clsCommon.ShowMessageBox(Me, "Kiểm soát thành công chờ phản hồi của TCT")
                End If
            Else
                clsCommon.ShowMessageBox(Me, "Kiểm soát không thành công. Lỗi: " & error_msg)
            End If
            hdrTRANG_THAI.Value = objUpdate.TRANG_THAI
            Load_grid_DSNNTDT()

        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Kiểm soát không thành công. Lỗi:" & ex.Message)
        End Try
    End Sub
    'Button: Duyệt hủy
    Protected Sub btnDuyetHuy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuyetHuy.Click
        Try
            Valid_Button_Enable("00")
            Dim strErrMsg As String = ""
            Dim objCur As New infNNTDT
            objCur.NNTDT_ID = hdfNNTDT_ID.Value
            'Kiểm tra trạng thái của thông tin hiện tại có thay đổi gì không
            If Get_Data_DB(objCur, strErrMsg) Then
                If objCur.TRANG_THAI <> hdrTRANG_THAI.Value Then
                    clsCommon.ShowMessageBox(Me, "Duyệt hủy không thành công. Thông tin này đã thay đổi")
                    Exit Sub
                End If
            Else
                clsCommon.ShowMessageBox(Me, "Duyệt hủy không thành công." & strErrMsg)
                Exit Sub
            End If
            Dim objUpdate As New infNNTDT
            SetDataToDB(objUpdate)
            objUpdate.NNTDT_ID = hdfNNTDT_ID.Value
            objUpdate.NV_TT = hdfMA_NV.Value
            'Trạng thái Hủy - chờ xác nhận từ tct
            objUpdate.TRANG_THAI = "09"
            Dim sendMSG As New ETAX_GDT.ProcessThongBaoDangKyNNTDT_NH_TTXL
            Dim error_code As String = ""
            Dim error_msg As String = ""
            If hdfMA_GDICH.Value.Length > 0 Then

            End If
            sendMSG.sendMSG06007(objUpdate, error_code, error_msg)
            If error_code = "00" Then
                Dim bu As New buNNTDT
                If Not bu.Update_TrangThai(objUpdate) Then
                    clsCommon.ShowMessageBox(Me, "Duyệt hủy không thành công")
                Else
                    ' Load_grid_DSNNTDT(drpTrangThai.SelectedValue)
                    ' Valid_Button_Enable(objUpdate.TRANG_THAI)
                    clsCommon.ShowMessageBox(Me, "Duyệt hủy thành công- chờ xác nhận của TCT.")
                End If
            Else
                clsCommon.ShowMessageBox(Me, "Duyệt hủy không thành công. Lỗi: " & error_msg)
            End If
            hdrTRANG_THAI.Value = objUpdate.TRANG_THAI
            Load_grid_DSNNTDT()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Duyệt hủy không thành công. Lỗi:" & ex.Message)
        End Try
    End Sub
    'Click chọn 1 người NNTDT
    Protected Sub btnSelectCT_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Load_grid_DSNNTDT()
            Dim btnSelectCT As LinkButton = CType(sender, LinkButton)
            Dim v_strRetArgument As String() = btnSelectCT.CommandArgument.ToString.Split(",")
            If (v_strRetArgument.Length > 0) Then
                Dim v_strNNTDT_ID As String = v_strRetArgument(0)
                'Load thông tin chi tiết khi người dùng chọn 1 MST cụ thể
                If (Not v_strNNTDT_ID Is Nothing) Then
                    Dim strErrMsg As String = ""
                    Dim obj As New infNNTDT
                    obj.NNTDT_ID = v_strNNTDT_ID
                    If Get_Data_DB(obj, strErrMsg) Then
                        If Not Get_DataToForm(obj, strErrMsg) Then
                            clsCommon.ShowMessageBox(Me, strErrMsg)
                        End If
                        Load_Grid_DS_TKNH()
                    Else
                        clsCommon.ShowMessageBox(Me, strErrMsg)
                    End If
                    Valid_Button_Enable(obj.TRANG_THAI)
                End If
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được thông tin. Lỗi: " & ex.Message)
        End Try
    End Sub
    'Chuyển trả
    Protected Sub btnChuyenTra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChuyenTra.Click
        Try
            If txtLyDo.Text.Trim = "" Then
                clsCommon.ShowMessageBox(Me, "Bạn cần nhập vào lý do chuyển trả")
                txtLyDo.Focus()
                Exit Sub
            End If
            Dim strErrMsg As String = ""
            Dim objCur As New infNNTDT
            objCur.NNTDT_ID = hdfNNTDT_ID.Value
            'Kiểm tra trạng thái của thông tin hiện tại có thay đổi gì không
            If Get_Data_DB(objCur, strErrMsg) Then
                If objCur.TRANG_THAI <> hdrTRANG_THAI.Value Then
                    clsCommon.ShowMessageBox(Me, "Chuyển trả không thành công. Thông tin này đã thay đổi")
                    Exit Sub
                End If
            Else
                clsCommon.ShowMessageBox(Me, "Chuyển trả không thành công." & strErrMsg)
                Exit Sub
            End If
            Dim objUpdate As New infNNTDT
            objUpdate.NNTDT_ID = hdfNNTDT_ID.Value
            objUpdate.NV_TT = hdfMA_NV.Value
            objUpdate.LDO_TCHOI = txtLyDo.Text.Trim
            'Trạng thái chuyển trả = 07
            objUpdate.TRANG_THAI = "07"
            Dim bu As New buNNTDT
            If Not bu.Update_TrangThai(objUpdate) Then
                clsCommon.ShowMessageBox(Me, "Chuyển trả không thành công")
            Else
                Load_grid_DSNNTDT(drpTrangThai.SelectedValue)
                Valid_Button_Enable(objUpdate.TRANG_THAI)
                clsCommon.ShowMessageBox(Me, "Chuyển trả thành công")
            End If
            hdrTRANG_THAI.Value = objUpdate.TRANG_THAI
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Chuyển trả không thành công. Lỗi: " & ex.Message)
        End Try
    End Sub
    'Thoát
    Protected Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Response.Redirect("~/pages/frmHomeNV.aspx", False)
    End Sub
    'Làm mới DS_NNT
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Load_grid_DSNNTDT(drpTrangThai.SelectedValue)
    End Sub
    'Chọn drop trạng thái NNTDT
    Protected Sub drpTrangThai_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpTrangThai.SelectedIndexChanged
        Load_grid_DSNNTDT(drpTrangThai.SelectedValue)
        grdDSCT.PageIndex = 0
    End Sub
    'Chuyển trang DS_NNTDT
    Protected Sub grdDSCT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdDSCT.PageIndexChanging
        Dim v_ds As DataSet = Load_Data_DSNNTDT(drpTrangThai.SelectedValue)
        grdDSCT.PageIndex = e.NewPageIndex
        grdDSCT.DataSource = v_ds
        grdDSCT.DataBind()
    End Sub

    Private Function GetSo_TK_NH(ByVal NNTDT_ID As String, ByVal MST As String) As String
        Dim So_TK_NH As String = ""
        Dim sql As String = "Select * from NTDT_MAP_MST_TKNH where NNTDT_ID=" & NNTDT_ID & " AND MST='" & MST & "'"
        Dim dt As DataTable = DataAccess.ExecuteToTable(sql)
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            For i As Integer = 0 To dt.Rows.Count - 1
                So_TK_NH = So_TK_NH & dt.Rows(i)("SO_TK_NH").ToString & ";"
            Next
            So_TK_NH = So_TK_NH.Substring(0, So_TK_NH.Length - 1)
        End If
        Return So_TK_NH
    End Function
    Private Sub Load_TrangThai()
        Try
            Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_NTDT WHERE GHICHU='NNT'"

            Dim cnDiemThu As New DataAccess
            Dim dsDiemThu As DataSet

            dsDiemThu = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)

            drpTrangThai.DataSource = dsDiemThu
            drpTrangThai.DataTextField = "MOTA"
            drpTrangThai.DataValueField = "TRANG_THAI"
            drpTrangThai.DataBind()
            drpTrangThai.Items.Insert(0, "Tất cả")

            'Dim dr As IDataReader
            'dr = DataAccess.ExecuteDataReader(sql, CommandType.Text)
            'dr.Read()
            'drpTrangThai.DataSource = dr
            'drpTrangThai.DataTextField = "MOTA"
            'drpTrangThai.DataValueField = "TRANG_THAI"
            'drpTrangThai.DataBind()
            'dr.Close()
            'drpTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái NNTDT. Lỗi: " & ex.Message)
        End Try
        'Try
        '    Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_NTDT WHERE GHICHU='NNT' and trang_thai <> '06'"
        '    Dim dr As OracleClient.OracleDataReader
        '    dr = DataAccess.ExecuteDataReader(sql, CommandType.Text)
        '    drpTrangThai.DataSource = dr
        '    drpTrangThai.DataTextField = "MOTA"
        '    drpTrangThai.DataValueField = "TRANG_THAI"
        '    drpTrangThai.DataBind()
        '    dr.Close()
        '    drpTrangThai.Items.Insert(0, "Tất cả")
        'Catch ex As Exception
        '    clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái NNTDT. Lỗi: " & ex.Message)
        'End Try
    End Sub
#End Region
End Class
