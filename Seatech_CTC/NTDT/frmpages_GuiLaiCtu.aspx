﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmpages_GuiLaiCtu.aspx.vb"
     Inherits="pages_GuiLaiCtu" title="Gửi lại chứng từ" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
    </script>
    <style type="text/css">
        .container{width:100%;padding:10px; margin-top:10px; margin-bottom:30px}
        .dataView{padding-bottom:10px}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle"> Gửi lại thông báo chứng từ </p>
    <div class="container">
        <table cellpadding="0" cellspacing="10" border="0"  width="100%">
            <tr>
                <td style="width:100%" align="center">
                    <table cellpadding="3" cellspacing="1" width="70%" class="form_input">
                        <tr>
                            <td class="form_label">Mã số thuế</td>
                            <td class="form_control">
                                <asp:TextBox ID="txtMST" runat="server" CssClass="inputflat"></asp:TextBox>
                            </td>
                            <td class="form_label">Số chứng từ</td>
                            <td class="form_control">
                                <asp:TextBox ID="txtSO_CT" runat="server" CssClass="inputflat"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="form_label">Số GNT</td>
                            <td class="form_control">
                                <asp:TextBox ID="txtGNT" runat="server" CssClass="inputflat"></asp:TextBox>
                            </td>
                            <td class="form_label"></td>
                            <td class="form_control">
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4" class="form_control">
                                <asp:Button ID="btnTimKiem" runat="server" Text="Tìm kiếm" Width="100px"/> &nbsp;&nbsp;
                                <asp:Button ID="btnGuiLai" runat="server" Text="Gửi lại" Width="100px"/> &nbsp;&nbsp;
                                <asp:Button ID="btnGIP" runat="server" Text="Gửi lại GIP" Width="100px"/> &nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr><td colspan="4">&nbsp;</td></tr>
        </table>
    </div>
    <div class="dataView">
        <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
        Width="100%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True">
        <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
        <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
        <ItemStyle CssClass="grid_item" />
        <Columns>
            
            <asp:TemplateColumn HeaderText="STT" HeaderStyle-Width="10%">
                <ItemTemplate>
                    <%#(Container.ItemIndex + 1)%>
                </ItemTemplate>
                <HeaderStyle Width="5%"></HeaderStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Chọn" HeaderStyle-Width="10%">
                <ItemTemplate>
                    <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                </ItemTemplate>
                <HeaderStyle></HeaderStyle>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="so_gnt" HeaderText="Số GNT">
                <HeaderStyle></HeaderStyle>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:BoundColumn>
             <asp:BoundColumn DataField="so_ctu" HeaderText="Số chứng từ">
                <HeaderStyle></HeaderStyle>
                <ItemStyle HorizontalAlign="Left"></ItemStyle>
            </asp:BoundColumn>
            <asp:BoundColumn DataField="MST" HeaderText="Mã số thuế">
                <HeaderStyle></HeaderStyle>
                <ItemStyle HorizontalAlign="center"></ItemStyle>
            </asp:BoundColumn>
            <asp:BoundColumn DataField="tt_chuyen" HeaderText="Số lần gửi lại">
                <HeaderStyle></HeaderStyle>
                <ItemStyle HorizontalAlign="center"></ItemStyle>
            </asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Trạng thái GIP" HeaderStyle-Width="10%">
                <ItemTemplate>
                    <%#LayTTGip(Eval("tt_gip").ToString)%>
                </ItemTemplate>
                <HeaderStyle></HeaderStyle>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Ngày gửi lại" HeaderStyle-Width="10%">
                <ItemTemplate>
                    <%#Eval("ngay_chuyen")%>
                </ItemTemplate>
                <HeaderStyle></HeaderStyle>
            </asp:TemplateColumn>
        </Columns>
        <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
            Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
            Font-Strikeout="False" Font-Underline="False" ForeColor="#003399" VerticalAlign="Middle">
        </PagerStyle>
    </asp:DataGrid><br />
        
    </div>
    <!-- required plugins -->
    <script src="../../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>		
</asp:Content>

