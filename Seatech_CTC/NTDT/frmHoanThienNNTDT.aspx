﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmHoanThienNNTDT.aspx.vb" 
    Inherits="NTDT_frmHoanThienNNTDT" title="Hoàn thiện thông tin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
        function deleteConfirm(pubid) {
            var result = confirm('Bạn có thực sự muốn xóa tài khoản ' + pubid + ' ?');
            if (result) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <style type="text/css">
        .txtHoanThien{width:80%;}
        .container{width:100%;padding-bottom:50px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle"> Chi tiết đăng ký sử dụng dịch vụ NTDT </p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
            <tr>
                <td align="left" style="width:25%" class="form_label">Mã số thuế <span class="requiredField">(*)</span></td>
                <td class="form_control"><asp:TextBox ID="txtMST" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="false"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên NNT <span class="requiredField">(*)</span></td>
                <td class="form_control"><asp:TextBox ID="txtTenNNT" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Địa chỉ NNT</td>
                <td class="form_control"><asp:TextBox ID="txtDCNNT" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Mã cơ quan thu <span class="requiredField">(*)</span></td>
                <td class="form_control"><asp:TextBox ID="txtMaCQT" runat="server" CssClass="inputflat txtHoanThien" MaxLength="5" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Email</td>
                <td class="form_control"><asp:TextBox ID="txtEmail" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số điện thoại</td>
                <td class="form_control"><asp:TextBox ID="txtSDT" runat="server" CssClass="inputflat txtHoanThien" MaxLength="15" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên người liên hệ</td>
                <td class="form_control"><asp:TextBox ID="txtNguoiLH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số serial chứng thư số <span class="requiredField">(*)</span></td>
                <td class="form_control"><asp:TextBox ID="txtSoSerial" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="false" MaxLength="200"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Nhà cung cấp chứng thư số</td>
                <td class="form_control"><asp:TextBox ID="txtNhaCC" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Mã ngân hàng</td>
                <td class="form_control"><asp:TextBox ID="txtMaNH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="10" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên ngân hàng</td>
                <td class="form_control"><asp:TextBox ID="txtTenNH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Mã TVAN</td>
                <td class="form_control"><asp:TextBox ID="txtMaTVAN" runat="server" CssClass="inputflat txtHoanThien" MaxLength="6" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên TVAN</td>
                <td class="form_control"><asp:TextBox ID="txtTenTVAN" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Ngày gửi</td>
                <td class="form_control"><asp:TextBox ID="txtNgayGui" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" ></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Check số tài khoản</td>
                <td class="form_control"><asp:TextBox ID="txtSoTKNH" runat="server" style="width:60%;" CssClass="inputflat" ></asp:TextBox>
                <asp:Button ID="btnCheckTK" runat="server" Text="Check TK" />
                <asp:Button ID="btnAdd" runat="server" Text="Lưu" Enabled="false"/></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên tài khoản</td>
                <td class="form_control"><asp:TextBox ID="txtTenTKNH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số tài khoản sử dụng NTDT <span class="requiredField">(*)</span></td>
                <td>
                    <asp:GridView ID="grdTKNH" runat="server"  Width="80%" DataKeyNames="ID" ShowHeader="false"
                        AutoGenerateColumns="False" >
                        <Columns>
                            <asp:BoundField DataField="ID" Visible="false"/>
                            <asp:TemplateField HeaderText="Tài khoản ngân hàng">
                                <EditItemTemplate><asp:TextBox ID="txtSoTKNH_Edit" runat="server" Text='<%#Eval("SO_TK_NH") %>'></asp:TextBox></EditItemTemplate>
                                <ItemTemplate><asp:Label ID="lblSoTKNH" runat="server" Text='<%#Eval("SO_TK_NH") %>'></asp:Label></ItemTemplate>
                                <ItemStyle Width="80%" CssClass="form_label"/>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <%--<EditItemTemplate>
                                    <asp:Button ID="ButtonUpdate" runat="server" CommandName="Update"  Text="Lưu"  />
                                    <asp:Button ID="ButtonCancel" runat="server" CommandName="Cancel"  Text="Không lưu" />
                                </EditItemTemplate>--%>
                                <ItemTemplate>
                                    <%--<asp:Button ID="ButtonEdit" runat="server" CommandName="Edit"  Text="Sửa"  />--%>
                                    <asp:Button ID="ButtonDelete" runat="server" CommandName="Delete"  Text="Xóa" Font-Bold="true" />
                                </ItemTemplate>
                                <ItemStyle CssClass="form_label"/>
                             </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Lý do hủy/chuyển trả</td>
                <td class="form_control"><asp:TextBox ID="txtLyDo" runat="server" CssClass="inputflat txtHoanThien" ></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;
                <asp:HiddenField ID="hdfNNTDT_ID" runat="server" />
                <asp:HiddenField ID="hdrTRANG_THAI" runat="server" />
                <asp:HiddenField ID="hdfMA_CN" runat="server" />
                <asp:HiddenField ID="hdfMA_NV" runat="server" />
                <asp:HiddenField ID="hdrMax_SoTKNH" runat="server" /></td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:Button ID="btnChuyenKS" runat="server" Text="Ghi & Chuyển KS" Width="120px"/>&nbsp;&nbsp;
                    <asp:Button ID="btnDieuChinh" runat="server" Text="Điều chỉnh" Width="90px"/>&nbsp;&nbsp;
                    <asp:Button ID="btnHuy" runat="server" Text="Hủy" Width="90px"/>&nbsp;&nbsp;
                    <asp:Button ID="btnIn" runat="server" Text="In" Width="90px" Visible="false"/>
                    <asp:Button ID="btnExit" runat="server" Text="Thoát" Width="90px" />
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
        </table>
    </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtNgayGui").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>		
</asp:Content>

