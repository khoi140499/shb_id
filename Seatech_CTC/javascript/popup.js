﻿function ReverseString()
{
         var originalString = document.getElementById('txtMaDanhMuc').value;
         var reversedString = Reverse(originalString);
         RetrieveControl();
         // to handle in IE 7.0
         if (window.showModalDialog)
         {              
              window.returnValue = reversedString;
              window.close();
         }
         // to handle in Firefox
         else
         {
              if ((window.opener != null) && (!window.opener.closed)) 
              {               
                // Access the control.        
                window.opener.document.getElementById(ctr[1]).value = reversedString; 
              }
              window.close();
         }
}
 
function Reverse(str)
{
   var revStr = ""; 
   for (i=str.length - 1 ; i > - 1 ; i--)
   { 
      revStr += str.substr(i,1); 
   } 
   return revStr;
}
 
function RetrieveControl()
{
        //Retrieve the query string 
        queryStr = window.location.search.substring(1); 
        // Seperate the control and its value
        ctrlArray = queryStr.split("&"); 
        ctrl1 = ctrlArray[0]; 
        //Retrieve the control passed via querystring 
        ctr = ctrl1.split("=");
}