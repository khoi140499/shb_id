﻿/**
* jQuery内没有的验证
*/
$(document).ready(function(){
	//字符验证(中文,英文字母,数字,下划线)
	jQuery.validator.addMethod("userName",function(value,element){
		return this.optional(element) || /^[\u0391-\uFFE5\w]+$/.test(value);
	},"please enter a valid userName");
	
	//一个中文两个字符
	jQuery.validator.addMethod("byteRangeLength",function(value,element,param){
		var length = value.length;
		for(var i = 0; i < value.length; i++){
			if(value.charCodeAt(i) > 127){
				length ++;
			}
		}
		return this.optional(element) || (length >= param[0] && length <= param[1]);
	},"please enter a valid length");
	
	jQuery.validator.addMethod("password",function(value,element){
		var tel=/[0-9]{6}/i;
		return this.option(element)||(tel.test(value));
	},'Please enter a valid password');
	//验证邮政编码
	jQuery.validator.addMethod("isZipCode",function(value,element){
		var tel=/^[0-9]{6}$/;
		return this.optional(element) || (tel.test(value));
	},"Please enter a valid zipcode");
	
	//验证数值型输入框保留整数数位、小数数位
	// @author zhongyuyan
	jQuery.validator.addMethod("validNumber",function(value,element,params){
		var _validNum = new RegExp("^\\d{1," + params[0] + "}(\\.\\d{1," + params[1] + "})?$",'i');
		return this.optional(element) || _validNum.test(value);
	},"Please enter a valid validNumber");
  //chiness idcardno check   
/*	 jQuery.validator.addMethod("isIdCardNo", function(value, element) {
	   return this.optional(element) || isIdCardNo(value);   
	 }, "please enter a valid idcardno");*/
   // 中文字两个字节
	jQuery.validator.addMethod("byteRangeLength", function(value, element, param) {
	var length = value.length;
	for(var i = 0; i < value.length; i++){
	if(value.charCodeAt(i) > 127){
	length++;
	}
	}
	return this.optional(element) || ( length >= param[0] && length <= param[1] );    
	}, $.validator.format("请确保输入的值在{0}-{1}个字节之间(一个中文字算2个字节)"));
	/**
	* 设置默认的错误信息显示方法
	*/
	jQuery.validator.setDefaults({
		errorPlacement: function(error, element) {
						var _parent = element.parent('td');
						var _br = _parent.find('br');
						if(_br.size() == 0){
							$('<br/><div></div>').appendTo(_parent); 
							error.appendTo(_parent.find('br').next('div')); 
						}
		}
	});
});
/**
 * 身份证号码验证
 *
 */
/*
function isIdCardNo(num) {

 var factorArr = new Array(7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2,1);
 var parityBit=new Array("1","0","X","9","8","7","6","5","4","3","2");
 var varArray = new Array();
 var intValue;
 var lngProduct = 0;
 var intCheckDigit;
 var intStrLen = num.length;
 var idNumber = num;
   // initialize
     if ((intStrLen != 15) && (intStrLen != 18)) {
         return false;
     }
     // check and set value
     for(i=0;i<intStrLen;i++) {
         varArray[i] = idNumber.charAt(i);
         if ((varArray[i] < '0' || varArray[i] > '9') && (i != 17)) {
             return false;
         } else if (i < 17) {
             varArray[i] = varArray[i] * factorArr[i];
         }
     }
     
     if (intStrLen == 18) {
         //check date
         var date8 = idNumber.substring(6,14);
         if (isDate8(date8) == false) {
            return false;
         }
         // calculate the sum of the products
         for(i=0;i<17;i++) {
             lngProduct = lngProduct + varArray[i];
         }
         // calculate the check digit
         intCheckDigit = parityBit[lngProduct % 11];
         // check last digit
         if (varArray[17] != intCheckDigit) {
             return false;
         }
     }
     else{        //length is 15
         //check date
         var date6 = idNumber.substring(6,12);
         if (isDate6(date6) == false) {
             return false;
         }
     }
     return true;
     
 }*/
	