/**
* 一个简单的tab 实现
* 在cms网站使用
*/
/*(function($){
	$.fn.simpleTabs = function(settings){
		var defaults = {classOn:'classOn',classOff:'classOff',eventName:'click',displayArea:'#displayArea',choiced:'0'};
		var params = $.extend({},defaults,settings);
		var _this = this;
		$(_this).find('li').attr('class',params['classOff']);
		var _defaultHref = $(_this).find('li:eq('+params['choiced']+')').attr('class',params['classOn']).find('a').attr('href');
		_ajaxContext(_defaultHref,params['displayArea']);
		$(_this).find('li a').click(function(){
			$(_this).find('li').attr('class',params['classOff']);
			$(this).parent().attr('class',params['classOn']);
			var _href = $(this).attr('href');
			if($.trim(_href) != ''){
				_ajaxContext(_href,params['displayArea']);
			}
			return false;
		});
		return _this;
	}
	
	function _ajaxContext(_url,_area){
			$.ajax({
					type:'POST',
					url:_url,
					success:function(msg){
						$(_area).html(msg);
					}
				});
	}
})(jQuery);*/
function simpleTabs(selector,settings){
		var defaults = {classOn:'classOn',classOff:'classOff',eventName:'click',displayArea:'#displayArea',choiced:'0'};
		var params = $.extend({},defaults,settings);
		var _this = $(selector);
		$(_this).find('li').attr('class',params['classOff']);
		var _defaultHref = $(_this).find('li:eq('+params['choiced']+')').attr('class',params['classOn']).find('a').attr('href');
		_ajaxContext(_defaultHref,params['displayArea']);
		$(_this).find('li a').click(function(){
			$(_this).find('li').attr('class',params['classOff']);
			$(this).parent().attr('class',params['classOn']);
			var _href = $(this).attr('href');
			if($.trim(_href) != ''){
				_ajaxContext(_href,params['displayArea']);
			}
			return false;
		});
		return _this;
	
}
function _ajaxContext(_url,_area){
			$.ajax({
					type:'POST',
					url:_url,
					success:function(msg){
						$(_area).html(msg);
					}
				});
	}