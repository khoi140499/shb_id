﻿/**
* 展开或者隐藏选中的menu
* @param {String} menuCode
* @param {String} 折叠图片名称
* @param {String} 展开图片名称
* @author zhanghao@ecc
*/
function expand(menuCode,imgFold,imgExpand)
{
	var menuArea = eval("document.all.BLOCK"+menuCode);
	var imgObject = eval("document.all.IMG"+menuCode);
	var changeStyle ;

	if( menuArea.style.display=='none' )
	{
		changeStyle='inline';
		imgObject.src=imgExpand;
	}
	else
	{
		changeStyle='none';
		imgObject.src=imgFold;
	}
	
	menuArea.style.display=changeStyle;
	var allNodes = document.all.tags('BLOCKQUOTE');
	for(var i=0; i<allNodes.length; i++)
	{
		var tagBody = allNodes[i];
		
		if (tagBody.getAttribute("id") == 'BLOCK'+menuCode )
		{
			var menuNodes = tagBody.childNodes;
			for(var j=0; j<menuNodes.length; j++)
			{
				var childId =menuNodes[j].id;
				if ( childId.indexOf('BLOCK') == 0 )
				{
					var chlidMenuCode = childId.substr(5);
					var childArea = eval("document.all.BLOCK"+chlidMenuCode);

					if ( changeStyle != childArea.style.display )
					{
						expand(chlidMenuCode,imgFold,imgExpand);
					}
				}
				else
				{
					menuNodes[j].style.display=changeStyle;
				}
			}
			break;
		}
	}
	parent.sizeChange();
}