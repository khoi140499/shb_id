function nst(t)
{
   var stmp = "";
   if(t.value==stmp) return;
   var ms = t.value.replace(/[^\d\.]/g,"").replace(/(\.\d{2}).+$/,"$1").replace(/^0+([1-9])/,"$1").replace(/^0+$/,"0");
   var txt = ms.split(".");
   while(/\d{4}(,|$)/.test(txt[0]))
     txt[0] = txt[0].replace(/(\d)(\d{3}(,|$))/,"$1,$2");
   t.value = stmp = txt[0]+(txt.length>1?"."+txt[1]:"");
}

function setAmountFmt(amount){

$(amount).css("direction","rtl");

transFrame=amount.parentNode;

while(transFrame.tagName!="FORM")
transFrame=transFrame.parentNode;

$(transFrame).submit(function (){
amount.value=amount.value.replace(",","");
} 
)
$(amount).keyup(function(){
  nst(amount);
  setCaretPosition(amount,amount.value.length);
});

$(amount).focus(function(){
  amount.value=amount.value.replace(".00","");
   setCaretPosition(amount,amount.value.length);
})
$(amount).focusout(function() {
	amount.value+=".00";
})
}

function setCaretPosition(ctrl, pos){
	if(ctrl.setSelectionRange)
	{
		ctrl.focus();
		ctrl.setSelectionRange(pos,pos);
	}
	else if (ctrl.createTextRange) {
		var range = ctrl.createTextRange();
		range.collapse(true);
		range.moveEnd('character', pos);
		range.moveStart('character', pos);
		range.select();
	}
}

function bindAmount(){
for(i=0;i<$(".amount").length;i++)
	setAmountFmt($(".amount")[i]);
}