var selectedNoName = "no";
var unSelectedAlertMsg = "Please choose one record!"; 
	
function formAction(frm,eventName,needCheck,autoSubmit){
	
	var confirmMsg = "Are you sure to "+ eventName +" the record?"
	
	if(needCheck){
		var selected = checkSelected();
		if(!selected){
			alert(unSelectedAlertMsg);
			return false;
		}
		if((eventName=="remove" || eventName=="delete") && !confirm(confirmMsg)){
		 	return false;
		}else if(eventName=="approve" && !confirm(confirmMsg)){
		 	return false;
		}else if(eventName=="reject" && !confirm(confirmMsg)){
		 	return false;
		}else if(eventName=="suspend" && !confirm(confirmMsg)){
		 	return false;
		}else if(eventName=="enable" && !confirm(confirmMsg)){
		 	return false;
		}else if(eventName=="cancel" && !confirm(confirmMsg)){
		 	return false;
		}else if(eventName=="resetPassword" && !confirm(confirmMsg)){
		 	return false;
		}
		
	}	
	frm.dse_nextEventName.value=eventName;
	if(autoSubmit)
		frm.submit();
	return true;
}

function checkSelected(){
    var selected=false;
    for(var i=0;i<document.getElementsByName(selectedNoName).length;i++)
	{
	  	if(document.getElementsByName(selectedNoName)[i].checked){
	        selected=true;
	        break;
	   	}
	}
	return selected;
}



//current time
var timerHandle=null;
var timerRunning=false;
var months = new Array("Jan.","Feb.","Mar.","Apr.","May.","Jun.","Jul.","Aug.","Sept.","Oct.","Nov.","Dec.");
function stopClock()
{
	if(timerRunning)
		clearTimeout (timerHandle);
	timerRunning=false;
}
function showTime()
{
	var now=new Date();
	var year=now.getFullYear();
	var month=now.getMonth();
	var date=now.getDate();
	var hours=now.getHours();
	var mins=now.getMinutes();
	var secs=now.getSeconds();
	var timeVal= months[month];
	timeVal += "&nbsp;"+date ;
	timeVal += ","+year ;
	timeVal += "&nbsp;"+((hours < 12) ? "AM" : "PM");
	timeVal += "&nbsp;"+((hours<=12) ? hours : hours-12);
	timeVal += ((mins<10)? ":0" : ":")+ mins;
	document.getElementById('timeBoard').innerHTML = timeVal;
	timerHandle = setTimeout("showTime();",1000);
	timerRunning = true;
}

function startClock()
{
	stopClock();
	showTime();
}