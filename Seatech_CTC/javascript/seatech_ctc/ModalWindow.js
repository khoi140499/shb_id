﻿/*
Script: modalwindow.js
	ModalWindow - Simple javascript popup overlay to replace builtin alert, prompt and confirm.

License:
	PHP-style license.

Copyright:
	Copyright (c) 2009 [Kieron Wilson](http://kd3sign.co.uk/).

Code & Documentation:
	http://kd3sign.co.uk/examples/modalwindow/

*/

var ModalWindow = {
	
	box: null,
	overlay: null,
	
	open: function() {
		var options = arguments[0] || {};
		var defaults = {
			'type': "alert",
			'opacity': "0.2",
			'width': null,
			'height': 'auto',
			'title': "Popup",
			'text': "",
			'yes_button': "Yes",
			'no_button': "No",
			'cancel_button': "Cancel",
			'yes': null,
			'no': null
		};

		for( key in defaults ) {
			this[key] = ( typeof options[key] != "undefined" ) ? options[key] : defaults[key] ;
		}
		
		this.close();
		this.initOverlay();
		this.initBox();
	},
	
	close: function() {
		if( this.box && this.overlay ) {
			this.remove( this.box );
			this.box = null;
			this.remove( this.overlay );
			this.overlay = null;
		}
		this.removeEvent(document, "keyup", ModalWindow.keyup);
		this.opened = false;
	},
	
	option: function(method) {
		if( this[ method ] ) {
			if(this.type == "prompt") {
				this[ method ](this.input.value);
			} else this[ method ]();
		}
		this.close();
	},
	
	keyup: function(e) {
		if( ModalWindow.getKeyCode(e) == 13 ) {
			ModalWindow.option('yes');
		}
	},
	
	initBox: function() {
		var dim = this.getPageSize();
		
		var boxWidth = ( ( this.width ) ? this.width : ( dim.pageWidth / 4 ) )  + "px";
		//var boxHeight = ( typeof this.height == "string" ) ? "auto" : this.height + "px" ;
		var boxHeight = (dim.pageHeight/4-20) + "px" ;
		
		//var boxPosVCentre = ( typeof this.height == "string" ) ? 0 : ( ( dim.windowHeight / 2 ) - ( parseInt(boxHeight) / 2 ) ) ;
		var boxPosVCentre = ( ( dim.pageHeight / 2 ) - ( parseInt(boxHeight) / 2 ) ) ;
		var boxPosHCentre = ( ( dim.pageWidth / 2 ) - ( parseInt(boxWidth) / 2 ) );
		
		//var boxPosTop = ( typeof this.height == "string" ) ? "10px" : boxPosVCentre + "px" ;
		var boxPosTop =  boxPosVCentre + "px";
		var boxPosLeft = boxPosHCentre + "px";
		
		this.box = this.element("div", {
			'class': 'ModalWindowBox',
			'styles' : {
				'width': boxWidth,
				'height': boxHeight,
				'position': "absolute",
				'top': boxPosTop,
				'left': boxPosLeft,
				'zIndex': 10000
			}
		});
		this.inject(this.box);
		
		var container = this.element("div", {
			'class': "ModalWindowContainer"
		});

		container.appendChild(this.element("h3", {
			'html': this.title
		}));

		container.appendChild(this.element("p", {
			'html': this.text
		}));
		
		var buttonContainer = this.element("div", {
			'class': "ModalWindowButtons"
		});

		switch( this.type ) {
			case "alert" :
				
				if(this.yes != false) {
					buttonContainer.appendChild(this.element("a", {
						'html': this.yes_button,
						'href': "javascript:ModalWindow.close();"
					}));
					container.appendChild(buttonContainer);
				}

			break;
			case "alert2" :

				if(this.yes) {
					buttonContainer.appendChild(this.element("a", {
						'html': "",
						'href': "",
						'style': "font-weight: bold;"
					}));
				}
				container.appendChild(buttonContainer);
			break;
            case "confirm" :

				if(this.yes) {
					buttonContainer.appendChild(this.element("a", {
						'html': this.yes_button,
						'href': "javascript:ModalWindow.option('yes');",
						'style': "font-weight: bold;"
					}));
				}
				if(this.no) {
					buttonContainer.appendChild(this.element("a", {
						'html': this.no_button,
						'href': "javascript:ModalWindow.option('no');"
					}));
				}
				buttonContainer.appendChild(this.element("a", {
					'html': this.cancel_button,
					'href': "javascript:ModalWindow.close();"
				}));

				container.appendChild(buttonContainer);

			break;

			case "prompt" :
				
				this.input = this.element("input", {
					'name': "prompt",
					'type': "text"
				});
				container.appendChild(this.input);

				if( this.yes ) {
					buttonContainer.appendChild(this.element("a", {
						'html': ModalWindow.yes_button,
						'href': "javascript:ModalWindow.option('yes');",
						'style': "font-weight: bold;"
					}));
				}
				buttonContainer.appendChild(this.element("a", {
					'html': ModalWindow.cancel_button,
					'href': "javascript:ModalWindow.close();"
				}));

				container.appendChild(buttonContainer);
			
			break;
		}
		
		this.box.appendChild(container);
		
		if(this.input) this.input.focus();
		else this.box.getElementsByTagName('a')[0].focus();
		
		if(this.yes || this.yes != false) this.addEvent(document, "keyup", ModalWindow.keyup);
		
	},
	
	initOverlay: function() {
		var dim = this.getPageSize();
		this.overlay = this.element("div", {
			'styles': {
			
				'backgroundColor': "black",
				'opacity': this.opacity,
				'position': "absolute",
				'top': "0px",
				'left': "0px",
				'width': dim.pageWidth + "px",
				'height': dim.pageHeight + "px",
				'zIndex': 9999
			
			}
		});
		this.inject(this.overlay);
	},
	
	inject: function(el) {
		document.body.appendChild( el );
	},
	
	remove: function(el) {
		document.body.removeChild( el );
	},
	
	element: function() {
		var tag = arguments[0], options = arguments[1];
		var el = document.createElement(tag);
		var attributes = {
			'html': 'innerHTML',
			'class': 'className',
			'for': 'htmlFor',
			'text': 'innerText'
		};
		if( options ) {
			if( typeof options == "object" ) {
				for( name in options ) {
					var value = options[name];
					if(name == "styles") {
						this.setStyles(el, value);
					} else if (attributes[name]) { 
						el[attributes[name]] = value; 
					} else { 
						el.setAttribute(name, value); 
					}
				}
			}
		}
		return el;
	},
	
	addEvent: function( o, e, f ) {
		if(o) {
			if( o.addEventListener ) o.addEventListener(e, f, false );
			else if( o.attachEvent ) o.attachEvent( 'on'+e , f);
		}
	},
	
	removeEvent: function( o, e, f ) {
		if(o) {
			if( o.removeEventListener ) o.removeEventListener( e, f, false );
			else if( o.detachEvent ) o.detachEvent( 'on'+e, f );
		}
	},
	
	getKeyCode: function(e) {
		if(!e) e = window.event ;
		return (e) ? (e.which || e.keyCode) : null ;
	},
	
	setStyles: function(e, o) {
		for( k in o ) {
			this.setStyle(e, k, o[k]);
		}
	},
	
	setStyle: function(e, p, v) {
		if ( p == 'opacity' ) {
			if (window.ActiveXObject) {
				e.style.filter = "alpha(opacity=" + v*100 + ")";
			}
			e.style.opacity = v;
		} else {
			e.style[p] = v;
		}
	},
		
	getPageSize: function() {
		var xScroll, yScroll;
		
		if (window.innerHeight && window.scrollMaxY) {	
			xScroll = window.innerWidth + window.scrollMaxX;
			yScroll = window.innerHeight + window.scrollMaxY;
		} else if (document.body.scrollHeight > document.body.offsetHeight) {
			xScroll = document.body.scrollWidth;
			yScroll = document.body.scrollHeight;
		} else {
			xScroll = document.body.offsetWidth;
			yScroll = document.body.offsetHeight;
		}
		
		var windowWidth, windowHeight;
	
		if (self.innerHeight) {
			if(document.documentElement.clientWidth){
				windowWidth = document.documentElement.clientWidth; 
			} else {
				windowWidth = self.innerWidth;
			}
			windowHeight = self.innerHeight;
		} else if (document.documentElement && document.documentElement.clientHeight) {
			windowWidth = document.documentElement.clientWidth;
			windowHeight = document.documentElement.clientHeight;
		} else if (document.body) {
			windowWidth = document.body.clientWidth;
			windowHeight = document.body.clientHeight;
		}	

		if(yScroll < windowHeight){
			pageHeight = windowHeight;
		} else { 
			pageHeight = yScroll;
		}

		if(xScroll < windowWidth){	
			pageWidth = xScroll;		
		} else {
			pageWidth = windowWidth;
		}
	
		return { 'pageWidth': pageWidth, 'pageHeight': pageHeight, 'windowWidth' : windowWidth, 'windowHeight': windowHeight };
	}
	
};

