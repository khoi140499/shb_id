﻿//Author: Created by HungVD
//Purpose:
//Update:

function Chu(gNumber) {
    var result = "";
    switch (gNumber) {
        case "0":
            result = "không";
            break;
        case "1":
            result = "một";
            break;
        case "2":
            result = "hai";
            break;
        case "3":
            result = "ba";
            break;
        case "4":
            result = "bốn";
            break;
        case "5":
            result = "năm";
            break;
        case "6":
            result = "sáu";
            break;
        case "7":
            result = "bảy";
            break;
        case "8":
            result = "tám";
            break;
        case "9":
            result = "chín";
            break;
    }

    return result;
}

function DonVi(so) {
    var Kdonvi = "";
    switch (so) {
        case "1":
            Kdonvi = "";
            break;
        case "2":
            Kdonvi = "nghìn";
            break;
        case "3":
            Kdonvi = "triệu";
            break;
        case "4":
            Kdonvi = "tỷ";
            break;
        case "5":
            Kdonvi = "nghìn";
            break;
        case "6":
            Kdonvi = "triệu";
            break;
        case "7":
            Kdonvi = "tỷ tỷ";
            break;
    }

    return Kdonvi;
}

function Tach(tach3) {
    var Ktach = "";
    var result = "";
    if (tach3 == "000") {
        result = "";
    } else {
        if (tach3.length == 3) {
            var tr = tach3.trim().substr(0, 1).toString().trim();
            var ch = tach3.trim().substr(1, 1).toString().trim();
            var dv = tach3.trim().substr(2, 1).toString().trim();
            //alert("tr: " + tr + " - ch: " + ch + " - dv: " + dv);
            if (tr == "0" && ch == "0") {
                Ktach = " không trăm lẻ " + Chu(dv.toString().trim()) + " ";
            }
            if (tr != "0" && ch == "0" && dv == "0") {
                Ktach = Chu(tr.toString().trim()).trim() + " trăm ";
            }
            if (tr != "0" && ch == "0" && dv != "0") {
                Ktach = Chu(tr.toString().trim()).trim() + " trăm lẻ " + Chu(dv.trim()).trim() + " ";
            }
            if (tr == "0" && parseInt(ch) > 1 && parseInt(dv) > 0 && dv != "5") {
                Ktach = " không trăm " + Chu(ch.trim()).trim() + " mươi " + Chu(dv.trim()).trim() + " ";
            }
            if (tr == "0" && parseInt(ch) > 1 && dv == "0") {
                Ktach = " không trăm " + Chu(ch.trim()).trim() + " mươi ";
            }
            if (tr == "0" && parseInt(ch) > 1 && dv == "5") {
                Ktach = " không trăm " + Chu(ch.trim()).trim() + " mười lăm ";
            }
            if (tr == "0" && parseInt(ch) == "1" && parseInt(dv) > 0 && dv != "5") {
                Ktach = " không trăm mười " + Chu(dv.trim()).trim() + " ";
            }
            if (tr == "0" && ch == "1" && dv == "0") {
                Ktach = " không trăm mười ";
            }
            if (tr == "0" && ch == "1" && dv == "5") {
                Ktach = " không trăm mười lăm ";
            }

            if (parseInt(tr) > 0 && parseInt(ch) > 1 && parseInt(dv) > 0 && parseInt(dv) != 5) {
                Ktach = " " + Chu(tr.trim()).trim() + " trăm " + Chu(ch.trim()).trim() + " mươi " + Chu(dv.trim()).trim() + " ";
            }
            if (parseInt(tr) > 0 && parseInt(ch) > 1 && dv == "0") {
                Ktach = Chu(tr.trim()).trim() + " trăm " + Chu(ch.trim()).trim() + " mươi ";
            }
            if (parseInt(tr) > 0 && parseInt(ch) > 1 && dv == "5") {
                Ktach = Chu(tr.trim()).trim() + " trăm " + Chu(ch.trim()).trim() + " mươi lăm ";
            }
            if (parseInt(tr) > 0 && ch == "1" && parseInt(dv) > 0 && dv != "5") {
                Ktach = Chu(tr.trim()).trim() + " trăm mười " + Chu(dv.trim()).trim() + " ";
            }
            if (parseInt(tr) > 0 && ch == "1" && dv == "0") {
                Ktach = Chu(tr.trim()).trim() + " trăm mười ";
            }
            if (parseInt(tr) > 0 && ch == "1" && dv == "5") {
                Ktach = Chu(tr.trim()).trim() + " trăm mười lăm ";
            }
        }
        result = Ktach;
    }

    return result;
}

function So_chu(param) {
    var bIsSoAm = false;
    var result = "";
    
    var gNum = parseFloat(param);
    
    try {
        if (gNum == 0.0) {
            result = "Không đồng";
        } else {
            if (gNum < 0.0) {
                bIsSoAm = true;
                gNum = -gNum;
            }

            var lso_chu = "";
            var tach_mod = "";
            var tach_conlai = "";

            var Num = Math.round(gNum, 0); //Careful! Need to re-check
            var gN = Num.toString();
            var i = parseInt(gN.length / 3);
            var mod = gN.length - (i * 3);
            
            var dau = "";
            if (mod == 1) {
                tach_mod = "00" + Num.toString().trim().substr(0, 1).trim();
            }
            if (mod == 2) {
                tach_mod = "0" + Num.toString().trim().substr(0, 2).trim();
            }
            if (mod == 0) {
                tach_mod = "000";
            }
            if (Num.toString().length > 2) {
                tach_conlai = Num.toString().trim().substr(mod, Num.toString().length - mod).trim();
            }
            var im = i + 1;
            if (mod > 0) {
                //alert(Tach(tach_mod).toString().trim());
                lso_chu = Tach(tach_mod).toString().trim() + " " + DonVi(im.toString().trim());
            }

            var j = i;
            var _m = i;
            var k = 1;
            while (j > 0) {
                var tach3 = tach_conlai.trim().substr(0, 3).trim();
                var tach3_ = tach3;
                lso_chu = lso_chu.trim() + " " + Tach(tach3.trim()).trim();
                i = _m + 1 - k;
                if (tach3_ != "000") {
                    lso_chu = lso_chu.trim() + " " + DonVi(i.toString().trim()).trim();
                }
                tach_conlai = tach_conlai.trim().substr(3, tach_conlai.trim().length - 3);
                j--;
                k++;
            }
            if (lso_chu.trim().substr(0, 1) == "k") {
                lso_chu = lso_chu.trim().substr(10, lso_chu.trim().length - 10).trim();
            }
            if (lso_chu.trim().substr(0, 1) == "l") {
                lso_chu = lso_chu.trim().substr(2, lso_chu.trim().length - 2).trim();
            }
            if (lso_chu.trim().length > 0) {
                lso_chu = dau.trim() + " " + lso_chu.trim().substr(0, 1).trim().toUpperCase() + lso_chu.trim().substr(1, lso_chu.trim().length - 1).trim() + " đồng chẵn.";
            }
            lso_chu = lso_chu.replace("mươi một", "mươi mốt");

            if (!bIsSoAm) {
                result = lso_chu.toString().trim();
            }
            else {
                result = "Âm " + lso_chu.toString().trim();
            }
        }
    } catch (err) {
        alert(err.message);
    }
    return result;
}
