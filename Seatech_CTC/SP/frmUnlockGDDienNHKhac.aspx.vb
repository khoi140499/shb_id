﻿Imports Business
Imports VBOracleLib
Imports System.Data
Imports System.Data.OracleClient
Imports System.Xml.Serialization
Imports Business.Common.mdlCommon
Imports CorebankServiceESB

Partial Class SP_frmUnlockGDDienNHKhac
    Inherits System.Web.UI.Page


    Dim clsSP As CorebankServiceESB.clsSongPhuong = New CorebankServiceESB.clsSongPhuong
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Search()
    End Sub

    Protected Function Search() As DataTable
        Dim strSql As String = ""
        Dim strWhere As String = ""
        Dim strOrder As String = ""
        If txtTuNgay.Value.Trim() = "" And txtDenNgay.Value.Trim() <> "" Then
            strWhere += " and NGAY_HT <= to_date('" & txtDenNgay.Value.Trim & " 23:59:59','dd/MM/yyyy HH24:MI:SS') "
        ElseIf txtTuNgay.Value.Trim() <> "" And txtDenNgay.Value.Trim() <> "" Then
            strWhere += " and NGAY_HT >= to_date('" & txtTuNgay.Value.Trim & " 00:00:00','dd/MM/yyyy HH24:MI:SS') and NGAY_HT <= to_date('" & txtDenNgay.Value.Trim() & " 23:59:59','dd/MM/yyyy HH24:MI:SS')"
        ElseIf txtTuNgay.Value.Trim() <> "" And txtDenNgay.Value.Trim() = "" Then
            strWhere += " and NGAY_HT >= to_date('" & txtTuNgay.Value.Trim() & " 00:00:00','dd/MM/yyyy HH24:MI:SS') "
        End If
        If txtSoCT.Text.Trim() <> "" Then
            strWhere += " and so_ct = '" & txtSoCT.Text.Trim() & "'"
        End If
        If txtMST.Text.Trim() <> "" Then
            strWhere += " and ma_nnthue = '" & txtMST.Text.Trim() & "'"
        End If
        strOrder = " order by so_ct desc"

        strSql &= "select SO_CT, so_ct_nh, ma_nnthue, ten_nnthue, ttien, 'Chờ kiểm soát' trang_thai , DECODE(ma_lthue,'04', 'TQHQ','TQND') lthue "
        strSql &= " from tcs_ctu_songphuong_hdr where trang_thai ='02' and lan_ks = '1' "

        strSql &= " " & strWhere & strOrder
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        'dt = DatabaseHelp.ExecuteToTable(strSql)
        If dt.Rows.Count > 0 Then
            dtgdata.DataSource = dt
            dtgdata.DataBind()
        Else
            dtgdata.DataSource = Nothing
            dtgdata.DataBind()
            clsCommon.ShowMessageBox(Me, "Không có dữ liệu.")
        End If
        Return dt
    End Function

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim succsessCount As Integer
        Dim selectedCount As Integer
        For i As Integer = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox = dtgdata.Items(i).FindControl("chkSelectItem")
            If chkSelect.Checked Then
                selectedCount += 1
                Dim strTYPE As String = dtgdata.Items(i).Cells(7).Text.Trim()
                Dim strSo_ct As String = dtgdata.Items(i).Cells(8).Text.Trim()
                If strTYPE = "TQND" Or strTYPE = "TQHQ" Then
                    strTYPE = "CITAD_02"
                End If
                If ProcessSelectedMsg(strSo_ct, strTYPE) Then
                    succsessCount += 1
                End If
            End If
        Next
        If selectedCount = 0 Then
            clsCommon.ShowMessageBox(Me, "Bạn chưa chọn giao dịch nào để cập nhật")
            Return
        Else
            If succsessCount > 0 Then
                clsCommon.ShowMessageBox(Me, "Đã cập nhật thành công " & succsessCount.ToString() & " giao dịch trên " & selectedCount.ToString() & " giao dịch đã chọn")
            Else
                clsCommon.ShowMessageBox(Me, "Cập nhật giao dịch thất bại. Vui lòng thử lại sau")
            End If
        End If
        Search()
    End Sub

    Protected Function ProcessSelectedMsg(ByVal strSo_ct As String, ByVal strTYPE As String) As Boolean
        Dim result As Boolean = True
        Dim strMess_Content As String = ""
        Dim processStatus As Boolean = True
        Dim strMA_NV As String = Session.Item("User").ToString()
        'Try
        '    'send core here
        '    clsSP.SendChungTuSP(strSo_ct, strTYPE)
        'Catch ex As Exception
        '    log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("CO LOI KHI CAP NHAT GIAO DICH - ProcessSelectedMsg.process_Msg" & ex.Message)
        '    result = False
        '    processStatus = False
        'End Try
        Dim strSql As String = ""

        strSql = "update tcs_ctu_songphuong_hdr set lan_ks='0' where so_ct='" & strSo_ct & "'"

        DataAccess.ExecuteNonQuery(strSql, CommandType.Text)
        Return result
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = "Unlock giao dịch điện thu NSNN từ CITAD"

        If Not IsPostBack Then
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub

    Protected Sub dtgdata_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgdata.ItemCommand
        Dim commandName = e.CommandName.ToString()
        Dim strID_Gdich = e.CommandArgument.ToString()

    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        Try
            dtgdata.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
        End Try
        Search()
    End Sub
End Class
