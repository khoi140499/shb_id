﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmBCTongChungTu.aspx.vb" 
Inherits="SP_frmBCTongChungTu" title="Báo cáo tổng hợp chứng từ thành công" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }
    </script>
    <style type="text/css">
        .dataView{padding-bottom:10px}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">Báo cáo tổng hợp chứng từ thành công</asp:Label>
            </td>
        </tr>
        <tr>
        <td>
    <div style="width:100%; margin-bottom:30px">
          <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
            <tr>
                <td align="left" style="width:15%" class="form_label">Từ ngày</td>
                <td class="form_control" style="width:35%"><span style="display:block; width:40%; position:relative;vertical-align:middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                    onblur="CheckDate(this);" onfocus="this.select()" />
                </span></td>
                <td align="left" style="width:15%" class="form_label">Đến ngày</td>
                <td class="form_control" style="width:35%">
                <span style="display:block; width:40%; position:relative; vertical-align:middle;">
                    <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                     onblur="CheckDate(this);" onfocus="this.select()" />
                </span></td>
            </tr>
            <tr>
                <td align="left" class="form_label">Loại thuế</td>
                <td class="form_control">
                    <asp:DropDownList ID="drpLoaiThue" runat="server" Width="91%" CssClass="inputflat" >
                         <asp:ListItem Selected="True" Value="" Text="Tất cả"></asp:ListItem>
                         <asp:ListItem  Value="ETAX_ND" Text="Thuế nội địa tại quầy"></asp:ListItem>
                         <asp:ListItem  Value="ETAX_HQ" Text="Thuế hải quan tại quầy"></asp:ListItem>
                         <asp:ListItem  Value="NTDT" Text="Thuế nội địa điện tử"></asp:ListItem>
                        <asp:ListItem  Value="ETAX_HQ247" Text="Thuế hải quan điện tử"></asp:ListItem>
                    </asp:DropDownList>

                </td>
            </tr>
            
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnTimKiem" runat="server" Text="Tìm kiếm" />
                    <asp:Button ID="btnExport" runat="server" Text="Export" />
                </td>
            </tr>
        </table>
    </div>
    <div class="dataView">
        <asp:GridView ID="dtgrd" runat="server" AutoGenerateColumns="False" ShowFooter="true"
            Width="100%" BorderColor="#000" CssClass="grid_data" AllowPaging="True" PageSize='15'>
            <AlternatingRowStyle CssClass="grid_item_alter"></AlternatingRowStyle>
            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
            <RowStyle CssClass="grid_item" />
            <Columns>
                <asp:TemplateField HeaderText="STT">
                    <ItemTemplate>
                        <%#Container.DataItemIndex + 1%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate><b>Tổng</b></FooterTemplate>
                    <FooterStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="REF_NO" HeaderText="Mã Ref">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:TemplateField HeaderText="Số chứng từ">
                    <ItemTemplate>
                        <%#Eval("SO_CTU")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderTemplate>
                        Số chứng từ
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate>
                        <asp:Label ID="totalCT" runat="server" ForeColor="Blue" ></asp:Label>
                    </FooterTemplate>
                    <FooterStyle HorizontalAlign="Center" />
                </asp:TemplateField>
              
                 <asp:BoundField DataField="TEN_LTHUE" HeaderText="Loại thuế">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                 <asp:TemplateField>
                    <ItemTemplate>
                        <%#Eval("MA_NGUYENTE")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <HeaderTemplate>
                        Nguyên tệ
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate>
                        <div><span style="font-weight:bold;color:blue">VND</span></div>
                    </FooterTemplate>
                    <FooterStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%#Eval("TONG_TIEN", "{0:n2}")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    <HeaderTemplate>
                        Số tiền
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate>
                        <asp:Label ID="totalAmount" runat="server" ForeColor="Blue"></asp:Label>
                    </FooterTemplate>
                    <FooterStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                 <asp:BoundField DataField="TRAN_DATE" HeaderText="Ngày chứng từ">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
            </Columns>
            <PagerStyle HorizontalAlign="Right" BackColor="#E4E5D7" Font-Bold="False"
                Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                Font-Strikeout="False" Font-Underline="False" ForeColor="#000" VerticalAlign="Middle">
            </PagerStyle>
        </asp:GridView>
        <asp:GridView ID="grdExport" runat="server" AutoGenerateColumns="False" ShowFooter="true" Visible="false"
            Width="100%" BorderColor="#000" CssClass="grid_data" AllowPaging="false" >
            <AlternatingRowStyle CssClass="grid_item_alter"></AlternatingRowStyle>
            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
            <RowStyle CssClass="grid_item" />
            <Columns>
                <asp:TemplateField HeaderText="STT">
                    <ItemTemplate>
                        <%#Container.DataItemIndex + 1%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate><b>Tổng</b></FooterTemplate>
                    <FooterStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="REF_NO" HeaderText="Mã Ref">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:TemplateField HeaderText="Số chứng từ">
                    <ItemTemplate>
                        <%#Eval("SO_CTU")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderTemplate>
                        Số chứng từ
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate>
                        <asp:Label ID="totalCT" runat="server" ForeColor="Blue" ></asp:Label>
                    </FooterTemplate>
                    <FooterStyle HorizontalAlign="Center" />
                </asp:TemplateField>
               
                 <asp:BoundField DataField="TEN_LTHUE" HeaderText="Loại thuế">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                 <asp:TemplateField>
                    <ItemTemplate>
                        <%#Eval("MA_NGUYENTE")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <HeaderTemplate>
                        Nguyên tệ
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate>
                        <div><span style="font-weight:bold;color:blue">VND</span></div> 
                    </FooterTemplate>
                    <FooterStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%#Eval("TONG_TIEN", "{0:n2}")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    <HeaderTemplate>
                        Số tiền
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                    <FooterTemplate>
                        <asp:Label ID="totalAmount" runat="server" ForeColor="Blue"></asp:Label>
                    </FooterTemplate>
                    <FooterStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                 <asp:BoundField DataField="TRAN_DATE" HeaderText="Ngày chứng từ">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
            </Columns>
            <PagerStyle HorizontalAlign="Right" BackColor="#E4E5D7" Font-Bold="False"
                Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                Font-Strikeout="False" Font-Underline="False" ForeColor="#000" VerticalAlign="Middle">
            </PagerStyle>
        </asp:GridView>
    </div>
    </td>
        </tr>
    </table>
    <!-- required plugins -->
    <script src="../../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function () {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>	
</asp:Content>

