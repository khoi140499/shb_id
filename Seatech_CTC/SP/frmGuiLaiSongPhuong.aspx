﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmGuiLaiSongPhuong.aspx.vb" Inherits="SP_frmGuiLaiSongPhuong" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }
        function jsShowChiTiet(so_ct,lt)
        {
            var a = "";
            var ph = lt;
            switch (ph) {
                case 'TQHQ':
                    a = "~/../../pages/HaiQuan/frmChiTietCT.aspx?soct=" + so_ct;
                    break;
                case 'TQND':
                    a = "~/../../pages/ChungTu/frmChiTietCT.aspx?soct=" + so_ct;
                    break;
                case 'HQ247':
                    a = "~/../../HQ247/frmViewYCTTThueChiTiet.aspx?ID=" + so_ct;
                    break;
                case 'NHOTHU':
                    a = "~/../../HQNHOTHU/frmViewYCTTThueChiTiet.aspx?ID=" + so_ct;
                    break;
                case 'NTDT':
                    a = "~/../../NTDT/frmChiTietCTNTDT.aspx?soct=" + so_ct;;
                    break;
            }
           window.open(a, "_blank", "toolbar=no,scrollbars=yes,menubar=no, addressbar=no,location=no,resizable=yes,top=20,left=20,width=1024,height=700");
        }
    </script>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function () {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })

            if ($('#ctl00_MstPg05_MainContent_txtTuNgay').val() == "" && $("#ctl00_MstPg05_MainContent_txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>
    <style type="text/css">
        .container {
            width: 100%;
            margin-bottom: 30px;
        }

        .dataView {
            padding-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <p class="pageTitle">Tra cứu và gửi lại song phương</p>
    <div class="container" style="vertical-align: middle">
        <table cellpadding="2" cellspacing="1" border="0" width="80%" class="form_input">
            <tr>
                <td align="left" style="width: 15%" class="form_label">Từ ngày</td>
                <td class="form_control" style="width: 35%"><span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                        onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                </span></td>
                <td align="left" style="width: 15%" class="form_label">Đến ngày</td>
                <td class="form_control" style="width: 35%">
                    <span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                        <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                            onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                    </span></td>
            </tr>
            <tr>
                <td class="form_label" align="left">Số chứng từ</td>
                <td class="form_control">
                    <asp:TextBox ID="txtSoCT" runat="server" CssClass="inputflat" />
                </td>
                <td class="form_label" align="left">Mã số thuế</td>
                <td class="form_control">
                    <asp:TextBox runat="server" ID="txtMST" CssClass="inputflat" /></td>
            </tr>
            <tr>
                 <td class="form_label" align="left">Số GD Corebank
                </td>
                <td class="form_control">
                    <asp:TextBox ID="txtSo_CT_NH" runat="server" CssClass="inputflat" />
                </td>
                <td class="form_label" align="left">Loại thuế
                </td>
                <td class="form_control">
                    <asp:DropDownList ID="ddlMaLoaiThue" runat="server">
                        <asp:ListItem Text="<<Tất cả>>" Value="00"></asp:ListItem>
                        <asp:ListItem Text="Thuế Nội Địa tại quầy" Value="01"></asp:ListItem>
                        <asp:ListItem Text="Thuế Hải Quan tại quầy" Value="02"></asp:ListItem>
                        <asp:ListItem Text="Thuế Hải Quan 247" Value="04"></asp:ListItem>
                        <asp:ListItem Text="Thuế Hải quan nhờ thu" Value="05"></asp:ListItem>
                        <asp:ListItem Text="Thuế điện tử nội địa" Value="03"></asp:ListItem>
                    </asp:DropDownList>
                </td>

            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button runat="server" ID="btnSearch" Text="Tra cứu" />
                    <asp:Button runat="server" ID="btnProcess" Text="Xử lý" OnClientClick="return confirm('Bạn có muốn xử lý chứng từ đã chọn?')" />
                </td>
            </tr>

        </table>
        <div class="dataView">
            <asp:DataGrid ID="dtgdata" runat="server" AutoGenerateColumns="False" TabIndex="9"
                Width="80%" BorderColor="#989898" CssClass="grid_data" AllowPaging="True"
                PageSize="20">
                <PagerStyle Mode="NumericPages" />
                <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header"></HeaderStyle>
                <ItemStyle CssClass="grid_item" />
                <Columns>
                    <asp:TemplateColumn HeaderText="Chọn">
                        <ItemTemplate>
                            <asp:CheckBox runat="server" ID="chkSelectItem" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Số chứng từ" >
                     <ItemTemplate>
                        <a href="#" onclick="jsShowChiTiet('<%#Eval("id").ToString %>','<%#Eval("lthue").ToString%>')"><%#Eval("so_ct").ToString%></a>
                     </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="so_ct_nh" HeaderText="Số GD Corebank"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ma_nnthue" HeaderText="Mã số thuế"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ten_nnthue" HeaderText="Tên người nộp thuế"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ttien" HeaderText="Số tiền"></asp:BoundColumn>
                    <asp:BoundColumn DataField="trang_thai" HeaderText="Trạng thái"></asp:BoundColumn>
                    <asp:BoundColumn DataField="lthue" HeaderText="Phân hệ" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="table_update" HeaderText="table" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="so_ct" HeaderText="So Chung Tu2" Visible="false"></asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
</asp:Content>

