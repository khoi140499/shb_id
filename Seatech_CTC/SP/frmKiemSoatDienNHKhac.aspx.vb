﻿Imports Business_HQ
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common.mdlSystemVariables
Imports System.Data
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports System.Diagnostics
Imports System.Xml.XmlDocument
Imports Business_HQ.NewChungTu
Imports System.Xml
Imports Business_HQ.CTuCommon
Imports log4net
Imports GIPBankService
Imports System.IO
Imports Business_HQ.BaoCao
Imports Business.Common
'Imports CoreBankSV

Partial Class SP_frmKiemSoatDienNHKhac
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private mv_objUser As New CTuUser
    Private illegalChars As String = "[&]"
    Private Shared strMaNhom_GDV As String = "" 'ConfigurationManager.AppSettings.Get("GROUP_TELLER").ToString()
    Private Shared strTT_TIENMAT As String = "" 'ConfigurationManager.AppSettings.Get("TK_TIENMAT").ToString()
    Private Shared pv_cifNo As String = String.Empty
    Private Shared cls_corebank As New CorebankServiceESB.clsCoreBank
    Private Shared CORE_VERSION As String = ConfigurationManager.AppSettings.Get("CORE.VERSION").ToString()
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            CreateDumpChiTietCTGrid() 'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu
            ClearCTHeader() 'Xóa thông tin ở grid header
            ShowHideCTRow(False)
            lblStatus.Text = "Hãy chọn một chứng từ cần kiểm soát"

        End If
        If Session.IsNewSession Then
            ' Force session to be created;
            ' otherwise the session ID changes on every request.
            Session("ForceSession") = DateTime.Now
        End If
        ' 'Sign' the viewstate with the current session.
        Me.ViewStateUserKey = Session.SessionID
        'Server.HtmlEncode(txtContentEx.Text)
        If Page.EnableViewState Then
            ' Make sure ViewState wasn't passed on the querystring.
            ' This helps prevent one-click attacks.
            If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
                Throw New Exception("Viewstate existed, but not on the form.")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'BEGIN-TYNK CHECK NGAY NGHI
        'If Not Business_HQ.CTuCommon.GetNgayNghi() Then
        '    ' clsCommon.ShowMessageBox(Me, "Hôm nay là ngày nghỉ. Bạn không được vào chức năng này!!!")
        '    Response.Redirect("~/pages/frmHomeNV.aspx?CheckNN=1", False)
        '    Exit Sub
        'End If
        'END TYNK CHECK NGAY NGHI
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            'RemoveHandler cmdKS.Click, AddressOf cmdKS_Click
            RemoveHandler cmdChuyenThue.Click, AddressOf cmdChuyenThue_Click
            RemoveHandler cmdHuy.Click, AddressOf cmdHuy_Click
            RemoveHandler cmdChuyenTra.Click, AddressOf cmdChuyenTra_Click
            Exit Sub
        End If
        'Kiểm tra trạng thái session
        If Not Session.Item("User") Is Nothing Then
            mv_objUser = New CTuUser(Session.Item("User"))
        Else
            If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                Response.Cookies("ASP.NET_SessionId").Value = String.Empty
                Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
            End If
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        'Check các quyền của user

        If Not IsPostBack Then
            'Khởi tạo các giá trị khởi đầu cho các điều khiển
            'Load danh sách chứng từ thành lập bởi điểm thu
            LoadDSCT("99")
            LoadDM_Nhanvien()
            'LoadCateLoaiTienThue()
        End If
    End Sub

    Private Sub CreateDumpChiTietCTGrid()
        'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu
        Dim v_dt As New DataTable()
        v_dt.Columns.Add(New DataColumn("SO_TK"))
        v_dt.Columns.Add(New DataColumn("NGAY_TK"))
        v_dt.Columns.Add(New DataColumn("SAC_THUE"))
        v_dt.Columns.Add(New DataColumn("MA_CHUONG"))
        v_dt.Columns.Add(New DataColumn("MA_TMUC"))
        v_dt.Columns.Add(New DataColumn("NOI_DUNG"))
        v_dt.Columns.Add(New DataColumn("MA_KHOAN"))
        v_dt.Columns.Add(New DataColumn("KY_THUE"))
        v_dt.Columns.Add(New DataColumn("SOTIEN"))
        v_dt.Columns.Add(New DataColumn("SOTIEN_NT"))
        grdChiTiet.Controls.Clear()
        Dim v_dumpRow As DataRow = v_dt.NewRow
        v_dt.Rows.Add(v_dumpRow)
        grdChiTiet.DataSource = v_dt
        grdChiTiet.DataBind()
    End Sub

    Private Sub CreateDumpDSCTGrid()
        'Tạo grid CT chi tiết với 1 dòng duy nhất để hiển thị lúc ban đầu
        Dim v_dt As New DataTable()
        v_dt.Columns.Add(New DataColumn("SHKB"))
        v_dt.Columns.Add(New DataColumn("NGAY_KB"))
        v_dt.Columns.Add(New DataColumn("SO_CT"))
        v_dt.Columns.Add(New DataColumn("KYHIEU_CT"))
        v_dt.Columns.Add(New DataColumn("SO_BT"))
        v_dt.Columns.Add(New DataColumn("MA_DTHU"))
        v_dt.Columns.Add(New DataColumn("MA_NV"))
        v_dt.Columns.Add(New DataColumn("TRANG_THAI"))
        v_dt.Columns.Add(New DataColumn("TT_SP"))
        v_dt.Columns.Add(New DataColumn("MA_NNTHUE"))
        v_dt.Columns.Add(New DataColumn("TEN_DN"))
        v_dt.Columns.Add(New DataColumn("response_msg"))
        v_dt.Columns.Add(New DataColumn("so_ct_nh"))
        v_dt.Columns.Add(New DataColumn("LAN_KS"))
        v_dt.Columns.Add(New DataColumn("Ma_KS"))

        grdDSCT.Controls.Clear()
        'Dim v_dumpRow As DataRow = v_dt.NewRow
        'v_dt.Rows.Add(v_dumpRow)
        grdDSCT.DataSource = v_dt
        grdDSCT.DataBind()
    End Sub

    Private Sub ClearCTHeader()
        txtSHKB.Text = ""
        txtTenKB.Text = ""
        txtTKCo.Text = ""
        txtTKNo.Text = ""
        txtMaDBHC.Text = ""
        txtTenDBHC.Text = ""
        txtMaNNT.Text = ""
        txtTenNNT.Text = ""
        txtDChiNNT.Text = ""
        txtMaNNTien.Text = ""
        txtTenNNTien.Text = ""
        txtDChiNNT.Text = ""
        txtMaCQThu.Text = ""
        txtTenCQThu.Text = ""
        txtTK_KH_NH.Text = ""
        txtSoCT_NH.Text = ""
    End Sub

    Private Sub LoadDSCT(ByVal pv_strTrangThaiCT As String)
        Try
            Dim dsCT As DataSet = GetDSCT(pv_strTrangThaiCT)
            If (Not dsCT Is Nothing) And (dsCT.Tables(0).Rows.Count > 0) Then
                Me.grdDSCT.DataSource = dsCT
                Me.grdDSCT.DataBind()
            Else
                CreateDumpDSCTGrid()
            End If
        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub
    Private Sub LoadDM_Nhanvien()
        Dim dt As DataTable = DM_NV()
        ddlMacker.DataSource = dt
        ddlMacker.DataTextField = "ten_dn"
        ddlMacker.DataValueField = "Ma_nv"
        ddlMacker.DataBind()
        ddlMacker.Items.Insert(0, New ListItem("Tất cả", "99"))
        ddlMacker.SelectedIndex = 0
    End Sub
    Private Function DM_NV() As DataTable
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable
        Dim dk As String = ""
        Dim strcheckHS As String = checkHSC()
        Dim str_Ma_Nhom As String = ""
        Dim strSQL As String = ""

        str_Ma_Nhom = LOAD_MN()

        dt = HaiQuan.HaiQuan.GET_DM_NV(strcheckHS, str_Ma_Nhom, Session.Item("MA_CN_USER_LOGON").ToString, Session.Item("User").ToString, strMaNhom_GDV)

        Return dt
    End Function
    Public Function LOAD_MN() As String
        Dim str_return As String = ""

        str_return = HaiQuan.HaiQuan.GET_MA_NHOM(Session.Item("User").ToString)

        Return str_return
    End Function
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function
    'Lay danh sach chung tu 
    Private Function GetDSCT(ByVal pv_strTrangThaiCT As String) As DataSet
        Try
            If pv_strTrangThaiCT.Equals("99") Then pv_strTrangThaiCT = ""
            If ddlMacker.SelectedValue = "99" Then
                mv_objUser.Ma_NV = ""
            Else
                mv_objUser.Ma_NV = ddlMacker.SelectedValue
            End If

            'mv_objUser.Ban_LV="" cho phep lay chung tu thu ho
            Dim dsCT As DataSet = Business.ChungTuSP.buChungTu.CTU_GetListKiemSoat(mdlCommon.ConvertDateToNumber(mv_objUser.Ngay_LV), pv_strTrangThaiCT, _
                                 "", mv_objUser.Ma_NV, Session.Item("User"), "")
            Return dsCT
        Catch ex As Exception
            logger.Error(ex.StackTrace)
            Return Nothing
        End Try
    End Function

    Private Sub LoadCTChiTietAll(ByVal sSOCT As String)
        Try
            Dim v_strTT As String = String.Empty
            Dim v_strTTSP As String = String.Empty
            Dim v_strLoaiThue As String = String.Empty
            Dim v_strPhuongThucTT As String = String.Empty
            Dim v_str_Ma_NNT As String = String.Empty
            Dim v_strSO_CT As String = sSOCT
            'Load chứng từ header
            Dim dsCT As New DataSet
            dsCT = Business.ChungTuSP.buChungTu.CTU_Header_Set(sSOCT)
            If (dsCT.Tables(0).Rows.Count > 0) Then
                'ShowHideCTRow(True)
                FillDataToCTHeader(dsCT)
                v_strTT = dsCT.Tables(0).Rows(0)("TRANG_THAI").ToString
                v_strLoaiThue = dsCT.Tables(0).Rows(0)("MA_LTHUE").ToString
                v_str_Ma_NNT = dsCT.Tables(0).Rows(0)("Ma_NNThue").ToString
                v_strTTSP = dsCT.Tables(0).Rows(0)("TT_SP").ToString
                v_strSO_CT = dsCT.Tables(0).Rows(0)("so_ct").ToString
            Else
                lblStatus.Text = "Không tìm thấy chứng từ cần tìm.Hãy thử lại"
                Exit Sub
            End If

            'Load thông tin chi tiết
            Dim dsCTChiTiet As New DataSet
            dsCTChiTiet = Business.ChungTuSP.buChungTu.CTU_ChiTiet_Set_CT_TK(v_strSO_CT)
            If dsCTChiTiet.Tables(0).Rows.Count > 0 Then
                FillDataToCTDetail(dsCTChiTiet, v_str_Ma_NNT)
                ' CaculateTongTien(dsCTChiTiet.Tables(0))
            Else
                lblStatus.Text = "Không lấy được thông tin chi tiết của chứng từ.Hãy kiểm tra lại"
                Exit Sub
            End If

            ShowHideButtons(v_strTT, v_strTTSP)
            'ShowHideControlByLoaiThue(v_strLoaiThue)

        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

            'Throw ex
        Finally
            'Catch ex As Exception
            '    logger.Error(ex.StackTrace)
        End Try
    End Sub

    Private Sub FillDataToCTHeader(ByVal dsCT As DataSet)
        Try
            Dim strTenVT = String.Empty
            Dim strTenLH As String = String.Empty
            Dim strMaLH As String = String.Empty
            Dim dr As DataRow = dsCT.Tables(0).Rows(0)
            Dim v_strTrangThai As String = dr("TRANG_THAI").ToString()
            Dim v_strHTTT As String = GetString(dr("PT_TT")).ToString
            txtMaNNT.Text = dr("MA_NNTHUE").ToString()
            txtTenNNT.Text = dr("TEN_NNTHUE").ToString()
            txtDChiNNT.Text = dr("DC_NNTHUE").ToString()
            txtMaNNTien.Text = dr("Ma_NNTien").ToString()
            txtTenNNTien.Text = dr("TEN_NNTIEN").ToString()
            txtDChiNNTien.Text = dr("DC_NNTIEN").ToString()
            txtNgayNNT.Text = dr("ngay_ntien").ToString()
            txtSHKB.Text = dr("SHKB").ToString()
            txtTenKB.Text = dr("TEN_KB").ToString()

            txtNgayKB.Text = ConvertNumbertoString(dr("NGAY_KB").ToString())

            txtMaDThu.Text = dr("MA_DTHU").ToString()

            txtSo_BT.Text = dr("SO_BT").ToString()

            txtNgayChungTu.Text = dr("NGAY_CTU").ToString()
            txtTKCo.Text = dr("TK_Co").ToString()
            txtTKNo.Text = dr("TK_No").ToString()
            ddlMaLThue.SelectedValue = dr("MA_LTHUE").ToString()
            txtCQQD.Text = dr("CQ_QD").ToString()
            txtMaCQThu.Text = dr("MA_CQTHU").ToString()
            txtTenCQThu.Text = dr("TEN_CQTHU").ToString()
            txtMaDBHC.Text = dr("MA_DBHC").ToString()
            txtTenDBHC.Text = dr("TEN_DBHC").ToString()
            txtTK_KH_NH.Text = GetString(dr("TK_KH_NH"))
            txtNgayKH_NH.Text = GetString(dr("NGAY_KH_NH"))
            txtSTK_KH_Nhan.Text = GetString(dr("TK_KH_NHAN"))
            txtTenKH_Nhan.Text = GetString(dr("TENKH_NHAN"))
            txtSTK_KH_Nhan.Text = GetString(dr("TK_KH_NHAN"))
            txtDiaChiKH_Nhan.Text = GetString(dr("DIACHI_KH_NHAN"))

            txtMaNH_TK_NNT.Text = GetString(dr("NH_GIU_TKNNT"))
            txtMaNH_TK_KBNN.Text = GetString(dr("NH_GIU_TKKB"))

            ddlMaNT.SelectedValue = dr("MA_NT").ToString()
            txtTyGia.Text = VBOracleLib.Globals.Format_Number(dr("Ty_Gia").ToString(), ",")
            txtTTienVND.Text = dr("TTIEN").ToString()
            txtTTienNT.Text = VBOracleLib.Globals.Format_Number(GetString(dr("ttien_nt")).ToString(), ",")
            'Event: Them ma hai quan
            txtMaHQ.Text = dr("MA_HQ").ToString()
            txtTenMaHQ.Text = dr("TEN_HQ").ToString()
            txtMaHQPH.Text = dr("MA_HQ_PH").ToString()
            txtTenMaHQPH.Text = dr("TEN_HQ_PH").ToString()

            txtTimeLap.Text = GetString(dr("ngay_ht"))
         
            txtDienGiai.Text = GetString(dr("REMARK"))
            txtContentEx.Text = GetString(dr("CONTENT_EX")).Replace("<", "[").Replace(">", "]")

            txtNgayCitad.Text = GetString(dr("ngay_citad"))
            txtSoBTCitad.Text = GetString(dr("sobt_citad"))
            txtTen_Nguoi_chuyen.Text = GetString(dr("TEN_NGUOI_CHUYEN"))


            txtRM_REF_NO.Text = dr("SO_CT_NH").ToString()
            txtREF_NO.Text = dr("SO_CT_NH").ToString()
            txtSoCT_NH.Text = dr("SO_CT_NH").ToString()

            txtRefNoSHB.Text = dr("REF_NO").ToString()

            'If dr("LY_DO_HUY").ToString().Trim <> "" Then
            '    txtLyDoHuy.Text = dr("LY_DO_HUY").ToString()
            'Else
            '    txtLyDoHuy.Text = dr("LY_DO").ToString()
            'End If
            txtLyDoCTra.Text = dr("LY_DO").ToString()
            txtLyDoHuy.Text = dr("LY_DO_HUY").ToString()
            hdfTrangThai_CT.Value = dr("trang_thai").ToString().Trim
            hdfTTSP.Value = dr("tt_SP").ToString().Trim
            hdfDienGiai.Value = dr("ghi_chu").ToString().Trim
            hdfMACN.Value = dr("ma_cn").ToString().Trim

            Dim v_dt As DataTable = Nothing

        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình FillDataToCTHeader")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

            Throw ex
        Finally
        End Try
    End Sub

    Private Sub FillDataToCTDetail(ByVal pv_dsCT As DataSet, ByVal v_strMa_NNT As String)
        Try
            Dim v_dt As DataTable = pv_dsCT.Tables(0)
            Dim dr As DataRow
            Dim sodathu As String
            grdChiTiet.DataSource = v_dt
            grdChiTiet.DataBind()

        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub



    Private Function GetTenLoaiThue(ByVal pv_strMaLoaiThue As String) As String
        Select Case pv_strMaLoaiThue
            Case "01"
                Return "Thuế nội địa"
            Case "02"
                Return "Thuế thu nhập cá nhân"
            Case "03"
                Return "Thuế chước bạ"
            Case "04"
                Return "Thuế Hải Quan"
            Case "05"
                Return "Thu khác"
            Case "06"
                Return "Thu tài chính"
            Case Else '05'
                Return "Phạt vi phạm hành chính"
        End Select
    End Function

    Private Sub GetThongTinTKKH(ByVal v_strHTTT As String, ByVal v_strTrangThaiCT As String, _
                    ByVal pv_strSoTKKH As String, ByRef pv_strTenKH As String, ByRef pv_strTTKH As String)
        If (pv_strSoTKKH.Length > 0) Then 'Lay theo sotk
            If v_strHTTT.Equals("01") Then 'Chuyen Khoan
                If v_strTrangThaiCT.Equals("05") Then 'Cho KS lay thong tin tu corebank

                Else 'Cac truong hop khac thi lay thong tin tu db

                End If
            End If
        End If
    End Sub

    Private Sub ShowHideButtons(ByVal strTT As String, ByVal strTTSP As String)
        If strTT <> "01" Then
            lbSoFT.Text = ""
        End If
        Select Case strTT
            Case "02"
                lblStatus.Text = "Chờ kiểm soát"
                cmdHuy.Enabled = False
                'cmdKS.Enabled = True
                cmdChuyenTra.Enabled = True
            Case "03"
                lblStatus.Text = "Hạch toán thành công"
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdHuy.Enabled = False
            Case "04"
                lblStatus.Text = "Chuyển trả"
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdHuy.Enabled = False
            Case "05"
                lblStatus.Text = "Chờ duyệt hủy"
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdHuy.Enabled = True
            Case "06"
                lblStatus.Text = "Đã hủy"
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdHuy.Enabled = False
            Case "19"
                lblStatus.Text = "Điều chỉnh chờ kiểm soát"
                'cmdKS.Enabled = False
                cmdChuyenTra.Enabled = False
                cmdHuy.Enabled = False
        End Select

        cmdChuyenThue.Enabled = False

        If strTT = "19" Then
            cmdChuyenThue.Enabled = True
        End If

        If strTTSP = "0" Or strTTSP = "" Then
            If strTT = "03" Then
                cmdChuyenThue.Enabled = True
                cmdHuy.Enabled = False
            End If
        End If
        'If strTTSP = "1" Then
        '    If strTT = "04" Then
        '        cmdChuyenThue.Enabled = True
        '    End If
        'End If

    End Sub

    Private Sub ShowHideCTRow(ByVal pv_blnDisplayRow As Boolean)
        'rowDChiNNT.Visible = pv_blnDisplayRow
        'rowNNTien.Visible = pv_blnDisplayRow
        'rowDChiNNTien.Visible = False

        'rowDBHC.Visible = pv_blnDisplayRow
        'rowCQThu.Visible = pv_blnDisplayRow
        'rowTKNo.Visible = False

        'rowTK_KH_NH.Visible = pv_blnDisplayRow

        'rowMaNT.Visible = pv_blnDisplayRow

        'rowNNTien.Visible = pv_blnDisplayRow

        'rowDChiNNTien.Visible = pv_blnDisplayRow

        'rowMaHQPH.Visible = pv_blnDisplayRow
        'rowSHKB.Visible = pv_blnDisplayRow
        'rowMaHQ.Visible = pv_blnDisplayRow

    End Sub

    Private Sub ShowHideControlByLoaiThue(ByVal pv_strLoaiThue As String)
        Select Case pv_strLoaiThue
            Case "01" 'NguonNNT.NNThue_CTN, NguonNNT.NNThue_VL, NguonNNT.SoThue_CTN, NguonNNT.VangLai
                'An thong tin To khai

            Case "02"

            Case "03" 'NguonNNT.TruocBa

            Case "04" 'NguonNNT.HaiQuan

            Case Else '"05" Khong xac dinh
        End Select
    End Sub

    Private Sub ShowHideControlByPT_TT(ByVal pv_strPT_TT As String, ByVal pv_strTT As String)

    End Sub

    Protected Sub cmdChuyenTra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyenTra.Click
        Try

            If txtLyDoCTra.Text.ToString().Trim() = "" Then
                lblStatus.Text = "Bạn phải nhập vào lý do chuyển trả."
                Response.Write("<SCRIPT LANGUAGE=""JavaScript"">  alert('Bạn phải nhập vào lý do chuyển trả.');</script>")
                Exit Sub
            End If
            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = Business.ChungTuSP.buChungTu.CTU_Get_TrangThai(v_strSo_CT)
                Dim strTT_Form As String = v_strTrangThai_CT

                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    Exit Sub
                End If
                If strTT_Data = "02" Then
                    Business.ChungTuSP.buChungTu.CTU_SetTrangThai(v_strSo_CT, "04", "", "", "", "", txtLyDoCTra.Text.ToString(), True)
                    LoadDSCT(ddlTrangThai.SelectedValue)
                    EnabledButtonWhenDoTransaction(False)
                    cmdHuy.Enabled = False
                    lblStatus.Text = "Chuyển trả chứng từ thành công."
                    Exit Sub
                ElseIf strTT_Data = "05" Then
                    Business.ChungTuSP.buChungTu.CTU_SetTrangThai(v_strSo_CT, "06", "", "", "", "", txtLyDoCTra.Text.ToString(), True)
                    LoadDSCT(ddlTrangThai.SelectedValue)
                    EnabledButtonWhenDoTransaction(False)
                    Dim strTK_KH_NH As String = txtTK_KH_NH.Text.Trim
                    Dim strPTTinhPhi As String = ""
                End If
                lblStatus.Text = "Chuyển trả chứng từ thành công."
            Else
                lblStatus.Text = "Không tìm thấy chứng từ cần tìm.Hãy thử lại"
                Exit Sub
            End If
        Catch ex As Exception
            logger.Error(ex.StackTrace)
            logger.Error("Error source: " & ex.Source & vbNewLine _
                  & "Error code: System error!" & vbNewLine _
                  & "Error message: " & ex.Message)
        End Try
    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function ValidNHSHB_SP(ByVal strLanKS As String, ByVal strSo_ct As String) As String
        Dim strResult As String = ""
        Dim dt As New DataTable
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If
        Dim sMaCN As String = "13.3"
        If Business.SP.SongPhuongController.checkPhanQuyen(HttpContext.Current.Session("User"), sMaCN) = False Then
            Return "ssF;User của bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Dim strClick As String = "0"
        strClick = Business.ChungTuSP.buChungTu.CTU_Get_Valid_LanKS_Citad(strSo_ct)

        If strClick.Length > 0 Then
        Else
            strClick = "0"
        End If

        Return strClick

    End Function

    Protected Sub cmdKSBK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdKSBK.Click

        'Dim intRowcount1 As Integer
        'Dim str_Desc1 As String = BuildRemarkForPayment(intRowcount1)

        Dim Mota As String = ""
        Dim NewValue As String = ""
        Dim Action As String = "KIEMSOAT_CT_SP_KIEMSOAT"
        Dim v_blnReturn As Boolean = False
        Dim CITAD_VERSION As String = ""
        Dim strErrorNum As String = ""
        Dim strErrorMes As String = ""
        Dim pv_so_tn_ct As String = ""
        Dim pv_ngay_tn_ct As String = ""
        Dim MSGType As String = ""
        Dim v_strTransaction_id As String = String.Empty

        Try
            'Disable button tranh lam viec 2 lan
            EnabledButtonWhenDoTransaction(False)
            Dim strUser = mv_objUser.Ten_DN.ToString
            'Check Hạn mức
            'Dim tien_tongTN As Decimal = Convert.ToDecimal(txtTTienNT.Text.ToString().Replace(".", "").Replace(",", ""))

            'If Not Business.Common.mdlCommon.CheckHanMuc_KSV(Session.Item("User").ToString(), tien_tongTN) Then
            '    lblStatus.Text = "Hạn mức của bạn không đủ để duyệt giao dịch này."
            '    Exit Sub
            'End If


            Dim v_strErrorCode As String = String.Empty
            Dim v_strErrorMessage As String = String.Empty
            Dim v_objGDV As CTuUser
            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            v_objGDV = New CTuUser(v_strMa_NV)

            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value

            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu
                '
                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = Business.ChungTuSP.buChungTu.CTU_Get_TrangThai(v_strSo_CT)
                Dim strTT_Form As String = v_strTrangThai_CT

                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If
                'vinhvv
                'Cập nhật số lần nhấn kiểm soát: check cột save_type
                Dim solan As Integer = 0
                Dim strClick As String = Business.ChungTuSP.buChungTu.CTU_Get_LanKS_Citad(v_strSo_CT)
                If strClick.Length > 0 Then
                    solan = CInt(strClick) + 1
                    strClick = solan.ToString()
                Else
                    strClick = "1"
                End If
                Business.ChungTuSP.buChungTu.CTU_SetLanKS_Citad(v_strSo_CT, strClick)
                'end cập nhật số lần
                'Lấy trực tiếp phương thức thanh toán trong DB
                Dim strTongTienTT As String = txtTTienNT.Text.Replace(".", "").Replace(",", "")


                Dim isGL As Boolean = True

                Dim v_strMaNNT As String = hdfMaNNT.Value
                Dim strMaNNT As String = txtMaNNT.Text.Trim
                Dim strTenNNT As String = Globals.RemoveSign4VietnameseString(txtTenNNT.Text.Trim)
                Dim strDiaChiNNT As String = Globals.RemoveSign4VietnameseString(txtDChiNNT.Text.Trim)
                Dim strSoCT_NH As String = txtSoCT_NH.Text.Trim
                Dim strErrorCode As String = ""
                Dim strErrorMessege As String = ""
                Dim a As Integer = 1

                Dim str_AccountNo As String = txtTK_KH_NH.Text.Trim
                Dim str_HTTT As String

                Dim intRowcount As Integer

                Dim str_clk As String = ""
                str_clk = BuildCKTM()
                Dim str_loaiPhi As String = ""
                'If rdTypeMP.Checked = True Then
                '    str_loaiPhi = "W"
                'ElseIf rdTypePN.Checked = True Then
                '    str_loaiPhi = "O"
                'Else
                str_loaiPhi = "B"
                'End If
                Dim str_TKKB As String = txtTKCo.Text.Trim
                Dim str_TenKB As String = Globals.RemoveSign4VietnameseString(txtTenKB.Text.Trim)
                Dim str_TenCQT As String = Globals.RemoveSign4VietnameseString(txtTenCQThu.Text.Trim)
                If str_TenKB.Length > 70 Then
                    str_TenKB = str_TenKB.Substring(0, 69)
                End If
                If str_TenCQT.Length > 70 Then
                    str_TenCQT = str_TenCQT.Substring(0, 69)
                End If

                Dim str_CIFNo As String = ""

                Dim str_RefNo As String = txtREF_NO.Text.Trim
                Dim strLoaiTien As String = HaiQuan.HaiQuan.GetCODE_TIENTE(ddlMaNT.SelectedValue.ToString) '"704" 'txtMaNT.Text
                'Dim str_MaSanPham As String = txtSanPham.Text.Trim
                Dim str_TimeInput As String = txtTimeLap.Text
                Dim strTKCo As String = txtTKCo.Text.Trim
                Dim TRAN_ID As String = ""
                Dim ngay_GD As String = ConvertNumberToDate(mv_objUser.Ngay_LV).ToString("dd/MM/yyyy")
                Dim TT_CITAD As String = ""
                If strTKCo.Substring(0, 3) = "711" Then
                    TT_CITAD = "1"
                Else
                    TT_CITAD = "0"
                End If
                Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)

                CITAD_VERSION = clsCTU.TCS_GETTS("CITAD_VERSION")

                Dim trx_date As String = mv_objUser.Ngay_LV
                Dim create_date As Date = ConvertNumberToDate(v_strNgay_KB)
                Dim currency As String = strLoaiTien
                Dim amount As String = strTongTienTT
                Dim sd_name As String = strTenNNT
                Dim sd_addr As String = strDiaChiNNT
                Dim sd_accnt As String = ""
                Dim blnSuccess As Boolean = True
                Dim desMSG_HQ As String = ""


                Dim rv_name As String = str_TenKB
                Dim rv_acctn As String = str_TKKB & "." & txtMaCQThu.Text.Trim

                Dim rv_addr As String = "MaCQT" & txtMaCQThu.Text.ToString() & Get_TenCQThue_DAB(txtMaCQThu.Text.Trim)
                Dim rv_accnt As String = strTKCo
                Dim content As String = "NopThue"   'giống MSB-Mac dinh là NOPTHUE
                Dim relation_no As String = Get_SoRelation_No()
                Dim content_ex As String = ""
                Dim reference As String = ""


                Dim relation_no_ref As String = ""
                Dim ghi_chu As String = ""
                ''''Dim strMa_SP As String = txtSanPham.Text.Trim.Split("-")(0).ToString

                Dim v_content As String = String.Empty
                Dim v_contentEx As String = String.Empty

                NewValue = "newvalue=""SO_CT[SO_CT=" & v_strSo_CT & ",Ma_NNT=" & strMaNNT & ",SHKB= " & v_strSHKB & ", Ma_CN=" & v_objGDV.MA_CN & ",Ten_DN= " & mv_objUser.Ten_DN & " ]"" Description: " & ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) & " Exception:"
                Dim makerUser As HeThong.infUser = HeThong.buUsers.NHANVIEN_DETAIL(v_strMa_NV)

                Dim str_PayMent As String = ""
                Dim rm_ref_no As String = ""
                Dim req_Internal_rmk As String = ""
                Dim sTenMaCN As String = ""
                sTenMaCN = Get_TenCN(hdfMACN.Value)
                req_Internal_rmk = "" 'hdfMACN.Value & "-" & sTenMaCN

                ''cho ham moi de goi thanh toan ngan hang khac
                Dim sMACN As String = hdfMACN.Value
                Dim ngay_GD_SP As String = ConvertNumberToDate(mv_objUser.Ngay_LV).ToString("yyyyMMdd")

                Dim str_Desc As String = txtDienGiai.Text.Trim
                Dim strMaNT As String = ddlMaNT.SelectedValue.ToString
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'check SP tại SHB
                Dim cls_songphuong As New CorebankServiceESB.clsSongPhuong

                Dim core_citad As String = clsCTU.TCS_GETTS("CORE_CITAD")

                If core_citad.Equals("1") Then
                    Dim strResult As String = cls_songphuong.ValiadSHB(v_strSHKB)

                    Dim strNHB As String = ""
                    Dim strACC_NHB As String = ""
                    Dim strMA_CN_SP As String = ""
                    Dim strTK_Trunggian As String = ""
                    If strResult.Length > 8 Then
                        Dim strarr() As String
                        strarr = strResult.Split(";")
                        If strarr.Length >= 4 Then
                            strNHB = strarr(0).ToString
                            strACC_NHB = strarr(1).ToString
                            strMA_CN_SP = strarr(2).ToString
                            strTK_Trunggian = strarr(3).ToString
                        End If
                    Else

                        Mota = " object: " & v_strSo_CT & " result: Fail add_info: " & NewValue
                        LogApp.AddDebug(Action, Mota)
                        lblStatus.Text = "Đã có lỗi xảy ra khi lấy thông tin kho bạc từ song phương."
                        EnabledButtonWhenDoTransaction(True)
                        Exit Sub
                    End If

                    If strTK_Trunggian.Trim.Length <= 0 Then
                        Mota = " object: " & v_strSo_CT & " result: Fail add_info: " & NewValue
                        LogApp.AddDebug(Action, Mota)
                        lblStatus.Text = "Không lấy được tài khoản trung gian từ song phương."
                        EnabledButtonWhenDoTransaction(True)
                        Exit Sub
                    End If
                    ''đi theo hàm thanh toán mới, sau đó gửi song phương
                    Dim strMACN_TK As String = ""

                    strMACN_TK = sMACN

                    'cap nhat TTSP ve 1
                    'ChungTu.buChungTu.CTU_Update_TTSP(v_strSo_CT, "1")
                    str_PayMent = cls_corebank.PaymentSP("TAX", ngay_GD_SP, v_strSo_CT, sMACN, strMACN_TK, strTK_Trunggian, strTongTienTT, strMaNT, req_Internal_rmk, str_Desc, _
                             v_strMa_NV, mv_objUser.Ma_NV, "CITAD", strTKCo, strNHB.ToString().Substring(2, 3), strNHB, strACC_NHB, strMA_CN_SP, v_strErrorCode, rm_ref_no)
                Else
                    v_strErrorCode = "00000"
                    rm_ref_no = txtSoCT_NH.Text.Trim
                End If

                If v_strErrorCode = "00000" Then
                    Mota = " object: result: Succes add_info: " & NewValue
                    LogApp.AddDebug(Action, Mota)
                    'Cập nhật lại trạng thái của chứng từ
                    v_strTrangThai_CT = "03" 'Trạng thái chưa cập nhật vào BDS
                    lbSoFT.Text = "Số FT:" & rm_ref_no
                    Business.ChungTuSP.buChungTu.CTU_SetTrangThai(v_strSo_CT, "03", "", "", "", "", "", True)

                    'cap nhat TG_KY
                    DataAccess.ExecuteNonQuery("update tcs_ctu_songphuong_hdr set tg_ky = sysdate, REF_NO='" & rm_ref_no & "' where so_ct='" & v_strSo_CT & "'", CommandType.Text)

                    lblStatus.Text = "Kiểm soát chứng từ thành công.Xin chờ một lát để gửi thông tin sang song phương"

                    strErrorNum = cls_songphuong.SendChungTuSP(v_strSo_CT, "CITAD")

                Else
                    Mota = " object: " & v_strSo_CT & " result: Fail add_info: " & NewValue
                    LogApp.AddDebug(Action, Mota)
                    lblStatus.Text = "Đã có lỗi xảy ra khi hạch toán chứng từ."
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If

                If strErrorNum.ToUpper().Equals("SUCCESS") Then
                    lblStatus.Text = "Chứng từ đã được hạch toán và chuyển số liệu sang song phương thành công"
                Else
                    blnSuccess = False
                    lblStatus.Text = "Chứng từ đã được hạch toán. Có lỗi khi gửi số liệu sang song phương. Mã lỗi " & strErrorNum & " Mô tả lỗi: " & strErrorMes
                    'cmdKS.Enabled = False
                    cmdChuyenTra.Enabled = False
                    LoadDSCT(ddlTrangThai.SelectedValue)

                End If
                'CAP NHAT TRANG THAI DA CHUYEN SONG PHUONG

                LoadDSCT(ddlTrangThai.SelectedValue)
                'ShowHideCTRow(True)
                EnabledButtonWhenDoTransaction(False)
                'lblStatus.Text = "Chứng từ đã được hạch toán thành công"
                cmdChuyenTra.Enabled = False
                'cmdHuy.Enabled = True
                hdfTrangThai_CT.Value = v_strTrangThai_CT
                hdfTrangThai_CThue.Value = IIf(blnSuccess, "1", "0")
                hdfTTSP.Value = IIf(blnSuccess, "1", "0")
            End If

        Catch ex As Exception
            Mota = " object:  result: Fail  add_info: " & NewValue
            LogApp.AddDebug(Action, Mota & ex.Message)
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình kiểm soát chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())
            lblStatus.Text = sbErrMsg.ToString
            Throw ex
        End Try
    End Sub



    Private Function BuildDTLCitadXML(ByRef intRowcount As Integer) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim str_KTBatDau As String = "<VSTD>"
        Dim str_KTKetThuc As String = "</VSTD>"
        Dim v_strRetRemark As String = String.Empty

        'For i = 0 To v_intRowCnt - 1
        '    v_strRetRemark &= str_KTBatDau
        '    v_strRetRemark &= "<MCH>" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text & "</MCH>"
        '    v_strRetRemark &= "<NDK>" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text & "</NDK>"
        '    v_strRetRemark &= "<STN>" & CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text.Replace(",", "") & "</STN>"
        '    v_strRetRemark &= "<NDG>" & Globals.RemoveSign4VietnameseString(CType(grdChiTiet.Items(i).Controls(5).Controls(1), TextBox).Text) & "</NDG>"
        '    v_strRetRemark &= str_KTKetThuc
        'Next
        'intRowcount = v_intRowCnt
        Return v_strRetRemark
    End Function

    Private Function BuildCKTM() As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim v_strRetRemark As String = String.Empty

        'For i = 0 To v_intRowCnt - 1
        '    v_strRetRemark &= "C" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text
        '    v_strRetRemark &= "T" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text
        'Next

        Return v_strRetRemark
    End Function

    Protected Sub cmdHuy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdHuy.Click
        LogApp.AddDebug("cmdHuy_Click", "KIEM SOAT CHUNG TU: Huy chung tu")
        Dim confirmValue As String = Request.Form("confirm_value")
        Dim i_char As Integer = confirmValue.LastIndexOf(",")
        i_char = i_char + 1
        confirmValue = confirmValue.Substring(i_char, confirmValue.Length - i_char)
        If confirmValue = "Yes" Then
        Else
            Exit Sub
        End If
        Dim v_blnReturn As Boolean = False

        Try

            'Disable button tranh lam viec 2 lan
            EnabledButtonWhenDoTransaction(False)
            cmdHuy.Enabled = False

            'Kiểm tra mật khẩu kiểm soát
            Dim strUser = mv_objUser.Ten_DN.ToString

            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            Dim v_strMaNNT As String = hdfMaNNT.Value
            Dim v_strTT_ChuyenThue As String = hdfTrangThai_CThue.Value
            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""
            Dim v_strTransaction_id As String = ""

            Dim strLyDoHuy As String = ""
            strLyDoHuy = txtLyDoHuy.Text.Trim()

            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu


                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = Business.ChungTuSP.buChungTu.CTU_Get_TrangThai(v_strSo_CT)
                Dim strTT_Form As String = v_strTrangThai_CT
                Dim descERR_HQ As String = ""
                Dim str_TrangThaiCT_moi As String = ""
                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    cmdHuy.Enabled = True
                    Exit Sub
                End If

                Business.ChungTuSP.buChungTu.CTU_SetTrangThai(v_strSo_CT, "06", "", v_strMa_NV, "", "", strLyDoHuy, True)
                'LoadDSCT(ddlTrangThai.SelectedValue)
                EnabledButtonWhenDoTransaction(False)
                cmdHuy.Enabled = False
                lblStatus.Text = "Duyệt hủy chứng từ thành công."


            End If
            'Load lai chung tu
            LoadDSCT("99")
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình hủy chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

            ' Throw ex
        Finally
        End Try
    End Sub

    Private Sub EnabledButtonWhenDoTransaction(ByVal blnEnabled As Boolean)
        'cmdKS.Enabled = blnEnabled
        cmdChuyenTra.Enabled = blnEnabled
    End Sub

    Protected Sub btnSelectCT_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnSelectCT As LinkButton = CType(sender, LinkButton)
            Dim v_strRetArgument As String() = btnSelectCT.CommandArgument.ToString.Split(",")
            If (v_strRetArgument.Length > 3) Then
                Dim v_strSHKB As String = v_strRetArgument(0)
                Dim v_strNgay_KB As String = v_strRetArgument(1)
                Dim v_strSo_CT As String = v_strRetArgument(2)
                Dim v_strKyHieu_CT As String = v_strRetArgument(3)
                Dim v_strSo_BT As String = v_strRetArgument(4)
                Dim v_strMa_Dthu As String = v_strRetArgument(5)
                Dim v_strMa_NV As String = v_strRetArgument(6)
                Dim v_strTrangThai_CT As String = v_strRetArgument(7)
                Dim v_strMaNNT As String = v_strRetArgument(8)
                Dim v_strSO_CT_NH As String = v_strRetArgument(9)
                Dim v_strLanClickKS As String = v_strRetArgument(10)
                'Load thông tin chi tiết khi người dùng chọn 1 CT cụ thể
                If ((Not v_strSHKB Is Nothing) And (Not v_strMa_NV Is Nothing) _
                     And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                    Dim key As New KeyCTu
                    key.Ma_Dthu = v_strMa_Dthu
                    key.Kyhieu_CT = v_strKyHieu_CT
                    key.Ma_NV = v_strMa_NV
                    key.Ngay_KB = v_strNgay_KB
                    key.SHKB = v_strSHKB
                    key.So_BT = v_strSo_BT
                    key.So_CT = v_strSo_CT
                    LoadCTChiTietAll(v_strSo_CT)

                    'Dat lai gia tri
                    hdfSHKB.Value = v_strSHKB
                    hdfNgay_KB.Value = v_strNgay_KB
                    hdfSo_CT.Value = v_strSo_CT
                    hdfKyHieu_CT.Value = v_strKyHieu_CT
                    hdfSo_BT.Value = v_strSo_BT
                    hdfMa_Dthu.Value = v_strMa_Dthu
                    hdfMa_NV.Value = v_strMa_NV
                    hdfTrangThai_CT.Value = v_strTrangThai_CT
                    hdfMaNNT.Value = v_strMaNNT
                    hdfSoCTNH.Value = v_strSO_CT_NH
                    hdfClickKS.Value = v_strLanClickKS
                End If
                lbSoCT.Text = "Số CT: " & v_strSo_CT
                lbSoCT.Visible = True

                lbSoFT.Text = "Số FT: " & v_strSO_CT_NH
                lbSoFT.Visible = True
            End If
        Catch ex As Exception
            logger.Error(ex.StackTrace)
        End Try
    End Sub

    Protected Sub ddlTrangThai_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTrangThai.SelectedIndexChanged
        LoadDSCT(ddlTrangThai.SelectedValue)
        ClearCTHeader()
        CreateDumpChiTietCTGrid()
        ShowHideCTRow(False)
    End Sub

    Private Function BuildRemarkForPayment(ByVal pv_strTenNNT As String, ByVal pv_strMaNNT As String, _
                                 ByVal v_strMaNHNhan As String, ByVal pv_strNgayNop As String, ByVal pv_strMaNHTT As String) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim v_strRetRemark As String = String.Empty
        ' v_strRetRemark &= "MST:" & pv_strMaNNT & ","
        'For i = 0 To v_intRowCnt - 1
        '    v_strRetRemark &= "(C" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text
        '    v_strRetRemark &= ".TM" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text
        '    'v_strRetRemark &= ",KT" & Globals.RemoveSign4VietnameseString(CType(grdChiTiet.Items(i).Controls(8).Controls(1), TextBox).Text.ToUpper)
        '    v_strRetRemark &= ".ST:" & CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text.Replace(",", "")
        '    v_strRetRemark &= ")"
        '    v_strRetRemark &= IIf(i < v_intRowCnt - 1, "+", "")
        'Next
        'If ddlLoaiThue.SelectedValue = "04" Then
        '    v_strRetRemark &= " .TK:" & txtToKhaiSo.Text ' Globals.RemoveSign4VietnameseString(v_strMaNHNhan)
        '    v_strRetRemark &= " .N:" & txtNgayDK.Text
        '    v_strRetRemark &= " .LH:" & txtLHXNK.Text
        'End If
        Return v_strRetRemark
    End Function

    Protected Sub grdDSCT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdDSCT.PageIndexChanging
        Dim v_ds As DataSet = GetDSCT(ddlTrangThai.SelectedValue)
        grdDSCT.PageIndex = e.NewPageIndex
        grdDSCT.DataSource = v_ds
        grdDSCT.DataBind()
    End Sub

    Protected Sub cmdChuyenThue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdChuyenThue.Click
        LogApp.AddDebug("cmdChuyenThue_Click", "KIEM SOAT CHUNG TU SP: Chuyen thue")
        Dim v_blnReturn As Boolean = False
        'Dim strNguyenTe As String = ""
        Try
            Dim strUser = mv_objUser.Ten_DN.ToString
           
            EnabledButtonWhenDoTransaction(False)
            cmdChuyenThue.Enabled = False

            'Dat lai gia tri
            Dim v_strSHKB As String = hdfSHKB.Value
            Dim v_strNgay_KB As String = hdfNgay_KB.Value
            Dim v_strSo_CT As String = hdfSo_CT.Value
            Dim v_strKyHieu_CT As String = hdfKyHieu_CT.Value
            Dim v_strSo_BT As String = hdfSo_BT.Value
            Dim v_strMa_Dthu As String = hdfMa_Dthu.Value
            Dim v_strMa_NV As String = hdfMa_NV.Value
            Dim v_strTrangThai_CT As String = hdfTrangThai_CT.Value
            'Neu ma trang thai 19 thi chuyen thanh trang thai 01 de sau do chuyen sang hai quan

            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""
            Dim v_strTransaction_id As String = ""
            Dim pv_so_tn_ct_ych As String = ""
            Dim pv_so_tn_ct As String = ""
            Dim pv_ngay_tn_ct As String = ""
            Dim hdr As New infDoiChieuNHHQ_HDR
            Dim dtls() As infDoiChieuNHHQ_DTL
            hdr.ten_kb = Globals.RemoveSign4VietnameseString(txtTenKB.Text)
            If ((Not v_strSHKB Is Nothing) _
                 And (Not v_strSo_BT Is Nothing) And (Not v_strSo_CT Is Nothing)) Then
                Dim key As New KeyCTu
                key.Kyhieu_CT = v_strKyHieu_CT
                key.So_CT = v_strSo_CT
                key.SHKB = v_strSHKB
                key.Ngay_KB = v_strNgay_KB
                key.Ma_NV = CInt(v_strMa_NV)
                key.So_BT = v_strSo_BT
                key.Ma_Dthu = v_strMa_Dthu

                'Kiểm tra trạng thái 1 lần nữa trước khi ghi
                Dim strTT_Data As String = Business.ChungTuSP.buChungTu.CTU_Get_TrangThai(v_strSo_CT)
                Dim strTT_Form As String = v_strTrangThai_CT

                If (strTT_Data <> strTT_Form) Then
                    lblStatus.Text = "Trạng thái của chứng từ đã thay đổi. Hãy 'Refresh', rồi thực hiện lại"
                    EnabledButtonWhenDoTransaction(True)
                    Exit Sub
                End If
                Dim blnSuccess As Boolean = True
                Dim MSGType As String = ""
                Dim desMSG_HQ As String = ""
                Try
                    Dim cls_songphuong As New CorebankServiceESB.clsSongPhuong
                    'gui lai song phuong ơ day
                    strErrorNum = cls_songphuong.SendChungTuSP(v_strSo_CT, "CITAD")
                    ''Neu gui thong tin thanh cong
                    Dim str_TTSP As String = String.Empty
                    If strErrorNum = "SUCCESS" Then
                        'ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT)
                        lblStatus.Text = "Chứng từ đã chuyển số liệu sang song phương thành công"
                    Else
                        lblStatus.Text = "Có lỗi khi gửi số liệu sang song phương. Mã lỗi " & strErrorNum & " Mô tả lỗi: " & strErrorMes
                        'ChungTu.buChungTu.CTU_SetTrangThai(key, v_strTrangThai_CT)
                        cmdChuyenThue.Enabled = True
                    End If
                Catch ex As Exception
                    blnSuccess = False
                    lblStatus.Text = "Chứng từ chưa chuyển được số liệu sang song phương. " & ex.Message
                    Exit Sub
                End Try



            End If
            LoadDSCT(ddlTrangThai.SelectedValue)
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình chuyển sang song phương")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

            ' Throw ex
        Finally
        End Try
    End Sub

    Protected Sub grdChiTiet_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdChiTiet.ItemDataBound

        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            'If Not e.Item.ItemType.ToString() = "Header" Then
            'e.Item.Cells(7).Text = VBOracleLib.Globals.Format_Number(e.Item.Cells(7).Text, ",")
            Dim SOTIEN As TextBox = CType(e.Item.FindControl("SOTIEN"), TextBox)
            SOTIEN.Text = VBOracleLib.Globals.Format_Number(SOTIEN.Text, ",")
            Dim SOTIEN_NT As TextBox = CType(e.Item.FindControl("SOTIEN_NT"), TextBox)
            SOTIEN_NT.Text = VBOracleLib.Globals.Format_Number2(SOTIEN_NT.Text)
        End If
    End Sub


    Protected Sub ddlMacker_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMacker.SelectedIndexChanged

        LoadDSCT(ddlTrangThai.SelectedValue)
        ClearCTHeader()
        CreateDumpChiTietCTGrid()
        ShowHideCTRow(False)

    End Sub

    Protected Sub cmdRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefresh.Click
        LogApp.AddDebug("cmdRefresh_Click", "KIEM SOAT CHUNG TU HQ: Lam moi danh sach chung tu")
        LoadDSCT(ddlTrangThai.SelectedValue)
    End Sub





    Private Sub TCS_IN_GNT_HQ()
        Try
            If hdfSo_CT.Value.Length > 0 Then
                clsCommon.OpenNewWindow(Me, "../BaoCao/frmInGNT_HQ.aspx?so_ct=" & hdfSo_CT.Value, "ManPower")
                'Response.Redirect("../BaoCao/frmInGNT_HQ.aspx?so_ct=" & hdfSo_CT.Value)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub



    Private Function BuildRemarkForPaymentCITAD20(ByVal pv_strTenNNT As String, ByVal pv_strMaNNT As String, _
                                  ByVal v_strMaNHNhan As String, ByVal pv_strNgayNop As String, ByVal pv_strMaNHTT As String) As String
        Dim v_intRowCnt As Integer = CInt(grdChiTiet.Items.Count.ToString)
        Dim v_strRetRemark As String = String.Empty

        v_strRetRemark &= "MST:" & pv_strMaNNT & ","
        For i = 0 To v_intRowCnt - 1
            'SoTK 1
            v_strRetRemark &= "TK" & CType(grdChiTiet.Items(i).Controls(2).Controls(1), TextBox).Text
            v_strRetRemark &= ",N" & CType(grdChiTiet.Items(i).Controls(5).Controls(1), TextBox).Text
            v_strRetRemark &= ",LH" & CType(grdChiTiet.Items(i).Controls(4).Controls(1), TextBox).Text
            v_strRetRemark &= "(C" & CType(grdChiTiet.Items(i).Controls(7).Controls(1), TextBox).Text
            v_strRetRemark &= ",TM" & CType(grdChiTiet.Items(i).Controls(9).Controls(1), TextBox).Text
            v_strRetRemark &= ",ST:" & CType(grdChiTiet.Items(i).Controls(11).Controls(1), TextBox).Text.Replace(",", "")
            v_strRetRemark &= ");"
        Next

        v_strRetRemark &= "CT:" & txtTenKB.Text.Trim
        'v_strRetRemark &= ".TAI:" & txtTen_NH_B.Text.Trim

        Return v_strRetRemark
    End Function

    ''Get money customer
    'Public Shared Function GetTK_KH_NH(ByVal pv_strTKNH As String) As Decimal
    '    Dim sRet As Decimal = 0
    '    Try
    '        Dim strXML As String = String.Empty
    '        Dim ma_loi As String = ""
    '        Dim so_du As String = ""
    '        strXML = cls_corebank.GetTK_KH_NH(pv_strTKNH.Replace("-", ""), ma_loi, so_du)

    '        Dim doc As New XmlDocument()
    '        doc.LoadXml(strXML)
    '        Dim xnList As XmlNodeList = doc.SelectNodes("/PARAMETER")

    '        For Each itemNode As XmlNode In xnList
    '            Dim nodeParams As XmlNode = itemNode.SelectSingleNode("PARAMS")
    '            If nodeParams IsNot Nothing Then
    '                Decimal.TryParse(nodeParams("ACY_AVL_BAL").InnerText.Replace(".", "").Replace(",", ""), sRet)
    '            End If
    '        Next

    '    Catch ex As Exception
    '        sRet = 0
    '        Throw ex
    '    End Try

    '    Return sRet
    'End Function
    Public Shared Function V_Substring(ByVal pv_s As String, ByVal length As Integer) As String
        If Not pv_s Is Nothing And pv_s.Length > length Then
            pv_s = pv_s.Substring(0, length)
        End If
        Return pv_s
    End Function
End Class
