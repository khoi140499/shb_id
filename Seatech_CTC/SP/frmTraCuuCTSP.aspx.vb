﻿Imports System.Data
Imports Business.NTDT.NNTDT

Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao
Imports log4net
Imports Business_HQ.Common.mdlCommon

Partial Class SP_frmTraCuuCTSP
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                Response.Cookies("ASP.NET_SessionId").Value = String.Empty
                Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
            End If
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If

        If Not IsPostBack Then
            Load_TrangThai()
            Load_Loai_HS()
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub
    Private Sub load_dataGrid()
        Dim ds As DataSet

        Dim WhereClause As String = ""
        Try
            dtgrd.DataSource = Nothing
            dtgrd.DataBind()
            If ConvertDateToNumber(txtTuNgay.Value) > ConvertDateToNumber(txtDenNgay.Value) And txtDenNgay.Value.ToString.Length > 0 Then
                clsCommon.ShowMessageBox(Me, "Điều kiện Từ ngày phải nhỏ hơn hoặc bằng Đến ngày")
                Exit Sub
            End If
            If txtMST.Text <> "" Then
                WhereClause += " AND upper(A.MA_NNTHUE) like upper('%" & txtMST.Text.Trim & "%')"
            End If
            If txtTen_NNT.Text <> "" Then
                WhereClause += " AND upper(A.TEN_NNTHUE) like upper('%" & txtTen_NNT.Text.Trim & "%')"
            End If
         
            If txtTuNgay.Value <> "" Then
                WhereClause += " AND to_char(A.TG_KY,'RRRRMMDD') >=" & ConvertDateToNumber(txtTuNgay.Value)
            End If
            If txtDenNgay.Value <> "" Then
                WhereClause += " AND to_char(A.TG_KY,'RRRRMMDD') <= " & ConvertDateToNumber(txtDenNgay.Value)
            End If
            If txtSoCTU.Text.Length > 0 Then
                WhereClause += " AND A.SO_CT = '" & Business.CTuCommon.SQLInject1(Business.CTuCommon.SQLInject2(txtSoCTU.Text.Trim())) & "' "
            End If
            If txtSO_CT_NH.Text.Length > 0 Then
                WhereClause += " AND A.SO_CT_NH = '" & Business.CTuCommon.SQLInject1(Business.CTuCommon.SQLInject2(txtSO_CT_NH.Text.Trim())) & "' "
            End If
            If cboTrangThai.SelectedIndex > 0 Then
                WhereClause += " AND A.TRANG_THAI = '" & cboTrangThai.SelectedValue.ToString() & "' "
            End If


            ds = Business.SP.SongPhuongController.getDien_NHKhac_TracuuXuly(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgrd.DataSource = ds.Tables(0)
                dtgrd.PageIndex = 0
                dtgrd.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub
    Private Sub Load_TrangThai()
        Try

            Dim sql = "SELECT * FROM tcs_dm_trangthai_sp_bl "
            Dim dr As DataSet = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            cboTrangThai.DataSource = dr.Tables(0)
            cboTrangThai.DataTextField = "MOTA"
            cboTrangThai.DataValueField = "TRANGTHAI"
            cboTrangThai.DataBind()
            cboTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái yêu cầu. Lỗi: " & ex.Message)
        End Try
    End Sub

    Private Sub Load_Loai_HS()
        Try



        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách loại hồ sơ người nộp thuế. Lỗi: " & ex.Message)
        End Try
    End Sub



    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        load_dataGrid()

    End Sub


    Protected Function ReSENDHQ(ByVal strID As String) As Integer
        Try
            Dim strErrorNum As String = "0"
            Dim strErrorMes As String = ""
            Dim v_strTransaction_id As String = ""
            Dim pv_so_tn_ct As String = ""
            Dim pv_ngay_tn_ct As String = ""
            Dim ds As DataSet = Business_HQ.HQ247.daCTUHQ304.getMSG304Detail(strID)
            If ds Is Nothing Then
                Return 1
            End If
            If ds.Tables.Count <= 0 Then
                Return 1
            End If
            If ds.Tables(0).Rows.Count <= 0 Then
                Return 1
            End If
            Dim strMST As String = ds.Tables(0).Rows(0)("MA_DV").ToString()
            Dim strSO_CTU As String = ds.Tables(0).Rows(0)("SO_CTU").ToString()
            CustomsServiceV3.ProcessMSG.guiCTThueHQ247(strMST, strSO_CTU, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
            If Not strErrorNum.Equals("0") Then
                Business_HQ.HQ247.daCTUHQ304.TCS_HQ247_TRANGTHAI_304_SEND301_ERROR(strID, Session.Item("UserName").ToString())
                Return 1
            End If
            'doan nay cho doi chieu
            Dim decResult As Decimal = Business_HQ.HQ247.daCTUHQ304.TCS_HQ247_TRANGTHAI_304_SEND301_OK(strID, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct, Session.Item("UserName").ToString())
            If decResult > 0 Then
                Return 1
            End If
            Return 0
        Catch ex As Exception
            Return 1
        End Try
        Return 1
    End Function
    Protected Sub dtgrd_PageIndexChanging(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dtgrd.PageIndexChanging
        load_dataGrid()
        dtgrd.PageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

End Class
