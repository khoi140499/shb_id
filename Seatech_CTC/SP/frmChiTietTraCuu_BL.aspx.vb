﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.HQ247.NNTDT
Imports Business.Common.mdlCommon
Imports Business.BaoCao
Imports Business.Common
Imports System.IO
Imports System.Xml
Imports CustomsV3.MSG.MSG304
Imports System.Collections.Generic
Imports System.Diagnostics
Imports Business_HQ
Imports HQ247XulyMSG
Imports log4net.Repository.Hierarchy

Partial Class SP_frmChiTietTraCuu_BL
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim v_strRootPath As String = ConfigurationManager.AppSettings("APP_ROOT_PATH").ToString
        Dim iResult As Integer = 0
        Dim strSql As String = " SELECT COUNT (1) KIEMTRA   FROM ("
        strSql &= "  SELECT A.MA_CN           FROM TCS_DM_CHUCNANG A , TCS_PHANQUYEN B , TCS_DM_NHANVIEN C , TCS_SITE_MAP D         "
        strSql &= "   WHERE UPPER(C.TEN_DN)=UPPER('" & Session.Item("UserName").ToString() & "') AND D.STATUS = 1  AND D.NAVIGATEURL = '" + HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") + "' "
        strSql &= "  AND (A.MA_CN=D.NODEID OR (A.MA_CN=D.PARENTNODEID ) )"
        strSql &= "  AND A.MA_CN=B.MA_CN AND B.MA_NHOM =C.MA_NHOM ) "

        If HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") <> "~/pages/frmHomeNV.aspx" Then
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
            If Not Globals.IsNullOrEmpty(dt) Then
                iResult = CInt(dt.Rows(0)(0).ToString())
            End If
        End If

        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                If Not Request.Params("ID") Is Nothing Then
                    Dim NNTDT_ID As String = Request.Params("ID").ToString
                    Dim strErrMsg As String = ""
                    If NNTDT_ID <> "" Then
                        LoadCTChiTietAll(NNTDT_ID)
                    End If
                End If
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Lỗi: " & ex.Message)
            End Try
        End If

    End Sub
    Private Sub LoadCTChiTietAll(ByVal sSOCT As String)
        Try
            Dim v_strTT As String = String.Empty
            Dim v_strTTSP As String = String.Empty
            Dim v_strLoaiThue As String = String.Empty
            Dim v_strPhuongThucTT As String = String.Empty
            Dim v_str_Ma_NNT As String = String.Empty
            Dim v_strSO_CT As String = sSOCT
            Dim vStrProduct As String = ""
            'Load chứng từ header
            Dim dsCT As New DataSet

            Dim objCTU As Business.BienLai.infoCTUBIENLAI = Nothing
            objCTU = Business.BienLai.buBienLai.GetCTUDetailCT(sSOCT)
            hdfMACN.Value = objCTU.MA_CN
            txtSoCTU.Text = objCTU.SO_CT
            txtNgayCTU.Text = objCTU.NGAY_CT
            txtSoRefTTSP.Text = objCTU.REF_CORE_TTSP
            txtSoBL.Text = objCTU.SOBL
            txtKH_BL.Text = objCTU.KYHIEU_BL
            txtSHKB.Text = objCTU.SHKB
            txtTenKB.Text = objCTU.TEN_KB
            txtCQQD.Text = objCTU.CQQD
            txtTenCQQD.Text = objCTU.TEN_CQQD
            txtMaCQThu.Text = objCTU.MA_CQTHU
            txtMA_DBHC.Text = objCTU.MA_DBHC
            txtMA_LOAIHINH.Text = objCTU.MA_LHTHU
            txtTEN_LOAIHINH.Text = objCTU.TEN_LHTHU
            txtTK_NSNN.Text = objCTU.TK_NS
            txtMA_CHUONG.Text = objCTU.MA_CHUONG
            txtMA_TMUC.Text = objCTU.MA_NDKT
            txtNOIDUNG.Text = objCTU.NOI_DUNG
            txtLoaiBienLai.SelectedValue = objCTU.LOAIBIENLAI
            txtMaNNT.Text = objCTU.MA_NNTHUE
            txtTenNNT.Text = objCTU.TEN_NNTHUE
            'GIAYTO_NNTIEN
            txtMaNNTien.Text = objCTU.GIAYTO_NNTIEN
            txtTenNNTien.Text = objCTU.TEN_NNTIEN
            txtDIACHI.Text = objCTU.DC_NNTIEN
            txtSO_QD.Text = objCTU.SO_QD
            txtNGAY_QD.Text = objCTU.NGAY_QD
            txtTongTien.Text = VBOracleLib.Globals.Format_Number(objCTU.TTIEN, ".")
            txtMA_NT.Text = objCTU.MA_NT
            txtTTIEN_VPHC.Text = VBOracleLib.Globals.Format_Number(objCTU.TTIEN_VPHC, ".")
            txtLYDO.Text = objCTU.LY_DO
            txtTTIEN_CHAMNOP.Text = VBOracleLib.Globals.Format_Number(objCTU.TTIEN_NOP_CHAM, ".")
            txtLYDO_CHAMNOP.Text = objCTU.LY_DO_NOP_CHAM
            txtGHICHU.Text = objCTU.DIEN_GIAI
            ddlMaHTTT.SelectedValue = objCTU.HINH_THUC
            If objCTU.HINH_THUC.Equals("01") Then

                txtTK_KH_NH.Text = objCTU.TK_KH_NH
                txtTenTK_KH_NH.Text = objCTU.TEN_KH_NH
                'txtTKGL.Text = objCTU.TTIEN_VPHC
            Else
                'txtTenTKGL.Text = objCTU.TEN_KH_NH
                txtTK_KH_NH.Text = objCTU.TK_KH_NH
                txtTenTK_KH_NH.Text = " "
                'txtTKGL.Text = objCTU.TK_KH_NH
            End If

            txtNGAY_KH_NH.Text = objCTU.NGAY_CT
            txtSoRefTTSP.Text = objCTU.SO_CT_NH
            txtLYDO_TC.Text = objCTU.LY_DO_TUCHOI

            'hdfSoRefCore.Text = objCTU.REF_CORE_TTSP
            'hdfSo_CT.Text = objCTU.SO_CT
            txtSoBL.Text = objCTU.SOBL
            txtCharge.Text = VBOracleLib.Globals.Format_Number(objCTU.PHI_GD, ".")
            txtVAT.Text = VBOracleLib.Globals.Format_Number(objCTU.PHI_VAT, ".")

            txtTTienx.Text = VBOracleLib.Globals.Format_Number(objCTU.TTIEN, ".")

            txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number((Decimal.Parse(objCTU.TTIEN) + Decimal.Parse(objCTU.PHI_GD) + Decimal.Parse(objCTU.PHI_VAT)).ToString, ".")



            Dim MA_NV As String = String.Empty
            Dim TRANG_THAI As String = String.Empty
            Dim MA_CN_USER As String = String.Empty

            MA_CN_USER = objCTU.MA_CN
            TRANG_THAI = objCTU.TRANG_THAI
            MA_NV = objCTU.MA_NV
            hdfSo_CT.Value = objCTU.SO_CT
            ' hdfSoRefCore.Value = objCTU.REF_CORE_TTSP
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

            If Not ex.InnerException Is Nothing Then
                sbErrMsg = New StringBuilder()

                sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ ")
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.Message)
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.StackTrace)

                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)
            End If

            'Throw ex
        Finally
            'Catch ex As Exception
            '    logger.Error(ex.StackTrace)
        End Try
    End Sub

    Private Sub FillDataToCTHeader(ByVal dsCT As DataSet)
        Try
            Dim strTenVT = String.Empty
            Dim strTenLH As String = String.Empty
            Dim strMaLH As String = String.Empty

            Dim MA_NV As String = String.Empty
            Dim TRANG_THAI As String = String.Empty
            Dim MA_CN_USER As String = String.Empty

            Dim dr As DataRow = dsCT.Tables(0).Rows(0)

            hdfSo_CT.Value = dr("so_ct").ToString()
            MA_CN_USER = dr("MA_CN").ToString()
            'hdfTrangThai_CT.Value = dr("trang_thai").ToString().Trim
            'hdfTTSP.Value = dr("tt_SP").ToString().Trim
            'hdfDienGiai.Value = dr("dien_giai").ToString().Trim
            'hdfMACN.Value = dr("ma_cn").ToString().Trim



        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình FillDataToCTHeader")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

            If Not ex.InnerException Is Nothing Then
                sbErrMsg = New StringBuilder()

                sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ ")
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.Message)
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.StackTrace)

                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)
            End If

            Throw ex
        Finally
        End Try
    End Sub

    Private Sub FillDataToCTDetail(ByVal pv_dsCT As DataSet, ByVal v_strMa_NNT As String)
        Try
            Dim v_dt As DataTable = pv_dsCT.Tables(0)
            Dim dr As DataRow
            Dim sodathu As String
            'grdChiTiet.DataSource = v_dt
            'grdChiTiet.DataBind()

        Catch ex As Exception

        End Try
    End Sub
    Public Function convertDateHQ(ByVal strDate As String) As String

        Dim str As String = ""
        Try
            str = strDate.Substring(8, 2) & "/" & strDate.Substring(5, 2) & "/" & strDate.Substring(0, 4)
            If strDate.Trim.Length > 10 Then
                str &= strDate.Substring(10, strDate.Length - 10)
            End If
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

        End Try

        Return str
    End Function

    Protected Sub btnIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIn.Click
        LogApp.AddDebug("cmdInCT_Click", "In GNT chứng từ biên lai")
        Try
            TCS_IN_GNT()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình in giấy nộp tiền chứng từ biên lai!")
        Finally
            btnIn.Enabled = True
        End Try
    End Sub

    Private Sub TCS_IN_GNT()
        Try
            If hdfSo_CT.Value.Length > 0 Then
                clsCommon.OpenNewWindow(Me, "../pages/BaoCao/frmInGNT_BL.aspx?so_ct=" & hdfSo_CT.Value, "ManPower")
                'Response.Redirect("../BaoCao/frmInGNT_HQ.aspx?so_ct=" & hdfSo_CT.Value)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub btnInBL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInBL.Click
        LogApp.AddDebug("btnInBL_Click", "In biên lai")
        Try
            TCS_IN_BIENLAI()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình in biên lai!")
        Finally
            btnIn.Enabled = True
        End Try
    End Sub

    Private Sub TCS_IN_BIENLAI()
        Try
            If hdfSo_CT.Value.Length > 0 Then
                clsCommon.OpenNewWindow(Me, "../pages/BaoCao/frmInBL.ashx?so_ct=" & hdfSo_CT.Value, "ManPower")
            End If
            LoadCTChiTietAll(hdfSo_CT.Value)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub TCS_IN_BIENLAI()
    '    Try
    '        If hdfSo_CT.Value.Length > 0 Then
    '            clsCommon.OpenNewWindow(Me, "../../pages/BaoCao/frmInBL.aspx?so_ct=" & hdfSo_CT.Value, "ManPower")
    '            window.open("../../pages/BaoCao/frmInBL.ashx?so_ct=" + strSoCT, "", params);
    '        End If

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub


    Protected Sub btnInBangKe_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInBangKe.Click
        LogApp.AddDebug("btnInBangKe_Click", "In bảng kê")
        Try
            TCS_IN_BANGKE()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình in bảng kê!")
        Finally
            btnIn.Enabled = True
        End Try
    End Sub

    Private Sub TCS_IN_BANGKE()
        Try
            If hdfSo_CT.Value.Length > 0 Then
                clsCommon.OpenNewWindow(Me, "../pages/BaoCao/frmInBangKe.aspx?so_ct=" & hdfSo_CT.Value, "ManPower")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
