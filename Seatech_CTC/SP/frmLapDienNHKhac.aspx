﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/shared/MasterPage05_1.master"
    EnableEventValidation="true" CodeFile="frmLapDienNHKhac.aspx.vb" Inherits="SP_frmLapDienNHKhac" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script language="javascript" src="../javascript/CheckDate.js" type="text/javascript"></script>

    <script language="javascript" src="../javascript/ChuyenSoSangChu.js" type="text/javascript"></script>

    <script language="javascript" src="../javascript/popup.js" type="text/javascript"></script>

    <script src="../javascript/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>

    <script src="../javascript/jquery/1.11.2/jquery-ui.min.js" type="text/javascript"></script>

    <script src="../javascript/json/json2.js" type="text/javascript"></script>

    <script src="../javascript/accounting.min.js" type="text/javascript"></script>

    <script src="../javascript/object/chungTu.js" type="text/javascript"></script>

    <script src="../javascript/table/paging.js" type="text/javascript"></script>

    <link href="../javascript/table/paging.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript">

        //Bien javascript cuc bo
        var maTienTe = 'VND';
        var feeRate = 2;
        var minFee = 10000;
        var isGL = false;
        function infHDRCompare(SoTK, Ma_HQ_PH, Ma_HQ_CQT, Ma_NTK, TKKB, MaSoThue, TTNo, TTNo_CT) {
            this.SoTK = SoTK;
            this.Ma_HQ_PH = Ma_HQ_PH;
            this.Ma_HQ_CQT = Ma_HQ_CQT;
            this.Ma_NTK = Ma_NTK;
            this.TKKB = TKKB;
            this.MaSoThue = MaSoThue;
            this.TTNo = TTNo;
            this.TTNo_CT = TTNo_CT;
        }

        $(document).ready(function () {

            try {
                $('#grdDSCT').paging({ limit: 15 });
            } catch (err) {
                paginationList(15);
            }

            $('#ddlMa_NTK').change(function () {
                $('#ddlMaTKCo').val('');
            });

            $("#ddlMaNT option[value='VND']").prop('selected', true);

            //$('#ddlMaNT').change(); 
        });

        function paginationList(listPaginationCount) {
            $('table#grdDSCT').each(function () {
                var currentPage = 0;
                var numPerPage = listPaginationCount;
                var $pager = $('.pager').remove();
                var $paging = $("#paging").remove();
                var $table = $(this);
                $table.bind('repaginate', function () {
                    $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
                });
                $table.trigger('repaginate');
                var numRows = $table.find('tbody tr').length;
                var numPages = Math.ceil(numRows / numPerPage);
                var $pager = $('<div id="pager"></div>');

                for (var page = 0; page < numPages; page++) {
                    $('<span class="page-number"></span>').text(page + 1).bind('click', {
                        newPage: page
                    }, function (event) {
                        currentPage = event.data['newPage'];
                        $table.trigger('repaginate');
                        $(this).addClass('active').siblings().removeClass('active');
                    }).appendTo($pager).addClass('clickable');

                }

                $pager.insertAfter($table).find('span.page-number:first').addClass('active');
            });
        }

        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }

        function jsCheckYear(elementInput) {
            var elmInput = elementInput;
            var yearNum = elmInput.value;
            if (yearNum !== '') {
                if (!validYear(yearNum)) {
                    alert('Năm truyền vào không hợp lệ');
                    elmInput.value = "";
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }

        function validYear(yearNum) {
            var txt = /^[0-9]+$/;
            if (yearNum) {
                if (yearNum.length != 4) {
                    return false;
                } else {
                    if (txt.test(yearNum)) {
                        if ((parseInt(yearNum) < 1000) || (parseInt(yearNum) > 3000)) {
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }



        function jsGet_SacThue(pvSacThue) {
            var maST = '';
            //console.log(arrSacThue);
            if (typeof arrSacThue !== 'undefined' && arrSacThue !== null) {
                if ((arrSacThue instanceof Array) && (arrSacThue.length > 0)) {
                    for (var i = 0; i < arrSacThue.length; i++) {
                        var arr = arrSacThue[i].split(';');
                        if (arr[0] == pvSacThue) {
                            maST = arr[1];
                            break;
                        }
                    }
                }
            }
            return maST;
        }


        function jsCalTotal() {
            var dblTongTien = 0;
            var dblPhi = 0;
            var dblVAT = 0;
            var dblTongTrichNo = 0;

            if ($get('SOTIEN_1').value.length > 0) {
                if (document.getElementById("chkRemove1").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_1').value.replaceAll('.', ''));
                    jsFormatNumber('SOTIEN_1');
                }
            }
            if ($get('SOTIEN_2').value.length > 0) {
                if (document.getElementById("chkRemove2").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_2').value.replaceAll('.', ''));
                    jsFormatNumber('SOTIEN_2');
                }
            }

            if ($get('SOTIEN_3').value.length > 0) {
                if (document.getElementById("chkRemove3").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_3').value.replaceAll('.', ''));
                    jsFormatNumber('SOTIEN_3');
                }
            }

            if ($get('SOTIEN_4').value.length > 0) {
                if (document.getElementById("chkRemove4").checked) {
                    dblTongTien += parseFloat($get('SOTIEN_4').value.replaceAll('.', ''));
                    jsFormatNumber('SOTIEN_4');
                }
            }
            if (document.getElementById("ddlMaHTTT").value == "01" || document.getElementById("ddlMaHTTT").value == "00") {

                dblVAT = $get('txtVAT').value.replaceAll('.', '');
                dblPhi = $get('txtCharge').value.replaceAll('.', '');

                $get('txtTongTien').value = dblTongTien;

                dblTongTrichNo = parseFloat(dblPhi) + parseFloat(dblVAT) + parseFloat(dblTongTien);

                $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo)).change();
                jsFormatNumber('txtTongTien');
                jsFormatNumber('txtCharge');
                jsFormatNumber('txtVAT');
                //jsFormatNumber('txtTongTrichNo');
                $get('hdnTongTienCu').value = $get('txtTongTien').value;

            } else {

                $get('txtCharge').value = 0;
                $get('txtVAT').value = 0;
                $get('txtTongTien').value = dblTongTien;
                dblTongTrichNo = dblTongTien;

                $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo)).change();
                jsFormatNumber('txtTongTien');
                jsFormatNumber('txtCharge');
                jsFormatNumber('txtVAT');
                jsFormatNumber('txtTongTrichNo');
                $get('hdnTongTienCu').value = $get('txtTongTrichNo').value;

            }

            if ($("#txtTongTien").val().replaceAll('.', '') != _TTIEN) {
                _TTIEN = $("#txtTongTien").val().replaceAll('.', '');
                ResetTinhPhi();
            }

        }

        function jsCheckUserSession(param) {
            var bRet = true;
            if (param) {
                var arrKQ = param.split(';');
                if (arrKQ[0] == "errUserSession") {
                    window.open("../../pages/Warning.html", "_self");
                    bRet = false;
                }
            }
            return bRet;
        }
        function jsThem_Moi() {
            jsEnableDisableControlButton("0");

            jsFillNguyenTe();
            //jsFillMA_NH_A();
            jsFillPTTT();

            jsLoadCTList();

            //clear data
            $('#hdfSo_CTu').val('');
            $('#hdfIsEdit').val(false);
            $('#hdfDescCTu').val('');

            $('#hdfHQResult').val('');
            $('#hdfHDRSelected').val('');
            $('#hdfNgayKB').val('');

            // jsBindKQTruyVanGrid(null);
            jsClearHDR_Panel();
            jsClearDTL_Grid()
            jsBindDTL_Grid(null, null);

            //jsCalCharacter();
            jsEnableDisableControlButton("1");
            jsEnableDisableAddRowButton("1");
        }

        function jsLoadCTList() {
            var sSoCtu = $('#txtTraCuuSoCtu').val();
            if (sSoCtu == 'undefined' || sSoCtu == '' || sSoCtu == null)
                sSoCtu = '';
            PageMethods.LoadCTList(curMa_NV, sSoCtu, LoadCTList_Complete, LoadCTList_Error);
        }

        function LoadCTList_Complete(result, methodName) {
            if (result.length > 0) {
                jsCheckUserSession(result);
                document.getElementById('divDSCT').innerHTML = result;
            }
            try {
                $('#grdDSCT').paging({ limit: 15 });
            } catch (err) {
                paginationList(15);
            }

        }

        function LoadCTList_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy danh sách chứng từ" + error.get_message();
            }
        }
        //xxx)Lay thong tin tai ngan hang song phuong
        function jsValidNHSHB_SP() {
           // console.log("1.txtSHKB: " + $get('txtSHKB').value);
            if ($get('txtSHKB').value.length > 0) {
                PageMethods.ValidNHSHB_SP($get('txtSHKB').value.trim(), ValidNHSHB_SP_Complete, ValidNHSHB_SP_Error);
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Không tìm thấy SHKB';
            }
        }

        function ValidNHSHB_SP_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            if (result.length < 8) {
                $get('txtMaNH_TK_KBNN').value = '';
                document.getElementById("hndNHSHB").value = '';
            } else {
                document.getElementById("hndNHSHB").value = result;
                if (result.length > 0) {
                    $get('txtMaNH_TK_KBNN').value = result;
                }
            }
        }
        function ValidNHSHB_SP_Error(error, userContext, methodName) {
            if (error !== null) {

                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin ngân hàng giữ tài khoản kho bạc" + error.get_message();
            }
        }
       
        //xxx)Lay thong tin tai ngan hang song phuong jsValidNHSHB_SP_GhiCtu
        function jsValidNHSHB_SP_GhiCtu(strSHKB) {
            if ($get('txtSHKB').value.length > 0) {
                PageMethods.ValidNHSHB_SP($get('txtSHKB').value.trim(), ValidNHSHB_SP_GhiCtu_Complete, ValidNHSHB_SP_GhiCtu_Error);
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Không tìm thấy SHKB';
            }
        }

        function ValidNHSHB_SP_GhiCtu_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            if (result.length < 8) {
                document.getElementById("hndNHSHB").value = '';
            } else {
                document.getElementById("hndNHSHB").value = result;
            }
        }
        function ValidNHSHB_SP_GhiCtu_Error(error, userContext, methodName) {
            if (error !== null) {

                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin ngân hàng hưởng" + error.get_message();
            }
        }
        //xxx)Lay thong tin tai ngan hang song phuong
        function jsGetMaDBHC() {
            //console.log("2.txtSHKB: " + $get('txtSHKB').value);
            if ($get('txtSHKB').value.length > 0) {
                PageMethods.GetMaDBHC($get('txtSHKB').value, GetMaDBHC_Complete, GetMaDBHC_Error);
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Không tìm thấy SHKB';
            }
        }

        function GetMaDBHC_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            //console.log("GetMaDBHC: " + result);
            if (result.length > 0) {
                $get('txtMaDBHC').value = result;
            } else {
                $get('txtMaDBHC').value = '';
            }
           
        }
        function GetMaDBHC_Error(error, userContext, methodName) {
            if (error !== null) {

                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin mã địa bàn hành chính" + error.get_message();
            }
        }
        function jsGet_NNThue_HQ() {


            if ($('#txtFilter_MaSoThue').val().length == 0) {
                alert("Vui lòng nhập mã số thuế để truy vấn thông tin nợ thuế Hải quan!");
                $('#txtFilter_MaSoThue').focus();
                return;
            }

            jsShowDivStatus(false, "");
            jsShowHideDivProgress(true);
            jsEnableDisableControlButton('4');
            //clear data before proccess
            $('#hdfSo_CTu').val('');
            $('#hdfIsEdit').val(false);
            $('#hdfDescCTu').val('');

            //$('#hdfHQResult').val('');
            $('#hdfHDRSelected').val('');

            $('#hdfNgayKB').val('');

            jsClearHDR_Panel();
            jsBindDTL_Grid(null, null);


            var strMaSoThue = $('#txtFilter_MaSoThue').val();
            var strSoTK = $('#txtFilter_SoToKhai').val();
            var strNgayDK = $('#txtFilter_NgayDKy').val();

            //Kiem tra xem co ma so thue chua => neu chua co => set hdfHQResult = null
            if ($('#hdfMST').val().length == 0) {
                $('#hdfMST').val($('#txtFilter_MaSoThue').val());
                $('#hdfHQResult').val('');
            } else {   //Neu co kiem tra xem ma so thue lan truoc voi lan truy van hien tai co trung khop nhau khong?
                if ($('#hdfMST').val() != $('#txtFilter_MaSoThue').val()) {
                    if (!confirm("Mã số thuế truy vấn hiện tại khác mã số thuế truy vấn lần trước. Bạn có muốn truy vấn không?"))
                        return;
                    else {   //reset hdfHQResult = null
                        $('#hdfMST').val($('#txtFilter_MaSoThue').val());
                        $('#hdfHQResult').val('');
                    }
                }
            }


            $.ajax({
                type: "POST",
                url: "frmLapCTu_HQ.aspx/Get_NNThue_HQ",
                cache: false,
                data: JSON.stringify({
                    "sMaSoThue": strMaSoThue,
                    "sSoTK": strSoTK || null,
                    "sNgayDK": strNgayDK || null
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: GetNNThueHQ_OnSuccess,
                failure: function (failure) {
                    if (failure !== null) {
                        var err;
                        err = JSON.parse(failure.responseText);
                        if (err) {
                            if (err.Message.indexOf(';') > -1) {
                                jsCheckUserSession(err.Message);
                            }
                            jsShowDivStatus(true, err.Message);
                        }
                    }
                    jsEnableDisableAddRowButton("1");
                    jsShowHideDivProgress(false);
                },
                error: function (request, status, error) {
                    var err;
                    err = JSON.parse(request.responseText);
                    if (err) {
                        if (err.Message.indexOf(';') > -1) {
                            jsCheckUserSession(err.Message);
                        }
                        jsShowDivStatus(true, err.Message);
                    }
                    jsEnableDisableAddRowButton("1");
                    jsShowHideDivProgress(false);
                }
            });
        }

        function GetNNThueHQ_OnSuccess(response) {
            var jsonObj = null;
            jsShowHideGet_NNThue_HQ(true);

            if (typeof response !== 'undefined' && response !== null) {
                if (response.d) {
                    if (!jsCheckUserSession(response.d)) {
                        return;
                    }
                    jsonObj = JSON.parse(response.d);
                    if (jsonObj) {
                        //set object vua truy van vao objChung
                        jsonObj.Data.Item = setHQresult(jsonObj.Data.Item);
                        //Save object to hiddenfield
                        $('#hdfHQResult').val(JSON.stringify(jsonObj));

                    }
                }
            }
            jsEnableDisableAddRowButton("1"); //Hien thi nut them moi chi tiet
            jsShowHideDivProgress(false);

        }

        function setHQresult(arrRequestDataItem) {
            if ($('#hdfHQResult').val().length > 0) {
                var objHQ_Old = JSON.parse($('#hdfHQResult').val());
                var arrHQ_Old = [];
                arrHQ_Old = objHQ_Old.Data.Item;

                var flag = true;
                var objItemTemp = {};
                //Duyet tat ca cac Item cu => xem cai moi co trung ko => trung ko add them vao
                for (var i = 0; i < arrHQ_Old.length; i++) {
                    for (var j = 0; j < arrRequestDataItem.length; j++) {
                        if (!(arrHQ_Old[i].So_TK == arrRequestDataItem[j].So_TK && arrHQ_Old[i].Ma_NTK == arrRequestDataItem[j].Ma_NTK && arrHQ_Old[i].Ma_HQ_PH == arrRequestDataItem[j].Ma_HQ_PH &&
                                arrHQ_Old[i].Ma_HQ_CQT == arrRequestDataItem[j].Ma_HQ_CQT && arrHQ_Old[i].TKKB == arrRequestDataItem[j].TKKB)) {
                            flag = true;
                        } else {
                            flag = false;
                            break;
                        }
                    }
                    if (flag)
                        arrRequestDataItem.push(arrHQ_Old[i])
                }
            }

            return arrRequestDataItem;
        }

        function jsCheckTKhai(param) {
            //lấy thông tin mã và tên căn cứ theo msg HQ trả về chứ ko raise change event để lấy theo etax
            //Get all checked header rows from hidden field:
            var arrHDRSelected = [];
            if ($('#hdfHDRSelected').val().length > 0) {
                arrHDRSelected = JSON.parse($('#hdfHDRSelected').val());
            }

            //Procedure:
            var grdKQTruyVan = $('#grdKQTruyVan');
            if ($('#HDR_chkSelect_' + param).is(":checked")) { //checked - if compare result is true => add to hdfHDRList
                var sSoTK, sMa_HQ_PH, sMa_HQ_CQT, sMa_NTK, sTKKB, sMaSoThue;

                sSoTK = grdKQTruyVan.find('#HDR_SO_TK_' + param).val().trim();
                sMa_HQ_PH = grdKQTruyVan.find('#HDR_Ma_HQ_PH_' + param).val().trim();
                sMa_HQ_CQT = grdKQTruyVan.find('#HDR_Ma_HQ_CQT_' + param).val().trim();
                sMa_NTK = grdKQTruyVan.find('#HDR_Ma_NTK_' + param).val().trim();
                sTKKB = $('#HDR_TKKB_' + param).val().trim();
                sMaSoThue = $('#HDR_MST_' + param).val().trim();
                sTTNo = $('#HDR_TTNo_' + param).val().trim();
                sTTNo_CT = $('#HDR_TTNo_CT_' + param).val().trim();

                var objTmp = new infHDRCompare(sSoTK, sMa_HQ_PH, sMa_HQ_CQT, sMa_NTK, sTKKB, sMaSoThue, sTTNo, sTTNo_CT);

                var objRetHQ = JSON.parse($('#hdfHQResult').val());
                var arrRetHQ = [];
                if (typeof objRetHQ !== 'undefined' && objRetHQ !== null && objRetHQ.Data !== null) {
                    arrRetHQ = objRetHQ.Data.Item;
                }

                if (arrHDRSelected.length == 0) { //the first time or there are no selected row before

                    var arrDTLtmp = [];

                    arrHDRSelected.push(objTmp);
                    $('#hdfHDRSelected').val(JSON.stringify(arrHDRSelected));
                    if ((typeof arrRetHQ !== 'undefined') && (arrRetHQ !== null) && (arrRetHQ instanceof Array) && (arrRetHQ.length > 0)) {
                        for (var i = 0; i < arrRetHQ.length; i++) {
                            //So_TK la duy nhat cho moi to khai => neu da xac dinh duoc thi out of loop
                            if (arrRetHQ[i].So_TK == sSoTK && arrRetHQ[i].Ma_NTK == sMa_NTK && arrRetHQ[i].Ma_HQ_PH == sMa_HQ_PH && arrRetHQ[i].Ma_HQ_CQT == sMa_HQ_CQT && arrRetHQ[i].TKKB == sTKKB && arrRetHQ[i].TTNo == sTTNo && arrRetHQ[i].TTNo_CT == sTTNo_CT) {
                                //day du lieu DTL (bao gom thong tin HDR) vao mang tam arrDTLtmp, phuc vu viec bind DTL grid
                                arrDTLtmp.push(arrRetHQ[i]);
                                break;
                            }
                        }

                        if (typeof arrDTLtmp !== 'undefined' && arrDTLtmp !== null) {
                            //Bind HDR panel
                            if (arrDTLtmp && arrDTLtmp.length > 0) {
                                jsBindKQTruyVanHDR_Panel(arrDTLtmp[0]);
                                jsAddRowDTL_Grid2(arrDTLtmp);
                            } else {
                                jsBindKQTruyVanHDR_Panel(null);
                            }
                        }

                    }
                } else { //next time

                    var arrDTLtmp = [];
                    var diffProp = { msgRet: "" };
                    if (jsCheckTKhaiSameHeader(objTmp, "SoTK", ",", arrHDRSelected, diffProp)) {
                        //check: if not exists this item in array => add it to array
                        if (!jsCheckObjExistsInArray(objTmp, null, null, arrHDRSelected)) {

                            arrHDRSelected.push(objTmp);
                            $('#hdfHDRSelected').val(JSON.stringify(arrHDRSelected));
                            //And also bind detail grid view
                            if ((typeof arrRetHQ !== 'undefined') && (arrRetHQ !== null) && (arrRetHQ instanceof Array) && (arrRetHQ.length > 0)) {
                                if (typeof objTmp !== 'undefined' && objTmp !== null) {
                                    for (var i = 0; i < arrRetHQ.length; i++) {
                                        if (arrRetHQ[i].So_TK == objTmp.SoTK && arrRetHQ[i].Ma_NTK == objTmp.Ma_NTK && arrRetHQ[i].Ma_HQ_PH == objTmp.Ma_HQ_PH && arrRetHQ[i].Ma_HQ_CQT == objTmp.Ma_HQ_CQT && arrRetHQ[i].TKKB == objTmp.TKKB && arrRetHQ[i].TTNo == objTmp.TTNo && arrRetHQ[i].TTNo_CT == objTmp.TTNo_CT) {
                                            arrDTLtmp.push(arrRetHQ[i]);
                                            break;
                                        }
                                    }
                                    jsAddRowDTL_Grid2(arrDTLtmp); //add them row vao grid
                                }
                            }
                        }
                    } else {
                        if (diffProp && diffProp.msgRet) {
                            alert("Tờ khai này không cùng thông tin " + diffProp.msgRet + " với các tờ khai đã chọn: ");
                        } else {
                            alert("Tờ khai này không cùng thông tin chung với các tờ khai đã chọn");
                        }
                        $('#HDR_chkSelect_' + param).prop('checked', false);
                    }
                }

                //Them doan tinh toan lai so thu tu but toan
                jsGenThuTuButToan();

            } else {    //uncheck
                var sSoToKhai = '';
                sSoToKhai = grdKQTruyVan.find('#HDR_SO_TK_' + param).val();
                if (sSoToKhai) {
                    jsRemoveRowDTL_Grid2(sSoToKhai);

                    var arrHDRSelectTmp = [];
                    for (var j = 0; j < arrHDRSelected.length; j++) {
                        if (arrHDRSelected[j].SoTK !== sSoToKhai) {
                            arrHDRSelectTmp.push(arrHDRSelected[j]);
                        }
                    }
                    arrHDRSelected = arrHDRSelectTmp;
                }
                $('#hdfHDRSelected').val(JSON.stringify(arrHDRSelected));

                //if HDR selected is empty => clear form
                if (arrHDRSelected.length <= 0) {
                    jsClearHDR_Panel();
                }

            }
            jsCalcTotalMoney();


            getNoiDungByMaTieuMuc();

            changeNT();

        }

        function jsCheckTKhaiSameHeader(object, propExcept, splitChar, array, retVar) {
            var sRet = false;
            sRet = jsCheckObjExistsInArrayWithRetVar(object, propExcept, splitChar, array, retVar);

            return sRet;
        }

        function jsCheckStringInArray(str, arr) {
            if (str) {
                if (arr && arr.length > 0) {
                    for (var item in arr) {
                        if (str.trim() == arr[item].toString().trim()) {
                            return true; //break for
                        }
                    }
                }
            }

            return false;
        }

        function jsCheckObjExistsInArray(attr, value, arrTmp) {
            $.each(arrTmp, function (index, item) {
                if (item[attr].toString() == value.toString()) {
                    return true;     // break the $.each() loop
                }
            });
            return false;
        }

        function jsCheckObjExistsInArray(obj, propExcept, splitChar, array) {
            var sRet = true;
            var arrExcept = [];
            if (propExcept) {
                if (splitChar) {
                    arrExcept = propExcept.split(splitChar);
                } else {
                    arrExcept.push(propExcept);
                }
            }

            loopArray:
                for (var key in array) {
                    var arr = array[key];
                    for (var prop in obj) {
                        if (arrExcept && arrExcept.length > 0) {
                            if (!jsCheckStringInArray(prop, arrExcept)) {
                                if (obj.hasOwnProperty(prop)) {
                                    if (obj[prop].toString() !== arr[prop].toString()) {
                                        sRet = false;
                                        break loopArray;
                                    }
                                }
                            }
                        } else {
                            if (obj.hasOwnProperty(prop)) {
                                if (obj[prop].toString() !== arr[prop].toString()) {
                                    sRet = false;
                                    break loopArray;
                                }
                            }
                        }
                    }
                }

            return sRet;
        }

        function jsCheckObjExistsInArrayWithRetVar(obj, propExcept, splitChar, array, retVar) {
            var sRet = true;
            var arrExcept = [];
            if (propExcept) {
                if (splitChar) {
                    arrExcept = propExcept.split(splitChar);
                } else {
                    arrExcept.push(propExcept);
                }
            }

            loopArray:
                for (var key in array) {
                    var arr = array[key];
                    for (var prop in obj) {
                        if (arrExcept && arrExcept.length > 0) {
                            if (!jsCheckStringInArray(prop, arrExcept)) {
                                if (obj.hasOwnProperty(prop)) {
                                    if (obj[prop].toString() !== arr[prop].toString()) {
                                        sRet = false;
                                        if (retVar) {
                                            retVar.msgRet = prop.toString();
                                        }
                                        break loopArray;
                                    }
                                }
                            }
                        } else {
                            if (obj.hasOwnProperty(prop)) {
                                if (obj[prop].toString() !== arr[prop].toString()) {
                                    sRet = false;
                                    if (retVar) {
                                        retVar.msgRet = prop.toString();
                                    }
                                    break loopArray;
                                }
                            }
                        }
                    }
                }

            return sRet;
        }

        function jsClearHDR_Panel() {
            var today = new Date();

            $('#lblSoCTu').text('');
            $('#txtMaNNT').val('');
            $('#txtTenNNT').val('');
            $('#txtDChiNNT').val('');
            $('#txtMaNNTien').val('');
            $('#txtTenNNTien').val('');
            $('#txtDChiNNTien').val('');
            $('#txtNgayNNT').val('');
            $('#txtSHKB').val('');
            $('#txtTenKB').val('');
            $('#txtNgayKB').val('');
            $('#txtMaDThu').val('');
            $('#txtSoBT').val('');
            $('#txtNgayChungTu').val('');
            $('#txtTKCo').val('');
            $('#txtTKNo').val('');
            $('#txtCQQD').val('');
            $('#txtMaCQThu').val('');
            $('#txtTenCQThu').val('');
            $('#txtMaDBHC').val('');
            $('#txtTenDBHC').val('');

            $('#txtTK_KH_NH').val('');
            $('#txtNgayKH_NH').val('');
            $('#txtTenKH_Nhan').val('');
            $('#txtSTK_KH_Nhan').val('');

            $('#txtDiaChiKH_Nhan').val('');
            $('#txtMaNH_TK_NNT').val('');
            $('#txtMaNH_TK_KBNN').val('');
            $('#txtTyGia').val('1');

            $('#txtTTienVND').val('');
            $('#txtTTienNT').val('');

            $('#txtMaHQ').val('');
            $('#txtTenMaHQ').val('');

            $('#txtMaHQPH').val('');
            $('#txtTenMaHQPH').val('');

            $('#txtDienGiai').val('');


        }

        function jsBindKQTruyVanHDR_Panel(obj) {
            //Caution: get thong tin tu msg HQ tra ve, ko lay thong tin tu etax
            if (obj) {
                var today = new Date();
                $('#txtMaNNT').val(obj.Ma_DV);
                $('#txtTenNNT').val(obj.Ten_DV);
                $('#txtDChiNNT').val('');
                $('#txtHuyen_NNT').val('');
                $('#txtMaHQ').val(obj.Ma_HQ);
                //$('#txtMaHQ').val(obj.Ma_HQ);
                $('#txtTenMaHQ').val(obj.Ten_HQ);
                $('#txtMaCQThu').val(obj.Ma_HQ_CQT);
                $('#txtTenCQThu').val(obj.Ten_HQ_PH);
                $('#txtMaHQPH').val(obj.Ma_HQ_PH);
                $('#txtTenMaHQPH').val(obj.Ten_HQ_PH);
                $('#txtSHKB').val(obj.Ma_KB);
                $('#txtMaCuc').val(obj.Ma_Cuc);
                $('#txtTenCuc').val(obj.Ten_Cuc);

                $('#txtDienGiai').val(obj.REMARKS);
                $('#txtDienGiaiHQ').val(obj.DIENGIAI_HQ);

                //Handx =>SHKB => lay ra Ma ngan hang
                jsGetTTNganHangNhan();

                //$('#txtSHKB').val(obj.Ma_KB);
                $('#txtTenKB').val(obj.Ten_KB);
                $('#txtMaDBHC').val('');
                $('#txtTenDBHC').val('');
                //$('#ddlMa_NTK').val(obj.Ma_NTK);
                $('#ddlMa_NTK').val(obj.Ma_NTK);
                //$('#ddlMaTKNo').val('');
                $('#ddlMaTKCo').val(obj.TKKB);
                $('#txtMaNNTien').val('');
                $('#txtTenNNTien').val('');
                $('#txtDChiNNTien').val('');
                $('#txtQuan_HuyenNNTien').val('');
                $('#txtTinh_NNTien').val('');
                $('#ddlMaLoaiThue').val(obj.Ma_LT);
                if (typeof dtmNgayLV !== 'undefined' && dtmNgayLV !== null) {
                    $('#txtNGAY_HT').val(dtmNgayLV);
                } else {
                    $('#txtNGAY_HT').val(('0' + today.getDate()).slice(-2) + '/' + ('0' + (today.getMonth() + 1)).slice(-2) + '/' + today.getFullYear());
                }
                //$('#ddlMaNT').val($('#ddlMaNT option:first').val());
                $("#ddlMaNT option[value='VND']").prop('selected', true);
                //$('#ddlMaLoaiTienThue').val(obj.Ma_LT);
                $('#txtTK_KH_NH').val('');
                //$('#txtNGAY_KH_NH').val(('0' + today.getDate()).slice(-2) + '/' + ('0' + (today.getMonth() + 1)).slice(-2) + '/' + today.getFullYear());
                $('#ddlMA_NH_A').val($('#ddlMA_NH_A option:first').val());
                //$('#txtMA_NH_TT').val('');
                //$('#txtMA_NH_B').val('');
                $('#txtTongTien').val(obj.DuNo_TO);
                $('#txtLyDoHuy').val('');

                $('#txtCmtnd').val('');
                $('#txtDienThoai').val('');

                $('#txtTongTienNT').val(obj.DuNo_TO);
                $('#txtTyGia').val(1);
                jsLoadByCQT_Default();
            } else {
                jsClearHDR_Panel();
            }

        }

        function jsBindHDR_Panel(obj) {
            if (typeof obj !== 'undefined' && obj !== null) {
                $('#lblSoCTu').html(' - Số CT: ' + obj.SO_CT + ' - Số FT: ' + obj.SO_CT_NH);
                $('#txtMaNNT').val(obj.MA_NNTHUE);
                $('#txtTenNNT').val(obj.TEN_NNTHUE);
                $('#txtDChiNNT').val(obj.DC_NNTHUE);

                $('#txtMaNNTien').val(obj.MA_NNTIEN);
                $('#txtTenNNTien').val(obj.TEN_NNTIEN);
                $('#txtDChiNNTien').val(obj.DC_NNTIEN);
                $('#txtNgayNNT').val(obj.NGAY_NTIEN);
                $('#txtRefCore').val(obj.SO_CT_NH);
                $('#txtSHKB').val(obj.SHKB);
                $('#txtTenKB').val(obj.TEN_KB);
                $('#txtNgayKB').val(obj.NGAY_KB);
                $('#txtMaDThu').val(obj.MA_DTHU);
                $('#SoBT').val(obj.SO_BT)
                $('#txtNgayChungTu').val(obj.NGAY_CTU)
                $('#txtTKCo').val(obj.TK_CO)
                $('#txtTKNo').val(obj.TK_NO)
                if (typeof obj.MA_LTHUE !== 'undefined' && obj.MA_LTHUE !== null && obj.MA_LTHUE !== '') {
                    $('#ddlMaLThue').val(obj.MA_LTHUE);
                }
                $('#txtCQQD').val(obj.CQ_QD)
                $('#txtMaCQThu').val(obj.MA_CQTHU);
                $('#txtTenCQThu').val(obj.TEN_CQTHU);
                $('#txtMaDBHC').val(obj.MA_DBHC).change();/*change de lay len ten dia ban hanh chinh*/
                $('#txtTenDBHC').val(obj.TEN_DBHC);
                $('#txtTK_KH_NH').val(obj.TK_KH_NH);
                $('#txtNgayKH_NH').val(obj.NGAY_KH_NH);
                $('#txtTenKH_Nhan').val(obj.TENKH_NHAN);
                $('#txtSTK_KH_Nhan').val(obj.TK_KH_NHAN);
                $('#txtDiaChiKH_Nhan').val(obj.DIACHI_KH_NHAN);
                $('#txtMaNH_TK_NNT').val(obj.NH_GIU_TKNNT)
                $('#txtMaNH_TK_KBNN').val(obj.NH_GIU_TKKB)
                if (typeof obj.MA_NT !== 'undefined' && obj.MA_NT !== null && obj.MA_NT !== '') {
                    $('#ddlMaNT').val(obj.MA_NT);
                }
                $('#txtTyGia').val(obj.TY_GIA)
                $('#txtTTienVND').val(obj.TTIEN)
                $('#txtTTienNT').val(obj.TTIEN_NT)
                $('#txtMaHQ').val(obj.MA_HQ);
                $('#txtTenMaHQ').val(obj.TEN_HQ);

                $('#txtMaHQPH').val(obj.MA_HQ_PH);
                $('#txtTenMaHQPH').val(obj.TEN_HQ_PH);

                $('#txtDienGiai').val(obj.REMARK);
                $('#txtContentEx').val(obj.CONTENT_EX);

                $('#txtGhiChu').val(obj.LY_DO);
                $('#txtLyDoHuy').val(obj.LY_DO_HUY);

                if (obj.MA_NT != 'VND') {
                    $('#txtTTienNT').val(accounting.formatNumber(obj.TTIEN_NT, 2, ",", "."));

                } else {
                    //$('#txtTTienVND').val(accounting.formatNumber(obj.TTIEN));
                    //$('#txtTTienNT').val(accounting.formatNumber(obj.TTIEN_NT));
                    $('#txtTTienVND').val(accounting.formatNumber(obj.TTIEN));
                    $('#txtTTienNT').val(accounting.formatNumber(obj.TTIEN));
                }

                $('#hdfSoBT').val(obj.SO_BT);
                //$('#hdfTrang_Thai').val(obj.Trang_Thai);
                $('#txtNgayCitad').val(obj.NGAY_CITAD);
                $('#txtSoBTCitad').val(obj.SOBT_CITAD);
                $('#txtTen_Nguoi_chuyen').val(obj.TEN_NGUOI_CHUYEN);
                $('#txtRefNoSHB').val(obj.REF_NO);


                jsValidNHSHB_SP_GhiCtu(obj.SHKB);

            }
        }

        function jsBindDTL_Grid(objHDR, arrDTL) {
            jsClearDTL_Grid();

            var top = [];
            top.push("<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdChiTiet' style='width: 100%; border-collapse: collapse;'>");
            top.push("<tr class='grid_header'>");
            top.push("<td align='center' style='width: 4%;'>Chọn</td>");
            top.push("<td align='center' style='width: 10%;'>Số tờ khai</td>");
            top.push("<td align='center' style='width: 10%;'>Năm đăng ký</td>");
            top.push("<td align='center' style='width: 5%;'>Sắc thuế</td>");
            top.push("<td align='center' style='width: 5%;'>Mã chương</td>");
            top.push("<td align='center' style='width: 5%;'>Mã NDKT</td>");
            top.push("<td align='center' style='width: 27%;'>Nội dung</td>");
            top.push("<td align='center' style='width: 8%;'>Kỳ thuế</td>");
            top.push("<td align='center' style='width: 13%;'>Số tiền VND</td>");
            top.push("<td align='center' style='width: 13%;'>Số tiền NT</td>");
            top.push("<td align='center' style='display: none;'>Thông tin bút toán</td>");
            top.push("</tr>");

            var content = [];
            if (typeof arrDTL !== 'undefined' && arrDTL !== null && arrDTL instanceof Array && arrDTL.length > 0) {
                for (var i = 0; i < arrDTL.length; i++) {
                    if (arrDTL[i]) { //NOT: null, undefined, 0, NaN, false, or ""
                        content.push("<tr>");
                        content.push("<td style='text-align: center;'><input type='checkbox' id='DTL_chkSelect_" + (i + 1) + "' checked='checked'  onclick='jsCalcTotalMoney();jsGenThuTuButToan();' /></td>");
                        content.push("<td><input type='text' id='DTL_SO_TK_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrDTL[i].SO_TK + "' onblur='jsGenThuTuButToan();' /></td>");
                        content.push("<td><input type='text' id='DTL_Ngay_TK_" + (i + 1) + "' style='width: 98%; border-color: White;'  class='inputflat' value='" + arrDTL[i].NGAY_TK + "' /></td>");
                        content.push("<td><input type='text' id='DTL_SAC_THUE_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat' maxlength='2' value='" + arrDTL[i].SAC_THUE + "' /></td>");
                        content.push("<td><input type='text' id='DTL_MA_CHUONG_" + (i + 1) + "' maxlength='3' style='width: 98%; border-color: White;' class='inputflat'  value='" + arrDTL[i].Ma_Chuong + "' /></td>");
                        content.push("<td><input type='text' id='DTL_Ma_TMuc_" + (i + 1) + "' maxlength='4' style='width: 98%; border-color: White;' class='inputflat'  value='" + arrDTL[i].Ma_TMuc + "' onblur='onBlurMaTieuMuc(" + (i + 1) + ")' onkeypress='if (event.keyCode==13){ShowMLNS(1," + (i + 1) + ")}' /></td>");
                        content.push("<td><input type='text' id='DTL_Noi_Dung_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat'  value='" + arrDTL[i].Noi_Dung + "' /></td>");
                        content.push("<td><input type='text' id='DTL_KY_THUE_" + (i + 1) + "' style='width: 98%; border-color: White;' class='inputflat'  value='" + arrDTL[i].Ky_Thue + "'  maxlength='10'  /></td>");
                        if (objHDR.MA_NT != 'VND') {
                            content.push("<td><input type='text' id='DTL_SoTien_" + (i + 1) + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onfocus='this.select()' onkeyup='ValidInteger(this); jsFormatNumber(this);' maxlength='17' onblur='jsCalcTotalMoney();jsCalcCurrency(" + (i + 1) + ");' value='" + arrDTL[i].SoTien + "' /></td>");
                            content.push("<td><input type='text' id='DTL_SoTienNT_" + (i + 1) + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat'  onfocus='this.select()' onkeyup='jsFormatNumber2(this);' maxlength='17' value='" + arrDTL[i].SoTien_NT + "' /></td>");
                        }
                        else {
                            content.push("<td><input type='text' id='DTL_SoTien_" + (i + 1) + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onfocus='this.select()' onkeyup='ValidInteger(this); jsFormatNumber(this);' maxlength='17' onblur='jsCalcTotalMoney();jsCalcCurrency(" + (i + 1) + ");' value='" + arrDTL[i].SoTien + "' /></td>");
                            content.push("<td><input type='text' id='DTL_SoTienNT_" + (i + 1) + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onfocus='this.select()' onkeyup='ValidInteger(this); jsFormatNumber(this);' maxlength='17' onblur='jsCalcTotalMoney();jsCalcCurrency(" + (i + 1) + ");' value='" + arrDTL[i].SoTien + "' /></td>");

                        }

                        content.push("<td style = 'display: none;'><input type='text' id='DTL_TT_BTOAN_" + (i + 1) + "' style='width: 98%; display: none;' class='inputflat' value='" + arrDTL[i].TT_BTOAN + "' /></td>");
                        content.push("</tr>");
                    }
                }
            }

            var bottom = "</table>";

            $('#pnlCTietTTinTKhai').html(top.join("") + content.join("") + bottom);
            jsFormatNumberDTL_Grid();
        }

        function jsGenSoButToan_DTL() {
            //lấy tạm giá trị cột STT của dòng được check cuối cùng
            var iRet = 0;
            $('#grdKQTruyVan tr').each(function () {
                var ctrCheck = $(this).find("input[id^='HDR_chkSelect_']");
                var ctrInput = null;
                if (ctrCheck) {
                    if (ctrCheck.is(":checked")) {
                        ctrInput = $(this).find("input[id^='HDR_STT_']");
                        if (typeof ctrInput !== 'undefined' && ctrInput !== null && ctrInput.length > 0) {
                            iRet = ctrInput.prop("value");
                        }
                        return false; //break out each loop
                    }
                }
            });

            return iRet;
        }

        function jsFormatNumberKQTruyVanGrid() {
            $('#grdKQTruyVan tr').each(function () {
                var control = $(this).find("input[id^='HDR_DuNo_TO_']");
                if (typeof control !== 'undefined' && control !== null) {
                    control.val(accounting.formatNumber(control.val()));
                }
            });
        }

        function jsFormatNumberDTL_Grid() {
            if ($("#ddlMaNT option:selected").val() != 'VND') {
                $('#grdChiTiet tr').each(function () {
                    var control = $(this).find("input[id^='DTL_SoTien_']");
                    if (typeof control !== 'undefined' && control !== null) {
                        control.val(accounting.formatNumber(control.val()));
                    }

                    var control2 = $(this).find("input[id^='DTL_SoTienNT_']");
                    if (typeof control2 !== 'undefined' && control2 !== null) {
                        control2.val(accounting.formatNumber(control2.val(), 2, ",", "."));
                    }
                });
            }
            else {
                $('#grdChiTiet tr').each(function () {
                    var control = $(this).find("input[id^='DTL_SoTien_']");
                    if (typeof control !== 'undefined' && control !== null) {
                        control.val(accounting.formatNumber(control.val()));
                    }

                    var control2 = $(this).find("input[id^='DTL_SoTienNT_']");
                    if (typeof control2 !== 'undefined' && control2 !== null) {
                        control2.val(accounting.formatNumber(control2.val()));
                    }
                });
            }
        }

        function jsClearDTL_Grid() {
            $('#pnlCTietTTinTKhai').val('');
        }

        function jsAddDummyRowDTL_Grid(iNumberRows) {
            var sID = jsGenDTLrowID();
            var sAppendRows = '';
            sID++;
            sAppendRows = sAppendRows + '<tr>';
            sAppendRows = sAppendRows + "<td style='text-align: center;'><input type='checkbox' id='DTL_chkSelect_" + sID + "' onclick='jsCalcTotalMoney(); jsGenThuTuButToan();' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SO_TK_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='' onblur='jsGenThuTuButToan();' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Ngay_TK_" + sID + "' style='width: 98%; border-color: White;' onkeyup='if (event.keyCode !== 8){javascript:return mask(this.value,this,\"2,5\",\"/\");}' class='inputflat' maxlength='10' onblur='javascript:CheckDate(this)' value='' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SAC_THUE_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' maxlength='2' value='' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_MA_CHUONG_" + sID + "' maxlength='3' style='width: 98%; border-color: White;' class='inputflat' value='' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Ma_TMuc_" + sID + "' maxlength='4' style='width: 98%; border-color: White;' class='inputflat' value='' onblur='onBlurMaTieuMuc(" + sID + ")' onkeypress='' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Noi_Dung_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_KY_THUE_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='' maxlength='10' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SoTien_" + sID + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onkeyup='ValidInteger(this); jsFormatNumber(this);' jsCalcCurrency(" + sID + ");' value='' /></td>";
            sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SoTienNT_" + sID + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat' onkeyup='jsFormatNumber2(this);' value='' /></td>";

            sAppendRows = sAppendRows + "<td style = 'display: none;'><input type='text' id='DTL_TT_BTOAN_" + sID + "' style='width: 98%; display: none;' class='inputflat' value='' /></td>";
            sAppendRows = sAppendRows + '</tr>';

            $('#grdChiTiet tr:last').after(sAppendRows);
         

        }

        function jsRemoveRowDTL_Grid2(sSoTK) {
            $('#grdChiTiet tr').each(function () {
                var ctrl = $(this).find("input[id^='DTL_SO_TK_']");
                if (typeof ctrl !== 'undefined' && ctrl !== null) {
                    if (ctrl.prop("value") == sSoTK) {
                        $(this).remove();
                    }
                }
            });
            jsFormatNumberDTL_Grid();
        }

        function jsGenDTLrowID() {
            //muc dich: sinh ra so ID cao nhat trong cac <tr> detail => tranh trung lap <tr> ID detail 
            var sRet = null;
            var sID = '';
            var arrID = [];

            $('#grdChiTiet tr').each(function () {
                sID = '';
                var ctrInput = $(this).find("input[id^='DTL_SO_TK_']");
                if (ctrInput) {
                    sID = ctrInput.prop("id");
                }
                if (sID && sID.trim() != '') {
                    var arrTmp = sID.trim().split('_');
                    sID = arrTmp[arrTmp.length - 1];
                    if (typeof sID !== 'undefined' && sID !== null && !isNaN(sID)) {
                        arrID.push(parseInt(sID));
                    }
                }
            });
            if (arrID && arrID.length > 0) {
                sRet = Math.max.apply(Math, arrID);
            } else {
                sRet = 0;
            }

            return sRet;
        }

        function jsCalcTotalMoney() {
            var dTotalMoney = 0;
            var dDetailMoney = 0;

            var tongTienNT = 0;
            var ctTongTienNT = 0;
            //get detail
            $('#grdChiTiet tr').each(function () {
                var ctrCheck = $(this).find("input[id^='DTL_chkSelect_']");
                var ctrInput = null;
                var ctrInputNT = null;
                if (ctrCheck) {
                    if (ctrCheck.is(":checked")) {
                        ctrInput = $(this).find("input[id^='DTL_SoTien_']");
                        if (ctrInput) {
                            dDetailMoney = 0;
                            dDetailMoney = accounting.unformat(ctrInput.prop("value"));
                            if (!isNaN(dDetailMoney)) {
                                dTotalMoney = dTotalMoney + dDetailMoney;
                            }
                        }

                        ctrInputNT = $(this).find("input[id^='DTL_SoTienNT_']");

                        if (ctrInputNT) {
                            ctTongTienNT = 0;
                            ctTongTienNT = accounting.unformat(ctrInputNT.prop("value"), ".");
                            if (!isNaN(ctTongTienNT)) {
                                tongTienNT += ctTongTienNT;
                            }
                        }
                    } else {
                        ctrInput = null;
                        ctrInputNT = null;
                    }
                }
            });

            dblPhi = 0;
            dblVAT = 0;

            if (dTotalMoney > 0 && FEE_RATE != undefined && FEE_MAX != undefined && FEE_MIN != undefined) {
                dblPhi = parseFloat(dTotalMoney) * parseFloat(FEE_RATE);
                if (dblPhi > FEE_MAX) {
                    dblPhi = FEE_MAX;
                }
                if (dblPhi < FEE_MIN) {
                    dblPhi = FEE_MIN;
                }

                dblVAT = parseFloat(dblPhi) * parseFloat(VAT_RATE);
                if (dblVAT > VAT_MAX) {
                    dblVAT = VAT_MAX;
                }
                if (dblVAT < VAT_MIN) {
                    dblVAT = VAT_MIN;
                }
            }


            dblTongTrichNo = parseFloat(dblPhi) + parseFloat(dblVAT) + parseFloat(dTotalMoney);
            $('#txtVAT').val(accounting.formatNumber(dblVAT));
            $('#txtCharge').val(accounting.formatNumber(dblPhi));
            $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo)).change();
            $('#txtTongTien').val(accounting.formatNumber(dTotalMoney));

            if ($("#ddlMaNT option:selected").val() != 'VND')
                $('#txtTongTienNT').val(accounting.formatNumber(tongTienNT, 2, ",", "."));
            else
                $('#txtTongTienNT').val(accounting.formatNumber(tongTienNT));
        }


        function ValidDate(dateString) {
            var regex_date = /^\d{1,2}\/\d{1,2}\/\d{4}$/; //pattern: dd/MM/yyyy
            if (!regex_date.test(dateString)) {
                return false;
            }
            var parts = dateString.split("/");
            var day = parseInt(parts[0], 10);
            var month = parseInt(parts[1], 10);
            var year = parseInt(parts[2], 10);

            if (year < 1000 || year > 3000 || month == 0 || month > 12) {
                return false;
            }
            var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

            //For leap years
            if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
                monthLength[1] = 29;
            }

            return day > 0 && day <= monthLength[month - 1];
        }


        function jsCheckMaNNT(param) {
            if ((param.value.trim()).length != 0 && (param.value.trim()).length != 10 && (param.value.trim()).length != 13) {
                param.value = "";
                param.focus();
                alert("Mã số thuế không hợp lệ!");
                return;
            }
            jsGet_TenNNT();
        }

        function jsGet_DataByMaHQPH() {
            $get('txtMaHQ').value = '';
            $get('txtTenMaHQ').value = '';
            $get('txtMaCQThu').value = '';
            $get('txtTenCQThu').value = '';
            $get('txtTenMaHQPH').value = '';
            $get('txtSHKB').value = '';
            $get('txtTenKB').value = '';

            PageMethods.Get_TenMaHQ($('#txtMaHQPH').val(), Get_DataByMaHQPH_Complete, Get_DataByMaHQPH_Error);
        }

        function Get_DataByMaHQPH_Complete(result, methodName) {
            jsCheckUserSession(result);
            if (result.length > 0) {
                var arr = result.split(';');
                $('#txtMaHQ').val($get('txtMaHQPH').value);
                $('#txtTenMaHQ').val(arr[1]);
                $('#txtMaCQThu').val(arr[0]);
                $('#txtTenCQThu').val(arr[1]);
                $('#txtTenMaHQPH').val(arr[1]);
                $('#txtSHKB').val(arr[2]); //auto fire jsGetTTNganHangNhan, jsGet_TenKB ...

            }
        }
        function Get_DataByMaHQPH_Error(error, userContext, methodName) {
        }


        function jsGet_TenNNT() {
            //$get('txtTenNNT').value = '';
            PageMethods.Get_TenNNT($get('txtMaNNT').value, Get_TenNNT_Complete, Get_TenNNT_Error);
        }

        function Get_TenNNT_Complete(result, methodName) {
            //jsCheckUserSession(result);
            if (result.length > 0) {
                $get('txtTenNNT').value = result;
            }
        }
        function Get_TenNNT_Error(error, userContext, methodName) {
            if (error !== null) {

            }
        }

        function jsShowDivStatus(bShow, sMessage) {
            if (bShow) {
                document.getElementById('divStatus').style.display = '';
                document.getElementById('divStatus').innerHTML = sMessage;
            } else {
                document.getElementById('divStatus').style.display = 'none';
                document.getElementById('divStatus').innerHTML = sMessage;
            }
        }


        function jsShowHideDivProgress(bShow) {
            if (bShow) {
                document.getElementById('divProgress').style.display = '';
            } else {
                document.getElementById('divProgress').innerHTML = 'Đang xử lý. Xin chờ một lát...';
                document.getElementById('divProgress').style.display = 'none';
            }
        }

        function jsHuy_CTU() {
            if (true) {
                var sSoCTu, sLyDoHuy;
                sSoCTu = $('#hdfSo_CTu').val();
                sLyDoHuy = $('#txtLyDoHuy').val();
                if (typeof sSoCTu !== 'undefined' && sSoCTu !== null && sSoCTu !== '') {
                    if (typeof sLyDoHuy == 'undefined' || sLyDoHuy == null || sLyDoHuy.trim() == '') {
                        alert('Vui lòng nhập lý do hủy.');
                        return;
                        //if (curCtuStatus == '04') {
                        //    alert('Vui lòng nhập lý do hủy.');
                        //    return;
                        //}
                    }
                    $.ajax({
                        type: 'POST',
                        url: 'frmLapDienNHKhac.aspx/Huy_CTU',
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify({
                            sSoCT: sSoCTu,
                            sLyDo: sLyDoHuy || null
                        }),
                        success: function (reponse) {
                            var sRet = reponse.d.split(';');
                            if (typeof sRet !== 'undefined' && sRet !== null && typeof sRet[1] !== 'undefined' && sRet[1] !== '') {
                                var sRetSplit = sRet[1].split('/');
                                if (typeof sRetSplit !== 'undefined' && sRetSplit !== null && sRetSplit.length > 0) {
                                    alert(sRet[0] + ' - Số CT: ' + sRetSplit[1] + ' - Số BT: ' + sRetSplit[0] + ' - SHKB: ' + sRetSplit[2]);
                                    jsEnableDisableControlButton('5');
                                    $('#hdfDescCTu').val('SHKB/SoCT/SoBT:' + +sRet[1]);
                                } else {
                                    alert('Hủy chứng từ không thành công. Vui lòng liên hệ quản lý hệ thống');
                                    $('#hdfDescCTu').val('SHKB/SoCT/SoBT://');
                                }
                            } else {
                                alert('Hủy chứng từ không thành công. Vui lòng liên hệ quản lý hệ thống');
                                $('#hdfDescCTu').val('SHKB/SoCT/SoBT://');
                              
                            }
                            $('#hdfIsEdit').val(true);
                            jsLoadCTList();
                        },
                        error: function (request, status, error) {
                            var err;
                            err = JSON.parse(request.responseText);
                            if (err) {
                                if (err.Message.indexOf(';') > -1) {
                                    jsCheckUserSession(err.Message);
                                }
                                alert(err.Message);
                                //jsEnableDisableControlButton('4');
                            }
                        }
                    });
                } else {
                    alert('Không xác định được chứng từ cần hủy');
                }
            }
        }

        function jsValidBeforeGetFormData(sAct) {
            if (sAct == "GHIDC")
                if ($('#txtLyDoHuy').val().length == 0) {
                    alert("Xin vui lòng nhập Lý do điều chỉnh!");
                    $('#txtLyDoHuy').focus();
                    return false;
                }

            return true;
        }

        function jsEnableDisableAddRowButton(sCase) {
            switch (sCase) {
                case "0":
                    $('#btnAddRowDTL').prop('disabled', true);
                    break;
                case "1":
                    $('#btnAddRowDTL').prop('disabled', false);
                    break;
            }
        }

        function jsEnableDisableCtrlBtnByTThaiCTu(sTThaiCTu, sTThaiCSP) {
            switch (sTThaiCTu) {
                case "01": //Mới nhận từ Citad
                    jsEnableDisableControlButton("1");
                    jsEnableDisableAddRowButton("1");
                    break;
                case "02": //Chờ kiểm soát
                    jsEnableDisableControlButton("2");
                    jsEnableDisableAddRowButton("0");
                    break;
                case "03": //Thanh cong
                    if (sTThaiCSP == "1") { //gửi song phuong thành công 
                        jsEnableDisableControlButton("3");
                        jsEnableDisableAddRowButton("0");
                    } else { //
                        jsEnableDisableControlButton("3");
                        jsEnableDisableAddRowButton("0");
                    }
                    break;
                case "04": //Chuyển trả
                    jsEnableDisableControlButton("4");
                    jsEnableDisableAddRowButton("1");
                    break;
                case "05": //Chờ duyệt hủy
                    jsEnableDisableControlButton("5");
                    jsEnableDisableAddRowButton("0");
                    break;
                case "06": //Đã hủy
                    jsEnableDisableControlButton("6");
                    jsEnableDisableAddRowButton("0");
                    break;
            }
        }

        function jsEnableDisableControlButton(sCase) {
            switch (sCase) {
                case "1": ////Mới nhận từ Citad - btnAddRowDTL
                    $('#cmdChuyenKS').prop('disabled', false);
                    $('#cmdHuyCT').prop('disabled', false);
                    break;
                case "2": ////Chờ kiểm soát
                    $('#cmdChuyenKS').prop('disabled', true);
                    $('#cmdHuyCT').prop('disabled', false);
                    break;
                case "3": //Thanh cong
                    $('#cmdChuyenKS').prop('disabled', true);
                    $('#cmdHuyCT').prop('disabled', true);
                    break;
                case "4": //Chuyển trả
                    $('#cmdChuyenKS').prop('disabled', false);
                    $('#cmdHuyCT').prop('disabled', false);
                    break;
                case "5": //Chờ duyệt hủy
                    $('#cmdChuyenKS').prop('disabled', true);
                    $('#cmdHuyCT').prop('disabled', true);
                    break;
                case "6": //Đã hủy
                    $('#cmdChuyenKS').prop('disabled', true);
                    $('#cmdHuyCT').prop('disabled', true);
                    break;
              }
        }

        function jsGetFloatNumberFromString(str, exceptChar) {
            var fRet = 0;
            if (typeof str !== 'undefined' && str !== null && str.trim() !== '') {

            }

            return fRet;
        }

        function jsGetCTU(sSoCT) {
            jsShowDivStatus(false, "");
            jsShowHideDivProgress(true);

            //clear data
            $('#hdfSo_CTu').val('');
            $('#hdfIsEdit').val(false);
            $('#hdfDescCTu').val('');

            $('#hdfHQResult').val('');
            $('#hdfHDRSelected').val('');
            $('#hdfNgayKB').val('');

            jsClearHDR_Panel();
            jsClearDTL_Grid()
            jsBindDTL_Grid(null, null);

            //NOT: null, undefined, 0, NaN, false, or ""
            if (sSoCT) {
                $.ajax({
                    type: "POST",
                    url: "frmLapDienNHKhac.aspx/GetCTU",
                    cache: false,
                    data: JSON.stringify({
                        "sSoCT": sSoCT
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: GetCtu_OnSuccess,
                    error: function (request, status, error) {
                        jsShowDivStatus(true, request.responseText);
                        var err;
                        err = JSON.parse(request.responseText);
                        if (err) {
                            if (err.Message.indexOf(';') > -1) {
                                jsCheckUserSession(err.Message);
                            }
                            jsShowDivStatus(true, err.Message);
                        }
                        jsShowHideDivProgress(false);
                    }
                });
            }
        }

        function GetCtu_OnSuccess(response) {
            if (response != null) {
                    if (typeof response !== 'undefined' && response !== null) {
                    if (response.d) {
                        if (!jsCheckUserSession(response.d)) {
                            return;
                        }
                        var objCTu = JSON.parse(response.d);
                        if (objCTu) {
                            //Bind HDR panel
                            var HDR = objCTu.HDR;
                            if (typeof HDR !== 'undefined' && HDR !== null) {
                                //Bind hidden field
                                $('#hdfSo_CTu').val(HDR.SO_CT);
                                $('#hdfSoCTNH').val(HDR.SO_CT_NH);
                                $('#hdfNgayKB').val(HDR.NGAY_KB); //hdfTrang_Thai
                                $('#hdfTrang_Thai').val(HDR.TRANG_THAI);
                                curCtuStatus = HDR.TRANG_THAI;
                                $('#hdfIsEdit').val(true);
                                var sDescCTu = "SHKB/SoCT/SoBT" + ":" + HDR.SHKB + "/" + HDR.SO_CT + "/" + HDR.SO_BT;
                                $('#hdfDescCTu').val(sDescCTu);
                                jsEnableDisableCtrlBtnByTThaiCTu(curCtuStatus, HDR.TT_SP);
                                //console.log("TT: " + curCtuStatus);
                            }
                            jsBindHDR_Panel(HDR);
                            //Bind dtl grid
                            var listDTL = objCTu.ListDTL;
                            jsBindDTL_Grid(HDR, listDTL);
                        }
                    }
                }
            }

            jsShowHideDivProgress(false);

        }

        function jsIn_CT(isBS) {
            var width = screen.availWidth - 100;
            var height = screen.availHeight - 10;
            var left = 0;
            var top = 0;
            var params = 'width=' + width + ', height=' + height;
            params += ', top=' + top + ', left=' + left;
            params += ', directories=no';
            params += ', location=no';
            params += ', menubar=yes';
            params += ', resizable=no';
            params += ', scrollbars=yes';
            params += ', status=no';
            params += ', toolbar=no';

            var strSHKB, strSoCT, strSo_BT, MaNV, ngayLV;
            var descCTu = $('#hdfDescCTu').val();
            if (typeof descCTu !== 'undefined' && descCTu !== null) {
                if (descCTu.indexOf(':') > -1) {
                    strSHKB = $('#hdfDescCTu').val().split(':')[1].split('/')[0];
                    strSoCT = $('#hdfDescCTu').val().split(':')[1].split('/')[1];
                    strSo_BT = $('#hdfDescCTu').val().split(':')[1].split('/')[2];
                    MaNV = curMa_NV;
                    ngayLV = $('#txtNGAY_HT').val();
                }
            }
            window.open("../../pages/BaoCao/frmInGNT_HQ.aspx?so_ct=" + strSoCT, "", params);
        }

        function jsLoadByCQT_Default() {
            $('#txtTenCQThu').val('');
            var strMaCQThu = $get('txtMaCQThu').value;
            var strMaHQ = $get('txtMaHQ').value;
            //console.log("MaHQ: " + strMaHQ);
            var strMaHQPH = $get('txtMaHQPH').value;
            //strMaHQ = ""; //ko lấy kèm Mã HQ
            if (strMaCQThu.length > 0) {
                PageMethods.Get_DataByCQT(strMaCQThu, strMaHQ, strMaHQPH, LoadByCQT_Default_Complete, LoadByCQT_Error);
            }
        }

        //jsLoadSHKBByCQT
        function jsLoadSHKBByCQT() {
            $('#txtTenCQThu').val('');
            var strMaCQThu = $get('txtMaCQThu').value;
            var strMaHQ = $get('txtMaHQ').value;
            var strMaHQPH = $get('txtMaHQPH').value;
            if (strMaCQThu.length > 0 || strMaHQ.length > 0 || strMaHQPH.length > 0) {
                PageMethods.Get_DataByCQT(strMaCQThu, strMaHQ, strMaHQPH, LoadSHKBByCQT_Complete, LoadSHKBByCQT_Error);
            }
        }
        function LoadSHKBByCQT_Complete(result, userContext, methodName) {
            jsCheckUserSession(result);
            if (result.lenght != 0) {
                var arr = result.split("|");
                if (arr[2]) {
                    $get('txtTenCQThu').value = arr[2].toString();
                    $('#txtSHKB').val(arr[1]);
                    $('#txtMaCQThu').val(arr[3]);
                    jsGet_TenKB();
                    jsGetTTNganHangNhan();
                } else {
                    document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin cơ quan thu với mã nhập vào';
                }
            }
        }
        function LoadSHKBByCQT_Error(error, userContext, methochedName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = 'Không lấy được dữ liệu theo cơ quan thu';
            }
        }
        function jsLoadByCQT() {
            $('#txtTenCQThu').val('');
            var strMaCQThu = $get('txtMaCQThu').value;
            var strMaHQ = $get('txtMaHQ').value;
            var strMaHQPH = $get('txtMaHQPH').value;
            if (strMaCQThu.length > 0 || strMaHQ.length > 0 || strMaHQPH.length > 0) {
                PageMethods.Get_DataByCQT(strMaCQThu, strMaHQ, strMaHQPH, LoadByCQT_Complete, LoadByCQT_Error);
            }
        }

        function LoadByCQT_Complete(result, userContext, methodName) {
            jsCheckUserSession(result);
            if (result.lenght != 0) {
                var arr = result.split("|");
                if (arr[2]) {
                    $get('txtTenCQThu').value = arr[2].toString();
                    $('#txtMaHQ').val(arr[0]);
                    $('#txtMaHQPH').val(arr[0]);
                    $('#txtTenMaHQ').val(arr[2]);
                    $('#txtTenMaHQPH').val(arr[2]);

                    $('#txtSHKB').val(arr[1]);
                    $('#txtMaCQThu').val(arr[3]);
                    jsGet_TenKB();
                    jsValidNHSHB_SP();
                    jsGetMaDBHC();
                    //jsGetTTNganHangNhan();
                } else {
                    document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin cơ quan thu với mã nhập vào';
                }
            }
        }
        function LoadByCQT_Error(error, userContext, methochedName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = 'Không lấy được dữ liệu theo cơ quan thu';
            }
        }
        function LoadByCQT_Default_Complete(result, userContext, methodName) {
            jsCheckUserSession(result);
            if (result.lenght != 0) {
                var arr = result.split("|");

                if (arr[2]) {
                    $get('txtTenCQThu').value = arr[2].toString();
                    $('#txtMaHQ').val(arr[0]);
                    $('#txtMaHQPH').val(arr[0]);
                    $('#txtTenMaHQ').val(arr[2]);
                    $('#txtTenMaHQPH').val(arr[2]);

                } else {
                    document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin cơ quan thu với mã nhập vào';
                }
            }
        }


        //Load Ten CQTHU
        function jsLoadTenCQT(txtMaMuc, txtNoiDung) {
            var userContext = {};
            userContext.ControlID = txtNoiDung;

            var strMaNDKT = document.getElementById(txtMaMuc).value;
            if (strMaNDKT.length > 0) {
                PageMethods.Load_TenCQT(strMaNDKT, Load_TenCQT_Complete, Load_TenCQT_Error, userContext);
            }
        }

        function Load_TenCQT_Complete(result, userContext, methodName) {
            checkSS(result);
            if (result.length > 0) {
                document.getElementById(userContext.ControlID).value = result;
                document.getElementById('divStatus').innerHTML = "Tìm thấy Tên cơ quan thu phù hợp";
            } else {
                document.getElementById('divStatus').innerHTML = "Không tìm thấy Tên cơ quan thu phù hợp.Hãy tìm lại";
            }
        }
        function Load_TenCQT_Error(error, userContext, methodName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy nội dung kinh tế" + error.get_message();
            }
        }
        function jsGetTenQuanHuyen_NNThue(ma_xa) {
            //var ma_xa = $('#txtHuyen').val();
            //var ma_xa1 = $get('txtHuyen_NNT').value
            if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
                PageMethods.GetTenQuanHuyen(ma_xa, GetTenQuanHuyenNNThue_Complete, GetTenQuanHuyenNNThue_Error);
                jsGetTenTinh_NNThue(ma_xa);
            } else {
                document.getElementById('txtHuyen_NNT').value = "";
                document.getElementById('txtTenHuyen_NNT').value = "";
                document.getElementById('txtTinh_NNT').value = "";
                document.getElementById('txtTenTinh_NNT').value = "";
            }
        }

        function GetTenQuanHuyenNNThue_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Quan = result;
                document.getElementById('txtTenHuyen_NNT').value = ten_Quan;
            } else {
                alert('Mã Quận người nộp thuế không chính xác. Vui lòng nhập lại.');
                document.getElementById('txtHuyen_NNT').focus();
                document.getElementById('txtHuyen_NNT').value = "";
                document.getElementById('txtTenHuyen_NNT').value = "";
                document.getElementById('txtTinh_NNT').value = "";
                document.getElementById('txtTenTinh_NNT').value = "";
            }
        }

        function GetTenQuanHuyenNNThue_Error(error, userContext, methodName) {
            alert('Mã Quận người nộp thuế không chính xác. Vui lòng nhập lại.');
            document.getElementById('txtHuyen_NNT').focus();
            document.getElementById('txtHuyen_NNT').value = "";
            document.getElementById('txtTenHuyen_NNT').value = "";
            document.getElementById('txtTinh_NNT').value = "";
            document.getElementById('txtTenTinh_NNT').value = "";
        }

        function jsGetTenTinh_NNThue(ma_xa) {
            //var ma_xa = $('#txtHuyen_NNT').val();
            $('#txtTinh_NNT').val('');
            $('#txtTenTinh_NNT').val('');
            if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
                PageMethods.GetTenTinh(ma_xa, GetTenTinhNNThue_Complete, GetTenTinhNNThue_Error);
            }
        }

        function GetTenTinhNNThue_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Tinh = result.split(';');
                document.getElementById('txtTinh_NNT').value = ten_Tinh[0];
                document.getElementById('txtTenTinh_NNT').value = ten_Tinh[1];
            }

        }

        function GetTenTinhNNThue_Error(error, userContext, methodName) {

        }

        function jsGetTenQuanHuyen_NNTien(ma_xa) {
            if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
                PageMethods.GetTenQuanHuyen(ma_xa, GetTenQuanHuyenNNTien_Complete, GetTenQuanHuyenNNTien_Error);
                jsGetTenTinh_NNTien(ma_xa);
            } else {
                document.getElementById('txtQuan_HuyenNNTien').value = "";
                document.getElementById('txtTenQuan_HuyenNNTien').value = "";
                document.getElementById('txtTinh_NNTien').value = "";
                document.getElementById('txtTenTinh_NNTien').value = "";
            }
        }

        function GetTenQuanHuyenNNTien_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Quan = result;
                document.getElementById('txtTenQuan_HuyenNNTien').value = ten_Quan;
            } else {
                alert('Mã Quận người nộp tiền không chính xác. Vui lòng nhập lại.');
                document.getElementById('txtQuan_HuyenNNTien').focus();
                document.getElementById('txtQuan_HuyenNNTien').value = "";
                document.getElementById('txtTenQuan_HuyenNNTien').value = "";
                document.getElementById('txtTinh_NNTien').value = "";
                document.getElementById('txtTenTinh_NNTien').value = "";
            }
        }

        function GetTenQuanHuyenNNTien_Error(error, userContext, methodName) {
            alert('Mã Quận người nộp tiền không chính xác. Vui lòng nhập lại.');
            document.getElementById('txtQuan_HuyenNNTien').focus();
            document.getElementById('txtQuan_HuyenNNTien').value = "";
            document.getElementById('txtTenQuan_HuyenNNTien').value = "";
            document.getElementById('txtTinh_NNTien').value = "";
            document.getElementById('txtTenTinh_NNTien').value = "";
        }

        function jsGetTenTinh_NNTien(ma_xa) {
            //var ma_xa = $('#txtHuyen_NNT').val();
            $('#txtTinh_NNTien').val('');
            $('#txtTenTinh_NNTien').val('');
            if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
                PageMethods.GetTenTinh(ma_xa, GetTenTinhNNTien_Complete, GetTenTinhNNTien_Error);
            }
        }

        function GetTenTinhNNTien_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_Tinh = result.split(';');
                document.getElementById('txtTinh_NNTien').value = ten_Tinh[0];
                document.getElementById('txtTenTinh_NNTien').value = ten_Tinh[1];
            }

        }
        function GetTenTinhNNTien_Error(error, userContext, methodName) {

        }

        function jsGet_TenKB() {
            $('#txtTenKB').val('');
            if (arrSHKB.length > 0) {
                for (var i = 0; i < arrSHKB.length; i++) {
                    var arr = arrSHKB[i].split(';');
                    if (arr[0] == $('#txtSHKB').val()) {
                        $('#txtTenKB').val(arr[1])
                        break;
                    }
                }
            }

        }

        function jsGet_TenDBHC() {
            $('#txtTenDBHC').val('');
            if (typeof arrDBHC !== 'undefined' && arrDBHC !== null) {
                if ((arrDBHC instanceof Array) && (arrDBHC.length > 0)) {
                    for (var i = 0; i < arrDBHC.length; i++) {
                        var arr = arrDBHC[i].split(';');
                        if (arr[0] == $('#txtMaDBHC').val()) {
                            $('#txtTenDBHC').val(arr[1]);
                            break;
                        }
                    }
                }
            }
        }

        function jsGet_TenCQThu() {
            $('#txtTenCQThu').val('');
            if (arrCQThu.length > 0) {
                for (var i = 0; i < arrCQThu.length; i++) {
                    var arr = arrCQThu[i].split(';');
                    if (arr[1] == $('#txtMaCQThu').val()) {
                        $('#txtTenCQThu').val(arr[2]);
                        break;
                    }
                }
            }
        }

        function jsFillNguyenTe() {
            var cb = document.getElementById('ddlMaNT');
            cb.options.length = 0;

            if (arrMaNT.length > 0) {
                for (var i = 0; i < arrMaNT.length; i++) {
                    var arr = arrMaNT[i].split(';');
                    var value = arr[0];
                    var label = arr[1];
                    cb.options[cb.options.length] = new Option(label, value);
                }
            }

            $("#ddlMaNT option[value='VND']").prop('selected', true);
        }

        function jsShowTyGia() {
            $('#txtTyGia').val('');
            if (arrTyGia.length > 0) {
                for (var i = 0; i < arrTyGia.length; i++) {
                    var arr = arrTyGia[i].split(';');
                    var value = arr[0];
                    var label = arr[1];
                    if (value == $('#ddlMaNT').val()) {
                        $('#txtTyGia').val(label);
                        break;
                    }
                }
            }
        }

        function jsGetTen_TKNo() {
        }

        function jsGet_TenLHSX() {
            $('#txtDescLHXNK').val('');
            if (arrLHSX.length > 0) {
                for (var i = 0; i < arrLHSX.length; i++) {
                    var arr = arrLHSX[i].split(';');
                    if (arr[0] == $get('txtLHXNK').value.trim()) {
                        $get('txtDescLHXNK').value = arr[1];
                        break;
                    }
                }
            }
        }

        function checkSS(param) {

            var arrKQ = param.split(';');
            if (arrKQ[0] == "ssF") {
                window.open("../../pages/Warning.html", "_self");
                return;
            }
        }

        function jsGet_TenNH_B() {
            //$('#txtTenMA_NH_B').val('');
            if (arrTT_NH_B.length > 0) {
                for (var i = 0; i < arrTT_NH_B.length; i++) {
                    var arr = arrTT_NH_B[i].split(';');
                    if (arr[0] == $get('txtMA_NH_B').value.trim() && arr[4] == $get('txtSHKB').value.trim()) {
                        $('#txtTenMA_NH_B').val(arr[1]);
                        $('#txtMA_NH_TT').val(arr[2]);
                        $('#txtTEN_NH_TT').val(arr[3]);
                        break;
                    }
                }
            }
        }



        function jsFillPTTT() {
        }

        function ShowLov(strType) {
            if (strType == "NNT") return FindNNT_NEW('txtMaNNT', 'txtTenNNT');
            if (strType == "SHKB") return FindDanhMuc('KhoBac', 'txtSHKB', 'txtTenKB', 'txtSHKB');
            if (strType == "DBHC") return FindDanhMuc('DBHCTINH_KB', 'txtMaDBHC', 'txtTenDBHC', 'txtMaDBHC');
            if (strType == "CQT") return FindDanhMuc('CQThu_HaiQuan', 'txtMaCQThu', 'txtTenCQThu', 'txtMTkNo');
            if (strType == "LHXNK") return FindDanhMuc('LHXNK', 'txtLHXNK', 'txtDescLHXNK', 'txtSHKB');
            if (strType == "LHXNK") return FindDanhMuc('LHXNK', 'txtLHXNK', 'txtDescLHXNK', 'txtSHKB');
            if (strType == "MHQ") return FindDanhMuc('Ma_HaiQuan', 'txtMaHQ', 'txtTenMaHQ', 'txtTenMaHQ');
            if (strType == "MHQPH") return FindDanhMuc('Ma_HaiQuan', 'txtMaHQPH', 'txtTenMaHQPH', 'txtTenMaHQPH');
            if (strType == "DMNH_TT") return FindDanhMuc('DMNH_TT', 'txtMA_NH_TT', 'txtTEN_NH_TT', 'txtTEN_NH_TT');
            if (strType == "DMNH_GT") return FindDanhMuc('DMNH_GT', 'txtMA_NH_B', 'txtTenMA_NH_B', 'txtTenMA_NH_B');
            if (strType == "DBHC_NNT") return FindDanhMuc('DBHCHUYEN', 'txtHuyen_NNT', 'txtTenHuyen_NNT', 'txtHuyen_NNT');//DBHC_NNT
            if (strType == "DBHC_NNTHUE") return FindDanhMuc('DBHCHUYEN', 'txtQuan_HuyenNNTien', 'txtTenQuan_HuyenNNTien', 'txtQuan_HuyenNNTien');//DBHC_NNTHUE
        }

        function FindNNT_NEW(txtID, txtTitle) {
            var returnValue = window.showModalDialog("../Find_DM/Find_NNT_NEW.aspx?Src=TK&initParam=" + $get('txtMaNNT').value + "&SHKB=" + $get('txtSHKB').value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;
            }
        }

        function FindDanhMuc(strPage, txtID, txtTitle, txtFocus) {
            var strSHKB;
            var returnValue;
            if (document.getElementById('txtSHKB').value.length > 0) {
                strSHKB = document.getElementById('txtSHKB').value;
            }
            else {
                strSHKB = defSHKB.split(';')[0];
            }

            returnValue = window.showModalDialog("../Find_DM/Find_DanhMuc.aspx?page=" + strPage + "&SHKB=" + strSHKB + "&initParam=" + $get(txtID).value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                if (txtTitle != null) {
                    document.getElementById(txtTitle).value = returnValue.Title;
                }
                if (txtFocus != null) {
                    //if ($get('txtFocus').getAttribute('disabled') != 'disabled') {
                    try {
                        document.getElementById(txtFocus).focus();
                    }
                    catch (e) { };
                }
            }
        }

        function jsSHKB_lostFocus(strSHKB) {
            jsLoadDM('KHCT', strSHKB);
            jsLoadDM('SHKB', strSHKB);
            //jsLoadDM('DBHC',strSHKB);
            jsLoadDM('CQTHU', strSHKB);
            jsLoadDM('CQTHU_DEF', strSHKB);
            jsLoadDM('DBHC_DEF', strSHKB);
            jsLoadDM('TAIKHOANNO', strSHKB);
            jsLoadDM('TAIKHOANCO', strSHKB);
            jsLoadDM('CQTHU_SHKB', strSHKB);
            jsLoadDM('TINH_SHKB', strSHKB);
        }

        function jsLoadDM(strLoaiDM, strSHKB) {
            if (strSHKB.length == 0) {
                strSHKB = document.getElementById('txtSHKB').value;
            }
            if (strSHKB.length > 0) {
                PageMethods.GetData_SHKB(strLoaiDM, strSHKB, LoadDM_Complete, LoadDM_Error);
            }
            else {
            }
        }

        function LoadDM_Complete(result, userContext, methodName) {
            jsCheckUserSession(result);
            var mangKQ = new Array;
            mangKQ = result.toString().split('|');

            if (mangKQ[0] == 'TAIKHOANCO') {
                arrTKCoNSNN = new Array(mangKQ.length - 2);
                for (i = 1; i < mangKQ.length - 1; i++) {
                    var strTemp = mangKQ[i].toString();
                    arrTKCoNSNN[i - 1] = strTemp;
                }

            }
            else if (mangKQ[0] == 'CQTHU') {
                arrCQThu = new Array(mangKQ.length - 2);
                for (i = 1; i < mangKQ.length - 1; i++) {
                    var strTemp = mangKQ[i].toString();
                    arrCQThu[i - 1] = strTemp;
                }
            } else if (mangKQ[0] == 'CQTHU_SHKB') {
                arrCQThu_SHKB = new Array(mangKQ.length - 2);
                for (i = 1; i < mangKQ.length - 1; i++) {
                    var strTemp = mangKQ[i].toString();
                    arrCQThu_SHKB[i - 1] = strTemp;
                }
            }
            else if (mangKQ[0] == 'DBHC_DEF') {
                if (mangKQ[1].length != 0) {
                    document.getElementById('txtMaDBHC').value = mangKQ[1].split(';')[0];
                    document.getElementById('txtTenDBHC').value = mangKQ[1].split(';')[1];
                }
                else {
                    document.getElementById('txtMaDBHC').value = '';
                    document.getElementById('txtTenDBHC').value = '';
                }

            }
        }

        function LoadDM_Error(error, userContext, methochedName) {
            if (error !== null) {
                document.getElementById('divStatus').innerHTML = 'Không lấy được dữ liệu liên quan đến kho bạc này';
            }
        }

        function jsGetTTNganHangNhan() {
            try {
                var strSHKB = $('#txtSHKB').val();//$get('txtSHKB').value;
                var strDBHC = '';//$get('txtMaDBHC').value;
                var strCQThu = '';//$get('txtMaCQThu').value;            	
                PageMethods.GetTT_NH(strSHKB, GetTT_NganHangNhan_Complete, GetTT_NganHangNhan_Error);
            } catch (ex) {
                alert(ex.message);
            }
        }

        function GetTT_NganHangNhan_Complete(result, methodName) {
            try {
                jsCheckUserSession(result);
                if (result.length > 0) {
                    var arr2 = result.split(';');
                    $('#txtMA_NH_B').val(arr2[0]);
                    $('#txtTenMA_NH_B').val(arr2[1]);
                    $('#txtMA_NH_TT').val(arr2[2]);
                    $('#txtTEN_NH_TT').val(arr2[3]);
                    jsValidNHSHB_SP();
                    var sMaTinh = $get('txtMA_NH_B').value.substring(0, 2);
                    jsGetTTTinhThanhKB(sMaTinh);
                }
                else {
                    $('#txtMA_NH_B').val('');
                    $('#txtTenMA_NH_B').val('');
                    $('#txtMA_NH_TT').val('');
                    $('#txtTEN_NH_TT').val('');
                    document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin ngân hàng nhận.Hãy kiểm tra lại';
                }
            } catch (ex) {
                alert(ex.message);
            }
        }

        function GetTT_NganHangNhan_Error(error, userContext, methodName) {
            try {
                $('#txtMA_NH_B').val('');
                $('#txtTenMA_NH_B').val('');
                $('#txtMA_NH_TT').val('');
                $('#txtTEN_NH_TT').val('');

                if (error !== null) {
                    document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin ngân hàng nhận" + error.get_message();
                }
            } catch (ex) {
                alert(ex.message);
            }
        }

        function jsGetTTTinhThanhKB(sMaTinh) {
            try {
                if (sMaTinh != "") {
                    PageMethods.GetTTTinhThanhKB(sMaTinh, GetTTTinhThanhKB_Complete, GetTTTinhThanhKB_Error);
                }

            } catch (ex) {
                alert(ex.message);
            }
        }

        function GetTTTinhThanhKB_Complete(result, methodName) {
            try {
                jsCheckUserSession(result);
                if (result.length > 0) {
                    var arr2 = result.split(';');
                    $('#txtMaDBHC').val(arr2[0]);
                    $('#txtTenDBHC').val(arr2[1]);
                }
                else {
                    $('#txtMaDBHC').val('');
                    $('#txtTenDBHC').val('');
                    document.getElementById('divStatus').innerHTML = 'Không tìm thấy thông tin tỉnh thành kho bạc.Hãy kiểm tra lại';
                }
            } catch (ex) {
                alert(ex.message);
            }
        }

        function GetTTTinhThanhKB_Error(error, userContext, methodName) {
            try {
                $('#txtMaDBHC').val('');
                $('#txtTenDBHC').val('');

                if (error !== null) {
                    document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy thông tin tỉnh thành kho bạc : " + error.get_message();
                }
            } catch (ex) {
                alert(ex.message);
            }
        }

        function jsGetTK_ddlDSTKKH() {
            $get('txtTK_KH_NH').value = document.getElementById('ddlDSTK_KH').value;
            jsGetTK_KH_NH();
        }

        function jsGetTK_KH_NH() {
            // mask($get('txtTK_KH_NH').value, $get('txtTK_KH_NH'), '3,6,9,17', '-');
            if ($get('txtTK_KH_NH').value.length > 0) {
                if (checkGLBeforGet($get('txtTK_KH_NH').value) == false) {
                    document.getElementById('divStatus').innerHTML = '';
                    var strAccType = '';
                    var intPP_TT = document.getElementById("ddlPT_TT").selectedIndex;
                    //if (intPP_TT == 0)
                    if (intPP_TT == 3) {
                        strAccType = 'TG';
                    } else {
                        strAccType = 'CK';
                    }

                    jsShowHideDivProgress(true);
                    PageMethods.GetTK_KH_NH($get('txtTK_KH_NH').value.trim(), GetTK_NH_Complete, GetTK_NH_Error);
                }
                else {
                    document.getElementById('divStatus').innerHTML = 'Định dạng tài khoản khách hàng không hợp lệ.Hãy nhập lại';
                }
            }
        }

        function checkGLBeforGet(tkGL) {
            for (var i = 0; i < arrGL.length; i++) {
                var arr = arrGL[i].split(';');
                if (arr[0] == tkGL) {
                    isGL = true;
                    glName = arr[1];
                    return true;
                }
            }
            return false;
        }

        function jsShowHideRowProgress(blnShow) {
            if (blnShow) {
                document.getElementById("divProgress").style.display = '';
            } else {
                document.getElementById("divProgress").style.display = 'none';
            }
        }
        function loadXMLString(txt) {
            if (window.DOMParser) {
                parser = new DOMParser();
                xmlDoc = parser.parseFromString(txt, "text/xml");
            }
            else // code for IE
            {
                xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = false;
                xmlDoc.loadXML(txt);
            }
            return xmlDoc;
        }
        function GetTK_NH_Complete(result, methodName) {
            checkSS(result);
            jsShowHideRowProgress(false);

            if (result.length > 0) {
                var xmlDoc = loadXMLString(result);
                var rootCTU_HDR = xmlDoc.getElementsByTagName('PARAMETER')[0];
                if (rootCTU_HDR != null) {

                    if (xmlDoc.getElementsByTagName("RETCODE")[0].childNodes[0].nodeValue == "00000") {
                        if (xmlDoc.getElementsByTagName("BANKACCOUNT")[0].childNodes != null) $get('txtTK_KH_NH').value = xmlDoc.getElementsByTagName("BANKACCOUNT")[0].childNodes[0].nodeValue;
                        if (xmlDoc.getElementsByTagName("ACY_AVL_BAL")[0].childNodes != null) $get('txtSoDu_KH_NH').value = xmlDoc.getElementsByTagName("ACY_AVL_BAL")[0].childNodes[0].nodeValue;
                        $('#txtSoDu_KH_NH').val(accounting.formatNumber($get('txtSoDu_KH_NH').value));
                        //jsFormatNumber('txtSoDu_KH_NH');
                        if (rootCTU_HDR.getElementsByTagName("CRR_CY_CODE")[0].childNodes.length != 0) $get('hdLimitAmout').value = rootCTU_HDR.getElementsByTagName("CRR_CY_CODE")[0].childNodes[0].nodeValue;
                        else $get('hdLimitAmout').value = "0";
                    } else {
                        $get('txtTK_KH_NH').value = '';
                        $get('txtTenTK_KH_NH').value = '';
                        $get('txtSoDu_KH_NH').value = '';
                        document.getElementById('divStatus').innerHTML = rootCTU_HDR.getElementsByTagName("ERRCODE")[0].firstChild.nodeValue;
                    }
                }
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Có lỗi trong quá trình truy vấn số dư tài khoản.';
                $get('txtTenTK_KH_NH').value = '';
                $get('txtSoDu_KH_NH').value = '';
            }

        }

        function GetTK_NH_Error(error, userContext, methodName) {
            if (error !== null) {
                $get('txtTenTK_KH_NH').value = '';
                $get('txtSoDu_KH_NH').value = '';
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản ngân hàng" + error.get_message();
            }
        }

        function jsGetTenTK_KH_NH() {
            if ($get('txtTK_KH_NH').value.length > 0) {
                document.getElementById('divStatus').innerHTML = '';
                var strAccType = '';
                var intPP_TT = document.getElementById("ddlPT_TT").selectedIndex;
                var ma_NT = document.getElementById("txtTenNT").value;
                if (intPP_TT == 3) {
                    strAccType = 'TG';
                } else {
                    strAccType = 'CK';
                }
                jsShowHideRowProgress(true);
                PageMethods.GetTEN_KH_NH($get('txtTK_KH_NH').value.trim(), GetTEN_KH_NH_Complete, GetTEN_KH_NH_Error);
            }
            else {
                document.getElementById('divStatus').innerHTML = 'Tài khoản khách hàng không được để trống';
            }
        }

        function GetTEN_KH_NH_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            if (result.length > 0) {
                $get('txtTenTK_KH_NH').value = result;
            } else {
                document.getElementById('divStatus').innerHTML = 'Không lấy được tên tài khoản khách hàng';
                $get('txtTenTK_KH_NH').value = '';
            }
        }
        function GetTEN_KH_NH_Error(error, userContext, methodName) {
            if (error !== null) {
                $get('txtTenTK_KH_NH').value = '';
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản ngân hàng" + error.get_message();
            }
        }

        function changeNT() {
            maTienTe = $("#ddlMaNT option:selected").val();
            $("#txtTenNT").val(maTienTe);

            var arr = arrTyGia;
            var toFix = 0;
            var tyGia = 1;
            for (var i = 0; i < arrTyGia.length; i++) {
                var arr = arrTyGia[i].split(';');
                var value = arr[0];
                tyGia = arr[1];
                toFix = arr[2];
                if (value == maTienTe) {
                    $('#txtTyGia').val(accounting.formatNumber(tyGia));
                    break;
                }
            }

            if (maTienTe != 'VND') {
                $('#grdChiTiet > tbody  > tr').each(function () {
                    var ctrInput = $(this).find("input[id^='DTL_SoTien_']");
                    var ctrInput2 = $(this).find("input[id^='DTL_SoTienNT_']");
                    if (ctrInput) {
                        var tienVnd = accounting.unformat(ctrInput.prop("value"));
                        if (tienVnd > 0) {
                            var tienNT = (tienVnd / tyGia).toFixed(toFix);
                            ctrInput2.val(accounting.formatNumber(tienNT, toFix, ",", "."));
                        } else {
                            ctrInput.val("0");
                            ctrInput2.val("0");
                        }
                    }
                });
            }
            else {
                $('#grdChiTiet > tbody  > tr').each(function () {
                    var ctrInput = $(this).find("input[id^='DTL_SoTien_']");
                    var ctrInput2 = $(this).find("input[id^='DTL_SoTienNT_']");

                    if (ctrInput) {
                        ctrInput2.val(ctrInput.val());
                    } else {
                        ctrInput.val("0");
                        ctrInput2.val("0");
                    }
                });
            }

            jsCalcTotalMoney();
        }

        //    function jsShowCustomerSignature()
        //    {        
        //        if  ($get('txtTenTK_KH_NH').value !='' )
        //        {
        //            window.showModalDialog("../../Find_DM/ShowSignature.aspx?account=" + $get('txtTK_KH_NH').value, "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;","_new");
        //        }
        //        else{
        //             alert('Phải truy vấn được thông tin khách hàng trước khi xem chữ ký');}
        //    }

        function jsShowCustomerSignature() {
            if ($get('txtTenTK_KH_NH').value != '') {
                var cifNo = $get('cifNo').value;

                window.showModalDialog("../../Find_DM/ShowSignature.aspx?account=" + $get('txtTK_KH_NH').value + "&cifNo=" + cifNo, "", "dialogWidth:700px;dialogHeight:500px;help:0;status:0;", "_new");
            } else {
                alert('Phải truy vấn được thông tin khách hàng trước khi xem chữ ký');
            }
        }

        function ValidNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function ValidInteger(obj) {
            //alert(obj.value);
            var i, strVal, blnChange;
            blnChange = false
            strVal = "";

            for (i = 0; i < (obj.value).length; i++) {
                switch (obj.value.charAt(i)) {
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9": strVal = strVal + obj.value.charAt(i);
                        break;
                    default: blnChange = true;
                        break;
                }
            }
            if (blnChange) {
                obj.value = strVal;
            }
        }
        String.prototype.replaceAll = function (strTarget, strSubString) {
            var strText = this;
            var intIndexOfMatch = strText.indexOf(strTarget);
            while (intIndexOfMatch != -1) {
                strText = strText.replace(strTarget, strSubString)
                intIndexOfMatch = strText.indexOf(strTarget);
            }
            return (strText);
        }

        function jsFormatNumber(control) {
            var number = accounting.unformat(control.value);
            control.value = accounting.formatNumber(number);
        }

        function jsFormatNumber2(control) {
            var number = accounting.unformat(control.value, ".");
            if ($("#ddlMaNT option:selected").val() != "VND")
                control.value = accounting.formatNumber(number, 2, ",", ".");
            else
                control.value = accounting.formatNumber(number);
        }


        function getSoTK_TruyVanCore(pttt) {
            PageMethods.getSoTK_TruyVanCore(pttt, getSoTK_TruyVanCore_Complete, getSoTK_TruyVanCore_Error);

        }

        function getSoTK_TruyVanCore_Complete(result, methodName) {
            jsShowHideRowProgress(false);
            if (result.length > 0) {
                $get('txtTK_KH_NH').value = result;
            } else {
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản tiền mặt ngân hàng" + error.get_message();
            }
        }
        function getSoTK_TruyVanCore_Error(error, userContext, methodName) {
            if (error !== null) {
                jsShowHideRowProgress(false);
                $get('txtTenTK_KH_NH').value = '';
                $get('txtSoDu_KH_NH').value = '';
                document.getElementById('divStatus').innerHTML = "Lỗi trong quá trình lấy tài khoản tiền mặt ngân hàng" + error.get_message();
            }
        }
        //
        function onBlurMaTieuMuc(id) {
            if ($('#DTL_Ma_TMuc_' + id).val().length > 0) {
                $.ajax({
                    type: "POST",
                    url: "frmLapDienNHKhac.aspx/onBlurMaTieuMuc",
                    cache: false,
                    data: JSON.stringify({
                        "v_maTieuMuc": $('#DTL_Ma_TMuc_' + id).val()
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (reponse) {

                        if (reponse.length = 0) {
                            alert("Có lỗi khi Tìm kiếm Sắc thuế, Nội dung theo Mã tiểu mục " + $('#DTL_Ma_TMuc_' + id).val());

                            $('#DTL_SAC_THUE_' + id).val('');
                            $('#DTL_Noi_Dung_' + id).val('');
                        } else {
                            var result = reponse;
                            var jsonData = JSON.parse(result.d);

                            if (jsonData.length == 1) {
                                if (jsonData[0].STT == 1) {
                                    //alert("Có lỗi khi Tìm kiếm Nội dung theo Mã tiểu mục " + $('#DTL_Ma_TMuc_' + id).val());
                                    $('#DTL_Noi_Dung_' + id).val('');
                                    $('#DTL_SAC_THUE_' + id).val(jsonData[0].VALUE);
                                } else if (jsonData[0].STT == 2) {
                                    //alert("Có lỗi khi Tìm kiếm Sắc thuế theo Mã tiểu mục " + $('#DTL_Ma_TMuc_' + id).val());
                                    $('#DTL_Noi_Dung_' + id).val(jsonData[0].VALUE);
                                    $('#DTL_SAC_THUE_' + id).val('');
                                }
                            } else {
                                for (var i = 0; i < jsonData.length; i++) {
                                    if (jsonData[i].STT == 1)
                                        $('#DTL_SAC_THUE_' + id).val(jsonData[i].VALUE);
                                    else if (jsonData[i].STT == 2)
                                        $('#DTL_Noi_Dung_' + id).val(jsonData[i].VALUE);
                                }
                            }
                        }

                    }, error: function (request, status, error) {
                        alert("Có lỗi khi Tìm kiếm Sắc thuế, Nội dung theo Mã tiểu mục " + $('#DTL_Ma_TMuc_' + id).val());
                    }
                });
            } else {
                $('#DTL_SAC_THUE_' + id).val('');
                $('#DTL_Noi_Dung_' + id).val('');
            }
        }

        function getNoiDungByMaTieuMuc() {
            var arrTieuMuc = {};
            var sTieuMuc = "";
            $('#grdChiTiet > tbody  > tr').each(function () {
                var ctrInput = $(this).find("input[id^='DTL_Ma_TMuc_']");
                var ctrInput2 = $(this).find("input[id^='DTL_Noi_Dung_']");
                if (ctrInput) {
                    var maTieuMuc = ctrInput.val();
                    sTieuMuc += "'" + maTieuMuc + "',";
                    ctrInput2.val('');
                }
            });

            sTieuMuc = sTieuMuc.substring(0, sTieuMuc.length - 1);

            if (arrTieuMuc) {
                $.ajax({
                    type: "POST",
                    url: "frmLapCTu_HQ.aspx/getNoiDungByMaTieuMuc",
                    cache: false,
                    data: JSON.stringify({
                        "v_jSon": sTieuMuc
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (reponse) {
                        if (reponse.d.length == 0) {
                            $('#grdChiTiet > tbody  > tr').each(function () {
                                var ctrInput = $(this).find("input[id^='DTL_Ma_TMuc_']");
                                var ctrInput2 = $(this).find("input[id^='DTL_Noi_Dung_']");
                                if (ctrInput) {
                                    ctrInput2.val("");
                                }
                            });
                        } else {
                            var result = reponse;
                            var jsonData = JSON.parse(result.d);

                            for (var i = 0; i < jsonData.length; i++) {
                                $('#grdChiTiet > tbody  > tr').each(function () {
                                    var ctrInput = $(this).find("input[id^='DTL_Ma_TMuc_']");
                                    var ctrInput2 = $(this).find("input[id^='DTL_Noi_Dung_']");

                                    if (ctrInput.val() == jsonData[i].MA_TMUC) {
                                        ctrInput2.val(jsonData[i].TEN);
                                    }
                                });
                            }
                        }

                    }
                });
            }
        }

        function jsCalcCurrency(id) {
            var tienVnd = accounting.unformat($("#DTL_SoTien_" + id).val());

            var arr = arrTyGia;
            var toFix = 0;
            var tyGia = 1;
            for (var i = 0; i < arrTyGia.length; i++) {
                var arr = arrTyGia[i].split(';');
                var value = arr[0];
                tyGia = arr[1];
                if (value == $("#ddlMaNT option:selected").val()) {
                    toFix = arr[2];
                    break;
                }
            }

            var tienNT = (tienVnd / tyGia).toFixed(toFix);
            if ($("#ddlMaNT option:selected").val() != "VND")
                $("#DTL_SoTienNT_" + id).val(accounting.formatNumber(tienNT, toFix, ",", "."));
            else
                $("#DTL_SoTienNT_" + id).val(accounting.formatNumber(tienNT));

            jsCalcTotalMoney();

        }

        function jsCalCharge() {

            if ($('#txtCharge').val() == "")
                $('#txtCharge').val("0");
            var dblPhi = parseFloat(accounting.unformat($('#txtCharge').val()));
            var dblVAT = parseInt(dblPhi / 10);

            var dblTongTien = parseFloat(accounting.unformat($('#txtTongTien').val()));
            var dblTongTrichNo = dblPhi + dblVAT + dblTongTien;

            $('#txtCharge').val(accounting.formatNumber(dblPhi));
            $('#txtVAT').val(accounting.formatNumber(dblVAT));

            $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo)).change();

        }

        function jsEnabledInputCharge() {
            var dblTongTien = parseFloat(accounting.unformat($('#txtTongTien').val()));
            var dblPhi = 0;
            var dblVAT = 0;
            var dblTongTrichNo = 0;
            if (document.getElementById('chkChargeType').checked) {
                $('#txtCharge').disabled = true;
                //neu ma checked thi tinh lai fee
                dblPhi = dblTongTien * feeRate / minFee;
                dblVAT = parseInt(dblTongTien * feeRate / minFee / 10);
                if (dblPhi < minFee) {
                    dblPhi = minFee;
                    //dblVAT=dblPhi/10;
                    dblVAT = parseInt(dblPhi / 10);

                }
                dblTongTrichNo = dblPhi + dblVAT + dblTongTien;
                $('#txtCharge').val(accounting.formatNumber(dblPhi));
                $('#txtVAT').val(accounting.formatNumber(dblVAT));

                $('#txtTongTrichNo').val(accounting.formatNumber(dblTongTrichNo)).change();

            } else {
                $('#txtCharge').prop('disabled', false);
                $('#txtCharge').val("0");
                $('#txtVAT').val("0");
                //$('#txtTongTrichNo').val($('#txtTongTien').val());
                $('#txtTongTrichNo').val($('#txtTongTien').val()).change();
            }

        }

        function valInteger(obj) {
            var i, strVal, blnChange;
            blnChange = false
            strVal = "";

            for (i = 0; i < (obj.value).length; i++) {
                switch (obj.value.charAt(i)) {
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9": strVal = strVal + obj.value.charAt(i);
                        break;
                    default: blnChange = true;
                        break;
                }
            }
            if (blnChange) {
                obj.value = strVal;
            }
        }

        function jsGetTenTinh_KB(ma_xa) {
            //$('#txtMaDBHC').val('');
            $('#txtTenDBHC').val('');
            if (typeof ma_xa !== 'undefined' && ma_xa !== null && ma_xa.toString() !== '') {
                PageMethods.GetTenDBHC(ma_xa, GetTenTinh_KB_Complete, GetTenTinh_KB_Error);
            }
        }

        function GetTenTinh_KB_Complete(result, methodName) {
            if (result.length > 0) {
                var ten_dbhc = result;
                //document.getElementById('txtMaDBHC').value = ten_Tinh[0];
                document.getElementById('txtTenDBHC').value = ten_dbhc;
            }
        }

        function GetTenTinh_KB_Error(error, userContext, methodName) {
        }


        function ShowMLNS(strType, id) {
            if (strType == 1)
                return FindDanhMuc('MucTMuc', 'DTL_Ma_TMuc_' + id, 'DTL_Noi_Dung_' + id, 'DTL_SoTien_' + id);
        }

        function jsGenThuTuButToan() {
            var arrSoTk = [];

            $('#grdChiTiet > tbody  > tr').each(function () {
                var ctrSoTk = $(this).find("input[id^='DTL_SO_TK_']");
                if (typeof ctrSoTk !== 'undefined' && ctrSoTk !== null) {
                    var objSoTk = {};
                    objSoTk["SO_TK"] = ctrSoTk.val();
                    objSoTk["TT_BTOAN"] = "";
                    //Kiem tra xem objGoc co gia tri chua
                    if (arrSoTk.length > 0) {
                        var flag = true;

                        for (var j = 0; j < arrSoTk.length; j++) {
                            var objDtl = arrSoTk[j];
                            if (objSoTk.SO_TK != arrSoTk[j].SO_TK)
                                flag = true;
                            else {
                                flag = false;
                                break;
                            }
                        }
                        if (flag)
                            arrSoTk.push(objSoTk);
                    } else
                        arrSoTk.push(objSoTk);
                }
            });



            for (var i = 0; i < arrSoTk.length; i++) {
                var objDtl = arrSoTk[i];
                objDtl.TT_BTOAN = i;
            }

            $('#grdChiTiet > tbody  > tr').each(function () {
                var ctrSoTk = $(this).find("input[id^='DTL_SO_TK_']");
                var ctrTtBToan = $(this).find("input[id^='DTL_TT_BTOAN_']");
                if (typeof ctrSoTk !== 'undefined' && ctrSoTk !== null) {
                    for (var jj = 0; jj < arrSoTk.length; jj++) {
                        var objDtl = arrSoTk[jj];
                        if (ctrSoTk.val() == objDtl.SO_TK) {
                            ctrTtBToan.val(objDtl.TT_BTOAN);
                            break;
                        }
                    }
                }
            });
        }

        function jsGhi_CTU2(sAct) {
            $('#cmdChuyenKS').prop('disabled', true);
            var objHDR = jsGetHDRObjectSP();
            var objDTL = jsGetDTLArraySP();
            var sType = null;
            var bIsEdit = $('#hdfIsEdit').val();

            jsValidMin();
            //curCtuStatus
            if (sAct == "GHIKS") {
                if (bIsEdit == "true" && curCtuStatus == "04")
                    sType = 'UPDATE_KS';
                else
                    sType = 'INSERT_KS';
            } else if (sAct == "GHIDC") {
                if (bIsEdit == "true")
                    sType = 'UPDATE_DC';
            } else
                return;
            if (!jsValidFormData2(objHDR, objDTL, sAct, sType)) {
                $('#cmdChuyenKS').prop('disabled', false);
                return;
            }
            //jsValidNHSHB_SP_GhiCtu($get('txtSHKB').value);
            if (document.getElementById('hndNHSHB').value.length < 8) {
                alert('Kho bạc không mở tại SHB!');
                $('#cmdChuyenKS').prop('disabled', false);
                return;
            }
            if (jsValidFormData2(objHDR, objDTL, sAct, sType)) {
                $.ajax({
                    type: 'POST',
                    url: 'frmLapDienNHKhac.aspx/Ghi_CTu2',
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    data: JSON.stringify({
                        ctuHdr: objHDR,
                        ctuDtl: objDTL,
                        sAction: sAct,
                        sSaveType: sType
                    }),
                    success: function (reponse) {
                        var sRet = reponse.d.split(';');
                        if (sRet && sRet[0] == 'Success') {
                            var sOut = '';
                            if ((sRet[2]) && (sRet[2].indexOf('/') > 0) && (sRet[2].split('/').length > 0)) {
                                sOut = sRet[2].split('/');
                            }

                            switch (sType) {
                                case "INSERT":
                                    if ((sOut instanceof Array) && (typeof sOut[0] !== 'undefined' && sOut[0] !== null) && (typeof sOut[1] !== 'undefined' && sOut[1] !== null) && (typeof sOut[2] !== 'undefined' && sOut[2] !== null)) {
                                        alert('Hoàn thiện thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                    } else {
                                        alert('Hoàn thiện thành công!');
                                    }
                                    jsEnableDisableControlButton('6');
                                    break;
                                case "UPDATE":
                                    if ((sOut instanceof Array) && (typeof sOut[0] !== 'undefined' && sOut[0] !== null) && (typeof sOut[1] !== 'undefined' && sOut[1] !== null) && (typeof sOut[2] !== 'undefined' && sOut[2] !== null)) {
                                        alert('Cập nhật thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                    } else {
                                        alert('Cập nhật thành công!');
                                    }
                                    jsEnableDisableControlButton('6');
                                    break;
                                case "INSERT_KS":
                                    if ((sOut instanceof Array) && (typeof sOut[0] !== 'undefined' && sOut[0] !== null) && (typeof sOut[1] !== 'undefined' && sOut[1] !== null) && (typeof sOut[2] !== 'undefined' && sOut[2] !== null)) {
                                        alert('Hoàn thiện & gửi kiểm soát thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                    } else {
                                        alert('Hoàn thiện & gửi kiểm soát thành công!');
                                    }
                                    jsEnableDisableControlButton('7');
                                    break;
                                case "UPDATE_KS":
                                    if ((sOut instanceof Array) && (typeof sOut[0] !== 'undefined' && sOut[0] !== null) && (typeof sOut[1] !== 'undefined' && sOut[1] !== null) && (typeof sOut[2] !== 'undefined' && sOut[2] !== null)) {
                                        alert('Cập nhật & gửi kiểm soát thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                    } else {
                                        alert('Cập nhât & gửi kiểm soát thành công!');
                                    }
                                    jsEnableDisableControlButton('7');
                                    break;
                                case "UPDATE_DC":
                                    if (true) {
                                        alert('Cập nhật điều chỉnh & gửi kiểm soát thành công! Số chứng từ: ' + sOut[1] + ' - SHKB: ' + sOut[0] + ' - Số BT: ' + sOut[2]);
                                    }
                                    jsEnableDisableControlButton('3');
                                    break;
                            }

                            //re-load lai chung tu len form (trong này đã có set hdfDescCTu)
                            if ((sOut) && (sOut.length > 1)) {
                                jsGetCTU(sOut[1]);
                            } else {
                                jsGetCTU(null);
                            }
                        } else {
                            $('#hdfDescCTu').val('SHKB/SoCT/SoBT://');
                            switch (sType) {
                                case "INSERT":
                                    alert('Hoàn thiện không thành công: ' + sRet[1]);
                                    jsEnableDisableControlButton('4');
                                    break;
                                case "UPDATE":
                                    alert('Cập nhật không thành công: ' + sRet[1]);
                                    jsEnableDisableControlButton('4');
                                    break;
                                case "INSERT_KS":
                                    alert('Hoàn thiện & gửi kiểm soát không thành công: ' + sRet[1]);
                                    jsEnableDisableControlButton('4');
                                    break;
                                case "UPDATE_KS":
                                    alert('Cập nhật & gửi kiểm soát không thành công: ' + sRet[1]);
                                    jsEnableDisableControlButton('3');
                                    break;
                                case "UPDATE_DC":
                                    alert('Cập nhật điều chỉnh & gửi kiểm soát không thành công: ' + sRet[1]);
                                    jsEnableDisableControlButton('10');
                                    break;
                            }
                        }
                        $('#hdfIsEdit').val(true);
                        jsLoadCTList();
                    },
                    error: function (request, status, error) {
                        var err;
                        err = JSON.parse(request.responseText);
                        if (typeof err !== 'undefined' && err !== null) {
                            if (err.Message.indexOf(';') > -1) {
                                jsCheckUserSession(err.Message);
                            }
                            alert(err.Message);
                            switch (sType) {
                                case "INSERT":
                                    jsEnableDisableControlButton('4');
                                    break;
                                case "UPDATE":
                                    jsEnableDisableControlButton('4');
                                    break;
                                case "INSERT_KS":
                                    jsEnableDisableControlButton('4');
                                    break;
                                case "UPDATE_KS":
                                    jsEnableDisableControlButton('3');
                                    break;
                                case "UPDATE_DC":
                                    jsEnableDisableControlButton('10');
                                    break;
                            }
                        }
                    }
                });
            }
        }


        function jsValidFormData2(HDR, DTL, sAction, sType) {
            if (!HDR.NGAY_CTITAD) {
                alert('Vui lòng nhập vào ngày citad nhận!');
                $('#txtNgayCitad').focus();
                return false;
            }
            if (!HDR.SOBT_CITAD) {
                alert('Vui lòng nhập vào số bút toán trên citad!');
                $('#txtSoBTCitad').focus();
                return false;
            }


            if (HDR.TEN_NNTHUE.length > 200) {
                alert('Tên người nộp thuế > 200 ký tự. Vui lòng kiểm tra lại');
                $('#txtTenNNT').focus();
                return false;
            }
            //validate HDR
            if (!HDR.TEN_NNTHUE) {
                alert('Vui lòng nhập vào tên người nộp thuế');
                $('#txtTenNNT').focus();
                return false;
            }
            //validate HDR
            if (!HDR.MA_NNTHUE) {
                alert('Vui lòng nhập vào Mã số thuế');
                $('#txtMaNNT').focus();
                return false;
            }
            if (HDR.DC_NNTHUE.length > 200) {
                alert('Địa chỉ người nộp thuế > 200 ký tự. Vui lòng kiểm tra lại');
                $('#txtDChiNNT').focus();
                return false;
            }

            if (!HDR.NGAY_NTIEN) {
                alert('Vui lòng nhập vào ngày NNT nộp tiền!');
                $('#txtNgayNNT').focus();
                return false;
            }

            if (!HDR.SHKB) {
                alert('Vui lòng nhập vào SHKB');
                $('#txtSHKB').focus();
                return false;
            }

            if (!HDR.NH_GIU_TKKB) {
                alert('Vui lòng nhập vào mã ngân hàng giữ tài khoản kho bạc!');
                $('#txtMaNH_TK_KBNN').focus();
                return false;
            }

            //if (!HDR.MA_DTHU) {
            //    alert('Vui lòng nhập vào mã điểm thu');
            //    $('#txtMaDThu').focus();
            //    return false;
            //}

            if (!HDR.NGAY_CTU) {
                alert('Vui lòng nhập vào ngày chứng từ');
                $('#txtNgayChungTu').focus();
                return false;
            }

            if (!HDR.TK_CO) {
                alert('Vui lòng nhập vào tài khoản có');
                $('#txtTKCo').focus();
                return false;
            }
            //if (!HDR.SO_CT_NH) {
            //    alert('Vui lòng nhập vào số giao dịch Corebank!');
            //    $('#txtRefCore').focus();
            //    return false;
            //}

            //if (!HDR.TK_NO) {
            //    alert('Vui lòng nhập vào tài khoản nợ');
            //    $('#txtTKNo').focus();
            //    return false;
            //}

            if (!HDR.MA_CQTHU) {
                alert('Vui lòng nhập vào mã cơ quan thu');
                $('#txtMaCQThu').focus();
                return false;
            }
            if (!HDR.TEN_CQTHU) {
                alert('Vui lòng nhập vào tên cơ quan thu');
                $('#txtTenCQThu').focus();
                return false;
            }

            if (!HDR.MA_DBHC) {
                alert('Vui lòng nhập vào mã địa bàn hành chính');
                $('#txtMaDBHC').focus();
                return false;
            }
            //if (!HDR.TK_KH_NHAN) {
            //    alert('Vui lòng nhập vào số tài khoản khách hàng nhận');
            //    $('#txtSTK_KH_Nhan').focus();
            //    return false;
            //}

            //txtTK_KH_NH
            if (!HDR.TK_KH_NH) {
                alert('Vui lòng nhập vào số tài khoản khách hàng!');
                $('#txtTK_KH_NH').focus();
                return false;
            }
            if (!HDR.TY_GIA) {
                alert('Vui lòng nhập vào tỷ giá');
                $('#txtTyGia').focus();
                return false;
            }

            if (!HDR.TTIEN) {
                alert('Vui lòng nhập vào số tiền VND');
                $('#txtTTienVND').focus();
                return false;
            }
            if (!HDR.TTIEN_NT) {
                alert('Vui lòng nhập vào số tiền nguyên tệ');
                $('#txtTTienNT').focus();
                return false;
            }
            if (!HDR.REMARK) {
                alert('Vui lòng nhập vào trường diễn giải!');
                $('#txtDienGiai').focus();
                return false;
            }

            var text = $("#txtDienGiai").val();
            var match = /\r|\n/.exec(text);
            if (match) {
                alert('Trường diễn giải có ký tự xuống dòng, vui lòng nhập diễn giải trên cùng 1 dòng!');
                return false;
            }

            //validate DTL       
            if (!(DTL) || !(DTL instanceof Array) || !(DTL.length > 0)) { //IS: null, undefined, 0, NaN, false, or ""
                alert('Chứng từ thuế phải có chi tiết. Vui lòng kiểm tra lại');
                return false;
            }

            //var strLthue = "";
            //if (HDR.MA_CQTHU.length > 0) {
            //    strLthue = HDR.MA_CQTHU.substring(0,1);
            //}

            var strLthue = $("#ddlMaLThue option:selected").val();
           // console.log("LTHUE: "+ strLthue);
            var tttien_dtl = 0;
            var tttien_nt_dtl = 0;
           
            var flag = true;
            for (var j = 0; j < DTL.length; j++) {
                var objDTL = DTL[j];
                var ii = j + 2;

                if (strLthue == "04") {
                    if (objDTL.SO_TK.length == 0) {
                        alert('Số tờ khai bị đang trống. Xin vui lòng nhập số tờ khai!');
                        flag = false;
                        break;
                    }
                    if (objDTL.SAC_THUE.length == 0) {
                        alert('Mã sắc thuế bị đang trống. Xin vui lòng nhập mã sắc thuế!');
                        flag = false;
                        break;
                    }

                } 
               
                if (objDTL.MA_CHUONG.length == 0) {
                    alert('Mã chương bị đang trống. Xin vui lòng nhập mã chương!');
                    flag = false;
                    break;
                }
                if (objDTL.MA_TMUC.length == 0) {
                    alert('Mã tiểu mục bị đang trống. Xin vui lòng nhập mã tiểu mục!');
                    flag = false;
                    break;
                }
                if (objDTL.NOI_DUNG.length == 0) {
                    alert('Trường nội dung tiểu mục bị đang trống. Xin vui lòng nhập nội dung!');
                    flag = false;
                    break;
                }
                //if (objDTL.SOTIEN.length == 0) {
                //    alert('Số tiền nộp thuế bị đang trống. Xin vui lòng nhập số tiền!');
                //    flag = false;
                //    break;
                //} else {
                //    if (parseFloat(objDTL.SOTIEN) <= 0) {
                //        alert('Số tiền nộp thuế bị đang nhỏ hơn 0. Xin vui lòng nhập số tiền!');
                //        flag = false;
                //        break;
                //    }
                //}

                //tttien_dtl += parseFloat(objDTL.SOTIEN);
                //if (objDTL.SOTIEN_NT.length == 0) {
                //    alert('Số tiền nộp thuế bị đang trống. Xin vui lòng nhập số tiền!');
                //    flag = false;
                //    break;
                //} else {
                //    if (parseFloat(objDTL.SOTIEN_NT) <= 0) {
                //        alert('Số tiền nộp thuế bị đang nhỏ hơn 0. Xin vui lòng nhập số tiền!');
                //        flag = false;
                //        break;
                //    }
                //}

                //tttien_nt_dtl += parseFloat(objDTL.SOTIEN_NT);
            }



            for (var j = 0; j < DTL.length; j++) {
                var objDTL = DTL[j];
                var ii = j + 2;
                if (objDTL.SOTIEN.length == 0) {
                    alert('Số tiền nộp thuế bị đang trống. Xin vui lòng nhập số tiền!');
                    flag = false;
                    break;
                } else {
                    if (parseFloat(objDTL.SOTIEN) <= 0) {
                        alert('Số tiền nộp thuế bị đang nhỏ hơn 0. Xin vui lòng nhập số tiền!');
                        flag = false;
                        break;
                    }
                }

                tttien_dtl += parseFloat(objDTL.SOTIEN);
                if (objDTL.SOTIEN_NT.length == 0) {
                    alert('Số tiền nộp thuế bị đang trống. Xin vui lòng nhập số tiền!');
                    flag = false;
                    break;
                } else {
                    if (parseFloat(objDTL.SOTIEN_NT) <= 0) {
                        alert('Số tiền nộp thuế bị đang nhỏ hơn 0. Xin vui lòng nhập số tiền!');
                        flag = false;
                        break;
                    }
                }

                tttien_nt_dtl += parseFloat(objDTL.SOTIEN_NT);

            }
            if (parseFloat(HDR.TTIEN) != parseFloat(HDR.TTIEN_NT))
            {
                alert('Tổng tiền VND và tổng tiền nguyên tệ đang không khớp. Xin vui lòng nhập lại số tiền!');
                return false;
            }

            if (parseFloat(HDR.TTIEN) != tttien_dtl)
            {
                alert('Tổng tiền nộp và tổng tiền chi tiết nộp thuế đang không khớp. Xin vui lòng nhập lại số tiền!');
                return false;
                //flag = false;
            }
            if (parseFloat(HDR.TTIEN_NT) != tttien_nt_dtl) {
                alert('Tổng tiền nguyên tệ nộp và tổng tiền nguyên tệ chi tiết nộp thuế đang không khớp. Xin vui lòng nhập lại số tiền!');
                return false;
                //flag = false;
            }
            return flag;




        }
        function checkTKGL() {
            for (var i = 0; i < arrTKGL.length; i++) {
                var arr = arrTKGL[i].split(';');
                if (arr[0] == $get('txtTKGL').value) {
                    return true;
                }
            }
            return false;
        }
        function jsCheckEmptyVal(cmpCtrl, divCtrl, strMessage) {
            if (document.getElementById(cmpCtrl).value == '') {
                document.getElementById(divCtrl).innerHTML = strMessage;
                return false;
            }
            return true;
        }

        function jsValidSoDuKH() {
            var dblTongTrichNo = parseFloat($get('txtTongTien').value.replaceAll(',', ''));
            var dblTongTrichCu = parseFloat($get('hdnTongTienCu').value.replaceAll(',', ''));
            if ($get('hdnTongTienCu').value == null || $get('hdnTongTienCu').value == "" || $get('txtTK_KH_NH').value.substring(0, 3) == 'VND') {
                dblTongTrichCu = 0;
            } else
                dblTongTrichCu = parseFloat($get('hdnTongTienCu').value.replaceAll(',', ''));
            var dblSoDuKH = parseFloat($get('txtSoDu_KH_NH').value.replaceAll('.', '').replaceAll(',', ''));

            if (dblTongTrichNo - dblTongTrichCu <= dblSoDuKH) {
                return false;
            } else {
                return true;
            }
        }


        function jsGet_TenTKGL() {

            if (arrTKGL.length > 0) {
                for (var i = 0; i < arrTKGL.length; i++) {

                    var arr = arrTKGL[i].split(';');
                    $get('txtTK_KH_NH').value = arr[0];
                    $get('txtTenTK_KH_NH').value = arr[1];
                    if (arr[0] == $('#txtTKGL').val()) {
                        $('#txtTenTKGL').val(arr[1]);
                        break;
                    }
                }
            }
        }


        function jsAddRowDTL_Grid2(arrParam) {

            var sID = jsGenDTLrowID();
            var sTT_BToan = jsGenSoButToan_DTL();
            var sAppendRows = '';
            var sNgayDK = '';

            //arrParam: array of Item inside HQ result (it's include DTL object array)
            if ((typeof arrParam !== 'undefined') && (arrParam !== null) && (arrParam instanceof Array) && (arrParam.length > 0)) {
                for (var i = 0; i < arrParam.length; i++) {
                    var arrCT_No = arrParam[i].CT_No;
                    if (arrCT_No && arrCT_No.length > 0) {
                        for (var j = 0; j < arrCT_No.length; j++) {
                            if (!isNaN(sID)) {
                                sID++;
                                sNgayDK = ('0' + arrParam[i].Ngay_DK.substr(8, 2)).slice(-2) + '/' + ('0' + arrParam[i].Ngay_DK.substr(5, 2)).slice(-2) + '/' + arrParam[i].Ngay_DK.substr(0, 4);
                                sAppendRows = sAppendRows + '<tr>';
                                if (arrCT_No[j].DuNo > 0) {
                                    sAppendRows = sAppendRows + "<td style='text-align: center;'><input type='checkbox' id='DTL_chkSelect_" + sID + "' onclick='jsCalcTotalMoney();jsGenThuTuButToan();' checked /></td>";
                                } else {
                                    sAppendRows = sAppendRows + "<td style='text-align: center;'><input type='checkbox' id='DTL_chkSelect_" + sID + "'  onclick='jsCalcTotalMoney();jsGenThuTuButToan();' /></td>";
                                }
                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SO_TK_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' value='" + arrParam[i].So_TK + "' onblur='jsGenThuTuButToan();' /></td>";
                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Ngay_TK_" + sID + "' style='width: 98%; border-color: White;' onkeyup='if (event.keyCode !== 8){javascript:return mask(this.value,this,\"2,5\",\"/\");}' class='inputflat' maxlength='10' onblur='javascript:CheckDate(this);' value='" + sNgayDK + "' /></td>";

                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SAC_THUE_" + sID + "' style='width: 98%; border-color: White;' class='inputflat' maxlength='2'  value='" + arrCT_No[j].LoaiThue + "' /></td>";
                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_MA_CHUONG_" + sID + "' maxlength='3' style='width: 98%; border-color: White;' class='inputflat' value='" + arrParam[i].Ma_Chuong + "' /></td>";
                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Ma_TMuc_" + sID + "' maxlength='4' style='width: 98%; border-color: White;' class='inputflat'  value='" + arrCT_No[j].TieuMuc + "' onblur='onBlurMaTieuMuc(" + sID + ")' onkeypress='if (event.keyCode==13){ShowMLNS(1," + sID + ")}' /></td>";

                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_Noi_Dung_" + sID + "' style='width: 98%; border-color: White;' class='inputflat'  value='" + arrCT_No[j].Noi_Dung + "' /></td>";
                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_KY_THUE_" + sID + "' style='width: 98%; border-color: White;' class='inputflat'  value='" + arrCT_No[j].Ky_Thue + "'   maxlength='10' /></td>";
                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SoTien_" + sID + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat'  onfocus='this.select()' onkeyup='ValidInteger(this); jsFormatNumber(this);' maxlength='17' onblur='jsCalcTotalMoney(); jsCalcCurrency(" + sID + ");' value='" + arrCT_No[j].DuNo + "' /></td>";
                                sAppendRows = sAppendRows + "<td><input type='text' id='DTL_SoTienNT_" + sID + "' style='width: 98%; text-align: right; border-color: White; font-weight: bold;' class='inputflat'  onfocus='this.select()' onkeyup='jsFormatNumber2(this);' maxlength='17' value='" + arrCT_No[j].DuNo + "' /></td>";

                                sAppendRows = sAppendRows + "<td style = 'display: none;'><input type='text' id='DTL_TT_BTOAN_" + sID + "' style='width: 98%; display: none;' class='inputflat' value='" + sTT_BToan + "' /></td>";
                                sAppendRows = sAppendRows + '</tr>';
                            }
                        }
                    }
                }
                $('#grdChiTiet tr:last').after(sAppendRows);
                jsFormatNumberDTL_Grid();
            }
        }

        function jsValidMin() {
            var TKCo = document.getElementById('txtTKCo').value;
            if (TKCo.length < 4) {

                alert("TK Có NSNN cần tối thiểu 4 ký tự");
            }
        };

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <input type="hidden" id="hdfSoBT" value="" />
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr valign="top">
            <!--DANH SACH CHUNG TU-->
            <td valign="top" width="25%" style="text-align: left;">
                <asp:Panel ID="pnlDSCT" runat="server" Height="450px" Width="99%" Style="overflow: auto;">
                    <table class="grid_data" border="0" cellpadding="0" cellspacing="0" width="100%"
                        style="padding: 10px 0; display: none;">
                        <tr>
                            <td>
                                <input type="text" id="txtTraCuuSoCtu" value="" style="width: 110px;" />
                            </td>
                            <td style="text-align: center;">
                                <input type="button" id="btnTraCuuSoCtu" value="Tra cứu" onclick="jsLoadCTList()"
                                    style="width: 70px;" />
                            </td>
                        </tr>
                    </table>
                    <table class="grid_data" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr class='grid_header'>
                            <td style="width: 30%">Số CT
                            </td>
                            <td style="width: 30%">Số GD Corebank
                            </td>
                            <td style="width: 40%">Trạng thái
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <div id='divDSCT'>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr style="display: none">
                        <td style="width: 10%">
                            <img src="../images/icons/ChuaKS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">Mới nhận từ CITAD
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../images/icons/ChuyenKS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">Chờ kiểm soát
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../images/icons/DaKS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">Hạch toán thành công
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../images/icons/KSLoi.png" />
                        </td>
                        <td style="width: 90%" colspan="2">Chuyển trả
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../images/icons/HuyHS.gif" />
                        </td>
                        <td style="width: 90%" colspan="2">Chờ duyệt hủy
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../images/icons/Huy_KS.png" />
                        </td>
                        <td style="width: 90%" colspan="2">Đã hủy
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                            <img src="../images/icons/TT_CThueLoi.gif" />
                        </td>
                        <td style="width: 90%" colspan="2">Chuyển TTSP lỗi
                        </td>
                    </tr>
                </table>
            </td>
            <!--THONG TIN CHUNG TU-->
            <td valign="top" width="80%">

                <%--<!--THONG BAO-->
                <div class="errorMessage" style="width: 100%; margin: 0; padding: 0;">
                    <div id="divStatus" style="width: 100%; margin: 0; padding: 0; background-color: Aqua; font-weight: bold; font-size: 18pt; display: none;">
                        <br />
                    </div>
                    <div id="divProgress" style="width: 100%; margin: 0; padding: 0; background-color: Aqua; font-weight: bold; font-size: 18pt; display: none;">
                        Đang xử lý. Xin chờ một lát...
                    </div>
                </div>
                <!--THONG TIN TO KHAI-->
                <br />--%>
                <div id="divTTinToKhai" style="width: 100%; margin: 0; padding: 0;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr align="left" class="grid_header">
                            <td align="left" style="width: 70%;">
                                <div style="float: left;">
                                    THÔNG TIN CHỨNG TỪ&nbsp;
                                </div>
                                <div id="lblSoCTu" style="float: left; display: inline;">
                                </div>
                            </td>
                            <td align="right">Trường <span style="color: #FF0000; font-weight: bold">(*)</span> là bắt buộc nhập
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="2">
                                <div id="pnlTTinTKhai" style="width: 100%; height: 610px;">
                                    <table width="100%" cellspacing="0" cellpadding="3" rules="all" border="1" style="border-width: 1px; border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse;">
                                         <tr align="left" id="rowTTCITAD">
                                            <td>
                                                <b>Ngày Citad nhận</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtNgayCitad" type="text" class="inputflat" style="width: 98%; background: Aqua;" maxlength="10" onkeyup='if (event.keyCode !== 8){javascript:return mask(this.value,this,\"2,5\",\"/\");}'  onblur='javascript:CheckDate(this)' />
                                            </td>
                                            <td style="width: 15%;">
                                                <b>Số bút toán trên Citad</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtSoBTCitad" type="text" class="inputflat" style="width: 98%; background: Aqua;" maxlength="20" />
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowMaNNT">
                                            <td>
                                                <b>Mã số thuế</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtMaNNT" type="text" class="inputflat" style="width: 98%; background: Aqua;" maxlength="14" />
                                            </td>
                                            <td style="width: 15%;">
                                                <b>Tên NNT</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtTenNNT" type="text" class="inputflat" style="width: 98%; background: Aqua;" maxlength="200" />
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowDChiNNT">
                                            <td colspan="1">
                                                <b>Địa chỉ NNT</b>
                                            </td>
                                            <td colspan="3" style="width: 100%;">
                                                <input id="txtDChiNNT" type="text" class="inputflat" style="width: 99%; height: 16px;" maxlength="200" />
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowNNTien">
                                            <td>
                                                <b>Mã người nộp tiền</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtMaNNTien" type="text" class="inputflat" style="width: 98%;" maxlength="15" />
                                            </td>
                                            <td style="width: 15%;">
                                                <b>Tên người nộp tiền</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtTenNNTien" type="text" class="inputflat" style="width: 98%; margin-left: 0px;" maxlength="200" />
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowDChiNNTien">
                                            <td>
                                                <b>Địa chỉ người nộp tiền</b>
                                            </td>
                                            <td  style="width: 35%;">
                                                <input id="txtDChiNNTien" type="text" class="inputflat" style="width: 99%;" maxlength="200" />
                                            </td>
                                            <td style="width: 15%;">
                                                <b>Số Ref No SHB</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtRefNoSHB" type="text" class="inputflat" style="width: 99%;" maxlength="50" readonly/>
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowSHKB">
                                            <td>
                                                <b>Số hiệu kho bạc</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtSHKB" maxlength="4" type="text" class="inputflat" style="width: 98%; background: Aqua;"  onkeypress="if (event.keyCode==13){ShowLov('SHKB');}" onblur="jsGet_TenKB();jsValidNHSHB_SP();jsGetMaDBHC();" onchange="jsGet_TenKB();jsValidNHSHB_SP();jsGetMaDBHC();" />
                                            </td>
                                            <td style="width: 15%;">
                                                <b>Tên kho bạc</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtTenKB" type="text" class="inputflat" style="width: 98%;" maxlength="200" />
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowNgayKBDThu">
                                            <%-- <td>
                                                <b>Ngày kho bạc</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtNgayKB" type="text" class="inputflat" style="width: 98%; background: Aqua;" />
                                            </td>--%>
                                            <td style="width: 15%;">
                                                <b>Ngày NNT nộp tiền</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtNgayNNT" type="text" class="inputflat" style="width: 98%; background: Aqua;" onkeyup='if (event.keyCode !== 8){javascript:return mask(this.value,this,\"2,5\",\"/\");}'  maxlength='10' onblur='javascript:CheckDate(this)'/>
                                            </td>
                                               <td style="width: 15%;">
                                                <b>Số GD Corebank</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtRefCore" type="text" class="inputflat" style="width: 98%; background: Aqua;" readonly/>
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowBTNgayCT">
                                            <td>
                                                <b>Mã điểm thu</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtMaDThu" type="text" class="inputflat" style="width: 98%;" maxlength="2" />
                                            </td>
                                            <td style="width: 15%;">
                                                <b>Ngày chứng từ</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtNgayChungTu" type="text" class="inputflat" style="width: 98%; background: Aqua;" maxlength="10" onkeyup='if (event.keyCode !== 8){javascript:return mask(this.value,this,\"2,5\",\"/\");}'  onblur='javascript:CheckDate(this)' />
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowTKCo">
                                            <td>
                                                <b>Tài khoản có</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtTKCo" type="text" class="inputflat" style="width: 98%; background: Aqua;" onblur="javascript:jsValidMin();" maxlength="16" />
                                            </td>
                                            <td style="width: 15%;">
                                                <b>Tài khoản nợ</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtTKNo" type="text" class="inputflat" style="width: 98%;" maxlength="16"/>
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowMaLThue">
                                            <td>
                                                <b>Mã loại thuế</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <select id="ddlMaLThue" style="width: 98%; background: Aqua;" class="inputflat">
                                                    <option value="01" selected>01_Khoản thu do cơ quan thuế quản lý</option>
                                                    <option value="02">02_Thu phí, lệ phí bộ ngành</option>
                                                    <option value="03">03_Khoản thu do cơ quan khác quản lý</option>
                                                    <option value="04">04_Khoản thu do cơ quan hải quan quản lý</option>
                                                </select>
                                            </td>
                                            <td style="width: 15%;">
                                                <b>Cơ quan ra quyết định</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtCQQD" type="text" class="inputflat" style="width: 98%;" maxlength="50" />
                                            </td>
                                        </tr>

                                        <tr align="left" id="rowCQThu">
                                            <td>
                                                <b>Mã cơ quan thu</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtMaCQThu" maxlength="7" type="text" class="inputflat" style="width: 98%; background: Aqua;" onkeypress="if (event.keyCode==13){ShowLov('CQT');}"  onblur="jsLoadTenCQT('txtMaCQThu','txtTenCQThu');" />
                                            </td>
                                            <td style="width: 15%;">
                                                <b>Tên cơ quan thu</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtTenCQThu" type="text" class="inputflat" style="width: 98%; background: Aqua;" maxlength="200" />
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowDBHC">
                                            <td>
                                                <b>Mã ĐBHC</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                               <input id="txtMaDBHC" maxlength="5" type="text" class="inputflat" style="width: 98%; background: Aqua;" onblur="jsGetTenTinh_KB(this.value);jsGet_TenDBHC();" onchange="jsGet_TenDBHC();" />
                                                <%--<input id="txtMaDBHC" type="text" class="inputflat" style="width: 98%; background: Aqua;" onblur="jsGet_TenDBHC();" onchange="jsGet_TenDBHC();" />--%>
                                            </td>
                                            <td style="width: 15%;">
                                                <b>Tên ĐBHC</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtTenDBHC" type="text" class="inputflat" style="width: 98%;" />
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowTK_KH_NH">
                                            <td>
                                                <b>Số tài khoản KH chuyển</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtTK_KH_NH" type="text" class="inputflat" style="width: 98%;background: Aqua;" />
                                            </td>
                                            <td style="width: 15%;">
                                                <b>Ngày khách hàng NH</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtNgayKH_NH" type="text" class="inputflat" style="width: 98%;" onkeyup='if (event.keyCode !== 8){javascript:return mask(this.value,this,\"2,5\",\"/\");}' maxlength='10' onblur='javascript:CheckDate(this)' />
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowKHNhan">
                                            <td>
                                                <b>Tên khách hàng nhận</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtTenKH_Nhan" type="text" class="inputflat" style="width: 98%;" maxlength="200" />
                                            </td>
                                            <td style="width: 15%;">
                                                <b>STK KH nhận</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtSTK_KH_Nhan" type="text" class="inputflat" style="width: 98%;" />
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowDiaChiKHNhan">
                                            <td colspan="1">
                                                <b>Địa chỉ khách hàng nhận</b>
                                            </td>
                                            <td colspan="3" style="width: 100%;">
                                                <input id="txtDiaChiKH_Nhan" type="text" class="inputflat" style="width: 99%; height: 16px;" maxlength="200" />
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowMaNHGiuTK">
                                            <td>
                                                <b>Mã NH giữ TK NNT</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtMaNH_TK_NNT" type="text" class="inputflat" style="width: 98%;" />
                                            </td>
                                            <td style="width: 15%;">
                                                   <b>Mã NH giữ TK KBNN</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtMaNH_TK_KBNN" type="text" class="inputflat" style="width: 98%;background: Aqua;" readonly/>
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowMaNT">
                                            <td>
                                                <b>Mã nguyên tệ</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <select id="ddlMaNT" style="width: 98%; background: Aqua;" class="inputflat">
                                                    <option value="VND" selected>VND</option>
                                                    <option value="USD">USD</option>
                                                </select>
                                            </td>
                                            <td style="width: 15%;">
                                                <b>Tỷ giá</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtTyGia" type="text" class="inputflat" style="width: 98%; background: Aqua;"  readonly />
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowTTTien">
                                            <td>
                                                <b>Tổng tiền VND Citad/VCB</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtTTienVND" type="text" class="inputflat" style="width: 98%; background: Aqua;" onkeyup="ValidInteger(this);jsFormatNumber(this);" />
                                            </td>
                                            <td style="width: 15%;">
                                                <b>Tổng tiền NT</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td style="width: 35%;">
                                                <%--<input id="txtTTienNT" type="text" class="inputflat" style="width: 98%; background: Aqua;" />--%>
                                                <input id="txtTTienNT" type="text" class="inputflat" style="width: 98%; background: Aqua;" onkeyup="ValidInteger(this);jsFormatNumber(this);" />
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowMaHQ">
                                            <td>
                                                <b>Mã hải quan</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtMaHQ" maxlength="4" type="text" class="inputflat" style="width: 98%;" onblur="jsLoadByCQT(this.value);"
                                                    onchange="jsLoadByCQT(this.value);" />
                                            </td>
                                            <td style="width: 15%;">
                                                <b>Tên hải quan</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtTenMaHQ" type="text" class="inputflat" style="width: 98%;" />
                                            </td>
                                        </tr>
                                        <tr align="left" id="rowMaHQPH">
                                            <td>
                                                <b>Mã hải quan phát hành</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtMaHQPH" maxlength="4" type="text" class="inputflat" style="width: 98%;" onblur="jsLoadByCQT();" onkeypress="if (event.keyCode==13){ShowLov('MHQPH');}" />
                                            </td>
                                            <td style="width: 15%;">
                                                <b>Tên hải quan phát hành</b>
                                            </td>
                                            <td style="width: 35%;">
                                                <input id="txtTenMaHQPH" type="text" class="inputflat" style="width: 98%;" />
                                            </td>
                                        </tr>
                                         <tr align="left" id="rowTenNguoiChuyen">
                                            <td>
                                                <b>Tên Người chuyển</b>
                                            </td>
                                            <td colspan="3">
                                                <input id="txtTen_Nguoi_chuyen" type="text" class="inputflat" style="width: 99%;" maxlength ="250" />
                                            </td>
                                        </tr>
                                        <tr id="rowDienGiai">
                                            <td align="left">
                                                <b>Diễn giải(210)</b>&nbsp;<span class="requiredField">(*)</span>
                                            </td>
                                            <td colspan="4">
                                                 <textarea id="txtDienGiai" style="width: 99%; height: 48px;" class="inputflat" rows="20" cols="1" 
                                                    maxlength="210"></textarea>
                                            </td>
                                        </tr>
                                          <tr id="rowContentEx">
                                            <td align="left">
                                                <b>Diễn giải(Citad2.5)</b>
                                            </td>
                                            <td colspan="4">
                                                <%--<textarea id="txtDienGiai" style="width: 99%; height: 48px;" class="inputflat" cols="1" readonly="readonly"
                                                    maxlength="4000"></textarea>--%>
                                                 <textarea id="txtContentEx" style="width: 99%; height: 48px;" class="inputflat" rows="20" cols="1" 
                                                    maxlength="4000"></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                               <!--THONG BAO-->
                              <div class="errorMessage" style="width: 100%; margin-top: 40px; padding: 0;">
                                <div id="divStatus" style="width: 100%; margin: 0; padding: 0; background-color: Aqua; font-weight: bold; font-size: 18pt;display: none;">
                                </div>
                                <div id="divProgress" style="width: 100%; margin-top: 5px; padding: 0; background-color: Aqua; font-weight: bold; font-size: 18pt;display: none;">
                                       Đang xử lý. Xin chờ một lát...
                                </div>
                              </div>
                              <!--CHI TIET TO KHAI-->
                              <div style="width: 100%; margin-top: 50px;margin-bottom:5px; padding: 0;">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr class="grid_header">
                                        <td align="left">CHI TIẾT CHỨNG TỪ 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <div id="pnlCTietTTinTKhai" style="height: 150px; min-height: 150px; width: 100%; overflow: auto;">
                                            </div>
                                            <div id="" style="width: 100%; vertical-align: middle;" align="right">
                                                <input id="btnAddRowDTL" type="button" value="Thêm dòng" class="ButtonCommand" onclick="jsAddDummyRowDTL_Grid(1);" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                              </div>
                              <div id="divCTietToKhai" style="width: 100%; margin: 0; padding: 0;">
                              </div>
                              <!--THONG TIN KHAC-->
                              <div id="divTTinKhac" style="width: 100%; margin: 0; padding: 0;">
                                    <table cellspacing="3" cellpadding="1" style="width: 100%;">
                                        <tr id="rowGhiChu" align="left">
                                            <td align="left" style="width: 99%" colspan="2" valign="middle">
                                                <span style="font-weight: bold;">Lý do chuyển trả:</span>
                                                <input type="text" id="txtGhiChu" style="width: 99%; border-color: black; font-weight: bold"
                                                    maxlength="100" class="inputflat" />
                                            </td>
                                        </tr>
                                        <tr id="rowLyDoHuy" align="left">
                                            <td align="left" style="width: 99%" colspan="2" valign="middle">
                                                <span style="font-weight: bold;">Lý do hủy:</span>
                                                <input type="text" id="txtLyDoHuy" style="width: 99%; border-color: black; font-weight: bold"
                                                    maxlength="100" class="inputflat" />
                                            </td>
                                        </tr>
                                    </table>
                                    <input id="hdfSo_CTu" type="hidden" />
                                    <input id="hdfLoaiCTu" type="hidden" value="T" />
                                    <input id="hdfHQResult" type="hidden" />
                                    <input id="hdfHDRSelected" type="hidden" />
                                    <input id="hdfIsEdit" type="hidden" />
                                    <input id="hdfDescCTu" type="hidden" />
                                    <input id="hdfNgayKB" type="hidden" />
                                    <input id="hdfMST" type="hidden" />
                                    <input id="hdfTrang_Thai" type="hidden" />
                                    <input id="hdfSoCTNH" type="hidden" />
                                    <input type="hidden" id="cifNo" />
                                    <input type="hidden" id="branchCode" />
                                    <input type="hidden" id="hdnTongTienCu" value="0" />

                                    <input type="hidden" id="hdLimitAmout" value="0" />
                                    <input type="hidden" id="hndNHSHB" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width='100%' align="center" valign="top" class='text' colspan='2' align="right">

                                <input type="button" id="cmdChuyenKS" class="ButtonCommand" value="[(C)huyển kiểm soát]" style=""
                                    accesskey="C" onclick="jsGhi_CTU2('GHIKS');" visible="true"/>
                                &nbsp;
                            <input type="button" id="cmdHuyCT" class="ButtonCommand" value="[(H)ủy CT]" onclick="jsHuy_CTU();"
                                accesskey="H" />
                                &nbsp;

                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp; </td>
                        </tr>
                    </table>
</asp:Content>
