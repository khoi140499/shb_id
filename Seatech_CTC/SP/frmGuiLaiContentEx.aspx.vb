﻿Imports Business
Imports VBOracleLib
Imports System.Data
Imports System.Data.OracleClient
Imports System.Xml.Serialization
Imports Business.Common.mdlCommon
Imports CorebankServiceESB

Partial Class SP_frmGuiLaiContentEx
    Inherits System.Web.UI.Page


    Dim clsSP As CorebankServiceESB.clsSongPhuong = New CorebankServiceESB.clsSongPhuong
    Dim cls_corebank As New CorebankServiceESB.clsCoreBank()
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Search()
    End Sub

    Protected Function Search() As DataTable
        Dim strSql As String = ""
        Dim strWhere As String = ""
        Dim strOrder As String = ""
        If txtTuNgay.Value.Trim() = "" And txtDenNgay.Value.Trim() <> "" Then
            strWhere += " and ngay_kh_nh <= to_date('" & txtDenNgay.Value.Trim & " 23:59:59','dd/MM/yyyy HH24:MI:SS') "
        ElseIf txtTuNgay.Value.Trim() <> "" And txtDenNgay.Value.Trim() <> "" Then
            strWhere += " and ngay_kh_nh >= to_date('" & txtTuNgay.Value.Trim & " 00:00:00','dd/MM/yyyy HH24:MI:SS') and ngay_kh_nh <= to_date('" & txtDenNgay.Value.Trim() & " 23:59:59','dd/MM/yyyy HH24:MI:SS')"
        ElseIf txtTuNgay.Value.Trim() <> "" And txtDenNgay.Value.Trim() = "" Then
            strWhere += " and ngay_kh_nh >= to_date('" & txtTuNgay.Value.Trim() & " 00:00:00','dd/MM/yyyy HH24:MI:SS') "
        End If
        If txtSoCT.Text.Trim() <> "" Then
            strWhere += " and so_ct = '" & txtSoCT.Text.Trim() & "'"
        End If
        If txtMST.Text.Trim() <> "" Then
            strWhere += " and ma_nnthue = '" & txtMST.Text.Trim() & "'"
        End If
        If txtSo_CT_NH.Text.Trim() <> "" Then
            strWhere += " and so_ct_nh = '" & txtSo_CT_NH.Text.Trim() & "'"
        End If
        If ddlMaLoaiThue.SelectedValue = "01" Then
            strWhere += " and ma_lthue <> '04'"
        ElseIf ddlMaLoaiThue.SelectedValue = "02" Then
            strWhere += " and ma_lthue = '04'"
        End If
        strOrder = " order by so_ct desc"
        strSql &= "select * from ("
        If ddlMaLoaiThue.SelectedValue = "00" Then
            strSql &= "select so_ct,so_ct_nh,ma_nnthue,ten_nnthue,ttien,ngay_kh_nh, 'Gui content_ex loi' trang_thai, DECODE(ma_lthue,'04', 'TQHQ','TQND') lthue, 'tcs_ctu_hdr' table_update,ma_lthue,so_ct as id from tcs_ctu_hdr where trang_thai ='01' and SEND_CONTENT_EX = '0' and  tt_sp = '0' "
            strSql &= "UNION ALL "
            strSql &= "select so_ctu as so_ct,so_ct_nh,mst_nnop ma_nnthue,ten_nnop ten_nnthue,tong_tien as ttien,ngay_nhang as ngay_kh_nh,'Gui content_ex loi' trang_thai, 'NTDT' lthue,'tcs_ctu_ntdt_hdr' table_update,'' ma_lthue,so_ctu as id from tcs_ctu_ntdt_hdr where tthai_ctu ='11' and SEND_CONTENT_EX = '0' and  tt_sp = '0' "
            strSql &= "UNION ALL "
            strSql &= "select a.so_ctu so_ct, a.so_ct_nh,a.ma_dv ma_nnthue,a.ten_dv ten_nnthue,a.hq_sotien_to ttien,a.ngaynhanmsg ngay_kh_nh, 'Gui content_ex loi' trang_thai,'HQDT' lthue,'tcs_hq247_304' table_update,'' ma_lthue,id from tcs_hq247_304 a  where a.trangthai in('02','03','04', '09','10') and a.SEND_CONTENT_EX = '0' and  a.tt_sp = '0' "
            strSql &= "UNION ALL "
            strSql &= "select a.so_ctu so_ct, a.so_ct_nh,a.ma_dv ma_nnthue,a.ten_dv ten_nnthue,a.hq_sotien_to ttien,a.ngaynhanmsg ngay_kh_nh, 'Gui content_ex loi' trang_thai, 'HQNT' lthue,'TCS_HQ247_201' table_update, '' ma_lthue,id from tcs_hq247_201 a  where a.trangthai in('02','03','04', '09','10') and a.SEND_CONTENT_EX = '0' and  a.tt_sp = '0' "
            strSql &= "UNION ALL "
            strSql &= "SELECT so_ct,so_ct_nh, ma_nnthue , ten_nnthue ,ttien , ngay_kh_nh ,'Gui content_ex loi' trang_thai,'BIENLAI' lthue, 'tcs_bienlai_hdr' table_update,'' ma_lthue, so_ct AS id from tcs_bienlai_hdr where trang_thai ='03' and SEND_CONTENT_EX = '0' and  tt_sp = '0' "
        ElseIf ddlMaLoaiThue.SelectedValue = "01" Or ddlMaLoaiThue.SelectedValue = "02" Then
            strSql &= "select so_ct,so_ct_nh,ma_nnthue,ten_nnthue,ttien,ngay_kh_nh, 'Gui content_ex loi' trang_thai, DECODE(ma_lthue,'04', 'TQHQ','TQND') lthue, 'tcs_ctu_hdr' table_update,ma_lthue,so_ct as id from tcs_ctu_hdr where trang_thai ='01' and SEND_CONTENT_EX = '0' and  tt_sp = '0' "
        ElseIf ddlMaLoaiThue.SelectedValue = "03" Then
            strSql &= "select so_ctu as so_ct,so_ct_nh,mst_nnop ma_nnthue,ten_nnop ten_nnthue,tong_tien as ttien,ngay_nhang as ngay_kh_nh,'Gui content_ex loi' trang_thai, 'NTDT' lthue,'tcs_ctu_ntdt_hdr' table_update,'' ma_lthue,so_ctu as id from tcs_ctu_ntdt_hdr where tthai_ctu ='11' and SEND_CONTENT_EX = '0' and  tt_sp = '0' "
        ElseIf ddlMaLoaiThue.SelectedValue = "04" Then
            strSql &= "select a.so_ctu so_ct, a.so_ct_nh,a.ma_dv ma_nnthue,a.ten_dv ten_nnthue,a.hq_sotien_to ttien,a.ngaynhanmsg ngay_kh_nh, 'Gui content_ex loi' trang_thai,'HQDT' lthue,'tcs_hq247_304' table_update,'' ma_lthue,id from tcs_hq247_304 a  where a.trangthai in('02','03','04', '09','10') and a.SEND_CONTENT_EX = '0' and  a.tt_sp = '0' "
        ElseIf ddlMaLoaiThue.SelectedValue = "05" Then
            strSql &= "select a.so_ctu so_ct, a.so_ct_nh,a.ma_dv ma_nnthue,a.ten_dv ten_nnthue,a.hq_sotien_to ttien,a.ngaynhanmsg ngay_kh_nh, 'Gui content_ex loi' trang_thai, 'HQNT' lthue,'TCS_HQ247_201' table_update, '' ma_lthue,id from tcs_hq247_201 a  where a.trangthai in('02','03','04', '09','10') and a.SEND_CONTENT_EX = '0' and  a.tt_sp = '0' "
        ElseIf ddlMaLoaiThue.SelectedValue = "06" Then
            strSql &= "SELECT so_ct,so_ct_nh, ma_nnthue , ten_nnthue ,ttien , ngay_kh_nh ,'Gui content_ex loi' trang_thai,'BIENLAI' lthue, 'tcs_bienlai_hdr' table_update,'' ma_lthue, so_ct AS id from tcs_bienlai_hdr a  where a.trang_thai ='03' and a.SEND_CONTENT_EX = '0' and  a.tt_sp = '0' "
        End If
        strSql &= ") where 1 = 1 " & strWhere & strOrder
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        'dt = DatabaseHelp.ExecuteToTable(strSql)
        If dt.Rows.Count > 0 Then
            dtgdata.DataSource = dt
            dtgdata.DataBind()
        Else
            dtgdata.DataSource = Nothing
            dtgdata.DataBind()
            clsCommon.ShowMessageBox(Me, "Không có dữ liệu.")
        End If
        Return dt
    End Function

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim succsessCount As Integer
        Dim selectedCount As Integer
        For i As Integer = 0 To dtgdata.Items.Count - 1
            Dim chkSelect As CheckBox = dtgdata.Items(i).FindControl("chkSelectItem")
            If chkSelect.Checked Then
                selectedCount += 1
                Dim strTYPE As String = dtgdata.Items(i).Cells(7).Text.Trim()
                Dim strTable As String = dtgdata.Items(i).Cells(8).Text.Trim()
                Dim strSo_ct As String = dtgdata.Items(i).Cells(9).Text.Trim()
                Dim strSo_CT_NH As String = dtgdata.Items(i).Cells(2).Text.Trim()
                If strTYPE = "TQND" Or strTYPE = "TQHQ" Then
                    strTYPE = "NTTQ"
                End If
                If ProcessSelectedMsg(strSo_ct, strTYPE, strTable, strSo_CT_NH) Then
                    succsessCount += 1
                End If
            End If
        Next
        If selectedCount = 0 Then
            clsCommon.ShowMessageBox(Me, "Bạn chưa chọn chứng từ nào để xử lý")
            Return
        Else
            If succsessCount > 0 Then
                clsCommon.ShowMessageBox(Me, "Đã xử lý thành công " & succsessCount.ToString() & " chứng từ trên " & selectedCount.ToString() & " chứng từ đã chọn")
            Else
                clsCommon.ShowMessageBox(Me, "Xử lý chứng từ thất bại. Vui lòng thử lại sau")
            End If
        End If
        Search()
    End Sub

    Protected Function ProcessSelectedMsg(ByVal strSo_ct As String, ByVal strTYPE As String, ByVal strTable As String, ByVal strSO_CT_NH As String) As Boolean
        Dim result As Boolean = True
        Dim strMess_Content As String = ""
        Dim processStatus As Boolean = True
        Dim v_strErrorCode As String = ""
        Dim strMA_NV As String = Session.Item("User").ToString()
        Try
            Dim create_cex As New Business.CitadV25

            Dim core_date As String = cls_corebank.Get_VALUE_DATE()

            Dim content_ex As String = create_cex.CreateContentEx(strSo_ct, strTYPE, core_date)

            content_ex = Business.Common.mdlCommon.EncodeBase64_ContentEx(content_ex)

            cls_corebank.SendContentEx(strSo_ct, strSO_CT_NH, strTYPE, content_ex, v_strErrorCode)

            If v_strErrorCode = "00000" Then
                'cap nhap send core thanh cong: 1 la thanh cong, 0 la chua thanh cong
                If strTYPE.Equals("NTTQ") Then
                    DataAccess.ExecuteNonQuery("update TCS_CTU_HDR set SEND_CONTENT_EX = '1' where so_ct='" & strSo_ct & "'", CommandType.Text)
                ElseIf strTYPE.Equals("NTDT") Then
                    DataAccess.ExecuteNonQuery("update TCS_CTU_NTDT_HDR set SEND_CONTENT_EX = '1' where so_ctu='" & strSo_ct & "'", CommandType.Text)
                ElseIf strTYPE.Equals("HQDT") Then
                    DataAccess.ExecuteNonQuery("update tcs_hq247_304 set SEND_CONTENT_EX = '1' where so_ctu='" & strSo_ct & "'", CommandType.Text)
                ElseIf strTYPE.Equals("HQNT") Then
                    DataAccess.ExecuteNonQuery("update TCS_HQ247_201 set SEND_CONTENT_EX = '1' where so_ctu='" & strSo_ct & "'", CommandType.Text)
                ElseIf strTYPE.Equals("BIENLAI") Then
                    DataAccess.ExecuteNonQuery("update tcs_bienlai_hdr set SEND_CONTENT_EX = '1' where so_ct='" & strSo_ct & "'", CommandType.Text)
                End If

                '''''''''''''''''''''''''
                'Dim strSql As String = ""
                'If processStatus Then
                '    If strTYPE.Equals("NTTQ") Or strTYPE.Equals("BIENLAI") Then
                '        strSql = "update " & strTable & " set SEND_CONTENT_EX = '1',NHANVIEN_VL='" & strMA_NV & "',TTHAI_XL_LAI='SUCCESS',NGAY_XL_LAI=SYSDATE where so_ct='" & strSo_ct & "'"
                '    Else
                '        strSql = "update " & strTable & " set SEND_CONTENT_EX = '1',NHANVIEN_VL='" & strMA_NV & "',TTHAI_XL_LAI='SUCCESS',NGAY_XL_LAI=SYSDATE where so_ctu='" & strSo_ct & "'"
                '    End If
                'End If
                'DataAccess.ExecuteNonQuery(strSql, CommandType.Text)

            End If
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("CO LOI KHI GUI CONTENT_EX SANG COREBANK - " & ex.Message)
            result = False
            processStatus = False
        End Try
       
        Return result
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Title = "Xử lý gửi lại ContentEx"

        If Not IsPostBack Then
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub

    Protected Sub dtgdata_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgdata.ItemCommand
        Dim commandName = e.CommandName.ToString()
        Dim strID_Gdich = e.CommandArgument.ToString()

    End Sub

    Protected Sub dtgdata_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgdata.PageIndexChanged
        Try
            dtgdata.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
        End Try
        Search()
    End Sub
End Class
