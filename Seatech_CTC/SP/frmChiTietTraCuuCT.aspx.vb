﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.HQ247.NNTDT
Imports Business.Common.mdlCommon
Imports Business.BaoCao
Imports Business.Common
Imports System.IO
Imports System.Xml
Imports CustomsV3.MSG.MSG304
Imports System.Collections.Generic
Imports System.Diagnostics
Imports Business_HQ
Imports HQ247XulyMSG

Partial Class SP_frmChiTietTraCuuCT
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim v_strRootPath As String = ConfigurationManager.AppSettings("APP_ROOT_PATH").ToString
        Dim iResult As Integer = 0
        Dim strSql As String = " SELECT COUNT (1) KIEMTRA   FROM ("
        strSql &= "  SELECT A.MA_CN           FROM TCS_DM_CHUCNANG A , TCS_PHANQUYEN B , TCS_DM_NHANVIEN C , TCS_SITE_MAP D         "
        strSql &= "   WHERE UPPER(C.TEN_DN)=UPPER('" & Session.Item("UserName").ToString() & "') AND D.STATUS = 1  AND D.NAVIGATEURL = '" + HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") + "' "
        strSql &= "  AND (A.MA_CN=D.NODEID OR (A.MA_CN=D.PARENTNODEID ) )"
        strSql &= "  AND A.MA_CN=B.MA_CN AND B.MA_NHOM =C.MA_NHOM ) "

        If HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") <> "~/pages/frmHomeNV.aspx" Then
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
            If Not Globals.IsNullOrEmpty(dt) Then
                iResult = CInt(dt.Rows(0)(0).ToString())
            End If
        End If

        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                If Not Request.Params("ID") Is Nothing Then
                    Dim NNTDT_ID As String = Request.Params("ID").ToString
                    Dim strErrMsg As String = ""
                    If NNTDT_ID <> "" Then
                        LoadCTChiTietAll(NNTDT_ID)
                    End If
                End If
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Lỗi: " & ex.Message)
            End Try
        End If
     
    End Sub
    Private Sub LoadCTChiTietAll(ByVal sSOCT As String)
        Try
            Dim v_strTT As String = String.Empty
            Dim v_strTTSP As String = String.Empty
            Dim v_strLoaiThue As String = String.Empty
            Dim v_strPhuongThucTT As String = String.Empty
            Dim v_str_Ma_NNT As String = String.Empty
            Dim v_strSO_CT As String = sSOCT
            'Load chứng từ header
            Dim dsCT As New DataSet
            dsCT = Business.ChungTuSP.buChungTu.CTU_Header_Set(sSOCT)
            If (dsCT.Tables(0).Rows.Count > 0) Then
                'ShowHideCTRow(True)
                FillDataToCTHeader(dsCT)
                v_strTT = dsCT.Tables(0).Rows(0)("TRANG_THAI").ToString
                v_strLoaiThue = dsCT.Tables(0).Rows(0)("MA_LTHUE").ToString
                v_str_Ma_NNT = dsCT.Tables(0).Rows(0)("Ma_NNThue").ToString
                v_strTTSP = dsCT.Tables(0).Rows(0)("TT_SP").ToString
                v_strSO_CT = dsCT.Tables(0).Rows(0)("so_ct").ToString
            Else
                'lblStatus.Text = "Không tìm thấy chứng từ cần tìm.Hãy thử lại"
                Exit Sub
            End If

            'Load thông tin chi tiết
            Dim dsCTChiTiet As New DataSet
            dsCTChiTiet = Business.ChungTuSP.buChungTu.CTU_ChiTiet_Set_CT_TK(v_strSO_CT)
            If dsCTChiTiet.Tables(0).Rows.Count > 0 Then
                FillDataToCTDetail(dsCTChiTiet, v_str_Ma_NNT)
                ' CaculateTongTien(dsCTChiTiet.Tables(0))
            Else
                'lblStatus.Text = "Không lấy được thông tin chi tiết của chứng từ.Hãy kiểm tra lại"
                Exit Sub
            End If

        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

            'Throw ex
        Finally
            'Catch ex As Exception
            '    logger.Error(ex.StackTrace)
        End Try
    End Sub
    Private Sub FillDataToCTHeader(ByVal dsCT As DataSet)
        Try
            Dim strTenVT = String.Empty
            Dim strTenLH As String = String.Empty
            Dim strMaLH As String = String.Empty
            Dim dr As DataRow = dsCT.Tables(0).Rows(0)
            Dim v_strTrangThai As String = dr("TRANG_THAI").ToString()
            Dim v_strHTTT As String = GetString(dr("PT_TT")).ToString
            txtMaNNT.Text = dr("MA_NNTHUE").ToString()
            txtTenNNT.Text = dr("TEN_NNTHUE").ToString()
            txtDChiNNT.Text = dr("DC_NNTHUE").ToString()
            txtMaNNTien.Text = dr("Ma_NNTien").ToString()
            txtTenNNTien.Text = dr("TEN_NNTIEN").ToString()
            txtDChiNNTien.Text = dr("DC_NNTIEN").ToString()
            txtNgayNNT.Text = dr("ngay_ntien").ToString()
            txtSHKB.Text = dr("SHKB").ToString()
            txtTenKB.Text = dr("TEN_KB").ToString()

            txtNgayKB.Text = ConvertNumbertoString(dr("NGAY_KB").ToString())

            txtMaDThu.Text = dr("MA_DTHU").ToString()

            'txtSo_BT.Text = dr("SO_BT").ToString()

            txtNgayChungTu.Text = dr("NGAY_CTU").ToString()
            txtTKCo.Text = dr("TK_Co").ToString()
            txtTKNo.Text = dr("TK_No").ToString()
            ddlMaLThue.SelectedValue = dr("MA_LTHUE").ToString()
            txtCQQD.Text = dr("CQ_QD").ToString()
            txtMaCQThu.Text = dr("MA_CQTHU").ToString()
            txtTenCQThu.Text = dr("TEN_CQTHU").ToString()
            txtMaDBHC.Text = dr("MA_DBHC").ToString()
            txtTenDBHC.Text = dr("TEN_DBHC").ToString()
            txtTK_KH_NH.Text = GetString(dr("TK_KH_NH"))
            txtNgayKH_NH.Text = GetString(dr("NGAY_KH_NH"))
            txtSTK_KH_Nhan.Text = GetString(dr("TK_KH_NHAN"))
            txtTenKH_Nhan.Text = GetString(dr("TENKH_NHAN"))
            txtSTK_KH_Nhan.Text = GetString(dr("TK_KH_NHAN"))
            txtDiaChiKH_Nhan.Text = GetString(dr("DIACHI_KH_NHAN"))

            txtMaNH_TK_NNT.Text = GetString(dr("NH_GIU_TKNNT"))
            txtMaNH_TK_KBNN.Text = GetString(dr("NH_GIU_TKKB"))

            ddlMaNT.SelectedValue = dr("MA_NT").ToString()
            txtTyGia.Text = VBOracleLib.Globals.Format_Number(dr("Ty_Gia").ToString(), ",")
            txtTTienVND.Text = dr("TTIEN").ToString()
            txtTTienNT.Text = VBOracleLib.Globals.Format_Number(GetString(dr("ttien_nt")).ToString(), ",")
            'Event: Them ma hai quan
            txtMaHQ.Text = dr("MA_HQ").ToString()
            txtTenMaHQ.Text = dr("TEN_HQ").ToString()
            txtMaHQPH.Text = dr("MA_HQ_PH").ToString()
            txtTenMaHQPH.Text = dr("TEN_HQ_PH").ToString()

            'txtTimeLap.Text = GetString(dr("ngay_ht"))

            txtDienGiai.Text = GetString(dr("REMARK"))
            txtContentEx.Text = GetString(dr("CONTENT_EX")).Replace("<", "[").Replace(">", "]")

            txtNgayCitad.Text = GetString(dr("ngay_citad"))
            txtSoBTCitad.Text = GetString(dr("sobt_citad"))
            txtTen_Nguoi_chuyen.Text = GetString(dr("TEN_NGUOI_CHUYEN"))

            lblSoCTU.Text = GetString(dr("SO_CT"))

            'txtRM_REF_NO.Text = dr("SO_CT_NH").ToString()
            'txtREF_NO.Text = dr("SO_CT_NH").ToString()
            'txtSoCT_NH.Text = dr("SO_CT_NH").ToString()

            If dr("LY_DO_HUY").ToString().Trim <> "" Then
                txtLydo_ChuyenTra.Text = dr("LY_DO_HUY").ToString()
            Else
                txtLydo_ChuyenTra.Text = dr("LY_DO").ToString()
            End If
            'hdfTrangThai_CT.Value = dr("trang_thai").ToString().Trim
            'hdfTTSP.Value = dr("tt_SP").ToString().Trim
            'hdfDienGiai.Value = dr("ghi_chu").ToString().Trim
            'hdfMACN.Value = dr("ma_cn").ToString().Trim

            Dim v_dt As DataTable = Nothing

        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình FillDataToCTHeader")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

            Throw ex
        Finally
        End Try
    End Sub

    Private Sub FillDataToCTDetail(ByVal pv_dsCT As DataSet, ByVal v_strMa_NNT As String)
        Try
            Dim v_dt As DataTable = pv_dsCT.Tables(0)
            dtgrd.DataSource = v_dt
            dtgrd.DataBind()

        Catch ex As Exception

        End Try
    End Sub
    Public Function convertDateHQ(ByVal strDate As String) As String

        Dim str As String = ""
        Try
            str = strDate.Substring(8, 2) & "/" & strDate.Substring(5, 2) & "/" & strDate.Substring(0, 4)
            If strDate.Trim.Length > 10 Then
                str &= strDate.Substring(10, strDate.Length - 10)
            End If
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

        End Try

        Return str
    End Function
End Class
