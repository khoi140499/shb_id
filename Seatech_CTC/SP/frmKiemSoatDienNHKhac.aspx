﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage07.master" AutoEventWireup="false"
    CodeFile="frmKiemSoatDienNHKhac.aspx.vb" Inherits="SP_frmKiemSoatDienNHKhac" 
    Title="Kiểm soát điện từ ngân hàng khác" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../javascript/jquery/jquery-1.3.2.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            //alert("xxx");
            var strTT = $get('<%=hdfTrangThai_CT.ClientID%>').value;
            //alert("xxx" + strTT);
            if (strTT.length > 0) {
                if (strTT == "01" || strTT == "03" || strTT == "04" || strTT == "05" || strTT == "06") {
                    document.getElementById('cmdKS').setAttribute("disabled", "disabled");
                } else if (strTT == "02") {
                    document.getElementById('cmdKS').removeAttribute("disabled");
                }

            } else {
                document.getElementById('cmdKS').setAttribute("disabled", "disabled");
            }

        });



        function jsShowCustomerSignature() {
            if ($get('<%=txtTK_KH_NH.clientID%>').value != '') {
                window.showModalDialog("../../Find_DM/ShowSignature.aspx?account=" + $get('<%=txtTK_KH_NH.clientID%>').value, "", "dialogWidth:700px;dialogHeight:500px;help:0;status:0;", "_new");
            }
            else {
                alert('Phải truy vấn được thông tin khách hàng trước khi xem chữ ký');
            }
        }
        function DG_changeBackColor(row, highlight) {
            if (highlight) {
                row.style.cursor = "hand";
                lastColor = row.style.backgroundColor;
                row.style.backgroundColor = '#C6D6FF';
            }
            else
                row.style.backgroundColor = lastColor;
        };


        function jsFormatNumber(txtCtl) {
            document.getElementById(txtCtl).value = localeNumberFormat(document.getElementById(txtCtl).value.replaceAll('.', ''), '.');
        }


        function localeNumberFormat(amount, delimiter) {
            try {

                amount = parseFloat(amount);
            } catch (e) {
                throw ('localeNumberFormat caused INVALID FLOAT with value ' + amount)
                return null;
            }
            if (delimiter == null || delimiter == undefined) { delimiter = '.'; }

            // convert to string
            if (amount.match != 'function') { amount = amount.toString(); }

            // validate as numeric
            var regIsNumeric = /[^\d,\.-]+/igm;
            var results = amount.match(regIsNumeric);

            if (results != null) {
                outputText('INVALID NUMBER', eOutput)
                return null;
            }
        }
        function disableButton() {
            document.getElementById('cmdKS').setAttribute("disabled", "disabled");
            document.getElementById('<%=cmdChuyenTra.clientID%>').disabled = true;
            document.getElementById('<%=cmdChuyenThue.clientID%>').disabled = true;
            //                document.getElementById('<%=cmdHuy.clientID%>').disabled=true;
        }
        function XacNhan() {
            var ldhuy;
            ldhuy = $get('<%=txtLyDoHuy.clientID%>').value.trim();
            if (ldhuy == '') {
                alert('Bạn phải nhập lý do hủy.');
                document.getElementById($get('<%=txtLyDoHuy.clientID%>')).focus();
                return;
            } else {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                if (confirm("Bạn có thực sự muốn Duyệt hủy cho chứng từ này?")) {
                    confirm_value.value = "Yes";
                } else {
                    confirm_value.value = "No";
                }
                document.forms[0].appendChild(confirm_value);
            }
        }

        function jsValidateKS() {
            var strLanKS = $get('<%=hdfClickKS.ClientID%>').value.trim();
            var strSo_CT = $get('<%=hdfSo_CT.ClientID%>').value.trim();
            PageMethods.ValidNHSHB_SP(strLanKS, strSo_CT, ValidateKS_Complete, ValidateKS_Error);
        }

        function ValidateKS_Complete(result, methodName) {
            if (result.length > 0) {
                var validLanKS = result.toString();
                if (parseInt(validLanKS) > 0) {
                    alert('Chứng từ đã từng kiểm soát. Bạn cần check hạch toán CORE');
                    document.getElementById('cmdKS').setAttribute("disabled", "disabled");
                } else {
                    document.getElementById("<%= cmdKSBK.ClientID%>").click();
                }
     
            }
        }


        function ValidateKS_Error(error, userContext, methodName) {

        }

      
    </script>

    <style type="text/css">
        .tblHeader tr td, .tblNganHang tr td {
            text-align: left;
        }

        .auto-style1 {
            width: 13%;
        }

        .auto-style2 {
            width: 15%;
        }

        .hiddenClass
        {
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <%--<asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>--%>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">KIỂM SOÁT CHỨNG TỪ</asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr height="8px">
                        <td></td>
                    </tr>
                    <tr>
                        <td width='100%' align="center" valign="top" style="padding-left: 3; padding-right: 3">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width='900' align="left" valign="top" class="frame">
                                        <asp:HiddenField ID="hdfMaNNT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfSHKB" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfNgay_KB" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfSo_CT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfKyHieu_CT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfSo_BT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfMa_Dthu" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfMa_NV" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfTrangThai_CT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfTrangThai_CThue" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfLoai_CT" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfDienGiai" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfSoCTNH" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfMACN" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfTTSP" runat="server" Value="" />
                                        <asp:HiddenField ID="hdfClickKS" runat="server" Value="" />
                                        <table width="100%" align="center">
                                            <tr>
                                                <td valign="top" width="210px" height="300px" align="left">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr align="left">
                                                            <td>
                                                                <asp:GridView ID="grdDSCT" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                                                    ShowHeader="True" ShowFooter="True" BorderColor="#000" CssClass="grid_data" PageSize="15"
                                                                    Width="100%">
                                                                    <PagerStyle CssClass="cssPager" />
                                                                    <AlternatingRowStyle CssClass="grid_item_alter" />
                                                                    <HeaderStyle BackColor="#5DC8D2" CssClass="grid_header" HorizontalAlign="Left"></HeaderStyle>
                                                                    <RowStyle CssClass="grid_item" />
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Số CT">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <FooterStyle HorizontalAlign="Left"></FooterStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnSelectCT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "so_ct") %>'
                                                                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SHKB") & "," & DataBinder.Eval(Container.DataItem, "NGAY_KB") & "," & DataBinder.Eval(Container.DataItem, "SO_CT") & "," & DataBinder.Eval(Container.DataItem, "KYHIEU_CT") & "," & DataBinder.Eval(Container.DataItem, "SO_BT") & "," & DataBinder.Eval(Container.DataItem, "MA_DTHU") & "," & DataBinder.Eval(Container.DataItem, "MA_NV") & "," & Eval("TRANG_THAI").ToString() & "," & Eval("MA_NNTHUE").ToString() & "," & Eval("SO_CT_NH").ToString() & "," & Eval("LAN_KS").ToString()%>'
                                                                                    OnClick="btnSelectCT_Click" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Số GD Corebank" FooterText="Tổng số CT:">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <FooterStyle HorizontalAlign="Left"></FooterStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbSo_CT_NH" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "so_ct_nh") %>' />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Trạng thái">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <FooterStyle Font-Bold="true" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblMaNV" runat="server" Text='<%#Business.ChungTuSP.buChungTu.Get_TrangThai_Citad(Eval("TRANG_THAI").ToString(), Eval("TT_SP").ToString())%>' />
                                                                                <%--  <asp:Label ID="lblMaNV" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TRANG_THAI")%>' />
                                                                                  <asp:Label ID="lblDelimiter" runat="server" Text='/' />
                                                                                <asp:Label ID="lblTTSP" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TT_SP")%>' />--%>
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <%#grdDSCT.Rows.Count.ToString%>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                        <%--   <asp:TemplateField HeaderText="Tên ĐN/Số BT">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <FooterStyle Font-Bold="true" />
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblMaNV" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TEN_DN") %>' />
                                                                                <asp:Label ID="lblDelimiter" runat="server" Text='/' />
                                                                                <asp:Label ID="lblSoBT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "SO_BT")%>' />
                                                                            </ItemTemplate>
                                                                            <FooterTemplate>
                                                                                <%#grdDSCT.Rows.Count.ToString%>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>--%>
                                                                    </Columns>
                                                                    <PagerStyle HorizontalAlign="Left" BackColor="#E4E5D7"></PagerStyle>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                <asp:Button ID="cmdRefresh" runat="server" Text="[Refresh]" AccessKey="K" CssClass="ButtonCommand"
                                                                    UseSubmitBehavior="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">Trạng thái chứng từ:
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%'>
                                                                <asp:DropDownList ID="ddlTrangThai" runat="server" CssClass="inputflat" Width="100%"
                                                                    AutoPostBack="true">
                                                                    <asp:ListItem Value="99" Text="Tất cả"></asp:ListItem>
                                                                    <asp:ListItem Value="02" Text="Chờ kiểm soát"></asp:ListItem>
                                                                    <asp:ListItem Value="030" Text="Hạch toán thành công gửi TTSP lỗi"></asp:ListItem>
                                                                    <asp:ListItem Value="031" Text="Thành công"></asp:ListItem>
                                                                    <asp:ListItem Value="04" Text="Chuyển trả"></asp:ListItem>
                                                                    <asp:ListItem Value="06" Text="Đã hủy"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">Giao dịch viên thực hiện:
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%'>
                                                                <asp:DropDownList ID="ddlMacker" runat="server" CssClass="inputflat" Width="100%"
                                                                    AutoPostBack="true">
                                                                    <asp:ListItem Value="99" Text="Tất cả"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td align="left" style="width: 100%" colspan="3">
                                                                <hr style="width: 50%" />
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td style="width: 100%" colspan="3">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr style="display: none">
                                                                        <td style="width: 10%">
                                                                            <img src="../images/icons/ChuaKS.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Chưa Kiểm soát
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../images/icons/ChuyenKS.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Chờ Duyệt
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../images/icons/DaKS.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Thành công
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../images/icons/KSLoi.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Chuyển trả
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../images/icons/HuyHS.gif" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Giao dịch hủy - chờ duyệt
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../images/icons/Huy_KS.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Đã Hủy
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../images/icons/TT_CThueLoi.gif" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Chuyển TTSP lỗi
                                                                        </td>
                                                                    </tr>

                                                                    <tr style="display: none">
                                                                        <td style="width: 10%">
                                                                            <img src="../images/icons/Huy_CT_Loi.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">Hủy chuyển thuế/HQ lỗi
                                                                        </td>
                                                                    </tr>

                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td align="left" style="width: 100%" colspan="3">
                                                                <hr style="width: 50%" />
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td style="width: 100%" colspan="3">Alt + K : Kiểm soát<br />
                                                                Alt + C : Chuyển trả
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td align="left" style="width: 100%" colspan="3">
                                                                <hr style="width: 50%" />
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td style="width: 100%" colspan="3">Đối với Firefox
                                                                <br />
                                                                Alt + Shift + K : Kiểm soát<br />
                                                                Alt + Shift + C : Chuyển trả
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top" width="680px">
                                                    <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                                        <tr>
                                                            <td colspan="4" width='100%' align="right">
                                                                <table width="100%">
                                                                    <tr class="grid_header">
                                                                        <td height="15px" align="left">
                                                                            <%--Trường <span style="color: #FF0000; font-weight: bold">(*)</span> là bắt buộc nhập--%>
                                                                            THÔNG TIN CHỨNG TỪ 
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <div id="Panel1" style="/*height: 380px; */ width: 100%; /*overflow-y: scroll; */">
                                                                    <table runat="server" id="grdHeader" cellspacing="0" cellpadding="1" rules="all"
                                                                        border="1" style="border-color: #989898; border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse; margin-left: 2px;"
                                                                        class="tblHeader">
                                                                        <tr id="rowTTCITAD">
                                                                            <td>
                                                                                <b>Ngày Citad nhận </b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td width="25%">
                                                                                <asp:TextBox runat="server" ID="txtNgayCitad" CssClass="inputflat3" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>Số bút toán trên Citad </b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td width="27%">
                                                                                <asp:TextBox runat="server" ID="txtSoBTCitad" CssClass="inputflat" ReadOnly="true" BackColor="Aqua" Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowMaNNT">
                                                                            <td>
                                                                                <b>Mã số thuế </b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td width="25%">
                                                                                <asp:TextBox runat="server" ID="txtMaNNT" CssClass="inputflat3" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>Tên NNT </b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td width="27%">
                                                                                <asp:TextBox runat="server" ID="txtTenNNT" CssClass="inputflat" ReadOnly="true" BackColor="Aqua" Width="98%" />
                                                                            </td>
                                                                        </tr>

                                                                        <tr id="rowDChiNNT">
                                                                            <td>
                                                                                <b>Địa chỉ người nộp thuế</b>
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:TextBox runat="server" ID="txtDChiNNT" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNNTien">
                                                                            <td>
                                                                                <b>Mã người nộp tiền</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMaNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="99%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>Tên người nộp tiền</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="99%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowDChiNNTien">
                                                                            <td>
                                                                                <b>Địa chỉ người nộp tiền</b>
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:TextBox runat="server" ID="txtDChiNNTien" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>

                                                                        </tr>
                                                                        <tr id="rowTT">
                                                                            <td>
                                                                                <b>Ngày NNT nộp tiền</b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtNgayNNT" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>Số Ref No SHB</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtRefNoSHB" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowSHKB">
                                                                            <td>
                                                                                <b>Số hiệu kho bạc </b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td width="25%">
                                                                                <asp:TextBox runat="server" ID="txtSHKB" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>Tên kho bạc </b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenKB" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowNgayKBDThu">
                                                                            <td>
                                                                                <b>Ngày kho bạc</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtNgayKB" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="99%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>Mã điểm thu</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMaDThu" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="99%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowBTNgayCT">
                                                                            <td>
                                                                                <b>Số bút toán</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtSoBT" CssClass="inputflat3" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>Ngày chứng từ</b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtNgayChungTu" runat="server" Width="98%" BackColor="Aqua" CssClass="inputflat">
                                                                                </asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTKNo">
                                                                            <td>
                                                                                <b>Tài khoản có</b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTKCo" CssClass="inputflat" ReadOnly="true" BackColor="Aqua" Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>Tài khoản nợ</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTKNo" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowMaLThue">
                                                                            <td>
                                                                                <b>Mã loại thuế</b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList runat="server" ID="ddlMaLThue" CssClass="inputflat" Enabled="false" BackColor="Aqua"
                                                                                    Width="98%">
                                                                                    <asp:ListItem Value="01" Text="Khoản thu do cơ quan thuế quản lý" Selected="True"></asp:ListItem>
                                                                                    <asp:ListItem Value="02" Text="Thu phí, lệ phí bộ ngành"></asp:ListItem>
                                                                                    <asp:ListItem Value="03" Text="Khoản thu do cơ quan khác quản lý"></asp:ListItem>
                                                                                    <asp:ListItem Value="04" Text="Khoản thu do cơ quan hải quan quản lý"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <b>Cơ quan ra quyết định</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtCQQD" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowCQThu">
                                                                            <td>
                                                                                <b>Mã cơ quan thu</b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMaCQThu" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>Tên cơ quan thu</b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenCQThu" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowDBHC" style="">
                                                                            <td>
                                                                                <b>Mã ĐBHC</b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td style="">
                                                                                <asp:TextBox runat="server" ID="txtMaDBHC" CssClass="inputflat" ReadOnly="true" BackColor="Aqua" Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>Tên ĐBHC</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenDBHC" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTK_KH_NH">
                                                                            <td>
                                                                                <b>Số tài khoản KH</b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTK_KH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>Ngày khách hàng NH</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtNgayKH_NH" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowKHNhan">
                                                                            <td>
                                                                                <b>Tên khách hàng nhận</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenKH_Nhan" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>STK KH nhận</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtSTK_KH_Nhan" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowDiaChiKHNhan">
                                                                            <td>
                                                                                <b>Địa chỉ khách hàng nhận</b>
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:TextBox runat="server" ID="txtDiaChiKH_Nhan" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="100%" />
                                                                            </td>

                                                                        </tr>
                                                                        <tr id="rowMaNHGiuTK">
                                                                            <td>
                                                                                <b>Mã NH giữ TK NNT</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMaNH_TK_NNT" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>Mã NH giữ TK KBNN</b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMaNH_TK_KBNN" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowMaNT">
                                                                            <td>
                                                                                <b>Mã nguyên tệ</b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList runat="server" ID="ddlMaNT" CssClass="inputflat" Enabled="false" BackColor="Aqua"
                                                                                    Width="98%">
                                                                                    <asp:ListItem Value="VND" Text="VND" Selected="True"></asp:ListItem>
                                                                                    <asp:ListItem Value="USD" Text="USD"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <b>Tỷ giá</b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTyGia" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTTTien">
                                                                            <td>
                                                                                <b>Tổng tiền VND Citad/VCB</b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTTienVND" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>Tổng tiền NT</b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTTienNT" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowMaHQ">
                                                                            <td>
                                                                                <b>Mã hải quan</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMaHQ" CssClass="inputflat3" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>Tên hải quan</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenMaHQ" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="99%" />
                                                                            </td>
                                                                        </tr>

                                                                        <tr id="rowMaHQPH">
                                                                            <td>
                                                                                <b>Mã hải quan phát hành</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtMaHQPH" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>
                                                                            <td>
                                                                                <b>Tên hải quan phát hành</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox runat="server" ID="txtTenMaHQPH" CssClass="inputflat" ReadOnly="true"
                                                                                    Width="98%" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="rowTenNguoiChuyen">
                                                                            <td>
                                                                                <b>Tên Người chuyển</b>
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:TextBox runat="server" ID="txtTen_Nguoi_chuyen" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                                                            </td>

                                                                        </tr>
                                                                        <tr id="rowDienGiai">
                                                                            <td>
                                                                                <b>Diễn giải</b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:TextBox runat="server" ID="txtDienGiai" Style="width: 100%;" CssClass="inputflat"
                                                                                    MaxLength="4000" TextMode="MultiLine" Rows="3" Columns="1" ReadOnly="true"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                         <tr id="rowContentEx">
                                                                            <td>
                                                                                <b>Diễn giải(Citad2.5)</b>&nbsp;<span class="requiredField">(*)</span>
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:TextBox runat="server" ID="txtContentEx" Style="width: 100%;" CssClass="inputflat"
                                                                                    MaxLength="4000" TextMode="MultiLine" Rows="3" Columns="1" ReadOnly="true"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr style="visibility: hidden">
                                                            <td width="25%" style="height: 0px">
                                                                <asp:TextBox runat="server" ID="txtSo_BT" CssClass="inputflat" ReadOnly="true" Visible="false"
                                                                    Width="99%" />
                                                                <asp:TextBox runat="server" ID="txtSoCT" CssClass="inputflat" ReadOnly="true" Visible="false"
                                                                    Width="99%" />
                                                                <asp:TextBox runat="server" ID="txtSoCT_NH" CssClass="inputflat" ReadOnly="true"
                                                                    Visible="false" Width="99%" />
                                                            </td>
                                                            <td width="51%" style="height: 0px">
                                                                <asp:TextBox runat="server" ID="txtTRANG_THAI" CssClass="inputflat" ReadOnly="true"
                                                                    Visible="false" Width="99%" />
                                                                <asp:TextBox runat="server" ID="txtMA_NV" CssClass="inputflat" ReadOnly="true" Visible="false"
                                                                    Width="99%" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan='2' align="left" class="errorMessage">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td width="100%" align="right">
                                                                            <asp:Label ID="lbSoFT" runat="server" Style="font-weight: bold;" Visible="false"></asp:Label>
                                                                            / 
                                                                            <asp:Label ID="lbSoCT" runat="server" Style="font-weight: bold;" Visible="false"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left" width='99%' colspan="2">
                                                                            <asp:UpdateProgress ID="updProgress" runat="server">
                                                                                <ProgressTemplate>
                                                                                    <div style="background-color: Aqua;">
                                                                                        <b style="font-size: 15pt">Đang lấy dữ liệu tại server.Xin chờ một lát...</b>
                                                                                    </div>
                                                                                </ProgressTemplate>
                                                                            </asp:UpdateProgress>
                                                                            <b>
                                                                                <asp:Literal ID="lblStatus" runat="server"></asp:Literal></b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="grid_header">
                                                                        <td height="15px" align="left">CHI TIẾT CHỨNG TỪ 
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" width="100%">
                                                                            <asp:DataGrid ID="grdChiTiet" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                                ShowHeader="true" Width="100%" CssClass="grid_data">
                                                                                <HeaderStyle CssClass="grid_header"></HeaderStyle>
                                                                                <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                                                                                <ItemStyle CssClass="grid_item" />
                                                                                <Columns>
                                                                                    <asp:TemplateColumn Visible="false">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkRemove" runat="server" Enabled="false" Checked="true" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Số TK" ItemStyle-HorizontalAlign="Center">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="SO_TK" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "SO_TK") %>'
                                                                                                Font-Bold="true"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Năm đăng ký" ItemStyle-HorizontalAlign="Center">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="NGAY_TK" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "NGAY_TK") %>'
                                                                                                Font-Bold="true"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Sắc thuế" ItemStyle-HorizontalAlign="Center">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="25px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="SAC_THUE" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "SAC_THUE") %>'
                                                                                                Font-Bold="true"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Mã chương" ItemStyle-HorizontalAlign="Center">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="MA_CHUONG" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_CHUONG") %>'
                                                                                                Font-Bold="true" MaxLength="3" ToolTip="Ấn phím Enter để chọn mã chương"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Mã NDKT" ItemStyle-HorizontalAlign="Center">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="MA_MUC" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_TMUC") %>'
                                                                                                Font-Bold="true" MaxLength="4" ToolTip="Ấn phím Enter để chọn mã tiểu mục" AutoPostBack="true"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Nội dung" ItemStyle-HorizontalAlign="Left">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="150px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="NOI_DUNG" runat="server" Width="97%" BorderColor="white" CssClass="inputflat"
                                                                                                ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "NOI_DUNG") %>'
                                                                                                MaxLength="200" Font-Bold="true"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Kỳ thuế" ItemStyle-HorizontalAlign="Right">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="KY_THUE" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: right" ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "KY_THUE") %>'
                                                                                                Font-Bold="true"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Số tiền VND" ItemStyle-HorizontalAlign="Right">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="right" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="SOTIEN" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: right" ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "SOTIEN")%>'
                                                                                                AutoPostBack="true" onfocus="this.select()" Font-Bold="true">
                                                                                            </asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderText="Số tiền NT" ItemStyle-HorizontalAlign="Right">
                                                                                        <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                                                                        <ItemStyle HorizontalAlign="right" VerticalAlign="Middle"></ItemStyle>
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="SOTIEN_NT" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                                                                Style="text-align: right" ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "SOTIEN_NT") %>'
                                                                                                AutoPostBack="true" onfocus="this.select()" Font-Bold="true">
                                                                                            </asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan='2' height='5'>
                <asp:TextBox runat="server" ID="txtRM_REF_NO" CssClass="inputflat" Visible="false"
                    ReadOnly="true" Text="" />
                <asp:TextBox runat="server" ID="txtREF_NO" CssClass="inputflat" Visible="false" ReadOnly="true"
                    Text="" />
                <asp:TextBox runat="server" ID="txtTimeLap" CssClass="inputflat" Visible="false"
                    ReadOnly="true" Text="" />
            </td>
        </tr>

        <tr>
            <td align="right" colspan="2">
                <table width="100%">
                    <tr align="right">
                        <td align="right" valign="top" colspan="3">Lý do chuyển trả
                            <asp:TextBox ID="txtLyDoCTra" runat="server" CssClass="inputflat" Width="50%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr align="right">
                        <td align="right" valign="top" colspan="3">Lý do hủy
                            <asp:TextBox ID="txtLyDoHuy" runat="server" CssClass="inputflat" Width="50%"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td align="center" valign="top" class='text'>
                            <asp:Button ID="cmdHuy" runat="server" Text="[Duyệt  CT]" CssClass="ButtonCommand hiddenClass"
                                UseSubmitBehavior="false" OnClientClick="XacNhan();" />&nbsp;
                           <input type="button" id="cmdKS" class="ButtonCommand" value="[(K)iểm soát]" style="" accesskey="K" onclick="jsValidateKS(); disableButton();" />
                            <%-- <asp:Button ID="cmdKS" runat="server" Text="[(K)iểm soát]" CssClass="ButtonCommand" UseSubmitBehavior="false"
                                OnClientClick="disableButton();" />--%>
                            &nbsp;
                            <asp:Button ID="cmdChuyenTra" runat="server" Text="[(C)huyển trả]" AccessKey="C" CssClass="ButtonCommand"
                                UseSubmitBehavior="false" OnClientClick="disableButton();" />
                            &nbsp;
                            <asp:Button ID="cmdChuyenThue" runat="server" Text="[(G)ửi lại TTSP]" CssClass="ButtonCommand"
                                UseSubmitBehavior="false" OnClientClick="disableButton();" />&nbsp;

                             <div style="display: none">
                                 <asp:Button ID="cmdKSBK" runat="server" Text="[(K)iểm soát] BK" CssClass="ButtonCommand"
                                     UseSubmitBehavior="false" OnClientClick="disableButton();" />
                             </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
