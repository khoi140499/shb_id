﻿Imports System.Data
Imports Business.NTDT.NNTDT
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports System.IO

Partial Class SP_frmBCTongChungTu
    Inherits System.Web.UI.Page
    Dim total As Decimal = 0
    Dim totalRow As Integer = 0
    Dim totalUSD As Decimal = 0
    Dim strNguyeTe As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
           
            'hdfMA_CN.Value = Session.Item("MA_CN_USER_LOGON").ToString
        End If
    End Sub
    Protected Function GetMoTa_TrangThai(ByVal strTrang_Thai As String) As String
        Try
            Dim result As String = ""
            Dim sql As String = "select mota from tcs_dm_trangthai_ntdt where trang_thai='" & strTrang_Thai & "' and GHICHU='CT'"
            Dim dr As IDataReader = DataAccess.ExecuteDataReader(sql, CommandType.Text)
            If dr.Read Then
                result = dr("MOTA").ToString
            End If
            Return result
        Catch ex As Exception
            Return strTrang_Thai
        End Try
    End Function
    Private Sub load_dataGrid()
        total = 0
        totalRow = 0
        Dim objNNTDT As New buNNTDT
        Dim WhereClause As String = ""
        Try
            If txtTuNgay.Value <> "" Then
                WhereClause += " AND (TO_CHAR(A.TRAN_DATE,'yyyymmdd') >= " & ConvertDateToNumber(txtTuNgay.Value) & ") "
            End If
            If txtDenNgay.Value <> "" Then
                WhereClause += " AND (TO_CHAR(A.TRAN_DATE,'yyyymmdd') <= " & ConvertDateToNumber(txtDenNgay.Value) & ") "
            End If
            If drpLoaiThue.SelectedValue.Length > 0 Then
                WhereClause += " AND A.kenh_ct = '" & drpLoaiThue.SelectedValue & "' "
            End If

            Dim strSQL As String = ""
            strSQL = "SELECT A.TRAN_DATE,A.response_msg as REF_NO, A.RM_REF_NO as SO_CTU,A.AMT TONG_TIEN,A.kenh_ct TEN_LTHUE," & _
                     " 'VND' MA_NGUYENTE FROM tcs_log_payment A WHERE 1=1 " & WhereClause & _
                     " ORDER BY  A.TRAN_DATE DESC "
            Dim dt = DatabaseHelp.ExecuteToTable(strSQL)
            If dt.Rows.Count > 0 Then
                totalRow = dt.Rows.Count
                For Each item In dt.Rows
                    total += Decimal.Parse(item("TONG_TIEN").ToString)
                Next
                dtgrd.DataSource = dt
                dtgrd.DataBind()
                grdExport.DataSource = dt
                grdExport.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub

    Protected Sub dtgrd_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dtgrd.PageIndexChanging
        load_dataGrid()
        dtgrd.PageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub

    Protected Sub dtgrd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dtgrd.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lblTotal As Label = DirectCast(e.Row.FindControl("totalAmount"), Label)
            'lblTotal.Text = Replace(total.ToString("##,##"), ",", ".")
            Dim strLBVND As String = ""
            Dim strLBUSD As String = ""
            'lblTotal.Text = total.ToString("##,##") & " / " & totalUSD.ToString("##,##")
            If total = 0 Then
                strLBVND = "0"
            ElseIf total > 0 Then
                strLBVND = total.ToString("##,##")
            End If

            lblTotal.Text = strLBVND
            Dim lblTotalCT As Label = DirectCast(e.Row.FindControl("totalCT"), Label)
            lblTotalCT.Text = totalRow.ToString()
        End If
    End Sub

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        load_dataGrid()
        dtgrd.PageIndex = 0
        dtgrd.DataBind()
    End Sub
   
 

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        grdExport.Visible = True
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=ExportBCTongHopCTThanhCong.xls")
        Response.Charset = "utf-8"
        Response.ContentType = "application/vnd.ms-excel"
        Using sw1 As New StringWriter()
            Dim hw1 As New HtmlTextWriter(sw1)
            grdExport.RenderControl(hw1)
            Response.Output.Write(sw1.ToString)
            Response.Flush()
            Response.End()
        End Using
        grdExport.Visible = False
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub grdExport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdExport.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim lblTotal As Label = DirectCast(e.Row.FindControl("totalAmount"), Label)
            lblTotal.Text = total.ToString("##,##")
            Dim lblTotalCT As Label = DirectCast(e.Row.FindControl("totalCT"), Label)
            lblTotalCT.Text = totalRow.ToString()
        End If
    End Sub
End Class
