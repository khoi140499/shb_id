﻿Imports System.Data
Imports Business.NTDT.NNTDT
Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao
Imports log4net
Imports Business_HQ.Common.mdlCommon

Partial Class SP_frmTraCuu_BL
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If

        If Not IsPostBack Then
            Load_TrangThai()
            Load_Loai_HS()
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub
    Private Sub load_dataGrid()
        Dim ds As DataSet

        Dim WhereClause As String = ""
        Try
            dtgrd.DataSource = Nothing
            dtgrd.DataBind()
            If ConvertDateToNumber(txtTuNgay.Value) > ConvertDateToNumber(txtDenNgay.Value) And txtDenNgay.Value.ToString.Length > 0 Then
                clsCommon.ShowMessageBox(Me, "Điều kiện Từ ngày phải nhỏ hơn hoặc bằng Đến ngày")
                Exit Sub
            End If

            If txtTuNgay.Value <> "" Then
                WhereClause += " AND to_char(A.ngay_ct,'RRRRMMDD') >=" & ConvertDateToNumber(txtTuNgay.Value)
            End If
            If txtDenNgay.Value <> "" Then
                WhereClause += " AND to_char(A.ngay_ct,'RRRRMMDD') <= " & ConvertDateToNumber(txtDenNgay.Value)
            End If
            If txtSoCTU.Text.Length > 0 Then
                WhereClause += " AND A.SO_CT = '" & txtSoCTU.Text.Trim() & "' "
            End If
            If cboTrangThai.SelectedIndex > 0 Then
                WhereClause += " AND A.TRANG_THAI = '" & cboTrangThai.SelectedValue.ToString() & "' "
            End If
            'If cbdTinhPhi.SelectedValue.ToString().Length > 0 Then
            '    If cbdTinhPhi.SelectedValue.ToString().Equals("-1") Then
            '        WhereClause += " AND (A.code_phi='" & cbdTinhPhi.SelectedValue.ToString() & "')"
            '    Else
            '        WhereClause += " AND (A.code_phi is null or A.code_phi <> '-1')"
            '    End If
            'End If
            ds = Business.SP.SongPhuongController.getBienLai_TracuuXuly(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                ' dtgrd.CurrentPageIndex = 0
                SumTien.Value = "0"
                SumCT.Value = "0"
                Dim decTotal As Decimal = 0
                For Each vrow As DataRow In ds.Tables(0).Rows
                    decTotal += Decimal.Parse(vrow("TTIEN").ToString())
                Next
                SumCT.Value = ds.Tables(0).Rows.Count
                SumTien.Value = Business_HQ.Common.mdlCommon.NumberToStringVN(decTotal.ToString())
                dtgrd.DataSource = ds.Tables(0)
                'dtgrd.DataBind()
                dtgrd.PageIndex = 0
                dtgrd.DataBind()

                'grdExport.DataSource = ds.Tables(0)
                'grdExport.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub
    Private Sub Load_TrangThai()
        Try

            Dim sql = "SELECT * FROM tcs_dm_trangthai_sp_bl  WHERE PHAN_HE='CTBL'"
            Dim dr As DataSet = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            cboTrangThai.DataSource = dr.Tables(0)
            cboTrangThai.DataTextField = "MOTA"
            cboTrangThai.DataValueField = "TRANGTHAI"
            cboTrangThai.DataBind()
            cboTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái yêu cầu. Lỗi: " & ex.Message)
        End Try
    End Sub

    Private Sub Load_Loai_HS()
        Try



        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách loại hồ sơ người nộp thuế. Lỗi: " & ex.Message)
        End Try
    End Sub



    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        load_dataGrid()

    End Sub

    Protected Sub dtgrd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dtgrd.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then

            Dim lblTotalTien As Label = DirectCast(e.Row.FindControl("lblTongSotien"), Label)
            lblTotalTien.Text = SumTien.Value

            Dim lblTotalCT As Label = DirectCast(e.Row.FindControl("lblTongSoCT"), Label)
            lblTotalCT.Text = SumCT.Value
        End If
    End Sub

    Protected Sub dtgrd_PageIndexChanging(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dtgrd.PageIndexChanging
        load_dataGrid()
        dtgrd.PageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

End Class
