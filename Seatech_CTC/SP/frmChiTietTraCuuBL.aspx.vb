﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.HQ247.NNTDT
Imports Business.Common.mdlCommon
Imports Business.BaoCao
Imports Business.Common
Imports System.IO
Imports System.Xml
Imports CustomsV3.MSG.MSG304
Imports System.Collections.Generic
Imports System.Diagnostics
Imports Business_HQ
Imports HQ247XulyMSG
Imports log4net.Repository.Hierarchy

Partial Class SP_frmChiTietTraCuuBL
    Inherits System.Web.UI.Page
    Private MACN_HS As String = ConfigurationManager.AppSettings.Get("MACN_HS").ToString()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim v_strRootPath As String = ConfigurationManager.AppSettings("APP_ROOT_PATH").ToString
        Dim iResult As Integer = 0
        Dim strSql As String = " SELECT COUNT (1) KIEMTRA   FROM ("
        strSql &= "  SELECT A.MA_CN           FROM TCS_DM_CHUCNANG A , TCS_PHANQUYEN B , TCS_DM_NHANVIEN C , TCS_SITE_MAP D         "
        strSql &= "   WHERE UPPER(C.TEN_DN)=UPPER('" & Session.Item("UserName").ToString() & "') AND D.STATUS = 1  AND D.NAVIGATEURL = '" + HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") + "' "
        strSql &= "  AND (A.MA_CN=D.NODEID OR (A.MA_CN=D.PARENTNODEID ) )"
        strSql &= "  AND A.MA_CN=B.MA_CN AND B.MA_NHOM =C.MA_NHOM ) "

        If HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") <> "~/pages/frmHomeNV.aspx" Then
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
            If Not Globals.IsNullOrEmpty(dt) Then
                iResult = CInt(dt.Rows(0)(0).ToString())
            End If
        End If

        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                If Not Request.Params("ID") Is Nothing Then
                    Dim NNTDT_ID As String = Request.Params("ID").ToString
                    Dim strErrMsg As String = ""
                    If NNTDT_ID <> "" Then
                        LoadCTChiTietAll(NNTDT_ID)
                    End If
                End If
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Lỗi: " & ex.Message)
            End Try
        End If

    End Sub
    Private Sub LoadCTChiTietAll(ByVal sSOCT As String)
        Try
            Dim v_strTT As String = String.Empty
            Dim v_strTTSP As String = String.Empty
            Dim v_strLoaiThue As String = String.Empty
            Dim v_strPhuongThucTT As String = String.Empty
            Dim v_str_Ma_NNT As String = String.Empty
            Dim v_strSO_CT As String = sSOCT
            Dim vStrProduct As String = ""
            'Load chứng từ header
            Dim dsCT As New DataSet
            dsCT = Business.ChungTuSP.buChungTu.CTU_Header_Set_BL(sSOCT)
            If (dsCT.Tables(0).Rows.Count > 0) Then
                'ShowHideCTRow(True)
                FillDataToCTHeader(dsCT)
                v_strTT = dsCT.Tables(0).Rows(0)("TRANG_THAI").ToString
                v_strLoaiThue = dsCT.Tables(0).Rows(0)("MA_LTHUE").ToString
                v_str_Ma_NNT = dsCT.Tables(0).Rows(0)("Ma_NNThue").ToString
                v_strTTSP = dsCT.Tables(0).Rows(0)("TT_SP").ToString
                v_strSO_CT = dsCT.Tables(0).Rows(0)("so_ct").ToString
                vStrProduct = dsCT.Tables(0).Rows(0)("PRODUCT").ToString
            Else
                lblStatus.Text = "Không tìm thấy chứng từ cần tìm.Hãy thử lại"
                Exit Sub
            End If

            'Load thông tin chi tiết
            Dim dsCTChiTiet As New DataSet
            'neu la etax thi lay thuong
            If vStrProduct.ToUpper().Equals("ETAX") Then
                dsCTChiTiet = Business.ChungTuSP.buChungTu.CTU_ChiTiet_Set_CT_BL(v_strSO_CT)
            Else
                dsCTChiTiet = Business.ChungTuSP.buChungTu.CTU_ChiTiet_Set_CT_BL_New(v_strSO_CT)
            End If


            If dsCTChiTiet.Tables(0).Rows.Count > 0 Then
                FillDataToCTDetail(dsCTChiTiet, v_str_Ma_NNT)
                ' CaculateTongTien(dsCTChiTiet.Tables(0))
            Else
                lblStatus.Text = "Không lấy được thông tin chi tiết của chứng từ.Hãy kiểm tra lại"
                Exit Sub
            End If

            'ShowHideButtons(v_strTT, v_strTTSP)
            'ShowHideControlByLoaiThue(v_strLoaiThue)

        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())
            'Throw ex
        Finally
            'Catch ex As Exception
            '    logger.Error(ex.StackTrace)
        End Try
    End Sub

    Private Sub FillDataToCTHeader(ByVal dsCT As DataSet)
        Try
            Dim strTenVT = String.Empty
            Dim strTenLH As String = String.Empty
            Dim strMaLH As String = String.Empty

            Dim MA_NV As String = String.Empty
            Dim TRANG_THAI As String = String.Empty
            Dim MA_CN_USER As String = String.Empty

            Dim dr As DataRow = dsCT.Tables(0).Rows(0)
            Dim v_strTrangThai As String = dr("TRANG_THAI").ToString()
            Dim v_strHTTT As String = GetString(dr("PT_TT")).ToString

            txtMaNNT.Text = dr("MA_NNTHUE").ToString()
            txtTenNNT.Text = dr("TEN_NNTHUE").ToString()
            txtDChiNNT.Text = dr("DC_NNTHUE").ToString()

            ddlHTTT.SelectedValue = dr("PT_TT").ToString()
            ddlMaNT.SelectedValue = dr("MA_NT").ToString()

            txtTK_KH_NH.Text = dr("TK_KH_NH").ToString()
            txtTenTK_KH_NH.Text = dr("TEN_KH_NH").ToString()

            txtSoBienLai.Text = dr("SO_BIENLAI").ToString()
            txtHuyenNNThue.Text = dr("ten_huyen_nnthue").ToString()
            txtTinhNNThue.Text = dr("ten_tinh_nnthue").ToString()
            txtHuyenNNtien.Text = dr("ten_huyen_nntien").ToString()
            txtTinhNNTien.Text = dr("ten_tinh_nntien").ToString()


            txtMaNNTien.Text = dr("Ma_NNTien").ToString()
            txtTenNNTien.Text = dr("TEN_NNTIEN").ToString()
            txtDChiNNTien.Text = dr("DC_NNTIEN").ToString()

            txtSHKB.Text = dr("SHKB").ToString()
            txtTenKB.Text = dr("TEN_KB").ToString()
            txtMaCQThu.Text = dr("MA_CQTHU").ToString()
            txtTenCQThu.Text = dr("TEN_CQTHU").ToString()
            ddlMaLThue.SelectedValue = dr("MA_LTHUE").ToString()
            ddlHT_Thu.SelectedValue = dr("HT_THU").ToString()
            ddlMa_NTK.SelectedValue = dr("MA_NTK").ToString()

            txtTKCo.Text = dr("TK_Co").ToString()
            txtMA_NH_TT.Text = dr("MA_NH_TT").ToString()
            txtTEN_NH_TT.Text = dr("TEN_NH_TT").ToString()
            txtMA_NH_B.Text = dr("MA_NH_GT").ToString()
            txtTen_NH_B.Text = dr("TEN_NH_GT").ToString()
            txtTTienVND.Text = dr("TTIEN").ToString()
            txtSo_BT.Text = dr("SO_BT").ToString()
            'txtTimeLap.Text = GetString(dr("ngay_ht"))

            txtMa_CQQD.Text = dr("MA_CQQD").ToString()
            txtTen_CQQD.Text = dr("TEN_CQQD").ToString()

            txtMaLHThu.Text = dr("MA_LHTHU").ToString()
            txtTenLHTHU.Text = dr("TEN_LHTHU").ToString()

            txtTTien_VPHC.Text = dr("TTIEN_VPHC").ToString()
            txtLyDo_VPHC.Text = dr("LY_DO_VPHC").ToString()
            txtTTien_nop_cham.Text = dr("TTIEN_NOP_CHAM").ToString()
            txtTTien_CTBL.Text = dr("TTIEN_CTBL").ToString()

            txtDienGiai.Text = GetString(dr("DIEN_GIAI"))

            txtSoQD.Text = dr("SO_QD").ToString()
            txtNgay_QD.Text = dr("NGAY_QD").ToString()

            'txtRM_REF_NO.Text = dr("SO_CT_NH").ToString()
            'txtREF_NO.Text = dr("SO_CT_NH").ToString()
            txtSoCT_NH.Text = dr("SO_CT_NH").ToString()
            txtProduct.Text = dr("PRODUCT").ToString()

            txtLyDoHuy.Text = dr("LY_DO_HUY").ToString()

            txtLyDoCTra.Text = dr("LY_DO").ToString()
            hdfSo_CT.Value = dr("so_ct").ToString()
            MA_CN_USER = dr("MA_CN").ToString()
            'hdfTrangThai_CT.Value = dr("trang_thai").ToString().Trim
            'hdfTTSP.Value = dr("tt_SP").ToString().Trim
            'hdfDienGiai.Value = dr("dien_giai").ToString().Trim
            'hdfMACN.Value = dr("ma_cn").ToString().Trim

            If ((v_strTrangThai = "08" Or v_strTrangThai = "03" Or v_strTrangThai = "09") And MA_CN_USER = Session.Item("MA_CN_USER_LOGON")) Or (MACN_HS = Session.Item("MA_CN_USER_LOGON")) Then
                btnIn.Enabled = True
                btnInBL.Enabled = True
                btnInBangKe.Enabled = True
            Else
                btnIn.Enabled = False
                btnInBL.Enabled = False
                btnInBangKe.Enabled = False
            End If

            Dim v_dt As DataTable = Nothing

        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình FillDataToCTHeader")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

           log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

            Throw ex
        Finally
        End Try
    End Sub

    Private Sub FillDataToCTDetail(ByVal pv_dsCT As DataSet, ByVal v_strMa_NNT As String)
        Try
            Dim v_dt As DataTable = pv_dsCT.Tables(0)
            Dim dr As DataRow
            Dim sodathu As String
            grdChiTiet.DataSource = v_dt
            grdChiTiet.DataBind()

        Catch ex As Exception

        End Try
    End Sub
    Public Function convertDateHQ(ByVal strDate As String) As String

        Dim str As String = ""
        Try
            str = strDate.Substring(8, 2) & "/" & strDate.Substring(5, 2) & "/" & strDate.Substring(0, 4)
            If strDate.Trim.Length > 10 Then
                str &= strDate.Substring(10, strDate.Length - 10)
            End If
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error(sbErrMsg.ToString())

        End Try

        Return str
    End Function

    Protected Sub btnIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIn.Click
        LogApp.AddDebug("cmdInCT_Click", "In GNT chứng từ biên lai")
        Try
            TCS_IN_GNT()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình in giấy nộp tiền chứng từ biên lai!")
        Finally
            btnIn.Enabled = True
        End Try
    End Sub

    Private Sub TCS_IN_GNT()
        Try
            If hdfSo_CT.Value.Length > 0 Then
                clsCommon.OpenNewWindow(Me, "../pages/BaoCao/frmInGNT_BL.aspx?so_ct=" & hdfSo_CT.Value, "ManPower")
                'Response.Redirect("../BaoCao/frmInGNT_HQ.aspx?so_ct=" & hdfSo_CT.Value)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Protected Sub btnInBL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInBL.Click
        LogApp.AddDebug("btnInBL_Click", "In biên lai")
        Try
            TCS_IN_BIENLAI()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình in biên lai!")
        Finally
            btnIn.Enabled = True
        End Try
    End Sub

    Private Sub TCS_IN_BIENLAI()
        Try
            If hdfSo_CT.Value.Length > 0 Then
                clsCommon.OpenNewWindow(Me, "../pages/BaoCao/frmInBL.aspx?so_ct=" & hdfSo_CT.Value, "ManPower")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnInBangKe_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInBangKe.Click
        LogApp.AddDebug("btnInBangKe_Click", "In bảng kê")
        Try
            TCS_IN_BANGKE()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình in bảng kê!")
        Finally
            btnIn.Enabled = True
        End Try
    End Sub

    Private Sub TCS_IN_BANGKE()
        Try
            If hdfSo_CT.Value.Length > 0 Then
                clsCommon.OpenNewWindow(Me, "../pages/BaoCao/frmInBangKe.aspx?so_ct=" & hdfSo_CT.Value, "ManPower")
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
