﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmTraCuuCTSP.aspx.vb" Inherits="SP_frmTraCuuCTSP" Title="Tra cứu và xử lý message nhận từ Tổng cục hải quan" %>

<%@ Import Namespace="Business_HQ.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }
        function jsChiTietNNT(_ID) {

            window.open(_ID.replace("~", ".."), "_blank", "toolbar=no,scrollbars=yes,menubar=no, addressbar=no,location=no,resizable=yes,top=100,left=200,width=800,height=480");
        }
    </script>
    <script type="text/javascript">
        //       $(document).ready(function(){
        //         $(".JchkAll").click(function(){
        //        // alert($(this).is(':checked'));
        //            $('.JchkGrid').attr('checked',$(this).is(':checked'));
        //            GetListSoCT();
        //         });
        //          
        //         });
        function GetListSoCT() {
            var values = $('input:checkbox:checked.JchkGrid').map(function () {
                return this.value;
            }).get();



            if (values == '') {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);


             }
             else {
                 $('#<%=ListCT_NoiDia.ClientID %>').val(values);


             }
         }
    </script>
    <style type="text/css">
        .container {
            width: 100%;
            margin-bottom: 30px;
        }

        .dataView {
            padding-bottom: 10px;
        }

        .JchkAll, .JchkGrid {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <p class="pageTitle">Tra cứu điện thu NSNN từ CITAD</p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
              <tr>
                <td align="left" style="width: 15%" class="form_label">Từ ngày</td>
                <td class="form_control" style="width: 35%"><span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                        onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                </span></td>
                <td align="left" style="width: 15%" class="form_label">Đến ngày</td>
                <td class="form_control" style="width: 35%">
                    <span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                        <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                            onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                    </span></td>
            </tr>

            <tr>
                <td align="left" class="form_label">Mã số thuế</td>
                <td class="form_control">
                    <asp:TextBox ID="txtMST" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <td align="left" class="form_label">Tên Người nộp thuế</td>
                <td class="form_control">
                    <asp:TextBox ID="txtTen_NNT" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="form_label">Số chứng từ</td>
                <td class="form_control">
                    <asp:TextBox ID="txtSoCTU" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <td align="left" class="form_label">Số GD Corebank</td>
                <td class="form_control">
                    <asp:TextBox ID="txtSO_CT_NH" runat="server" class="inputflat" Width="89%"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td align="left" class="form_label">Trạng thái</td>
                <td class="form_control">
                    <asp:DropDownList ID="cboTrangThai" runat="server" Width="90%" CssClass="selected inputflat">
                        <asp:ListItem Selected="True" Value="" Text="Tất cả"></asp:ListItem>
                        <%--<asp:ListItem Value="01" Text="Mới nhận từ Citad"></asp:ListItem>
                        <asp:ListItem Value="02" Text="Chờ kiểm soát"></asp:ListItem>
                        <asp:ListItem Value="03" Text="Hạch toán thành công"></asp:ListItem>
                        <asp:ListItem Value="04" Text="Chuyển trả"></asp:ListItem>
                        <asp:ListItem Value="05" Text="Chờ duyệt hủy"></asp:ListItem>
                        <asp:ListItem Value="06" Text="Đã hủy"></asp:ListItem>--%>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnTimKiem" CssClass="buttonField" runat="server" Text="Tìm kiếm" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </div>

    <div class="dataView">

        <asp:HiddenField ID="ListCT_NoiDia" runat="server" />
        <asp:GridView ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="100%" BorderColor="Black" AllowPaging="True">
            <Columns>
                <asp:TemplateField HeaderText="Số chứng từ">
                    <ItemTemplate>
                        <a href="#" onclick="jsChiTietNNT('<%#Eval("NAVIGATEURL").ToString %>')"><%#Eval("SO_CT").ToString%></a>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:BoundField DataField="SO_CT_NH" HeaderText="Số GD CoreBank"
                    ReadOnly="True" SortExpression="SO_CT_NH" />
                <asp:BoundField DataField="MA_NNTHUE" HeaderText="Mã số thuế" ReadOnly="True" />
                <asp:BoundField DataField="TEN_NNTHUE" HeaderText="Tên người nộp thuế" ReadOnly="True" />
                <asp:TemplateField HeaderText="Số tiền (VNĐ)">
                    <ItemTemplate>
                        <asp:Label ID="lblSotien" runat="server" Text='<%# mdlCommon.NumberToStringVN(DataBinder.Eval(Container.DataItem, "TTIEN")) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Ngày Nhận CT" DataField="TG_KY" Visible="true" ReadOnly="true" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="TRANG_THAI" HeaderText="Trạng thái" ReadOnly="True" />

                <asp:BoundField DataField="MA_LOI" HeaderText="Mô tả lỗi" ReadOnly="True" />
            </Columns>

            <HeaderStyle BackColor="#999966" BorderColor="Black" BorderStyle="Solid"
                BorderWidth="1px" Font-Bold="True" />
            <AlternatingRowStyle BackColor="#EFEDE4" />

        </asp:GridView>

    </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function () {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>
</asp:Content>
