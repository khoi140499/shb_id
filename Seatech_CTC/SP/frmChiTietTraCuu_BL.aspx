﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmChiTietTraCuu_BL.aspx.vb" Inherits="SP_frmChiTietTraCuu_BL" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Chi tiết thông tin</title>
    <link href="../css/myStyles.css" type="text/css" rel="stylesheet" />
    <link href="../css/menuStyle.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .txtHoanThien80 {
            max-width: 400px;
            width: 80%;
        }

        .txtHoanThien {
            width: 400px;
        }

        .txtHoanThien01 {
            width: 557px;
        }

        .container {
            width: 100%;
            padding-bottom: 50px;
        }

        .dataView {
            padding-bottom: 10px;
        }

        .number {
            text-align: right;
        }

        .formx {
            color: #E7E7FF;
            background: #eaedf0;
            padding-left: 1px;
            color: Black;
        }

        .ButtonCommand {
            margin-left: 371px;
        }

        .auto-style1 {
            width: 509px;
        }
        .form1
        {
            background-color:#f7f6f6 ;
        }
        BODY {
            background-color:#fff ;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div class="container">
            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                <tr>
                    <td colspan="4" width='100%' align="right">
                        <table width="100%">
                            <tr class="grid_header">
                                <td height="15px" align="left">
                                    <%--Trường <span style="color: #FF0000; font-weight: bold">(*)</span> là bắt buộc nhập--%>
                                                                            THÔNG TIN CHỨNG TỪ 
                                </td>
                            </tr>
                        </table>
                        <div id="Panel1" style="/*height: 380px; */ width: 100%; /*overflow-y: scroll; */">
                            <table id="grdHeader" cellspacing="0" cellpadding="1" rules="all" border="1" style="border-width: 1px; border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse;">
                                <tr id="Tr1">
                                    <td width="24%">
                                        <b>Số/ Ngày chứng từ </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td width="25%">
                                        <asp:TextBox runat="server" ID="txtSoCTU" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                    <td width="51%">
                                        <asp:TextBox runat="server" ID="txtNgayCTU" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="Tr2">
                                    <td width="24%">
                                        <b>Số/ Ký hiệu BL </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td width="25%">
                                        <asp:TextBox runat="server" ID="txtSoBL" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                    <td width="51%">
                                        <asp:TextBox runat="server" ID="txtKH_BL" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="rowNgay_KH_NH">
                                    <td>
                                        <b>Ngày chuyển/ Số Ref SP</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtNGAY_KH_NH" class="inputflat" Style="width: 99%; background: #D6D3CE" disabled="disabled" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtSoRefTTSP" class="inputflat" style="width: 99%; background:#D6D3CE" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="rowSHKB">
                                    <td width="24%">
                                        <b>Số hiệu KB </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td width="25%">
                                        <asp:TextBox runat="server" ID="txtSHKB" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />


                                    </td>
                                    <td width="51%">
                                        <asp:TextBox runat="server" ID="txtTenKB" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="Tr3">
                                    <td>
                                        <b>Cơ quan quyết định</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtCQQD" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTenCQQD" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="rowCQThu">
                                    <td class="style1">
                                        <b>CQ Thu/DBHC </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td class="style1">
                                        <asp:TextBox runat="server" ID="txtMaCQThu" Style="width: 99%; background: #f7f6f6" disabled="disabled"
                                            class="inputflat" />
                                    </td>
                                    <td class="style1">
                                        <asp:TextBox runat="server" ID="txtMA_DBHC" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="rowLHBLThu">
                                    <td>
                                        <b>Loại Hình thu</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtMA_LOAIHINH" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTEN_LOAIHINH" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="rowTKNSNN">
                                    <td>
                                        <b>Tài Khoản NSNN / Chương</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTK_NSNN" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtMA_CHUONG" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="rowNDKT">
                                    <td>
                                        <b>Mục NSNN</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtMA_TMUC" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtNOIDUNG" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                </tr>

                                 <tr id="Tr9">
                                      <td width="24%">
                                        <b>Loại biên lai </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                         <asp:DropDownList runat="server" ID="txtLoaiBienLai" CssClass="inputflat" Enabled="false"
                                            Width="100%">
                                            <asp:ListItem Value="1" Text="Biên lai thu phạt" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Biên lai thu phí, lệ phí"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>

                                <tr id="rowMaNNT">
                                    <td width="24%">
                                        <b>Mã số thuế/Tên </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td width="25%">
                                        <asp:TextBox runat="server" ID="txtMaNNT" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                    <td width="51%">
                                        <asp:TextBox runat="server" ID="txtTenNNT" class="inputflat" Style="width: 92%; background: #f7f6f6" disabled="disabled" />

                                    </td>
                                </tr>
                                <tr id="rowNNTien">
                                    <td>
                                        <b>Số CMT/Tên người nộp tiền</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtMaNNTien" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTenNNTien" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="Tr6">
                                    <td>
                                        <b>Địa chỉ</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox runat="server" ID="txtDIACHI" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>

                                </tr>
                                <tr id="rowSoQD">
                                    <td>
                                        <b>Số/ Ngày Quyết định</b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtSO_QD" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                    <td>

                                        <asp:TextBox runat="server" ID='txtNGAY_QD' runat="server" MaxLength='10' class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />

                                    </td>
                                </tr>
                                <tr id="Tr4">
                                    <td>
                                        <b>Tổng tiền/ Nguyên tệ </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTongTien" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtMA_NT" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" value="VND" />
                                    </td>

                                </tr>
                                <tr id="row">
                                    <td>
                                        <b>Số tiền/Lý do nộp VPHC </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTTIEN_VPHC" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtLYDO" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>

                                </tr>
                                <tr id="Tr5">
                                    <td>
                                        <b>Số tiền/Lý do chậm nộp </b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTTIEN_CHAMNOP" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtLYDO_CHAMNOP" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>

                                </tr>
                                <tr id="Tr7">
                                    <td>
                                        <b>Ghi chú</b>
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox runat="server" ID="txtGHICHU" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>


                                </tr>
                                <tr id="rowHTTT">
                                    <td>
                                        <b>Hình thức thanh toán</b>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlMaHTTT" CssClass="inputflat" Enabled="false"
                                            Width="100%">
                                            <asp:ListItem Value="01" Text="Chuyển khoản" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="05" Text="Nộp tiền mặt"></asp:ListItem>
                                        </asp:DropDownList>

                                    </td>
                                </tr>
                                <tr id="rowTK_KH_NH">
                                    <td>
                                        <b>TK Chuyển </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTK_KH_NH" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTenTK_KH_NH" class="inputflat" Style="width: 95%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="rowSoDuNH">
                                    <td>
                                        <b>Số dư NH</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtSoDu_KH_NH" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtDesc_SoDu_KH_NH" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                </tr>
                                <tr id="rowTKGL" style="display:none">
                                    <td>
                                        <b>TK chuyển </b><span class="requiredField">(*)</span>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTKGL" MaxLength="12" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTenTKGL" class="inputflat" Style="width: 99%; background: #f7f6f6" disabled="disabled" />
                                    </td>
                                </tr>
                               
                                <tr id="Tr8">
                                    <td>
                                        <b>Lý do từ chối </b>
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox runat="server" ID="txtLYDO_TC" class="inputflat" Style="width: 99%;" />
                                    </td>

                                </tr>

                            </table>
                        </div>
                    </td>
                </tr>
                <tr style="visibility: hidden">
                    <td width="25%" style="height: 0px">
                        <asp:TextBox runat="server" ID="txtSo_BT" CssClass="inputflat" ReadOnly="true" Visible="false"
                            Width="99%" />
                        <asp:TextBox runat="server" ID="txtSoCT" CssClass="inputflat" ReadOnly="true" Visible="false"
                            Width="99%" />
                         <asp:TextBox runat="server" ID="txtSoRefCore" CssClass="inputflat" ReadOnly="true" Visible="false"
                            Width="99%" />
                        <asp:TextBox runat="server" ID="txtSoCT_NH" CssClass="inputflat" ReadOnly="true"
                            Visible="false" Width="99%" />
                    </td>
                    <td width="51%" style="height: 0px">
                        <asp:TextBox runat="server" ID="txtTRANG_THAI" CssClass="inputflat" ReadOnly="true"
                            Visible="false" Width="99%" />
                        <asp:TextBox runat="server" ID="txtMA_NV" CssClass="inputflat" ReadOnly="true" Visible="false"
                            Width="99%" />
                    </td>
                </tr>
                <tr>
                                                            <td align="right" colspan="2">
                                                                <table width="550px">
                                                                    <tr align="right">
                                                                        <td align="right"  colspan="6" style =" display:none">
                                                                            <span style="font-family: @Arial Unicode MS; font-weight: bold;">Tổng Tiền: </span>
                                                                              <asp:TextBox runat="server" ID="txtTTienx" Width="100px" Style="text-align: right" CssClass="inputflat" ReadOnly="true"
                                                                                Text="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr  align="right"> 

                                                                        <td style =" display:none">Phí
                                                                        </td>
                                                                        <td class="auto-style1" style =" display:none">
                                                                            <asp:TextBox runat="server" ID="txtCharge" Width="100px" Style="text-align: right" CssClass="inputflat" ReadOnly="true"
                                                                                Text="0" />
                                                                        </td>
                                                                        <td style =" display:none">VAT:
                                                                        </td>
                                                                        <td style =" display:none">
                                                                            <asp:TextBox runat="server" ID="txtVAT" Width="100px" Style="text-align: right" CssClass="inputflat" ReadOnly="true"
                                                                                Text="0" />
                                                                        </td>
                                                                        <td>Tổng trích nợ:
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtTongTrichNo" Style="text-align: right" Width="120px" CssClass="inputflat"
                                                                                ReadOnly="true" Text="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr align="right" style="display: none">
                                                                        <td align="right" valign="top" colspan="6" style="display: none">Lý do hủy: &nbsp;                                                                                                            
                                                                                    <asp:DropDownList ID="drpLyDoHuy" runat="server" Style="width: 60%" CssClass="inputflat">
                                                                                        <asp:ListItem Value="0">Không có trong dữ liệu HQ/NH </asp:ListItem>
                                                                                        <asp:ListItem Value="2">Điện thừa </asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
            </table>
        </div>
        <div>
            <asp:HiddenField ID="hdfSo_CT" runat="server" Value="" />
            <asp:HiddenField ID="hdfMACN" runat="server" Value="" />
            <asp:HiddenField ID="hdfSoRefCore" runat="server" Value="" />
            <table width="100%">
                <tr align="right" style="display:none">
                    <td align="right" valign="top" colspan="3">Lý do chuyển trả
                            <asp:TextBox ID="txtLyDoCTra" runat="server" CssClass="inputflat" Width="50%"></asp:TextBox>
                    </td>
                </tr>
                <tr align="right" style="display:none">
                    <td align="right" valign="top" colspan="3">Lý do hủy
                            <asp:TextBox ID="txtLyDoHuy" runat="server" CssClass="inputflat" Width="50%"></asp:TextBox>
                    </td>
                </tr>
            </table>

        </div>
        <div>
            <table width="100%">
                <tr align="center">
                    <td align="center">

                        <asp:Button ID="btnIn" runat="server" Text="In Chứng từ" UseSubmitBehavior="false"  Visible ="false"/>
                        &nbsp;&nbsp;
                        <asp:Button ID="btnInBL" runat="server" Text="In Biên Lai" UseSubmitBehavior="false" />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnInBangKe" runat="server" Text="In Bảng kê" UseSubmitBehavior="false"  Visible ="false"/>
                    </td>

                </tr>
            </table>
        </div>
    </form>

</body>
</html>
