﻿Imports Business_HQ
Imports Business_HQ.Common
Imports System.Data
Imports CustomsV3.MSG.MSG101
Imports System.Collections.Generic
Imports CustomsV3.MSG.MSG102
Imports Newtonsoft.Json
Imports VBOracleLib
Imports System.Diagnostics


Partial Class SP_frmLapDienNHKhac
    Inherits System.Web.UI.Page
    Private Shared cls_corebank As New CorebankServiceESB.clsCoreBank
    Private Shared cls_songphuong As New CorebankServiceESB.clsSongPhuong
    Private m_User As Business_HQ.CTuUser
    Private Shared CORE_VERSION As String = ConfigurationManager.AppSettings.Get("CORE.VERSION").ToString()

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Session.IsNewSession Then
            ' Force session to be created;
            ' otherwise the session ID changes on every request.
            Session("ForceSession") = DateTime.Now
        End If
        ' 'Sign' the viewstate with the current session.
        Me.ViewStateUserKey = Session.SessionID
        If Page.EnableViewState Then
            ' Make sure ViewState wasn't passed on the querystring.
            ' This helps prevent one-click attacks.
            If Not String.IsNullOrEmpty(Request.Params("__VIEWSTATE")) AndAlso String.IsNullOrEmpty(Request.Form("__VIEWSTATE")) Then
                Throw New Exception("Viewstate existed, but not on the form.")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sSHKB As String = String.Empty
        Dim sMaDiemThu As String = String.Empty
        Dim sNgay_LV As String = ""

        If Session.Item("User") Is Nothing Then
            If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                Response.Cookies("ASP.NET_SessionId").Value = String.Empty
                Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
            End If
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            If Request.Cookies("ASP.NET_SessionId") IsNot Nothing Then
                Response.Cookies("ASP.NET_SessionId").Value = String.Empty
                Response.Cookies("ASP.NET_SessionId").Expires = DateTime.Now.AddMonths(-20)
            End If
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If

        m_User = New Business_HQ.CTuUser(Session.Item("User"))
        If Not Session.Item("User") Is Nothing Then

            sSHKB = clsCTU.TCS_GetKHKB(CType(Session("User"), Integer))
            sMaDiemThu = clsCTU.TCS_GetMaDT(CType(Session("User"), Integer))
        End If

        If Not IsPostBack Then
            If (Not ClientScript.IsStartupScriptRegistered(Me.GetType(), "InitDefValues")) Then
                ClientScript.RegisterStartupScript(Me.GetType(), "InitDefValues", PopulateDefaultValues(sMaDiemThu), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrSHKB", Business_HQ.HaiQuan.HaiQuanController.PopulateArrSHKB(sMaDiemThu), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrDBHC", Business_HQ.HaiQuan.HaiQuanController.PopulateArrDBHC(sSHKB), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrCQThu", Business_HQ.HaiQuan.HaiQuanController.PopulateArrCQThu(sMaDiemThu), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTKGL", Business_HQ.HaiQuan.HaiQuanController.PopulateArrTKGL(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrNT", Business_HQ.HaiQuan.HaiQuanController.PopulateArrNT(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTyGia", Business_HQ.HaiQuan.HaiQuanController.PopulateArrTyGia(), True)
                'ClientScript.RegisterStartupScript(Me.GetType(), "InitArrLTHUE", HaiQuan.HaiQuanController.PopulateArrLoaiThue(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrLTIEN", Business_HQ.HaiQuan.HaiQuanController.PopulateArrLoaiTien(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTKNoNSNN", Business_HQ.HaiQuan.HaiQuanController.PopulateArrTKNoNSNN(sSHKB), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTKCoNSNN", Business_HQ.HaiQuan.HaiQuanController.PopulateArrTKCoNSNN(sSHKB), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrLHSX", Business_HQ.HaiQuan.HaiQuanController.PopulateArrLHSX(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTT_NH_B", Business_HQ.HaiQuan.HaiQuanController.PopulateArrTT_NH_B(), True)
                'ClientScript.RegisterStartupScript(Me.GetType(), "InitArrTT_NH_TT", Business_HQ.HaiQuan.HaiQuanController.PopulateArrTT_NH_TT(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrMANHA", Business_HQ.HaiQuan.HaiQuanController.PopulateArrDM_MA_NH_A(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrPTTT", Business_HQ.HaiQuan.HaiQuanController.PopulateArrDM_PTTT(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitPage", "jsThem_Moi();", True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitGL", GetGL(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitTinhPhi", Business_HQ.HaiQuan.HaiQuanController.PopulateHangSoTinhPhi(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitArrSacThue", Business_HQ.HaiQuan.HaiQuanController.PopulateArrSacThue(), True)
                '20180403_Core version
                ClientScript.RegisterStartupScript(Me.GetType(), "InitCoreVersion", GetCoreVersion(), True)
                ClientScript.RegisterStartupScript(Me.GetType(), "InitRemark2936", GetRemark2936(), True)
            End If
            If Not Request.Params("SoCT") Is Nothing Then
                Dim soCT As String = Request.Params("SoCT").ToString
                If soCT <> "" Then
                    Dim dtCT As DataTable
                    dtCT = Business_HQ.HaiQuan.HaiQuanController.CtuLoad(Session.Item("User").ToString, clsCTU.TCS_GetNgayLV(Session.Item("User").ToString), "01", soCT)
                    If dtCT IsNot Nothing AndAlso dtCT.Rows.Count > 0 Then
                        ClientScript.RegisterStartupScript(Me.GetType(), "PageLoad", "jsGetCTU('" & dtCT.Rows(0)("SO_CT").ToString & "');", True)
                    End If
                End If
            End If

        End If
    End Sub
    Private Function GetGL() As String
        Dim strSQL As String = "select so_tk,ten_tk from tcs_dm_tk_tg"
        Dim strProvice As String = DataAccess.ExecuteSQLScalar(strSQL)

        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Dim noOfItems As Integer = dt.Rows.Count
            Dim myJavaScript As StringBuilder = New StringBuilder()
            myJavaScript.Append(" var arrGL = new Array(" & noOfItems.ToString & ");")
            For i As Integer = 0 To dt.Rows.Count - 1
                myJavaScript.Append(" arrGL[" & i & "] ='" & dt.Rows(i)("SO_TK") & ";" & dt.Rows(i)("TEN_TK") & "';")
            Next
            Return myJavaScript.ToString()
        End If
        Return String.Empty

    End Function
    Private Function GetCoreVersion() As String
        Dim myJavaScript As StringBuilder = New StringBuilder()

        myJavaScript.Append(" var v_coreVersion =""" & CORE_VERSION & """;")

        Return myJavaScript.ToString
    End Function
    Private Function GetRemark2936() As String
        Dim myJavaScript As StringBuilder = New StringBuilder()

        myJavaScript.Append(" var v_remark2936 =""" & Business_HQ.CTuCommon.GetValue_THAMSOHT("CITAD_REMARK") & """;")

        Return myJavaScript.ToString
    End Function
    Private Function PopulateDefaultValues(ByVal sMaDiemThu As String) As String
        Dim myJavaScript As StringBuilder = New StringBuilder()
        Dim sSHKB = Business_HQ.HaiQuan.HaiQuanController.GetSHKB(sMaDiemThu)
        myJavaScript.Append(" var dtmNgayLV =""" & mdlCommon.ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString)) & """;")
        myJavaScript.Append(" var dtmNgayNH =""" & mdlCommon.ConvertNumbertoString(clsCTU.TCS_GetNgayKH_NH(Session.Item("User").ToString)) & """;")
        myJavaScript.Append(" var defSHKB =""" & sSHKB & """;")
        myJavaScript.Append(" var defDBHC =""" & Business_HQ.HaiQuan.HaiQuanController.GetDefaultDBHC(sMaDiemThu) & """;")
        myJavaScript.Append(" var defMaVangLai =""" & Business_HQ.HaiQuan.HaiQuanController.GetDefaultMaVangLai() & """;")
        myJavaScript.Append(" var defMaNHNhan =""" & Business_HQ.HaiQuan.HaiQuanController.GetDefaultMaNHTH(sSHKB) & """;")
        myJavaScript.Append(" var defMaNHTT =""" & Business_HQ.HaiQuan.HaiQuanController.GetDefaultMaNHTT(sSHKB) & """;")
        myJavaScript.Append(" var defMaHQ =""" & Business_HQ.HaiQuan.HaiQuanController.GetDefaultMaHQ(sMaDiemThu) & """;")
        myJavaScript.Append(" var curCtuStatus =""99"";")
        myJavaScript.Append(" var curMaNNT ="""";")
        myJavaScript.Append(" var curKyThue =""" & mdlCommon.GetKyThue(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString)) & """;")
        myJavaScript.Append(" var curSoCT ="""";")
        myJavaScript.Append(" var curSo_BT ="""";")
        myJavaScript.Append(" var curMa_NV =""" & CType(Session("User"), Integer).ToString & """;")
        myJavaScript.Append(" var curTeller_ID =""" & clsCTU.Get_TellerID(CType(Session("User"), Integer)) & """;")
        myJavaScript.Append(" var saiSo = """ & Business_HQ.CTuCommon.Get_ParmValues("SAI_THUCTHU", sMaDiemThu).ToString() & """;")
        myJavaScript.Append(" var varLoai_CTU = """";")

        Return myJavaScript.ToString
    End Function
    Private Shared Function ValidUserSession() As Boolean
        Dim bRet As Boolean = False
        Try
            If clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then bRet = True
        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Error valid user session: " & ex.Message & "-" & ex.StackTrace)
            bRet = False
        End Try

        Return bRet
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function getSoTK_TruyVanCore(ByVal pv_strTienMat As String) As String
        Dim strReturn As String = ""
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If
        Dim objUser As CTuUser = New CTuUser(HttpContext.Current.Session("User"))
        Dim macn As String = objUser.MA_CN
        'strReturn = getSoTK_TienMat()
        Dim strSQL As String = "SELECT TAIKHOAN_TIENMAT FROM tcs_dm_chinhanh WHERE branch_code = '" & macn & "'"
        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            Return dt.Rows(0)("TAIKHOAN_TIENMAT").ToString
        Else
            Return ""
        End If
        Return strReturn
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function ValidNHSHB_SP(ByVal strSHKB As String) As String
        Dim strResult As String = ""
        Dim dt As New DataTable
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If
        strResult = cls_songphuong.ValiadSHB(strSHKB)
        Dim strNHB As String = ""
        If strResult.Length > 8 Then
            Dim strarr() As String
            strarr = strResult.Split(";")
            strNHB = strarr(0).ToString
        End If
        Return strNHB
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetMaDBHC(ByVal strSHKB As String) As String
        Dim strResult As String = ""
        Dim dt As New DataTable
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If
        strResult = cls_songphuong.ValiadSHB(strSHKB)
        Dim strSql As String = ""
        If strResult.Length > 0 Then
            strSql = "Select MA_DB  From TCS_DM_KHOBAC where shkb = '" & strSHKB & "'"
            dt = DataAccess.ExecuteToTable(strSql)
            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                Return dt.Rows(0)(0).ToString()
            Else
                Return ""
            End If
        End If

        Return strResult
    End Function
    '<System.Web.Services.WebMethod()> _
    'Public Shared Function Get_DataByCQT(ByVal sMaCQT As String, ByVal sMaHQ As String, ByVal sMaHQPH As String) As String
    '    If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
    '        Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
    '    End If

    '    Return Business_HQ.HaiQuan.HaiQuanController.GetDataByCQT(sMaCQT, sMaHQ, sMaHQPH)
    'End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_NNThue_HQ(ByVal sMaSoThue As String, ByVal sSoTK As String, ByVal sNgayDK As String) As String
        Dim sRet As String = String.Empty
        Dim sXMLPhanHoi As String = String.Empty
        Dim sError As String = String.Empty
        Dim sErrorDesc As String = String.Empty
        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Throw New Exception("errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại.")
            End If
            If String.IsNullOrEmpty(sSoTK) Then sSoTK = String.Empty
            If String.IsNullOrEmpty(sNgayDK) Then sNgayDK = String.Empty
            sXMLPhanHoi = CustomsServiceV3.ProcessMSG.getMSG12(sMaSoThue, sNgayDK, sSoTK, sError, sErrorDesc)
            'sXMLPhanHoi = "<?xml version='1.0' encoding='UTF-8'?><Customs><Header><Application_Name>Payment</Application_Name><Application_Version>3.0</Application_Version><Sender_Code>99999999</Sender_Code><Sender_Name>Tổng cục hải quan</Sender_Name><Message_Version>2.0</Message_Version><Message_Type>201</Message_Type><Message_Name>Thông điệp trả lời số thuế phải thu</Message_Name><Transaction_Date>2019-11-25T16:54:17</Transaction_Date><Transaction_ID>cfa3c7d6-cc27-423f-addd-be9011e2ce5f</Transaction_ID><Request_ID>63714f9b-6100-439b-b9a1-b49ee1c67df4</Request_ID></Header><Data><Item><Ma_Cuc>18</Ma_Cuc><Ten_Cuc>Cục Hải quan Bắc Ninh</Ten_Cuc><Ma_HQ_PH>18A3</Ma_HQ_PH><Ten_HQ_PH>Đội nghiệp vụ CC HQ Bắc Ninh</Ten_HQ_PH><Ma_HQ_CQT>2995100</Ma_HQ_CQT><Ma_DV>2300372796</Ma_DV><Ten_DV>CôNG TY Cổ PHầN  HANACANS</Ten_DV><Ma_Chuong>554</Ma_Chuong><Ma_HQ>18A3</Ma_HQ><Ten_HQ>Đội nghiệp vụ CC HQ Bắc Ninh</Ten_HQ><Ma_LH>A12</Ma_LH><Ten_LH>Nhập kinh doanh sản xuất</Ten_LH><Nam_DK>2019</Nam_DK><So_TK>10300536191</So_TK><Ma_NTK>1</Ma_NTK><Ten_NTK>TK Nộp ngân sách</Ten_NTK><Ma_LT>1</Ma_LT><Ma_HTVCHH>2</Ma_HTVCHH><Ten_HTVCHH>Không qua biên giới đất liền</Ten_HTVCHH><Ngay_DK>2019-11-25</Ngay_DK><Ma_KB>1111</Ma_KB><Ten_KB>VP KBNN Bắc Ninh</Ten_KB><TKKB>7111</TKKB><TTNo>1</TTNo><Ten_TTN>Nợ trong hạn</Ten_TTN><TTNo_CT>19</TTNo_CT><Ten_TTN_VT>TH</Ten_TTN_VT><DuNo_TO>3022199498</DuNo_TO><CT_No><LoaiThue>VA</LoaiThue><Khoan>000</Khoan><TieuMuc>1702</TieuMuc><DuNo>3022199498</DuNo></CT_No></Item><Item><Ma_Cuc>18</Ma_Cuc><Ten_Cuc>Cục Hải quan Bắc Ninh</Ten_Cuc><Ma_HQ_PH>18A3</Ma_HQ_PH><Ten_HQ_PH>Đội nghiệp vụ CC HQ Bắc Ninh</Ten_HQ_PH><Ma_HQ_CQT>2995100</Ma_HQ_CQT><Ma_DV>2300372796</Ma_DV><Ten_DV>CôNG TY Cổ PHầN  HANACANS</Ten_DV><Ma_Chuong>554</Ma_Chuong><Ma_HQ>18A3</Ma_HQ><Ten_HQ>Đội nghiệp vụ CC HQ Bắc Ninh</Ten_HQ><Ma_LH>A12</Ma_LH><Ten_LH>Nhập kinh doanh sản xuất</Ten_LH><Nam_DK>2019</Nam_DK><So_TK>10300536191</So_TK><Ma_NTK>2</Ma_NTK><Ten_NTK>TK Tạm gửi của cơ quan Hải quan</Ten_NTK><Ma_LT>11</Ma_LT><Ma_HTVCHH>2</Ma_HTVCHH><Ten_HTVCHH>Không qua biên giới đất liền</Ten_HTVCHH><Ngay_DK>2019-11-25</Ngay_DK><Ma_KB>1111</Ma_KB><Ten_KB>VP KBNN Bắc Ninh</Ten_KB><TKKB>3511</TKKB><TTNo>1</TTNo><Ten_TTN>Nợ trong hạn</Ten_TTN><TTNo_CT>11</TTNo_CT><Ten_TTN_VT>TH</Ten_TTN_VT><DuNo_TO>20000</DuNo_TO><CT_No><LoaiThue>KH</LoaiThue><Khoan>000</Khoan><TieuMuc>2663</TieuMuc><DuNo>20000</DuNo></CT_No></Item></Data><Error><ErrorNumber>0</ErrorNumber><ErrorMessage>Xử lý thành công</ErrorMessage></Error><Signature xmlns='http://www.w3.org/2000/09/xmldsig#'><SignedInfo><CanonicalizationMethod Algorithm='http://www.w3.org/TR/2001/REC-xml-c14n-20010315'></CanonicalizationMethod><SignatureMethod Algorithm='http://www.w3.org/2000/09/xmldsig#rsa-sha1'></SignatureMethod><Reference URI=''><Transforms><Transform Algorithm='http://www.w3.org/2000/09/xmldsig#enveloped-signature'></Transform></Transforms><DigestMethod Algorithm='http://www.w3.org/2000/09/xmldsig#sha1'></DigestMethod><DigestValue>NQWSuwUj5TAnJ/YaMlZjsqcGebQ=</DigestValue></Reference></SignedInfo><SignatureValue>RlqwDHvJLXycK0SJl+6JJgUuzqZgWSr5dxsNGky9gnGhUCROeU6wxDlBKWukx4uKhbST3XyI+Wk9QkuXVWm6GOufFDB0RXAXt2KFjiVGTFNsaNT99jhuNyCOCAcLa3zK8JgSDTHe2RM4bCmsRVCSGGCWNOxxrR+QmwIe/XBsttTDrEMCWzkcxz3JYjLYDQH4vbUjhqt+PKL8q40kOCbWhK7pW7SnJ2qcFUlu6eeP2JX6zy+T43lziCFbrjYhKM88zweQhfGIG5M7Fq/UKd4lCr448SFbFM5rID2HphaERtNWCOnPdGV+psvlPoRSxfc4HskDqGrbMSqpe4NY3+jESA==</SignatureValue><KeyInfo><X509Data><X509IssuerSerial><X509IssuerName>CN=Co quan chung thuc so Bo Tai chinh, O=Ban Co yeu Chinh phu, C=VN</X509IssuerName><X509SerialNumber>7017934</X509SerialNumber></X509IssuerSerial><X509Certificate>MIIFJjCCBA6gAwIBAgIDaxXOMA0GCSqGSIb3DQEBBQUAMFkxCzAJBgNVBAYTAlZOMR0wGwYDVQQKDBRCYW4gQ28geWV1IENoaW5oIHBodTErMCkGA1UEAwwiQ28gcXVhbiBjaHVuZyB0aHVjIHNvIEJvIFRhaSBjaGluaDAeFw0xNTAyMTMwODM1MTZaFw0yMDAyMTIwODM1MTZaMCoxDTALBgNVBAMMBFRDSFExDDAKBgNVBAoMA0JUQzELMAkGA1UEBhMCVk4wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCJ/DZItyf/ofpqDz9g15psnW7wfSlNFFzOPgRKt+aod1iwnTKk6mIFqqzio9TvYmeRBiIybw20YRLELtDcEoYGMzvox8wC7P+h4+fyPPZEShyHx5tBQToXuymCjWiKf66PMdJlExC97ux3Pk9HlultBeCkz2Oaf//ljtP7JeoJ1MnDeFUIsHOFKwbbGADYccivY2MarkKUegjDWQm4pCG7GQO1HoN68Pnmg15lWxl092oN+WVe0vK0U4ddDYnGSRU8JaGYUN5Ph4Tp6FMx7NLQ+nWy9dspRaZvgKNx1pN9RCP+rxVFvH6ep+qvBCADH1PiiDacccGC8gjgKHIBtl0FAgMBAAGjggIkMIICIDAJBgNVHRMEAjAAMAsGA1UdDwQEAwIGQDAfBglghkgBhvhCAQ0EEhYQVXNlciBTaWduIG9mIEJUQzAdBgNVHQ4EFgQUncGEujPodX9zvYCAjfP9KLx1djEwgZUGA1UdIwSBjTCBioAUnjia1imViWoFfyr/XwGXtFcwZrKhb6RtMGsxCzAJBgNVBAYTAlZOMR0wGwYDVQQKDBRCYW4gQ28geWV1IENoaW5oIHBodTE9MDsGA1UEAww0Q28gcXVhbiBjaHVuZyB0aHVjIHNvIGNodXllbiBkdW5nIENoaW5oIHBodSAoUm9vdENBKYIBAzAJBgNVHRIEAjAAMF8GCCsGAQUFBwEBBFMwUTAfBggrBgEFBQcwAYYTaHR0cDovL29jc3AuY2EuYnRjLzAuBggrBgEFBQcwAoYiaHR0cDovL2NhLmJ0Yy9wa2kvcHViL2NlcnQvYnRjLmNydDAwBglghkgBhvhCAQQEIxYhaHR0cDovL2NhLmJ0Yy9wa2kvcHViL2NybC9idGMuY3JsMDAGCWCGSAGG+EIBAwQjFiFodHRwOi8vY2EuYnRjL3BraS9wdWIvY3JsL2J0Yy5jcmwwXgYDVR0fBFcwVTAnoCWgI4YhaHR0cDovL2NhLmJ0Yy9wa2kvcHViL2NybC9idGMuY3JsMCqgKKAmhiRodHRwOi8vY2EuZ292LnZuL3BraS9wdWIvY3JsL2J0Yy5jcmwwDQYJKoZIhvcNAQEFBQADggEBACazYSs4oCC9klzaHCCNoJaR1D78QaI8cdm+k4hGrvq+LoL5Y7CbSFF0erj/2ycmto5zh3KVTV6mhfbzWxTmumEqrSB5BgHEmUKTSNAMbISxZlqv+q1ZZ4e08dXxKFwFQesPypVocJgGP0yYTJO9+zpN79od9s6fP25ZK0vTHQE98rt6pAeNnMUrJfqDAvUpCXPsAVHLkum8WbN0URQLHNCstPmCwxAZzravhETFmLXbO9/d0oTTZkxgFAfH5Zk49TBlNxPng9FVVOqFmMuU/F6hJmI7IQUwS7AM4JOCxBk+Ha3TfXDtYO/BwjtYByOFdHXqyohSgcHM+2m1tXcvV94=</X509Certificate></X509Data></KeyInfo></Signature></Customs>"
            sXMLPhanHoi = sXMLPhanHoi.Replace("<Customs>", "<Customs xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns=""http://www.cpandl.com"">")

            Dim msg201Convert As New CustomsV3.MSG.MSG201.MSG201()
            Dim oMSG201 As CustomsV3.MSG.MSG201.MSG201
            oMSG201 = msg201Convert.MSGToObject(sXMLPhanHoi)

            'For Each item As CustomsV3.MSG.MSG201.ItemData In oMSG201.Data.Item

            'Next
            'Sort object - for display group table html on the form:

            If oMSG201.Error.ErrorNumber <> "0" Then
                sRet = String.Empty
                Throw New Exception("HQ_Error:" & "ErrorNumber: " & oMSG201.Error.ErrorNumber & "--" & "ErrorMessage: " & oMSG201.Error.ErrorMessage)
            Else
                sRet = Newtonsoft.Json.JsonConvert.SerializeObject(oMSG201)
            End If

        Catch ex As Exception

            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Public Shared Function Get_NNThue_HQ:" + ex.Message)

            sRet = String.Empty

            If ex.Message.StartsWith("HQ_Error:") Then
                Throw New Exception("Lỗi trả về từ hải quan: " & ex.Message.Substring(9))
            Else
                'Throw ex
                Throw New Exception("Có lỗi trong quá trình lấy thông tin người nộp thuế")
            End If
        End Try

        Return sRet
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetCTU(ByVal sSoCT As String) As String
        Dim sRet As String = ""
        Dim sMaNV As String = ""
        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If

            sMaNV = HttpContext.Current.Session("User")

            Dim objCTU As Business.ChungTuSP.infChungTuSP = Nothing
            objCTU = Business.SP.SongPhuongController.GetChungTu(sSoCT)

            If objCTU IsNot Nothing Then
                'modify something before serialize to json object
                If objCTU.HDR IsNot Nothing AndAlso Not String.IsNullOrEmpty(objCTU.HDR.Ngay_HT) AndAlso objCTU.HDR.Ngay_HT.Length >= 10 Then
                    objCTU.HDR.Ngay_HT = objCTU.HDR.Ngay_HT.Substring(0, 10)
                End If
                If objCTU.ListDTL IsNot Nothing AndAlso objCTU.ListDTL.Count > 0 Then
                    For Each DTL In objCTU.ListDTL
                        If Not String.IsNullOrEmpty(DTL.NGAY_TK) AndAlso DTL.NGAY_TK.Length >= 10 Then
                            DTL.NGAY_TK = DTL.NGAY_TK.Substring(0, 10)
                        End If
                    Next
                End If

                sRet = Newtonsoft.Json.JsonConvert.SerializeObject(objCTU)
            End If
        Catch ex As Exception
            sRet = String.Empty
            Throw New Exception("Có lỗi trong quá trình lấy thông tin chứng từ. " & ex.Message)
        End Try

        Return sRet
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Ghi_CTu(ByVal ctuHDR As Business_HQ.NewChungTu.infChungTuHDR, ByVal arrDTL() As Business_HQ.NewChungTu.infChungTuDTL, ByVal sAction As String, ByVal sSaveType As String) As String
        Dim sMaNV As String = ""
        Dim sMa_DThu As String = ""
        Dim sRet As String = String.Empty
        Try
            If HttpContext.Current.Session("User") Is Nothing Then
                sRet = ""
                Throw New Exception("errUserSession;Bạn hãy thoát khỏi chương trình và đăng nhập lại")
            End If
            'If Not ValidUserSession() Then
            '    sRet = "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            '    Throw New Exception(sRet)
            'End If

            sMaNV = HttpContext.Current.Session("User")
            sMa_DThu = Business_HQ.HaiQuan.HaiQuanController.GetMaDiemThu(sMaNV)

            If String.IsNullOrEmpty(sMaNV) Then
                sRet = "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
                Throw New Exception(sRet)
            End If

            Dim listDTL As New List(Of Business_HQ.NewChungTu.infChungTuDTL)
            If arrDTL IsNot Nothing AndAlso arrDTL.Length > 0 Then
                listDTL.AddRange(arrDTL)
            End If
            For Each DTL In listDTL
                DTL.Ma_DThu = sMa_DThu
            Next



            If ctuHDR IsNot Nothing Then
                ctuHDR.Ma_DThu = sMa_DThu
                ctuHDR.Ma_NV = CInt(sMaNV) 'get current user login
                sRet = Business_HQ.HaiQuan.HaiQuanController.GhiChungTu(ctuHDR, listDTL, sAction, sSaveType)
            End If

        Catch ex As Exception
            Select Case sAction
                Case "GHI"
                    log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Lỗi trong quá trình hoàn thiện chứng từ" & ex.Message & "-" & ex.StackTrace)
                    Throw ex
                Case "GHIKS"
                    log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Lỗi trong quá trình hoàn thiện & gửi kiểm soát chứng từ" & ex.Message & "-" & ex.StackTrace)
                    Throw ex
                Case "GHIDC"
                    log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Lỗi trong quá trình điều chỉnh & gửi kiểm soát chứng từ" & ex.Message & "-" & ex.StackTrace)
                    Throw ex
                Case Else
                    Throw ex
            End Select
        End Try

        Return sRet
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Huy_CTU(ByVal sSoCT As String, ByVal sLyDo As String) As String
        Dim sRet As String = String.Empty
        Dim sMaNV As String = String.Empty
        Try
            If HttpContext.Current.Session("User") Is Nothing Then
                sRet = ""
                Throw New Exception("errUserSession;Bạn hãy thoát khỏi chương trình và đăng nhập lại")
            End If

            sMaNV = HttpContext.Current.Session("User")

            sRet = Business.SP.SongPhuongController.HuyChungTu(sSoCT, sLyDo, sMaNV)
        Catch ex As Exception
            sRet = ""
            Throw New Exception(ex.Message)
        End Try

        Return sRet
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function LoadCTList(ByVal sMaNV As String, ByVal sSoCtu As String) As String
        Dim dt As DataTable = Nothing
        Dim sTable As String = ""
        Try
            If HttpContext.Current.Session("User") Is Nothing Then
                sTable = ""
                Throw New Exception("errUserSession;Bạn hãy thoát khỏi chương trình và đăng nhập lại")
            End If

            If sSoCtu IsNot Nothing Then
                If sSoCtu.Length > 0 Then
                    dt = Business.SP.SongPhuongController.GetSoCtu(sMaNV, sSoCtu, "T")
                Else
                    dt = mdlCommon.DataSet2Table(Business.SP.SongPhuongController.GetListChungTu(clsCTU.TCS_GetNgayLV(sMaNV), sMaNV, clsCTU.TCS_GetMaChiNhanh(sMaNV)))
                End If
            Else
                dt = mdlCommon.DataSet2Table(Business.SP.SongPhuongController.GetListChungTu(clsCTU.TCS_GetNgayLV(sMaNV), sMaNV, clsCTU.TCS_GetMaChiNhanh(sMaNV)))
            End If

            sTable &= "<table class='grid_data' cellspacing='0' rules='all' border='1' id='grdDSCT' style='width:100%;border-collapse:collapse;'>"

            Dim i As Integer = 0
            For Each dr As DataRow In dt.Rows
                sTable &= "<tr onclick=jsGetCTU('" & dr("SO_CT").ToString & "') " & "class='" & IIf(i Mod 2 = 0, "grid_item", "grid_item_alter") & "'>"
                sTable &= "<td align='center' style='width: 30%'><a href='#'>" & dr("SO_CT").ToString & "</a></td>"
                sTable &= "<td align='center' style='width: 30%'>" & dr("so_ref_core").ToString & "</td>"
                sTable &= "<td align='center' style='width: 40%'>" & Business.ChungTuSP.buChungTu.Get_TrangThai_Citad(dr("TRANG_THAI").ToString, dr("tt_sp").ToString) & "</td>"
                sTable &= "</tr>"
            Next

            sTable &= "</table>"

            sTable &= "<table cellspacing='0' rules='all' border='1' style='width:100%;border-collapse:collapse;'>"
            sTable &= "     <tr style='height: 24px;padding-right:2px;'><td align='right' colspan='3'>Tổng số chứng từ: <span style='font-weight:bold'>" & dt.Rows.Count & "</span></td></tr>"
            sTable &= "</table>"

        Catch ex As Exception
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Lỗi dữ liệu chi tiết chứng từ" & ex.Message & "-" & ex.StackTrace)
            sTable = String.Empty
        End Try

        Return sTable
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTK_KH_NH(ByVal pv_strTKNH As String) As String
        Dim sRet As String = String.Empty
        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If

            Dim ma_loi As String = ""
            Dim so_du As String = ""
            Dim branch_tk As String = ""
            sRet = cls_corebank.GetTK_KH_NH(pv_strTKNH.Replace("-", ""), ma_loi, so_du, branch_tk)
            Return sRet
        Catch ex As Exception
            sRet = String.Empty
            Throw ex
        End Try

        Return sRet
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTEN_KH_NH(ByVal pv_strTKNH As String) As String
        Dim strReturn As String = ""
        Dim ma_loi As String = ""
        Dim so_du As String = ""
        'If CORE_ON_OFF = "1" Then
        strReturn = cls_corebank.GetTen_TK_KH_NH(pv_strTKNH.Replace("-", ""))
        'Else
        '    strReturn = "Tài khoản test"
        'End If
        Return strReturn
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenQuanHuyen(ByVal pv_strMaNV As String) As String
        Return clsCTU.Get_TenQuanHuyen(pv_strMaNV)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenTinh(ByVal pv_strMaNV As String) As String
        Return clsCTU.Get_TenTinhTP(pv_strMaNV)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTenDBHC(ByVal pv_strMaNV As String) As String
        Return clsCTU.Get_TenDBHC(pv_strMaNV)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenNNT(ByVal sMaNNT As String) As String
        Dim sRet As String = String.Empty
        Try
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If

            sRet = Business_HQ.HaiQuan.HaiQuanController.GetTenNguoiNopThue(sMaNNT)
        Catch ex As Exception
            sRet = String.Empty
        End Try

        Return sRet
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function Load_TenCQT(ByVal sMaCQT As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Return Business_HQ.HaiQuan.HaiQuanController.LoadTenCQT(sMaCQT)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenMaHQ(ByVal sMaHQ As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Return Business_HQ.HaiQuan.HaiQuanController.GetDataByMaHQ(sMaHQ)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_DataByCQT(ByVal sMaCQT As String, ByVal sMaHQ As String, ByVal sMaHQPH As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Return Business_HQ.HaiQuan.HaiQuanController.GetDataByCQT(sMaCQT, sMaHQ, sMaHQPH)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetData_SHKB(ByVal sLoaiDM As String, ByVal sSHKB As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Return Business_HQ.HaiQuan.HaiQuanController.GetDataBySHKB(sLoaiDM, sSHKB)
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTT_NH(ByVal sSHKB As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Return Business_HQ.HaiQuan.HaiQuanController.GetTT_NganHang(sSHKB)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetTTTinhThanhKB(ByVal sSHKB As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Return Business_HQ.HaiQuan.HaiQuanController.GetTTTinhThanhKB(sSHKB)
    End Function
    <System.Web.Services.WebMethod()> _
    Public Shared Function onBlurMaTieuMuc(ByVal v_maTieuMuc As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Dim v_dt As DataTable = Nothing

        v_dt = Business_HQ.HaiQuan.HaiQuanController.onBlurMaTieuMuc(v_maTieuMuc)

        If (v_dt IsNot Nothing) Then
            If (v_dt.Rows.Count > 0) Then
                Return Newtonsoft.Json.JsonConvert.SerializeObject(v_dt)
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetFee(ByVal pProvince As String, ByVal accountNo As String, ByVal Type_Tranfer As String, ByVal Sender_bank_code As String, ByVal Sender_bank_name As String, ByVal cust_ID As String, ByVal ben_account As String, ByVal ben_name_id As String, ByVal ben_bank_id As String, ByVal Ben_bank_id_name As String, ByVal ben_bank_id_2 As String, ByVal Ben_bank_id_2_name As String, ByVal Related_cust As String, ByVal Amount As String, ByVal currency As String) As String
        Dim strMaCN As String = HttpContext.Current.Session("MA_CN_USER_LOGON")
        Dim strMaNV As String = HttpContext.Current.Session("UserName")
        Dim strTellerID As String = HttpContext.Current.Session.Item("TELLER_ID")
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "ssF;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            ' System.Web.HttpContext.Current.Response.Redirect("~/pages/frmLogin.aspx", True)
        End If

        'Dim v_coreUtils As New CoreBankSV.ClsCoreBank

        Return "" 'v_coreUtils.Fee(pProvince, accountNo, Type_Tranfer, Sender_bank_code, Sender_bank_name, cust_ID, ben_account, ben_name_id, ben_bank_id, Ben_bank_id_name, ben_bank_id_2, Ben_bank_id_2_name, strTellerID, Related_cust, Amount, currency)
    End Function


    <System.Web.Services.WebMethod()> _
    Public Shared Function getNoiDungByMaTieuMuc(ByVal v_jSon As String) As String
        If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
            Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
        End If

        Dim v_dt As DataTable = Nothing

        v_dt = Business_HQ.HaiQuan.HaiQuanController.getNoiDungByMaTieuMuc(v_jSon)

        If (v_dt IsNot Nothing) Then
            If (v_dt.Rows.Count > 0) Then
                Return Newtonsoft.Json.JsonConvert.SerializeObject(v_dt)
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function

    <System.Web.Services.WebMethod()> _
    Public Shared Function Ghi_CTu2(ByVal ctuHdr As Business.SongPhuongObj.HdrSp, ByVal ctuDtl() As Business.SongPhuongObj.Dtl, _
                                    ByVal sAction As String, ByVal sSaveType As String) As String
        Dim sMaNV As String = ""
        'Dim sMa_DThu As String = ""
        Dim sRet As String = ""
        Try
            If HttpContext.Current.Session("User") Is Nothing Then
                Return "errUserSession;Bạn hãy thoát khỏi chương trình và đăng nhập lại"
            End If
            sMaNV = HttpContext.Current.Session("User")
            'sMa_DThu = Business.SP.SongPhuongController.GetMaDiemThu(sMaNV)

            If String.IsNullOrEmpty(sMaNV) Then
                Return "errUserSession;User của bạn đang đăng nhập trên một máy khác hoặc bạn không có quyền thao tác chức năng này. Vui lòng liên hệ quản trị hệ thống hoặc hãy thoát khỏi chương trình và đăng nhập lại."
            End If
            If ctuHdr IsNot Nothing Then
                Dim strTrangThai As String = ""
                Dim sMaCN As String = "13.2"
                If Business.SP.SongPhuongController.checkPhanQuyen(sMaNV, sMaCN) Then
                    ctuHdr.MA_NV = CInt(sMaNV) 'get current user login
                    ctuHdr.MA_CN = HttpContext.Current.Session("MA_CN_USER_LOGON")
                    ctuHdr.NGAY_KH_NH = IIf(ctuHdr.NGAY_KH_NH Is Nothing, Date.Now.ToString("dd/MM/yyyy"), ctuHdr.NGAY_KH_NH)
                    sRet = Business.SP.SongPhuongController.GhiChungTu2(ctuHdr, ctuDtl, sAction, sSaveType)
                Else
                    Return "errUserSession;Bạn không được phân quyền vào chức năng này"
                End If

               
            End If
        Catch ex As Exception
            Select Case sAction
                Case "GHI"
                    log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Lỗi trong quá trình hoàn thiện chứng từ" & ex.Message & "-" & ex.StackTrace)
                Case "GHIKS"
                    log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Lỗi trong quá trình hoàn thiện & gửi kiểm soát chứng từ" & ex.Message & "-" & ex.StackTrace)
                Case "GHIDC"
                    log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType).Error("Lỗi trong quá trình điều chỉnh & gửi kiểm soát chứng từ" & ex.Message & "-" & ex.StackTrace)
            End Select
            sRet = "Err;" & ex.Message
        End Try
        Return sRet
    End Function

    Public Shared Function convertDate(ByVal strText As String) As String
        Dim strDay As String, strMonth As String, strYear As String
        strDay = strText.Substring(6, 2)
        strMonth = strText.Substring(4, 2)
        strYear = strText.Substring(0, 4)
        Return Convert.ToString((Convert.ToString(strDay & Convert.ToString("/")) & strMonth) + "/") & strYear
    End Function

    Public Class TK_KH_NH
        Public Property Account() As String
            Get
                Return m_Account
            End Get
            Set(ByVal value As String)
                m_Account = value
            End Set
        End Property
        Private m_Account As String
        Public Property Balance() As String
            Get
                Return m_Balance
            End Get
            Set(ByVal value As String)
                m_Balance = value
            End Set
        End Property
        Private m_Balance As String
        Public Property DescAccount() As String
            Get
                Return m_DescAccount
            End Get
            Set(ByVal value As String)
                m_DescAccount = value
            End Set
        End Property
        Private m_DescAccount As String
    End Class

End Class

