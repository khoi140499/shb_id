﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmChiTietTraCuuBL.aspx.vb" Inherits="SP_frmChiTietTraCuuBL" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Chi tiết thông tin</title>
    <link href="../css/myStyles.css" type="text/css" rel="stylesheet" />
    <link href="../css/menuStyle.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .txtHoanThien80 {
            max-width: 400px;
            width: 80%;
        }

        .txtHoanThien {
            width: 400px;
        }

        .txtHoanThien01 {
            width: 557px;
        }

        .container {
            width: 100%;
            padding-bottom: 50px;
        }

        .dataView {
            padding-bottom: 10px;
        }

        .number {
            text-align: right;
        }

        .formx {
            color: #E7E7FF;
            background: #eaedf0;
            padding-left: 1px;
            color: Black;
        }

        .ButtonCommand {
            margin-left: 371px;
        }

        .auto-style1 {
            width: 509px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div class="container">
            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                <tr>
                    <td colspan="4" width='100%' align="right">
                        <table width="100%">
                            <tr class="grid_header">
                                <td height="15px" align="left">
                                    <%--Trường <span style="color: #FF0000; font-weight: bold">(*)</span> là bắt buộc nhập--%>
                                                                            THÔNG TIN CHỨNG TỪ 
                                </td>
                            </tr>
                        </table>
                        <div id="Panel1" style="/*height: 380px; */ width: 100%; /*overflow-y: scroll; */">
                            <table runat="server" id="grdHeader" cellspacing="0" cellpadding="1" rules="all"
                                border="1" style="border-color: #989898; border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse; margin-left: 2px;"
                                class="tblHeader">
                                <tr id="rowMaNNT">
                                    <td>
                                        <b>Mã số thuế </b>
                                    </td>
                                    <td width="25%">
                                        <asp:TextBox runat="server" ID="txtMaNNT" CssClass="inputflat3" ReadOnly="true" BackColor="Aqua"
                                            Width="98%" />
                                    </td>
                                    <td>
                                        <b>Tên NNT </b>
                                    </td>
                                    <td width="27%">
                                        <asp:TextBox runat="server" ID="txtTenNNT" CssClass="inputflat" ReadOnly="true" BackColor="Aqua" Width="98%" />
                                    </td>
                                </tr>

                                <tr id="rowDChiNNT">
                                    <td>
                                        <b>Địa chỉ người nộp thuế</b>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox runat="server" ID="txtDChiNNT" CssClass="inputflat" ReadOnly="true"
                                            Width="100%" />
                                    </td>
                                </tr>
                                <tr id="rowQuanHuyenNNThue">
                                    <td>
                                        <b>Huyện người nộp thuế</b>
                                    </td>
                                    <td width="25%">
                                        <asp:TextBox runat="server" ID="txtHuyenNNThue" CssClass="inputflat3" ReadOnly="true"
                                            Width="98%" />
                                    </td>
                                    <td>
                                        <b>Tỉnh người nộp thuế</b>
                                    </td>
                                    <td width="27%">
                                        <asp:TextBox runat="server" ID="txtTinhNNThue" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                    </td>
                                </tr>
                                <tr id="rowHTTT">
                                    <td>
                                        <b>Hình thức thanh toán</b>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlHTTT" CssClass="inputflat" Enabled="false" Width="100%">
                                            <asp:ListItem Value="01" Text="Chuyển khoản" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="05" Text="Tiền mặt"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <b>Mã nguyên tệ</b>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlMaNT" CssClass="inputflat" Enabled="false" BackColor="Aqua"
                                            Width="98%">
                                            <asp:ListItem Value="VND" Text="VND" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="USD" Text="USD"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="rowTKChuyen">
                                    <td>
                                        <b>Số tài khoản chuyển </b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTK_KH_NH" CssClass="inputflat" ReadOnly="true"
                                            Width="98%" />
                                    </td>
                                    <td>
                                        <b>Tên tài khoản Chuyển </b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTenTK_KH_NH" CssClass="inputflat" ReadOnly="true"
                                            Width="98%" />
                                        <img id="img1" alt="Chọn từ danh mục đặc biệt" src="../../images/icons/MaDB.png"
                                            onclick="jsShowCustomerSignature();" style="width: 16px; height: 16px; display: none" />
                                    </td>
                                </tr>
                                <tr id="rowSoDuNH">
                                    <td>
                                        <b>Số dư khả dụng</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtSoDu_KH_NH" CssClass="inputflat" ReadOnly="true"
                                            Width="98%" />
                                    </td>
                                    <td>
                                        <b>Số biên lai</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtSoBienLai" CssClass="inputflat" ReadOnly="true"
                                            Width="98%" />
                                    </td>
                                </tr>
                                <tr id="rowNNTien">
                                    <td>
                                        <b>Mã người nộp tiền</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtMaNNTien" CssClass="inputflat" ReadOnly="true"
                                            Width="99%" />
                                    </td>
                                    <td>
                                        <b>Tên người nộp tiền</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTenNNTien" CssClass="inputflat" ReadOnly="true"
                                            Width="99%" />
                                    </td>
                                </tr>
                                <tr id="rowDChiNNTien">
                                    <td>
                                        <b>Địa chỉ người nộp tiền</b>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox runat="server" ID="txtDChiNNTien" CssClass="inputflat" ReadOnly="true"
                                            Width="99%" />
                                    </td>

                                </tr>
                                <tr id="rowQuanHuyenNNTien">
                                    <td>
                                        <b>Huyện người nộp tiền</b>
                                    </td>
                                    <td width="25%">
                                        <asp:TextBox runat="server" ID="txtHuyenNNtien" CssClass="inputflat3" ReadOnly="true"
                                            Width="98%" />
                                    </td>
                                    <td>
                                        <b>Tỉnh người nộp tiền</b>
                                    </td>
                                    <td width="27%">
                                        <asp:TextBox runat="server" ID="txtTinhNNTien" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                    </td>
                                </tr>
                                <tr id="rowSHKB">
                                    <td>
                                        <b>Số hiệu kho bạc </b>
                                    </td>
                                    <td width="25%">
                                        <asp:TextBox runat="server" ID="txtSHKB" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                            Width="98%" />
                                    </td>
                                    <td>
                                        <b>Tên kho bạc </b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTenKB" CssClass="inputflat" ReadOnly="true" Width="98%" />
                                    </td>
                                </tr>
                                <tr id="rowCQThu">
                                    <td>
                                        <b>Mã cơ quan thu</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtMaCQThu" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                            Width="98%" />
                                    </td>
                                    <td>
                                        <b>Tên cơ quan thu</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTenCQThu" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                            Width="98%" />
                                    </td>
                                </tr>
                                <tr id="rowMaLThue">
                                    <td>
                                        <b>Mã loại thuế</b>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlMaLThue" CssClass="inputflat" Enabled="false" BackColor="Aqua"
                                            Width="98%">
                                            <asp:ListItem Value="03" Text="03_Khoản thu do cơ quan khác quản lý" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <b>Hình thức thu</b>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlHT_Thu" CssClass="inputflat" Enabled="false" Width="100%">
                                            <asp:ListItem Value="1" Text="Thu phạt" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Thu phí"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="Thu thuế"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="rowTKNo">
                                    <td>
                                        <b>Tên tài khoản có</b>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlMa_NTK" CssClass="inputflat" Enabled="false" Width="100%">
                                            <asp:ListItem Value="1" Text="Tài khoản thu NSNN" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Tài khoản tạm thu"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="Thu hoàn thuế GTGT"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <b>Tài khoản có</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTKCo" CssClass="inputflat" ReadOnly="true" BackColor="Aqua" Width="98%" />
                                    </td>

                                </tr>
                                <tr id="rowNhanHangTT">
                                    <td>
                                        <b>Mã NH trực tiếp </b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtMA_NH_TT" CssClass="inputflat" ReadOnly="true"
                                            Width="98%" />
                                    </td>
                                    <td>
                                        <b>Tên NH trực tiếp </b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTEN_NH_TT" CssClass="inputflat" ReadOnly="true"
                                            Width="98%" />
                                    </td>
                                </tr>
                                <tr id="rowNganHangNhan">
                                    <td>
                                        <b>Mã NH gián tiếp </b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtMA_NH_B" CssClass="inputflat" ReadOnly="true"
                                            Width="98%" />
                                    </td>
                                    <td>
                                        <b>Tên NH gián tiếp </b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTen_NH_B" CssClass="inputflat" ReadOnly="true"
                                            Width="98%" />
                                    </td>
                                </tr>
                                <tr id="rowTTTien">
                                    <td>
                                        <b>Tổng tiền của CT</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTTienVND" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                            Width="98%" />
                                    </td>
                                    <td>
                                        <b>Kênh CT</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtProduct" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                            Width="98%" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr style="visibility: hidden">
                    <td width="25%" style="height: 0px">
                        <asp:TextBox runat="server" ID="txtSo_BT" CssClass="inputflat" ReadOnly="true" Visible="false"
                            Width="99%" />
                        <asp:TextBox runat="server" ID="txtSoCT" CssClass="inputflat" ReadOnly="true" Visible="false"
                            Width="99%" />
                        <asp:TextBox runat="server" ID="txtSoCT_NH" CssClass="inputflat" ReadOnly="true"
                            Visible="false" Width="99%" />
                    </td>
                    <td width="51%" style="height: 0px">
                        <asp:TextBox runat="server" ID="txtTRANG_THAI" CssClass="inputflat" ReadOnly="true"
                            Visible="false" Width="99%" />
                        <asp:TextBox runat="server" ID="txtMA_NV" CssClass="inputflat" ReadOnly="true" Visible="false"
                            Width="99%" />
                    </td>
                </tr>
                <tr>
                    <td colspan='2' align="left" class="errorMessage">
                        <table width="100%">
                            <tr>
                                <td width="100%" align="right">
                                    <asp:Label ID="lbSoFT" runat="server" Style="font-weight: bold;" Visible="false"></asp:Label>
                                    / 
                                                                            <asp:Label ID="lbSoCT" runat="server" Style="font-weight: bold;" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width='99%' colspan="2">
                                    <asp:UpdateProgress ID="updProgress" runat="server">
                                        <ProgressTemplate>
                                            <div style="background-color: Aqua;">
                                                <b style="font-size: 15pt">Đang lấy dữ liệu tại server.Xin chờ một lát...</b>
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <b>
                                        <asp:Literal ID="lblStatus" runat="server"></asp:Literal></b>
                                </td>
                            </tr>
                            <tr class="grid_header">
                                <td height="15px" align="left">CHI TIẾT CHỨNG TỪ 
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" width="100%">
                                    <asp:DataGrid ID="grdChiTiet" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                        ShowHeader="true" Width="100%" CssClass="grid_data">
                                        <HeaderStyle CssClass="grid_header"></HeaderStyle>
                                        <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                                        <ItemStyle CssClass="grid_item" />
                                        <Columns>
                                            <asp:TemplateColumn Visible="false">
                                                <HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkRemove" runat="server" Enabled="false" Checked="true" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Mã chương" ItemStyle-HorizontalAlign="Center">
                                                <HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="MA_CHUONG" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                        Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_CHUONG") %>'
                                                        Font-Bold="true" MaxLength="3" ToolTip="Ấn phím Enter để chọn mã chương"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Mã NDKT" ItemStyle-HorizontalAlign="Center">
                                                <HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="MA_MUC" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                        Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_TMUC") %>'
                                                        Font-Bold="true" MaxLength="4" ToolTip="Ấn phím Enter để chọn mã tiểu mục" AutoPostBack="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Nội dung" ItemStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="Center" Width="150px"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="NOI_DUNG" runat="server" Width="97%" BorderColor="white" CssClass="inputflat"
                                                        ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "NOI_DUNG") %>'
                                                        MaxLength="200" Font-Bold="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Kỳ thuế" ItemStyle-HorizontalAlign="Right">
                                                <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="KY_THUE" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                        Style="text-align: right" ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "KY_THUE") %>'
                                                        Font-Bold="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Số tiền VND" ItemStyle-HorizontalAlign="Right">
                                                <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="right" VerticalAlign="Middle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="SOTIEN" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                                        Style="text-align: right" ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "SOTIEN")%>'
                                                        AutoPostBack="true" onfocus="this.select()" Font-Bold="true">
                                                    </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" width='100%' align="right">
                        <table width="100%">
                            <tr class="grid_header">
                                <td height="15px" align="left">THÔNG TIN CHỨNG TỪ BIÊN LAI
                                </td>
                            </tr>
                        </table>
                        <div id="Div1" style="/*height: 380px; */ width: 100%; /*overflow-y: scroll; */">
                            <table runat="server" id="Table1" cellspacing="0" cellpadding="1" rules="all"
                                border="1" style="border-color: #989898; border-style: solid; font-family: Verdana; font-size: 8pt; width: 100%; border-collapse: collapse; margin-left: 2px;"
                                class="tblHeader">
                                <tr id="rowCQQD">
                                    <td>
                                        <b>Mã CQQD</b>
                                    </td>
                                    <td width="25%">
                                        <asp:TextBox runat="server" ID="txtMa_CQQD" CssClass="inputflat3" ReadOnly="true" BackColor="Aqua"
                                            Width="98%" />
                                    </td>
                                    <td>
                                        <b>Tên CQQD</b>
                                    </td>
                                    <td width="27%">
                                        <asp:TextBox runat="server" ID="txtTen_CQQD" CssClass="inputflat" ReadOnly="true" BackColor="Aqua" Width="98%" />
                                    </td>
                                </tr>
                                <tr id="rowLHThu">
                                    <td>
                                        <b>Mã loại hình thu</b>
                                    </td>
                                    <td width="25%">
                                        <asp:TextBox runat="server" ID="txtMaLHThu" CssClass="inputflat3" ReadOnly="true" BackColor="Aqua"
                                            Width="98%" />
                                    </td>
                                    <td>
                                        <b>Tên loại hình thu</b>
                                    </td>
                                    <td width="27%">
                                        <asp:TextBox runat="server" ID="txtTenLHTHU" CssClass="inputflat" ReadOnly="true" BackColor="Aqua" Width="98%" />
                                    </td>
                                </tr>

                                <tr id="rowTTQD">
                                    <td>
                                        <b>Số quyết định</b>
                                    </td>
                                    <td width="25%">
                                        <asp:TextBox runat="server" ID="txtSoQD" CssClass="inputflat3" ReadOnly="true" BackColor="Aqua"
                                            Width="98%" />
                                    </td>
                                    <td>
                                        <b>Ngày quyết định</b>
                                    </td>
                                    <td width="27%">
                                        <asp:TextBox runat="server" ID="txtNgay_QD" CssClass="inputflat" ReadOnly="true" BackColor="Aqua" Width="98%" />
                                    </td>
                                </tr>
                                <tr id="rowVPHC">
                                    <td>
                                        <b>Tổng tiền VPHC</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTTien_VPHC" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                            Width="98%" />
                                    </td>
                                    <td>
                                        <b>Lý do nộp phạt VPHC</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtLyDo_VPHC" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                            Width="98%" />
                                    </td>
                                </tr>
                                <tr id="rowTTienBL">
                                    <td>
                                        <b>Tổng tiền nộp chậm</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTTien_nop_cham" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                            Width="98%" />
                                    </td>
                                    <td>
                                        <b>Tổng tiền</b>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtTTien_CTBL" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                                            Width="98%" />
                                    </td>
                                </tr>
                                <tr id="rowDienGiai">
                                    <td>
                                        <b>Diễn giải</b>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox runat="server" ID="txtDienGiai" Style="width: 100%;" CssClass="inputflat"
                                            MaxLength="4000" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <asp:HiddenField ID="hdfSo_CT" runat="server" Value="" />
            <table width="100%">
                <tr align="right">
                    <td align="right" valign="top" colspan="3">Lý do chuyển trả
                            <asp:TextBox ID="txtLyDoCTra" runat="server" CssClass="inputflat" Width="50%"></asp:TextBox>
                    </td>
                </tr>
                <tr align="right">
                    <td align="right" valign="top" colspan="3">Lý do hủy
                            <asp:TextBox ID="txtLyDoHuy" runat="server" CssClass="inputflat" Width="50%"></asp:TextBox>
                    </td>
                </tr>
            </table>

        </div>
        <div>
            <table width="100%">
                <tr align="center">
                    <td align="center">

                        <asp:Button ID="btnIn" runat="server" Text="In Chứng từ" UseSubmitBehavior="false" />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnInBL" runat="server" Text="In Biên Lai" UseSubmitBehavior="false" />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnInBangKe" runat="server" Text="In Bảng kê" UseSubmitBehavior="false" />
                    </td>

                </tr>
            </table>
        </div>
    </form>

</body>
</html>
