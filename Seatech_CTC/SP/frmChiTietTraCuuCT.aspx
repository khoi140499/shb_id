﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmChiTietTraCuuCT.aspx.vb" Inherits="SP_frmChiTietTraCuuCT" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Chi tiết thông tin</title>
    <link href="../css/myStyles.css" type="text/css" rel="stylesheet" />
    <link href="../css/menuStyle.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        .txtHoanThien80 {
            max-width: 400px;
            width: 80%;
        }

        .txtHoanThien {
            width: 400px;
        }

        .txtHoanThien01 {
            width: 557px;
        }

        .container {
            width: 100%;
            padding-bottom: 50px;
        }

        .dataView {
            padding-bottom: 10px;
        }

        .number {
            text-align: right;
        }

        .formx {
            color: #E7E7FF;
            background: #eaedf0;
            padding-left: 1px;
            color: Black;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div style="text-align: right; color: Red">Số CT:<asp:Label runat="server" ID="lblSoCTU"></asp:Label></div>
            <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
                <tr id="rowTTCITAD">
                    <td>
                        <b>Ngày Citad nhận </b>
                    </td>
                    <td width="25%">
                        <asp:TextBox runat="server" ID="txtNgayCitad" CssClass="inputflat3" ReadOnly="true" BackColor="Aqua"
                            Width="98%" />
                    </td>
                    <td>
                        <b>Số bút toán trên Citad </b>
                    </td>
                    <td width="27%">
                        <asp:TextBox runat="server" ID="txtSoBTCitad" CssClass="inputflat" ReadOnly="true" BackColor="Aqua" Width="98%" />
                    </td>
                </tr>
                <tr id="rowMaNNT">
                    <td>
                        <b>Mã số thuế </b>
                    </td>
                    <td width="25%">
                        <asp:TextBox runat="server" ID="txtMaNNT" CssClass="inputflat3" ReadOnly="true" BackColor="Aqua"
                            Width="98%" />
                    </td>
                    <td>
                        <b>Tên NNT </b>
                    </td>
                    <td width="27%">
                        <asp:TextBox runat="server" ID="txtTenNNT" CssClass="inputflat" ReadOnly="true" BackColor="Aqua" Width="98%" />
                    </td>
                </tr>

                <tr id="rowDChiNNT">
                    <td>
                        <b>Địa chỉ người nộp thuế</b>
                    </td>
                    <td colspan="3">
                        <asp:TextBox runat="server" ID="txtDChiNNT" CssClass="inputflat" ReadOnly="true"
                            Width="100%" />
                    </td>
                </tr>
                <tr id="rowNNTien">
                    <td>
                        <b>Mã người nộp tiền</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtMaNNTien" CssClass="inputflat" ReadOnly="true"
                            Width="99%" />
                    </td>
                    <td>
                        <b>Tên người nộp tiền</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTenNNTien" CssClass="inputflat" ReadOnly="true"
                            Width="99%" />
                    </td>
                </tr>
                <tr id="rowDChiNNTien">
                    <td>
                        <b>Địa chỉ người nộp tiền</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtDChiNNTien" CssClass="inputflat" ReadOnly="true"
                            Width="98%" />
                    </td>
                    <td>
                        <b>Ngày NNT nộp tiền</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtNgayNNT" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                            Width="98%" />
                    </td>
                </tr>
                <tr id="rowSHKB">
                    <td>
                        <b>Số hiệu kho bạc </b>
                    </td>
                    <td width="25%">
                        <asp:TextBox runat="server" ID="txtSHKB" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                            Width="98%" />
                    </td>
                    <td>
                        <b>Tên kho bạc </b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTenKB" CssClass="inputflat" ReadOnly="true" Width="98%" />
                    </td>
                </tr>
                <tr id="rowNgayKBDThu">
                    <td>
                        <b>Ngày kho bạc</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtNgayKB" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                            Width="99%" />
                    </td>
                    <td>
                        <b>Mã điểm thu</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtMaDThu" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                            Width="99%" />
                    </td>
                </tr>
                <tr id="rowBTNgayCT">
                    <td>
                        <b>Số bút toán</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtSoBT" CssClass="inputflat3" ReadOnly="true" BackColor="Aqua"
                            Width="98%" />
                    </td>
                    <td>
                        <b>Ngày chứng từ</b>
                    </td>
                    <td>
                        <asp:TextBox ID="txtNgayChungTu" runat="server" Width="98%" BackColor="Aqua" CssClass="inputflat">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr id="rowTKNo">
                    <td>
                        <b>Tài khoản có</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTKCo" CssClass="inputflat" ReadOnly="true" BackColor="Aqua" Width="98%" />
                    </td>
                    <td>
                        <b>Tài khoản nợ</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTKNo" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                            Width="98%" />
                    </td>
                </tr>
                <tr id="rowMaLThue">
                    <td>
                        <b>Mã loại thuế</b>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlMaLThue" CssClass="inputflat" Enabled="false" BackColor="Aqua"
                            Width="98%">
                            <asp:ListItem Value="01" Text="Khoản thu do cơ quan thuế quản lý" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="02" Text="Thu phí, lệ phí bộ ngành"></asp:ListItem>
                            <asp:ListItem Value="03" Text="Khoản thu do cơ quan khác quản lý"></asp:ListItem>
                            <asp:ListItem Value="04" Text="Khoản thu do cơ quan hải quan quản lý"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <b>Cơ quan ra quyết định</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtCQQD" CssClass="inputflat" ReadOnly="true"
                            Width="98%" />
                    </td>
                </tr>
                <tr id="rowCQThu">
                    <td>
                        <b>Mã cơ quan thu</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtMaCQThu" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                            Width="98%" />
                    </td>
                    <td>
                        <b>Tên cơ quan thu</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTenCQThu" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                            Width="98%" />
                    </td>
                </tr>
                <tr id="rowDBHC" style="">
                    <td>
                        <b>Mã ĐBHC</b>
                    </td>
                    <td style="">
                        <asp:TextBox runat="server" ID="txtMaDBHC" CssClass="inputflat" ReadOnly="true" BackColor="Aqua" Width="98%" />
                    </td>
                    <td>
                        <b>Tên ĐBHC</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTenDBHC" CssClass="inputflat" ReadOnly="true"
                            Width="98%" />
                    </td>
                </tr>
                <tr id="rowTK_KH_NH">
                    <td>
                        <b>Số tài khoản KH</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTK_KH_NH" CssClass="inputflat" ReadOnly="true"
                            Width="98%" />
                    </td>
                    <td>
                        <b>Ngày khách hàng NH</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtNgayKH_NH" CssClass="inputflat" ReadOnly="true"
                            Width="98%" />
                    </td>
                </tr>
                <tr id="rowKHNhan">
                    <td>
                        <b>Tên khách hàng nhận</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTenKH_Nhan" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                            Width="98%" />
                    </td>
                    <td>
                        <b>STK KH nhận</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtSTK_KH_Nhan" CssClass="inputflat" ReadOnly="true"
                            Width="98%" />
                    </td>
                </tr>
                <tr id="rowDiaChiKHNhan">
                    <td>
                        <b>Địa chỉ khách hàng nhận</b>
                    </td>
                    <td colspan="3">
                        <asp:TextBox runat="server" ID="txtDiaChiKH_Nhan" CssClass="inputflat" ReadOnly="true"
                            Width="100%" />
                    </td>

                </tr>
                <tr id="rowMaNHGiuTK">
                    <td>
                        <b>Mã NH giữ TK NNT</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtMaNH_TK_NNT" CssClass="inputflat" ReadOnly="true"
                            Width="98%" />
                    </td>
                    <td>
                        <b>Mã NH giữ TK KBNN</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtMaNH_TK_KBNN" CssClass="inputflat" ReadOnly="true"
                            Width="98%" />
                    </td>
                </tr>
                <tr id="rowMaNT">
                    <td>
                        <b>Mã nguyên tệ</b>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlMaNT" CssClass="inputflat" Enabled="false" BackColor="Aqua"
                            Width="98%">
                            <asp:ListItem Value="VND" Text="VND" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="USD" Text="USD"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <b>Tỷ giá</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTyGia" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                            Width="98%" />
                    </td>
                </tr>
                <tr id="rowTTTien">
                    <td>
                        <b>Tổng tiền VND Citad/VCB</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTTienVND" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                            Width="98%" />
                    </td>
                    <td>
                        <b>Tổng tiền NT</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTTienNT" CssClass="inputflat" ReadOnly="true" BackColor="Aqua"
                            Width="98%" />
                    </td>
                </tr>
                <tr id="rowMaHQ">
                    <td>
                        <b>Mã hải quan</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtMaHQ" CssClass="inputflat3" ReadOnly="true" Width="98%" />
                    </td>
                    <td>
                        <b>Tên hải quan</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTenMaHQ" CssClass="inputflat" ReadOnly="true"
                            Width="99%" />
                    </td>
                </tr>

                <tr id="rowMaHQPH">
                    <td>
                        <b>Mã hải quan phát hành</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtMaHQPH" CssClass="inputflat" ReadOnly="true" Width="98%" />
                    </td>
                    <td>
                        <b>Tên hải quan phát hành</b>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTenMaHQPH" CssClass="inputflat" ReadOnly="true"
                            Width="98%" />
                    </td>
                </tr>
                <tr id="rowTenNguoiChuyen">
                    <td>
                        <b>Tên Người chuyển</b>
                    </td>
                    <td colspan="3">
                        <asp:TextBox runat="server" ID="txtTen_Nguoi_chuyen" CssClass="inputflat" ReadOnly="true" Width="98%" />
                    </td>

                </tr>
                <tr id="rowDienGiai">
                    <td>
                        <b>Diễn giải</b>
                    </td>
                    <td colspan="3">
                        <asp:TextBox runat="server" ID="txtDienGiai" Style="width: 100%;" CssClass="inputflat"
                            MaxLength="4000" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                 <tr id="rowContentEx">
                    <td>
                        <b>Diễn giải(Citad2.5)</b>
                    </td>
                    <td colspan="3">
                        <asp:TextBox runat="server" ID="txtContentEx" Style="width: 100%;" CssClass="inputflat"
                            MaxLength="4000" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <div class="dataView">
                <asp:DataGrid ID="dtgrd" runat="server" AutoGenerateColumns="False"
                    Width="100%" BorderColor="Black" CssClass="grid_data">
                    <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
                    <HeaderStyle CssClass="grid_header"></HeaderStyle>
                    <ItemStyle CssClass="grid_item" />
                    <Columns>
                        <asp:TemplateColumn Visible="false">
                            <HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkRemove" runat="server" Enabled="false" Checked="true" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Số TK" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                                <asp:TextBox ID="SO_TK" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                    Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "SO_TK") %>'
                                    Font-Bold="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Năm đăng ký" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                                <asp:TextBox ID="NGAY_TK" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                    Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "NGAY_TK") %>'
                                    Font-Bold="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Sắc thuế" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Center" Width="25px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                                <asp:TextBox ID="SAC_THUE" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                    Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "SAC_THUE") %>'
                                    Font-Bold="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Mã chương" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                                <asp:TextBox ID="MA_CHUONG" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                    Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_CHUONG") %>'
                                    Font-Bold="true" MaxLength="3" ToolTip="Ấn phím Enter để chọn mã chương"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Mã NDKT" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Center" Width="20px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                                <asp:TextBox ID="MA_MUC" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                    Style="text-align: center" ReadOnly="true" BackColor="Yellow" Text='<%# DataBinder.Eval(Container.DataItem, "MA_TMUC") %>'
                                    Font-Bold="true" MaxLength="4" ToolTip="Ấn phím Enter để chọn mã tiểu mục" AutoPostBack="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Nội dung" ItemStyle-HorizontalAlign="Left">
                            <HeaderStyle HorizontalAlign="Center" Width="150px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="left" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                                <asp:TextBox ID="NOI_DUNG" runat="server" Width="97%" BorderColor="white" CssClass="inputflat"
                                    ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "NOI_DUNG") %>'
                                    MaxLength="200" Font-Bold="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Kỳ thuế" ItemStyle-HorizontalAlign="Right">
                            <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                                <asp:TextBox ID="KY_THUE" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                    Style="text-align: right" ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "KY_THUE") %>'
                                    Font-Bold="true"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Số tiền VND" ItemStyle-HorizontalAlign="Right">
                            <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="right" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                                <asp:TextBox ID="SOTIEN" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                    Style="text-align: right" ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "SOTIEN")%>'
                                    AutoPostBack="true" onfocus="this.select()" Font-Bold="true">
                                </asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Số tiền NT" ItemStyle-HorizontalAlign="Right">
                            <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                            <ItemStyle HorizontalAlign="right" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                                <asp:TextBox ID="SOTIEN_NT" runat="server" Width="90%" BorderColor="white" CssClass="inputflat"
                                    Style="text-align: right" ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "SOTIEN_NT") %>'
                                    AutoPostBack="true" onfocus="this.select()" Font-Bold="true">
                                </asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
                    <tr>
                        <td align="left" style="width: 25%" class="formx">Lý do</td>
                        <td class="form_control" style="text-align: left">
                            <asp:TextBox ID="txtLydo_ChuyenTra" runat="server" CssClass="inputflat txtHoanThien01" ReadOnly="true"></asp:TextBox></td>
                    </tr>
                </table>
            </div>
           
        </div>
    </form>
</body>
</html>
