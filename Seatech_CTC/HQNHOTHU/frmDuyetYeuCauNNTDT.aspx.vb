﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.nhothu
Imports Business.Common.mdlCommon
Imports ETAX_GDT.ProcessMSG
Imports System.Collections.Generic
Imports System.Xml
Partial Class NTDT_frmDuyetYeuCauNNTDT
    Inherits System.Web.UI.Page
    'Toanva create
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Load_TrangThai_frmDuyetYeuCauNNTDT()
            Load_grid_DSNNTDT()
            hdfMA_NV.Value = Session.Item("User").ToString
        End If
        btnDuyetYeuCau.Attributes.Add("onclick", "javascript:disableButton() ;" + Page.ClientScript.GetPostBackEventReference(btnDuyetYeuCau, "").ToString())
        btnDuyetHuy.Attributes.Add("onclick", "javascript:disableButton() ;" + Page.ClientScript.GetPostBackEventReference(btnDuyetHuy, "").ToString())
        btnChuyenTra.Attributes.Add("onclick", "this.disabled=true;" + Page.ClientScript.GetPostBackEventReference(btnChuyenTra, "").ToString())
    End Sub
    'Private Sub Load_TrangThai()
    '    Try
    '        Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_YEUCAU WHERE TRANG_THAI = '01' OR TRANG_THAI = '02' OR TRANG_THAI = '03' OR TRANG_THAI = '04' OR TRANG_THAI = '05' OR TRANG_THAI = '06' OR TRANG_THAI = '07' OR TRANG_THAI = '08' ORDER BY TRANG_THAI "
    '        Dim dr As OracleClient.OracleDataReader
    '        dr = DataAccess.ExecuteDataReader(sql, CommandType.Text)
    '        drpTrangThai.DataSource = dr
    '        drpTrangThai.DataTextField = "MOTA"
    '        drpTrangThai.DataValueField = "TRANG_THAI"
    '        drpTrangThai.DataBind()
    '        dr.Close()
    '        drpTrangThai.Items.Insert(0, "Tất cả")
    '    Catch ex As Exception
    '        clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái NNTDT. Lỗi: " & ex.Message)
    '    End Try
    'End Sub
    Private Sub Load_TrangThai()
        Try
            Dim ds As DataSet
            Dim strSQL = "SELECT * FROM TCS_DM_TRANGTHAI_YEUCAU WHERE TRANG_THAI = '01' OR TRANG_THAI = '02' OR TRANG_THAI = '03' OR TRANG_THAI = '04' OR TRANG_THAI = '05' OR TRANG_THAI = '06' OR TRANG_THAI = '07' OR TRANG_THAI = '08' ORDER BY TRANG_THAI "
            ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            drpTrangThai.DataSource = ds
            drpTrangThai.DataTextField = "MOTA"
            drpTrangThai.DataValueField = "TRANG_THAI"
            drpTrangThai.DataBind()
            drpTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái NNTDT. Lỗi: " & ex.Message)
        End Try
    End Sub
    'Private Sub Load_TrangThai_frmDuyetYeuCauNNTDT()
    '    Try
    '        Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_YEUCAU WHERE TRANG_THAI IN ('02','03','05','06') ORDER BY TRANG_THAI "
    '        Dim dr As OracleClient.OracleDataReader
    '        dr = DataAccess.ExecuteDataReader(sql, CommandType.Text)
    '        drpTrangThai.DataSource = dr
    '        drpTrangThai.DataTextField = "MOTA"
    '        drpTrangThai.DataValueField = "TRANG_THAI"
    '        drpTrangThai.DataBind()
    '        dr.Close()
    '        drpTrangThai.Items.Insert(0, "Tất cả")
    '    Catch ex As Exception
    '        clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái NNTDT. Lỗi: " & ex.Message)
    '    End Try
    'End Sub
    Private Sub Load_TrangThai_frmDuyetYeuCauNNTDT()
        Try
            Dim ds As DataSet
            Dim strSQL = "SELECT * FROM TCS_DM_TRANGTHAI_YEUCAU WHERE TRANG_THAI IN ('02','03','05','06') ORDER BY TRANG_THAI "
            ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            drpTrangThai.DataSource = ds
            drpTrangThai.DataTextField = "MOTA"
            drpTrangThai.DataValueField = "TRANG_THAI"
            drpTrangThai.DataBind()
            drpTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái NNTDT. Lỗi: " & ex.Message)
        End Try
    End Sub
    'Lấy dữ liệu cho gird người nộp thuế
    Private Sub Load_grid_DSNNTDT(Optional ByVal strTrangThai As String = "99")
        Dim ds As DataSet
        'Dim objNNTDT As New NNTDT.infNNTDT
        Dim WhereClause As String = ""
        Try
            ds = daTCS_HQ247_314.getCheckerMSG213(, , "" + Session.Item("UserName").ToString + "")
            'If Not IsEmptyDataSet(ds) Then
            grdDSCT.DataSource = ds.Tables(0)
            grdDSCT.DataBind()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách người nộp thuế. Lỗi: " & ex.Message)
        End Try
    End Sub
    Private Function Load_grid_DSNNTDT1(Optional ByVal strTrangThai As String = "99") As DataSet
        Dim ds As New DataSet
        Dim WhereClause As String = ""
        Try
            If drpTrangThai.SelectedIndex <> 0 Then
                WhereClause = " AND A.TRANGTHAI = '" & strTrangThai.ToString() & "'"
            End If
            ds = daTCS_HQ247_314.getCheckerMSG213(WhereClause, , "" + Session.Item("UserName").ToString + "")
            Return ds
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách người nộp thuế. Lỗi: " & ex.Message)
            Return ds
        End Try
    End Function
    Protected Sub btnSelectCT_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim btnSelectCT As LinkButton = CType(sender, LinkButton)
        Dim v_strRetArgument As String() = btnSelectCT.CommandArgument.ToString.Split(",")
        If (v_strRetArgument.Length > 0) Then
            Dim v_strNNTDT_ID As String = v_strRetArgument(0)
            hdfNNTDT_ID.Value = v_strNNTDT_ID
            Dim ds As DataSet = Business_HQ.HQ247.TCS_DM_NTDT_HQ.getMakerMSG213(v_strNNTDT_ID)
            If ds.Tables(0).Rows.Count > 0 Then
                txtSO_HS.Text = ds.Tables(0).Rows(0)("So_HS").ToString()
                txtLOAI_HS.Value = ds.Tables(0).Rows(0)("Loai_HS").ToString()
                txtID.Value = ds.Tables(0).Rows(0)("ID").ToString()
                txtTenLoai_HS.Text = ds.Tables(0).Rows(0)("TenLoai_HS").ToString()
                txtMA_DV.Text = ds.Tables(0).Rows(0)("Ma_DV").ToString()
                txtTEN_DV.Text = ds.Tables(0).Rows(0)("Ten_DV").ToString()
                txtDIACHI.Text = ds.Tables(0).Rows(0)("DiaChi").ToString()
                txtHO_TEN.Text = ds.Tables(0).Rows(0)("Ho_Ten").ToString()
                txtSO_CMT.Text = ds.Tables(0).Rows(0)("So_CMT").ToString()
                txtNGAYSINH.Text = ds.Tables(0).Rows(0)("NgaySinh").ToString()
                txtNGUYENQUAN.Text = ds.Tables(0).Rows(0)("NguyenQuan").ToString()
                txtSO_DT.Text = ds.Tables(0).Rows(0)("So_DT").ToString()
                txtEMAIL.Text = ds.Tables(0).Rows(0)("Email").ToString()
                txtSO_DT1.Text = ds.Tables(0).Rows(0)("So_DT1").ToString()
                txtEMAIL1.Text = ds.Tables(0).Rows(0)("Email1").ToString()
                txtSERIALNUMBER.Text = ds.Tables(0).Rows(0)("SerialNumber").ToString()
                txtNOICAP.Text = ds.Tables(0).Rows(0)("NOICAP").ToString()
                txtNGAY_HL.Text = ds.Tables(0).Rows(0)("Ngay_HL").ToString()
                txtNGAY_HHL.Text = ds.Tables(0).Rows(0)("Ngay_HHL").ToString()
                txtMA_NH_TH.Text = ds.Tables(0).Rows(0)("Ma_NH_TH").ToString()
                txtTEN_NH_TH.Text = ds.Tables(0).Rows(0)("Ten_NH_TH").ToString()
                txtTAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("TaiKhoan_TH").ToString()
                txtTEN_TAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("Ten_TaiKhoan_TH").ToString()
                txtNGAY_NHAN311.Text = ds.Tables(0).Rows(0)("NGAY_NHAN311").ToString()
                txtNOIDUNG_XL.Text = ds.Tables(0).Rows(0)("LY_DO_HUY").ToString()
                txtLY_DO_CHUYEN_TRA.Text = ds.Tables(0).Rows(0)("LY_DO_CHUYEN_TRA").ToString()
                txtNOIDUNG_XL.ReadOnly = True
                txtLY_DO_CHUYEN_TRA.ReadOnly = False
                Dim strTMP As String = "<data>" & ds.Tables(0).Rows(0)("THONGTINLIENHE").ToString() & "</data>"
                If strTMP.Length > 0 Then
                    Dim tbl As DataTable = New DataTable()
                    tbl.Columns.Add("STT")
                    tbl.Columns.Add("So_DT")
                    tbl.Columns.Add("Email")
                    Dim xmldoc As XmlDocument = New XmlDocument()
                    xmldoc.LoadXml(strTMP)
                    Dim x As Integer = 0
                    For Each vnode As XmlNode In xmldoc.SelectNodes("data/ThongTinLienHe")
                        Dim vrow As DataRow = tbl.NewRow()
                        For i As Integer = 0 To vnode.ChildNodes.Count - 1
                            vrow(vnode.ChildNodes.Item(i).Name) = vnode.ChildNodes.Item(i).InnerText
                        Next
                        x = x + 1
                        vrow("STT") = x
                        tbl.Rows.Add(vrow)
                    Next
                    dtgrd.DataSource = tbl
                    dtgrd.DataBind()
                End If
                Select Case ds.Tables(0).Rows(0)("TRANGTHAI").ToString()
                    Case "01"
                        btnDuyetYeuCau.Enabled = False
                        btnDuyetHuy.Enabled = False
                        btnChuyenTra.Enabled = False
                    Case "02"
                        btnDuyetYeuCau.Text = "Duyệt yêu cầu"
                        btnDuyetYeuCau.Enabled = True
                        btnDuyetHuy.Enabled = False
                        btnChuyenTra.Enabled = True
                    Case "03"
                        btnDuyetYeuCau.Text = "Duyệt lại yêu cầu"
                        btnDuyetYeuCau.Enabled = True
                        btnDuyetHuy.Enabled = False
                        btnChuyenTra.Enabled = False
                    Case "04"
                        btnDuyetYeuCau.Enabled = False
                        btnDuyetHuy.Enabled = False
                        btnChuyenTra.Enabled = False
                    Case "05"
                        btnDuyetYeuCau.Enabled = False
                        btnDuyetHuy.Enabled = True
                        btnDuyetHuy.Text = "Duyệt hủy"
                        btnChuyenTra.Enabled = True

                    Case "06"
                        btnDuyetYeuCau.Enabled = False
                        btnDuyetHuy.Text = "Duyệt Hủy lại"
                        btnDuyetHuy.Enabled = True
                        btnChuyenTra.Enabled = False
                    Case "07"
                        btnDuyetYeuCau.Enabled = False
                        btnDuyetHuy.Enabled = False
                        btnChuyenTra.Enabled = False
                    Case "08"
                        btnDuyetYeuCau.Enabled = False
                        btnDuyetHuy.Enabled = False
                        btnChuyenTra.Enabled = False
                End Select
            End If
        End If
    End Sub
    Protected Sub btnDuyetYeuCau_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuyetYeuCau.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        Try
            Dim strID As String = txtID.Value
            Dim strLoai213 As String = "1" '1: Chấp nhận; 2: Không chấp nhận
            Dim strNoiDung_XL As String = txtNOIDUNG_XL.Text ' Lý do Hủy, kèm hướng dẫn xử lý
            Dim strTenDN As String = Session.Item("UserName").ToString.ToUpper ' Tên đăng nhập
            Dim errID As Decimal = daTCS_HQ247_314.CheckerMSG213(strID, strNoiDung_XL, strLoai213, strTenDN) 'Update TRANGTHAI của đăng ký thành Duyệt yêu cầu - chờ xác nhận từ TCHQ
            If errID = 0 Then
                errID = CustomsServiceV3.ProcessMSG.sendMSG213_314("314", strLoai213, strNoiDung_XL, hdfNNTDT_ID.Value.ToString(), strTenDN)
                If errID = 0 Then
                    clsCommon.ShowMessageBox(Me, "Duyệt yêu cầu thành công. Đã gửi thông tin tới HQ !")
                    'cam nut bam
                Else
                    clsCommon.ShowMessageBox(Me, "Duyệt yêu cầu lỗi:  " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(errID.ToString()))
                End If
                'If errID = 0 Then
                '    clsCommon.ShowMessageBoxHQ247(Me, "Cập nhật trạng thái thành công !", HttpContext.Current.Request.Url.AbsoluteUri.ToString)
                'Else
                '    clsCommon.ShowMessageBox(Me, "Lỗi gửi CTTDTHQ !")
                'End If
            End If
            If Not errID = 0 Then
                clsCommon.ShowMessageBox(Me, "Duyệt yêu cầu:  " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(errID.ToString()))
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Duyệt yêu cầu lỗi: " & ex.Message)
        End Try
    End Sub

    Protected Sub btnDuyetHuy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuyetHuy.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        Try
            Dim strID As String = txtID.Value
            Dim strLoai213 As String = "2" '1: Chấp nhận; 2: Không chấp nhận
            Dim strNoiDung_XL As String = txtLY_DO_HUY.Text ' Lý do Hủy, kèm hướng dẫn xử lý
            Dim strTenDN As String = Session.Item("UserName").ToString.ToUpper ' Tên đăng nhập
            Dim errID As Decimal = daTCS_HQ247_314.CheckerMSG213(strID, strNoiDung_XL, strLoai213, strTenDN) 'Nhận mã lỗi trả về
            If errID = 0 Then
                errID = CustomsServiceV3.ProcessMSG.sendMSG213_314("311", strLoai213, strNoiDung_XL, hdfNNTDT_ID.Value.ToString(), strTenDN)
                If errID = 0 Then
                    clsCommon.ShowMessageBox(Me, "Duyệt hủy thành công !")
                    'cam nut bam
                End If
                'If errID = 0 Then
                '    clsCommon.ShowMessageBoxHQ247(Me, "Cập nhật trạng thái thành công !", HttpContext.Current.Request.Url.AbsoluteUri.ToString)
                'Else
                '    clsCommon.ShowMessageBox(Me, "Lỗi gửi CTTDTHQ !")
                'End If
            End If
            If Not errID = 0 Then
                clsCommon.ShowMessageBox(Me, "Kiểm soát lỗi:  " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(errID.ToString()))
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi: " & ex.Message)
        End Try
    End Sub
    Protected Sub btnChuyenTra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChuyenTra.Click
        If txtLY_DO_CHUYEN_TRA.Text.Trim().Length <= 0 Then
            clsCommon.ShowMessageBox(Me, "Mục LÝ DO CHUYỂN TRẢ là bắt buộc nhập !")
            txtLY_DO_CHUYEN_TRA.BorderColor = Drawing.Color.DarkOrange
            txtLY_DO_CHUYEN_TRA.Focus()
            Exit Sub
        End If
        'Chuyển trạng thái HS sang Chuyển trả yêu cầu (08)
        Dim strTenDN As String = Session.Item("UserName").ToString.ToUpper ' Tên đăng nhập
        Dim errorID As Decimal = daTCS_HQ247_314.UPDATE_TRANGTHAI_TRUYENTRA_213(hdfNNTDT_ID.Value.Trim(), strTenDN)
        If errorID = 0 Then
            clsCommon.ShowMessageBox(Me, "Chuyển trả thành công !")
        Else
            clsCommon.ShowMessageBox(Me, "Xảy ra lỗi: " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(errorID.ToString()))
        End If
    End Sub
    Protected Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Response.Redirect("~/pages/frmHomeNV.aspx", False)
    End Sub
    'Chuyển trang DS_NNTDT
    Protected Sub grdDSCT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdDSCT.PageIndexChanging
        Dim v_ds As DataSet = Load_grid_DSNNTDT1(drpTrangThai.SelectedValue)
        grdDSCT.PageIndex = e.NewPageIndex
        grdDSCT.DataSource = v_ds
        grdDSCT.DataBind()
    End Sub
    'Chọn drop trạng thái NNTDT
    Protected Sub drpTrangThai_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpTrangThai.SelectedIndexChanged
        Dim v_ds As DataSet = Load_grid_DSNNTDT1(drpTrangThai.SelectedValue)
        grdDSCT.DataSource = v_ds
        grdDSCT.DataBind()
        grdDSCT.PageIndex = 0
    End Sub
End Class
