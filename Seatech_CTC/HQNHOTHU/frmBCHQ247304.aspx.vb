﻿Imports System.Data
Imports VBOracleLib
Imports Business.BaoCao
Imports Business.XLCuoiNgay
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables

Partial Class frmBCHQ247304
    Inherits System.Web.UI.Page

    Private mv_strNgay As String

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenDiemThu As String
    Private mv_strTenKB As String
    Private mv_strLoaiBaoCao As String
    ' Private Shared strMaNhom_GDV As String = ConfigurationManager.AppSettings.Get("GROUP_TELLER").ToString()
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
#Region "Page event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session.Item("User") Is Nothing Then
            clsCTU.TCS_SetupParams(CType(Session("User"), Integer), mv_strSHKB, mv_strKyHieuCT, _
                                   mv_strMaDiemThu, mv_strTenDiemThu, mv_strDBThu, mv_strMaNHKB, mv_strTenKB)

        End If
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại

                txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại

                'get_NguyenTe()
                clsCommon.get_NguyenTe(cboNguyenTe)
                DM_DiemThu()
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub

    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        LogApp.AddDebug("cmdExit_Click", "BC TINH HINH THU NSNN: In BC")
        Try
            TCS_BC_TONGCN()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình Lập bảng kê chứng từ!")
        Finally
            cmdIn.Enabled = True
        End Try
    End Sub

    Protected Sub cmdExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        LogApp.AddDebug("cmdExit_Click", "BC TINH HINH THU NSNN: Thoat")
        Response.Redirect("../pages/frmHomeNV.aspx", False)
    End Sub

#End Region

#Region "Call Report"
    Private Sub TCS_BC_TONGCN()
        Try
            Dim ds As DataSet
            'If cboBangKeCT.SelectedValue = "1" Then
            If cboLoaiBaoCao.SelectedValue.Equals("10") Then
                mv_strLoaiBaoCao = Report_Type.TCS_BC_TONG
            ElseIf cboLoaiBaoCao.SelectedValue.Equals("20") Then
                mv_strLoaiBaoCao = Report_Type.TCS_BC_TONG_CT
            Else
                mv_strLoaiBaoCao = Report_Type.BC_CHITIET_GD_HQ247_NHOTHU
            End If
            If mv_strLoaiBaoCao = Report_Type.TCS_BC_TONG_CT Or mv_strLoaiBaoCao = Report_Type.TCS_BC_TONG Then
                '02/10/2012-manhnv
                Dim WhereLoaiThue As String = ""
                If cboLoaiBaoCao.SelectedValue.Substring(1, 1) = "1" Then
                    WhereLoaiThue = " AND hdr.ma_lthue <> '04' "
                ElseIf cboLoaiBaoCao.SelectedValue.Substring(1, 1) = "2" Then
                    WhereLoaiThue = " AND hdr.ma_lthue = '04' "
                End If
                Dim WhereNguyenTe As String = ""

                WhereNguyenTe = "AND hdr.ma_nt = '" & cboNguyenTe.SelectedValue.ToString() & "'"
                'End If

                '-----
                Dim strWhere As String = WhereNgay() & WhereBranch() & WhereLoaiThue & WhereNguyenTe
                If Not Session.Item("WhereAll") Is Nothing Then
                    Session.Item("WhereAll") = ""
                End If
                Session.Item("WhereAll") = strWhere
                'ds = buBaoCao.TCS_BC_TONG_CN(strWhere)
                ds = Business_HQ.nhothu.daBaoCaoNHOTHU.TCS_BC_TONG_CN(strWhere)
                Dim frm As New infBaoCao
                frm.TCS_DS = ds
                frm.TCS_Ngay_CT = mv_strNgay
                frm.Mau_BaoCao = cboLoaiBaoCao.SelectedItem.Text
                frm.Ngay = Mid(clsCTU.TCS_GetNgayLV(Session.Item("User")), 7, 2)
                frm.Thang = Mid(clsCTU.TCS_GetNgayLV(Session.Item("User")), 5, 2)
                frm.Nam = Mid(clsCTU.TCS_GetNgayLV(Session.Item("User")), 1, 4)
                frm.TCS_Tungay = txtTuNgay.Value.ToString()
                frm.TCS_Denngay = txtDenNgay.Value.ToString()
                Session("objBaoCao") = frm 'Gọi form show báo cáo
                clsCommon.OpenNewWindow(Me, "../pages/BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao, "ManPower")
            Else

                Dim str_where As String = ""
                If txtTuNgay.Value <> "" Then
                    str_where &= " AND  TO_CHAR(A.ngaynhanmsg,'yyyyMMdd') >=" & ConvertDateToNumber(txtTuNgay.Value)
                Else
                    clsCommon.ShowMessageBox(Me, "Bạn cần nhập vào thời gian từ ngày đến ngày cho báo cáo")
                    txtTuNgay.Focus()
                    Return
                End If
                If txtDenNgay.Value <> "" Then
                    str_where &= " AND  TO_CHAR(A.ngaynhanmsg,'yyyyMMdd') <=" & ConvertDateToNumber(txtDenNgay.Value)
                Else
                    clsCommon.ShowMessageBox(Me, "Bạn cần nhập vào thời gian từ ngày đến ngày cho báo cáo")
                    txtDenNgay.Focus()
                    Return
                End If

                If cboDiemThu.SelectedIndex > 0 Then
                    str_where += " AND A.MA_CN = '" & cboDiemThu.SelectedValue & "' "
                End If

                Dim sql As String = "SELECT A.SO_CTU SO_CT,NVL(A.SO_CT_NH,'_') SO_FT,A.TEN_DV TEN_NNT,A.MA_DV MST,A.HQ_SOTIEN_TO SO_TIEN," & _
                                    " NVL((SELECT MAX(C.REMARKS) FROM TCS_CTU_HQ_NHOTHU_HDR C WHERE C.SO_CT = A.SO_CTU),'_') NOI_DUNG," & _
                                    " TO_CHAR(A.NGAYNHANMSG,'DD/MM/RRRR HH24:MI:SS') TT_CT_SHB,B.MOTA TRANG_THAI,NVL(A.FEEAMT,0) FEEAMT,NVL(VATAMT,0) VATAMT " & _
                                    " FROM TCS_HQ247_201 A,TCS_DM_TRANGTHAI_TT_HQ247 B WHERE A.TRANGTHAI = B.TRANGTHAI " & str_where

                ds = New DataSet
                Dim frm = New infBaoCao
                ds = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
                If (mdlCommon.IsEmptyDataSet(ds)) Then
                    clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                    Return
                End If
                frm.TCS_DS = ds
                frm.TCS_Tungay = txtTuNgay.Value.ToString()
                frm.TCS_Denngay = txtDenNgay.Value.ToString()
                Session("objBaoCao") = frm 'Gọi form show báo cáo
                clsCommon.OpenNewWindow(Me, "../pages/BaoCao/frmShowReports.aspx?BC=" & Report_Type.BC_CHITIET_GD_HQ247_NHOTHU, "ManPower")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



#End Region
#Region "Load DM"
    Private Sub DM_DiemThu()

        Dim dsDiemThu As DataSet
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        'Dim str_Ma_Nhom As String = ""
        Dim dt As New DataTable
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        'str_Ma_Nhom = LOAD_MN()
        'If str_Ma_Nhom = strMaNhom_GDV Then
        '    strSQL = "SELECT a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a where a.id= '" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        'Else
        If strcheckHSC = "1" Then
            'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
            ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
            strSQL = "SELECT a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a  "

        ElseIf strcheckHSC = "2" Then

            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  (branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' or id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "')"
        Else
            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        End If
        'End If
        Try
            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)

            dt = dsDiemThu.Tables(0)
            cboDiemThu.DataSource = dt
            cboDiemThu.DataTextField = "name"
            cboDiemThu.DataValueField = "id"
            cboDiemThu.DataBind()

            If strcheckHSC = "1" Then
                clsCommon.ComboBoxWithValue(cboDiemThu, dsDiemThu, 1, 0, "Tất cả")
            End If

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace)
            Throw ex
        Finally

        End Try
    End Sub

    Public Function LOAD_MN() As String
        Dim str_return As String = ""
        Dim strSQL_MaNhom As String = "select ma_nhom from tcs_dm_nhanvien where ma_nv='" & Session.Item("User").ToString & "'"
        Dim dt As New DataTable
        dt = DataAccess.ExecuteToTable(strSQL_MaNhom)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            str_return = dt.Rows(0)("ma_nhom").ToString
        End If
        Return str_return
    End Function
#End Region

#Region "Where Clause"
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Private Function WhereNgay() As String
        Dim strWhereNgay As String
        Dim strNgayStart As String
        Dim strNgayEnd As String
        strNgayStart = ConvertDateToNumber(txtTuNgay.Value.ToString())
        strNgayEnd = ConvertDateToNumber(txtDenNgay.Value.ToString())
        strWhereNgay = " and hdr.ngay_kb >=" & strNgayStart & " "
        strWhereNgay = strWhereNgay & " and hdr.ngay_kb <=" & strNgayEnd & " "


        mv_strNgay = "Ngày " & txtTuNgay.Value.ToString()
        Return strWhereNgay
    End Function
    Private Function WhereBranch() As String
        Dim strWhereBranch As String = ""
        If cboDiemThu.SelectedValue <> "" Then
            strWhereBranch = " and (hdr.ma_cn='" & cboDiemThu.SelectedValue & "') "
        End If
        Return strWhereBranch
    End Function
    Private Sub get_NguyenTe()

        Dim dt As DataTable
        Try
            Dim strSQL As String = "SELECT * from Tcs_dm_nguyente "
            dt = DataAccess.ExecuteToTable(strSQL)

            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then

                For i As Integer = 0 To dt.Rows.Count
                    Dim item As ListItem
                    item = New ListItem(dt.Rows(i)("TEN").ToString(), dt.Rows(i)("Ma_NT").ToString())
                    cboNguyenTe.Items.Add(item)
                Next

            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace)
        End Try
    End Sub
#End Region

End Class
