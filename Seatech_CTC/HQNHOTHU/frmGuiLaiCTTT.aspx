﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmGuiLaiCTTT.aspx.vb" Inherits="HQ247_frmGuiLaiCTTT" Title="GỬI LẠI THÔNG BÁO CHỨNG TỪ" %>

<%@ Import Namespace="Business.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }
        function jsChiTietNNT(_ID, title) {
            createCookie("SEATECHIT_TENDN", '<%=Session.Item("User").ToString() %>', 1);
            window.open("ViewMessage.ashx?ID=" + _ID + "&IN=" + title, "", "", "_new");
            //window.showModalDialog("../NTDT/frmChiTiet.aspx?ID=" + _ID, "", "dialogWidth:800px;dialogHeight:480px;help:0;status:0;", "_new");
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".JchkAll").click(function () {
                // alert($(this).is(':checked'));
                $('.JchkGrid').attr('checked', $(this).is(':checked'));
                GetListSoCT();
            });
            
        });
        function GetListSoCT() {
            var values = $('input:checkbox:checked.JchkGrid').map(function () {
                return this.value;
            }).get();



            if (values == '') {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);


            }
            else {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);


            }
        }
        function ValidateNgay() {
            var tungay = $('#<%=txtTuNgay.ClientID%>').val();
             var denngay = $('#<%=txtDenNgay.ClientID%>').val();
             if (tungay.trim().length == 0 && denngay.trim().length == 0) {
                 return true;
             }
             var tungay1 = tungay.split("/");
             var denngay1 = denngay.split("/");
             var d1 = NaN;
             var d2 = NaN;
             if (tungay1.length == 3) {
                 d1 = Date.parse(tungay1[1] + "/" + tungay1[0] + "/" + tungay1[2]);
             }
             if (denngay1.length == 3) {
                 d2 = Date.parse(denngay1[1] + "/" + denngay1[0] + "/" + denngay1[2]);
             }
             if (tungay.trim().length == 0) {
                 if (d2 != NaN)
                     return true;
                 else {
                     alert("Đến ngày không hợp lệ.");
                     return false;
                 }
             }
             if (denngay.trim().length == 0) {
                 if (d1 != NaN)
                     return true;
                 else {
                     alert("Từ ngày không hợp lệ.");
                     return false;
                 }
             }
             if (d1 == NaN) {
                 alert("Từ ngày không hợp lệ.");
                 return false;
             }
             if (d2 == NaN) {
                 alert("Đến ngày không hợp lệ.");
                 return false;
             }
             if (d1 > d2) {
                 alert("Vui lòng nhập Từ ngày đăng ký nhỏ hơn hoặc bằng Đến ngày đăng ký");
                 return false;
             }
             return true;
         }
    </script>
    <style type="text/css">
        .container {
            width: 100%;
            margin-bottom: 30px;
        }

        .dataView {
            padding-bottom: 10px;
        }

        .JchkAll, .JchkGrid {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <p class="pageTitle">GỬI LẠI THÔNG BÁO CHỨNG TỪ</p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
            <tr>
                <td align="left" style="width: 15%" class="form_label">Từ ngày</td>
                <td class="form_control" style="width: 35%"><span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                        onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                </span></td>
                <td align="left" style="width: 15%" class="form_label">Đến ngày</td>
                <td class="form_control" style="width: 35%">
                    <span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                        <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                            onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                    </span></td>
            </tr>
            <tr>
                <td align="left" class="form_label">Mã số thuế</td>
                <td class="form_control" align="left">
                    <asp:TextBox ID="txtMST" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
               <td align="left" class="form_label">Trạng thái</td>
               <td class="form_control">
                    <asp:DropDownList ID="cboTrangThai" runat="server" Width="90%" CssClass="selected inputflat">
                        <asp:ListItem Selected="True" Value="" Text="Tất cả">                     
                        </asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" class="form_label">Số tài khoản</td>
                <td class="form_control" align="left">
                    <asp:TextBox ID="txtTK_KH_NH" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <td align="left" class="form_label">Số chứng từ CoreBank</td>
                <td class="form_control"><asp:TextBox ID="txtCTCorebank" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
            </tr>
            <tr>
	            <td align="left" class="form_label">Tên người nộp thuế</td>
	            <td class="form_control" colspan="3" align="left" >
		            <asp:TextBox ID="txtTenNNT" runat="server" class="inputflat" Width="85%"></asp:TextBox></td>
	       </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnTimKiem" CssClass="buttonField" runat="server" OnClientClick="return ValidateNgay();" Text="Tìm kiếm" />&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnReSend" CssClass="buttonField" runat="server" Text="  Gửi lại  " />

                </td>

            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </div>

    <div class="dataView">

        <asp:HiddenField ID="ListCT_NoiDia" runat="server" />
        <asp:GridView ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="100%" BorderColor="Black" PageSize="10" AllowPaging="True"
            OnItemCommand="ItemsGrid_Command">
            <Columns>
                <asp:BoundField HeaderText="STT" DataField="STT" Visible="true" ReadOnly="true" />
                <asp:BoundField DataField="id" HeaderText="ID" ReadOnly="True" SortExpression="id" />
                <asp:BoundField DataField="so_ct_nh" HeaderText="Số chứng Corebank" ReadOnly="True" SortExpression="so_ct_nh" />
                <asp:BoundField DataField="SO_CTU" HeaderText="Số chứng từ ETax" ReadOnly="True" SortExpression="SO_CTU" />
                <asp:BoundField DataField="MA_DV" HeaderText="Mã số thuế" ReadOnly="True" />
                <asp:BoundField DataField="TEN_DV" HeaderText="Tên NNT" ReadOnly="True" />
                <asp:BoundField DataField="TAIKHOAN_TH" HeaderText="Số TK NH" ReadOnly="True" />
                <asp:BoundField DataField="ngaynhan201" HeaderText="Ngày nhận 201" ReadOnly="True" />
                <asp:BoundField DataField="ngay_hq_ct" HeaderText="Ngày HQ CT" ReadOnly="True" />
                <asp:TemplateField HeaderText="Số tiền">
                     <ItemTemplate >
                        <asp:Label ID="lblSotien" runat="server" Text='<%# Business_HQ.Common.mdlCommon.NumberToStringVN(DataBinder.Eval(Container.DataItem, "sotien"))%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="TRANG_THAI" HeaderText="Trạng thái" ReadOnly="True" />
                <asp:TemplateField>
                    <HeaderTemplate>
                        <input type="checkbox" runat="server" id="HeaderCheckBox" title="Chọn tất" class="JchkAll" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <input type="checkbox" class="JchkGrid" value='<%# DataBinder.Eval(Container.DataItem, "ID") %>' onclick="GetListSoCT()" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#999966" BorderColor="Black" BorderStyle="Solid"
                BorderWidth="1px" Font-Bold="True" />
            <AlternatingRowStyle BackColor="#EFEDE4" />
        </asp:GridView>
    </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function () {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>
</asp:Content>
