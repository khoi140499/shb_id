﻿Imports System.Data
Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao
Imports log4net
Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.nhothu
Imports CustomsServiceV3
Imports Business_HQ.Common
Imports CustomsV3.MSG.MSG201
Imports CustomsV3.MSG.MSG314
Partial Class HQ247_frmTraCuuVaXulyCTTT
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If

        If Not IsPostBack Then
            Load_TrangThai()
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub
    Private Sub load_dataGrid(Optional ByVal isThrow As Boolean = True)
        Dim ds As DataSet
        Dim WhereClause As String = "    "
        Try
            dtgrd.DataSource = Nothing
            dtgrd.DataBind()
            'If txtTuNgay.Value.Trim = "" Then
            '    clsCommon.ShowMessageBox(Me, "Từ ngày đăng kí trống")
            '    Exit Sub
            'End If
            'If txtDenNgay.Value.Trim = "" Then
            '    clsCommon.ShowMessageBox(Me, "Đến ngày đăng kí trống")
            '    Exit Sub
            'End If
            If txtMST.Text <> "" Then
                WhereClause += " AND upper(a.MA_DV) like upper('%" & txtMST.Text.Trim & "%')"
            End If
            If txtTen_NNT.Text <> "" Then
                WhereClause += " AND upper(a.TEN_DV) like upper('%" & txtTen_NNT.Text.Trim & "%')"
            End If
            If txtSoTK.Text <> "" Then
                WhereClause += " AND upper(a.TAIKHOAN_TH) like upper('%" & txtSoTK.Text.Trim & "%')"
            End If
            If txtTuNgay.Value <> "" Then
                WhereClause += " AND to_char(a.NGAYNHANMSG,'RRRRMMDD') >=" & ConvertDateToNumber(txtTuNgay.Value)
            End If
            If txtDenNgay.Value <> "" Then
                WhereClause += " AND to_char(a.NGAYNHANMSG,'RRRRMMDD') <= " & ConvertDateToNumber(txtDenNgay.Value)
            End If
            If txtSoCTU.Text.Length > 0 Then
                WhereClause += " AND SO_CTU = " & txtSoCTU.Text.Trim() & " "
            End If
            If cboTrangThai.SelectedIndex > 0 Then
                WhereClause += " AND A.TRANGTHAI = '" & cboTrangThai.SelectedValue.ToString() & "' "
            End If
            If hdfShowMSG.Value.Equals("0") Then
                isThrow = False
            End If
            hdfShowMSG.Value = "1"
            ds = daCTUHQ201.getMSG201_TracuuXuly(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgrd.DataSource = ds.Tables(0)
                dtgrd.PageIndex = 0
                dtgrd.DataBind()
            Else
                If isThrow Then
                    clsCommon.ShowMessageBox(Me, "Không tìm thấy bản nghi thỏa mãn điều kiện tìm kiếm")
                End If
            End If
        Catch ex As Exception
            If isThrow Then
                clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
            End If
        End Try
    End Sub
    Private Sub Load_TrangThai()
        Try
            Dim sql As String
            sql = "SELECT * FROM TCS_DM_TRANGTHAI_TT_HQ247 where TRANGTHAI not in ('08','09')"
            'sql = "SELECT * FROM TCS_DM_TRANGTHAI_TT_HQ247 "
            Dim dr As DataSet = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            cboTrangThai.DataSource = dr.Tables(0)
            cboTrangThai.DataTextField = "MOTA"
            cboTrangThai.DataValueField = "TRANGTHAI"
            cboTrangThai.DataBind()
            cboTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái yêu cầu. Lỗi: " & ex.Message)
        End Try
    End Sub
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        load_dataGrid()
    End Sub
    Protected Function ReSENDHQ(ByVal strID As String) As Integer
        Try
            Dim strErrorNum As String = "0"
            Dim strErrorMes As String = ""
            Dim v_strTransaction_id As String = ""
            Dim pv_so_tn_ct As String = ""
            Dim pv_ngay_tn_ct As String = ""
            Dim ds As DataSet = Business_HQ.HQ247.daCTUHQ304.getMSG304Detail(strID)
            If ds Is Nothing Then
                Return 1
            End If
            If ds.Tables.Count <= 0 Then
                Return 1
            End If
            If ds.Tables(0).Rows.Count <= 0 Then
                Return 1
            End If
            Dim strMST As String = ds.Tables(0).Rows(0)("MA_DV").ToString()
            Dim strSO_CTU As String = ds.Tables(0).Rows(0)("SO_CTU").ToString()
            CustomsServiceV3.ProcessMSG.guiCTThueHQ247(strMST, strSO_CTU, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
            If Not strErrorNum.Equals("0") Then
                Business_HQ.HQ247.daCTUHQ304.TCS_HQ247_TRANGTHAI_304_SEND301_ERROR(strID, Session.Item("UserName").ToString())
                Return 1
            End If
            'doan nay cho doi chieu
            Dim decResult As Decimal = Business_HQ.HQ247.daCTUHQ304.TCS_HQ247_TRANGTHAI_304_SEND301_OK(strID, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct, Session.Item("UserName").ToString())
            If decResult > 0 Then
                Return 1
            End If
            Return 0
        Catch ex As Exception
            Return 1
        End Try
        Return 1
    End Function
    Protected Sub dtgrd_PageIndexChanging(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dtgrd.PageIndexChanging
        load_dataGrid()
        dtgrd.PageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub btnXuly_Click(sender As Object, e As EventArgs)
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If ListCT_NoiDia.Value.Length <= 0 Then
            clsCommon.ShowMessageBox(Me, "Chưa chọn chứng từ để xử lý! ")
            Return
        End If
        Dim str201ID As String = ListCT_NoiDia.Value
        Dim strSender_Code As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString()
        Dim obj As HQ247XulyMSG.XULY_MSG201 = New HQ247XulyMSG.XULY_MSG201(Session.Item("UserName").ToString(), Session.Item("User").ToString(), strSender_Code)
        Dim kq As String = obj.ProcessData(str201ID)
        If kq.Trim.Length > 0 Then
            clsCommon.ShowMessageBox(Me, kq)
        End If
    End Sub
End Class
