﻿Imports System.Data
Imports Business.NTDT.NNTDT
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao
Imports Business_HQ.nhothu
Partial Class HQ247_frmTraCuuNNTDT
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Load_TrangThai()
            Load_Loai_HS()

            DM_DiemThu()
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub
    Private Sub load_dataGrid(Optional ByVal isThrow As Boolean = True)
        Dim ds As DataSet
        Dim WhereClause As String = " AND A.TRANGTHAI NOT IN ('01','02','03','16') "
        Try
            If txtMST.Enabled Then
                'If txtTuNgay.Value.Trim = "" Then
                '    clsCommon.ShowMessageBox(Me, "Từ ngày đăng kí trống")
                '    Exit Sub
                'End If
                'If txtDenNgay.Value.Trim = "" Then
                '    clsCommon.ShowMessageBox(Me, "Đến ngày đăng kí trống")
                '    Exit Sub
                'End If
                If txtNgayHLUQ.Value <> "" Then
                    WhereClause += " AND TO_CHAR(ngay_hl_uq,'RRRRMMDD') >= " & ConvertDateToNumber(txtNgayHLUQ.Value)
                End If
                If txtNgayHHLUQ.Value <> "" Then
                    WhereClause += " AND TO_CHAR(ngay_hhl_uq,'RRRRMMDD') <= " & ConvertDateToNumber(txtNgayHHLUQ.Value)
                End If
                If txtSo_HS.Text <> "" Then
                    WhereClause += " AND upper(a.SO_HS) like upper('%" & txtSo_HS.Text.Trim & "%')"
                End If
                If drpLoai_HS.SelectedIndex <> 0 Then
                    WhereClause += " AND a.LOAI_HS = '" & drpLoai_HS.SelectedValue.ToString() & "'"
                End If
                If txtMST.Text <> "" Then
                    WhereClause += " AND upper(a.MA_DV) like upper('%" & txtMST.Text.Trim & "%')"
                End If
                If txtTuNgay.Value <> "" Then
                    WhereClause += " AND ( to_char(a.NGAY_NHAN311,'RRRRMMDD') >=" & ConvertDateToNumber(txtTuNgay.Value) & " OR to_char(a.NGAY_HT,'RRRRMMDD') >=" & ConvertDateToNumber(txtTuNgay.Value) & " )"
                End If
                If txtDenNgay.Value <> "" Then
                    WhereClause += " AND (to_char(a.NGAY_NHAN311,'RRRRMMDD') <= " & ConvertDateToNumber(txtDenNgay.Value) & " OR to_char(a.NGAY_HT,'RRRRMMDD') <=" & ConvertDateToNumber(txtDenNgay.Value) & " )"
                End If
                'If txtTenNNT.Text <> "" Then
                '    WhereClause += " AND upper(a.TEN_DV) like upper('%" + txtTenNNT.Text + "%')"
                'End If
                If txtTKNH.Text <> "" Then
                    WhereClause += " AND upper(CASE WHEN A.TRANGTHAI IN ('01','02','03','04','05','06','07','08') THEN A.TAIKHOAN_TH ELSE nvl(A.CUR_TAIKHOAN_TH,A.TAIKHOAN_TH) END ) like upper('%" + txtTKNH.Text.Trim() + "%')" '" AND upper(a.TAIKHOAN_TH) like upper('%" + txtTKNH.Text + "%')"
                End If
                If drpTrangThai.SelectedIndex <> 0 Then
                    WhereClause += " AND a.TRANGTHAI = '" + drpTrangThai.SelectedValue + "'"
                End If
            End If
            If hdfShowMSG.Value.Equals("0") Then
                isThrow = False
            End If
            hdfShowMSG.Value = "1"
            ds = daTCS_HQ247_314.getMSG314(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgrd.CurrentPageIndex = 0
                dtgrd.DataSource = ds.Tables(0)
                dtgrd.DataBind()
            Else
                If isThrow Then
                    clsCommon.ShowMessageBox(Me, "Không tìm thấy bản nghi thỏa mãn điều kiện tìm kiếm")
                End If
            End If
        Catch ex As Exception
            If isThrow Then
                clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
            End If
        End Try
    End Sub
    Private Sub Load_TrangThai()
        Try
            'Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_YEUCAU WHERE TRANG_THAI NOT IN ('01','02','03','05','06','07','08','16') "
            Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_YEUCAU WHERE TRANG_THAI IN ('04','09','10','11','13','15')"
            Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            drpTrangThai.DataSource = ds.Tables(0)
            drpTrangThai.DataTextField = "MOTA"
            drpTrangThai.DataValueField = "TRANG_THAI"
            drpTrangThai.DataBind()
            drpTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái yêu cầu. Lỗi: " & ex.Message)
        End Try
    End Sub
    Private Sub Load_Loai_HS()
        Try
            Dim sql = "SELECT * FROM TCS_DM_LOAI_HS_NNT ORDER BY LOAI_HS"
            Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            drpLoai_HS.DataSource = ds.Tables(0)
            drpLoai_HS.DataTextField = "MOTA"
            drpLoai_HS.DataValueField = "LOAI_HS"
            drpLoai_HS.DataBind()
            drpLoai_HS.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách loại hồ sơ người nộp thuế. Lỗi: " & ex.Message)
        End Try
    End Sub


    Private Sub DM_DiemThu()
        Dim cnDiemThu As New DataAccess
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        strcheckHSC = checkHSC()
        If strcheckHSC = "1" Then
            strSQL = "SELECT a.id, a.id ||'-'||a.name nam FROM tcs_dm_chinhanh a order by a.id "
        Else
            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  a.branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' or a.id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' order by a.id"
        End If
    End Sub

    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        load_dataGrid()
        dtgrd.CurrentPageIndex = 0
        dtgrd.DataBind()
    End Sub

    Protected Sub dtgrd_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgrd.PageIndexChanged
        load_dataGrid()
        dtgrd.CurrentPageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
End Class
