﻿Imports System.Data
Imports Business.NTDT.NNTDT
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao
Imports log4net
Imports Business_HQ

Partial Class HQ247_frmGuiLaiCTTT
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Load_TrangThai()
            'Load_Loai_HS()
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub
    Private Sub load_dataGrid()
        Dim ds As DataSet
        Dim WhereClause As String = " AND A.trangthai in('03','04','10') "
        '" AND TRANGTHAI IN ('" & Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_SEND301_ERROR & "','" & Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_SEND301_OK & "') "
        ' WhereClause &= " AND MA_CN='" & Session.Item("MA_CN_USER_LOGON").ToString() & "'"
        Try
            dtgrd.DataSource = Nothing
            dtgrd.DataBind()
            If txtMST.Text <> "" Then
                WhereClause += " AND upper(A.MA_DV) = upper('" & txtMST.Text.Trim & "')"
            End If
            If txtTuNgay.Value <> "" Then
                WhereClause += " AND to_char(A.NGAYNHANMSG,'RRRRMMDD') >=" & ConvertDateToNumber(txtTuNgay.Value)
            End If
            If txtDenNgay.Value <> "" Then
                WhereClause += " AND to_char(A.NGAYNHANMSG,'RRRRMMDD') <= " & ConvertDateToNumber(txtDenNgay.Value)
            End If
            If txtTenNNT.Text.Length > 0 Then
                WhereClause += " AND upper(A.ten_dv) like  upper('%" & txtTenNNT.Text.Trim() & "%') "
            End If
            'If txtSoCTU.Text.Length > 0 Then
            '    WhereClause += " AND SO_CTU like '%" & txtSoCTU.Text.Trim() & "%' "
            'End If
           
            If txtTK_KH_NH.Text.Trim.Length > 0 Then
                WhereClause += " AND A.taikhoan_th like '%" & txtTK_KH_NH.Text.Trim & "%'"
            End If
            If txtCTCorebank.Text.Trim.Length > 0 Then
                WhereClause += " AND A.so_ct_nh like '%" & txtCTCorebank.Text.Trim & "%'"
            End If
            If cboTrangThai.SelectedIndex > 0 Then
                WhereClause += " AND A.TRANGTHAI = '" & cboTrangThai.SelectedValue.ToString() & "' "
            End If
            ds = nhothu.daCTUHQ201.getMSG201_resend(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgrd.DataSource = ds.Tables(0)
                dtgrd.PageIndex = 0
                dtgrd.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không tìm thấy bản ghi thõa mãn điều kiện tìm kiếm.")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách chứng từ. Lỗi:" & ex.Message)
        End Try
    End Sub
    Private Sub Load_TrangThai()
        Try
            Dim sql As String = "SELECT * FROM TCS_DM_TRANGTHAI_TT_HQ247 WHERE TRANGTHAI IN ('03','04','10')"
            Dim dr As DataSet = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            cboTrangThai.DataSource = dr.Tables(0)
            cboTrangThai.DataTextField = "MOTA"
            cboTrangThai.DataValueField = "TRANGTHAI"
            cboTrangThai.DataBind()
            cboTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái yêu cầu. Lỗi: " & ex.Message)
        End Try
    End Sub
    
    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        load_dataGrid()
    End Sub
    Protected Sub btnReSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReSend.Click
        Try
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If ListCT_NoiDia.Value.Length <= 0 Then
                clsCommon.ShowMessageBox(Me, "Chưa chọn chứng từ để gửi lại! ")
                Return
            End If
            Dim soCTULoi As Integer = 0
            'lblError.Text = ListCT_NoiDia.Value
            Dim strListID As String() = ListCT_NoiDia.Value.Split(",")
            For i As Integer = 0 To strListID.Length - 1
                soCTULoi += ReSENDHQ(strListID(i))
            Next
            clsCommon.ShowMessageBox(Me, "Gửi lại thành công " & (strListID.Length - soCTULoi) & " / " & strListID.Length.ToString() & " chứng từ!")
            load_dataGrid()
        Catch ex As Exception
            lblError.Text = "Loi " & ex.Message
        End Try
    End Sub
    Protected Function ReSENDHQ(ByVal strID As String) As Integer
        Dim trangthai As String = ""
        Dim strErrorNum As String = "0"
        Dim strErrorMes As String = ""
        Try
            
            Dim v_strTransaction_id As String = ""
            Dim pv_so_tn_ct As String = ""
            Dim pv_ngay_tn_ct As String = ""
            Dim ds As DataSet = Business_HQ.NHOTHU.daCTUHQ201.getMSG201Detail(strID)
            If ds Is Nothing Then
                Return 1
            End If
            If ds.Tables.Count <= 0 Then
                Return 1
            End If
            If ds.Tables(0).Rows.Count <= 0 Then
                Return 1
            End If
            Dim strMST As String = ds.Tables(0).Rows(0)("MA_DV").ToString()
            Dim strSO_CTU As String = ds.Tables(0).Rows(0)("SO_CTU").ToString()
            trangthai = ds.Tables(0).Rows(0)("trangthai").ToString()
            CustomsServiceV3.ProcessMSG.guiCTThueHQNHOTHU(strMST, strSO_CTU, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
            If Not strErrorNum.Equals("0") Then
                'set trang thai chung tu la 10
                Business_HQ.NHOTHU.daCTUHQ201.TCS_HQ247_TRANGTHAI_201_SEND301_ERROR(strID, Session.Item("UserName").ToString())
                Return 1
            End If
            
            'doan nay cho doi chieu
            Dim decResult As Decimal = Business_HQ.NHOTHU.daCTUHQ201.TCS_HQ247_TRANGTHAI_201_SEND301_OK(strID, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct, Session.Item("UserName").ToString())
            If decResult > 0 Then
                Return 1
            End If
            Return 0
        Catch ex As Exception
            If strErrorNum <> "0" And trangthai = "03" Then
                Business_HQ.NHOTHU.daCTUHQ201.TCS_HQ247_TRANGTHAI_201_SEND301_ERROR(strID, Session.Item("UserName").ToString())
            End If
            Return 1
        End Try
        Return 1
    End Function
    Protected Sub dtgrd_PageIndexChanging(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dtgrd.PageIndexChanging
        load_dataGrid()
        dtgrd.PageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
End Class
