﻿<%@ WebHandler Language="VB" Class="ViewMessage" %>

Imports System
Imports System.Web

Public Class ViewMessage : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        context.Response.Cache.SetNoStore()
        context.Response.ContentType = "text/plain"
       
        Try
            If HttpContext.Current.Request.Cookies("SEATECHIT_TENDN") Is Nothing Then
                context.Response.Write("Loi khong tim thay message")
                Exit Sub
            End If
            If Not context.Request.QueryString("ID") Is Nothing Then
                Dim index As Integer = Integer.Parse(context.Request.QueryString("IN"))
                ' context.Response.Write(Business_HQ.Common.mdlCommon.fnc_getMessageHQ(context.Request.QueryString("ID"), index))
                Dim strXML_Content As String = Business_HQ.Common.mdlCommon.fnc_getMessageHQ(context.Request.QueryString("ID"), index)
  
                Dim data As Byte() = System.Text.Encoding.UTF8.GetBytes(strXML_Content)
                Dim strFileName As String = "GDICH_" + context.Request.QueryString("ID")
                context.Response.Clear()
                context.Response.BufferOutput = True
                context.Response.ContentType = "text/xml"
                context.Response.AppendHeader("Content-Disposition", "attachment; filename= " + strFileName + ".xml")
                context.Response.AddHeader("Content-Length", data.Length.ToString())
                context.Response.ContentType = "application/octet-stream"
                context.Response.ContentEncoding = Encoding.UTF8
                context.Response.Charset = "UTF-8"
                context.Response.BinaryWrite(data)
                context.Response.[End]()
   
            Else
                context.Response.Write("Loi khong tim thay message")
            End If
        Catch ex As Exception
            context.Response.Write("Loi khong tim thay message")
        End Try
        
        
        
        
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class