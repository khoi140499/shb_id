﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmKiemSoatNNTDT.aspx.vb"
    Inherits="NTDT_frmKiemSoatNNTDT" Title="KIỂM SOÁT THÔNG TIN ĐĂNG KÝ HQ24/7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <script language="javascript" src="../javascript/jquery/script/default/jquery-1.4.3.min.js" type="text/javascript"></script>
    <style type="text/css">
        .txtHoanThienDate {
            width: 200px;
        }

        .txtHoanThien {
            width: 80%;
        }

        .container {
            width: 100%;
            padding-bottom: 50px;
        }
    </style>
    <script type="text/javascript" language="javascript">
        //     function DisableButton() {
        //                document.forms[0].submit();
        //                window.setTimeout("disableButton('" + 
        //                   window.event.srcElement.id + "')", 200);
        //            }
        var strThongtin = '';
        var strHeader = ' <table width="80%"  cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">';
        strHeader += '<tr><td  class="form_label">STT</td><td  class="form_label">Số điện thoại<span class="requiredField">(*)</span></td><td  class="form_label">Email<span class="requiredField">(*)</span></td>';
        strHeader += '</tr>';
        var strFooter = '</table>';
        function showThongtin() {
            if (strThongtin.length > 0) {
                var tbl = strHeader;
                var arr = strThongtin.split('#');
                var i = 0;
                for (i = 0; i < arr.length; i++) {
                    var arrtmp = arr[i].split('|');
                    tbl += '<tr>';
                    tbl += ' <td  class="form_label">' + arrtmp[0] + '</td>';
                    tbl += '<td  class="form_label">' + arrtmp[1] + '</td>';
                    tbl += '<td  class="form_label">' + arrtmp[2] + '</td>';
                    tbl += '</tr>';
                }
                document.getElementById('divThongtin').innerHTML = tbl + strFooter;
            }
        }
        function isValidate()
        {
            var lydoCT = $('#<%=txtLY_DO_CHUYEN_TRA.ClientID%>').val();
            var valid = 0;
            if (lydoCT == null) valid = 1;
            if (lydoCT == undefined) valid = 1;
            if (lydoCT.trim() === "") valid = 1;
            if (lydoCT.length > 200) valid = 2;
            if (valid == 1)
            {
                alert("Xin mời nhập Lý do chuyển trả!");
                $('#<%=txtLY_DO_CHUYEN_TRA.ClientID%>').focus();
                return false;
            }
            if (valid == 2)
            {
                $('#<%=txtLY_DO_CHUYEN_TRA.ClientID%>').focus();
                alert("Vui lòng nhập lý do chuyển trả nhỏ hơn hoặc bằng 200 ký tự.");
                return false;
            }
            return true;
        }
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <p class="pageTitle">KIỂM SOÁT THÔNG TIN ĐĂNG KÝ</p>
    <div class="container">
        <table width="100%">
            <tr>
                <td style="width: 30%" valign="top">
                    <p class="form_label">
                        Trạng thái&nbsp;
                    <asp:DropDownList ID="drpTrangThai" runat="server" Width="82%" CssClass="inputflat" AutoPostBack="true">
                    </asp:DropDownList>
                    </p>
                    <asp:GridView ID="grdDSCT" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        ShowHeader="True" ShowFooter="True" BorderColor="#989898" CssClass="grid_data"
                        PageSize="15" Width="100%">
                        <PagerStyle CssClass="cssPager" />
                        <AlternatingRowStyle CssClass="grid_item_alter" />
                        <HeaderStyle CssClass="grid_header" HorizontalAlign="Left"></HeaderStyle>
                        <RowStyle CssClass="grid_item" />
                        <Columns>
                            <asp:TemplateField HeaderText="Trạng thái">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <%#Eval("TRANGTHAI")%>
                                </ItemTemplate>
                                <FooterTemplate>
                                    Tổng số:
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="MST" FooterText="Tổng số:">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnSelectCT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MA_DV") %>'
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID")%>'
                                        OnClick="btnSelectCT_Click" />
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STK" FooterText="">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <%#DataBinder.Eval(Container.DataItem, "CUR_TAIKHOAN_TH")%>
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Loại hồ sơ">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "TENLOAI_HS") %>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <%#grdDSCT.Rows.Count.ToString%>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Left" BackColor="#E4E5D7"></PagerStyle>
                    </asp:GridView>
                    <p>
                        <asp:Button ID="btnRefresh" runat="server" Text="Làm mới" Font-Bold="true" CssClass="buttonField" /></p>
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="txtThongTin" />
                    <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Trạng thái hồ sơ</td>
                            <td class="form_label" style="color: Red">
                                <asp:Label ID="lblTrangThai" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Số hồ sơ</td>
                            <td class="form_control">
                                <asp:TextBox ID="txtSO_HS" runat="server" CssClass="inputflat txtHoanThienDate" ReadOnly="true" Enabled="false" MaxLength="20"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <asp:HiddenField ID="txtLOAI_HS" runat="server" />
                            <td align="left" style="width: 25%" class="form_label">Loại hồ sơ <span class="requiredField">(*)</span></td>
                            <td class="form_control">
                                <asp:TextBox ID="txtTENLOAI_HS" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="20"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Mã số thuế<span class="requiredField">(*)</span></td>
                            <td class="form_control">
                                <asp:TextBox ID="txtMA_DV" runat="server" CssClass="inputflat txtHoanThienDate" ReadOnly="true" MaxLength="14"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Tên đơn vị XNK<span class="requiredField">(*)</span></td>
                            <td class="form_control">
                                <asp:TextBox ID="txtTEN_DV" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Địa chỉ đơn vị XNK<span class="requiredField">(*)</span></td>
                            <td class="form_control">
                                <asp:TextBox ID="txtDIACHI" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Họ tên NNT<span class="requiredField">(*)</span></td>
                            <td class="form_control">
                                <asp:TextBox ID="txtTenNNT" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="10"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Ngày hiệu lực ủy quyền<span class="requiredField">(*)</span></td>
                            <td class="form_control">
                                <asp:TextBox ID="txtNGAY_HL_UQ" runat="server" CssClass="inputflat txtHoanThienDate" ReadOnly="true" MaxLength="10"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Ngày hết hiệu lực ủy quyền<span class="requiredField">(*)</span></td>
                            <td class="form_control">
                                <asp:TextBox ID="txtNGAY_HHL_UQ" runat="server" CssClass="inputflat txtHoanThienDate" ReadOnly="true" MaxLength="10"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Số CMT<span class="requiredField">(*)</span></td>
                            <td class="form_control">
                                <asp:TextBox ID="txtSO_CMT" runat="server" CssClass="inputflat txtHoanThienDate" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Ngày sinh<span class="requiredField">(*)</span></td>
                            <td class="form_control">
                                <asp:TextBox ID="txtNGAYSINH" runat="server" CssClass="inputflat txtHoanThienDate" ReadOnly="true" MaxLength="10"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Nguyên quán<span class="requiredField">(*)</span></td>
                            <td class="form_control">
                                <asp:TextBox ID="txtNGUYENQUAN" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 25%" class="form_label"><u>Thông tin liên hệ</u></td>
                            <td class="form_control" style="text-align: left">
                                <div id="divThongtin">

                                    <table width="100%" cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
                                        <tr>
                                            <td class="form_label">STT</td>
                                            <td class="form_label">Số điện thoại<span class="requiredField">(*)</span></td>
                                            <td class="form_label">Email<span class="requiredField">(*)</span></td>

                                        </tr>

                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <td align="left" style="width: 25%" class="form_label">Số điện thoại</td>
                            <td class="form_control">
                                <asp:TextBox ID="txtSO_DT" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="15"></asp:TextBox></td>
                        </tr>
                        <tr style="display: none;">
                            <td align="left" style="width: 25%" class="form_label">Email</td>
                            <td class="form_control">
                                <asp:TextBox ID="txtEMAIL" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="50"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Số Seri của chứng thư</td>
                            <td class="form_control">
                                <asp:TextBox ID="txtSERIALNUMBER" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="40"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Đơn vị cấp chứng thư số</td>
                            <td class="form_control">
                                <asp:TextBox ID="txtNOICAP" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Ngày hiệu lực của chứng thư số</td>
                            <td class="form_control">
                                <asp:TextBox ID="txtNGAY_HL" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="19"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Ngày hết hiệu lực chứng thư số</td>
                            <td class="form_control">
                                <asp:TextBox ID="txtNGAY_HHL" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="19"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Mã ngân hàng<span class="requiredField">(*)</span></td>
                            <td class="form_control">
                                <asp:TextBox ID="txtMA_NH_TH" runat="server" CssClass="inputflat txtHoanThienDate" ReadOnly="true" MaxLength="7"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Tên ngân hàng<span class="requiredField">(*)</span></td>
                            <td class="form_control">
                                <asp:TextBox ID="txtTEN_NH_TH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Ngày đăng ký</td>
                            <td class="form_control">
                                <asp:TextBox ID="txtNGAY_NHAN311" runat="server" CssClass="inputflat txtHoanThienDate" ReadOnly="true" MaxLength="19"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">STK đăng ký từ TCHQ<span class="requiredField">(*)</span></td>
                            <td class="form_control">
                                <asp:TextBox ID="txtTAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThienDate" ReadOnly="true" MaxLength="50"></asp:TextBox></td>
                        </tr>
                        <tr style="display: none">
                            <td align="left" style="width: 25%" class="form_label">Tên tài khoản đăng ký</td>
                            <td class="form_control">
                                <asp:TextBox ID="txtTEN_TAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">STK sử dụng ủy quyền<span class="requiredField">(*)</span></td>
                            <td class="form_control">
                                <asp:TextBox ID="txtCHK_TAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThienDate" ReadOnly="true" MaxLength="50"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Tên TK sử dụng ủy quyền<span class="requiredField">(*)</span></td>
                            <td class="form_control">
                                <asp:TextBox ID="txtCHK_TEN_TAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <%--<tr style="display:none">
                            <td align="left" style="width:25%" class="form_label">Nội dung hướng dẫn NNT</td>
                            <td class="form_control"><asp:TextBox ID="txtNOIDUNG_XL" runat="server" CssClass="inputflat txtHoanThien"  MaxLength="4000" TextMode="MultiLine" Height="40"></asp:TextBox></td>
                        </tr>--%>
                        <%--<tr style="display:none">
                            <td align="left" style="width:25%" class="form_label">Ngày hiệu lực<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNGAY_KY_HDUQ" runat="server" CssClass="inputflat txtHoanThienDate" ReadOnly="true" MaxLength="10"></asp:TextBox></td>
                        </tr>--%>
                        <%-- <tr>
                            <td align="left" style="width:25%" class="form_label">Số tài khoản sử dụng NTDT <span class="requiredField">(*)</span></td>
                            <td class="form_control">
                        <asp:GridView ID="grdTKNH" runat="server"  Width="99%" DataKeyNames="ID" ShowHeader="false"
                        AutoGenerateColumns="False" >
                        <Columns>
                            <asp:BoundField DataField="ID" Visible="false"/>
                            <asp:TemplateField HeaderText="Tài khoản ngân hàng">
                                <EditItemTemplate><asp:TextBox ID="txtSoTKNH_Edit" runat="server" Text='<%#Eval("CUR_TAIKHOAN_TH") %>'></asp:TextBox></EditItemTemplate>
                                <ItemTemplate><asp:Label ID="lblSoTKNH" runat="server" Text='<%#Eval("CUR_TAIKHOAN_TH") %>'></asp:Label></ItemTemplate>
                                <ItemStyle Width="25%" CssClass="form_label"/>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tên tài khoản">
                                <ItemTemplate><asp:Label ID="lblCongTy" runat="server" Text='<%#Eval("CUR_TEN_TAIKHOAN_TH") %>'></asp:Label></ItemTemplate>
                                <ItemStyle  CssClass="form_label"/>
                            </asp:TemplateField>
                            
                        
                        </Columns>
                    </asp:GridView>
                            </td>
                        </tr>--%>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Lý do chuyển trả</td>
                            <td class="form_control">
                                <asp:TextBox ID="txtLY_DO_CHUYEN_TRA" TextMode="MultiLine" Height="40" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="200"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;
                            <asp:HiddenField ID="hdfNNTDT_ID" runat="server" />
                                <asp:HiddenField ID="hdrTRANG_THAI" runat="server" />
                                <asp:HiddenField ID="hdfMA_NV" runat="server" />
                                <asp:HiddenField ID="hdfSUBJECT_CERT_NTHUE" runat="server" />
                                <asp:HiddenField ID="hdfMA_GDICH" runat="server" />
                                <asp:HiddenField ID="hdfUpdate_TT" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width='100%' colspan="2" style="color: Red">
                                <b>
                                    <asp:Literal ID="lblStatus" runat="server"></asp:Literal></b>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnDuyet" runat="server" CssClass="buttonField" Text="   Duyệt    " Enabled="false" />&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnChuyenTra" runat="server" OnClientClick="return isValidate();" CssClass="buttonField" Text="Chuyển trả" Enabled="false" />&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </div>
    <script type="text/javascript" charset="utf-8">

        strThongtin = document.getElementById('<%=txtThongTin.ClientID %>').value;
        showThongtin();
    </script>
</asp:Content>

