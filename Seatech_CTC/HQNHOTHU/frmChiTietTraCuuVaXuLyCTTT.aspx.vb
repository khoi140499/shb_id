﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.NHOTHU
Imports Business.Common.mdlCommon
Imports Business.Common
Imports System.IO
Imports System.Xml
Imports CustomsV3.MSG.MSG201
Imports CustomsV3.MSG.MSG314
Imports System.Collections.Generic
Imports System.Diagnostics
Imports Business_HQ
Imports HQ247XulyMSG

Partial Class NTDTHQ247_frmChiTietTraCuuVaXuLyCTTT
    Inherits System.Web.UI.Page
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim v_strRootPath As String = ConfigurationManager.AppSettings("APP_ROOT_PATH").ToString
        Dim iResult As Integer = 0
        Dim strSql As String = " SELECT COUNT (1) KIEMTRA   FROM ("
        strSql &= "  SELECT A.MA_CN           FROM TCS_DM_CHUCNANG A , TCS_PHANQUYEN B , TCS_DM_NHANVIEN C , TCS_SITE_MAP D         "
        strSql &= "   WHERE UPPER(C.TEN_DN)=UPPER('" & Session.Item("UserName").ToString() & "') AND D.STATUS = 1  AND D.NAVIGATEURL = '" + HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") + "' "
        strSql &= "  AND (A.MA_CN=D.NODEID OR (A.MA_CN=D.PARENTNODEID ) )"
        strSql &= "  AND A.MA_CN=B.MA_CN AND B.MA_NHOM =C.MA_NHOM ) "

        If HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") <> "~/pages/frmHomeNV.aspx" Then
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
            If Not Globals.IsNullOrEmpty(dt) Then
                iResult = CInt(dt.Rows(0)(0).ToString())
            End If
            If iResult <= 0 Then
                Response.Redirect("~/pages/frmNoAuthorize.aspx", False)
                Exit Sub
            End If
        End If

        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                If Not Request.Params("ID") Is Nothing Then
                    Dim NNTDT_ID As String = Request.Params("ID").ToString
                    Dim strErrMsg As String = ""
                    If NNTDT_ID <> "" Then
                        LoadCTChiTietAll(NNTDT_ID)
                    End If
                End If
            Catch ex As Exception
                log.Error(ex.Message & "-" & ex.StackTrace) '
                clsCommon.ShowMessageBox(Me, "Lỗi: " & ex.Message)
            End Try
        End If
    End Sub
    Private Sub LoadCTChiTietAll(ByVal pStrID As String)
        Dim tt As String = ""
        Dim cls_songphuong As New CorebankServiceESB.clsSongPhuong
        Try
            Dim tongtien As Decimal = 0
            Dim phi As Decimal = 0
            Dim vat As Decimal = 0
            Dim tongtrichno As Decimal = 0
            txtID.Value = pStrID
            Dim ds As DataSet = daCTUHQ201.gf_getMSG201Detail(pStrID)
            If ds.Tables(0).Rows.Count > 0 Then
                If (ds.Tables(0).Rows(0)("So_CTU").ToString().Trim.Length > 0) Then
                    'load thong tin theo chung tu
                    tt = ds.Tables(0).Rows(0)("trangthai").ToString()
                    txtSo_CT.Text = ds.Tables(0).Rows(0)("So_CTU").ToString()
                    Dim obj As MSG201 = New MSG201()
                    'obj.SETDATA(ds.Tables(0).Rows(0)("MSG_CONTENT").ToString())
                    obj = obj.MSGToObject(ds.Tables(0).Rows(0)("MSG_CONTENT").ToString())
                    '  Dim obj314 As MSG314 = New MSG314(obj.Header.Request_ID)
                    txtMST.Text = obj.Data.Item(0).Ma_DV ' obj.Data.ThongTinChungTu.Ma_DV
                    txtTen_DV.Text = obj.Data.Item(0).Ten_DV ' obj.Data.ThongTinChungTu.Ten_DV
                    txtMaChuong.Text = obj.Data.Item(0).Ma_Chuong ' obj.Data.ThongTinChungTu.Ma_Chuong
                    txtSHKB.Text = obj.Data.Item(0).Ma_KB ' obj.Data.ThongTinChungTu.Ma_KB
                    txtTenKB.Text = obj.Data.Item(0).Ten_KB ' obj.Data.ThongTinChungTu.Ten_KB
                    txtMa_HQ_PH.Text = obj.Data.Item(0).Ma_HQ_PH ' obj.Data.ThongTinChungTu.Ma_HQ_PH
                    txtTen_HQ_PH.Text = obj.Data.Item(0).Ten_HQ_PH
                    txtMa_HQ_CQT.Text = obj.Data.Item(0).Ma_HQ_CQT ' obj.Data.ThongTinChungTu.Ma_HQ_CQT
                    txtTen_HQ_CQT.Text = obj.Data.Item(0).Ten_HQ_PH ' obj.Data.ThongTinChungTu.Ma_HQ_CQT
                    txtMa_NTK.Text = obj.Data.Item(0).Ma_NTK ' obj.Data.ThongTinChungTu.Ma_NTK
                    txtTKKB.Text = obj.Data.Item(0).TKKB ' obj.Data.ThongTinChungTu.TKKB
                    txtNgayLap_CT.Text = "" ' convertDateHQ(obj.Data.ThongTinChungTu.NgayLap_CT)
                    txtNgayTruyen_CT.Text = "" ' convertDateHQ(obj.Data.ThongTinChungTu.NgayTruyen_CT)
                    txtKyHieu.Text = "" ' obj.Data.ThongTinChungTu.KyHieu_CT
                    txtLoai_CT.Text = "" ' obj.Data.ThongTinChungTu.Loai_CT
                    ' convertDateHQ(obj.Data.ThongTinChungTu.Ngay_CT)
                    txtNgayBaoco.Text = ""
                    txtMa_NT.Text = ds.Tables(0).Rows(0)("MA_NT").ToString() ' obj.Data.ThongTinChungTu.Ma_NT
                    txtTyGia.Text = ds.Tables(0).Rows(0)("TY_GIA").ToString() ' obj.Data.ThongTinChungTu.Ty_Gia
                    Dim dsHDR As DataSet = daCTUHQ201.SelectCT_HDR(txtSo_CT.Text)
                    If (Not IsEmptyDataSet(dsHDR)) Then
                        txtNgayBaoco.Text = dsHDR.Tables(0).Rows(0)("ngay_baoco").ToString()
                        txtMST_NNT.Text = dsHDR.Tables(0).Rows(0)("ma_nnthue").ToString()
                        txtTen_NNT.Text = dsHDR.Tables(0).Rows(0)("ten_nnthue").ToString()
                        txtSo_CMT.Text = dsHDR.Tables(0).Rows(0)("SO_CMND").ToString()
                        txtDiaChi.Text = dsHDR.Tables(0).Rows(0)("dc_nnthue").ToString()
                        txtMa_NH_TH.Text = dsHDR.Tables(0).Rows(0)("MA_NH_A").ToString()
                        txtTen_NH_TH.Text = dsHDR.Tables(0).Rows(0)("ten_nh_a").ToString() ' obj314.Document.Data.ThongTin_NNT.ThongTinTaiKhoan.Ten_NH_TH
                        txtTaiKhoan_TH.Text = dsHDR.Tables(0).Rows(0)("tk_kh_nh").ToString()
                        txtTen_TaiKhoan_TH.Text = dsHDR.Tables(0).Rows(0)("ten_kh_nh").ToString()
                        txtSOTKTT.Text = dsHDR.Tables(0).Rows(0)("tk_kh_nh").ToString()
                        txtTenTKTT.Text = dsHDR.Tables(0).Rows(0)("ten_kh_nh").ToString()
                        txtMa_NH_TT.Text = dsHDR.Tables(0).Rows(0)("ma_nh_tt").ToString() '  $('#txtMA_NH_TT').val();
                        txtTen_NH_TT.Text = dsHDR.Tables(0).Rows(0)("ten_nh_tt").ToString() '$('#txtTEN_NH_TT').val();
                        txtMa_NH_GTiep.Text = dsHDR.Tables(0).Rows(0)("ma_nh_b").ToString() ' $('#txtMA_NH_B').val();
                        txtTen_NH_GTiep.Text = dsHDR.Tables(0).Rows(0)("ten_nh_b").ToString() ' $('#txtTenMA_NH_B').val();
                        txtDienGiai.Text = dsHDR.Tables(0).Rows(0)("diengiai_hq").ToString()  'obj.Data.ThongTinChungTu.DienGiai
                        txtNgay_BN.Text = dsHDR.Tables(0).Rows(0)("ngay_baono").ToString() '"ngay_baono" ' convertDateHQ(obj.Data.ThongTinChungTu.Ngay_BN)
                        txtKyHieu.Text = dsHDR.Tables(0).Rows(0)("kyhieu_ct").ToString()
                        txtNgay_CT.Text = mdlCommon.ConvertNumberToDate(dsHDR.Tables(0).Rows(0)("ngay_ct").ToString()).ToString("dd/MM/yyyy")
                    End If
                    txtSoTienCanNop.Text = obj.Data.Item(0).DuNo_TO ' obj.Data.ThongTinChungTu.SoTien_TO
                    If Decimal.TryParse(obj.Data.Item(0).DuNo_TO, tongtien) Then
                        txtTongTien.Text = Business_HQ.Common.mdlCommon.NumberToStringVN2(tongtien)
                    Else
                        txtTongTien.Text = obj.Data.Item(0).DuNo_TO
                    End If
                    lblSotienBangCHu.Text = ConcertCurToText.ConvertCurr.So_chu(obj.Data.Item(0).DuNo_TO)
                    lblSoCTU.Text = txtSo_CT.Text & "-" & ds.Tables(0).Rows(0)("TRANG_THAI").ToString()
                    txtLyDo_Huy.Text = ds.Tables(0).Rows(0)("LYDO").ToString()
                    If ds.Tables(0).Rows(0)("FEEAMT").ToString.Length > 0 Then
                        If Decimal.TryParse(ds.Tables(0).Rows(0)("FEEAMT").ToString, phi) Then
                            txtFEEAMT.Text = Business_HQ.Common.mdlCommon.NumberToStringVN2(phi)
                        Else
                            txtFEEAMT.Text = ds.Tables(0).Rows(0)("FEEAMT").ToString
                        End If
                    Else
                        phi = 0
                        txtFEEAMT.Text = "0"
                    End If
                    If ds.Tables(0).Rows(0)("VATAMT").ToString.Length > 0 Then
                        If Decimal.TryParse(ds.Tables(0).Rows(0)("VATAMT").ToString, vat) Then
                            txtVATAMT.Text = Business_HQ.Common.mdlCommon.NumberToStringVN2(vat)
                        Else
                            txtVATAMT.Text = ds.Tables(0).Rows(0)("VATAMT").ToString
                        End If
                    Else
                        vat = 0
                        txtVATAMT.Text = "0"
                    End If
                    tongtrichno = tongtien + phi + vat
                    txtTongTienTrichNo.Text = Business_HQ.Common.mdlCommon.NumberToStringVN2(tongtrichno)
                    txtTen_HQ_PH.Text = obj.Data.Item(0).Ten_HQ_PH ' Business_HQ.HQ247.daCTUHQ304.getTenHQ_PH(obj.Data.ThongTinChungTu.Ma_HQ_PH)
                    txtTen_HQ_CQT.Text = obj.Data.Item(0).Ten_HQ_PH ' Business_HQ.HQ247.daCTUHQ304.getTenHQ_CQT(obj.Data.ThongTinChungTu.Ma_HQ_CQT, obj.Data.ThongTinChungTu.Ma_HQ_PH)
                    dtgrd.DataSource = obj.convertGNT_CTToDS().Tables(0)
                    dtgrd.DataBind()
                    Dim strTrangthai As String = ds.Tables(0).Rows(0)("TRANGTHAI").ToString()
                    If Not strTrangthai.Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_201_SEND301_OK) And _
                    Not strTrangthai.Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_201_CHEKER213_RESPONSE_TUCHOI) Then
                        btnChuyenKS.Visible = True
                    Else
                        btnChuyenKS.Visible = False
                    End If
                Else
                    Dim obj As MSG201 = New MSG201()
                    'obj.SETDATA(ds.Tables(0).Rows(0)("MSG_CONTENT").ToString())
                    obj = obj.MSGToObject(ds.Tables(0).Rows(0)("MSG_CONTENT").ToString())
                    Dim obj314 As MSG314 = New MSG314(obj.Header.Request_ID)
                    txtMST.Text = obj.Data.Item(0).Ma_DV ' obj.Data.ThongTinChungTu.Ma_DV
                    txtTen_DV.Text = obj.Data.Item(0).Ten_DV ' obj.Data.ThongTinChungTu.Ten_DV
                    txtMaChuong.Text = obj.Data.Item(0).Ma_Chuong ' obj.Data.ThongTinChungTu.Ma_Chuong
                    txtSHKB.Text = obj.Data.Item(0).Ma_KB ' obj.Data.ThongTinChungTu.Ma_KB
                    txtTenKB.Text = obj.Data.Item(0).Ten_KB ' obj.Data.ThongTinChungTu.Ten_KB
                    txtMa_HQ_PH.Text = obj.Data.Item(0).Ma_HQ_PH ' obj.Data.ThongTinChungTu.Ma_HQ_PH
                    txtMa_HQ_CQT.Text = obj.Data.Item(0).Ma_HQ_CQT ' obj.Data.ThongTinChungTu.Ma_HQ_CQT
                    txtMa_NTK.Text = obj.Data.Item(0).Ma_NTK ' obj.Data.ThongTinChungTu.Ma_NTK
                    txtTKKB.Text = obj.Data.Item(0).TKKB ' obj.Data.ThongTinChungTu.TKKB
                    txtNgayLap_CT.Text = "" ' convertDateHQ(obj.Data.ThongTinChungTu.NgayLap_CT)
                    txtNgayTruyen_CT.Text = "" ' convertDateHQ(obj.Data.ThongTinChungTu.NgayTruyen_CT)
                    txtKyHieu.Text = "" ' obj.Data.ThongTinChungTu.KyHieu_CT
                    txtSo_CT.Text = "" ' obj.Data.ThongTinChungTu.So_CT
                    txtLoai_CT.Text = "" ' obj.Data.ThongTinChungTu.Loai_CT
                    txtNgay_BN.Text = "" ' convertDateHQ(obj.Data.ThongTinChungTu.Ngay_BN)
                    txtNgay_CT.Text = "" ' convertDateHQ(obj.Data.ThongTinChungTu.Ngay_CT)
                    txtNgayBaoco.Text = ""
                    txtMa_NT.Text = ds.Tables(0).Rows(0)("MA_NT").ToString() ' obj.Data.ThongTinChungTu.Ma_NT
                    txtTyGia.Text = ds.Tables(0).Rows(0)("TY_GIA").ToString() ' obj.Data.ThongTinChungTu.Ty_Gia
                    If (Not obj314.Document Is Nothing) Then
                        txtMST_NNT.Text = obj314.Document.Data.Ma_DV
                        txtTen_NNT.Text = obj314.Document.Data.ThongTin_NNT.Ho_Ten
                        txtSo_CMT.Text = obj314.Document.Data.ThongTin_NNT.So_CMT
                        txtDiaChi.Text = obj314.Document.Data.DiaChi
                        'txtTT_Khac.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.TT_Khac
                        txtMa_NH_TH.Text = obj314.Document.Data.ThongTin_NNT.ThongTinTaiKhoan.Ma_NH_TH
                        txtTen_NH_TH.Text = obj314.Document.Data.ThongTin_NNT.ThongTinTaiKhoan.Ten_NH_TH
                        txtTaiKhoan_TH.Text = obj314.Document.Data.ThongTin_NNT.ThongTinTaiKhoan.TaiKhoan_TH
                        txtTen_TaiKhoan_TH.Text = obj314.Document.Data.ThongTin_NNT.ThongTinTaiKhoan.Ten_TaiKhoan_TH
                        txtSOTKTT.Text = obj314.Document.Data.ThongTin_NNT.ThongTinTaiKhoan.TaiKhoan_TH
                        txtTenTKTT.Text = obj314.Document.Data.ThongTin_NNT.ThongTinTaiKhoan.Ten_TaiKhoan_TH
                    End If
                    txtDienGiai.Text = "" 'obj.Data.ThongTinChungTu.DienGiai
                    txtSoTienCanNop.Text = obj.Data.Item(0).DuNo_TO ' obj.Data.ThongTinChungTu.SoTien_TO
                    If Decimal.TryParse(obj.Data.Item(0).DuNo_TO, tongtien) Then
                        txtTongTien.Text = Business_HQ.Common.mdlCommon.NumberToStringVN2(tongtien)
                    Else
                        txtTongTien.Text = obj.Data.Item(0).DuNo_TO
                    End If
                    lblSotienBangCHu.Text = ConcertCurToText.ConvertCurr.So_chu(obj.Data.Item(0).DuNo_TO)
                    lblSoCTU.Text = txtSo_CT.Text & "-" & ds.Tables(0).Rows(0)("TRANG_THAI").ToString()
                    txtLyDo_Huy.Text = ds.Tables(0).Rows(0)("LYDO").ToString()
                    If ds.Tables(0).Rows(0)("FEEAMT").ToString.Length > 0 Then
                        If Decimal.TryParse(ds.Tables(0).Rows(0)("FEEAMT").ToString, phi) Then
                            txtFEEAMT.Text = Business_HQ.Common.mdlCommon.NumberToStringVN2(phi)
                        Else
                            txtFEEAMT.Text = ds.Tables(0).Rows(0)("FEEAMT").ToString
                        End If
                    Else
                        phi = 0
                        txtFEEAMT.Text = "0"
                    End If
                    If ds.Tables(0).Rows(0)("VATAMT").ToString.Length > 0 Then
                        If Decimal.TryParse("", vat) Then
                            txtVATAMT.Text = Business_HQ.Common.mdlCommon.NumberToStringVN2(vat)
                        Else
                            txtVATAMT.Text = ds.Tables(0).Rows(0)("VATAMT").ToString
                        End If
                    Else
                        vat = 0
                        txtVATAMT.Text = "0"
                    End If
                    tongtrichno = tongtien + phi + vat
                    txtTongTienTrichNo.Text = Business_HQ.Common.mdlCommon.NumberToStringVN2(tongtrichno)
                    txtTen_HQ_PH.Text = obj.Data.Item(0).Ten_HQ_PH ' Business_HQ.HQ247.daCTUHQ304.getTenHQ_PH(obj.Data.ThongTinChungTu.Ma_HQ_PH)
                    txtTen_HQ_CQT.Text = obj.Data.Item(0).Ten_HQ_PH ' Business_HQ.HQ247.daCTUHQ304.getTenHQ_CQT(obj.Data.ThongTinChungTu.Ma_HQ_CQT, obj.Data.ThongTinChungTu.Ma_HQ_PH)
                    dtgrd.DataSource = obj.convertGNT_CTToDS().Tables(0)
                    dtgrd.DataBind()
                    Dim dsx As DataSet
                    Dim strResult As String = cls_songphuong.ValiadSHB(obj.Data.Item(0).Ma_KB)

                    Dim strNHB As String = ""
                    Dim strACC_NHB As String = ""
                    If strResult.Length > 8 Then
                        Dim strarr() As String
                        strarr = strResult.Split(";")
                        strNHB = strarr(0).ToString
                        strACC_NHB = strarr(1).ToString
                    End If


                    If strNHB.Length = 8 Then
                        dsx = Business_HQ.HQ247.daCTUHQ304.getNHTrucTiepGianTiepKB_SHB(obj.Data.Item(0).Ma_KB, strNHB)
                    Else
                        dsx = Business_HQ.HQ247.daCTUHQ304.getNHTrucTiepGianTiep(obj.Data.Item(0).Ma_KB)
                    End If
                    If Not dsx Is Nothing Then
                        If dsx.Tables.Count > 0 Then
                            If dsx.Tables(0).Rows.Count > 0 Then
                                txtMa_NH_TT.Text = dsx.Tables(0).Rows(0)("MA_TRUCTIEP").ToString() '  $('#txtMA_NH_TT').val();
                                txtTen_NH_TT.Text = dsx.Tables(0).Rows(0)("TEN_TRUCTIEP").ToString()  '$('#txtTEN_NH_TT').val();
                                txtMa_NH_GTiep.Text = dsx.Tables(0).Rows(0)("MA_GIANTIEP").ToString() ' $('#txtMA_NH_B').val();
                                txtTen_NH_GTiep.Text = dsx.Tables(0).Rows(0)("TEN_GIANTIEP").ToString() ' $('#txtTenMA_NH_B').val();
                            End If
                        End If
                    End If
                    txtDienGiai.Text = XULY_MSG201.PGB_buildRemark1(obj)
                    Dim strTrangthai As String = ds.Tables(0).Rows(0)("TRANGTHAI").ToString()
                    If Not strTrangthai.Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_201_SEND301_OK) And _
                    Not strTrangthai.Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_201_CHEKER213_RESPONSE_TUCHOI) Then
                        btnChuyenKS.Visible = True
                    Else
                        btnChuyenKS.Visible = False
                    End If
                End If
            End If
            'Me.txtMatKhau.Focus()
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)
            log.Error(sbErrMsg.ToString())
           
        Finally
            btnXLDC.Visible = False
            If tt = "04" Then btnXLDC.Visible = True
        End Try
    End Sub
    Public Function convertDateHQ(ByVal strDate As String) As String
        Dim str As String = ""
        Try
            str = strDate.Substring(8, 2) & "/" & strDate.Substring(5, 2) & "/" & strDate.Substring(0, 4)
            If strDate.Trim.Length > 10 Then
                str &= strDate.Substring(10, strDate.Length - 10)
            End If
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)
            log.Error(sbErrMsg.ToString())
        End Try
        Return str
    End Function
    Protected Sub btnChuyenKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChuyenKS.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        btnChuyenKS.Enabled = False
        Try
            Dim strSender_Code As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString()
            Dim obj As XULY_MSG201 = New XULY_MSG201(Session.Item("UserName").ToString(), Session.Item("User").ToString(), strSender_Code)
            clsCommon.ShowMessageBox(Me, obj.ProcessData(txtID.Value))
            LoadCTChiTietAll(txtID.Value)
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình xử lý chứng từ 11.3.7")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)
            log.Error(sbErrMsg.ToString())
            
        End Try
        btnChuyenKS.Enabled = True
    End Sub
    Protected Sub btnXLDC_Click1(sender As Object, e As EventArgs) Handles btnXLDC.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        btnXLDC.Enabled = False
        Try
            Dim strSender_Code As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString()
            Dim obj As XULY_MSG201 = New XULY_MSG201(Session.Item("UserName").ToString(), Session.Item("User").ToString(), strSender_Code)
            clsCommon.ShowMessageBox(Me, obj.InsertDoiChieu201(txtID.Value))
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình xử lý chứng từ 11.3.7")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)
            log.Error(sbErrMsg.ToString())
            
        End Try
        btnXLDC.Enabled = True
    End Sub
End Class
