﻿Imports System.Data
Imports Business.NTDT.NNTDT

Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao
Imports log4net
Imports Business_HQ.Common.mdlCommon

Partial Class HQ247_frmTraCuuCTTT
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            If Not Session("WhereClause") Is Nothing Then
                Session.Remove("WhereClause")
            End If
            Load_TrangThai()
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub
    Private Sub DM_DiemThu()

        Dim dsDiemThu As DataSet
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        'Dim str_Ma_Nhom As String = ""
        Dim dt As New DataTable
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        If strcheckHSC = "1" Then
            'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
            ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
            strSQL = "SELECT a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a order by a.id "
        Else
            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  a.branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' or a.id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' order by a.id"
        End If
        Try
            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            dt = dsDiemThu.Tables(0)
            'cboDiemThu.DataSource = dt
            'cboDiemThu.DataTextField = "name"
            'cboDiemThu.DataValueField = "id"
            'cboDiemThu.DataBind()
            If strcheckHSC = "1" Then
                'clsCommon.ComboBoxWithValue(cboDiemThu, dsDiemThu, 1, 0, "Tất cả")
            End If
        Catch ex As Exception

            Throw ex
        Finally

        End Try
    End Sub
    Private Sub load_dataGrid(ByRef _show As String)
        Dim ds As DataSet
        Dim WhereClause As String = "   "
        Try
            dtgrd.DataSource = Nothing
            dtgrd.DataBind()
            If txtMST.Text <> "" Then
                WhereClause += " AND upper(a.MA_DV) like upper('%" & txtMST.Text.Trim & "%')"
            End If
            If txtTen_NNT.Text <> "" Then
                WhereClause += " AND upper(a.TEN_DV) like upper('%" & txtTen_NNT.Text.Trim & "%')"
            End If
            If txtSoTK.Text <> "" Then
                WhereClause += " AND upper(a.TAIKHOAN_TH) like upper('%" & txtSoTK.Text.Trim & "%')"
            End If
            If txtTuNgay.Value <> "" Then
                WhereClause += " AND to_char(a.NGAYNHANMSG,'RRRRMMDD') >=" & ConvertDateToNumber(txtTuNgay.Value)
            End If
            If txtDenNgay.Value <> "" Then
                WhereClause += " AND to_char(a.NGAYNHANMSG,'RRRRMMDD') <= " & ConvertDateToNumber(txtDenNgay.Value)
            End If
            If txtSoCTU.Text.Length > 0 Then
                WhereClause += " AND a.SO_CTU = '" & txtSoCTU.Text.Trim() & "'"
            End If
            If cboTrangThai.SelectedIndex > 0 Then
                WhereClause += " AND A.TRANGTHAI = '" & cboTrangThai.SelectedValue.ToString() & "' "
            End If
            ds = Business_HQ.NHOTHU.daCTUHQ201.getMSG201_Tracuu(WhereClause)
            Session("WhereClause") = WhereClause
            If Not IsEmptyDataSet(ds) Then
                SumTien.Value = "0"
                Dim decTotal As Decimal = 0
                For Each vrow As DataRow In ds.Tables(0).Rows
                    decTotal += Decimal.Parse(vrow("HQ_SOTIEN_TO").ToString())
                Next
                SumTien.Value = Business_HQ.Common.mdlCommon.NumberToStringVN(decTotal.ToString())
                dtgrd.DataSource = ds.Tables(0)
                dtgrd.PageIndex = 0
                dtgrd.DataBind()
            Else
                dtgrd.DataSource = Nothing
                dtgrd.PageIndex = 0
                dtgrd.DataBind()
                _show = "1"
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub
    Private Sub Load_TrangThai()
        Try
            Dim sql As String = "SELECT * FROM TCS_DM_TRANGTHAI_TT_HQ247 " '"SELECT * FROM TCS_DM_TRANGTHAI_TT_HQ247 WHERE TRANGTHAI NOT IN ('08','09') "
            Dim dr As DataSet = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            cboTrangThai.DataSource = dr.Tables(0)
            cboTrangThai.DataTextField = "MOTA"
            cboTrangThai.DataValueField = "TRANGTHAI"
            cboTrangThai.DataBind()
            cboTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái yêu cầu. Lỗi: " & ex.Message)
        End Try
    End Sub
    Protected Sub dtgrd_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dtgrd.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            For i As Integer = 0 To 5
                e.Row.Cells.RemoveAt(1)
                e.Row.Cells(0).ColumnSpan = 7
            Next
            Dim lblTotalCT As Label = DirectCast(e.Row.FindControl("lblTongSotien"), Label)
            lblTotalCT.Text = SumTien.Value
        End If
    End Sub
    Private Sub Load_Loai_HS()
        Try
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách loại hồ sơ người nộp thuế. Lỗi: " & ex.Message)
        End Try
    End Sub
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        Dim show As String = ""
        load_dataGrid(show)
        If show = "1" Then
            clsCommon.ShowMessageBox(Me, "Không tìm thấy bản ghi thỏa mãn điều kiện tìm kiếm.")
        End If
    End Sub

    Protected Sub btnReSend_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=DanhSachChungtuThanhToanHQ247.xls")
            Response.Charset = "utf-8"
            Response.ContentType = "application/vnd.ms-excel"
            Using sw1 As New StringWriter()
                Dim hw1 As New HtmlTextWriter(sw1)
                dtgrd.RenderControl(hw1)
                Dim style As String = "<style> .textmode { } </style>"
                Response.Write(style)
                Response.Output.Write(sw1.ToString)
                Response.Flush()
                Response.End()
            End Using
        Catch ex As Exception
            lblError.Text = "Loi " & ex.Message
        End Try
    End Sub
   
    Protected Sub dtgrd_PageIndexChanging(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dtgrd.PageIndexChanging
        Dim show As String = ""
        load_dataGrid(show)
        dtgrd.PageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
    End Sub

    Protected Sub btnReSend_Click1(sender As Object, e As EventArgs) Handles btnReSend.Click
        Try
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            '----------------------------------------------------------------
            Dim WhereClause As String = Session("WhereClause")
            If WhereClause Is Nothing Then
                Return
            End If
            If WhereClause = "" Then
                Return
            End If
            Dim ds As DataSet = Business_HQ.NHOTHU.daCTUHQ201.getMSG201_Tracuu(WhereClause)
            exportView1.DataSource = ds.Tables(0)
            'exportView1.PageIndex = 0
            exportView1.DataBind()
            Export()
        Catch ex As Exception
            lblError.Text = "Loi " & ex.Message
        End Try
    End Sub
    Sub Export()
        exportView1.Visible = True
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=ExportChungtuThanhToanHQNHOTHU.xls")
        Response.Charset = "utf-8"
        Response.ContentType = "application/vnd.ms-excel"
        Using sw1 As New StringWriter()
            Dim hw1 As New HtmlTextWriter(sw1)
            exportView1.RenderControl(hw1)
            Response.Output.Write(sw1.ToString)
            Response.Flush()
            Response.End()
        End Using
        exportView1.Visible = False
    End Sub
End Class
