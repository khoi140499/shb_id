﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.HQ247.NNTDT
Imports Business.Common.mdlCommon
Imports Business.BaoCao
Imports Business.Common
Imports System.IO
Imports System.Xml
Imports CustomsV3.MSG.MSG304
Partial Class HQ247_frmThucHienYCTTHQ
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then


            If Not Request.Params("ID") Is Nothing Then
                Dim strID As String = Request.Params("ID").ToString
                LoadCTUDetail(strID)


            End If
        End If

    End Sub
    Public Sub LoadCTUDetail(ByVal pStrID As String)
        Try
            txtID.Value = pStrID

            Dim ds As DataSet = Business_HQ.HQ247.daCTUHQ304.getMSG304Detail(pStrID)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim obj As MSG304 = New MSG304()
                obj.SETDATA(ds.Tables(0).Rows(0)("MSG_CONTENT").ToString())
                txtLyDo_Huy.Text = ds.Tables(0).Rows(0)("LYDO").ToString()
                txtLydo_ChuyenTra.Text = ds.Tables(0).Rows(0)("LYDOCHUYENTRA").ToString()
                txtMST.Text = obj.Data.ThongTinChungTu.Ma_DV
                txtTen_DV.Text = obj.Data.ThongTinChungTu.Ten_DV
                txtMaChuong.Text = obj.Data.ThongTinChungTu.Ma_Chuong
                txtSHKB.Text = obj.Data.ThongTinChungTu.Ma_KB
                txtTenKB.Text = obj.Data.ThongTinChungTu.Ten_KB
                txtMa_HQ_PH.Text = obj.Data.ThongTinChungTu.Ma_HQ_PH
                txtMa_HQ_CQT.Text = obj.Data.ThongTinChungTu.Ma_HQ_CQT
                txtMa_NTK.Text = obj.Data.ThongTinChungTu.Ma_NTK
                txtTKKB.Text = obj.Data.ThongTinChungTu.TKKB
                txtNgayLap_CT.Text = convertDateHQ(obj.Data.ThongTinChungTu.NgayLap_CT)
                txtNgayTruyen_CT.Text = convertDateHQ(obj.Data.ThongTinChungTu.NgayTruyen_CT)
                txtKyHieu.Text = obj.Data.ThongTinChungTu.KyHieu_CT
                txtSo_CT.Text = obj.Data.ThongTinChungTu.So_CT
                txtLoai_CT.Text = obj.Data.ThongTinChungTu.Loai_CT
                txtNgay_BN.Text = convertDateHQ(obj.Data.ThongTinChungTu.Ngay_BN)
                txtNgay_CT.Text = convertDateHQ(obj.Data.ThongTinChungTu.Ngay_CT)
                txtMa_NT.Text = obj.Data.ThongTinChungTu.Ma_NT
                txtTyGia.Text = obj.Data.ThongTinChungTu.Ty_Gia
                txtMST_NNT.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.Ma_ST
                txtTen_NNT.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.Ten_NNT
                txtSo_CMT.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.So_CMT
                txtDiaChi.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.DiaChi
                txtDienGiai.Text = obj.Data.ThongTinChungTu.DienGiai
                txtTT_Khac.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.TT_Khac
                txtMa_NH_TH.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ma_NH_TH
                txtTen_NH_TH.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ten_NH_TH
                txtSoTienCanNop.Text = obj.Data.ThongTinChungTu.SoTien_TO
                txtTongTien.Text = obj.Data.ThongTinChungTu.SoTien_TO
                txtTen_HQ_PH.Text = Business_HQ.HQ247.daCTUHQ304.getTenHQ_PH(obj.Data.ThongTinChungTu.Ma_HQ_PH)
                txtTen_HQ_CQT.Text = Business_HQ.HQ247.daCTUHQ304.getTenHQ_CQT(obj.Data.ThongTinChungTu.Ma_HQ_CQT, obj.Data.ThongTinChungTu.Ma_HQ_PH)
                Dim dsx As DataSet = Business_HQ.HQ247.daCTUHQ304.getNHTrucTiepGianTiep(obj.Data.ThongTinChungTu.Ma_KB)

                If Not dsx Is Nothing Then
                    If dsx.Tables.Count > 0 Then
                        If dsx.Tables(0).Rows.Count > 0 Then
                            txtMa_NH_TT.Text = dsx.Tables(0).Rows(0)("MA_TRUCTIEP").ToString() '  $('#txtMA_NH_TT').val();
                            txtTen_NH_TT.Text = dsx.Tables(0).Rows(0)("TEN_TRUCTIEP").ToString()  '$('#txtTEN_NH_TT').val();
                            txtMa_NH_GTiep.Text = dsx.Tables(0).Rows(0)("MA_GIANTIEP").ToString() ' $('#txtMA_NH_B').val();
                            txtTen_NH_GTiep.Text = dsx.Tables(0).Rows(0)("TEN_GIANTIEP").ToString() ' $('#txtTenMA_NH_B').val();

                        End If
                    End If
                End If


                lblSotienBangCHu.Text = ConcertCurToText.ConvertCurr.So_chu(obj.Data.ThongTinChungTu.SoTien_TO)
                lblSoCTU.Text = txtSo_CT.Text & "-" & ds.Tables(0).Rows(0)("TRANG_THAI").ToString()
                txtTaiKhoan_TH.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.TaiKhoan_TH
                txtTen_TaiKhoan_TH.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ten_TaiKhoan_TH
                dtgrd.DataSource = obj.Data.ThongTinChungTu.convertGNT_CTToDS().Tables(0)
                dtgrd.DataBind()


                'moi nhan 01
                If ds.Tables(0).Rows(0)("TRANGTHAI").ToString().Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_MOINHAN) Then
                    btnChuyenKS.Enabled = True
                    btnHuy.Enabled = True
                    btnCheckTK.Enabled = True
                    checktaiKhoanNganHang()
                    'CHUYEN TRA
                ElseIf ds.Tables(0).Rows(0)("TRANGTHAI").ToString().Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_CHUYENTRA) Then
                    btnChuyenKS.Enabled = True
                    btnHuy.Enabled = True
                    btnCheckTK.Enabled = True
                    checktaiKhoanNganHang()
                Else
                    btnChuyenKS.Enabled = False
                    btnHuy.Enabled = False
                    btnCheckTK.Enabled = False
                End If



            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnCheckTK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckTK.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        checktaiKhoanNganHang()
    End Sub
    Public Sub checktaiKhoanNganHang()
        lblErrorTaiKhoanTH.Text = ""
        Try
            Dim decResult As Decimal = Business_HQ.HQ247.daCTUHQ304.CheckValidTAIKHOAN_TH(txtMST.Text, txtTaiKhoan_TH.Text)
            If decResult > 0 Then
                lblErrorTaiKhoanTH.Text = "Lỗi Tài khoản ngân hàng không đúng hoặc trạng thái hồ sơ không hợp lệ"
                txtLyDo_Huy.Text = "Lỗi Tài khoản ngân hàng không đúng hoặc trạng thái hồ sơ không hợp lệ"
                btnChuyenKS.Enabled = False
                btnHuy.Enabled = True
                Return
            End If
        Catch ex As Exception
            txtLyDo_Huy.Text = "Lỗi Tài khoản ngân hàng không đúng hoặc trạng thái hồ sơ không hợp lệ"
            lblErrorTaiKhoanTH.Text = "Lỗi Tài khoản ngân hàng không đúng hoặc trạng thái hồ sơ không hợp lệ"
            btnChuyenKS.Enabled = False
            btnHuy.Enabled = True
            Return
        End Try
        Dim strCoreOn As String = "0"
        'doan nay neu lam ket noi ngan hang thi cal den core bank de check so du
        If strCoreOn.Equals("1") Then
        Else
            txtCurrTK.Text = txtTaiKhoan_TH.Text
            txtCurTenTK.Text = txtTen_NNT.Text
            txtSoDuTK.Text = "9999999999"
        End If
        btnChuyenKS.Enabled = True
        btnHuy.Enabled = True
    End Sub
    Public Function convertDateHQ(ByVal strDate As String) As String

        Dim str As String = ""
        Try
            str = strDate.Substring(8, 2) & "/" & strDate.Substring(5, 2) & "/" & strDate.Substring(0, 4)
            If strDate.Trim.Length > 10 Then
                str &= strDate.Substring(10, strDate.Length - 10)
            End If
        Catch ex As Exception

        End Try

        Return str
    End Function
    Protected Sub btnChuyenKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChuyenKS.Click

        Try
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            Dim decResult As Decimal = Business_HQ.HQ247.daCTUHQ304.CheckValidTAIKHOAN_TH(txtMST.Text, txtTaiKhoan_TH.Text)
            If decResult > 0 Then
                clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi: " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()))
                Return
            End If
            decResult = Business_HQ.HQ247.daCTUHQ304.checkTrangthaiMSG304(txtID.Value, Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MOINHAN)
            If decResult > 0 Then
                clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi: " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()))
                Return
            End If
            If txtLyDo_Huy.Text.Length = 0 Then
                txtLyDo_Huy.Text = "Đồng ý thực hiện thanh toán"
            End If
            decResult = Business_HQ.HQ247.daCTUHQ304.TCS_HQ247_TRANGTHAI_304_MAKER213(txtID.Value, txtLyDo_Huy.Text, Session.Item("UserName").ToString())
            If decResult > 0 Then
                clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi: " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()))
                Return
            End If
            btnChuyenKS.Enabled = False
            btnHuy.Enabled = False
            clsCommon.ShowMessageBox(Me, "Chuyển kiểm thành công chứng từ: " & txtSo_CT.Text)
            LoadCTUDetail(txtID.Value)
        Catch ex As Exception

            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi: " & ex.Message)
        End Try
    End Sub
    Protected Sub btnHuy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHuy.Click

        Try
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If txtLyDo_Huy.Text.Length <= 0 Then
                clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát hủy yêu cầu lỗi: chưa nhập thông tin từ chối yêu cầu thanh toán ")
                Return
            End If
            Dim decResult As Decimal = Business_HQ.HQ247.daCTUHQ304.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI(txtID.Value, txtLyDo_Huy.Text, txtTaiKhoan_TH.Text)
            If decResult > 0 Then
                clsCommon.ShowMessageBox(Me, "Chuyển kiểm hủy yêu cầu soát lỗi: " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()))
                Return
            End If

            btnChuyenKS.Enabled = False
            btnHuy.Enabled = False
            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát hủy thành công chứng từ: " & txtSo_CT.Text)
            LoadCTUDetail(txtID.Value)
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát hủy lỗi lỗi: " & ex.Message)
        End Try
    End Sub

    Protected Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click

        Try
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            Response.Redirect("~/HQ247/frmTiepNhanYCTT.aspx", False)

        Catch ex As Exception
            Response.Redirect("~/pages/frmLogin.aspx", False)
        End Try
    End Sub
End Class
