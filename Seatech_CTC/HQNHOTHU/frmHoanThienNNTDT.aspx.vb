﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.nhothu
Imports Business.Common.mdlCommon
Imports Business.BaoCao
Imports Business.Common
Imports System.IO
Imports System.Xml
Imports CorebankServiceESB
''' <summary>
''' Hoàn thiện thông tin đăng ký của NNT
''' Chức năng Tạo mới: Chỉ dùng trong trường hợp NNT đăng kí thông tin ủy quyền trích nợ tài khoản tại NHTM 
''' Chức năng Ghi và chuyển KS: sử dụng đối với những đăng ký ở trạng thái ĐÃ DUYỆT YÊU CẦU
''' </summary>
''' <remarks>Số tài khoản phải được kiểm tra và lấy được Tên tài khoản</remarks>
Partial Class HQ247_frmHoanThienNNTDT
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        txtMA_DV.Enabled = False
        txtTEN_DV.Enabled = False
        txtDIACHI.Enabled = False
        txtHO_TEN.Enabled = False
        txtSO_CMT.Enabled = False
        txtNGAYSINH.Enabled = False
        txtNGUYENQUAN.Enabled = False
        txtSO_DT.Enabled = False
        txtEMAIL.Enabled = False
        txtMA_NH_TH.Enabled = False
        txtTEN_NH_TH.Enabled = False
        'txtNoiDung_XL.Enabled = False
        txtCHK_TAIKHOAN_TH.Enabled = False
        'btnCheckCK.Enabled = False
        txtMA_NH_TH.Enabled = False
        txtTEN_NH_TH.Enabled = False
        btnLuuSoDT.Visible = True
        btnLuuEmail.Visible = True
        txtNGAY_HL_UQ.Enabled = False
        txtNGAY_HHL_UQ.Enabled = False
        'Kết nối với BankCore để lấy ra thông tin Tên tài khoản
        'Kết nối với BankCore để lấy ra tên ngân hàng thụ hưởng và mã ngân hàng thụ hưởng hoặc để mặc định
        'huynt_test
        If Not IsPostBack Then
            If Not Request.Params("ID") Is Nothing Then
                'Session.Add("THONGTINLIENHE", "")
                Dim strID As String = Request.Params("ID").ToString
                hdfNNTDT_ID.Value = strID
                dieuChinhConfirm.Value = "Cancel"
                btnLuuSoDT.Visible = False
                btnLuuEmail.Visible = False
                Dim ds As DataSet = daTCS_HQ247_314.getMakerMSG213(strID)
                'Nếu không tìm thấy hồ sơ trong bảng HQ_311 thì tìm tiếp trong bảng HQ
                If ds.Tables(0).Rows.Count <= 0 Then
                    ds = Business_HQ.HQ247.TCS_DM_NTDT_HQ.getDangKyNNT_HQ(strID)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    'If ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("04") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("10") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("11") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("12") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("13") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("14") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("15") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("16") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("09") Then
                    lblTrangThai.Text = Business_HQ.Common.mdlCommon.fnc_GET_TENTRANGTHAI_HS(ds.Tables(0).Rows(0)("TRANGTHAI").ToString())
                    txtSO_HS.Text = ds.Tables(0).Rows(0)("So_HS").ToString()
                    txtLoai_HS.Value = ds.Tables(0).Rows(0)("Loai_HS").ToString()
                    txtID.Value = ds.Tables(0).Rows(0)("ID").ToString()
                    txtTenLoai_HS.Text = ds.Tables(0).Rows(0)("TenLoai_HS").ToString()
                    txtMA_DV.Text = ds.Tables(0).Rows(0)("Ma_DV").ToString()
                    txtTEN_DV.Text = ds.Tables(0).Rows(0)("Ten_DV").ToString()
                    txtDIACHI.Text = ds.Tables(0).Rows(0)("DiaChi").ToString()
                    txtHO_TEN.Text = ds.Tables(0).Rows(0)("Ho_Ten").ToString()
                    txtSO_CMT.Text = ds.Tables(0).Rows(0)("So_CMT").ToString()
                    txtNGAYSINH.Text = ds.Tables(0).Rows(0)("NgaySinh").ToString()
                    txtNGUYENQUAN.Text = ds.Tables(0).Rows(0)("NguyenQuan").ToString()
                    txtSO_DT.Text = ds.Tables(0).Rows(0)("So_DT").ToString()
                    txtEMAIL.Text = ds.Tables(0).Rows(0)("Email").ToString()
                    txtSERIALNUMBER.Text = ds.Tables(0).Rows(0)("SerialNumber").ToString()
                    txtNOICAP.Text = ds.Tables(0).Rows(0)("NOICAP").ToString()
                    txtNGAY_HL.Text = ds.Tables(0).Rows(0)("Ngay_HL").ToString()
                    txtNGAY_HHL.Text = ds.Tables(0).Rows(0)("Ngay_HHL").ToString()
                    txtMA_NH_TH.Text = ds.Tables(0).Rows(0)("Ma_NH_TH").ToString()
                    txtTEN_NH_TH.Text = ds.Tables(0).Rows(0)("Ten_NH_TH").ToString()
                    txtTAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("TaiKhoan_TH").ToString()
                    txtTEN_TAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("Ten_TaiKhoan_TH").ToString()
                    txtNGAY_NHAN311.Text = ds.Tables(0).Rows(0)("NGAY_NHAN311").ToString()
                    'txtNoiDung_XL.Text = ds.Tables(0).Rows(0)("LY_DO_HUY").ToString()
                    txtLY_DO_CHUYEN_TRA.Text = ds.Tables(0).Rows(0)("LY_DO_CHUYEN_TRA").ToString()
                    txtCHK_TAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("cur_taikhoan_th").ToString() 'txtTAIKHOAN_TH.Text
                    txtCHK_TEN_TAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("cur_ten_taikhoan_th").ToString()
                    'If txtLoai_HS.Value.Equals("2") Or txtLoai_HS.Value.Equals("3") Then
                    '    txtCHK_TEN_TAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("cur_ten_taikhoan_th").ToString()
                    'End If
                    'Ngày hiệu lực được thay bằng ngày hiệu lực trên biên bản đã ký với NNT
                    txtNGAY_KY_HDUQ.Text = DateTime.Now.ToString("dd/MM/yyyy")
                    txtNGAY_HL_UQ.Text = DateTime.Now.ToString("dd/MM/yyyy")
                    txtNGAY_HHL_UQ.Text = DateTime.Now.AddMonths(2).ToString("dd/MM/yyyy")
                    If ds.Tables(0).Rows(0)("NGAY_HL_UQ").ToString().Length > 0 Then
                        txtNGAY_HL_UQ.Text = ds.Tables(0).Rows(0)("NGAY_HL_UQ").ToString()
                    End If
                    If ds.Tables(0).Rows(0)("NGAY_HHL_UQ").ToString().Length > 0 Then
                        txtNGAY_HHL_UQ.Text = ds.Tables(0).Rows(0)("NGAY_HHL_UQ").ToString()
                    End If
                    'txtNoiDung_XL.ReadOnly = True
                    txtLY_DO_CHUYEN_TRA.Enabled = False
                    txtLY_DO_CHUYEN_TRA.ReadOnly = False
                    txtTrangThai.Value = ds.Tables(0).Rows(0)("TRANGTHAI").ToString()
                    Dim strTMP As String = "<data>" & ds.Tables(0).Rows(0)("THONGTINLIENHE").ToString() & "</data>"
                    If strTMP.Length > 0 Then
                        Dim strThongtin As String = ""
                        Dim xmldoc As XmlDocument = New XmlDocument()
                        xmldoc.LoadXml(strTMP)
                        Dim x As Integer = 0
                        For Each vnode As XmlNode In xmldoc.SelectNodes("data/ThongTinLienHe")
                            x = x + 1
                            If strThongtin.Length > 0 Then
                                strThongtin &= "#"
                            End If
                            strThongtin &= x
                            For i As Integer = 0 To vnode.ChildNodes.Count - 1
                                strThongtin &= "|" & vnode.ChildNodes.Item(i).InnerText
                            Next
                        Next
                        Session.Add("THONGTINLIENHE", strThongtin)
                        txtThongTin.Value = strThongtin
                    End If
                    'End If
                End If
            Else
                txtMA_NH_TH.Text = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString()
                txtTEN_NH_TH.Text = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString()
                txtMA_DV.Enabled = True
                txtMA_DV.Enabled = True
                txtTEN_DV.Enabled = True
                txtDIACHI.Enabled = True
                txtHO_TEN.Enabled = True
                txtSO_CMT.Enabled = True
                txtNGAYSINH.Enabled = True
                txtNGUYENQUAN.Enabled = True
                txtSO_DT.Enabled = True
                txtEMAIL.Enabled = True
                txtMA_NH_TH.Enabled = False
                txtTEN_NH_TH.Enabled = False
                'txtNoiDung_XL.Enabled = False
                txtCHK_TAIKHOAN_TH.Enabled = False
                'txtCHK_TAIKHOAN_TH.Enabled = True
                'btnCheckCK.Enabled = False
                txtMA_NH_TH.Enabled = False
                txtTEN_NH_TH.Enabled = False
                btnLuuSoDT.Visible = True
                btnLuuEmail.Visible = True
                txtNGAY_HL_UQ.Enabled = True
                txtNGAY_HHL_UQ.Enabled = True
            End If
            'doan nay load so tai khoan dang dung
            'Dim strSQL As String = "" ' "SELECT  NVL(A.CUR_TAIKHOAN_TH,A.TAIKHOAN_TH) CUR_TAIKHOAN_TH,NVL(A.CUR_TEN_TAIKHOAN_TH,A.TEN_TAIKHOAN_TH) CUR_TEN_TAIKHOAN_TH FROM TCS_DM_NTDT_HQ A where so_hs='" & txtSO_HS.Text & "'"
            'If txtLoai_HS.Equals("3") Then
            '    strSQL = "SELECT  NVL(A.CUR_TAIKHOAN_TH,A.TAIKHOAN_TH) CUR_TAIKHOAN_TH,NVL(A.CUR_TEN_TAIKHOAN_TH,A.TEN_TAIKHOAN_TH) CUR_TEN_TAIKHOAN_TH FROM TCS_HQ247_314 A where so_hs='" & txtSO_HS.Text.Trim & "'"
            'Else
            '    If txtTrangThai.Value.Equals("13") Or txtTrangThai.Value.Equals("15") Then
            '        strSQL = "SELECT  NVL(A.CUR_TAIKHOAN_TH,A.TAIKHOAN_TH) CUR_TAIKHOAN_TH,NVL(A.CUR_TEN_TAIKHOAN_TH,A.TEN_TAIKHOAN_TH) CUR_TEN_TAIKHOAN_TH FROM TCS_HQ247_314 A where so_hs='" & txtSO_HS.Text.Trim & "'"
            '    Else
            '        strSQL = "SELECT  A.CUR_TAIKHOAN_TH ,A.CUR_TEN_TAIKHOAN_TH FROM TCS_HQ247_314_TMP A where ID='" & hdfNNTDT_ID.Value & "'"
            '    End If
            'End If
            'Dim dsTKKH As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            'If Not dsTKKH Is Nothing Then
            '    If dsTKKH.Tables.Count > 0 Then
            '        If dsTKKH.Tables(0).Rows.Count > 0 Then
            '            txtCHK_TAIKHOAN_TH.Text = dsTKKH.Tables(0).Rows(0)(0).ToString
            '            txtCHK_TEN_TAIKHOAN_TH.Text = dsTKKH.Tables(0).Rows(0)(0).ToString
            '            'grdTKNH.DataSource = dsTKKH.Tables(0)
            '        End If
            '    End If
            'End If
            'grdTKNH.DataBind()

            Select Case txtTrangThai.Value
                Case "04" 'Đã duyệt yêu cầu
                    txtCHK_TAIKHOAN_TH.Text = txtTAIKHOAN_TH.Text
                    btnCheckTK.Enabled = True
                    'btnCheckCK.Enabled = False
                    btnChuyenKS.Enabled = True
                    txtNGAY_KY_HDUQ.ReadOnly = False
                    txtCHK_TAIKHOAN_TH.ReadOnly = True
                    txtCHK_TAIKHOAN_TH.Enabled = False
                    'txtCHK_TAIKHOAN_TH.ReadOnly = False
                    'txtCHK_TAIKHOAN_TH.Enabled = True
                    txtNGAY_KY_HDUQ.Enabled = False
                    txtstrdisabled.Value = "disabled"
                    txtNGAY_HL_UQ.Enabled = True
                    txtNGAY_HHL_UQ.Enabled = True
                Case "09" 'Chờ kiểm soát
                    btnCheckTK.Enabled = False
                    btnChuyenKS.Enabled = False
                    txtstrdisabled.Value = "disabled"
                Case "10" 'Chuyển trả
                    btnCheckTK.Enabled = True
                    'btnCheckCK.Enabled = False
                    btnChuyenKS.Enabled = True
                    If txtSO_HS.Text.Length <= 0 And txtLoai_HS.Value.Equals("1") Then
                        txtMA_DV.Enabled = True
                        txtMA_DV.ReadOnly = False
                        txtTEN_DV.Enabled = True
                        txtTEN_DV.ReadOnly = False
                        txtDIACHI.Enabled = True
                        txtDIACHI.ReadOnly = False
                        txtHO_TEN.Enabled = True
                        txtHO_TEN.ReadOnly = False
                        txtNGAYSINH.Enabled = True
                        txtNGAYSINH.ReadOnly = False
                        txtSO_CMT.Enabled = True
                        txtSO_CMT.ReadOnly = False
                        txtNGUYENQUAN.Enabled = True
                        txtNGUYENQUAN.ReadOnly = False
                        txtNGAY_KY_HDUQ.Text = DateTime.Now.ToString("dd/MM/yyyy")
                        txtNGAY_HL_UQ.Enabled = True
                        txtNGAY_HHL_UQ.Enabled = True
                        txtstrdisabled.Value = ""
                    Else
                        txtstrdisabled.Value = "disabled"
                    End If
                    If txtSO_HS.Text.Length > 0 And (txtLoai_HS.Value.Equals("1") Or txtLoai_HS.Value.Equals("2")) Then
                        txtstrdisabled.Value = ""
                    End If
                    txtNGAY_HL.ReadOnly = False
                    txtCHK_TAIKHOAN_TH.ReadOnly = True
                    txtCHK_TAIKHOAN_TH.Enabled = False
                    'txtCHK_TAIKHOAN_TH.ReadOnly = False
                    'txtCHK_TAIKHOAN_TH.Enabled = True
                Case "11" 'Đã kiểm soát - Chờ xác nhận từ TCHQ
                    btnCheckTK.Enabled = False
                    txtNGAY_KY_HDUQ.Enabled = False
                    btnChuyenKS.Enabled = False
                    txtstrdisabled.Value = "disabled"
                Case "12" 'Đã kiểm soát – Chờ bổ sung TT NNT
                    btnCheckTK.Enabled = False
                    txtNGAY_KY_HDUQ.Enabled = False
                    btnChuyenKS.Enabled = False
                    txtstrdisabled.Value = "disabled"
                Case "13" 'Thành công
                    btnCheckTK.Enabled = False
                    btnChuyenKS.Enabled = False
                    txtstrdisabled.Value = "disabled"
                Case "14" 'Điều chỉnh
                    btnCheckTK.Enabled = True
                    btnChuyenKS.Enabled = True
                    txtNGAY_HL.Enabled = True
                    txtNGAY_HL_UQ.Enabled = True
                    txtNGAY_HHL_UQ.Enabled = True
                    txtCHK_TAIKHOAN_TH.Enabled = False
                    txtstrdisabled.Value = ""
                Case Else
                    btnChuyenKS.Enabled = False
                    btnCheckTK.Enabled = False
                    txtstrdisabled.Value = "disabled"
            End Select
            If txtLoai_HS.Value.Equals("3") Then
                'txtCHK_TEN_TAIKHOAN_TH.Text = txtTEN_TAIKHOAN_TH.Text
            End If
            If txtLoai_HS.Value.Equals("2") Then
                'txtCHK_TEN_TAIKHOAN_TH.Text = txtTEN_TAIKHOAN_TH.Text
                btnCheckTK.Enabled = False
                'btnChuyenKS.Enabled = True
                txtstrdisabled.Value = "disabled"
            End If
        End If
        If hdfNNTDT_ID.Value.Length <= 0 Then
            txtMA_NH_TH.Text = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString()
            txtTEN_NH_TH.Text = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString()
            txtMA_DV.Enabled = True
            txtMA_DV.Enabled = True
            txtTEN_DV.Enabled = True
            txtDIACHI.Enabled = True
            txtHO_TEN.Enabled = True
            txtSO_CMT.Enabled = True
            txtNGAYSINH.Enabled = True
            txtNGUYENQUAN.Enabled = True
            txtSO_DT.Enabled = True
            txtEMAIL.Enabled = True
            txtMA_NH_TH.Enabled = False
            txtTEN_NH_TH.Enabled = False
            'txtNoiDung_XL.Enabled = False
            txtCHK_TAIKHOAN_TH.Enabled = False
            'txtCHK_TAIKHOAN_TH.Enabled = True
            'btnCheckCK.Enabled = False
            txtMA_NH_TH.Enabled = False
            txtTEN_NH_TH.Enabled = False
            btnLuuSoDT.Visible = True
            btnLuuEmail.Visible = True
        End If
        If txtTrangThai.Value = "04" Or txtTrangThai.Value = "10" Then
            txtCHK_TAIKHOAN_TH.ReadOnly = True
            txtCHK_TAIKHOAN_TH.Enabled = False
            'txtCHK_TAIKHOAN_TH.ReadOnly = False
            'txtCHK_TAIKHOAN_TH.Enabled = True
            btnCheckTK.Enabled = True
        End If
    End Sub
   
    ''' <summary>
    ''' Kiểm tra sự tồn tại của tài khoản NNT tại NH
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCheckTK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckTK.Click
        If txtCHK_TAIKHOAN_TH.Text = "" Then
            txtCHK_TAIKHOAN_TH.Focus()
            clsCommon.ShowMessageBox(Me, "Số tài khoản ngân hàng không được để trống")
            Exit Sub
        End If
        Try
            txtCHK_TEN_TAIKHOAN_TH.Text = ""
            Dim strTenTaiKhoanTuCore As String = ""
            strTenTaiKhoanTuCore = clscorebank.GetTen_TK_KH_NH(txtCHK_TAIKHOAN_TH.Text.Replace("-", ""))
            txtCHK_TEN_TAIKHOAN_TH.Text = strTenTaiKhoanTuCore
            'Sau đó kiểm tra điều kiện
            If Not txtCHK_TEN_TAIKHOAN_TH.Text.Trim.Equals("") Then
                'txtTAIKHOAN_TH.Text = txtCHK_TAIKHOAN_TH.Text
                'txtTEN_TAIKHOAN_TH.Text = txtCHK_TEN_TAIKHOAN_TH.Text
                hdfCifno.Value = txtCHK_TAIKHOAN_TH.Text
                hdfMA_CNTK.Value = "00"
                ' btnCheckCK.Enabled = False
            Else
                clsCommon.ShowMessageBox(Me, "Không tìm thấy thông tin tài khoản tại Ngân Hàng !")
                Exit Sub
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được tên khách hàng. Lỗi: " & ex.Message)
        End Try
    End Sub
    ''' <summary>
    ''' Hoàn thiện thông tin NNT và chuyển sang cho KSV
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnChuyenKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChuyenKS.Click
        Dim strThongTinLineHe As String = ""
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If txtCHK_TAIKHOAN_TH.Text = "" Then
            txtCHK_TAIKHOAN_TH.Focus()
            clsCommon.ShowMessageBox(Me, "Số tài khoản ngân hàng không được để trống")
            Exit Sub
        End If
        Try
            If Not txtLoai_HS.Value.Equals("3") Then
                If txtCHK_TEN_TAIKHOAN_TH.Text.Trim.Length <= 0 Then
                    clsCommon.ShowMessageBox(Me, "Chưa kiểm tra tài khoản đăng ký ")
                    txtCHK_TAIKHOAN_TH.Focus()
                    Exit Sub
                End If
                Dim strTenTaiKhoanTuCore As String = ""
                strTenTaiKhoanTuCore = clsCoreBank.GetTen_TK_KH_NH(txtCHK_TAIKHOAN_TH.Text.Replace("-", ""))
                txtCHK_TEN_TAIKHOAN_TH.Text = strTenTaiKhoanTuCore
                'Sau đó kiểm tra điều kiện
                If strTenTaiKhoanTuCore.Equals("") Then
                    clsCommon.ShowMessageBox(Me, "Không tìm thấy thông tin tài khoản tại Ngân Hàng !")
                    Exit Sub
                End If
            End If
            If txtThongTin.Value.Length <= 0 Then
                clsCommon.ShowMessageBox(Me, "Chưa nhập Thông tin liên hệ ")
                Exit Sub
            Else
                Dim strDataLienHe As String = txtThongTin.Value
                Dim StrArrTMP As String() = strDataLienHe.Split("#")
                Dim x1 As Integer = 0
                Dim x2 As Integer = 0
                For i As Integer = 0 To StrArrTMP.Length - 1
                    Dim strTMPAS As String() = StrArrTMP(i).Split("|")
                    If txtSO_HS.Text.Length <= 0 And txtTrangThai.Value.Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_CHECKER312_CHUYENTRA) Then
                        If strTMPAS(1).Length <= 0 Or strTMPAS(2).Length <= 0 Then
                            clsCommon.ShowMessageBox(Me, "Thông tin liên hệ dòng " & (i + 1) & " Chưa nhập số điện thoại hoặc Email.")
                            Exit Sub
                        End If
                    End If
                    If (strTMPAS(1).Length > 0 Or strTMPAS(2).Length > 0) Then
                        strThongTinLineHe += "<ThongTinLienHe><So_DT>" & strTMPAS(1) & "</So_DT><Email>" & strTMPAS(2) & "</Email></ThongTinLienHe>"
                        If (x1 = 0) And strTMPAS(1).Length > 0 Then
                            x1 = 1
                            txtSO_DT.Text = strTMPAS(1)
                        End If
                        If (x2 = 0) And strTMPAS(2).Length > 0 Then
                            x2 = 1
                            txtEMAIL.Text = strTMPAS(2)
                        End If
                    End If
                Next
            End If
            If txtLoai_HS.Value.Equals("1") Or txtLoai_HS.Value.Equals("2") Then
                Dim strSQL As String = "SELECT SO_HS FROM TCS_HQ247_314_TMP WHERE MA_DV='" & txtMA_DV.Text.Trim() & "' and SO_HS<>'" & txtSO_HS.Text.Trim & "'"
                strSQL &= "UNION ALL SELECT SO_HS FROM TCS_HQ247_314 WHERE MA_DV='" & txtMA_DV.Text.Trim() & "' AND TRANGTHAI NOT IN ('15','16') AND SO_HS<>'" & txtSO_HS.Text.Trim & "'  "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            txtCHK_TAIKHOAN_TH.Focus()
                            clsCommon.ShowMessageBox(Me, "Mã đơn vị " & txtMA_DV.Text.Trim() & " Tài khoản" & txtCHK_TAIKHOAN_TH.Text.Trim & " đã có đăng ký trong hệ thống!")
                            Exit Sub
                        End If
                    End If
                End If
            End If
            If txtNGAY_HL_UQ.Text.Trim.Length <= 0 Then
                clsCommon.ShowMessageBox(Me, "Chưa nhập ngày hiệu lực ủy quyền")
                txtNGAY_HL_UQ.Focus()
                Exit Sub
            End If
            If txtNGAY_HHL_UQ.Text.Trim.Length <= 0 Then
                clsCommon.ShowMessageBox(Me, "Chưa nhập ngày hết hiệu lực ủy quyền")
                txtNGAY_HHL_UQ.Focus()
                Exit Sub
            End If
            'Chuyển trạng thái của Đăng ký
            'Do có cả 2 phương thức: đăng ký mới tại TCHQ và đăng ký mới tại NHTM đều sử dụng chung Form này, do đó để xác đinh
            'được đâu là đăng ký mới tại TCHQ và đâu là tại NHTM ta kiểm tra ID và Số hồ sơ
            '-Xác định đây là trường hợp đăng ký mới từ TCHQ
            If Not hdfNNTDT_ID.Value.Trim.Equals("") And Not txtSO_HS.Text.Trim.Equals("") Then
                'check xem dang la hoan thien loai 1 hay loai 2
                If txtLoai_HS.Value.Equals("1") Then
                    Dim decOk As Decimal = daTCS_HQ247_314.MakerMSG312(hdfNNTDT_ID.Value, txtNGAY_KY_HDUQ.Text, txtCHK_TAIKHOAN_TH.Text.Trim, txtCHK_TEN_TAIKHOAN_TH.Text.Trim, strThongTinLineHe, txtNGAY_HL_UQ.Text.Trim(), txtNGAY_HHL_UQ.Text.Trim(), Session.Item("UserName").ToString, hdfCifno.Value, hdfMA_CNTK.Value)
                    If decOk = Business_HQ.Common.mdlCommon.TCS_HQ247_OK Then
                        clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát thành công. Đã gửi thông tin tới KSV")
                        btnChuyenKS.Enabled = False
                    Else
                        clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi:  " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decOk.ToString()))
                        btnChuyenKS.Enabled = True
                    End If
                ElseIf txtLoai_HS.Value.Equals("2") Then
                    'kiem tra xem co phai dieu chinh tu bank khong
                    If Business_HQ.nhothu.daTCS_HQ247_314.fnc_CheckDieuChinhTaiNH(txtSO_HS.Text) = 0 Then
                        Dim decOk As Decimal = Business_HQ.nhothu.daTCS_HQ247_314.fnc_Maker312_SuaTaiNH(txtSO_HS.Text, txtCHK_TAIKHOAN_TH.Text, txtCHK_TEN_TAIKHOAN_TH.Text, txtNGAY_KY_HDUQ.Text, txtNGAY_HL_UQ.Text, txtNGAY_HHL_UQ.Text, Session.Item("UserName").ToString, hdfCifno.Value, hdfMA_CNTK.Value)
                        If decOk = Business_HQ.Common.mdlCommon.TCS_HQ247_OK Then
                            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát thành công. Đã gửi thông tin tới KSV")
                            btnChuyenKS.Enabled = False
                            'Dim strSQL As String = "" ' "SELECT  NVL(A.CUR_TAIKHOAN_TH,A.TAIKHOAN_TH) CUR_TAIKHOAN_TH,NVL(A.CUR_TEN_TAIKHOAN_TH,A.TEN_TAIKHOAN_TH) CUR_TEN_TAIKHOAN_TH FROM TCS_DM_NTDT_HQ A where so_hs='" & txtSO_HS.Text & "'"
                            'LAY LAI TRANG THAI
                            'If txtLoai_HS.Equals("3") Then
                            '    strSQL = "SELECT  NVL(A.CUR_TAIKHOAN_TH,A.TAIKHOAN_TH) CUR_TAIKHOAN_TH,NVL(A.CUR_TEN_TAIKHOAN_TH,A.TEN_TAIKHOAN_TH) CUR_TEN_TAIKHOAN_TH FROM TCS_HQ247_314 A where so_hs='" & txtSO_HS.Text & "'"
                            'Else
                            '    If txtTrangThai.Value.Equals("13") Or txtTrangThai.Value.Equals("15") Then
                            '        strSQL = "SELECT  NVL(A.CUR_TAIKHOAN_TH,A.TAIKHOAN_TH) CUR_TAIKHOAN_TH,NVL(A.CUR_TEN_TAIKHOAN_TH,A.TEN_TAIKHOAN_TH) CUR_TEN_TAIKHOAN_TH FROM TCS_HQ247_314 A where so_hs='" & txtSO_HS.Text & "'"
                            '    Else
                            '        strSQL = "SELECT  A.CUR_TAIKHOAN_TH ,A.CUR_TEN_TAIKHOAN_TH FROM TCS_HQ247_314_TMP A where ID='" & hdfNNTDT_ID.Value & "'"
                            '    End If
                            'End If
                            'Dim dsTKKH As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                            'grdTKNH.DataSource = Nothing
                            'If Not dsTKKH Is Nothing Then
                            '    If dsTKKH.Tables.Count > 0 Then
                            '        If dsTKKH.Tables(0).Rows.Count > 0 Then
                            '            'grdTKNH.DataSource = dsTKKH.Tables(0)
                            '        End If
                            '    End If
                            'End If
                            'grdTKNH.DataBind()
                        Else
                            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi:  " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decOk.ToString()))
                            btnChuyenKS.Enabled = True
                        End If
                        'neu da co roi thì thực hien maker312 binh thuong
                        'neu chua co thi phai copy ve 311 truoc
                    Else
                        Dim decOk As Decimal = Business_HQ.nhothu.daTCS_HQ247_314.MakerMSG312Sua(hdfNNTDT_ID.Value, txtNGAY_KY_HDUQ.Text, txtCHK_TAIKHOAN_TH.Text.Trim, txtCHK_TEN_TAIKHOAN_TH.Text.Trim, strThongTinLineHe, Session.Item("UserName").ToString, hdfCifno.Value, hdfMA_CNTK.Value)
                        If decOk = Business_HQ.Common.mdlCommon.TCS_HQ247_OK Then
                            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát thành công. Đã gửi thông tin tới KSV")
                            btnChuyenKS.Enabled = False
                            btnCheckTK.Enabled = False
                        Else
                            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi:  " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decOk.ToString()))
                            btnChuyenKS.Enabled = True
                        End If
                    End If
                ElseIf txtLoai_HS.Value.Equals("3") Then
                    'khai huy
                    Dim decOk As Decimal = Business_HQ.nhothu.daTCS_HQ247_314.fnc_Maker312_HuyTaiNH(txtSO_HS.Text, Session.Item("UserName").ToString)
                    If decOk = Business_HQ.Common.mdlCommon.TCS_HQ247_OK Then
                        clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát thành công. Đã gửi thông tin tới KSV")
                        btnChuyenKS.Enabled = False

                    Else
                        clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi:  " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decOk.ToString()))
                        btnChuyenKS.Enabled = True
                    End If
                End If
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi: " & ex.Message)
        End Try
        Dim tt As String = Business_HQ.NHOTHU.daTCS_HQ247_314.Get_TT_HS(hdfNNTDT_ID.Value)
        lblTrangThai.Text = Business_HQ.Common.mdlCommon.fnc_GET_TENTRANGTHAI_HS(tt)
    End Sub
    Shared dt As New DataTable
    Shared dem As Integer = 0
    Protected Sub btnDieuChinh_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        'cho phep sua so tai khoan
        txtCHK_TAIKHOAN_TH.Enabled = False

        txtMA_DV.Enabled = False
        txtTEN_DV.Enabled = False
        txtDIACHI.Enabled = False
        txtHO_TEN.Enabled = False
        txtSO_CMT.Enabled = False
        txtNGAYSINH.Enabled = False
        txtNGUYENQUAN.Enabled = False
        txtSERIALNUMBER.Enabled = False
        txtNGAY_HHL.Enabled = False
        txtNGAY_HL.Enabled = False
        txtNOICAP.Enabled = False
        txtMA_NH_TH.Enabled = False
        txtTEN_NH_TH.Enabled = False
        txtLoai_HS.Value = "2"
        txtTenLoai_HS.Text = "Khai sửa"
        txtNGAY_KY_HDUQ.ReadOnly = False
        txtNGAY_KY_HDUQ.Enabled = False
        'btnDieuChinh.Enabled = False
        btnChuyenKS.Enabled = True
        btnCheckTK.Enabled = False
        txtTAIKHOAN_TH.Enabled = False
        txtTEN_NH_TH.Enabled = False
        txtNGAY_HL_UQ.Enabled = True
        txtNGAY_HHL_UQ.Enabled = True

        'txtNGAY_KY_HDUQ.Text = DateTime.Now.ToString("dd/MM/yyyy")
        txtNGAY_KY_HDUQ.Enabled = False
        txtTrangThai.Value = "04"
    End Sub
    Protected Sub btnHuyDK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        txtLoai_HS.Value = "3"
        txtTenLoai_HS.Text = "Khai Hủy"

        'btnDieuChinh.Enabled = False
        btnChuyenKS.Enabled = True
        'btnHuyDK.Enabled = False
        txtSO_HS.Enabled = False
        txtMA_DV.Enabled = False
        txtCHK_TAIKHOAN_TH.Enabled = False
        txtCHK_TEN_TAIKHOAN_TH.Enabled = False
        txtTAIKHOAN_TH.Enabled = False
        txtTEN_TAIKHOAN_TH.Enabled = False
        txtNGAY_KY_HDUQ.Enabled = False
        Dim ds As DataSet = Business_HQ.HQ247.TCS_DM_NTDT_HQ.getDangKyNNT_HQ(hdfNNTDT_ID.Value)
        If ds.Tables(0).Rows.Count > 0 Then
            txtCHK_TAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("CUR_TAIKHOAN_TH").ToString()
            txtCHK_TEN_TAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("CUR_TEN_TAIKHOAN_TH").ToString()
        End If
        txtNGAY_KY_HDUQ.Text = DateTime.Now.ToString("dd/MM/yyyy")
        txtNGAY_KY_HDUQ.Enabled = False
        btnChuyenKS.Focus()
    End Sub
    Protected Sub btnExit_Click1(sender As Object, e As EventArgs)
        Response.Redirect("~/HQNHOTHU/frmTraCuuNNTDT.aspx", False)
    End Sub
End Class
