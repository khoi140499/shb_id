﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmTraCuuNNTDT.aspx.vb" Inherits="HQ247_frmTraCuuNNTDT" title="HOÀN THIỆN/ ĐIỀU CHỈNH THÔNG TIN ĐĂNG KÝ HQ24/7" %>

<%@ Import Namespace="Business.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
        function jsChiTietNNT(_ID) {
            //window.open(_ID.replace("~", ".."), "_blank", "toolbar=no,scrollbars=yes,menubar=no, addressbar=no,location=no,resizable=yes,top=100,left=200,width=800,height=480");
            var link = _ID.replace("~", "..");
            PopupCenterHoanThien(link, "Hoan thien Ho so NNTHQ",1000, 1100);
        }
        //----------------------------------------------------
        var newWindowCTu = null;
        function PopupCenterHoanThien(url, title, w, h) {
            if (newWindowCTu != null)
                if (!newWindowCTu.closed) {
                    newWindowCTu.focus();
                    return;
                }
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;
            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
            var systemZoom = width / window.screen.availWidth;
            var left = (width - w) / 2 / systemZoom + dualScreenLeft
            var top = (height - h) / 2 / systemZoom + dualScreenTop
            top = 10;
            left = 10;
            newWindowCTu = window.open(url, title, 'location=no,addressbar=no,scrollbars=1,resizable=yes, width=' + w / systemZoom + ', height=' + h / systemZoom + ', top=' + top + ', left=' + left);
            setTimeout(reFreshdata(), 500);
        }
        function reFreshdata() {

            try {
                if (newWindowCTu.closed) {
                    searchData();
                }
                else {
                    setTimeout(reFreshdata, 500);
                }
            } catch (e) {
                setTimeout(reFreshdata, 500);
            }
        }
         function searchData() {
            document.getElementById('<%=hdfShowMSG.ClientID%>').value = "0";
            document.getElementById('<%= btnTimKiem.ClientID%>').click();
         }
        function ValidateNgay() {
            var tungay = $('#<%=txtTuNgay.ClientID%>').val();
             var denngay = $('#<%=txtDenNgay.ClientID%>').val();
             if (tungay.trim().length == 0 && denngay.trim().length == 0) {
                 return true;
             }
             var tungay1 = tungay.split("/");
             var denngay1 = denngay.split("/");
             var d1 = NaN;
             var d2 = NaN;
             if (tungay1.length == 3) {
                 d1 = Date.parse(tungay1[1] + "/" + tungay1[0] + "/" + tungay1[2]);
             }
             if (denngay1.length == 3) {
                 d2 = Date.parse(denngay1[1] + "/" + denngay1[0] + "/" + denngay1[2]);
             }
             if (tungay.trim().length == 0) {
                 if (d2 != NaN)
                     return true;
                 else {
                     alert("Đến ngày không hợp lệ.");
                     return false;
                 }
             }
             if (denngay.trim().length == 0) {
                 if (d1 != NaN)
                     return true;
                 else {
                     alert("Từ ngày không hợp lệ.");
                     return false;
                 }
             }
             if (d1 == NaN) {
                 alert("Từ ngày không hợp lệ.");
                 return false;
             }
             if (d2 == NaN) {
                 alert("Đến ngày không hợp lệ.");
                 return false;
             }
             if (d1 > d2) {
                 alert("Vui lòng nhập Từ ngày đăng ký nhỏ hơn hoặc bằng Đến ngày đăng ký");
                 return false;
             }
             return true;
         }
    </script>
    <style type="text/css">
        
        .container{width:100%; margin-bottom:30px}
        .dataView{padding-bottom:10px}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle">HOÀN THIỆN THÔNG TIN ĐĂNG KÝ</p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
            <tr>
                <td align="left" style="width:15%" class="form_label">Từ ngày đăng ký</td>
                <td class="form_control" style="width:35%"><span style="display:block; width:40%; position:relative;vertical-align:middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                    onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span></td>
                <td align="left" style="width:15%" class="form_label">Đến ngày đăng ký</td>
                <td class="form_control" style="width:35%">
                <span style="display:block; width:40%; position:relative; vertical-align:middle;">
                    <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                     onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span></td>
            </tr>
            <tr>
	            <td align="left" style="width:15%" class="form_label">Ngày HL UQ</td>
	            <td class="form_control" style="width:35%"><span style="display:block; width:40%; position:relative;vertical-align:middle;">
		            <input type="text" id='txtNgayHLUQ' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
		            onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
	            </span></td>
	            <td align="left" style="width:15%" class="form_label">Ngày HHL UQ</td>
	            <td class="form_control" style="width:35%">
	            <span style="display:block; width:40%; position:relative; vertical-align:middle;">
		            <input type="text" id='txtNgayHHLUQ' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
		             onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
	            </span></td>
            </tr>
            <tr>
                <td align="left" class="form_label">Số hồ sơ</td>
                <td class="form_control"><asp:TextBox ID="txtSo_HS" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <td align="left" class="form_label">Loại hồ sơ</td>
                 <td class="form_control">
                    <asp:DropDownList ID="drpLoai_HS" runat="server" Width="90%" CssClass="selected inputflat" >
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" class="form_label">Mã số thuế</td>
                <td class="form_control"><asp:TextBox ID="txtMST" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <td align="left" class="form_label">Trạng thái</td>
                <td class="form_control">
                    <asp:DropDownList ID="drpTrangThai" runat="server" Width="90%" CssClass="selected inputflat" >
                    </asp:DropDownList>
                </td>
            </tr>
            
            <tr>
                <td align="left" class="form_label">Số tài khoản</td>
                <td class="form_control"><asp:TextBox ID="txtTKNH" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
               <%--<td align="left" class="form_label">Tên người nộp thuế</td>
                <td class="form_control"><asp:TextBox ID="txtTenNNT" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>--%>
                <td align="left" class="form_label"></td>
                <td align="left" class="form_label"></td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnTimKiem" CssClass="buttonField" runat="server" OnClientClick="return ValidateNgay();" Text="Tìm kiếm" />
                  
                </td>
            </tr>
        </table>
    </div>
    <div class="dataView">
        <asp:DataGrid ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="100%" BorderColor="#000000" CssClass="grid_data" 
            AllowPaging="True" >
            <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
            <HeaderStyle  CssClass="grid_header"></HeaderStyle>
            <ItemStyle CssClass="grid_item" />
            <Columns>
                <asp:BoundColumn DataField="MA_DV" HeaderText="ID" Visible="false">
                    <HeaderStyle Width="0"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"  Width="0"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="SO_HS" HeaderText="Số hồ sơ">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Mã số thuế">
                    <ItemTemplate>
                        <a href="#" onclick="jsChiTietNNT('<%#Eval("NAVIGATEURL").ToString & "?ID=" & Eval("ID").ToString%>')"><%#Eval("MA_DV").ToString%></a>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="TEN_LOAI_HS" HeaderText="Loại hồ sơ">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="TEN_DV" HeaderText="Tên NNT">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="DIACHI" HeaderText="Địa chỉ" Visible="false">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="TAIKHOAN_TH" HeaderText="Số TK NH">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Ngày đăng ký">
                    <ItemTemplate>
                        <%#Eval("NGAY_NHAN311").ToString%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle></HeaderStyle>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="ngay_ht" HeaderText="Ngày hệ thống">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ngay_hl_uq" HeaderText="Ngày_HL_UQ">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ngay_hhl_uq" HeaderText="Ngày_HHL_UQ">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Chi nhánh thực hiện" >
                    <ItemTemplate>
                        <%#Eval("CN_NAME")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle></HeaderStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Trạng thái">
                    <ItemTemplate>
                        <%#Eval("TRANG_THAI")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle></HeaderStyle>
                </asp:TemplateColumn>
                
            </Columns>
            <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                Font-Strikeout="False" Font-Underline="False" ForeColor="#000" VerticalAlign="Middle">
            </PagerStyle>
        </asp:DataGrid>
        <asp:GridView ID="grdExport" runat="server" AutoGenerateColumns="False" ShowFooter="true" Visible="false"
            Width="100%" BorderColor="#000" CssClass="grid_data" AllowPaging="false" >
            <AlternatingRowStyle CssClass="grid_item_alter"></AlternatingRowStyle>
            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
            <RowStyle CssClass="grid_item" />
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%#Eval("MA_CN").ToString + "&nbsp;"%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    <HeaderTemplate>
                        Mã chi nhánh
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%#Eval("NAME").ToString + "&nbsp;"%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    <HeaderTemplate>
                        Tên chi nhánh
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%#Eval("MST").ToString + "&nbsp;"%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    <HeaderTemplate>
                        Mã số thuế
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:TemplateField>
                <asp:BoundField DataField="TEN_NNT" HeaderText="Tên">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="DIACHI_NNT" HeaderText="Địa chỉ">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%#Eval("SDT_NNT").ToString + "&nbsp;"%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    <HeaderTemplate>
                        Số điện thoại
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:TemplateField>
                <asp:BoundField DataField="EMAIL_NNT" HeaderText="Email">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%#Eval("STK_NHANG").ToString + "&nbsp;"%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    <HeaderTemplate>
                        Số tài khoản
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%#mdlCommon.ConvertNumbertoString(Eval("NGAY_TAO").ToString)%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    <HeaderTemplate>
                        Ngày đăng ký
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <%#Eval("TRANG_THAI")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    <HeaderTemplate>
                        Trạng thái
                    </HeaderTemplate>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:TemplateField>
            </Columns>
            <PagerStyle HorizontalAlign="Right" BackColor="#E4E5D7" Font-Bold="False"
                Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                Font-Strikeout="False" Font-Underline="False" ForeColor="#000" VerticalAlign="Middle">
            </PagerStyle>
        </asp:GridView>
        <asp:HiddenField runat="server" ID="hdfShowMSG" Value="1" />
    </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>		
</asp:Content>
