﻿Imports System.Data
Imports Business.NTDT.NNTDT
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao

Partial Class HQ247_frmTimKiemNNTDT
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Load_TrangThai()
            Load_Loai_HS()
            DM_DiemThu()
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub
    Private Sub load_dataGrid()
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            If txtMST.Text <> "" Then
                WhereClause += " AND upper(a.MA_DV) like upper('%" & txtMST.Text.Trim & "%')"
            End If
            If txtTuNgay.Value <> "" Then
                WhereClause += " AND TO_CHAR(NVL(a.NGAY_NHAN311,A.NGAY_HT),'RRRRMMDD') >= " & ConvertDateToNumber(txtTuNgay.Value)
            End If
            If txtDenNgay.Value <> "" Then
                WhereClause += " AND TO_CHAR(NVL(a.NGAY_NHAN311,A.NGAY_HT),'RRRRMMDD') <= " & ConvertDateToNumber(txtDenNgay.Value)
            End If
            If txtNgayHLUQ.Value <> "" Then
                WhereClause += " AND TO_CHAR(ngay_hl_uq,'RRRRMMDD') >= " & ConvertDateToNumber(txtNgayHLUQ.Value)
            End If
            If txtNgayHHLUQ.Value <> "" Then
                WhereClause += " AND TO_CHAR(ngay_hhl_uq,'RRRRMMDD') <= " & ConvertDateToNumber(txtNgayHHLUQ.Value)
            End If
            'If txtTenNNT.Text <> "" Then
            '    WhereClause += " AND upper(a.TEN_DV) like upper('%" + txtTenNNT.Text + "%')"
            'End If
            If txtTKNH.Text <> "" Then
                WhereClause += " AND upper((CASE WHEN A.TRANGTHAI IN ('01','02','03','04','05','06','07','08') THEN A.TAIKHOAN_TH ELSE NVL(A.CUR_TAIKHOAN_TH,A.TAIKHOAN_TH) END)) like upper('%" + txtTKNH.Text.Trim() + "%')"
            End If
            If txtSo_HS.Text <> "" Then
                WhereClause += " AND upper(a.SO_HS) like upper('%" + txtSo_HS.Text.Trim() + "%')"
            End If
            If drpTrangThai.SelectedIndex <> 0 Then
                WhereClause += " AND a.TRANGTHAI = '" + drpTrangThai.SelectedValue + "'"
            End If
            If drpLoai_HS.SelectedIndex <> 0 Then
                WhereClause += " AND a.LOAI_HS = '" + drpLoai_HS.SelectedValue + "'"
            End If
            'If cboDiemThu.SelectedIndex <> 0 Then
            '    WhereClause += " AND a.MA_CN = '" + cboDiemThu.SelectedValue + "'"
            'End If
            If txtSoNgayconHL.Text.Trim <> "" Then
                WhereClause += " AND trunc(ngay_hhl_uq)-trunc(ngay_hl_uq)=" + txtSoNgayconHL.Text.Trim()
            End If
            ds = Business_HQ.nhothu.daTCS_HQ247_314.getMSG311View(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgrd.CurrentPageIndex = 0
                dtgrd.DataSource = ds.Tables(0)
                dtgrd.DataBind()
                grdExport.DataSource = ds.Tables(0)
                grdExport.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không tìm thấy bản nghi thỏa mãn điều kiện tìm kiếm")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub
    'Private Sub Load_TrangThai()
    '    Try
    '        Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_YEUCAU"
    '        Dim dr As OracleClient.OracleDataReader
    '        dr = DataAccess.ExecuteDataReader(sql, CommandType.Text)
    '        drpTrangThai.DataSource = dr
    '        drpTrangThai.DataTextField = "MOTA"
    '        drpTrangThai.DataValueField = "TRANG_THAI"
    '        drpTrangThai.DataBind()
    '        dr.Close()
    '        drpTrangThai.Items.Insert(0, "Tất cả")
    '    Catch ex As Exception
    '        clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái yêu cầu. Lỗi: " & ex.Message)
    '    End Try
    'End Sub
    Private Sub Load_TrangThai()
        Try
            Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_YEUCAU"
            Dim ds As DataSet
            ds = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            drpTrangThai.DataSource = ds
            drpTrangThai.DataTextField = "MOTA"
            drpTrangThai.DataValueField = "TRANG_THAI"
            drpTrangThai.DataBind()
            drpTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái yêu cầu. Lỗi: " & ex.Message)
        End Try
    End Sub
    'Private Sub Load_Loai_HS()
    '    Try
    '        Dim sql = "SELECT * FROM TCS_DM_LOAI_HS_NNT ORDER BY LOAI_HS"
    '        Dim dr As OracleClient.OracleDataReader
    '        dr = DataAccess.ExecuteDataReader(sql, CommandType.Text)
    '        drpLoai_HS.DataSource = dr
    '        drpLoai_HS.DataTextField = "MOTA"
    '        drpLoai_HS.DataValueField = "LOAI_HS"
    '        drpLoai_HS.DataBind()
    '        dr.Close()
    '        drpLoai_HS.Items.Insert(0, "Tất cả")
    '    Catch ex As Exception
    '        clsCommon.ShowMessageBox(Me, "Không lấy được danh sách loại hồ sơ người nộp thuế. Lỗi: " & ex.Message)
    '    End Try
    'End Sub
    Private Sub Load_Loai_HS()
        Try
            Dim sql = "SELECT * FROM TCS_DM_LOAI_HS_NNT ORDER BY LOAI_HS"
            Dim ds As DataSet
            ds = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            drpLoai_HS.DataSource = ds
            drpLoai_HS.DataTextField = "MOTA"
            drpLoai_HS.DataValueField = "LOAI_HS"
            drpLoai_HS.DataBind()
            drpLoai_HS.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách loại hồ sơ người nộp thuế. Lỗi: " & ex.Message)
        End Try
    End Sub


    Private Sub DM_DiemThu()

        Dim dsDiemThu As DataSet
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        'Dim str_Ma_Nhom As String = ""
        Dim dt As New DataTable
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        If strcheckHSC = "1" Then
            'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
            ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
            strSQL = "SELECT a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a order by a.id "
        Else
            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  a.branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' or a.id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' order by a.id"
        End If
        Try
            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)

            dt = dsDiemThu.Tables(0)
            'cboDiemThu.DataSource = dt
            'cboDiemThu.DataTextField = "name"
            'cboDiemThu.DataValueField = "id"
            'cboDiemThu.DataBind()

            If strcheckHSC = "1" Then
                'clsCommon.ComboBoxWithValue(cboDiemThu, dsDiemThu, 1, 0, "Tất cả")
            End If

        Catch ex As Exception
            Throw ex
        Finally

        End Try
    End Sub

    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        load_dataGrid()
        dtgrd.CurrentPageIndex = 0
        dtgrd.DataBind()
    End Sub

    Protected Sub dtgrd_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgrd.PageIndexChanged
        load_dataGrid()
        dtgrd.CurrentPageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        grdExport.Visible = True
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=ExportNNTHQPHAITHU.xls")
        Response.Charset = "utf-8"
        Response.ContentType = "application/vnd.ms-excel"
        Using sw1 As New StringWriter()
            Dim hw1 As New HtmlTextWriter(sw1)
            grdExport.RenderControl(hw1)
            Response.Output.Write(sw1.ToString)
            Response.Flush()
            Response.End()
        End Using
        grdExport.Visible = False
    End Sub
End Class
