﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmTimKiemNNTDT.aspx.vb" Inherits="HQ247_frmTimKiemNNTDT" title="TRA CỨU THÔNG TIN NNTDT" %>

<%@ Import Namespace="Business.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
        function jsChiTietNNT(_ID) {
       
            window.open( _ID.replace("~", ".."),  "_blank",  "toolbar=no,scrollbars=yes,menubar=no, addressbar=no,location=no,resizable=yes,top=100,left=200,width=800,height=480");
        }
        function ValidateNgay() {
            var tungay = $('#<%=txtTuNgay.ClientID%>').val();
             var denngay = $('#<%=txtDenNgay.ClientID%>').val();
             if (tungay.trim().length == 0 && denngay.trim().length == 0) {
                 return true;
             }
             var tungay1 = tungay.split("/");
             var denngay1 = denngay.split("/");
             var d1 = NaN;
             var d2 = NaN;
             if (tungay1.length == 3) {
                 d1 = Date.parse(tungay1[1] + "/" + tungay1[0] + "/" + tungay1[2]);
             }
             if (denngay1.length == 3) {
                 d2 = Date.parse(denngay1[1] + "/" + denngay1[0] + "/" + denngay1[2]);
             }
             if (tungay.trim().length == 0) {
                 if (d2 != NaN)
                     return true;
                 else {
                     alert("Đến ngày không hợp lệ.");
                     return false;
                 }
             }
             if (denngay.trim().length == 0) {
                 if (d1 != NaN)
                     return true;
                 else {
                     alert("Từ ngày không hợp lệ.");
                     return false;
                 }
             }
             if (d1 == NaN) {
                 alert("Từ ngày không hợp lệ.");
                 return false;
             }
             if (d2 == NaN) {
                 alert("Đến ngày không hợp lệ.");
                 return false;
             }
             if (d1 > d2) {
                 alert("Vui lòng nhập Từ ngày đăng ký nhỏ hơn hoặc bằng Đến ngày đăng ký");
                 return false;
             }
             return true;
         }
    </script>
    <style type="text/css">
        
        .container{width:100%; margin-bottom:30px}
        .dataView{padding-bottom:10px}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle">TRA CỨU THÔNG TIN NNTDT</p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
            <tr>
                <td align="left" style="width:15%" class="form_label">Từ ngày đăng ký</td>
                <td class="form_control" style="width:35%"><span style="display:block; width:40%; position:relative;vertical-align:middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                    onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span></td>
                <td align="left" style="width:15%" class="form_label">Đến ngày đăng ký</td>
                <td class="form_control" style="width:35%">
                <span style="display:block; width:40%; position:relative; vertical-align:middle;">
                    <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                     onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span></td>
            </tr>
            <tr>
	            <td align="left" style="width:15%" class="form_label">Ngày HL UQ</td>
	            <td class="form_control" style="width:35%"><span style="display:block; width:40%; position:relative;vertical-align:middle;">
		            <input type="text" id='txtNgayHLUQ' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
		            onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
	            </span></td>
	            <td align="left" style="width:15%" class="form_label">Ngày HHL UQ</td>
	            <td class="form_control" style="width:35%">
	            <span style="display:block; width:40%; position:relative; vertical-align:middle;">
		            <input type="text" id='txtNgayHHLUQ' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
		             onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
	            </span></td>
            </tr>
            <tr>
                <td align="left" class="form_label">Số hồ sơ</td>
                <td class="form_control"><asp:TextBox ID="txtSo_HS" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <td align="left" class="form_label">Loại hồ sơ</td>
                 <td class="form_control">
                    <asp:DropDownList ID="drpLoai_HS" runat="server" Width="90%" CssClass="selected inputflat" >
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" class="form_label">Mã số thuế</td>
                <td class="form_control"><asp:TextBox ID="txtMST" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <td align="left" class="form_label">Trạng thái</td>
                <td class="form_control">
                    <asp:DropDownList ID="drpTrangThai" runat="server" Width="90%" CssClass="selected inputflat" >
                    </asp:DropDownList>
                </td>
            </tr>
            
            <tr>
                <td align="left" class="form_label">Số tài khoản</td>
                <td class="form_control"><asp:TextBox ID="txtTKNH" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <td align="left" class="form_label">Tổng số ngày còn HL</td>
                <td class="form_control"><asp:TextBox ID="txtSoNgayconHL" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="4">&#160;</td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnTimKiem" CssClass="buttonField" runat="server" OnClientClick="return ValidateNgay();" Text=" Tìm kiếm " />&nbsp;&nbsp;
                    <asp:Button ID="btnExport" CssClass="buttonField" runat="server" Text= "  Export  " />
                </td>
            </tr>
        </table>
    </div>
    <div class="dataView">
        <asp:DataGrid ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="100%" BorderColor="#000000" CssClass="grid_data" 
            AllowPaging="True" >
            <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
            <HeaderStyle  CssClass="grid_header"></HeaderStyle>
            <ItemStyle CssClass="grid_item" />
            <Columns>
                <asp:BoundColumn DataField="MA_DV" HeaderText="ID" Visible="false">
                    <HeaderStyle Width="0"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"  Width="0"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="SO_HS" HeaderText="Số HS">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Mã số thuế">
                    <ItemTemplate>
                     <a href="#" onclick="jsChiTietNNT('<%#Eval("NAVIGATEURL").ToString %>')"><%#Eval("MA_DV").ToString%></a>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="TEN_LOAI_HS" HeaderText="Loại HS">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                
                <asp:BoundColumn DataField="TEN_DV" HeaderText="Tên NNT">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="DIACHI" HeaderText="Địa chỉ">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="TAIKHOAN_TH" HeaderText="Số TK NH">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="Ngày đăng ký">
                    <ItemTemplate>
                        <%#Eval("NGAY_NHAN311").ToString%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle></HeaderStyle>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="ngay_hl_uq" HeaderText="Ngày_HL_UQ">
	                <HeaderStyle></HeaderStyle>
	                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ngay_hhl_uq" HeaderText="Ngày_HHL_UQ">
	                <HeaderStyle></HeaderStyle>
	                <ItemStyle HorizontalAlign="Left"></ItemStyle>
               </asp:BoundColumn>
               <asp:TemplateColumn HeaderText="Trạng thái">
                    <ItemTemplate>
                        <%#Eval("TRANG_THAI")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle></HeaderStyle>
                </asp:TemplateColumn>
            </Columns>
            <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                Font-Strikeout="False" Font-Underline="False" ForeColor="#000" VerticalAlign="Middle">
            </PagerStyle>
        </asp:DataGrid>
        <asp:GridView ID="grdExport" runat="server" AutoGenerateColumns="False" ShowFooter="true" Visible="false"
            Width="100%" BorderColor="#000" CssClass="grid_data" AllowPaging="false" >
            <AlternatingRowStyle CssClass="grid_item_alter"></AlternatingRowStyle>
            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
            <RowStyle CssClass="grid_item" />
            <Columns>
                <asp:BoundField DataField="SO_HS" DataFormatString = "&nbsp;{0}&nbsp;" HeaderText="Số HS">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="MA_DV" DataFormatString = "&nbsp;{0}&nbsp;" HeaderText="Mã số thuế">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="TEN_LOAI_HS" HeaderText="Loại HS">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="TEN_DV" HeaderText="Tên NNT">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                 <asp:BoundField DataField="DIACHI" HeaderText="Địa chỉ">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="TAIKHOAN_TH" DataFormatString="&nbsp;{0}&nbsp;" HeaderText="Số TK NH">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="NGAY_NHAN311" HeaderText="Ngày đăng ký">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="NGAY_HT" HeaderText="Ngày hệ thống">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="ngay_hl_uq" HeaderText="Ngày_HL_UQ">
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
	                <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="ngay_hhl_uq" HeaderText="Ngày_HHL_UQ">
	                <ItemStyle HorizontalAlign="Left"></ItemStyle>
	                <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="songayhl" HeaderText="Tổng số ngày còn hiệu lực">
	                <ItemStyle HorizontalAlign="Left"></ItemStyle>
	                <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="cn_name" HeaderText="Mã chi nhánh thực hiện">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                <asp:BoundField DataField="TRANG_THAI" HeaderText="Trạng thái">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top"/>
                </asp:BoundField>
                
            </Columns>
            <PagerStyle HorizontalAlign="Right" BackColor="#E4E5D7" Font-Bold="False"
                Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                Font-Strikeout="False" Font-Underline="False" ForeColor="#000" VerticalAlign="Middle">
            </PagerStyle>
        </asp:GridView>
    </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>		
</asp:Content>
