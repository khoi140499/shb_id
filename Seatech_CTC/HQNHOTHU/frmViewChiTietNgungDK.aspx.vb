﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.HQ247.NNTDT
Imports Business.Common.mdlCommon
Imports Business.BaoCao
Imports Business.Common
Imports System.IO
Imports System.Xml

''' <summary>
''' Hoàn thiện thông tin đăng ký của NNT
''' Chức năng Tạo mới: Chỉ dùng trong trường hợp NNT đăng kí thông tin ủy quyền trích nợ tài khoản tại NHTM 
''' Chức năng Ghi và chuyển KS: sử dụng đối với những đăng ký ở trạng thái ĐÃ DUYỆT YÊU CẦU
''' </summary>
''' <remarks>Số tài khoản phải được kiểm tra và lấy được Tên tài khoản</remarks>
Partial Class HQNHOTHU_frmViewChiTietNgungDK
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        txtMA_DV.Enabled = False
        txtMA_DV.Enabled = False
        txtTEN_DV.Enabled = False
        txtDIACHI.Enabled = False
        txtHO_TEN.Enabled = False
        txtSO_CMT.Enabled = False
        txtNGAYSINH.Enabled = False
        txtNGUYENQUAN.Enabled = False
        txtSO_DT.Enabled = False
        txtEMAIL.Enabled = False
        txtMA_NH_TH.Enabled = False
        txtTEN_NH_TH.Enabled = False
        txtNoiDung_XL.Enabled = False
        txtCHK_TAIKHOAN_TH.Enabled = False
        txtMA_NH_TH.Enabled = False
        txtTEN_NH_TH.Enabled = False
        'Kết nối với BankCore để lấy ra thông tin Tên tài khoản
        'Kết nối với BankCore để lấy ra tên ngân hàng thụ hưởng và mã ngân hàng thụ hưởng hoặc để mặc định
        'huynt_test
        Dim txtTrangThai As String = ""
        '
        If Not IsPostBack Then
            If Not Request.Params("ID") Is Nothing Then
                'Session.Add("THONGTINLIENHE", "")
                Dim strID As String = Request.Params("ID").ToString
                hdfNNTDT_ID.Value = strID
                Dim ds As DataSet = Business_HQ.nhothu.daTCS_HQ247_314.getTTNgungDK(strID)

                If ds.Tables(0).Rows.Count > 0 Then
                    'lblTrangThai.Text = Business_HQ.Common.mdlCommon.fnc_GET_TENTRANGTHAI_HS(ds.Tables(0).Rows(0)("TRANGTHAI").ToString())
                    txtSO_HS.Text = ds.Tables(0).Rows(0)("So_HS").ToString()
                    txtLoai_HS.Value = ds.Tables(0).Rows(0)("Loai_HS").ToString()
                    txtID.Value = ds.Tables(0).Rows(0)("ID").ToString()
                    txtTenLoai_HS.Text = ds.Tables(0).Rows(0)("TenLoai_HS").ToString()
                    txtMA_DV.Text = ds.Tables(0).Rows(0)("Ma_DV").ToString()
                    txtTEN_DV.Text = ds.Tables(0).Rows(0)("Ten_DV").ToString()
                    txtDIACHI.Text = ds.Tables(0).Rows(0)("DiaChi").ToString()
                    txtHO_TEN.Text = ds.Tables(0).Rows(0)("Ho_Ten").ToString()
                    txtSO_CMT.Text = ds.Tables(0).Rows(0)("So_CMT").ToString()
                    txtNGAYSINH.Text = ds.Tables(0).Rows(0)("NgaySinh").ToString()
                    txtNGUYENQUAN.Text = ds.Tables(0).Rows(0)("NguyenQuan").ToString()
                    txtSO_DT.Text = ds.Tables(0).Rows(0)("So_DT").ToString()
                    txtEMAIL.Text = ds.Tables(0).Rows(0)("Email").ToString()
                    txtSERIALNUMBER.Text = ds.Tables(0).Rows(0)("SerialNumber").ToString()
                    txtNOICAP.Text = ds.Tables(0).Rows(0)("NOICAP").ToString()
                    txtNGAY_HL.Text = ds.Tables(0).Rows(0)("Ngay_HL").ToString()
                    txtNGAY_HHL.Text = ds.Tables(0).Rows(0)("Ngay_HHL").ToString()
                    txtMA_NH_TH.Text = ds.Tables(0).Rows(0)("Ma_NH_TH").ToString()
                    txtTEN_NH_TH.Text = ds.Tables(0).Rows(0)("Ten_NH_TH").ToString()
                    txtTAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("TaiKhoan_TH").ToString()
                    'txtTEN_TAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("Ten_TaiKhoan_TH").ToString()
                    'txtNGAY_NHAN311.Text = ds.Tables(0).Rows(0)("NGAY_NHAN311").ToString()
                    txtNoiDung_XL.Text = ds.Tables(0).Rows(0)("LY_DO_HUY").ToString()
                    txtLY_DO_CHUYEN_TRA.Text = ds.Tables(0).Rows(0)("LY_DO_CHUYEN_TRA").ToString()
                    txtTrangThai = ds.Tables(0).Rows(0)("trangthai").ToString()
                    txtCHK_TAIKHOAN_TH.Text = txtTAIKHOAN_TH.Text
                    txtNGAY_HL_UQ.Text = ds.Tables(0).Rows(0)("NGAY_HL_UQ").ToString()
                    txtNGAY_HHL_UQ.Text = ds.Tables(0).Rows(0)("NGAY_HHL_UQ").ToString()

                    'Ngày hiệu lực được thay bằng ngày hiệu lực trên biên bản đã ký với NNT
                    txtNGAY_KY_HDUQ.Text = ds.Tables(0).Rows(0)("NGAY_NHAN311").ToString() ' DateTime.Now.ToString("dd/MM/yyyy")
                    txtNoiDung_XL.ReadOnly = True
                    txtLY_DO_CHUYEN_TRA.Enabled = False
                    txtLY_DO_CHUYEN_TRA.ReadOnly = False
                    txtstrdisabled.Value = "disabled"
                    Dim strTMP As String = "<data>" & ds.Tables(0).Rows(0)("THONGTINLIENHE").ToString() & "</data>"
                    If strTMP.Length > 0 Then
                        Dim strThongtin As String = ""
                        Dim xmldoc As XmlDocument = New XmlDocument()
                        xmldoc.LoadXml(strTMP)
                        Dim x As Integer = 0
                        For Each vnode As XmlNode In xmldoc.SelectNodes("data/ThongTinLienHe")
                            x = x + 1
                            If strThongtin.Length > 0 Then
                                strThongtin &= "#"
                            End If
                            strThongtin &= x
                            For i As Integer = 0 To vnode.ChildNodes.Count - 1
                                strThongtin &= "|" & vnode.ChildNodes.Item(i).InnerText
                            Next
                        Next
                        Session.Add("THONGTINLIENHE", strThongtin)
                        txtThongTin.Value = strThongtin
                    End If
                End If
                Dim strSQL As String = "" ' "SELECT  NVL(A.CUR_TAIKHOAN_TH,A.TAIKHOAN_TH) CUR_TAIKHOAN_TH,NVL(A.CUR_TEN_TAIKHOAN_TH,A.TEN_TAIKHOAN_TH) CUR_TEN_TAIKHOAN_TH FROM TCS_DM_NTDT_HQ A where so_hs='" & txtSO_HS.Text & "'"
                If txtLoai_HS.Equals("3") Then
                    strSQL = "SELECT  NVL(A.CUR_TAIKHOAN_TH,A.TAIKHOAN_TH) CUR_TAIKHOAN_TH,NVL(A.CUR_TEN_TAIKHOAN_TH,A.TEN_TAIKHOAN_TH) CUR_TEN_TAIKHOAN_TH FROM tcs_hq247_314_log A where so_hs='" & txtSO_HS.Text & "'"
                End If
                Dim dsTKKH As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                'grdTKNH.DataSource = Nothing
                If Not dsTKKH Is Nothing Then
                    If dsTKKH.Tables.Count > 0 Then
                        If dsTKKH.Tables(0).Rows.Count > 0 Then
                            txtCHK_TAIKHOAN_TH.Text = dsTKKH.Tables(0).Rows(0)("CUR_TAIKHOAN_TH").ToString
                            txtCHK_TEN_TAIKHOAN_TH.Text = dsTKKH.Tables(0).Rows(0)("CUR_TEN_TAIKHOAN_TH").ToString
                            grdTKNH.DataSource = dsTKKH.Tables(0)
                        End If
                    End If
                End If
                'grdTKNH.DataBind()
            End If
        End If
    End Sub
End Class
