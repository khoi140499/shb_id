﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmDangKyTD.aspx.vb" Inherits="HQNHOTHU_frmDangKyTD" title="TRA CỨU VÀ XỬ LÝ TIẾP NHẬN ĐĂNG KÝ" %>

<%@ Import Namespace="Business.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <style type="text/css">
		#wapaler {
			width: 100%;
			height: 100%;
			background-color: #000;
			position: fixed;
			top: 0;
			left: 0;
			opacity: 0.4;
			display: block;
			z-index: 99;
		}

		div.msgBox {
			padding: 4px 10px 4px 10px;
			position: fixed;
			z-index: 999999;
			font-family: Verdana;
			min-width: 300px;
			color: #00335e;
			-moz-border-radius: 6px;
			-webkit-border-radius: 6px;
			border-radius: 6px;
			-moz-box-shadow: 0px 0px 11px #000000;
			-webkit-box-shadow: 0px 0px 11px #000000;
			box-shadow: 0px 0px 11px #000000;
			background-color: #f4f6f8;
			height: 200px;
			width: 400px;
		}

		div.msgBoxBackGround {
			top: 0;
			left: 0;
			position: absolute;
			padding: 0;
			margin: 0;
			width: 100%;
			height: 100%;
			background-color: #000000;
			opacity: 0.9;
			z-index: 999;
		}

		div.msgBoxTitle {
			padding: 5px 0 5px 0;
			font-size: 12pt;
			font-weight: lighter;
			color: #00335e;
			font-weight: bold;
			border-bottom: 1px solid #002c5f;
			text-align: left;
			cursor: move;
		}

		div.msgBox {
			font-family: Verdana;
			color: #00335e;
		}

		div.msgBoxImage {
			margin: 10px 5px 0 5px;
			display: inline-block;
			float: left;
		}

			div.msgBoxImage img {
				width: 30px;
				height: 30px;
			}

		div.msgBoxContent {
			font-size: 12pt;
			margin: 10px 3px 0px 0px;
			display: inline-block;
			text-align: left;
			width: 90%;
		}

			div.msgBoxContent p {
				padding: 0;
				margin: 0;
				display: table;
				height: 100%;
			}

			div.msgBoxContent span {
				display: table-cell;
				vertical-align: middle;
				color: #00335e;
			}

		div.msgBoxButtons {
			display: inline-block;
			width: 100%;
			text-align: right;
			margin: 5px 0px;
		}

			div.msgBoxButtons input[type='button'] {
				cursor: pointer;
				margin: 2px;
				height: 25px;
				width: 70px;
				border: 1px solid #AFAFAF;
				font-size: 12px;
				color: #FFFFFF;
				-moz-border-radius: 3px;
				-webkit-border-radius: 3px;
				border-radius: 3px;
			}

		.btn {
			border-left: 1px solid #E6E6E6;
			border-right: 1px solid #E6E6E6;
			border-top: 1px solid #E6E6E6;
			border-bottom: 1px solid #B3B3B3;
			display: inline-block;
			padding: 2px 8px 2px;
			margin-bottom: 0;
			font-size: 13px;
			line-height: 18px;
			color: #333;
			text-align: center;
			text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
			vertical-align: middle;
			cursor: pointer;
			background-color: whiteSmoke;
			background-repeat: repeat-x;
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
			-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
			-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
			box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05); /*background-image: -moz-linear-gradient(top, white, #E6E6E6);*/
		}

			.btn:hover {
				color: #333;
				text-decoration: none;
				background-color: #E6E6E6;
				background-position: 0 -15px;
				-webkit-transition: background-position 0.1s linear;
				-moz-transition: background-position 0.1s linear;
				-ms-transition: background-position 0.1s linear;
				-o-transition: background-position 0.1s linear;
				transition: background-position 0.1s linear;
			}

		.btn-tertiary {
			font-size: 11px;
			font-family: Arial, Verdana, Tahoma, Helvetica, sans-serif;
			text-shadow: 0px -1px 0px #505455;
			color: #ffffff;
			background-color: #336699;
			background-image: linear-gradient(top, #8c9091, #505455);
			border-left-color: #6E7273;
			border-right-color: #505455;
			border-top-color: #6E7273;
			border-bottom-color: #505455;
		}

			.btn-tertiary:hover, .btn-tertiary:active, .btn-tertiary.active, .btn-tertiary.disabled, .btn-tertiary[disabled], .btn-group.open .btn-tertiary.dropdown-toggle {
				background-color: #505455;
			}

			.btn-tertiary:hover {
				color: #ffffff;
				text-shadow: 0px -1px 0px #323637;
			}

			.btn-tertiary:focus {
				color: #d2d6d7;
				-webkit-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
				-moz-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
				box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
				border-color: #6E7273 #505455 #505455 #6E7273;
				border-color: rgba(0, 0, 0, 0.25) rgba(0, 0, 0, 0.35) rgba(0, 0, 0, 0.35) rgba(0, 0, 0, 0.25);
			}

			.btn-tertiary.active, .btn-tertiary:active {
				background-image: none;
				-webkit-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
				-moz-box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
				box-shadow: inset 0 2px 4px rgba(0, 0, 0, 0.15), 0 1px 2px rgba(0, 0, 0, 0.05);
				color: #d2d6d7;
			}
		/* end .btn-tertiary */
		.btn-primary, .btn-primary:hover, .btn-warning, .btn-warning:hover, .btn-danger, .btn-danger:hover, .btn-success, .btn-success:hover, .btn-info, .btn-info:hover, .btn-inverse, .btn-inverse:hover {
			color: #ffffff;
			text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
		}

			.btn-primary.active, .btn-warning.active, .btn-danger.active, .btn-success.active, .btn-info.active, .btn-inverse.active {
				color: rgba(255, 255, 255, 0.75);
			}

		.btn {
			border-color: #ccc;
			border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
		}

		.btn-primary {
			background-color: #0074cc;
			background-color: #0055cc;
			background-repeat: repeat-x;
			background-image: linear-gradient(top, #0088cc, #0055cc);
			border-left-color: #0055cc;
			border-right-color: #0055cc;
			border-top-color: #0055cc;
			border-bottom-color: #003580;
		}

			.btn-primary:hover, .btn-primary:active, .btn-primary.active, .btn-primary.disabled, .btn-primary[disabled] {
				background-color: #0055cc;
				background-color: #004ab3;
			}

			.btn-primary:active, .btn-primary.active {
				background-color: #004099;
			}

		.errorMessage {
			color: red;
		}
	</style>
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
         function jsChiTietNNT(_ID,title) {
            createCookie("SEATECHIT_TENDN",'<%=Session.Item("User").ToString() %>',1);
            window.open("ViewMessage.ashx?ID="+_ID+"&IN="+title,"","","_new");
            //window.showModalDialog("../NTDT/frmChiTiet.aspx?ID=" + _ID, "", "dialogWidth:800px;dialogHeight:480px;help:0;status:0;", "_new");
        }
    </script>
     <script type="text/javascript">
       $(document).ready(function(){
         $(".JchkAll").click(function(){
        // alert($(this).is(':checked'));
            $('.JchkGrid').attr('checked',$(this).is(':checked'));
            GetListSoCT();
         });
          
         });
         function GetListSoCT() {
            var values = $('input:checkbox:checked.JchkGrid').map(function() {
                return this.value;
            }).get();
            
            
        
            if (values == '') {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);
               
                
            }
            else {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);
               

            }
        }
    </script>
    <style type="text/css">
        
        .container{width:100%; margin-bottom:30px}
        .dataView{padding-bottom:10px}
        .JchkAll,.JchkGrid{}
        .JchkGridNO{display:none;}
                 
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle">TRA CỨU VÀ XỬ LÝ TIẾP NHẬN ĐĂNG KÝ</p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
            <tr>
                <td align="left" style="width:15%" class="form_label">Từ ngày</td>
                <td class="form_control" style="width:35%"><span style="display:block; width:40%; position:relative;vertical-align:middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                    onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span></td>
                <td align="left" style="width:15%" class="form_label">Đến ngày</td>
                <td class="form_control" style="width:35%">
                <span style="display:block; width:40%; position:relative; vertical-align:middle;">
                    <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                     onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span></td>
            </tr>
            <tr>
                <td align="left" class="form_label">Số hồ sơ</td>
                <td class="form_control"><asp:TextBox ID="txtSo_HS" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <td align="left" class="form_label">Loại hồ sơ</td>
                 <td class="form_control">
                    <asp:DropDownList ID="drpLoai_HS" runat="server" Width="90%" CssClass="selected inputflat" >
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" class="form_label">Mã số thuế</td>
                <td class="form_control"><asp:TextBox ID="txtMST" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                  <td align="left" class="form_label">Số tài khoản</td>
                <td class="form_control"><asp:TextBox ID="txtTKNH" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                
            </tr>
            
            <tr style="display:none;">
              <td align="left" class="form_label">Trạng thái</td>
                <td class="form_control">
                    <asp:DropDownList ID="drpTrangThai" runat="server" Width="90%" CssClass="selected inputflat" >
                    </asp:DropDownList>
                </td>
               <td align="left" class="form_label">Tên người nộp thuế</td>
                <td class="form_control"><asp:TextBox ID="txtTenNNT" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnTimKiem" CssClass="buttonField" runat="server" Text="Tìm kiếm" style="width:100px" />
                    <asp:Button ID="btnExport" CssClass="buttonField" runat="server" Text="Xử lý" style="width:100px" />
                     <input type="button" class="buttonField" onclick="jsDiaglogDK()"  value="Thêm mới" style="width:100px" />
                </td>
            </tr>
        </table>
    </div>
    <div class="dataView">
     <asp:HiddenField id="ListCT_NoiDia" runat="server" />
        <asp:DataGrid ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="100%" BorderColor="#000000" CssClass="grid_data" 
            AllowPaging="True" >
            <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
            <HeaderStyle  CssClass="grid_header"></HeaderStyle>
            <ItemStyle CssClass="grid_item" />
            <Columns>
                <asp:TemplateColumn>
                    <HeaderTemplate>
                    <input type="checkbox" runat="server" ID="HeaderCheckBox" title="Chọn tất" class="JchkAll" />
                   
                    </HeaderTemplate>
                    <ItemTemplate>
                    
                     <input type="checkbox"   class="<%# DataBinder.Eval(Container.DataItem, "URL") %>" value='<%# DataBinder.Eval(Container.DataItem, "ID") %>' form="form" onclick="GetListSoCT()" />
                    </ItemTemplate>
               </asp:TemplateColumn>
                <asp:BoundColumn DataField="SO_HS" HeaderText="Số Hồ sơ">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="MA_DV" HeaderText="Mã số thuế">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                  <asp:BoundColumn DataField="TENLOAIHS" HeaderText="Loại Hồ sơ">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                  <asp:BoundColumn DataField="TEN_DV" HeaderText="Tên Đơn vị">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="TAIKHOAN_TH" HeaderText="Số tài khoản">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
               
             
                <asp:BoundColumn DataField="NGAY_NHAN311" HeaderText="Ngày đăng ký">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                 <asp:TemplateColumn HeaderText="Export message nhận về">
                    <ItemTemplate>
                    
                      <a href="#" onclick="jsChiTietNNT('<%#Eval("MSG_ID311").ToString %>',0 )"><%#Eval("EXPORT311").ToString%></a>
                    </ItemTemplate>
                     </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="Export message gửi đi">
                    <ItemTemplate>
                      <a href="#" onclick="jsChiTietNNT('<%#Eval("MSG_ID213").ToString %>',1)"><%#Eval("EXPORT213").ToString%></a>
                    </ItemTemplate>
               </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Trạng thái">
                    <ItemTemplate>
                        <%#Eval("TRANG_THAI")%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle></HeaderStyle>
                </asp:TemplateColumn>
                
            </Columns>
            <PagerStyle HorizontalAlign="Right" Mode="NumericPages" BackColor="#E4E5D7" Font-Bold="False"
                Font-Italic="False" Font-Names="Tahoma" Font-Overline="False" Font-Size="X-Small"
                Font-Strikeout="False" Font-Underline="False" ForeColor="#000" VerticalAlign="Middle">
            </PagerStyle>
        </asp:DataGrid>
        
    </div>
    
	<div class="msgBox" id="msgBoxDK" style="display: none; opacity: 1; min-height: 100px">
		<div class="msgBoxTitle">Import File truy vấn tờ khai</div>
		<div>
			<div>
				<div class="msgBoxContent">

					<table border="0" cellpadding="2" cellspacing="1" class="form_input" style="width: 100%">
						<tr>
							<td class="form_label">Chọn file </td>
							<td class="form_label">
								<asp:FileUpload ID="cmdFileUpload" runat="server" Width="99%" /></td>
						</tr>
						<tr>
							<td colspan="2" class="form_label">
								<asp:Label ID="lblError" runat="server" CssClass="errorMessage" Visible="false"></asp:Label>
								<asp:HiddenField ID="hdfUrlImport" runat="server" />
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="msgBoxButtons" id="ConfirmDivButton">
				<div style="display:none;">
					<asp:Button ID="btnExFileLogIMP" runat="server" />
				</div>
				<asp:Button ID="Button1" runat="server" Text="   Nhập   " />
				<input id="btn-close" class="btn btn-primary" onclick="jsMsgBoxDKClose();" type="button" name="Ok" value="Đóng" />
			</div>
		</div>
	</div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
        function jsDivAutoCenter(divName) {
            $(divName).css({
                position: 'fixed',
                left: ($(window).width() - $(divName).outerWidth()) / 2,
                top: ($(window).height() - $(divName).outerHeight()) / 2,
                height: 'auto'
            });
        }
        //pop-up
        $(window).resize(function () {

            jsDivAutoCenter('.msgBox');
        });
        function jsDiaglogDK() {
            jsDivAutoCenter('.msgBox');
            $('#wapaler').css("display", "block");
            $('#msgBoxDK').css("display", "block");

        }
        function jsMsgBoxDKClose() {
            $('#wapaler').css("display", "none");
            $('#msgBoxDK').css("display", "none");
            var check_dialog_visible = $('#dialog-popup').is(':visible');
            var check_order_visible = $('#order-form').is(':visible');
          
        }
    </script>		
</asp:Content>
