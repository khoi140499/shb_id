﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.nhothu
Imports Business.Common.mdlCommon
Imports Business.BaoCao
Imports Business.Common
Imports System.IO
Imports System.Xml
Imports CorebankServiceESB
''' <summary>
''' Hoàn thiện thông tin đăng ký của NNT
''' Chức năng Tạo mới: Chỉ dùng trong trường hợp NNT đăng kí thông tin ủy quyền trích nợ tài khoản tại NHTM 
''' Chức năng Ghi và chuyển KS: sử dụng đối với những đăng ký ở trạng thái ĐÃ DUYỆT YÊU CẦU
''' </summary>
''' <remarks>Số tài khoản phải được kiểm tra và lấy được Tên tài khoản</remarks>
Partial Class NHOTHU_frm217
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        txtMA_DV.Enabled = True

        txtTEN_DV.Enabled = False
        txtDIACHI.Enabled = False
        txtHO_TEN.Enabled = False
        txtSO_CMT.Enabled = False
        txtNGAYSINH.Enabled = False
        txtNGUYENQUAN.Enabled = False
    
        txtMA_NH_TH.Enabled = False
        txtTEN_NH_TH.Enabled = False
      
        txtMA_NH_TH.Enabled = False
        txtTEN_NH_TH.Enabled = False
       
        txtNGAY_HL_UQ.Enabled = False
        txtNGAY_HHL_UQ.Enabled = False
        txtstrdisabled.Value = "disabled"
       
      
    End Sub

   
    ''' <summary>
    ''' Hoàn thiện thông tin NNT và chuyển sang cho KSV
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnChuyenKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckHS.Click
        Dim strThongTinLineHe As String = ""
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        Clear()
        If txtMA_DV.Text = "" Then
            txtMA_DV.Focus()
            clsCommon.ShowMessageBox(Me, "Mã số thuế ngân hàng không được để trống")
            Exit Sub
        End If
        Try
            Dim strError As String = "0"
            Dim strErrMSG As String = ""
            Dim strMsgXML As String = CustomsServiceV3.ProcessMSG.getMSG108(txtMA_DV.Text, strError, strErrMSG)
            If (Not strError.Equals("0")) Then
                clsCommon.ShowMessageBox(Me, "Có lỗi truy vấn [" & strError & "]" & strErrMSG)
                Exit Sub
            Else
                strMsgXML = strMsgXML.Replace("<Customs>", CustomsServiceV3.ProcessMSG.strXMLdefin2)
                Dim msg As CustomsV3.MSG.MSG217.MSG217 = New CustomsV3.MSG.MSG217.MSG217()
                msg = msg.MSGToObject(strMsgXML)
                If (msg.Document.Data.TrangThaiDK.Equals("0")) Then
                    txtTrangthaiHS.Text = "MST chưa có đăng ký"
                    Exit Sub
                ElseIf msg.Document.Data.TrangThaiDK.Equals("1") Then
                    txtTrangthaiHS.Text = "MST đã có đăng ký hết hiệu lực"
                Else
                    txtTrangthaiHS.Text = "MST đã có đăng ký còn hiệu lực"
                End If
                txtSO_HS.Text = msg.Document.Data.ThongTinDK.So_HS
                txtTEN_DV.Text = msg.Document.Data.ThongTinDK.Ten_DV
                txtDIACHI.Text = msg.Document.Data.ThongTinDK.DiaChi
                'Ngày hiệu lực được thay bằng ngày hiệu lực trên biên bản đã ký với NNT
                txtNGAY_HL_UQ.Text = Business_HQ.Common.mdlCommon.ConvertStringToDate(msg.Document.Data.ThongTinDK.Ngay_HL_UQ, "YYYY-MM-DD").ToString("dd/MM/yyyy")
                txtNGAY_HHL_UQ.Text = Business_HQ.Common.mdlCommon.ConvertStringToDate(msg.Document.Data.ThongTinDK.Ngay_HHL_UQ, "YYYY-MM-DD").ToString("dd/MM/yyyy")
                txtHO_TEN.Text = msg.Document.Data.ThongTinDK.ThongTin_NNT.Ho_Ten
                txtSO_CMT.Text = msg.Document.Data.ThongTinDK.ThongTin_NNT.So_CMT
                'txtNGAYSINH.Text = Business_HQ.Common.mdlCommon.ConvertStringToDate(msg.Document.Data.ThongTinDK.ThongTin_NNT.NgaySinh, "YYYY-MM-DD").ToString("dd/MM/yyyy")
                txtNGAYSINH.Text = msg.Document.Data.ThongTinDK.ThongTin_NNT.NgaySinh
                txtNGUYENQUAN.Text = msg.Document.Data.ThongTinDK.ThongTin_NNT.NguyenQuan
                Dim strThongtin As String = ""
                If msg.Document.Data.ThongTinDK.ThongTin_NNT.ThongTinLienHe.Count > 0 Then
                    Dim x As Integer = 0
                    For Each vnode As CustomsV3.MSG.MSG217.ThongTinLienHe In msg.Document.Data.ThongTinDK.ThongTin_NNT.ThongTinLienHe
                        x = x + 1
                        If strThongtin.Length > 0 Then
                            strThongtin &= "#"
                        End If
                        strThongtin &= x
                        strThongtin &= "|" & vnode.So_DT
                        strThongtin &= "|" & vnode.Email
                    Next
                End If
                txtThongTin.Value = strThongtin
                txtSERIALNUMBER.Text = msg.Document.Data.ThongTinDK.ThongTin_NNT.ChungThuSo.SerialNumber
                txtNOICAP.Text = msg.Document.Data.ThongTinDK.ThongTin_NNT.ChungThuSo.Noi_Cap
                'txtNGAY_HL.Text = Business_HQ.Common.mdlCommon.ConvertStringToDate(msg.Document.Data.ThongTinDK.ThongTin_NNT.ChungThuSo.Ngay_HL, "YYYY-MM-DD").ToString("dd/MM/yyyy")
                'txtNGAY_HHL.Text = Business_HQ.Common.mdlCommon.ConvertStringToDate(msg.Document.Data.ThongTinDK.ThongTin_NNT.ChungThuSo.Ngay_HHL, "YYYY-MM-DD").ToString("dd/MM/yyyy")
                txtNGAY_HL.Text = msg.Document.Data.ThongTinDK.ThongTin_NNT.ChungThuSo.Ngay_HL
                txtNGAY_HHL.Text = msg.Document.Data.ThongTinDK.ThongTin_NNT.ChungThuSo.Ngay_HHL
                txtMA_NH_TH.Text = msg.Document.Data.ThongTinDK.ThongTin_NNT.ThongTinTaiKhoan.Ma_NH_TH
                txtTEN_NH_TH.Text = msg.Document.Data.ThongTinDK.ThongTin_NNT.ThongTinTaiKhoan.Ten_NH_TH
                txtTAIKHOAN_TH.Text = msg.Document.Data.ThongTinDK.ThongTin_NNT.ThongTinTaiKhoan.TaiKhoan_TH
                txtTEN_TAIKHOAN_TH.Text = msg.Document.Data.ThongTinDK.ThongTin_NNT.ThongTinTaiKhoan.Ten_TaiKhoan_TH
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi: " & ex.Message)
        End Try
    End Sub
    Shared dt As New DataTable
    Shared dem As Integer = 0
    Sub Clear()
        txtTrangthaiHS.Text = ""
        txtSO_HS.Text = ""
        txtTEN_DV.Text = ""
        txtDIACHI.Text = ""

        txtNGAY_HL_UQ.Text = ""
        txtNGAY_HHL_UQ.Text = ""
        txtHO_TEN.Text = ""
        txtSO_CMT.Text = ""
        txtNGAYSINH.Text = ""
        txtNGUYENQUAN.Text = ""

        txtThongTin.Value = ""
        txtSERIALNUMBER.Text = ""
        txtNOICAP.Text = ""
        txtNGAY_HL.Text = ""
        txtNGAY_HHL.Text = ""
        txtMA_NH_TH.Text = ""
        txtTEN_NH_TH.Text = ""
        txtTAIKHOAN_TH.Text = ""
        txtTEN_TAIKHOAN_TH.Text = ""
    End Sub


    'Protected Sub btnDieuChinh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDieuChinh.Click

    '    'cho phep sua so tai khoan
    '    txtCHK_TAIKHOAN_TH.Enabled = False

    '    txtMA_DV.Enabled = False
    '    txtTEN_DV.Enabled = False
    '    txtDIACHI.Enabled = False
    '    txtHO_TEN.Enabled = False
    '    txtSO_CMT.Enabled = False
    '    txtNGAYSINH.Enabled = False
    '    txtNGUYENQUAN.Enabled = False
    '    txtSERIALNUMBER.Enabled = False
    '    txtNGAY_HHL.Enabled = False
    '    txtNGAY_HL.Enabled = False
    '    txtNOICAP.Enabled = False
    '    txtMA_NH_TH.Enabled = False
    '    txtTEN_NH_TH.Enabled = False
    '    txtLoai_HS.Value = "3"
    '    txtTenLoai_HS.Text = "Khai sửa"
    '    txtNGAY_KY_HDUQ.ReadOnly = False
    '    txtNGAY_KY_HDUQ.Enabled = False

    '    btnCheckTK.Enabled = False
    '    txtTAIKHOAN_TH.Enabled = False
    '    txtTEN_NH_TH.Enabled = False
    '    txtNGAY_HL_UQ.Enabled = True
    '    txtNGAY_HHL_UQ.Enabled = True

    '    'txtNGAY_KY_HDUQ.Text = DateTime.Now.ToString("dd/MM/yyyy")
    '    txtNGAY_KY_HDUQ.Enabled = False
    '    txtTrangThai.Value = "04"
    'End Sub
   
End Class
