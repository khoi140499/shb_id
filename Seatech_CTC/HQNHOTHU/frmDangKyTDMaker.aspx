﻿<%@ Page Title="" Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmDangKyTDMaker.aspx.vb" Inherits="HQNHOTHU_frmDangKyTDMaker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .maxStyle80 {
            width:80%;
        }
        .maxStyle {
            width:99%;
        }
    </style>
     <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
     <script src="../javascript/CheckDate.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <p class="pageTitle">Hoàn thiện đăng ký NTDT</p>
    <div class="container">
        <table style="width: 100%">
            <tr>
                <td style="width:300px;">
                    <%-- danh sach ho so cho xu ky --%>
                    <asp:GridView ID="grdDSCT" runat="server" AutoGenerateColumns="False"
                        AllowPaging="True" ShowFooter="True"  EmptyDataText="No record" BorderColor="#000000" CssClass="grid_data" PageSize="15"
                        Width="100%">
                        <PagerStyle CssClass="cssPager" />
                        <AlternatingRowStyle CssClass="grid_item_alter" />
                        <HeaderStyle BackColor="#0072BC" CssClass="grid_header" HorizontalAlign="Left"></HeaderStyle>
                        <RowStyle CssClass="grid_item" />
                        <Columns>
                            <asp:TemplateField HeaderText="Trạng thái" FooterText="Tổng số CT:">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTrangthai" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TRANG_THAI") %>' />
                                </ItemTemplate>

                            </asp:TemplateField>
                         
                            <asp:TemplateField HeaderText="Số tài khoản" >
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnSelectCT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TAIKHOAN_TH")%>'
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID") %>'
                                        OnClick="btnSelectCT_Click" />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <%#grdDSCT.Rows.Count.ToString%>
                                </FooterTemplate>
                            </asp:TemplateField>
                               <asp:BoundField HeaderText="Mã Số Thuế" DataField="MST" />
                        </Columns>
                        <PagerStyle HorizontalAlign="Left" BackColor="#E4E5D7"></PagerStyle>
                    </asp:GridView>
                </td>
                <td valign="top">
                    <%----noi dung xu ly --%>
                    <table cellpadding="2" cellspacing="1"  border="0" width="100%" class="form_input">
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Số hồ sơ</td>
                            <td class="form_control" style="text-align: left">
                                <asp:TextBox ID="txtSO_HS" runat="server" MaxLength="20" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="false"></asp:TextBox>
                                <asp:Label  ID="lblTrangthai" runat="server"></asp:Label>

                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Mã số thuế</td>
                            <td class="form_control" style="text-align: left">
                                <asp:TextBox ID="txtMST" runat="server" MaxLength="20" CssClass="inputflat txtHoanThien"  Enabled="false"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Số tài khoản</td>
                            <td class="form_control" style="text-align: left">
                                <asp:TextBox ID="TextBox1" runat="server"  CssClass="inputflat txtHoanThien maxStyle80"  Enabled="false"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">Tên Tài khoản</td>
                            <td class="form_control" style="text-align: left">
                                <asp:TextBox ID="TextBox2" runat="server"  CssClass="inputflat txtHoanThien maxStyle"  ReadOnly="true" Enabled="false"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">CIFNO</td>
                            <td class="form_control" style="text-align: left">
                                <asp:TextBox ID="TextBox3" runat="server"  CssClass="inputflat txtHoanThien"  ReadOnly="true" Enabled="false"></asp:TextBox></td>
                        </tr>
                          <tr>
                            <td align="left" style="width: 25%" class="form_label">Từ ngày</td>
                            <td class="form_control" style="text-align: left">
                            <span style="display:block; width:40%; position:relative;vertical-align:middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                    onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span>

                            </td>
                        </tr>
                          <tr>
                            <td align="left" style="width: 25%" class="form_label">Đến ngày</td>
                            <td class="form_control" style="text-align: left">
                                  <span style="display:block; width:40%; position:relative; vertical-align:middle;">
                    <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                     onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span></td>
                        </tr>
                         <tr>
                            <td align="left" style="width: 25%" class="form_label">NTDT</td>
                            <td class="form_control" style="text-align: left">
                                <asp:DropDownList ID="cboNTDT" runat="server" >
                                    <asp:ListItem Value="N" Text="Không" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="Y" Text="Có"></asp:ListItem>
                                </asp:DropDownList> </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">HQ247</td>
                            <td class="form_control" style="text-align: left">
                                <asp:DropDownList ID="cboHQ247" runat="server" >
                                    <asp:ListItem Value="N" Text="Không" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="Y" Text="Có"></asp:ListItem>
                                </asp:DropDownList> </td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 25%" class="form_label">HQ nhờ thu</td>
                            <td class="form_control" style="text-align: left">
                                <asp:DropDownList ID="cboHQNT" runat="server" >
                                    <asp:ListItem Value="N" Text="Không" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="Y" Text="Có"></asp:ListItem>
                                </asp:DropDownList> </td>
                        </tr>
                         <tr>
                            <td align="left" style="width: 25%" class="form_label">Lý do chuyển trả/ Hủy</td>
                            <td class="form_control" style="text-align: left">
                                <asp:TextBox ID="TextBox4" runat="server"  CssClass="inputflat txtHoanThien maxStyle" TextMode="MultiLine"   Enabled="false"></asp:TextBox></td>
                        </tr>
                       <tr>
                <td align="center" colspan="2">
                    <asp:Button ID="btnSave" CssClass="buttonField" runat="server" Text="Lưu chuyển KS"  />&nbsp;&nbsp;&nbsp;&nbsp;  
                    <asp:Button ID="btnHuy" CssClass="buttonField" runat="server" Text="Hủy chuyển KS"  />
                    
                    
                </td>
                
            </tr>
            <tr>
                 <td align="center" colspan="2">
                 <asp:Label ID="lblError" runat="server" ForeColor="Red" ></asp:Label>
                 </td>
            </tr>
                    </table>
                </td>
            </tr>
        </table>

    </div>
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function () {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>	
</asp:Content>

