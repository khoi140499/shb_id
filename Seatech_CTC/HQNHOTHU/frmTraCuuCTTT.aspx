﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmTraCuuCTTT.aspx.vb" Inherits="HQ247_frmTraCuuCTTT" Title="TRA CỨU CHỨNG TỪ NỘP THUẾ XNK" %>

<%@ Import Namespace="Business_HQ.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <%--<link href="../css/menuStyle.css" type="text/css" rel="stylesheet" />
    <link href="../css/myStyles.css" type="text/css" rel="stylesheet" />
    <link href="../javascript/DatePicker/datePicker.css" type="text/css" rel="stylesheet" />--%>
    <script type="text/javascript">
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }
        function jsChiTietNNT(_ID) {

            window.open(_ID.replace("~", ".."), "_blank", "toolbar=no,scrollbars=yes,menubar=no, addressbar=no,location=no,resizable=yes,top=20,left=20,width=900,height=580");
        }
        function ValidateNgay() {
            var tungay = $('#<%=txtTuNgay.ClientID%>').val();
            var denngay = $('#<%=txtDenNgay.ClientID%>').val();
            if (tungay.trim().length == 0 && denngay.trim().length == 0) {
                return true;
            }
            var tungay1 = tungay.split("/");
            var denngay1 = denngay.split("/");
            var d1 = NaN;
            var d2 = NaN;
            if (tungay1.length == 3) {
                d1 = Date.parse(tungay1[1] + "/" + tungay1[0] + "/" + tungay1[2]);
            }
            if (denngay1.length == 3) {
                d2 = Date.parse(denngay1[1] + "/" + denngay1[0] + "/" + denngay1[2]);
            }
            if (tungay.trim().length == 0) {
                if (d2 != NaN)
                    return true;
                else {
                    alert("Đến ngày không hợp lệ.");
                    return false;
                }
            }
            if (denngay.trim().length == 0) {
                if (d1 != NaN)
                    return true;
                else {
                    alert("Từ ngày không hợp lệ.");
                    return false;
                }
            }
            if (d1 == NaN) {
                alert("Từ ngày không hợp lệ.");
                return false;
            }
            if (d2 == NaN) {
                alert("Đến ngày không hợp lệ.");
                return false;
            }
            if (d1 > d2) {
                alert("Vui lòng nhập Từ ngày đăng ký nhỏ hơn hoặc bằng Đến ngày đăng ký");
                return false;
            }
            return true;
        }
    </script>
    <style type="text/css">
        .container {
            width: 100%;
            margin-bottom: 30px;
        }

        .dataView {
            padding-bottom: 10px;
        }

        .JchkAll, .JchkGrid {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <p class="pageTitle">TRA CỨU CHỨNG TỪ NỘP THUẾ XNK</p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
            <tr>
                <td align="left" style="width: 15%" class="form_label">Từ ngày</td>
                <td class="form_control" align="left" style="width: 35%"><span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                        onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                </span></td>
                <td align="left" style="width: 15%" class="form_label">Đến ngày</td>
                <td class="form_control" style="width: 35%">
                    <span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                        <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                            onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                    </span></td>
            </tr>

            <tr>
                <td align="left" class="form_label">Mã số thuế</td>
                <td align="left" class="form_control">
                    <asp:TextBox ID="txtMST" runat="server" class="inputflat" Width="90%"></asp:TextBox></td>
                <td align="left" class="form_label">Trạng thái</td>
                <td class="form_control">
                    <asp:DropDownList ID="cboTrangThai" runat="server" Width="90%" CssClass="selected inputflat">
                        <asp:ListItem Selected="True" Value="" Text="Tất cả">                     
                        </asp:ListItem>
                    </asp:DropDownList>
                </td>

            </tr>
            <tr>
                <td align="left" class="form_label" style="height: 23px">Số Tài khoản</td>
                <td align="left" class="form_control" style="height: 23px">
                    <asp:TextBox ID="txtSoTK" runat="server" class="inputflat" Width="90%"></asp:TextBox></td>
                <td align="left" class="form_label" style="height: 23px">Số chứng từ </td>
                <td class="form_control" style="height: 23px">
                    <asp:TextBox ID="txtSoCTU" runat="server" class="inputflat" Width="90%"></asp:TextBox></td>
            </tr>
            <tr align="left">
                <td align="left" class="form_label">Tên Người nộp thuế</td>
                <td class="form_control" colspan="3">
                    <asp:TextBox ID="txtTen_NNT" runat="server" class="inputflat" Width="98%"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnTimKiem" CssClass="buttonField" runat="server" OnClientClick="return ValidateNgay();" Text="Tìm kiếm" />&nbsp;&nbsp;
                    <asp:Button ID="btnReSend" CssClass="buttonField" runat="server" Text=" Export " />
                </td>

            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </div>

    <div class="dataView">
        <asp:HiddenField ID="SumTien" runat="server" />
        <asp:HiddenField ID="ListCT_NoiDia" runat="server" />
        <asp:GridView ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="100%" BorderColor="Black" AllowPaging="True" ShowFooter="true">
            <Columns>
                <asp:TemplateField HeaderText="STT">
                    <ItemTemplate>
                        <%#Eval("STT").ToString%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top" />
                    <FooterTemplate><b>Tổng</b></FooterTemplate>
                    <FooterStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ID">
                    <ItemTemplate>
                        <a href="#" onclick="jsChiTietNNT('<%#Eval("NAVIGATEURL").ToString %>')"><%#Eval("ID").ToString+"&nbsp;"%></a>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>

                <asp:BoundField DataField="SO_CT_NH" HeaderText="Số CT CoreBank"
                    ReadOnly="True" />
                <asp:BoundField DataField="SO_CTU" HeaderText="Số chứng từ ETax"
                    ReadOnly="True" />
                <%-- <asp:TemplateField HeaderText="Số chứng từ ETax">
                  <ItemTemplate >
                    <a href="#" onclick="jsChiTietNNT('<%#Eval("NAVIGATEURL").ToString %>')"><%#Eval("SO_CTU").ToString%></a>
                  </ItemTemplate>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>--%>
                <asp:BoundField DataField="MA_DV" HeaderText="Mã số thuế" ReadOnly="True" />
                <asp:BoundField DataField="TEN_DV" HeaderText="Tên NNT" ReadOnly="True" />
                <asp:BoundField DataField="TAIKHOAN_TH" HeaderText="Số TK NH" ReadOnly="True" />
                <asp:BoundField HeaderText="Ngày nhận 201" DataField="NGAYNHANMSG" Visible="true" ReadOnly="true" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField HeaderText="Ngày HQ CT" DataField="ngay_tn_ct" Visible="true" ReadOnly="true" ItemStyle-HorizontalAlign="Center" />
                <asp:TemplateField HeaderText="Số tiền">
                    <ItemTemplate>
                        <asp:Label ID="lblSotien" runat="server" Text='<%# mdlCommon.NumberToStringVN(DataBinder.Eval(Container.DataItem, "HQ_SOTIEN_TO"))%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    <FooterTemplate><b>
                        <asp:Label ID="lblTongSotien" runat="server"></asp:Label></b></FooterTemplate>
                    <FooterStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:BoundField DataField="TRANG_THAI" HeaderText="Trạng thái" ReadOnly="True" />
                <asp:BoundField HeaderText="Mô tả lỗi" DataField="Lydo" Visible="true" ReadOnly="true" ItemStyle-HorizontalAlign="Center" />
            </Columns>
            <HeaderStyle BackColor="#999966" BorderColor="Black" BorderStyle="Solid"
                BorderWidth="1px" Font-Bold="True" />
            <AlternatingRowStyle BackColor="#EFEDE4" />
        </asp:GridView>
    </div>
    <div class="dataExport">
        <asp:GridView ID="exportView1" runat="server" AutoGenerateColumns="False" ShowFooter="true" Visible="false"
            Width="100%" BorderColor="#000" CssClass="grid_data" AllowPaging="false">
            <AlternatingRowStyle CssClass="grid_item_alter"></AlternatingRowStyle>
            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
            <RowStyle CssClass="grid_item" />
            <Columns>
                <asp:TemplateField HeaderText="STT">
                    <ItemTemplate>
                        <%#Eval("STT").ToString%>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <HeaderStyle VerticalAlign="Top" />
                    <%-- <FooterTemplate><b>Tổng</b></FooterTemplate>--%>
                    <%--   <FooterStyle HorizontalAlign="Center" />--%>
                </asp:TemplateField>
                <asp:BoundField DataField="ID" HeaderText="ID" DataFormatString="{0}&nbsp;" HtmlEncode="False" ReadOnly="True" />
                <asp:BoundField DataField="SO_CT_NH" HeaderText="Số CT CoreBank"
                    ReadOnly="True" />
                <asp:BoundField DataField="SO_CTU" HeaderText="Số chứng từ ETax" ReadOnly="True" />
                <asp:BoundField DataField="MA_DV" HeaderText="Mã số thuế" ReadOnly="True" />
                <asp:BoundField DataField="TEN_DV" HeaderText="Tên NNT" ReadOnly="True" />
                <asp:BoundField DataField="TAIKHOAN_TH" HeaderText="Số TK NH" ReadOnly="True" />
                <asp:BoundField HeaderText="Ngày nhận 201" DataField="NGAYNHANMSG" Visible="true" ReadOnly="true" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField HeaderText="Ngày HQ CT" DataField="ngay_tn_ct" Visible="true" ReadOnly="true" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="HQ_SOTIEN_TO" HeaderText="Số tiền" ReadOnly="True" />
                <asp:BoundField DataField="TRANG_THAI" HeaderText="Trạng thái" ReadOnly="True" />
                <asp:BoundField HeaderText="Mô tả lỗi" DataField="Lydo" Visible="true" ReadOnly="true" ItemStyle-HorizontalAlign="Center" />
            </Columns>
            <HeaderStyle BackColor="#999966" BorderColor="Black" BorderStyle="Solid"
                BorderWidth="1px" Font-Bold="True" />
            <AlternatingRowStyle BackColor="#EFEDE4" />
        </asp:GridView>
    </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function () {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>
</asp:Content>
