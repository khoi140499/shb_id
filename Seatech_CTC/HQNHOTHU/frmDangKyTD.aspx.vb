﻿Imports System.Data
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao
Imports Business_HQ.nhothu
Imports HQ247XulyMSG
Partial Class HQNHOTHU_frmDangKyTD
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Load_TrangThai()
            Load_Loai_HS()

            DM_DiemThu()
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub
    Private Sub load_dataGrid()
        Dim ds As DataSet

        Dim WhereClause As String = ""
        Try

            If txtMST.Text <> "" Then
                WhereClause += " AND upper(A.MA_DV) like upper('%" & txtMST.Text.Trim & "%')"
            End If
            If txtTuNgay.Value <> "" Then
                WhereClause += " AND TO_CHAR(a.NGAY_NHAN311,'RRRRMMDD') >= " & ConvertDateToNumber(txtTuNgay.Value)
            End If
            If txtDenNgay.Value <> "" Then
                WhereClause += " AND TO_CHAR(a.NGAY_NHAN311,'RRRRMMDD') <= " & ConvertDateToNumber(txtDenNgay.Value)
            End If
            If txtSo_HS.Text.Trim().Length > 0 Then
                WhereClause += " AND A.SO_HS like '%" & txtSo_HS.Text.Trim() & "%' "
            End If
            If drpLoai_HS.SelectedIndex > 0 Then
                WhereClause += " AND A.LOAI_HS = '" & drpLoai_HS.SelectedValue.ToString() & "' "
            End If
            If drpTrangThai.SelectedIndex <> 0 Then
                WhereClause += " AND a.TRANG_THAI = '" + drpTrangThai.SelectedValue + "'"
            End If
            If txtTKNH.Text.Trim().Length > 0 Then
                WhereClause += " AND upper(case when a.trangthai in ('09','10','11','12','13','14','15') then  nvl(a.CUR_TAIKHOAN_TH,a.TAIKHOAN_TH) else a.TAIKHOAN_TH end) like upper('%" + txtTKNH.Text.Trim + "%')"
            End If
            ds = daTCS_HQ247_314.getMSG314FORTRACUUVAXULY(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgrd.CurrentPageIndex = 0
                dtgrd.DataSource = ds.Tables(0)
                dtgrd.DataBind()


            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub
    'Private Sub Load_TrangThai()
    '    Try
    '        Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_YEUCAU"
    '        Dim dr As OracleClient.OracleDataReader
    '        dr = DataAccess.ExecuteDataReader(sql, CommandType.Text)
    '        drpTrangThai.DataSource = dr
    '        drpTrangThai.DataTextField = "MOTA"
    '        drpTrangThai.DataValueField = "TRANG_THAI"
    '        drpTrangThai.DataBind()
    '        dr.Close()
    '        drpTrangThai.Items.Insert(0, "Tất cả")
    '    Catch ex As Exception
    '        clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái yêu cầu. Lỗi: " & ex.Message)
    '    End Try
    'End Sub
    Private Sub Load_TrangThai()
        Try
            Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_YEUCAU"
            Dim ds As DataSet
            ds = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            drpTrangThai.DataSource = ds
            drpTrangThai.DataTextField = "MOTA"
            drpTrangThai.DataValueField = "TRANG_THAI"
            drpTrangThai.DataBind()
            drpTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái yêu cầu. Lỗi: " & ex.Message)
        End Try
    End Sub
    'Private Sub Load_Loai_HS()
    '    Try
    '        Dim sql = "SELECT * FROM TCS_DM_LOAI_HS_NNT ORDER BY LOAI_HS"
    '        Dim dr As OracleClient.OracleDataReader
    '        dr = DataAccess.ExecuteDataReader(sql, CommandType.Text)
    '        drpLoai_HS.DataSource = dr
    '        drpLoai_HS.DataTextField = "MOTA"
    '        drpLoai_HS.DataValueField = "LOAI_HS"
    '        drpLoai_HS.DataBind()
    '        dr.Close()
    '        drpLoai_HS.Items.Insert(0, "Tất cả")
    '    Catch ex As Exception
    '        clsCommon.ShowMessageBox(Me, "Không lấy được danh sách loại hồ sơ người nộp thuế. Lỗi: " & ex.Message)
    '    End Try
    'End Sub
    Private Sub Load_Loai_HS()
        Try
            Dim sql = "SELECT * FROM TCS_DM_LOAI_HS_NNT ORDER BY LOAI_HS"
            Dim ds As DataSet
            ds = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            drpLoai_HS.DataSource = ds
            drpLoai_HS.DataTextField = "MOTA"
            drpLoai_HS.DataValueField = "LOAI_HS"
            drpLoai_HS.DataBind()
            drpLoai_HS.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách loại hồ sơ người nộp thuế. Lỗi: " & ex.Message)
        End Try
    End Sub


    Private Sub DM_DiemThu()
        Dim cnDiemThu As New DataAccess
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        If strcheckHSC = "1" Then
            'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
            ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
            strSQL = "SELECT a.id, a.id ||'-'||a.name nam FROM tcs_dm_chinhanh a order by a.id "
        Else
            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  a.branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' or a.id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' order by a.id"
        End If
    End Sub

    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        load_dataGrid()
        dtgrd.CurrentPageIndex = 0
        dtgrd.DataBind()

    End Sub

    Protected Sub dtgrd_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgrd.PageIndexChanged
        load_dataGrid()
        dtgrd.CurrentPageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim objxl As HQ247XulyMSG.MSG_314 = New HQ247XulyMSG.MSG_314("Y")

        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If ListCT_NoiDia.Value.Length <= 0 Then
            clsCommon.ShowMessageBox(Me, "Chưa chọn chứng từ để gửi lại! ")
            Return
        End If
        Dim soCTULoi As Integer = 0
        'lblError.Text = ListCT_NoiDia.Value

        Dim strListID As String() = ListCT_NoiDia.Value.Split(",")
        For i As Integer = 0 To strListID.Length - 1
            'neu la HQ311_TMP thi xu ly lai message
            If objxl.IsHQ314TMP(strListID(i)) > 0 Then
                objxl.AccountHQ247 = Session.Item("User").ToString()
                objxl.Ma_AccountHQ247 = Session.Item("UserName").ToString()
                Dim MSG As String = objxl.ProcessData314(strListID(i))
                If MSG.Length > 0 Then
                    If MSG.Length >= 3 Then
                        If MSG.Substring(0, 3).ToUpper().Equals("LOI") Then
                            soCTULoi = soCTULoi + 1
                        End If
                    End If
                Else

                End If
            Else
                objxl.xulyHQ314TMP(strListID(i))
            End If
            'con lai xu ly BT
            'soCTULoi += ReSENDHQ(strListID(i))

        Next
        load_dataGrid()
        clsCommon.ShowMessageBox(Me, "Xử lý thành công " & (strListID.Length - soCTULoi) & " / " & strListID.Length.ToString() & " yêu cầu đăng ký!")

    End Sub
End Class
