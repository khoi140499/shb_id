﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmTraCuuVaXulyCTTT.aspx.vb" Inherits="HQ247_frmTraCuuVaXulyCTTT" Title="TRA CỨU VÀ XỬ LÝ CHỨNG TỪ GD" %>

<%@ Import Namespace="Business_HQ.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }
        function jsChiTietNNT(_ID) {
            //window.open(_ID.replace("~", ".."), "_blank", "toolbar=no,scrollbars=yes,menubar=no, addressbar=no,location=no,resizable=yes,top=100,left=200,width=800,height=480");
            var link = _ID.replace("~", "..");
            PopupCenterHoanThien(link, "Hoan thien Ho so NNTHQ", 1000, 750);
        }
    </script>
    <script type="text/javascript">
        function GetListSoCT() {
            var values = $('input:checkbox:checked.JchkGrid').map(function () {
                return this.value;
            }).get();
            if (values == '') {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);
            }
            else {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);

            }
        }
        function jsGetSoCtu(obj) {
            var values = obj.value;
            if (values === '') {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);
             }
             else {
                 $('#<%=ListCT_NoiDia.ClientID %>').val(values);
             }

        }
        function jsExportMSG(_ID, title) {
            createCookie("SEATECHIT_TENDN", '<%=Session.Item("User").ToString() %>', 1);
            window.open("ViewMessage.ashx?ID=" + _ID + "&IN=" + title, "", "", "_new");
             //window.showModalDialog("../NTDT/frmChiTiet.aspx?ID=" + _ID, "", "dialogWidth:800px;dialogHeight:480px;help:0;status:0;", "_new");
        }
        function createCookie(name, value, days) {
            var expires = "";
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            };
            document.cookie = name + "=" + value + expires + "; path=/";
        }
        //----------------------------------------------------
        var newWindowCTu = null;
        function PopupCenterHoanThien(url, title, w, h) {
            if (newWindowCTu != null)
                if (!newWindowCTu.closed) {
                    newWindowCTu.focus();
                    return;
                }
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;
            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
            var systemZoom = width / window.screen.availWidth;
            var left = (width - w) / 2 / systemZoom + dualScreenLeft
            var top = (height - h) / 2 / systemZoom + dualScreenTop
            top = 10;
            left = 10;
            //"_blank", "toolbar=no,scrollbars=yes,menubar=no, addressbar=no,location=no,resizable=yes,top=100,left=200,width=800,height=480"
            newWindowCTu = window.open(url, title, 'toolbar=no,location=no,addressbar=no,scrollbars=1,resizable=yes, width=' + w / systemZoom + ', height=' + h / systemZoom + ', top=' + top + ', left=' + left);
            setTimeout(reFreshdata(), 500);
        }
        function reFreshdata() {

            try {
                if (newWindowCTu.closed) {
                    searchData();
                }
                else {
                    setTimeout(reFreshdata, 500);
                }
            } catch (e) {
                setTimeout(reFreshdata, 500);
            }
        }
        function searchData() {
            document.getElementById('<%=hdfShowMSG.ClientID%>').value = "0";
             document.getElementById('<%= btnTimKiem.ClientID%>').click();
        }
        function ValidateNgay() {
            var tungay = $('#<%=txtTuNgay.ClientID%>').val();
            var denngay = $('#<%=txtDenNgay.ClientID%>').val();
            if (tungay.trim().length == 0) {
                alert("Từ ngày không được để trống");
                return false;
            }
            if (denngay.trim().length == 0) {
                alert("Đến ngày không được để trống");
                return false;
            }
            var tungay1 = tungay.split("/");
            var denngay1 = denngay.split("/");
            if (tungay1.length != 3) {
                alert("Từ ngày không hợp lệ.");
                return false;
            }
            if (denngay1.length != 3) {
                alert("Đến ngày không hợp lệ.");
                return false;
            }
            var d1 = Date.parse(tungay1[1] + "/" + tungay1[0] + "/" + tungay1[2]);
            var d2 = Date.parse(denngay1[1] + "/" + denngay1[0] + "/" + denngay1[2]);
            if (d1 == NaN) {
                alert("Từ ngày không hợp lệ.");
                return false;
            }
            if (d2 == NaN) {
                alert("Đến ngày không hợp lệ.");
                return false;
            }
            if (d1 > d2) {
                alert("Vui lòng nhập Từ ngày đăng ký nhỏ hơn hoặc bằng Đến ngày đăng ký");
                return false;
            }
            return true;
        }
    </script>
    <style type="text/css">
        .container {
            width: 100%;
            margin-bottom: 30px;
        }

        .dataView {
            padding-bottom: 10px;
        }

        .JchkAll, .JchkGrid {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <p class="pageTitle">TRA CỨU VÀ XỬ LÝ CHỨNG TỪ GD</p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
            <tr>
                <td align="left" style="width: 15%" class="form_label">Từ ngày<span class="requiredField">(*)</span></td>
                <td class="form_control" style="width: 35%"><span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                        onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                </span></td>
                <td align="left" style="width: 15%" class="form_label">Đến ngày<span class="requiredField">(*)</span></td>
                <td class="form_control" style="width: 35%">
                    <span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                        <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                            onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                    </span></td>
            </tr>

            <tr>
                <td align="left" class="form_label">Mã số thuế</td>
                <td align="left" class="form_control">
                    <asp:TextBox ID="txtMST" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <td align="left" class="form_label">Trạng thái</td>
                <td class="form_control">
                    <asp:DropDownList ID="cboTrangThai" runat="server" Width="90%" CssClass="selected inputflat">
                        <asp:ListItem Selected="True" Value="" Text="Tất cả">                     
                        </asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" class="form_label">Số Tài khoản</td>
                <td align="left" class="form_control">
                    <asp:TextBox ID="txtSoTK" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <td align="left" class="form_label">Số chứng từ</td>
                <td class="form_control">
                    <asp:TextBox ID="txtSoCTU" runat="server" class="inputflat" Width="89%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" class="form_label">Tên Người nộp thuế</td>
                <td class="form_control" colspan="3" align="left">
                    <asp:TextBox ID="txtTen_NNT" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
            </tr>
            <tr>
                <td class="form_control" colspan="4"></td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnTimKiem" CssClass="buttonField" runat="server" OnClientClick="return ValidateNgay();" Text="Tìm kiếm" />&nbsp;&nbsp;&nbsp;
                   <%-- <asp:Button ID="btnXuly" CssClass="buttonField" runat="server" Text="  Xử lý  " />--%>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div class="dataView">
        <asp:HiddenField runat="server" ID="hdfShowMSG" Value="1" />
        <asp:HiddenField ID="ListCT_NoiDia" runat="server" />
        <asp:GridView ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="100%" BorderColor="Black" AllowPaging="True">
            <Columns>

                <asp:BoundField HeaderText="STT" DataField="STT" Visible="true" ReadOnly="true" />
                <asp:TemplateField  HeaderText="ID">
                <ItemTemplate >
                <a href="#" onclick="jsChiTietNNT('<%#Eval("NAVIGATEURL").ToString %>')"><%#Eval("ID").ToString%></a>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                
                <asp:BoundField DataField="SO_CT_NH" HeaderText="Số CT CoreBank" 
                    ReadOnly="True" SortExpression="SO_CT_NH" />
                <asp:BoundField DataField="SO_CTU" HeaderText="Số chứng từ ETax" ReadOnly="True" SortExpression="SO_CTU" />
                <%--<asp:TemplateField HeaderText="Mã số thuế">
                    <ItemTemplate>
                        <a href="#" onclick="jsChiTietNNT('<%#Eval("NAVIGATEURL").ToString %>')"><%#Eval("MA_DV").ToString%></a>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>--%>
                <asp:BoundField DataField="MA_DV" HeaderText="Mã số thuế" ReadOnly="True" />
                <asp:BoundField DataField="TEN_DV" HeaderText="Tên NNT" ReadOnly="True" />
                <asp:BoundField DataField="TAIKHOAN_TH" HeaderText="Số TK NH" ReadOnly="True" />
                <asp:BoundField HeaderText="Ngày nhận 201" DataField="NGAYNHANMSG" ReadOnly="true" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField HeaderText="Ngày HQ CT" DataField="ngay_tn_ct" ReadOnly="true" ItemStyle-HorizontalAlign="Center" />
                <asp:TemplateField HeaderText="Số tiền">
                    <ItemTemplate>
                        <asp:Label ID="lblSotien" runat="server" Text='<%# mdlCommon.NumberToStringVN(DataBinder.Eval(Container.DataItem, "HQ_SOTIEN_TO")) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:TemplateField>
                <asp:BoundField DataField="TRANG_THAI" HeaderText="Trạng thái" ReadOnly="True" />
                <asp:TemplateField HeaderText="Export message nhận về">
                    <ItemTemplate>
                      <a href="#" onclick="jsExportMSG('<%#Eval("MSG_ID201").ToString%>',0 )"><%#Eval("EXPORT304").ToString%></a>
                    </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Export message gửi đi">
                    <ItemTemplate>
                      <a href="#" onclick="jsExportMSG('<%#Eval("MSG_ID301").ToString %>',1)"><%#Eval("EXPORT213").ToString%></a>
                    </ItemTemplate>
               </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#999966" BorderColor="Black" BorderStyle="Solid"
                BorderWidth="1px" Font-Bold="True" />
            <AlternatingRowStyle BackColor="#EFEDE4" />
        </asp:GridView>
    </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function () {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>
</asp:Content>
