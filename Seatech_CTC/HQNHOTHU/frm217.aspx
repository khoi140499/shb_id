﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frm217.aspx.vb" Inherits="NHOTHU_frm217" title="Hoàn thiện đăng ký NTDT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
        function deleteConfirm(pubid) {
            var result = confirm('Bạn có thực sự muốn xóa tài khoản ' + pubid + ' ?');
            if (result) {
                return true;
            }
            else {
                return false;
            }
        }
        var strdisabled='';
        var strThongtin ='';
        //alert(strThongtin);
        var strHeader=' <table width="80%"  cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">';
            strHeader +='<tr><td  class="form_label">STT</td><td  class="form_label">Số điện thoại<span class="requiredField"> (*)</span></td><td  class="form_label">Email<span class="requiredField"> (*)</span></td>';
            strHeader +='</tr>';
         var strFooter = '</table>';
         function showThongtin()
         {
            strdisabled=document.getElementById('<%=txtstrdisabled.ClientID %>').value ;
             strThongtin=document.getElementById('<%=txtThongTin.ClientID %>').value;
            if (strThongtin.length>0)
            {
                var tbl =strHeader.replace('__DISABLED__',strdisabled);
                var arr = strThongtin.split('#');
                var i=0;
                for(i=0;i<arr.length;i++)
                {
                    var arrtmp=arr[i].split('|');
                    tbl+='<tr>'; 
                    tbl+=' <td  class="form_label"><input type="hidden" id="STT'+arrtmp[0] +'" value="'+arrtmp[0] +'"  />'+arrtmp[0] +' </td>';
                    tbl+='<td  class="form_label"><input type="text" Class="inputflat" id="txtSDT'+arrtmp[0] +'" value="'+arrtmp[1] +'"  style="width:90%" '+strdisabled+'   /> </td>';
                    tbl+='<td  class="form_label"><input type="text" Class="inputflat" id="txtEMAIL'+arrtmp[0] +'" value="'+arrtmp[2] +'" style="width:90%"  '+strdisabled+' /> </td>';
           
             
                    tbl+='</tr>'; 
                }
                document.getElementById('divThongtin').innerHTML=tbl+strFooter;
                
            }else
            {
          //  alert(strdisabled);
                strThongtin='1||';
                 var tbl =strHeader.replace('__DISABLED__',strdisabled);
                var arr = strThongtin.split('#');
                var i=0;
                for(i=0;i<arr.length;i++)
                {
                    var arrtmp=arr[i].split('|');
                    tbl+='<tr>'; 
                    tbl+=' <td  class="form_label"><input type="hidden" id="STT'+arrtmp[0] +'" value="'+arrtmp[0] +'"  />'+arrtmp[0] +' </td>';
                    tbl+='<td  class="form_label"><input type="text" Class="inputflat" id="txtSDT'+arrtmp[0] +'" value="'+arrtmp[1] +'" style="width:90%" '+strdisabled+' /> </td>';
                    tbl+='<td  class="form_label"><input type="text" Class="inputflat" id="txtEMAIL'+arrtmp[0] +'" value="'+arrtmp[2] +'" style="width:90%"   '+strdisabled+' /> </td>';
                  
          
                    tbl+='</tr>'; 
                }
                document.getElementById('divThongtin').innerHTML=tbl+strFooter;
            
            }
         }
         function AddThongtin()
         {
                strThongtin=document.getElementById('<%=txtThongTin.ClientID %>').value;
          if (strThongtin.length>0)
            {
                var strTMP="";
              //  var tbl =strHeader;
                var arr = strThongtin.split('#');
                var i=1;
                var x=0;
                for(i=1;i<=arr.length;i++)
                {
                  
                      x=x+1;
                      if(strTMP.length>0)
                       strTMP+="#";
                      strTMP+=x;
                      if(document.getElementById("txtSDT"+i).value!=null)
                       strTMP+="|"+document.getElementById("txtSDT"+i).value
                       if(document.getElementById("txtEMAIL"+i).value!=null)
                      strTMP+="|" +document.getElementById("txtEMAIL"+i).value
                   
                }
                strThongtin=strTMP;
               
                
            }
            if (strThongtin.length>0)
            {
                 var tbl =strHeader.replace('__DISABLED__',strdisabled);
                var arr = strThongtin.split('#');
                var i=0;
                for(i=0;i<arr.length;i++)
                {
                    var arrtmp=arr[i].split('|');
                    tbl+='<tr>'; 
                    tbl+=' <td  class="form_label" style="width:30px;"><input type="hidden" id="STT'+arrtmp[0] +'" value="'+arrtmp[0] +'"  />'+arrtmp[0] +' </td>';
                    tbl+='<td  class="form_control"><input type="text" Class="inputflat" id="txtSDT'+arrtmp[0] +'" value="'+arrtmp[1] +'" style="width:90%" "'+strdisabled+'  /> </td>';
                    tbl+='<td  class="form_control"><input type="text" Class="inputflat" id="txtEMAIL'+arrtmp[0] +'" value="'+arrtmp[2] +'" style="width:90%" '+strdisabled+'  /> </td>';
                
                
                    tbl+='</tr>'; 
                }
                i=i+1;
                 tbl+='<tr>'; 
                     tbl+=' <td  class="form_label" style="width:30px;"><input type="hidden" id="STT'+i +'" value="'+i +'"  />'+i +' </td>';
                    tbl+='<td  class="form_control"><input type="text" Class="inputflat" id="txtSDT'+i+'" value="" style="width:90%" '+strdisabled+'  /> </td>';
                    tbl+='<td  class="form_control"><input type="text" Class="inputflat" id="txtEMAIL'+i+'" value="" style="width:90%" '+strdisabled+'  /> </td>';
                    
                  
                    tbl+='</tr>'; 
                document.getElementById('divThongtin').innerHTML=tbl+strFooter;
                strThongtin +='#'+i+'||';   
            }else
            {
//               
                strThongtin +='#1||'; 
                document.getElementById('<%=txtThongTin.ClientID %>').value=strThongtin;
                showThongtin();
                return;
                
            }
             document.getElementById('<%=txtThongTin.ClientID %>').value=strThongtin;
            
         }
         function delThongTin(pos)
         {
       
            if (strThongtin.length>0)
            {
                var strTMP="";
              //  var tbl =strHeader;
                var arr = strThongtin.split('#');
                var i=1;
                var x=0;
                for(i=1;i<=arr.length;i++)
                {
                   if(i!=pos)
                   {
                      x=x+1;
                      if(strTMP.length>0)
                       strTMP+="#";
                      strTMP+=x;
                      if(document.getElementById("txtSDT"+i).value!=null)
                       strTMP+="|"+document.getElementById("txtSDT"+i).value
                       if(document.getElementById("txtEMAIL"+i).value!=null)
                      strTMP+="|" +document.getElementById("txtEMAIL"+i).value
                   }
                }
                 strThongtin=strTMP;
                document.getElementById('<%=txtThongTin.ClientID %>').value=strThongtin;
                showThongtin();
                
            }
         }
         function OnSave()
         {
            var strTMP="";
            if (strThongtin.length>0)
            {
               
              //  var tbl =strHeader;
                var arr = strThongtin.split('#');
                var i=1;
                var x=0;
                for(i=1;i<=arr.length;i++)
                {
                   
                      x=x+1;
                      if(strTMP.length>0)
                       strTMP+="#";
                      strTMP+=x;
                      if(document.getElementById("txtSDT"+i).value!=null)
                       strTMP+="|"+document.getElementById("txtSDT"+i).value
                       if(document.getElementById("txtEMAIL"+i).value!=null)
                      strTMP+="|" +document.getElementById("txtEMAIL"+i).value
                   
                }
              
               
                
            }
            strThongtin=strTMP;
            document.getElementById('<%=txtThongTin.ClientID %>').value=strThongtin;
         }

    
    </script>
    <style type="text/css">
        .txtHoanThien{width:80%;}
        .txtHoanThienDate{width:200px;}
        .container{width:100%;padding-bottom:50px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle" >Tra cứu thông tin ủy quyền tchq phát sinh nợ tới NHTM</p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
            <asp:HiddenField runat="server" ID="txtID" />
            <asp:HiddenField runat="server" ID="txtThongTin"  />
            <asp:HiddenField runat="server" ID="txtstrdisabled"  />
            <asp:HiddenField runat="server" ID="txtTrangThai" Value="01"  />
            <asp:HiddenField ID="dieuChinhConfirm" runat="server" Value="" />
             <tr>
                <td align="left" style="width:25%" class="form_label">Mã số thuế <span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtMA_DV" runat="server" CssClass="inputflat txtHoanThienDate" MaxLength="14"  Enabled="false">
                  
                                                                 </asp:TextBox>  <asp:Button ID="btnCheckHS" runat="server" Text="Tra cứu" CssClass="buttonField" /></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Trạng thái</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtTrangthaiHS" runat="server" CssClass="inputflat txtHoanThien" Enabled="false" MaxLength="255" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số hồ sơ</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtSO_HS"  runat="server" MaxLength="20" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="true"></asp:TextBox></td>
            </tr>
            <tr style="display:none;">
                <asp:HiddenField runat="server" ID="txtLoai_HS" />
                <td align="left" style="width:25%" class="form_label">Loại hồ sơ<span class="requiredField"> (*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtTenLoai_HS" Text="Khai mới" MaxLength="20" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="false"></asp:TextBox></td>
            </tr>
           
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên đơn vị XNK<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtTEN_DV" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ></asp:TextBox></td>
            </tr>
            
            <tr>
                <td align="left" style="width:25%" class="form_label">Địa chỉ đơn vị XNK<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtDIACHI" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Họ tên NNT<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtHO_TEN" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số chứng minh thư<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtSO_CMT" runat="server" CssClass="inputflat txtHoanThienDate"  ></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Ngày sinh<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtNGAYSINH" runat="server" CssClass="inputflat txtHoanThienDate" MaxLength="100" onblur="CheckDate(this);"></asp:TextBox> </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Nguyên quán<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtNGUYENQUAN" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ></asp:TextBox></td>
            </tr>
          
            <tr>
             <td align="left" style="width:25%" class="form_label">Thông tin liên hệ <span class="requiredField"> (*)</span></td>
                <td class="form_control" style="text-align:left">
                    <div id="divThongtin">
                    
                    <table width="80%"  cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
                    <tr>
                        <td  class="form_label">STT</td>
                        <td  class="form_label">Số điện thoại</td>
                        <td  class="form_label">Email</td>
                       
                    </tr>
                    <tr>
                        <td  class="form_label"><input type="hidden" id="STT1" value="1"  />1 </td>
                        <td  class="form_label"><input type="text" id="txtSDT1" value="1"  /> </td>
                        <td  class="form_label"><input type="text" id="txtEMAIL1" value="1" /> </td>
                  
                    </tr>
                    </table>
                    </div>
                </td>
            </tr>
           
             
            <tr>
                <td align="left" style="width:25%" class="form_label">Số serial của chứng thư số</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtSERIALNUMBER" runat="server" CssClass="inputflat txtHoanThien" MaxLength="40" ReadOnly="true" Enabled="false"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Đơn vị cấp chứng thư số</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtNOICAP" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ReadOnly="true" Enabled="false"></asp:TextBox></td>
            </tr>
            
                        <tr>
                <td align="left" style="width:25%" class="form_label">Ngày hiệu lực chứng thư số</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtNGAY_HL" runat="server" CssClass="inputflat txtHoanThien" MaxLength="100" ReadOnly="true" Enabled="false"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Ngày hết hiệu lực chứng thư số</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtNGAY_HHL" runat="server" CssClass="inputflat txtHoanThien" MaxLength="100" ReadOnly="true" Enabled="false"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Mã ngân hàng<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtMA_NH_TH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="7" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên ngân hàng<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtTEN_NH_TH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số tài khoản đăng kí</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtTAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThienDate" MaxLength="50" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên tài khoản đăng kí</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtTEN_TAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ReadOnly="true"></asp:TextBox></td>
            </tr>
        
            <tr style="display:none">
                <td align="left" style="width:25%" class="form_label">Ngày hiệu lực <span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <span style="display:block; width:200px; position:relative;vertical-align:middle;">
                <asp:TextBox ID="txtNGAY_KY_HDUQ" runat="server" CssClass="inputflat txtHoanThienDate date-pick dp-applied"
                onblur="CheckDate(this);" onfocus="this.select()"  MaxLength="10" Enabled="false" ></asp:TextBox></span> </td>
            </tr>
            <tr >
                <td align="left" style="width:25%" class="form_label">Ngày hiệu lực UQ <span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <span style="display:block; width:200px; position:relative;vertical-align:middle;">
                <asp:TextBox ID="txtNGAY_HL_UQ" runat="server" CssClass="inputflat txtHoanThienDate date-pick dp-applied"
                onblur="CheckDate(this);" onfocus="this.select()"  MaxLength="10"  ></asp:TextBox></span> </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Ngày hết hiệu lực UQ <span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <span style="display:block; width:200px; position:relative;vertical-align:middle;">
                <asp:TextBox ID="txtNGAY_HHL_UQ" runat="server" CssClass="inputflat txtHoanThienDate date-pick dp-applied"
                onblur="CheckDate(this);" onfocus="this.select()"  MaxLength="10"  ></asp:TextBox></span> </td>
            </tr>
 
            
          
            
            
            <tr>
                <td colspan="2">&nbsp;
                <asp:HiddenField ID="hdfNNTDT_ID" runat="server" />
                <asp:HiddenField ID="hdrTRANG_THAI" runat="server" />
                <asp:HiddenField ID="hdfMA_CN" runat="server" />
                <asp:HiddenField ID="hdfMA_NV" runat="server" />
                <asp:HiddenField ID="hdrMax_SoTKNH" runat="server" />
                <asp:HiddenField ID="hdfCifno" runat="server" />
                <asp:HiddenField ID="hdfMA_CNTK" runat="server" />
                </td>
            </tr>
          
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
        </table>
    </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
//            $('.date-pick').datePicker({
//                clickInput: true,
//                startDate: sDate.asString(),
//                endDate: eDate.asString()
//            })
//            if ($("#txtNGAY_KY_HDUQ").val() == "") {
//                $('.date-pick').datePicker({
//                    clickInput: true
//                })
//            }
        });
        strThongtin= document.getElementById('<%=txtThongTin.ClientID %>').value;
        strdisabled=document.getElementById('<%=txtstrdisabled.ClientID %>').value ;
        showThongtin();
    </script>		
</asp:Content>

