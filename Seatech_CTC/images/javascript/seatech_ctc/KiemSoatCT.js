﻿String.prototype.replaceAll = function(strTarget,strSubString)
    {
        var strText = this;
        var intIndexOfMatch = strText.indexOf( strTarget );
        while (intIndexOfMatch != -1){
            strText = strText.replace( strTarget, strSubString )
            intIndexOfMatch = strText.indexOf( strTarget );
        }
        return( strText );
    } 
    
    function jsKhoiTao()
    {
    jsLoadCTList();
    }
    function jsLoadCTList()
    {       
        var TT= document.getElementById("cboTT").value;
        PageMethods.LoadCTList(TT,LoadCTList_Complete,LoadCTList_Error);
       if (TT !="05")
       {
            document.getElementById("cmdKS").disabled=false;
            document.getElementById("cmdChuyenTra").disabled=false;
       }
       else
       {
            document.getElementById("cmdKS").disabled=true;
            document.getElementById("cmdChuyenTra").disabled=true;
       }
    }
    function LoadCTList_Complete(result,methodName)
    {                             
        divDSCT.innerHTML = result;          
    }
    function LoadCTList_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
            divStatus.innerHTML="Lỗi trong quá trình lấy danh sách chứng từ" + error.get_message();
        }
    }
     //4)Lay mot chung tu cu the
    function jsGetCTU(strSHKB,strSoCT,strSoBT,strSoTien,strSoTT)
    {
        PageMethods.GetCTU(strSHKB,strSoCT,strSoBT,GetCTU_Complete,GetCTU_Error);
        var strMaNTT= document.getElementById("txtMaNNT").value;
        PageMethods.LoadCTChiTiet(strMaNTT,strSHKB,strSoCT,strSoBT,GetCTU_ChiTiet_Complete,GetCTU_Error);
    }
    function GetCTU_ChiTiet_Complete(result,methodName)
    {
        gridChitiet.innerHTML=result;
    }
     function jsGetXMLDoc(xmlString)
    {
        var xmlDoc;
        try //Internet Explorer
        {
            xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async="false";
            xmlDoc.loadXML(xmlString);
            return xmlDoc; 
        }
        catch(e)
        {
            parser=new DOMParser();
            xmlDoc=parser.parseFromString(txt,"text/xml");
            return xmlDoc;
        }
    }
     function GetCTU_Complete(result,methodName)
     {
        var xmlstring = result;
        var xmlDoc = jsGetXMLDoc(xmlstring);
        alert(xmlstring);
        var arrControl =  new Array();
            arrControl[0]='txtSHKB';
            arrControl[1]='txtTenKB';
            arrControl[2]='txtDChiNNT';
            arrControl[3]='txtMaNNTien';
            arrControl[4]='txtDChiNNTien';
            arrControl[5]='txtMaDBHC';
            arrControl[6]='txtTenCQThu';
            arrControl[7]='txtMaCQThu';
            arrControl[8]='txtTenCQThu';
            arrControl[9]='ddlTKNo';
            arrControl[10]='ddlTKCo';
            arrControl[11]='txtMaNNT';
            arrControl[12]='txtTenNNT';
            arrControl[13]='txtSoDu_KH_NH';
            arrControl[14]='txtTK_KH_NH';
            arrControl[15]='txtNGAY_KH_NH';
            arrControl[16]='txtMaNT';
            arrControl[17]='txtTenNT';
            arrControl[18]='ddlLoaiThue';
            arrControl[19]='txtDescLoaiThue';
            arrControl[20]='txtToKhaiSo';
            arrControl[21]='txtNgayDK';
            arrControl[22]='txtLHXNK';
            arrControl[23]='txtDescLHXNK';
            arrControl[24]='txtSoKhung';
            arrControl[25]='txtSoMay';
            arrControl[26]='txtSo_BK';
            arrControl[27]='txtNgay_BK';
            arrControl[28]='txtTTIEN';
            arrControl[29]='txtTT_TTHU';
            arrControl[30]='txtMA_NV';
            arrControl[31]='txtSoCT';
            arrControl[32]='txtSo_BT';
            arrControl[33]='txtTRANG_THAI';
            arrControl[34]='ddlHTTT';
        var strTrang_Thai=XMLToBean(xmlDoc,arrControl);
        var blnEnableEdit = false;
        var blnEnableDelete = false;
       
        if (strTrang_Thai=="00"){//Chua ks
            blnEnableEdit=true;
            blnEnableDelete = false;
        }
        else if (strTrang_Thai=="01"){//Da ks
            blnEnableEdit=false;
            blnEnableDelete = false;
        }
        else if (strTrang_Thai=="03"){//Ks Loi
            blnEnableEdit=true;
            blnEnableDelete = true;
        }
        else if (strTrang_Thai=="04"){//Huy
            blnEnableEdit=false;
            blnEnableDelete = false;
        }
        else if (strTrang_Thai=="05"){//Chuyen KS
            blnEnableEdit=false;
            blnEnableDelete = false;
        }
        if(blnEnableDelete==true) {
          //  jsEnableButtons(3);
        } 
    }
    function GetCTU_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
            divStatus.innerHTML="Lỗi trong quá trình lấy thông tin chứng từ" + error.get_message();
        }
    }
    function XMLToBean(xmlDoc,arrControlName)
    {        
        var strRtTrang_Thai ="00";    
        var strLoaiThue ="";
        var strHTTT = "";
        var rootCTU_HDR = xmlDoc.getElementsByTagName('CTU_HDR')[0];
        for (var i=0;i<= arrControlName.length -1;i++)
            {
            if (rootCTU_HDR.getElementsByTagName(arrControlName[i].substring(3,99))[0].firstChild!=null)
                {  
                    document.getElementById(arrControlName[i]).value = rootCTU_HDR.getElementsByTagName(arrControlName[i].substring(3,99))[0].firstChild.nodeValue;                        
                }
            }
       document.getElementById('txtTT_TTHU').value=localeNumberFormat(document.getElementById('txtTT_TTHU').value,'.');
       if (rootCTU_HDR.getElementsByTagName("TRANG_THAI")[0].firstChild!=null) 
           {
               strRtTrang_Thai = rootCTU_HDR.getElementsByTagName("TRANG_THAI")[0].firstChild.nodeValue;                                                 
           }
        
          ShowHideControlByLoaiThue();
          ShowHideControlByPT_TT();
          return strRtTrang_Thai;      
        }
    function jsGetIndexByValue(ddlObject,cmpVal)
    {
        var obj = document.getElementById(ddlObject);
        for (var i=0;i<obj.length;i++){
            if(obj.options[i].value==cmpVal)
            {
                return i;
            }
        }             
        return 0;   
    }       
     function ShowHideControlByPT_TT()
    {        
         var intPP_TT = document.getElementById("ddlHTTT").selectedIndex;                
        var rTK_KH_NH = document.getElementById("rowTK_KH_NH");        
        var rSoDuNH = document.getElementById("rowSoDuNH");
        
        if (intPP_TT == 0 ){//"NH_TKTM"){
            rTK_KH_NH.style.display = 'none';
            rSoDuNH.style.display = 'none'; 
        }  
        if (intPP_TT == 1 ){//"NH_TKTH_KB"){
            rTK_KH_NH.style.display = '';
            rSoDuNH.style.display = ''; 
        } 
        if (intPP_TT == 2 ){//"NH_TKCT"){
            rTK_KH_NH.style.display = 'none';
            rSoDuNH.style.display = 'none';
        } 
        if (intPP_TT == 3 ){//"NH_TKTG"){
            rTK_KH_NH.style.display = '';
            rSoDuNH.style.display = 'none';
        }                                        
    }
    function ShowHideControlByLoaiThue()
    {   
        var intLoaiThue = document.getElementById("ddlLoaiThue").value;
        var rSoKhung=document.getElementById("rowSoKhung"); 
        var rSoMay=document.getElementById("rowSoMay"); 
        
        var rToKhaiSo=document.getElementById("rowToKhaiSo"); 
        var rNgayDK=document.getElementById("rowNgayDK"); 
        var rLHXNK=document.getElementById("rowLHXNK"); 

        if (intLoaiThue=="01")//"01"
        {
            rSoKhung.style.display = 'none';
            rSoMay.style.display = 'none';
            rToKhaiSo.style.display='none';
            rNgayDK.style.display='none';
            rLHXNK.style.display='none';        
            $get('txtDescLoaiThue').value = 'Thuế khác (Thuế công thương nghiệp)';
        } 
        else if (intLoaiThue=="03")//"03"
        {
            rSoKhung.style.display = '';
            rSoMay.style.display = '';
            
            rToKhaiSo.style.display='none';
            rNgayDK.style.display='none';
            rLHXNK.style.display='none';
            $get('txtDescLoaiThue').value = 'Trước bạ (Nhà đất,xe máy)';   
        }
        else if (intLoaiThue=="04")//"04"
        {
            rSoKhung.style.display = 'none';
            rSoMay.style.display = 'none';
            
            rToKhaiSo.style.display='';
            rNgayDK.style.display='';
            rLHXNK.style.display='';            
            
            $get('txtDescLoaiThue').value = 'Thuế Hải Quan';    
        }
        else //"05
        {
            rSoKhung.style.display = 'none';
            rSoMay.style.display = 'none';
            rToKhaiSo.style.display='none';
            rNgayDK.style.display='none';
            rLHXNK.style.display='none';    
            $get('txtDescLoaiThue').value = 'Không xác định';                             
        }        
    }
    function localeNumberFormat(amount,  delimiter) {
	        try {
		        amount = parseFloat(amount);
	        }
	        catch (e) {
		        throw ('localeNumberFormat caused INVALID FLOAT with value ' + amount);
		        return null;
	        }

	        if (delimiter == null || delimiter == undefined) { delimiter = '.'; }

	        // convert to string
	        if (amount.match != 'function') { amount = amount.toString(); }

	        // validate as numeric
	        var regIsNumeric = /[^\d,\.-]+/igm;
	        var results = amount.match(regIsNumeric);

	        if (results != null) {
		        outputText('INVALID NUMBER', eOutput)
		        return null;
	        }

	        var minus = amount.indexOf('-') >= 0 ? '-' : '';
	        amount = amount.replace('-', '');
	        var amtLen = amount.length;
	        var decPoint = amount.indexOf(',');
	        var wholeNumberEnd = decPoint > 0 ? amtLen - (amtLen - decPoint) : amtLen;

	        var wholeNumber = amount.substr(0, wholeNumberEnd);
	        var fraction = amount.substr(wholeNumberEnd, amtLen - wholeNumberEnd);

	        var segments = (wholeNumberEnd - (wholeNumberEnd % 3)) / 3;
	        var rvsNumber = wholeNumber.split('').reverse().join('');
	        var output = '';

	        for (i = 0; i < wholeNumberEnd; i++) {
		        if (i % 3 == 0 && i != 0 && i != wholeNumberEnd) { output += delimiter; }
		        output += rvsNumber.charAt(i);
	        }

	        output = minus +  output.split('').reverse().join('') + fraction;

	        return output;
    }
    function jsKiemSoat()
    {
          var strSHKB,strSoCT,strSoBT, strTrangThai, strMaNV,strPTTT,strMatKhau,strMaDBHC,strTongTien,strTKKH,strSoduKH_NH, strTongTienTT;
            strSHKB=document.getElementById('txtSHKB').value;
            strSoCT=document.getElementById('txtSoCT').value;
            strSoBT=document.getElementById('txtSo_BT').value;
            strTrangThai=document.getElementById('txtTRANG_THAI').value;
            strMaNV=document.getElementById('txtMA_NV').value;
            strPTTT=document.getElementById('ddlHTTT').value;
            strMatKhau=document.getElementById('txtMatKhau').value;
            strMaDBHC=document.getElementById('txtMaDBHC').value;
            strTongTien=document.getElementById('txtTTIEN').value.replaceAll('.','');
            strTKKH=document.getElementById('txtTK_KH_NH').value.replaceAll('.','');
            strSoduKH_NH=document.getElementById('txtSoDu_KH_NH').value.replaceAll('.','');
            strTongTienTT=document.getElementById('txtTT_TTHU').value.replaceAll('.','');
            PageMethods.KiemSoatCT(strSHKB,strSoCT,strSoBT,strTrangThai,strMaNV,strPTTT,strMatKhau,strMaDBHC,strTongTien,strTongTienTT,strTKKH,strSoduKH_NH,KiemSoat_Complete,KiemSoat_Error);   
    }
     function KiemSoat_Complete(result,methodName)
    {  
            divStatus.innerHTML=result;
            jsLoadCTList();
    }
    function KiemSoat_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
            divStatus.innerHTML="Lỗi trong quá trình kiểm soát chứng từ" + error.get_message();
            
        }
    }
    function jsChuyentra()
    {
            var strSHKB,strSoCT,strSoBT, strTrangThai, strMaNV;
            strSHKB=document.getElementById('txtSHKB').value;
            strSoCT=document.getElementById('txtSoCT').value;
            strSoBT=document.getElementById('txtSo_BT').value;
            strTrangThai=document.getElementById('txtTRANG_THAI').value;
            strMaNV=document.getElementById('txtMA_NV').value;
            PageMethods.ChuyenTraCT(strSHKB,strSoCT,strSoBT,strTrangThai,strMaNV,ChuyenTra_Complete,ChuyenTra_Error);
    }
    function ChuyenTra_Complete(result,methodName)
    {  
            divStatus.innerHTML=result;
            jsLoadCTList();
    }
    function ChuyenTra_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
            divStatus.innerHTML="Lỗi trong quá trình chuyển trả chứng từ" + error.get_message();
            
        }
    }
