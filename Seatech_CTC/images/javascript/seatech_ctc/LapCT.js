﻿ function jsFormatNumber(txtCtl){
        document.getElementById(txtCtl).value = localeNumberFormat(document.getElementById(txtCtl).value.replaceAll('.',''),'.');
    }
    function valInteger(obj) {
            var i, strVal, blnChange;
            blnChange = false
            strVal = "";

            for (i = 0; i < (obj.value).length; i++) {
                switch (obj.value.charAt(i)) {
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9": strVal = strVal + obj.value.charAt(i);
                        break;
                    default: blnChange = true;
                        break;
                }
            }
            if (blnChange) {
                obj.value = strVal;
            }
    }    
    function jsNhapBDS() {
        var mangTS= new Array();
        
        if (divDescCTU.innerHTML.length>0)
        {
            var desCTU= divDescCTU.innerHTML.split(':')[1];  
            mangTS = desCTU.split('/');            
        }else {
            mangTS = "///".split('/');
        }   
        //if (divDescCTU.innerHTML.length>0){
            //var returnValue = window.showModalDialog("../../pages/KhoQuy/frmNhapSoThucThu.aspx?SoCT=" + mangTS[1] +"&SHKB=" + mangTS[0]+ "&SoBT="+ mangTS[2]+ "&SoTo_BDS="+ strSoToBDS, "", "dialogWidth:810px;dialogHeight:650px;help:0;status:0;");               
            var returnValue = window.showModalDialog("../../pages/KhoQuy/frmNhapSoThucThu.aspx?SoCT=" + mangTS[1] +"&SHKB=" + mangTS[0]+ "&SoBT="+ mangTS[2], "", "dialogWidth:810px;dialogHeight:650px;help:0;status:0;");               
            //alert (returnValue);
            if( returnValue =="1"){ 
                //alert (returnValue);
                //document.getElementById('cmdChuyenKS').disable=false;
            }else{
                
            }
             
            
            //lay lai thong tin ve chung tu  
            jsLoadCTList();
            if (divDescCTU.innerHTML.length>0)
            {                
                var strSHKB = divDescCTU.innerHTML.split(':')[1].split('/')[0];
                var strSoCT = divDescCTU.innerHTML.split(':')[1].split('/')[1];
                var strSo_BT = divDescCTU.innerHTML.split(':')[1].split('/')[2];
                jsGetCTU(strSHKB,strSoCT,strSo_BT);
            }
//        }else{        
//            //alert('Không thể lấy danh sách loại tiền tại BDS.Hãy thử lại!!!'); 
//        }
        
    }    
    function localeNumberFormat(amount,  delimiter) {
	    try {
		    amount = parseFloat(amount);
	    }catch (e) {
		    throw ('localeNumberFormat caused INVALID FLOAT with value ' + amount)
		    return null;
	    }
	    if (delimiter == null || delimiter == undefined) { delimiter = '.'; }
	    
	    // convert to string
	    if (amount.match != 'function') { amount = amount.toString(); }

	    // validate as numeric
	    var regIsNumeric = /[^\d,\.-]+/igm;
	    var results = amount.match(regIsNumeric);

	    if (results != null) {
		    outputText('INVALID NUMBER', eOutput)
		    return null;
	    }

	    var minus = amount.indexOf('-') >= 0 ? '-' : '';
	    amount = amount.replace('-', '');
	    var amtLen = amount.length;
	    var decPoint = amount.indexOf(',');
	    var wholeNumberEnd = decPoint > 0 ? amtLen - (amtLen - decPoint) : amtLen;

	    var wholeNumber = amount.substr(0, wholeNumberEnd);
	    var fraction = amount.substr(wholeNumberEnd, amtLen - wholeNumberEnd);

	    var segments = (wholeNumberEnd - (wholeNumberEnd % 3)) / 3;
	    var rvsNumber = wholeNumber.split('').reverse().join('');
	    var output = '';

	    for (i = 0; i < wholeNumberEnd; i++) {
		    if (i % 3 == 0 && i != 0 && i != wholeNumberEnd) { output += delimiter; }
		    output += rvsNumber.charAt(i);
	    }
	    output = minus +  output.split('').reverse().join('') + fraction;
	    return output;
    }    
       
    String.prototype.replaceAll = function(strTarget,strSubString)
    {
        var strText = this;
        var intIndexOfMatch = strText.indexOf( strTarget );
        while (intIndexOfMatch != -1){
            strText = strText.replace( strTarget, strSubString )
            intIndexOfMatch = strText.indexOf( strTarget );
        }
        return( strText );
    } 
 
    function mask(str,textbox,loc,delim){
        var locs = loc.split(',');
        for (var i = 0; i <= locs.length; i++){
	        for (var k = 0; k <= str.length; k++){
	            if (k == locs[i]){
	            if (str.substring(k, k+1) != delim){
	                    str = str.substring(0,k) + delim + str.substring(k,str.length)
	                }
	            }
	        }
        }
        textbox.value = str
    }
    
    function FindSHKB(txtID, txtTitle) {
        var returnValue = window.showModalDialog("../../Find_DM/Find_Khobac.aspx?initParam=" + $get('txtSHKB').value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
        if (document.all) {
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;
            }
        }
        NextFocus();
    }
                
    function FindNNT_NEW(txtID, txtTitle) {
        var returnValue = window.showModalDialog("../../Find_DM/Find_NNT_NEW.aspx?initParam=" + $get('txtMaNNT').value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
        if (document.all) {
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;
            }
        }
        NextFocus();
    }
    
    function FindCQThu(txtID, txtTitle) {    
        var returnValue = window.showModalDialog("../../Find_DM/Find_CQThu.aspx?initParam=" + $get('txtMaCQThu').value, "", "dialogWidth:730px;dialogHeight:500px;help:0;status:0;");
        if (document.all) {
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;
            }
        }
        NextFocus();
    }

    function FindLHXNK(txtID, txtTitle) {
        var returnValue = window.showModalDialog("../../Find_DM/LOV_LHXNK.aspx?initParam=" + $get('txtLHXNK').value, "", "dialogWidth:550px;dialogHeight:500px;help:0;status:0;");
        if (document.all) {
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;
            }
        }
        NextFocus();
    }
    function FindDBHC(txtID, txtTitle) {
        var returnValue = window.showModalDialog("../../Find_DM/Find_DBHC.aspx?initParam=","", "dialogWidth:550px;dialogHeight:500px;help:0;status:0;");
        if (document.all) {
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;
            }
        }
         NextFocus();
    }
           
    function FindMaQuy(txtID) {
        var returnValue = window.showModalDialog("../../Find_DM/Find_MaQuy.aspx", "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;");
        if (document.all) {
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
            }
        }
    }
    function FindMaChuong(txtID) {
        var returnValue = window.showModalDialog("../../Find_DM/Find_MaChuong.aspx", "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;");
        if (document.all) {
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
            }
        }
    }
    function FindMaNganhKT(txtID) {
        var returnValue = window.showModalDialog("../../Find_DM/Find_NganhKT.aspx", "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;");
        if (document.all) {
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
            }
        }
    }
    function FindNDKT(txtID, txtTitle) {
        var returnValue = window.showModalDialog("../../Find_DM/Find_NDKT.aspx", "", "dialogWidth:600px;dialogHeight:500px;help:0;status:0;");
        if (document.all) {
            if (returnValue != null) {
                document.getElementById(txtID).value = returnValue.ID;
                document.getElementById(txtTitle).value = returnValue.Title;
            }
        }
    }

    function NextFocus() {
        if (event.keyCode == 13) {
            event.keyCode = 9;
        }
    }
    function NextControlFocus(IDControl) {
        if (event.keyCode == 13) {
            document.getElementById(IDControl).focus();
        }
    }
                  
    function jsShowHideRowProgress(blnShow)
    {
        if (blnShow) {            
            document.getElementById("rowProgress").style.display='';
        }
        else{
            document.getElementById("rowProgress").style.display='none';
        }
    }
    function jsGetIndexByValue(ddlObject,cmpVal)
    {
        var obj = document.getElementById(ddlObject);
        for (var i=0;i<obj.length;i++){
            if(obj.options[i].value==cmpVal)
            {
                return i;
            }
        }             
        return 0;   
    }       
            
    function jsFillTKNo()
    {
        var cb = document.getElementById('ddlMaTKNo');                
        cb.options.length=0; //
        cb.options[0] = new Option('<< Chọn tài khoản nợ >>', '');
        for (var i=0; i<arrTKNoNSNN.length; i++) {
            var arr = arrTKNoNSNN[i].split(';');                        
            var value = arr[1];
            var label = arr[1] + '-' + arr[2];            
            cb.options[cb.options.length] = new Option(label, value);
        }
    }
    function jsFillTKCo()
    {
        var cb = document.getElementById('ddlMaTKCo');    
        cb.options.length=0; //
        cb.options[0] = new Option('<< Chọn tài khoản có >>', '');    
        for (var i=0; i<arrTKCoNSNN.length; i++) {
            var arr = arrTKCoNSNN[i].split(';');                        
            var value = arr[2];
            var label = arr[2] + '-' + arr[3];            
            cb.options[cb.options.length] = new Option(label, value);
        }
    }
    function jsGet_TenCQThu()
    {
        if (arrCQThu.length>0)
        {
            for (var i=0;i<arrCQThu.length;i++)
            {
                var arr = arrCQThu[i].split(';');
                if (arr[1] == $get('txtMaCQThu').value){
                    $get('txtTenCQThu').value = arr[2];
                    break;
                }
            }
        }
    }
    function jsGet_TenDBHC()
    {       
        if (arrDBHC.length>0)
        {
            for (var i=0;i<arrDBHC.length;i++)
            {
                var arr = arrDBHC[i].split(';');
                if (arr[1] == $get('txtMaDBHC').value){
                    $get('txtTenDBHC').value = arr[2];
                    break;
                }
            }
        }
    }
    function jsCheckMapTK()
    {
        var strMaDBHC = document.getElementById ('txtMaDBHC').value;                              
        var strMaCQThu = document.getElementById ('txtMaCQThu').value;                      
        
        if (strMaCQThu.length ==0) return;
        if (strMaDBHC.length ==0) return;
        for (i=0;i<arrTKCoNSNN.length;i++)
        {            
            var arr = arrTKCoNSNN[i].split(';');            
            var strMaTKCo = document.getElementById('ddlMaTkCo').value;
            if (arr[2]==strMaTKCo)
            {          
                if (strMaDBHC + strMaCQThu == arr[0] + arr[1]) 
                {
                    return true;                
                }
            }
        } 
        return false;
    }
    
    //Lay tai khoan no,co
    function jsGet_TKNo()
    {        
        var strSHKB =document.getElementById ('txtSHKB').value;              
        var strMaDBHC = document.getElementById ('txtMaDBHC').value;    
                  
        //Kiem tra xem neu chua co trong arrTKNo thi moi querry ve db                        
        for (i=0;i<arrTKNoNSNN.length;i++)
        {
            var arr=arrTKNoNSNN[i].split(';');
            if (strSHKB  == arr[0] )
            {
                var intPos = jsGetIndexByValue('ddlMaTkNo',arr[1]);
                document.getElementById('ddlMaTkNo').selectedIndex = intPos;
                break;
            }
        }     
        //PageMethods.Get_TKNo(strSHKB,strMaDBHC,Get_TKNo_Complete,Get_TKNo_Error);                       
    }
   
    function jsGet_TKCo()
    {        
        var strMaDBHC = document.getElementById ('txtMaDBHC').value;                              
        var strMaCQThu = document.getElementById ('txtMaCQThu').value;                      
        //Kiem tra xem neu chua co trong arrTKNo thi moi querry ve db                        

        if (strMaCQThu.length ==0) return;
        if (strMaDBHC.length ==0) return;
        for (i=0;i<arrTKCoNSNN.length;i++)
        {
            
            var arr = arrTKCoNSNN[i].split(';');            
            if (strMaDBHC + strMaCQThu == arr[0] + arr[1])
            {                                
                var intPos = jsGetIndexByValue('ddlMaTkCo',arr[2]);
                document.getElementById('ddlMaTkCo').selectedIndex = intPos;
                break;
            }
        } 
        //PageMethods.Get_TKCo(txtMaDBHC,strMaCQThu,Get_TKCo_Complete,Get_TKCo_Error);                       
    }
   
    function jsGetMaDB()
    {
        jsClearGridHeader(true,true);
        jsClearGridDetail();
        $get('txtMaNNT').value = defMaVangLai;
        jsSetDefaultValues();
        jsGet_TKNo();
        jsGet_TKCo();
        jsShowGridAfterHasMaNNT();     
        jsEnableButtons(1); //Hien thi nut ghi,ghi va chuyen ks               
        $get('txtTenNNT').disabled = false;
        $get('txtTenNNT').focus(); 
        
    }
    function ShowLov(strType)
    {
        if (strType=="SHKB") return FindSHKB('txtSHKB', 'txtTenKB');         
        if (strType=="NNT") return FindNNT_NEW('txtMaNNT', 'txtTenNNT');         
        if (strType=="DBHC") return FindDBHC('txtMaDBHC', 'txtTenDBHC'); 
        if (strType=="CQT") return FindCQThu('txtMaCQThu','txtTenCQThu'); 
        if (strType=="LHXNK") return FindLHXNK('txtLHXNK','txtDescLHXNK');                    
    }    
    function ShowHideControlByPT_TT()
    {        
        var intPP_TT = document.getElementById("ddlMaHTTT").selectedIndex;                
        var rTK_KH_NH = document.getElementById("rowTK_KH_NH");        
        var rSoDuNH = document.getElementById("rowSoDuNH");
        
        if (intPP_TT == 0 ){//"NH_TKTM"){ :tien mat
            rTK_KH_NH.style.display = 'none';
            rSoDuNH.style.display = 'none'; 
        }  
        if (intPP_TT == 1 ){//"NH_TKTH_KB"){ :chuyen khoan
            rTK_KH_NH.style.display = '';
            rSoDuNH.style.display = ''; 
        } 
        if (intPP_TT == 2 ){//"NH_TKCT"){ :dien chuyen tien
            rTK_KH_NH.style.display = 'none';
            rSoDuNH.style.display = 'none';
        } 
        if (intPP_TT == 3 ){//"NH_TKTG"){ :trung gian
            rTK_KH_NH.style.display = '';
            rSoDuNH.style.display = 'none';
        }           
        jsEnabledButtonByBDS();  
    }
    function ShowHideControlByLoaiThue()
    {   
        var intLoaiThue = document.getElementById("ddlMaLoaiThue").selectedIndex;
        var rSoKhung=document.getElementById("rowSoKhung"); 
        var rSoMay=document.getElementById("rowSoMay"); 
        
        var rToKhaiSo=document.getElementById("rowToKhaiSo"); 
        var rNgayDK=document.getElementById("rowNgayDK"); 
        var rLHXNK=document.getElementById("rowLHXNK"); 

        if (intLoaiThue==0)//"01"
        {
            rSoKhung.style.display = 'none';
            rSoMay.style.display = 'none';
            rToKhaiSo.style.display='none';
            rNgayDK.style.display='none';
            rLHXNK.style.display='none';        
            $get('txtTenLoaiThue').value = 'Thuế khác (Thuế công thương nghiệp)';
        } 
        else if (intLoaiThue==1)//"03"
        {
            rSoKhung.style.display = '';
            rSoMay.style.display = '';
            
            rToKhaiSo.style.display='none';
            rNgayDK.style.display='none';
            rLHXNK.style.display='none';
            $get('txtTenLoaiThue').value = 'Trước bạ (Nhà đất,xe máy)';   
        }
        else if (intLoaiThue==2)//"04"
        {
            rSoKhung.style.display = 'none';
            rSoMay.style.display = 'none';
            
            rToKhaiSo.style.display='';
            rNgayDK.style.display='';
            rLHXNK.style.display='';            
            
            $get('txtTenLoaiThue').value = 'Thuế Hải Quan';    
        }
        else //"05
        {
            rSoKhung.style.display = 'none';
            rSoMay.style.display = 'none';
            rToKhaiSo.style.display='none';
            rNgayDK.style.display='none';
            rLHXNK.style.display='none';    
            $get('txtTenLoaiThue').value = 'Không xác định';                             
        }        
    }
    function jsClearGridHeader(clearDefVal,clearMa_NNT)
    {        
         if(clearDefVal==true) $get('txtSHKB').value = '';
         if(clearDefVal==true) $get('txtTenKB').value = '';
         if(clearMa_NNT==true) $get('txtMaNNT').value = '';
         $get('txtTenNNT').value = '';
         $get('txtDChiNNT').value = '';
         $get('txtMaNNTien').value = '';
         $get('txtDChiNNTien').value = '';
         if(clearDefVal==true) $get('txtMaDBHC').value = '';
         if(clearDefVal==true) $get('txtTenDBHC').value = '';
         if(clearDefVal==true) $get('txtMaCQThu').value = '';
         if(clearDefVal==true) $get('txtTenCQThu').value = '';
         $get('txtTenHTTT').value = '';
         $get('txtTK_KH_NH').value = '';
         $get('txtTenTK_KH_NH').value = '';
         $get('txtSoDu_KH_NH').value = '';
         $get('txtDesc_SoDu_KH_NH').value = '';
         $get('txtNGAY_KH_NH').value = '';
         $get('txtDesNGAY_KH_NH').value = '';
         $get('txtTenLoaiThue').value = '';
         $get('txtToKhaiSo').value = '';
         $get('txtNgayDK').value = '';
         $get('txtLHXNK').value = '';
         $get('txtDescLHXNK').value = '';
         $get('txtSoKhung').value = '';
         $get('txtDescSoKhung').value = '';
         $get('txtSoMay').value = '';
         $get('txtDescSoMay').value = '';
         $get('txtSo_BK').value = '';
         $get('txtNgayBK').value = '';   
         
         document.getElementById('ddlMaHTTT').selectedIndex =0;
         document.getElementById('ddlMaLoaiThue').selectedIndex =0;
         document.getElementById('ddlMaTKNo').selectedIndex =0;
         document.getElementById('ddlMaTKCo').selectedIndex =0;
         
         document.getElementById("rowSHKB").style.display='';
         document.getElementById("rowMaNNT").style.display='';
                
         document.getElementById("rowNNTien").style.display='none';
         document.getElementById("rowDChiNNT").style.display='none';
         document.getElementById("rowDChiNNTien").style.display='none';
         document.getElementById("rowDBHC").style.display='none';
         document.getElementById("rowCQThu").style.display='none';
         document.getElementById("rowTKNo").style.display='none';
         document.getElementById("rowTKCo").style.display='none';
         document.getElementById("rowHTTT").style.display='none';
         document.getElementById("rowTK_KH_NH").style.display='none';
         document.getElementById("rowSoDuNH").style.display='none';
         document.getElementById("rowNgay_KH_NH").style.display='none';
         document.getElementById("rowMaNT").style.display='none';
         document.getElementById("rowLoaiThue").style.display='none';
         document.getElementById("rowToKhaiSo").style.display='none';
         document.getElementById("rowNgayDK").style.display='none';
         document.getElementById("rowLHXNK").style.display='none';
         document.getElementById("rowSoKhung").style.display='none';
         document.getElementById("rowSoMay").style.display='none';
         document.getElementById("rowSoBK").style.display='none';
         document.getElementById("rowNgayBK").style.display='none';               
    }
    function jsCheckEmptyVal(cmpCtrl,divCtrl,strMessage)
    {
        if (document.getElementById(cmpCtrl).value =='')
        {
            divCtrl.innerHTML = strMessage;
            return false;
        }        
        return true;
    }
    
    
    //1)Lay thong tin nnthue
    function jsGet_NNThue()
    {        
        var strMaNNT = document.getElementById ('txtMaNNT').value;              
          
        if (strMaNNT.length==0) return;
        
        if (strMaNNT!=curMaNNT){//Chi cho phep querry lai db trong truong hop them moi chung tu
            if (strMaNNT == defMaVangLai)
            {
                jsGetMaDB();
            }
            else 
            {
                jsShowHideRowProgress(true);
                $get('txtTenNNT').disabled = true;
                PageMethods.Get_NNThue(strMaNNT,Get_NNThue_Complete,Get_NNThue_Error);    
            }                                                  
       }
    }
    function Get_NNThue_Complete(result,methodName)
    {     
        if (result.length > 0){                            
             jsShowHideRowProgress(false);
             var xmlstring = result;
             var xmlDoc = jsGetXMLDoc(xmlstring);                                            
             jsClearGridHeader (false,false);
             jsClearGridDetail ();
             XMLToBean(xmlDoc);                          
             jsEnableButtons(1); //Hien thi nut ghi,ghi va chuyen ks               
             $get('txtNgay_KH_NH').value = dtmNgayLV; 
             $get('txtDChiNNT').focus();                                      
        }   
        else{
            divStatus.innerHTML= "Mã NNT này không có trong CSDL.Hãy nhập lại!!!";
            document.getElementById($get('txtMaNNT')).focus();
        }                     
    }
    function Get_NNThue_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
            divStatus.innerHTML="Lỗi trong quá trình lấy thông tin người nộp thuế";   
            document.getElementById($get('txtMaNNT')).focus();         
        }
        jsShowHideRowProgress(false);
    }
    
    //2)Lay thong tin tai khoan ngan hang
    function jsGetTK_KH_NH()
    {
        if ($get('txtTK_KH_NH').value.length >0 )
        {
            PageMethods.GetTK_KH_NH($get('txtTK_KH_NH').value,GetTK_NH_Complete,GetTK_NH_Error);               
        }        
    }
    function GetTK_NH_Complete(result,methodName)
    {                      
        if (result.length > 0){
            var xmlstring = result;
            var xmlDoc = jsGetXMLDoc(xmlstring);                                                
            ProcessTK_KH_NH(xmlDoc);
        }   
        else{
            divStatus.innerHTML='Tài khoản khách hàng không tồn tại.Hãy nhập lại'; 
        }                     
    }
    function GetTK_NH_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
            divStatus.innerHTML="Lỗi trong quá trình lấy tài khoản ngân hàng" + error.get_message();
        }
    }
    
    function ProcessTK_KH_NH(xmlDoc)
    {                       
        var rootCTU_HDR = xmlDoc.getElementsByTagName('CTU_HDR')[0];               
        if (rootCTU_HDR!=null)
        {                                                   
        	//if (rootCTU_HDR.getElementsByTagName("TK_KH_NH")[0].firstChild!=null) $get('txtTK_KH_NH').value = rootCTU_HDR.getElementsByTagName("TK_KH_NH")[0].firstChild.nodeValue;                        
        	if (rootCTU_HDR.getElementsByTagName("SoDu_KH_NH")[0].firstChild!=null) $get('txtSoDu_KH_NH').value = rootCTU_HDR.getElementsByTagName("SoDu_KH_NH")[0].firstChild.nodeValue;        
        	if (rootCTU_HDR.getElementsByTagName("TenTK_KH_NH")[0].firstChild!=null) $get('txtTenTK_KH_NH').value = rootCTU_HDR.getElementsByTagName("TenTK_KH_NH")[0].firstChild.nodeValue;        
        }            
    }
    
    //3)Lay danh sach chung tu
    function jsLoadCTList()
    {       
       PageMethods.LoadCTList(LoadCTList_Complete,LoadCTList_Error);                
    }
    function LoadCTList_Complete(result,methodName)
    {                             
        divDSCT.innerHTML = result;          
    }
    function LoadCTList_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
            divStatus.innerHTML="Lỗi trong quá trình lấy danh sách chứng từ" + error.get_message();
        }
    }
    
    function jsGetXMLDoc(xmlString)
    {
        var xmlDoc;
        if (xmlString==null||xmlString.length==0) return;
        try //Internet Explorer
        {
            xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async="false";
            xmlDoc.loadXML(xmlString);
            return xmlDoc; 
        }
        catch(e)
        {
            parser=new DOMParser();
            xmlDoc=parser.parseFromString(txt,"text/xml");
            return xmlDoc;
        }
    }
    
    //4)Lay mot chung tu cu the
    function jsGetCTU(strSHKB,strSoCT,strSoBT)
    {
        PageMethods.GetCTU(strSHKB,strSoCT,strSoBT,GetCTU_Complete,GetCTU_Error);  
    }
    
    function GetCTU_Complete(result,methodName)
    {                         
        var xmlstring = result;        
        var xmlDoc = jsGetXMLDoc(xmlstring);                                            
        jsClearGridHeader (true,true);
        jsClearGridDetail ();
        XMLToBean(xmlDoc);
        var blnEnableEdit = false;
        
        if (curCtuStatus=="00"){//Chua ks
            blnEnableEdit = false;
            jsEnableButtons(2); 
        }
        else if (curCtuStatus=="01"){//Da ks
            blnEnableEdit= true;
            jsEnableButtons(5); 
        }
        else if (curCtuStatus=="03"){//Ks Loi
            blnEnableEdit=false;
            jsEnableButtons(2); 
        }
        else if (curCtuStatus=="04"){//Huy
            blnEnableEdit = true;
            jsEnableButtons(4); 
        }
        else if (curCtuStatus=="05"){//Chuyen KS
            blnEnableEdit= true;
            jsEnableButtons(5); 
        }
        
        //Lay trang thai cua chung tu cho biet
        jsEnableGridHeader(blnEnableEdit);
        jsEnableGridDetail(blnEnableEdit); 
    }
    function GetCTU_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
            divStatus.innerHTML="Lỗi trong quá trình lấy thông tin chứng từ" + error.get_message();
        }
    }
    
    function jsShowGridAfterHasMaNNT()
    {
        document.getElementById("rowNNTien").style.display='';
        document.getElementById("rowDChiNNT").style.display='';
        document.getElementById("rowDChiNNTien").style.display='';
        document.getElementById("rowDBHC").style.display='';
        document.getElementById("rowCQThu").style.display='';
        document.getElementById("rowTKNo").style.display='';
        document.getElementById("rowTKCo").style.display='';
        document.getElementById("rowHTTT").style.display='';
        document.getElementById("rowNgay_KH_NH").style.display='';
        document.getElementById("rowMaNT").style.display='';
        document.getElementById("rowLoaiThue").style.display='';        
        document.getElementById("rowSoBK").style.display='';
        document.getElementById("rowNgayBK").style.display='';
    }
    
    function XMLToBean(xmlDoc)
    {        
       
        var strLoaiThue ="";
        var strHTTT = "";
                 
        jsShowGridAfterHasMaNNT();    
        var rootCTU_HDR = xmlDoc.getElementsByTagName('CTU_HDR')[0];                       
        try
        {               
            if (rootCTU_HDR!=null)
            {                                       
                if (rootCTU_HDR.getElementsByTagName("NgayLV")[0].firstChild!=null&&rootCTU_HDR.getElementsByTagName("SoCT")[0].firstChild!=null)
        	    {
            	    divDescCTU.innerHTML = 'SHKB/SoCT/SoBT/TT_BDS/TT_NSTT:' + rootCTU_HDR.getElementsByTagName("SHKB")[0].firstChild.nodeValue + "/" + rootCTU_HDR.getElementsByTagName("SoCT")[0].firstChild.nodeValue + "/" + rootCTU_HDR.getElementsByTagName("So_BT")[0].firstChild.nodeValue + "/" + rootCTU_HDR.getElementsByTagName("TT_BDS")[0].firstChild.nodeValue + "/" + rootCTU_HDR.getElementsByTagName("TT_NSTT")[0].firstChild.nodeValue;
            	    curSoCT =rootCTU_HDR.getElementsByTagName("SoCT")[0].firstChild.nodeValue;
            	    curSo_BT =rootCTU_HDR.getElementsByTagName("So_BT")[0].firstChild.nodeValue;
        	    }
        	    if (rootCTU_HDR.getElementsByTagName("SHKB")[0].firstChild!=null) $get('txtSHKB').value = rootCTU_HDR.getElementsByTagName("SHKB")[0].firstChild.nodeValue;                        
        	    if (rootCTU_HDR.getElementsByTagName("TenKB")[0].firstChild!=null) $get('txtTenKB').value = rootCTU_HDR.getElementsByTagName("TenKB")[0].firstChild.nodeValue;        
        	    if (rootCTU_HDR.getElementsByTagName("MaNNT")[0].firstChild!=null) {
        	        $get('txtMaNNT').value = rootCTU_HDR.getElementsByTagName("MaNNT")[0].firstChild.nodeValue;
        	        //Luu mannt hien tai
        	        curMaNNT = rootCTU_HDR.getElementsByTagName("MaNNT")[0].firstChild.nodeValue;
        	    }        
                if (rootCTU_HDR.getElementsByTagName("TenNNT")[0].firstChild!=null) $get('txtTenNNT').value = rootCTU_HDR.getElementsByTagName("TenNNT")[0].firstChild.nodeValue;               	            
                if (rootCTU_HDR.getElementsByTagName("DChiNNT")[0].firstChild!=null) $get('txtDChiNNT').value = rootCTU_HDR.getElementsByTagName("DChiNNT")[0].firstChild.nodeValue;                    
                if (rootCTU_HDR.getElementsByTagName("MaNNTien")[0].firstChild!=null) $get('txtMaNNTien').value = rootCTU_HDR.getElementsByTagName("MaNNTien")[0].firstChild.nodeValue;                    
                if (rootCTU_HDR.getElementsByTagName("DChiNNTien")[0].firstChild!=null) $get('txtDChiNNTien').value = rootCTU_HDR.getElementsByTagName("DChiNNTien")[0].firstChild.nodeValue;                        
                if (rootCTU_HDR.getElementsByTagName("MaCQThu")[0].firstChild!=null) $get('txtMaCQThu').value = rootCTU_HDR.getElementsByTagName("MaCQThu")[0].firstChild.nodeValue;        
                if (rootCTU_HDR.getElementsByTagName("TenCQThu")[0].firstChild!=null) $get('txtTenCQThu').value = rootCTU_HDR.getElementsByTagName("TenCQThu")[0].firstChild.nodeValue;        
                if (rootCTU_HDR.getElementsByTagName("MaDBHC")[0].firstChild!=null) $get('txtMaDBHC').value = rootCTU_HDR.getElementsByTagName("MaDBHC")[0].firstChild.nodeValue;        
                if (rootCTU_HDR.getElementsByTagName("TenDBHC")[0].firstChild!=null) $get('txtTenDBHC').value = rootCTU_HDR.getElementsByTagName("TenDBHC")[0].firstChild.nodeValue;                       
                if (rootCTU_HDR.getElementsByTagName("TKNo")[0].firstChild!=null) {
                    var intPos = jsGetIndexByValue('ddlMaTKNo',rootCTU_HDR.getElementsByTagName("TKNo")[0].firstChild.nodeValue)
                    document.getElementById ('ddlMaTKNo').selectedIndex = intPos;
                }
                if (rootCTU_HDR.getElementsByTagName("TKCo")[0].firstChild!=null) {
                    var intPos = jsGetIndexByValue('ddlMaTKCo',rootCTU_HDR.getElementsByTagName("TKCo")[0].firstChild.nodeValue)
                    document.getElementById ('ddlMaTKCo').selectedIndex = intPos;
                }                       
                if (rootCTU_HDR.getElementsByTagName("HTTT")[0].firstChild!=null) {
                    strHTTT=rootCTU_HDR.getElementsByTagName("HTTT")[0].firstChild.nodeValue;        
                }
                
                if (rootCTU_HDR.getElementsByTagName("SoCTNH")[0].firstChild!=null) {}            
                if (rootCTU_HDR.getElementsByTagName("TK_KH_NH")[0].firstChild!=null) $get('txtTK_KH_NH').value = rootCTU_HDR.getElementsByTagName("TK_KH_NH")[0].firstChild.nodeValue;        
                if (rootCTU_HDR.getElementsByTagName("SoDu_KH_NH")[0].firstChild!=null) $get('txtSoDu_KH_NH').value = rootCTU_HDR.getElementsByTagName("SoDu_KH_NH")[0].firstChild.nodeValue;        
                //if (rootCTU_HDR.getElementsByTagName("TenTK_KH_NH")[0].firstChild.nodeValue!=null) $get('txtTenTK_KH_NH').value = rootCTU_HDR.getElementsByTagName("TenTK_KH_NH")[0].firstChild.nodeValue;        
                
                //if (rootCTU_HDR.getElementsByTagName("Desc_SoDu_KH_NH")[0].firstChild.nodeValue!=null) $get('txtDesc_SoDu_KH_NH').value = rootCTU_HDR.getElementsByTagName("Desc_SoDu_KH_NH")[0].firstChild.nodeValue;        
                if (rootCTU_HDR.getElementsByTagName("NGAY_KH_NH")[0].firstChild!=null) $get('txtNGAY_KH_NH').value = rootCTU_HDR.getElementsByTagName("NGAY_KH_NH")[0].firstChild.nodeValue;        
                //if (rootCTU_HDR.getElementsByTagName("DesNGAY_KH_NH")[0].firstChild.nodeValue!=null) $get('txtDesNGAY_KH_NH').value = rootCTU_HDR.getElementsByTagName("DesNGAY_KH_NH")[0].firstChild.nodeValue;                       
                if (rootCTU_HDR.getElementsByTagName("LoaiThue")[0].firstChild!=null) {
                    strLoaiThue=rootCTU_HDR.getElementsByTagName("LoaiThue")[0].firstChild.nodeValue;            
                    document.getElementById("ddlMaLoaiThue").selectedValue=rootCTU_HDR.getElementsByTagName("LoaiThue")[0].firstChild.nodeValue;            
                }
                if (rootCTU_HDR.getElementsByTagName("DescLoaiThue")[0].firstChild!=null) $get('txtTenLoaiThue').value = rootCTU_HDR.getElementsByTagName("DescLoaiThue")[0].firstChild.nodeValue;                 
                if (rootCTU_HDR.getElementsByTagName("MaNT")[0].firstChild!=null) $get('txtMaNT').value = rootCTU_HDR.getElementsByTagName("MaNT")[0].firstChild.nodeValue;                 
                if (rootCTU_HDR.getElementsByTagName("TenNT")[0].firstChild!=null) $get('txtTenNT').value = rootCTU_HDR.getElementsByTagName("TenNT")[0].firstChild.nodeValue;                                         
                if (rootCTU_HDR.getElementsByTagName("ToKhaiSo")[0].firstChild!=null) $get('txtToKhaiSo').value = rootCTU_HDR.getElementsByTagName("ToKhaiSo")[0].firstChild.nodeValue;                 
                if (rootCTU_HDR.getElementsByTagName("NgayDK")[0].firstChild!=null) $get('txtNgayDK').value = rootCTU_HDR.getElementsByTagName("NgayDK")[0].firstChild.nodeValue;                 
                if (rootCTU_HDR.getElementsByTagName("LHXNK")[0].firstChild!=null) $get('txtLHXNK').value = rootCTU_HDR.getElementsByTagName("LHXNK")[0].firstChild.nodeValue;                 
                if (rootCTU_HDR.getElementsByTagName("DescLHXNK")[0].firstChild!=null) $get('txtDescLHXNK').value = rootCTU_HDR.getElementsByTagName("DescLHXNK")[0].firstChild.nodeValue;                 
                if (rootCTU_HDR.getElementsByTagName("SoKhung")[0].firstChild!=null) $get('txtSoKhung').value = rootCTU_HDR.getElementsByTagName("SoKhung")[0].firstChild.nodeValue;                             
                //if (rootCTU_HDR.getElementsByTagName("DescSoKhung")[0].firstChild.nodeValue!=null) $get('txtDescSoKhung').value = rootCTU_HDR.getElementsByTagName("DescSoKhung")[0].firstChild.nodeValue;                 
                if (rootCTU_HDR.getElementsByTagName("SoMay")[0].firstChild!=null) $get('txtSoMay').value = rootCTU_HDR.getElementsByTagName("SoMay")[0].firstChild.nodeValue;                 
                //if (rootCTU_HDR.getElementsByTagName("DescSoMay")[0].firstChild.nodeValue!=null) $get('txtDescSoMay').value = rootCTU_HDR.getElementsByTagName("DescSoMay")[0].firstChild.nodeValue;                 
                if (rootCTU_HDR.getElementsByTagName("So_BK")[0].firstChild!=null) $get('txtSo_BK').value = rootCTU_HDR.getElementsByTagName("So_BK")[0].firstChild.nodeValue;                 
                if (rootCTU_HDR.getElementsByTagName("Ngay_BK")[0].firstChild!=null) $get('txtNgayBK').value = rootCTU_HDR.getElementsByTagName("Ngay_BK")[0].firstChild.nodeValue;                    
                if (rootCTU_HDR.getElementsByTagName("TRANG_THAI")[0].firstChild!=null) 
                {
                    curCtuStatus = rootCTU_HDR.getElementsByTagName("TRANG_THAI")[0].firstChild.nodeValue;                                                 
                }
                
            }                  
            var intPos = jsGetIndexByValue('ddlMaLoaiThue',strLoaiThue)
            document.getElementById('ddlMaLoaiThue').selectedIndex = intPos;
            ShowHideControlByLoaiThue();
            intPos = jsGetIndexByValue('ddlMaHTTT',strHTTT);
            document.getElementById('ddlMaHTTT').selectedIndex = intPos;
            ShowHideControlByPT_TT();

            FillGridDetailFromXML(xmlDoc);
            
            jsCalTotal ();//Tinh tong tien cua chung tu
        }
        catch(e)
        {}                
    }       
       
    //5)Hủy,Chuyển KS,Khôi phục chứng từ
    function jsUpdate_CTU_Status(strCTUAction)
    {
        var blnEnableEdit = false;
        if (divDescCTU.innerHTML.length>0)
        {
            var arrDesCTU = divDescCTU.innerHTML.split(":")[1].split("/");             
            if (strCTUAction=='2'){//Chuyen kiem soat
                curCtuStatus ='05';
                jsEnableButtons(5);//disable tat ca cac nut                 
                blnEnableEdit = true;
            }else if (strCTUAction=='4'){//Huy            
                curCtuStatus ='04';
                jsEnableButtons(3);//disable tat ca cac nut tru nut khoi phuc                
                blnEnableEdit = true;
            }else if (strCTUAction=='5'){//Khoi phuc
                curCtuStatus ='99';//de trang thai 99 chu khong phai 00
                jsEnableButtons(2);//enable tat ca cac nut tru nut in                                
                blnEnableEdit = false;
            }                       
            //Lay trang thai cua chung tu cho biet
            jsEnableGridHeader(blnEnableEdit);
            jsEnableGridDetail(blnEnableEdit);  
            //alert(curCtuStatus);      
            PageMethods.Update_CTU_Status(arrDesCTU[0],arrDesCTU[1],arrDesCTU[2],arrDesCTU[3],arrDesCTU[4],strCTUAction,Update_CTU_Status_Complete,Update_CTU_Status_Error);           
        }
    }
    function Update_CTU_Status_Complete(result,userContext,methodName)
    {                     
        if (result.length>0){
            var arrKQ = result.split (';');
            if (arrKQ[0] != "0"){
                divStatus.innerHTML= arrKQ[1];
            }
            else{
                divStatus.innerHTML=arrKQ[1];
                jsLoadCTList ();                                  
                divDescCTU.innerHTML = arrKQ[2];
            }        
        }
        else{
            divStatus.innerHTML="Thao tác không thành công.Hãy thực hiện lại!!!"
            //phuc hoi nguyen trang thai cac nut truoc thuc hien
        }        
    }
    function Update_CTU_Status_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
            divStatus.innerHTML="Lỗi trong quá trình cập nhật trạng thái chứng từ" + error.get_message();
            //phuc hoi nguyen trang thai cac nut truoc thuc hien
        }
    }
        
    //6)Ghi,Ghi và chuyển ks
    function jsGhi_CTU(updType)
    {
        //updType=1 : Ghi,=2 Ghi va chuyen ks
        //editMode=1 :Them moi,=2 cap nhat
        var editMode='1';
        if (jsValidForm()==false) return;  
        jsRemoveDumpRow();//Xoa cac row co so tien =0 truoc khi ghi
        var strXML = jsFormToBean();
        if (updType=='1'){
            jsEnableButtons(2); 
        }else if (updType=='2'){//Ghi va chuyen ks
            jsEnableButtons(5);  
        }
        if (divDescCTU.innerHTML.length>0){//Cap nhat chung tu
            editMode='2';                     
        }else{//Them moi chung tu
            editMode='1';
        }
        
        PageMethods.Ghi_CTU(strXML,updType,editMode,Ghi_CTU_Complete,Ghi_CTU_Error);      
    }
    function Ghi_CTU_Complete(result,userContext,methodName)
    {            
        if (result.length>0){
            var arrKQ = result.split (';');
            if (arrKQ[0] != "0"){
                divStatus.innerHTML= arrKQ[1];
                //phuc hoi nguyen trang thai cac nut truoc thuc hien
            }
            else{
                divStatus.innerHTML=arrKQ[1];//'="Ghi mới chứng từ thành công";
                jsLoadCTList ();                                   
                divDescCTU.innerHTML = arrKQ[2];
            }        
        }else{
            divStatus.innerHTML="Thao tác không thành công.Hãy thực hiện lại!!!"
            //phuc hoi nguyen trang thai cac nut truoc thuc hien
        }  
    }
    function Ghi_CTU_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
            divStatus.innerHTML="Lỗi trong quá trình cập nhật chứng từ" + error.get_message();
            //phuc hoi nguyen trang thai cac nut truoc thuc hien
        }
    }
           
    
    //7)Lay thong tin noi dung kinh te
    function jsNoiDungKT(txtMaMuc,txtNoiDung)
    {           
        var userContext = {};
        userContext.ControlID = txtNoiDung;
        
        var strMaNDKT = document.getElementById(txtMaMuc).value ;                     
        if (strMaNDKT.length > 0)
        {
            PageMethods.Get_TenTieuMuc(strMaNDKT,Get_NoiDungKT_Complete,Get_NoiDungKT_Error,userContext);  
        }        
    }
    
    function Get_NoiDungKT_Complete(result,userContext,methodName)
    {                             
        document.getElementById(userContext.ControlID).value=result;
    }
    function Get_NoiDungKT_Error(error,userContext,methodName)
    {
        if(error !== null) 
        {
            divStatus.innerHTML="Lỗi trong quá trình lấy nội dung kinh tế" + error.get_message();
        }
    }                                    
    function jsFormToBean()
    {
                
         var xmlResult = "<?xml version='1.0'?>";
         xmlResult +="<CTC_CTU>";
         xmlResult +="<CTU_HDR>";
         xmlResult +="<NgayLV></NgayLV>";
         xmlResult +="<SoCT>" + curSoCT + "</SoCT>";
         xmlResult +="<So_BT>" + curSo_BT + "</So_BT>";
         xmlResult +="<SHKB>" + $get('txtSHKB').value + "</SHKB>";
         xmlResult +="<TenKB></TenKB>";
         xmlResult +="<MaNNT>" + $get('txtMaNNT').value + "</MaNNT>";
         xmlResult +="<TenNNT>" + $get('txtTenNNT').value + "</TenNNT>";
         xmlResult +="<DChiNNT>" + $get('txtDChiNNT').value + "</DChiNNT>";
         xmlResult +="<MaNNTien>" + $get('txtMaNNTien').value + "</MaNNTien>";
         xmlResult +="<TenNNTien></TenNNTien>";         
         xmlResult +="<DChiNNTien>" + $get('txtDChiNNTien').value + "</DChiNNTien>";
         xmlResult +="<MaCQThu>" + $get('txtMaCQThu').value + "</MaCQThu>";
         xmlResult +="<TenCQThu></TenCQThu>";
         xmlResult +="<MaDBHC>" + $get('txtMaDBHC').value + "</MaDBHC>";         
         xmlResult +="<TenDBHC></TenDBHC>";                  
         xmlResult +="<TKNo>" + document.getElementById ('ddlMaTKNo').value + "</TKNo>";
         xmlResult +="<TenTKNo></TenTKNo>";
         xmlResult +="<TKCo>" + document.getElementById ('ddlMaTKCo').value + "</TKCo>";
         xmlResult +="<TenTKCo></TenTKCo>";                  
         xmlResult +="<HTTT>" + document.getElementById ('ddlMaHTTT').value + "</HTTT>";        
         xmlResult +="<SoCTNH></SoCTNH>";                 
         xmlResult +="<TK_KH_NH>" + $get('txtTK_KH_NH').value + "</TK_KH_NH>";
         xmlResult +="<TenTK_KH_NH>" + $get('txtTenTK_KH_NH').value + "</TenTK_KH_NH>";
         xmlResult +="<NGAY_KH_NH>" + $get('txtNGAY_KH_NH').value + "</NGAY_KH_NH>";         
         xmlResult +="<LoaiThue>" + document.getElementById('ddlMaLoaiThue').value + "</LoaiThue>";
         xmlResult +="<DescLoaiThue></DescLoaiThue>";         
         xmlResult +="<MaNT></MaNT>";
         xmlResult +="<TenNT></TenNT>";         
         xmlResult +="<ToKhaiSo>" + $get('txtToKhaiSo').value + "</ToKhaiSo>";
         xmlResult +="<NgayDK>" + $get('txtNgayDK').value + "</NgayDK>";
         xmlResult +="<LHXNK>" + $get('txtLHXNK').value + "</LHXNK>";
         xmlResult +="<DescLHXNK></DescLHXNK>";         
         xmlResult +="<SoKhung>" + $get('txtSoKhung').value + "</SoKhung>";        
         xmlResult +="<SoMay>" + $get('txtSoMay').value + "</SoMay>";
         xmlResult +="<So_BK>" + $get('txtSo_BK').value + "</So_BK>";
         xmlResult +="<NgayBK>" + $get('txtNgayBK').value + "</NgayBK>";
         xmlResult +="<TRANG_THAI></TRANG_THAI>";
         xmlResult +="<TT_BDS>" + curBDS_Status + "</TT_BDS>";
         xmlResult +="<TT_NSTT>N</TT_NSTT>";         
         xmlResult +="</CTU_HDR>";
         
         xmlResult +=jsBuildXMLFromGridDetail();                                                
         //Lay thong tin cua phan chi tiet
         return xmlResult;                                  
    }
    
    function jsIn_CT()
    {
    
    }
    
    function jsThem_Moi()
    {
        //1)Xoa grid header
        jsClearGridHeader(false,true);//khong xoa gt mac dinh                                 
        jsClearGridDetail();
                       
        jsSetDefaultValues();
        jsFillTKNo();
        jsFillTKCo();
        jsLoadCTList();
        jsEnableGridHeader(false);
        jsEnableGridDetail(false);
                     
        divStatus.innerHTML ="Chế độ làm việc: Lập chứng từ";        
        divDescCTU.innerHTML="";
        jsEnableButtons(0);
        jsShowHideRowProgress(true);
        
        curCtuStatus ='99';                
        divTTBDS.innerHTML = (curBDS_Status=='O'?'Trạng thái BDS : Online':'Trạng thái BDS : Offline');
        //focus ban dau
        $get('txtMaNNT').focus();
    }
    function jsEnableButtons(intButtonType)
    {
        if (intButtonType==0) //trang thai ban dau : ko cho hien thi nut nao ca
        {
            document.getElementById("cmdGhiKS").disabled = true;
            document.getElementById("cmdGhiCT").disabled = true;
            document.getElementById("cmdChuyenKS").disabled = true;
            document.getElementById("cmdInCT").disabled = true;
            document.getElementById("cmdKhoiPhuc").disabled = true;                
            document.getElementById("cmdHuyCT").disabled = true;
        }
        else if(intButtonType==1) //sau khi da co ma so thue ->hien thi 2 nut ghi
        {
            document.getElementById("cmdGhiKS").disabled = false;
            document.getElementById("cmdGhiCT").disabled = false;
            document.getElementById("cmdChuyenKS").disabled = true;
            document.getElementById("cmdInCT").disabled = true;
            document.getElementById("cmdKhoiPhuc").disabled = true;                
            document.getElementById("cmdHuyCT").disabled = true;
        }
        else if(intButtonType==2) //sau khi an nut ghi thanh cong 
        {
            document.getElementById("cmdGhiKS").disabled = false;
            document.getElementById("cmdGhiCT").disabled = false;
            document.getElementById("cmdChuyenKS").disabled = false;
            document.getElementById("cmdInCT").disabled = true;
            document.getElementById("cmdKhoiPhuc").disabled = true;                
            document.getElementById("cmdHuyCT").disabled = false;
        }
        else if(intButtonType==3) // sau khi an nut huy thanh cong
        {
            document.getElementById("cmdGhiKS").disabled = true;
            document.getElementById("cmdGhiCT").disabled = true;
            document.getElementById("cmdChuyenKS").disabled = true;
            document.getElementById("cmdInCT").disabled = true;
            document.getElementById("cmdKhoiPhuc").disabled = false;                
            document.getElementById("cmdHuyCT").disabled = true;
        }
        else if(intButtonType==4) // chung tu da huy cho phep khoi phuc chung tu
        {
            document.getElementById("cmdGhiKS").disabled = true;
            document.getElementById("cmdGhiCT").disabled = true;
            document.getElementById("cmdChuyenKS").disabled = true;
            document.getElementById("cmdInCT").disabled = true;
            document.getElementById("cmdKhoiPhuc").disabled = false ;                
            document.getElementById("cmdHuyCT").disabled = true;4
        }
        else if(intButtonType==5) // chung tu da chuyen sang trang thai cho kiem soat hoac da duoc kiem soat
        {
            document.getElementById("cmdGhiKS").disabled = true;
            document.getElementById("cmdGhiCT").disabled = true;
            document.getElementById("cmdChuyenKS").disabled = true;
            document.getElementById("cmdInCT").disabled = true;
            document.getElementById("cmdKhoiPhuc").disabled = true;                
            document.getElementById("cmdHuyCT").disabled = true;
        }       
        jsEnabledButtonByBDS();                                         
    }
    function jsEnabledButtonByBDS()
    {
        if (document.getElementById ('ddlMaHTTT').value='00'){
         //00,01,03,04,05,99
        if(curCtuStatus=='99'){         
            //alert ('1');           
            if (curBDS_Status=='O' && document.getElementById('ddlMaHTTT').value == '00'){
                document.getElementById('cmdGhiKS').disabled = true; 
                document.getElementById('cmdChuyenKS').disabled = true;
            }else{
                document.getElementById('cmdGhiKS').disabled = false; 
                document.getElementById('cmdChuyenKS').disabled =  (curCtuStatus=='99')?true:false;
            }
       }else if (curCtuStatus=='00'){
            if (divDescCTU.innerHTML.length>0){
                if (divDescCTU.innerHTML.split(':')[1].split('/')[4]=='Y'){
                    document.getElementById('cmdGhiKS').disabled = false; 
                    document.getElementById('cmdChuyenKS').disabled = false;
                    document.getElementById('cmdHuyCT').disabled = true;//khong cho huy ctu da nhap sott
                }else{
                    document.getElementById('cmdGhiKS').disabled = true; 
                    document.getElementById('cmdChuyenKS').disabled = true;
                    document.getElementById('cmdHuyCT').disabled = false;
                }                
            }            
       }
       }
    }
    function jsSetDefaultValues()
    {
        document.getElementById('txtNgay_KH_NH').value=dtmNgayLV;            
        if(defSHKB.length>0)
        {
           var arr1 = defSHKB.split(';');
           if ($get('txtSHKB').value.length==0)
           {
                $get('txtSHKB').value=arr1[0];
                $get('txtTenKB').value=arr1[1];               
           }
        }
        if(defCQThu.length>0)
        {
           var arr2 = defCQThu.split(';');
           if ($get('txtMaCQThu').value.length==0)
           {
                $get('txtMaCQThu').value=arr2[0];
                $get('txtTenCQThu').value=arr2[1];               
           }
        }
        if(defDBHC.length>0)
        {
           var arr3 = defDBHC.split(';');
           if ($get('txtMaDBHC').value.length==0)
           {
                $get('txtMaDBHC').value=arr3[0];
                $get('txtTenDBHC').value=arr3[1];              
           }
        }
    }
    function jsEnableGridHeader(blnEnable)
    {         
           document.getElementById('ddlMaHTTT').disabled = blnEnable;
           document.getElementById('ddlMaLoaiThue').disabled = blnEnable;
           document.getElementById('ddlMaTKCo').disabled = blnEnable;
           document.getElementById('ddlMaTKNo').disabled = blnEnable;                  
           document.getElementById('grdHeader').disabled=blnEnable;              
    }
    function jsEnableGridDetail(blnEnable)
    {    
          document.getElementById('grdChiTiet').disabled=blnEnable;
    }
    