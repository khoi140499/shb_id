﻿function CheckDate(field){
var checkstr = "0123456789";
var DateField = field;
var Datevalue = "";
var DateTemp = "";
var seperator = "/";
var day;
var month;
var year;
var leap = 0;
var err = 0;
var i;
   err = 0;
   DateValue = DateField.value;
   if (DateValue =="")	return;
   /* Delete all chars except 0..9 */
   for (i = 0; i < DateValue.length; i++) {
	  if (checkstr.indexOf(DateValue.substr(i,1)) >= 0) {
	     DateTemp = DateTemp + DateValue.substr(i,1);
	  }
   }
   DateValue = DateTemp;
   /* Always change date to 8 digits - string*/
   /* if year is entered as 2-digit / always assume 20xx */
   if (DateValue.length == 6) {
      DateValue = DateValue.substr(0,4) + '20' + DateValue.substr(4,2); }  
   
   if (DateValue.length != 8) {
      err = 19;}
   /* year is wrong if year = 0000 */
   year = DateValue.substr(4,4);
   if (year == 0) {
      err = 20;
   }
   /* Validation of month*/
   day = DateValue.substr(0,2);
   month = DateValue.substr(2,2);   
   if ((month < 1) || (month > 12)) {
      err = 21;
   }
   /* Validation of day*/
   
   if (day < 1) {
     err = 22;
   }
   
 
   /* Validation leap-year / february / day */
   if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0)) {
      leap = 1;
   }
   if ((month == 2) && (leap == 1) && (day > 29)) {
      err = 23;
   }
   if ((month == 2) && (leap != 1) && (day > 28)) {
      err = 24;
   }
   /* Validation of other months */
   if ((day > 31) && ((month == "01") || (month == "03") || (month == "05") || (month == "07") || (month == "08") || (month == "10") || (month == "12"))) {
      err = 25;
   }
   if ((day > 30) && ((month == "04") || (month == "06") || (month == "09") || (month == "11"))) {
      err = 26;
   }
   /* if 00 ist entered, no error, deleting the entry */
   if ((day == 0) && (month == 0) && (year == 00)) {
      err = 0; day = ""; month = ""; year = ""; seperator = "";
   }
   /* if no error, write the completed date to Input-Field (e.g. 13.12.2001) */
   
   if (err == 0) {
      DateField.value = day + seperator + month + seperator + year;
      if (DateField.value == "")  	
      {
		alert("Định dạng ngày không hợp lệ!");
		DateField.focus();	
		return;
      }      
   }
   /* Error-message if err != 0 */
   else {
		DateField.value = "";
		DateField.focus();
		alert("Định dạng ngày không hợp lệ!");
		return;
   }
   
   if ( parseInt(year) < 1800 )
   {
    	DateField.value = "";
		DateField.focus();
		alert("Định dạng ngày không hợp lệ!");
		return;
   }
}

function CheckMonthYear(field){
var checkstr = "0123456789";
var DateField = field;
var Datevalue = "";
var DateTemp = "";
var seperator = "/";
var month;
var year;
var leap = 0;
var err = 0;
var i;
   err = 0;
   DateValue = DateField.value;
   if (DateValue =="")	return;
   /* Delete all chars except 0..9 */
   for (i = 0; i < DateValue.length; i++) {
	  if (checkstr.indexOf(DateValue.substr(i,1)) >= 0) {
	     DateTemp = DateTemp + DateValue.substr(i,1);
	  }
   }
   DateValue = DateTemp;
   /* Always change date to 6 digits - string*/
   /* if year is entered as 2-digit / always assume 20xx */
   if (DateValue.length == 4) {
      DateValue = DateValue.substr(0,2) + '20' + DateValue.substr(2,2); }  
   
   if (DateValue.length != 6) {
      err = 19;}
   /* year is wrong if year = 0000 */
   year = DateValue.substr(2,4);
   if (year == 0) {
      err = 20;
   }
   /* Validation of month*/
   month = DateValue.substr(0,2);   
   if ((month < 1) || (month > 12)) {
      err = 21;
   }
 
   /* if 00 ist entered, no error, deleting the entry */
   if ((month == 0) && (year == 00)) {
      err = 0; month = ""; year = ""; seperator = "";
   }
   
   if (err == 0) {
      DateField.value = month + seperator + year;
      if (DateField.value == "")  	
      {
		alert("Định dạng ngày không hợp lệ!");
		DateField.focus();	
		return;
      }      
   }
   /* Error-message if err != 0 */
   else {
		DateField.value = "";
		DateField.focus();
		alert("Định dạng ngày không hợp lệ!");
		return;
   }
   
    if ( parseInt(year) < 1800 )
   {
    	DateField.value = "";
		DateField.focus();
		alert("Định dạng ngày không hợp lệ!");
		return;
   }
}



function CheckYear(field){
var checkstr = "0123456789";
var DateField = field;
var Datevalue = "";
var DateTemp = "";
var seperator = "/";
var month;
var year;
var leap = 0;
var err = 0;
var i;
   err = 0;
   
   DateValue = DateField.value;
   
   if (DateValue =="")	return;
   /* Delete all chars except 0..9 */
   for (i = 0; i < DateValue.length; i++) {
	  if (checkstr.indexOf(DateValue.substr(i,1)) >= 0) {
	     DateTemp = DateTemp + DateValue.substr(i,1);
	  }
   }
   DateValue = DateTemp;
   /* Always change date to 6 digits - string*/
   /* if year is entered as 2-digit / always assume 20xx */
   
   if (DateValue.length != 4) {
      err = 19;}
   /* year is wrong if year = 0000 */   
   year = DateValue;
   
   if (year == 0) {
      err = 20;
   }
   
   /* if 00 ist entered, no error, deleting the entry */
      
   if (err == 0) {
      DateField.value = year;
      if (DateField.value == "")  	
      {
		alert("Năm không hợp lệ!");
		DateField.focus();	
		return;
      }      
   }
   /* Error-message if err != 0 */
   else {
		DateField.value = "";
		DateField.focus();
		alert("Năm không hợp lệ!");
		return;
   }
   
   if ( parseInt(DateField.value) < 1900 )
   {
    	alert("Năm không hợp lệ!");
		DateField.focus();	
		return;
   }
}


function CheckMonth(field)
{
	obj = field;
	if(obj.value == "") return;
	if(isNaN(obj.value) || obj.value > 12 || obj.value < 1)
	{
		alert('Tháng không hợp lệ!');
		obj.focus();
		obj.value = "";
		return false;
	}
	if(obj.value.length == 1) obj.value = '0' + obj.value;
}



function CheckQuarter(field)
{
	obj = field;
	if(obj.value == "") return;
	if(isNaN(obj.value) || (obj.value != "1" && obj.value != "2" && obj.value != "3" && obj.value != "4"))
	{
		alert('Quý không hợp lệ!');
		obj.focus();
		obj.value = "";
		return false;
	}
	//if(obj.value.length == 1) obj.value = '0' + obj.value;
}



function trim(txt) {
    txt = txt.replace(/^(\s)+/, '');
    txt = txt.replace(/(\s)+$/, '');
   	return txt;
}
function validateTime(time1) {
	var time = time1.value;
	var iSepPos = time.indexOf(':');
	var sTimeStr = time;
	var sStr1 = new String;
	var sStr2 = new String;
	var sStr3 = new String;

	sStr1 = 'x';
	sStr2 = 'x';
	sStr3 = 'x';

	if (trim(time) == '') {
		return true;
	}

	if (iSepPos > 0) {
	    sStr1 = sTimeStr.substring(0,iSepPos);
	    sTimeStr = sTimeStr.substring(iSepPos + 1, sTimeStr.length);
	}

	iSepPos = sTimeStr.indexOf(':');
	if (iSepPos > 0) {
	    sStr2 = sTimeStr.substring(0,iSepPos);
	    sStr3 = sTimeStr.substring(iSepPos + 1, sTimeStr.length);
	}
	else {
	    sStr2 = sTimeStr;
	    sStr3 = '00';
	}

	var sHour = sStr1;
	var sMinute = sStr2;
	var sSecond = sStr3;

	if (isNaN(sHour)) {
		displayMessage('COMM',1030,time,'00:00:00');
	    return false;
	}
	if (isNaN(sMinute)) {
	    displayMessage('COMM',1030,time,'00:00:00');
	    return false;
	}
	if (isNaN(sSecond)) {
	    displayMessage('COMM',1030,time,'00:00:00');
	    return false;
	}

	if (sValid) {
	    if ((sHour < 0) || (sHour > 23)) {
	        displayMessage('COMM',1030,time,'00:00:00');
	        return false;
	    }
	    if ((sMinute < 0) || (sMinute > 59)) {
	        displayMessage('COMM',1030,time,'00:00:00');
	        return false;
	    }
	    if ((sSecond < 0) || (sSecond > 59)) {
	        displayMessage('COMM',1030,time,'00:00:00');
	        return false;
	    }
	}

	if (sValid) {
	    time = sHour + ':' + sMinute + ':' + sSecond;
	}
	return true;

}
