﻿////////////////////////////////////////////////////////////////////////////
// 
// Copyright (C) 2005 e-Channels CORPORATION
// 
// ALL RIGHTS RESERVED BY e-Channels CORPORATION, THIS PROGRAM
// MUST BE USED SOLELY FOR THE PURPOSE FOR WHICH IT WAS  
// FURNISHED BY e-Channels CORPORATION, NO PART OF THIS PROGRAM
// MAY BE REPRODUCED OR DISCLOSED TO OTHERS, IN ANY FORM
// WITHOUT THE PRIOR WRITTEN PERMISSION OF e-Channels CORPORATION.
// USE OF COPYRIGHT NOTICE DOES NOT EVIDENCE PUBLICATION
// OF THE PROGRAM
// 
//			e-Channels CONFIDENTIAL AND PROPRIETARY
// 
////////////////////////////////////////////////////////////////////////////
//
//	Liana 3.0 系统JS公用函数库
//
// $Log: public.js,v $
// Revision 1.1  2007/01/08 01:26:55  zhoujun
// new framesets
//
// Revision 1.14  2006/12/18 07:12:59  liql
// *** empty log message ***
//
// Revision 1.13  2006/12/15 08:23:58  wangzw
// add getSeltCheckbox() and delSeltChkBox()
//
// Revision 1.10  2006/11/28 05:54:53  wangzw
// 添加isLength方法
//
// Revision 1.9  2006/11/28 02:23:23  wangzw
// *** empty log message ***
//
// Revision 1.7  2006/11/28 02:21:45  wangzw
// *** empty log message ***
//
// Revision 1.6  2006/11/27 10:04:55  wangzw
// 添加isIntFigure方法和isYearMonth方法
//
// Revision 1.5  2006/11/20 06:37:10  zhoujun
// *** empty log message ***
//
// Revision 1.4  2006/11/07 08:03:19  zhuding
// *** empty log message ***
//
// Revision 1.3  2006/10/23 02:25:45  wanggd
// 相关页面 部分 JS
//
// Revision 1.2  2006/10/23 01:52:05  wanggd
// 相关页面 部分 JS
//
// Revision 1.1  2006/10/11 03:20:55  zhanghao
// v3.4 初始版本
//
// Revision 1.2  2006/08/23 03:51:52  zhanghao
// 添加nvl方法
//
// Revision 1.1  2006/08/17 01:34:13  zhanghao
// 初始版本
//
// Revision 1.18  2006/04/29 09:00:48  yangshu
// 修改使用密码控件
//
// Revision 1.17  2006/04/29 07:45:53  yangshu
// 修改使用密码控件
//
// Revision 1.16  2006/04/26 05:51:07  zhanghao
// 完善注释
//
// Revision 1.15  2006/04/21 03:24:31  zhanghao
// 添加方法dateInterval
//
// Revision 1.14  2006/04/14 02:16:44  zhanghao
// 添加方法openWindow
//
// Revision 1.13  2006/03/27 09:48:47  changcq
// 修改脚本
//
// Revision 1.12  2006/03/27 08:09:11  yangshu
// validateAccount 中改为!isShorter(account,length)
//
// Revision 1.11  2006/03/27 01:05:25  zhanghao
// 恢复版本到1.9
//
// Revision 1.9  2006/03/24 06:20:20  zhanghao
// 添加方法selectAll
//
// Revision 1.8  2006/03/20 01:20:01  zhanghao
// 修正
//
// Revision 1.5  2006/03/17 09:08:54  changcq
// 整理js
//
// Revision 1.4  2006/03/17 07:30:32  changcq
// 整理js
//
// Revision 1.3  2006/03/16 03:21:37  zhanghao
// 修正
//
// Revision 1.2  2006/03/16 02:17:02  zhanghao
// js整理
//
//

/**
* 若为null则return空
* @param {string} 要测试的字符串
*
* @return {string} 
*/
function nvl(s)
{
	if ( s == null )
		return "";
	else
		return s;
}

/**
* 将页面中的所有checkbox置为选中状态或全不选状态
* @param {bool} true选中；false不选
*/
function selectAll( choose )
{
	var allCheck = document.all.tags("input");
	for(var i=0; i<allCheck.length; i++){
		var tagBody = allCheck[i];
		if (tagBody.getAttribute("type") == "checkbox")
		{
			tagBody.checked= choose;
		}
	}
}

/**
* 取得页面中的所有选中的checkbox的name列表
* @return {String} 以'|'分隔的选中name，如果没有选中任何checkbox则返回空串
*/
function getCheckList()
{	
	var checkList = '';

	var allCheck = document.all.tags("input");
	for(var i=0; i<allCheck.length; i++)
	{
		var tagBody = allCheck[i];
		if (tagBody.getAttribute("type") == "checkbox")
		{
			if ( tagBody.checked )
			{
				checkList += tagBody.name  + '|';
			}
		}
	}
	
	return checkList;
}

/**
* 判断在列表中任意被选中的checkbox
* @param {String} 表的id
* @return {int[]} 是否为浮点数
*/
function getSeltCheckbox(tableId) {
	tablerows = document.getElementById(tableId).rows;
	var cell,chks,chk;
	arrIndex = new Array(tablerows.length);
	
	for(var i=0; i<tablerows.length; i++) {//数组初始化
		arrIndex[i] = 0;
	}
	
	for(var j=0; j<tablerows.length; j++) {
		cell = tablerows[j].cells[0];
		chks = cell.getElementsByTagName("input");
	
		for(var k=0; k<chks.length; k++) {
			chk = chks[k];
			if(chk.checked) {
				arrIndex[j] = j;
			}
		}
	}
	return arrIndex;
}

/**
* 删除任意被选中的checkbox记录
* @param {String} 表的id
*/
function delSeltChkBox(tableId) {
	var arrIndex = getSeltCheckbox(tableId);
	var count = 0;
	var flag = 0;
	var k = 0;
	for(var i=1; i<arrIndex.length; i++) {				
		if(arrIndex[i] != 0) {
			if(flag != 0) {
				k = i-count;
				document.getElementById(tableId).deleteRow(k);
			} else {
				k = i;
				document.getElementById(tableId).deleteRow(i);
				flag = 1;		
			}			
			count++;
		}				
	}
}
/**
* 判断输入变量是否是浮点数
* @param {String} 要检查的变量值
* @return {bool} 是否为浮点数
*/
function isFloat( s )
{
	var isFloat = RegExp(/^([0-9]+(\.+))[0-9]+$/);
	return ( isFloat.test(s) );
}

/**
* 判断输入变量是否是实数
* @param {String} 要检查的变量值
* @return {bool} 是否为实数
*/
function isDecimal( s )
{
	var isDecimal = RegExp(/^([0-9]+(\.?))?[0-9]+$/);
	return ( isDecimal.test(s) );
}

/**
* 判断输入变量是否是数字或者字母
* @param {String} 要检查的变量值
* @return {bool} 是否为数字或者字母
*/
function isIntChar( s )
{
 	var isIntChar = RegExp(/^[a-zA-Z0-9]+$/);
	return ( isIntChar.test(s) );
 }

/**
* 判断输入变量是否是数字
* @param {String} 要检查的变量值
* @return {bool} 是否为数字或者字母
*/
function isIntFigure( s )
{
 	var isIntFigure = RegExp(/^[0-9]+$/);
	return ( isIntFigure.test(s) );
 }


/**
* 判断输入变量是否是数字或者字母或者特殊字符
* 特殊字符：_ - * & % $ # @ ! ~ ^ ( )
* @param {String} 要检查的变量值
* @return {bool} 是否为数字或者字母或者特殊字符
*/
function isIntCharSpecial( s )
{
	var isIntCharSpecial = RegExp(/^[a-zA-Z0-9(\_)(\-)(\*)(\&)(\%)(\$)(\#)(\@)(\!)(\~)(\^)(\()(\))]+$/);
	return ( isIntCharSpecial.test(s) );
}

/**
* 对数据域重新进行编码
* @param {String} 字符串
* @return {String} 重新编码后的字符串
*/
function encode(s)
{
	if(s == null ) 
		return s;
	if(s.length == 0) 
		return s;
	
	var ret = "";
	for(i=0;i<s.length;i++)
	{
		var c = s.substr(i,1);
		var ts = escape(c);
		if(ts.substring(0,2) == "%u") 
		{
			ret = ret + ts.replace("%u","@@");
		} else {
			ret = ret + c;
		}
	}
	return ret;
}

/**
* 检验账号合法性
* @param {String} 账号
* @param {Integer} 合法的账号长度
* @return {bool} 账号是否合法
*/
function validateAccount(account,length) 
{
	if( isEmpty(account) )
	{
		alert("账号不能为空！");
		return false;
	}
	
	if( !isInteger(account) )
	{
		alert("账号必须为数字！");
		return false;
	}
	
	if( !isShorter(account,length) )
	{
		alert("账号长度为"+length+"位！");
		return false;
	}	
	
	return true;
}

/**
* @param {String} 0～100间的字符串
* @return {bool} 格式化后的字符串
*/
function getFmtRate(str) 
{
	var rate = parseInt(str);
	
	if ( rate > 100 || rate < 0 )
		return("-1");
	
	return (rate/100);
}


/**
* 检验字符串是否为空
* @param {String} 字符串
* @return {bool} 是否为空
*/
function isEmpty(e)
{
	if( trim(e) == "" )
		return true;
	else
		return false;
}

/**
* 去掉字符串前后的空格
* @param {String} 字符串
* @return {String} 去除空格后的字符串
*/
function trim(param) {
	if ((vRet = param) == '') { return vRet; }
	while (true) {
		if (vRet.indexOf (' ') == 0) {
			vRet = vRet.substring(1, parseInt(vRet.length));
		} else if ((parseInt(vRet.length) != 0) && (vRet.lastIndexOf (' ') == parseInt(vRet.length) - 1)) {
			vRet = vRet.substring(0, parseInt(vRet.length) - 1);
		} else {
			return vRet;
		}
	}
}

/**
* 检查字符串的长度
* @param {String} 字符串
* @param {Integer} 要比较的长度
* @return {bool} true：变量长度==给出的长度；false：其它
*/
function isLength(str,reqlength)
{
	if( str.length == reqlength )
		return true;
	else
		return false;
}

/**
* 检查字符串的长度
* @param {String} 字符串
* @param {Integer} 要比较的长度
* @return {bool} true：变量长度<给出的长度；false：变量长度>=给出的长度
*/
function isShorter(str,reqlength)
{
	if( str.length<reqlength )
		return true;
	else
		return false;
}

/**
* 检查字符串是否是整数
* @param {String} 字符串
* @return {bool} 是否是整数
*/
function isInteger( s )
{ 
	var isInteger = RegExp(/^[0-9]+$/);
	return ( isInteger.test(s) );
}

/**
* 检验日期是否符合YYYYMMDD的格式，是否合法
* @param {String} 日期字符串
* @return {bool} 是否是合法日期
*/
function isDate(param) {
	var pattern = /^\d+$/;
	if ( param.length != 8 ) {
		return false;
	}
	if(!pattern.test(param)) {
		return false;
	}
	sYear = param.substring( 0, 4 );
	sMonth = param.substring( 4, 6 );
	sDay = param.substring( 6, 8 );
	if ( ( eval( sMonth ) < 1 ) || ( eval( sMonth )  > 12 ) ) {
		return false;
	}
	var leapYear = ((( sYear % 4 == 0 ) && ( sYear % 100 != 0 )) || ( sYear % 400 == 0 )) ? true : false;
	var monthDay = new Array(12);
	monthDay[0] = 31;
	monthDay[1] = leapYear ? 29 : 28;
	monthDay[2] = 31;
	monthDay[3] = 30;
	monthDay[4] = 31;
	monthDay[5] = 30;
	monthDay[6] = 31;
	monthDay[7] = 31;
	monthDay[8] = 30;
	monthDay[9] = 31;
	monthDay[10] = 30;
	monthDay[11] = 31;
	if ( ( eval( sDay ) < 1 ) || ( eval( sDay )  > monthDay[eval(sMonth)-1] ) ) {
		return false;
	}
	return true;
}

/**
* 检验日期是否符合YYYYMM的格式，是否合法
* @param {String} 日期字符串
* @return {bool} 是否是合法日期
*/
function isYearMonth(param) {
	var pattern = /^\d+$/;
	if ( param.length != 6 ) {
		return false;
	}
	if(!pattern.test(param)) {
		return false;
	}
	sYear = param.substring( 0, 4 );
	sMonth = param.substring( 4, 6 );
	if ( ( eval( sMonth ) < 1 ) || ( eval( sMonth )  > 12 ) ) {
		return false;
	}
	return true;
}

/**
* 检查时间间隔是否在规定间隔之内
* @param {String} 开始日期
* @param {String} 结束日期
* @param {Integer} 间隔，单位为天
* @return {bool} 是否在规定间隔之内
*/
function dateInterval(startDate,endDate,interval) {
	var date1 = new Date(eval(startDate.substring(0,4)),eval(startDate.substring(4,6))-1,eval(startDate.substring(6,8)));
	var date2 = new Date(eval(endDate.substring(0,4)),eval(endDate.substring(4,6))-1,eval(endDate.substring(6,8)));
	if ( ( date2 - date1 ) / 86400000 > eval(interval) - 1 )
		return false;
	return true;
}

/**
* 检查字符串是否为合法email地址
* @param {String} 字符串
* @return {bool} 是否为合法email地址
*/
function isEmail(aEmail) {
	var bValidate = RegExp(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/).test(aEmail);
	
	if ( bValidate )
	{
		return true;
	}
	else
		return false;
}

/**
* 选中指定HTML对象
* @param {Object} HTML对象
*/
function makeFocus(txtObject) {
	txtObject.focus();
	txtObject.select();
}

/**
* 控制text域的最大输入长度
* 用法：onblur="checkLength(this,要控制的长度);"
* @param {Object} text对象
* @param {Integer} 要控制的长度
*/
function checkLength(v,l){
	var s = v.value;
	var len = 0;
	
	for(i=0;i<s.length;i++)
	{
		var c = s.substr(i,1);
		var ts = escape(c);
		if(ts.substring(0,2) == "%u") 
		{
			len+=2;
		} else {
			len+=1;
		}
	}
	if(l>=len)
	{
		return;
	}
	else
	{
		var aaa=Math.floor(l/2);
		alert("该输入项插入值过长！最多"+l+"个字符或"+aaa+"个汉字。");
		v.value = "";
		v.focus();
	}
}

/**
* 控制textarea域的最大输入长度
* 用法：onblur="checkAreaLength(this,要控制的长度);"
* @param {Object} textarea对象
* @param {Integer} 要控制的长度
*/
function checkAreaLength(v,l){
	var s= v.value;
	var temlen=0;
	var len = 0;
	
	for(i=0;i<s.length;i++)
	{
		var c = s.substr(i,1);
		var ts = escape(c);
	
		if(ts.substring(0,2) == "%u")
		{
			len+=2;
			len+=temlen;
			temlen=0;
		}
		else if(ts.substring(0,3) == "%0D"){
			temlen+=1;
		}
		else if(ts.substring(0,3) == "%0A"){
			temlen+=1;
		}
		else if(ts.substring(0,3) == "%20"){
			temlen+=1;
		}
		else{
			len+=1;
			len+=temlen;
			temlen=0;
		}
	}
	
	if(len>l){
		var aaa=Math.floor(l/2);
		alert("该输入项插入值过长！最多"+l+"个字符或"+aaa+"个汉字。");  	
		v.value = "";
		v.focus();
		return false;
	}
	return true;
}

/**
* 通用打印
* @param {String} 打印标题
* @param {String} 要打印的Div段的ID
* @param {Integer} 打印窗体的宽
* @param {Integer} 打印窗体的高
*/
function commonprint(title,divId,w,h){
	var sarg=new Array();
	var sdata=document.all.item(divId);
	sarg[0]=title;
	sarg[1]=sdata.outerHTML;
	window.showModalDialog("print.jsp",sarg,"dialogWidth:"+w+"px;dialogHeight:"+h+"px;center:yes;help:no;status:no;resizable:yes");
	return;
}

/**
* 对金额进行转换，将金额转换为以元为单位，小数点后有两位
* 例：输入域1234，转换后隐含域为1234.00
* @param {String} 表单名称
* @param {String} 要转换的金额输入框名称
* @param {String} 金额隐藏域名称
*/
function convertToMoney(formName,txtmoney,hidmoney){
    var tonumber;
    var re = /,/g;
    var txt_money = eval("document."+formName+"."+txtmoney);
    var hid_money = eval("document."+formName+"."+hidmoney);
    if (txt_money.value != ""){
    	var temp = trim(txt_money.value);
    	if (temp == ""){
    		alert("请输入正确的金额!");
    		txt_money.value="";
				hid_money.value="";
				txt_money.focus();
    		return;
    	}
    }
    tonumber = txt_money.value.replace(re,"");

    txt_money.value = "";
    hid_money.value = "";
   if (tonumber !="" && tonumber!=null){
   	rep = / /g;
		var amt = tonumber.replace(rep,"");
		
		for(var j = 0; j < amt.length; j++){
			if(isNaN(parseInt(amt.charAt(j),10)) && amt.charAt(j)!="," && amt.charAt(j)!=".") {
				alert("请输入正确的金额!");
				txt_money.value="";
				hid_money.value="";
				txt_money.focus();
				return;
			}
		}
		if(amt.indexOf(".")!=amt.lastIndexOf(".")){
			alert("请输入正确的金额!");
			txt_money.focus();
			return;
		}
	
		re = /,/g;
		var amt1 = amt.replace(re,"");

		var amt2=parseFloat(amt1);		
		if(amt2<=0){
			alert("输入的金额小于或等于零,请重新输入!");
			txt_money.focus();
			return;
		}else{		//大于0的正数;
			if(amt1.indexOf(".")!=-1){				
				var str = amt1.substr(amt1.indexOf(".")+1);				
				if(str.length>2){
					alert("输入的金额小数点后只能保留两位,请重新输入!");
					txt_money.focus();
					return;
				}else if(str.length==1){
					amt1=amt1 + "0";
				}else if(str.length<1){
					amt1=amt1 + "00";
				}
			}else{
				amt1=amt1 + ".00";
			}
			if(amt1.charAt(0)=='0' && amt1.indexOf(".")!=1){
			alert("请输入正确的金额!");
			txt_money.focus();
			return;
			}
			hid_money.value=amt1;
			var temp=amt1.substring(0,amt1.indexOf("."));
			if (temp.length > 10){
			    alert("输入的金额太大，请重新输入!");
			    txt_money.focus();
			    return;
			}
			txt_money.value=comma(temp) + amt1.substring(amt1.indexOf("."));
			return;							
		}
    }
}

/**
* 对金额进行转换，将金额转换为以分为单位
* 例：输入域1234，转换后隐含域为123400
* @param {String} 表单名称
* @param {String} 要转换的金额输入框名称
* @param {String} 金额隐藏域名称
*/
function convertToMoney2(formName,txtmoney,hidmoney){
    var tonumber;
    var re = /,/g;
    var txt_money = eval("document."+formName+"."+txtmoney+";");
    var hid_money = eval("document."+formName+"."+hidmoney+";");
    tonumber = txt_money.value.replace(re,"");

    txt_money.value = "";
    if (tonumber !="" && tonumber!=null){
   	rep = / /g;
		var amt = tonumber.replace(rep,"");
		
		for(var j = 0; j < amt.length; j++){
			if(isNaN(parseInt(amt.charAt(j),10)) && amt.charAt(j)!="," && amt.charAt(j)!=".") {
				alert("请输入正确的金额!");
				txt_money.value="";
				txt_money.focus();
				return;
			}
		}
		if(amt.indexOf(".")!=amt.lastIndexOf(".")){
			alert("请输入正确的金额!");
			txt_money.focus();
			return;
		}
	
		re = /,/g;
		var amt1 = amt.replace(re,"");

		var amt2=parseFloat(amt1);		
		if(amt2<0){
			alert("输入的金额小于零,请重新输入!");
			txt_money.focus();
			return;
		}else{		//大于0的正数;
			if(amt1.indexOf(".")!=-1){				
				var str = amt1.substr(amt1.indexOf(".")+1);				
				if(str.length>2){
					alert("输入的金额小数点后只能保留两位,请重新输入!");
					txt_money.focus();
					return;
				}else if(str.length==1){
					amt1=amt1 + "0";
				}else if(str.length<1){
					amt1=amt1 + "00";
				}
			}else{
				amt1=amt1 + ".00";
			}
			if(amt1.charAt(0)=='0' && amt1.indexOf(".")!=1){
			alert("请输入正确的金额!");
			txt_money.focus();
			return;
			}
			hid_money.value=amt1.substring(0,amt1.indexOf(".")) + amt1.substr(amt1.indexOf(".")+1);
			var temp=amt1.substring(0,amt1.indexOf("."));
			if (hid_money.value.length > 18){
			    alert("金额太大");
			    txt_money.focus();
			    return;
			}
			txt_money.value=comma(temp) + amt1.substring(amt1.indexOf("."));
			return;							
		}
    }
}

/**
* 表现形式增加逗号，只对整数部分做处理，由convertToMoney，convertToMoney2调用。
* @param {Integer} 需转换数值	
* @return {String} 转换后的字符串
*/
function comma(number) {
	number = '' + number;
	if (number.length > 3) {
		var mod = number.length % 3;
		var output = (mod > 0 ? (number.substring(0,mod)) : '');
		for (i=0 ; i < Math.floor(number.length / 3); i++) {
			if ((mod == 0) && (i == 0))
				output += number.substring(mod+ 3 * i, mod + 3 * i + 3);
			else
				output += ',' + number.substring(mod + 3 * i, mod + 3 * i + 3);
		}
		return (output);
	}
	else return number;
}


/**
* 生成Excel文件并保存
* @param {String} 要生成的数据所在的表单名称
* @param {String} 要生成的数据所在的table名称	
*/
function exportexcel(formName,tableName){
      	var oXL = new ActiveXObject("Excel.Application");
      	oXL.Visible = true;
      	var oWB = oXL.Workbooks.Add();
      	var oSheet = oWB.ActiveSheet;
      
      	var div1=document.all.item(formName);
      	var table1=div1.all.item(tableName);
      	var table=new Array();

	var retArr = getTableRowsCols(table1);
      	var rlen=retArr[0];
      	var clen=retArr[1];
	
      	var flagTable=new Array(rlen);
      	for(var i=0;i<rlen;i++){
      		flagTable[i]=new Array(clen);
      		for(var j=0;j<clen;j++){
      			flagTable[i][j]=0;
      		}
      	}

	var rowBegin=1, colBegin=1;		//导出的表格的起始位置
	
      	var c1=oSheet.Cells(rowBegin,colBegin);
      	var c2=oSheet.Cells(rlen+rowBegin-1,clen+colBegin-1);
      	oSheet.Range(c1,c2).VerticalAlignment = -4108;
      	oSheet.Range(c1,c2).HorizontalAlignment =  -4108;
      	
  	exportTable(oSheet, flagTable, table1, rowBegin, colBegin);    	
    	      	
       	oSheet.Range(oSheet.Cells(rowBegin,colBegin),oSheet.Cells(rlen+rowBegin-1,clen+colBegin-1)).EntireColumn.AutoFit();
      	oXL.UserControl = true;
      	oXL.Quit();
}

/**
* 返回当前表格的行数和列数
* 被exportexcel调用
* @param {Object} 表格对象
* @param {Array} 数组retArr, retArr[0], 行数；retArr[1], 列数	
*/
function getTableRowsCols(objTable)
{
	var rowCnt=0, colCnt=0;
	for (var i=0; i<objTable.rows.length; i++) {
		var row = objTable.rows[i];	//当前行
		var rowRows=1, rowCols=0;	//当前行的初始行数和列数
		for (var j=0; j<row.cells.length; j++) {
			var cell = row.cells[j];	//当前单元格
			if (cell.firstChild !=null && cell.firstChild.tagName == "TABLE") {
				var inRetArr = getTableRowsCols(cell.firstChild);
				rowCols +=inRetArr [1];
				rowRows = rowRows<inRetArr[0] ? inRetArr[0] : rowRows;
			} else {	//不是表格
				rowCols += parseInt(cell.colSpan);
				rowRows = rowRows<parseInt(cell.rowSpan) ? parseInt(cell.rowSpan) : rowRows;
			}
		}
		colCnt = colCnt<rowCols ? rowCols : colCnt;
		rowCnt += rowRows;
	}
	var retArr = new Array();
	retArr[0] = rowCnt;
	retArr[1] = colCnt;
	
	return retArr;
}

/**
* 导出表格
* 被exportexcel调用
*/
function exportTable(oSheet, flagTable, objTable, rowBegin, colBegin){
	var flagRow=0, flagCol=0;		//跟踪当前的表示表中的相对位置
	for(var i=0; i<objTable.rows.length; i++) {
		var row = objTable.rows[i];	//当前行
		flagCol = 0;
		var subTableRows = 1;		//当前行若有表格，则记录其中最大的行数
		for (var j=0; j<row.cells.length; j++) {
			while (flagTable[rowBegin-1+flagRow][colBegin-1+flagCol]==1) {
				flagCol++;
			}
			var cell=row.cells[j];	//当前单元格
			if (cell.firstChild !=null && cell.firstChild.tagName == "TABLE") {
				var retArrRows = getTableRowsCols(cell.firstChild)[0];
				subTableRows = subTableRows<retArrRows ? retArrRows : subTableRows;
				exportTable(oSheet, flagTable, cell.firstChild, rowBegin+flagRow, colBegin+flagCol);
			} else {
				
				oSheet.Cells(rowBegin+flagRow, colBegin+flagCol).Font.Bold=1;
	               	 				
				var rs=parseInt(cell.rowSpan);
	               	 	var cs=parseInt(cell.colSpan);
	               	 	oSheet.Cells(rowBegin+flagRow, colBegin+flagCol).NumberFormatLocal="@";
	               	 	oSheet.Cells(rowBegin+flagRow, colBegin+flagCol).Value = cell.innerText;
	               	 	oSheet.Range(oSheet.Cells(rowBegin+flagRow, colBegin+flagCol),oSheet.Cells(rowBegin+flagRow+rs-1,colBegin+flagCol+cs-1)).MergeCells = 1;  
				
				//填写当前单元格在标志表格中的标志
				for(var k=0; k<rs; k++) {
					for(var l=0; l<cs; l++) {
						flagTable[rowBegin-1+flagRow+k][colBegin-1+flagCol+l]=1;
					}
				}
			}
				
			flagCol =flagCol + cs - 1;	//加速标志表格的列的移动，因为当前表格已经标识了cs个标志单元  			              		
		}
		flagRow += subTableRows;
	}	
}

/**
* 输入框发生焦点事件
* 在交易区使用
* @param {String} 发生焦点事件输入框样式的id
* @param {String} 发生焦点事件输入框错误样式的id
* @param {String} 发生焦点事件输入框样式的名称
*/
/**
function isfocus(name,errname,classname)
{
	document.getElementById(errname).style.display='none';
	var ts = document.getElementById(name);
	ts.style.display='';
	ts.className =classname;
}
*/
/**
* 输入框发生焦点事件
* @param {String} 失去焦点事件输入框样式的id
* @param {String} 失去焦点事件输入框样式的名称
*/
/**
function isblur(name,classname)
{
	var ts = document.getElementById(name);
	ts.className =classname;
}
*/

/**
* 在主页中点击图片滑块事件
* @param {String} 点击之前的图片
* @param {String} 点击之后的图片
* @param {String} 友情提示区table的id
* @param {String} 图片滑块的id
*/
function shousuo(imgleft,imgright,yqti,slide) {
	var yq = document.getElementById(yqti);
	var sl = document.getElementById(slide);
	if(yq.style.display=='none')
	{
		yq.style.display='inline';
		sl.alt='点击隐藏';
		sl.src=imgleft;
	}
	else
	{
		yq.style.display='none';
		sl.alt='点击显示';
		sl.src=imgright;
	}
}

/**
* 去除字符串中的","和"."，转换金额时使用
* @param {Integer}： 需转换数值
* @return {String} 转换后的字符串          	
*/
function uncomma(number){
	
	var pos = number.indexOf(",");
	while(pos!=-1){
		number = number.substring(0,pos)+number.substring(pos+1,number.length);
		pos = number.indexOf(",");
	}
	
	pos = number.indexOf(".");
	while(pos!=-1){
		number = number.substring(0,pos)+number.substring(pos+1,number.length);
		pos = number.indexOf(".");
	}
	
	var pos = number.indexOf("0");
	while(pos==0){
		number = number.substring(1,number.length);
		pos = number.indexOf("0");
	}
	return number;
}
/**
* 有错误发生错误样式显示事件
* 在交易区使用
* @param {String} 发生错误时错误样式的id
* @param {String} 正确的提示框样式的id
*/
function hinderr(errname,name)
{
	document.getElementById(errname).style.display='';
	document.getElementById(name).style.display='none';
}

/**
* 打开一个弹出窗口
* @param {String} 要打开页面的url
* @param {String} 新窗口名称
* @param {String} 新窗口属性，如果为null则使用默认属性
*/
function openWindow(url,name,property)
{
	var printtop=(screen.height-600)/2;
	var printleft=(screen.width-500)/2;
	var theproperty;
	
	if ( property == null )
		theproperty= 'width=600,height=400,center=yes,help=no,status=no,scrollbars=yes,resizable=yes,left='+printleft+',top="'+printtop+',screenx='+printleft+',screeny='+printleft;	
	else
		theproperty = property;
	
	window.open(url,name,theproperty);
}
/**
* 打开一个模式窗口
* @param {String} 要打开页面的url
* @param {String} 新窗口宽度
* @param {String} 新窗口高度
*/
	function stanDialog(url,width,height){
	    var par="help:no;scroll:yes;status:no;"+"dialogWidth:"+width+"px;dialogHeight:"+height+"px;";
	    window.showModalDialog(url,'',par);
	}
/**
* 密码校验
* @param {bool} 使用密码控件标志
* @param {String} 错误提示信息域
* @param {String} 提示说明信息域
* @param {String} 密码输入值
* @param {String} 交易提交form名称
* @param {String} 是否使用安全控件标志
*/
function checkPassword(errName,noteName,passValue,tranForm,isSecure)
{
		var isSecure = isSecure;

		if(isSecure == true){                       //使用密码控件
			var safeInput=document.safeInput;
			var safeCommit=document.safeCommit;
			if(safeInput.isValid())
			{
				hinderr(errName,noteName);
				safeInput.clear();
				return false; 
			}
			if(safeInput.allType()!="10000")
			{
				hinderr(errName,noteName);
				safeInput.clear();
				return false;
			}

			safeInput.commit("safeCommit");
			safeCommit.submit(tranForm);
		}
		else{									//未使用密码控件
				var tranForm = eval("document."+tranForm);
				if(!isInteger(passValue) || isEmpty(passValue) || passValue.length < 6){
					hinderr(errName,noteName);
					return false;
				}
			tranForm.submit();

		}
		return true;

}
//判断浏览器的类型是否是firefox
function isFirefox(){

       if(window.navigator.userAgent.indexOf("Firefox")>=1)

              return true;

       else

             return false;

}
