﻿//************************************
// 此文件用于编写仅存在于LianaV3 Demo中的javascript脚本
// 创建人：zhoujun
// 创建日期：2006-11-17
// 
//************************************
//打开模式对话框，根据用户选择返回不同值。
 //state=true,点击”确定删除“按钮，state=false点击“取消按钮”。
function dialogOpt(state){
	window.close();
	window.returnValue=state;
	
}

//删除单行，注：需要将每一行的“name”的值设为rowsGroup。k代表行号，第一行k就是1
function commandDelete(k,content){
	   var m=0;
		var flag=window.showModalDialog(content,"","dialogWidth:500px;dialogHeight:445px;center:yes;help:no;status:no;resizable:no");
		if(flag==true){
			var allCheck = document.all.tags("tr");
			for(var i=0; i<allCheck.length; i++){
			var tagBody = allCheck[i];
			
			if (tagBody.getAttribute("name")=="rowsGroup" )
			{	 m=m+1;  
			     if(m==k){
					tagBody.style.display="none";
				}
			}
		}
		}
}

function commandDeleteVarP(k,content,width,height,copyHref){
	   	var m=0;
		var par="help:no;scroll:yes;status:no;"+"dialogWidth:"+width+"px;dialogHeight:"+height+"px;";
		var flag=showModalDialog(content,"",par);
		
		if(flag==1){
			var allCheck = document.all.tags("tr");
			for(var i=0; i<allCheck.length; i++){
				var tagBody = allCheck[i];
			
				if (tagBody.getAttribute("title")==k)
				{	
			    		
						tagBody.style.display="none";
					
				}
			}
		}
		else if(flag==2){
			document.location=copyHref;
			parent.m2(10);
		}
		else{}
}
function commandDeleteVarPP(k,content,width,height,copyHref){
	   	var m=0;
		var par="help:no;scroll:yes;status:no;"+"dialogWidth:"+width+"px;dialogHeight:"+height+"px;";
		var flag=showModalDialog(content,"",par);
		
		if(flag==1){
			var allCheck = document.all.tags("tr");
			for(var i=0; i<allCheck.length; i++){
				var tagBody = allCheck[i];
			
				if (tagBody.getAttribute("title")==k)
				{	
			    		
						tagBody.style.display="none";
					
				}
			}
		}
		else if(flag==2){
			document.location=copyHref;
			parent.m2(4);
		}
		else{}
}
function commandDeleteVar(k,content,width,height){
	   	var m=0;
		var par="help:no;scroll:yes;status:no;"+"dialogWidth:"+width+"px;dialogHeight:"+height+"px;";
		var flag=showModalDialog(content,"",par);
		if(flag==true){
			var allCheck = document.all.tags("tr");
			for(var i=0; i<allCheck.length; i++){
				var tagBody = allCheck[i];
			
				if (tagBody.getAttribute("name")==k )
				{	// m=m+1;  
			    		// if(m==k){
						tagBody.style.display="none";
					//}
				}
			}
		}

}
//打开一个模式窗口，content的为url，mode=false时，窗体本身关闭；
function stanDialogClose(content,mode,width,height){
	var par="help:no;scroll:yes;status:no;"+"dialogWidth:"+width+"px;dialogHeight:"+height+"px;";
	showModalDialog(content,"",par);
	if(mode==false){
		window.close();
	}
}

  //用于一个页面中有两个账号选择（select）的情况
  //各个参数代表的含义如下:
	//x：select下option的序号，例如第一个option就是0。
	//y:代表select标签的id.z:代表一个字符串，可以动态指定，用于输出提示信息等。
	function selectCtrl(x,y,z)
	{
		
		switch(x)
		{
		case 0:
		case 3:
		case 6:
		case 9: alert(z);document.getElementById(y).options[1].selected=true;break;
		case 1: 
		case 2:  
		case 4:  
		case 5: 
		case 7:  
		case 8:  
		case 10: 
		case 11: break;
		default: alert(z);break;
		}
	}
	function selectViewLoad(y,k){
		if(k==1){
			document.getElementById(y).options[1].selected=true;
		}
		else{
			document.getElementById(y).options[2].selected=true;
		}
		var l=document.getElementById(y).options.length;
		var index=document.getElementById(y).options.selectedIndex;
		if(index==0){
			document.getElementById(y).options[1].selected=true;
			index=document.getElementById(y).options.selectedIndex;
		}
		var label=document.getElementById(y).options[index].text;
		var isIntChar = RegExp(/^[a-zA-Z0-9]+$/);
		var flag=0;
		var place=0;
		if(index==0)return;
		if(!isIntChar.test(label.substring(0,1))){
			var value=document.getElementById(y).options[index].text.substring(2);
			document.getElementById(y).options[l]=new Option(value);
			document.getElementById(y).options[l].value=document.getElementById(y).options[index].value
			document.getElementById(y).options[l].selected=true;
		}
	}//y:代表select标签的id
	//?body???onload??,?????select??onclick?????????,?selectView(id)
		
	function selectView(y){
		
		var l=document.getElementById(y).options.length;
		var index=document.getElementById(y).options.selectedIndex;
		if(index==0){
			document.getElementById(y).options[1].selected=true;
			index=document.getElementById(y).options.selectedIndex;
		}
		var label=document.getElementById(y).options[index].text;
		var isIntChar = RegExp(/^[a-zA-Z0-9]+$/);
		
		if(!isIntChar.test(label.substring(0,1))){
			var value=document.getElementById(y).options[index].text.substring(2);
			document.getElementById(y).options[l]=new Option(value);
			document.getElementById(y).options[l].value=document.getElementById(y).options[index].value
			document.getElementById(y).options[l].selected=true;
		}
		else{
			
				document.getElementById(y).options[l-1]=null;
				
		}
		//alert(document.getElementById(y).options[l-1].text);
		
		/*for(var i=0;i<l;i++){
		
			if(document.getElementById(y).options[i].label=="djsdhfff"  ){
				flag=1;
				place=i;
				break;
			}
		}
		if(flag==1 ){
			if(parent.selectContent!=""){
				(document.getElementById(y).options[place].text)=(parent.selectContent);
				(document.getElementById(y).options[place]).label="";
				(document.getElementById(y).options[place]).selected=false;
				parent.selectContent="";
			}
		}
		else
		{
			
			if(index==0){
				document.getElementById(y).options[index].selected=false;
				return;
			}
			parent.selectContent=document.getElementById(y).options[index].text;
			var value=(document.getElementById(y).options[index].text).substring(2);
					document.getElementById(y).options[index].text=(value);
					document.getElementById(y).options[index].label="djsdhfff";
					document.getElementById(y).options[index].selected=true;
	  }*/
	  
		
}
		

	
	//一个二级级联菜单的函数。例子页面在客户服务。联行号查询中，可自己查看
		function makeshi(x){
		var groups=document.transForm.province.options.length;//获得第一个下拉列表的长度
    var group=new Array(groups-1);
    for (i=1; i<groups; i++)
       { group[i]=new Array();}
file://以下是北京的二级选项
group[1][0]=new Option("---请选择城市---");
group[1][1]=new Option("东城区");
group[1][2]=new Option("西城区");
group[1][3]=new Option("海淀区");
group[1][4]=new Option("朝阳区");

file://以下是上海的二级选项
group[2][0]=new Option("---请选择城市---");
group[2][1]=new Option("徐汇区");
group[2][2]=new Option("宝山区");
group[2][3]=new Option("浦东区");
group[2][4]=new Option("闵行区");

file://以下是山东的二级选项
group[3][0]=new Option("---请选择城市---");
group[3][1]=new Option("济南市");
group[3][2]=new Option("青岛市");
group[3][3]=new Option("聊城市");
group[3][4]=new Option("烟台市");

var temp=document.transForm.city

if(x==0){alert("您必须首先选择省份！");}
  else{
  	//document.transForm.city.disabled=false;
   for(m=temp.options.length-1;m>2;m--)
    //这个要看清楚,因为要重新填充下拉列表的话必须先清除里面原有的项,清除和增加当然是有区别的了,所以用递减
        temp.options[m]=null;//将该项设置为空,也就等于清除了
    for(j=0;j<group[x].length;j++){//这个循环是填充下拉列表
        temp.options[j]=new Option(group[x][j].text,group[x][j].value)
        //注意上面这据,列表的当前项等于 新项(数组对象的x,j项的文本为文本，)
    }
    temp.options[0].selected=true;//设置被选中的初始值

}
}

	function defaultShowHide(defaultShow){
		document.getElementById(defaultShow).style.display = "none";
	}
	