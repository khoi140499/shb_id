//var MONTH_NAMES=new Array('January','February','March','April','May','June','July','August','September','October','November','December','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
var MONTH_NAMES= new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
var DF_DDMMMYYYY = "dd-MMM-yyyy";
var DF_DDMMYYYY = "dd-MM-yyyy";
var DF_DDMMYY = "dd-MM-yy";
var DF_DDMMMYY = "dd-MMM-yy";
var DF_YYYYMMMDD = "yyyy-MMM-dd";
var DF_YYYYMMDD = "yyyy-MM-dd";
var DF_YYMMMDD = "yy-MMM-dd";
var DF_YYMMDD = "yy-MM-dd";
var DF_MMMDDYYYY = "MMM-dd-yyyy";
var DF_MMDDYYYY = "MM-dd-yyyy";
var DF_MMDDYY = "MM-dd-yy";
var DF_MMMDDYY = "MMM-dd-yy";
var DF_DDMMMYYYYHHMMSS = "dd-MMM-yyyy HH:mm:ss";
var DF_DDMMMYYYYHHMMSSA = "dd-MMM-yyyy hh:mm:ssa";
var DF_DDMMMYYYYHHMM = "dd-MMM-yyyy HH:mm";
var DW_dayTable = new Array();
DW_dayTable[0] = new Array(0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
DW_dayTable[1] = new Array(0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
var DW_cumDayTable = new Array();
DW_cumDayTable[0] = new Array(0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365);
DW_cumDayTable[1] = new Array(0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366);


function LZ(x) {return(x<0||x>9?"":"0")+x}

// ------------------------------------------------------------------
// isDate ( date_string, format_string )
// Returns true if date string matches format of format string and
// is a valid date. Else returns false.
// It is recommended that you trim whitespace around the value before
// passing it to this function, as whitespace is NOT ignored!
// ------------------------------------------------------------------
function isDate(val,format) {
	var date=getDateFromFormat(val,format);
	if (date==0) { return false; }
	return true;
	}

// -------------------------------------------------------------------
// compareDates(date1,date1format,date2,date2format)
//   Compare two date strings to see which is greater.
//   Returns:
//   1 if date1 is greater than date2
//   0 if date2 is greater than date1 of if they are the same
//  -1 if either of the dates is in an invalid format
// -------------------------------------------------------------------
function compareDates(date1,dateformat1,date2,dateformat2) {
	var d1=getDateFromFormat(date1,dateformat1);
	var d2=getDateFromFormat(date2,dateformat2);
	if (d1==0 || d2==0) {
		return -1;
		}
	else if (d1 > d2) {
		return 1;
		}
	return 0;
	}

// the date in string format like "2002-01-01"
function formatStringDate(aDate,aFormat) {
	if (aDate == "null" || aDate == "") {return "";}
	else{
	var tdate = new Date(aDate.substring(0,4),parseInt(aDate.substring(5,7),10)-1, aDate.substring(8,10));
	return formatDate(tdate, aFormat);
        }
}

// the date in string format like "2002-01-01 17:02:49.0"
function formatStringDatetime(aDate,aFormat) {
	if (aDate == "null" || aDate == "") {return "";}
	else{
	var tdate = new Date(aDate.substring(0,4),parseInt(aDate.substring(5,7),10)-1, aDate.substring(8,10), aDate.substring(11,13), aDate.substring(14,16), aDate.substring(17,19));
	return formatDate(tdate, aFormat);
        }
}


// the date in string format like "20020101"
function formatDecimalDate(aDate,aFormat) {
	if (isNaN(aDate)) {
		return "";
	}else if (parseInt(aDate,10) > 0){
         	var date = new Date(aDate.substring(0,4), parseInt(aDate.substring(4,6),10)-1, aDate.substring(6));
         	return formatDate(date, aFormat);
        }else {
        	return "";
        }
 }
 
// the date in string format like "20020101123030999" (yyyyMMddHHmmssnnn)
function formatLongDecimalDate(aDate,aFormat) 
{
	if (isNaN(aDate)) 
	{
		return "";
	}
	else if (parseInt(aDate,10) > 0)
	{
		var date = new Date(aDate.substring(0,4), parseInt(aDate.substring(4,6),10)-1, aDate.substring(6,8),aDate.substring(8,10),aDate.substring(10,12),aDate.substring(12,14));
		return formatDate(date, aFormat);
	}
	else 
	{
    	return "";
    }
} 

// ------------------------------------------------------------------
// formatDate (date_object, format)
// Returns a date in the output format specified.
// The format string uses the same abbreviations as in getDateFromFormat()
// ------------------------------------------------------------------
function formatDate(date,format) {
	format=format+"";
	var result="";
	var i_format=0;
	var c="";
	var token="";
	var y=date.getYear()+"";
	var M=date.getMonth()+1;
	var d=date.getDate();
	var H=date.getHours();
	var m=date.getMinutes();
	var s=date.getSeconds();
	var yyyy,yy,MMM,MM,dd,hh,h,mm,ss,ampm,HH,H,KK,K,kk,k;
	// Convert real date parts into formatted versions
	var value=new Object();
	if (y.length < 4) {y=""+(y-0+1900);}
	value["y"]=""+y;
	value["yyyy"]=y;
	value["yy"]=y.substring(2,4);
	value["M"]=M;
	value["MM"]=LZ(M);
	value["MMM"]=MONTH_NAMES[M-1];
	value["d"]=d;
	value["dd"]=LZ(d);
	value["H"]=H;
	value["HH"]=LZ(H);
	if (H==0){value["h"]=12;}
	else if (H>12){value["h"]=H-12;}
	else {value["h"]=H;}
	value["hh"]=LZ(value["h"]);
	if (H>11){value["K"]=H-12;} else {value["K"]=H;}
	value["k"]=H+1;
	value["KK"]=LZ(value["K"]);
	value["kk"]=LZ(value["k"]);
	if (H > 11) { value["a"]="PM"; }
	else { value["a"]="AM"; }
	value["m"]=m;
	value["mm"]=LZ(m);
	value["s"]=s;
	value["ss"]=LZ(s);
	while (i_format < format.length) {
		c=format.charAt(i_format);
		token="";
		while ((format.charAt(i_format)==c) && (i_format < format.length)) {
			token += format.charAt(i_format++);
			}
		if (value[token] != null) { result=result + value[token]; }
		else { result=result + token; }
		}
	return result;
	}

// ------------------------------------------------------------------
// Utility functions for parsing in getDateFromFormat()
// ------------------------------------------------------------------
function _isInteger(val) {
	var digits="1234567890";
	for (var i=0; i < val.length; i++) {
		if (digits.indexOf(val.charAt(i))==-1) { return false; }
		}
	return true;
	}
function _getInt(str,i,minlength,maxlength) {
	for (var x=maxlength; x>=minlength; x--) {
		var token=str.substring(i,i+x);
		if (token.length < minlength) { return null; }
		if (_isInteger(token)) { return token; }
		}
	return null;
	}

// ------------------------------------------------------------------
// getDateFromFormat( date_string , format_string )
//
// This function takes a date string and a format string. It matches
// If the date string matches the format string, it returns the
// getTime() of the date. If it does not match, it returns 0.
// ------------------------------------------------------------------
function getDateFromFormat(val,format) {
	val=val+"";
	format=format+"";
	var i_val=0;
	var i_format=0;
	var c="";
	var token="";
	var token2="";
	var x,y;
	var now=new Date();
	var year=now.getYear();
	var month=now.getMonth()+1;
	var date=now.getDate();
	var hh=now.getHours();
	var mm=now.getMinutes();
	var ss=now.getSeconds();
	var ampm="";

	while (i_format < format.length) {
		// Get next token from format string
		c=format.charAt(i_format);
		token="";
		while ((format.charAt(i_format)==c) && (i_format < format.length)) {
			token += format.charAt(i_format++);
			}
		// Extract contents of value based on format token
		if (token=="yyyy" || token=="yy" || token=="y") {
			if (token=="yyyy") { x=4;y=4; }
			if (token=="yy")   { x=2;y=2; }
			if (token=="y")    { x=2;y=4; }
			year=_getInt(val,i_val,x,y);
			if (year==null) { return 0; }
			i_val += year.length;
			if (year.length==2) {
				if (year > 70) { year=1900+(year-0); }
				else { year=2000+(year-0); }
				}
			}
		else if (token=="MMM"){
			month=0;
			for (var i=0; i<MONTH_NAMES.length; i++) {
				var month_name=MONTH_NAMES[i];
				if (val.substring(i_val,i_val+month_name.length).toLowerCase()==month_name.toLowerCase()) {
					month=i+1;
					if (month>12) { month -= 12; }
					i_val += month_name.length;
					break;
					}
				}
			if ((month < 1)||(month>12)){return 0;}
			}
		else if (token=="MM"||token=="M") {
			month=_getInt(val,i_val,token.length,2);
			if(month==null||(month<1)||(month>12)){return 0;}
			i_val+=month.length;}
		else if (token=="dd"||token=="d") {
			date=_getInt(val,i_val,token.length,2);
			if(date==null||(date<1)||(date>31)){return 0;}
			i_val+=date.length;}
		else if (token=="hh"||token=="h") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<1)||(hh>12)){return 0;}
			i_val+=hh.length;}
		else if (token=="HH"||token=="H") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<0)||(hh>23)){return 0;}
			i_val+=hh.length;}
		else if (token=="KK"||token=="K") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<0)||(hh>11)){return 0;}
			i_val+=hh.length;}
		else if (token=="kk"||token=="k") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<1)||(hh>24)){return 0;}
			i_val+=hh.length;hh--;}
		else if (token=="mm"||token=="m") {
			mm=_getInt(val,i_val,token.length,2);
			if(mm==null||(mm<0)||(mm>59)){return 0;}
			i_val+=mm.length;}
		else if (token=="ss"||token=="s") {
			ss=_getInt(val,i_val,token.length,2);
			if(ss==null||(ss<0)||(ss>59)){return 0;}
			i_val+=ss.length;}
		else if (token=="a") {
			if (val.substring(i_val,i_val+2).toLowerCase()=="am") {ampm="AM";}
			else if (val.substring(i_val,i_val+2).toLowerCase()=="pm") {ampm="PM";}
			else {return 0;}
			i_val+=2;}
		else {
			if (val.substring(i_val,i_val+token.length)!=token) {return 0;}
			else {i_val+=token.length;}
			}
		}
	// If there are any trailing characters left in the value, it doesn't match
	if (i_val != val.length) { return 0; }
	// Is date valid for month?
	if (month==2) {
		// Check for leap year
		if ( ( (year%4==0)&&(year%100 != 0) ) || (year%400==0) ) { // leap year
			if (date > 29){ return false; }
			}
		else { if (date > 28) { return false; } }
		}
	if ((month==4)||(month==6)||(month==9)||(month==11)) {
		if (date > 30) { return false; }
		}
	// Correct hours value
	if (hh<12 && ampm=="PM") { hh=hh-0+12; }
	else if (hh>11 && ampm=="AM") { hh-=12; }
	var newdate=new Date(year,month-1,date,hh,mm,ss);
	return newdate.getTime();
	}

// aDate should be like YYYYMMDD, aNum is number of how many days, aFreq should be D(day)/M(month)/W(week)
// return another date in format YYYYMMDD
function getDateDiff(aDate , aNum, aFreq){
    var orgDate = aDate + "";
    var orgYear = parseInt(orgDate.substring(0,4),10);
    var orgMon  = parseInt(orgDate.substring(4,6),10);
    var orgDay  = parseInt(orgDate.substring(6,8),10);
    var rltYear;
    var rltMon;
    var rltDay;
    var rltDate;
    var aleap;
    
    if (aFreq == 'M'){
        var addMon  = aNum % 12;
        rltYear = orgYear + parseInt((aNum / 12),10);
        rltDay  = orgDay;
        if ((orgMon + addMon) > 12 ){
           rltYear = rltYear + 1;
           rltMon  = orgMon + addMon - 12;
        }else{
           rltMon  = orgMon + addMon
        }

        aleap = DW_isLeap(rltYear) ? 1 : 0;
        if (rltDay > DW_dayTable[aleap][rltMon]){
            rltDay = DW_dayTable[aleap][rltMon]
        }

        rltDate = rltYear*10000 + rltMon*100 + rltDay + "";


    }

    if (aFreq == 'W'){
        aNum  = aNum * 7;
        aFreq = 'D';
    }

    if (aFreq == 'D'){
        rltDate = DW_RelativeDate(orgYear,orgMon,orgDay,aNum)
    }

    return rltDate;
}

function addMonth(aDate, numMons)
{
    var year = aDate.getYear();
    var month = aDate.getMonth();
    
    // 04Feb2005 Hu Bin
    if ((year+"").length < 4) 
    {
        year = year - 0 + 1900;
    }
    
    numMons = parseInt(numMons + "", 10);

    year += (month + numMons) / 12;

    month = (month + numMons) % 12;

    var newDate = new Date(year, month, aDate.getDate(), aDate.getHours(), aDate.getMinutes(), aDate.getSeconds());

    while (newDate.getMonth() != month)
    {
        newDate = new Date(newDate.getTime() - 3600 * 1000 * 24);
    }

    return newDate;
}

function DW_isLeap(year)
{
    return ((year%4 == 0 && year%100 != 0) || year%400 == 0);
}

function DW_RelativeDate(aYear, aMon, aDay, numDays)
{
    var year = aYear;
	var month;
	var day;
	var yearDays;
	var aleap;
	var rltYear;
	var rltMon;
	var rltDay;

  	year = aYear;

	day = numDays + DW_dayOfYear(year, aMon, aDay);
	
    yearDays = DW_daysInYear(year);
    
	while (day > yearDays)
	{
    	year++;
    	day -= yearDays;
	}
	
	//Add by Leo Zhi on 20Oct2005
	while (day < 0)
	{
	    year--;
	    day += DW_daysInYear(year);
	}
	//End add by Leo Zhi on 20Oct2005
	
	rltYear = year;

	aleap = DW_isLeap(year) ? 1 : 0;
	
	for (month=0; month< 12 && day >= DW_cumDayTable[aleap][month]; month++);

	rltMon = month;
	
	rltDay = DW_dayTable[aleap][month] - (DW_cumDayTable[aleap][month] - day);
	
	//Add by Leo Zhi on 20Oct2005
	if (rltDay == 0)
	{
	    rltMon--;
	    rltDay = DW_dayTable[aleap][rltMon];
	    if (rltMon == 0)
	    {
	        rltYear--;
	        rltMon = 12;
	        rltDay = DW_dayTable[aleap][rltMon];
	    }
	}
	//End add by Leo Zhi on 20Oct2005
    	
    return rltYear*10000 + rltMon*100 + rltDay + "";
}

function DW_dayOfYear(year, month, day)
{
	var i, aleap;

	aleap = DW_isLeap(year) ? 1 : 0;
	for (i = 1; i < month; i++)
		day += DW_dayTable[aleap][i];
	return day;                   // Offset from 0
}

function DW_daysInYear(year)
{
	return DW_isLeap(year) ? 366 : 365;
}

//[ John Cheung - 11June2007 - sync from HK version
//Convert DateString into Date Object
function dateStrToObj(dateString, format)
{
	if (format == null) format = DF_DDMMMYYYY;

	return new Date(getDateFromFormat(dateString, format));
}

//Convert Date Object into Date String
function dateObjToStr(dateObj, format)
{
	if (format == null) format = DF_DDMMMYYYY;
	
	return formatDate(dateObj, format);
}

function calDateDiff(from_date, to_date, format)
{ 
	if (format == null) format = DF_DDMMMYYYY;

	from_date = new Date(getDateFromFormat(from_date, format));
	to_date = new Date(getDateFromFormat(to_date, format));
  
	return Math.round((to_date - from_date) / 864e5);
}
/*
function calDateFieldDiff(from_date, to_date, format)
{ 
	if (format == null) format = DF_DDMMMYYYY;

	from_date = getFieldValue(from_date);
	if (from_date == null || from_date == "") return "";

	to_date = getFieldValue(to_date);
	if (to_date == null || to_date == "") return "";

	return calDateDiff(from_date, to_date, format);
}
*/
function getTodayDate(format) 
{
	if (format == null)
	{
		format = DF_DDMMYYYY;
	}
	return formatDate( new Date(), format);
}
/*
function getNow(format)
{
	if (format == null)
	{
		format = DF_HHMM;
	}
	return formatDate( new Date(), format);
}
*/
function getCurrentTime(format)
{
	if (format == null)
	{
		format = DF_HHMMSS;
	}
	return formatDate( new Date(), format);
}
// John Cheung - 11June2007 - sync from HK version]
