﻿/////////////////////////////////////////////////////////////////
//
// Copyright (C) 2006 e-Channels CORPORATION
// 
// ALL RIGHTS RESERVED BY e-Channels CORPORATION, THIS PROGRAM
// MUST BE USED SOLELY FOR THE PURPOSE FOR WHICH IT WAS  
// FURNISHED BY e-Channels CORPORATION, NO PART OF THIS PROGRAM
// MAY BE REPRODUCED OR DISCLOSED TO OTHERS, IN ANY FORM
// WITHOUT THE PRIOR WRITTEN PERMISSION OF e-Channels CORPORATION.
// USE OF COPYRIGHT NOTICE DOES NOT EVIDENCE PUBLICATION
// OF THE PROGRAM
// 
//			e-Channels CONFIDENTIAL AND PROPRIETARY
// 
/////////////////////////////////////////////////////////////////
/**
 * Revision: 1.2
 * Author: zhuding@echannels.com.cn
 * ------------------------------------------------------------
 * 交互丰富的表格
 * 1、支持排序，支持表头拖动
 * 2、排序规则：默认按照td中的value属性值排序，如果value不存在，
 * 则按照innerText排序，示例：
 * 		<td class="center" value="20060910">2006年9月10日</td>  
 * 3、如果在表头的属性中加入type="fixed"，则该列为固定列，不能排序，
 * 也不能被拖动，并且其它列的排序不影响该列，适用于序号列的情况
 * 4、支持中文排序
 * ============================================================
 */
 
 
 var RichTable=Class.create();
  
 RichTable.prototype={
 
     // 构造器
     initialize:function(tableid){
        this.tabley=null;
        this.cur_row=null;
        this.cur_col=null;
        this.cur_cell=null;
        this.sort_col=null;
        this.dragStart=false;
        this.dragColStart=null;
        this.dragColEnd=null;
        this.sort_col=null;
        this.show_col=false;
        this.hasreadref=false;
        this.act_bgc="#FDECEC";
        this.act_col_bgc="red";
        this.act_fc="black";
        this.cur_bgc="";
        this.cur_fc="black";
        this.charMode=1;
        this.charPYStr="啊阿埃挨哎唉哀皑癌蔼矮艾碍爱隘鞍氨安俺按暗岸胺案肮昂盎凹敖熬翱袄傲奥懊澳芭捌扒叭吧笆八疤巴拔跋靶把耙坝霸罢爸白柏百摆佰败拜稗斑班搬扳般颁板版扮拌伴瓣半办绊邦帮梆榜膀绑棒磅蚌镑傍谤苞胞包褒剥薄雹保堡饱宝抱报暴豹鲍爆杯碑悲卑北辈背贝钡倍狈备惫焙被奔苯本笨崩绷甭泵蹦迸逼鼻比鄙笔彼碧蓖蔽毕毙毖币庇痹闭敝弊必辟壁臂避陛鞭边编贬扁便变卞辨辩辫遍标彪膘表鳖憋别瘪彬斌濒滨宾摈兵冰柄丙秉饼炳病并玻菠播拨钵波博勃搏铂箔伯帛舶脖膊渤泊驳捕卜哺补埠不布步簿部怖擦猜裁材才财睬踩采彩菜蔡餐参蚕残惭惨灿苍舱仓沧藏操糙槽曹草厕策侧册测层蹭插叉茬茶查碴搽察岔差诧拆柴豺搀掺蝉馋谗缠铲产阐颤昌猖场尝常长偿肠厂敞畅唱倡超抄钞朝嘲潮巢吵炒车扯撤掣彻澈郴臣辰尘晨忱沉陈趁衬撑称城橙成呈乘程惩澄诚承逞骋秤吃痴持匙池迟弛驰耻齿侈尺赤翅斥炽充冲虫崇宠抽酬畴踌稠愁筹仇绸瞅丑臭初出橱厨躇锄雏滁除楚础储矗搐触处揣川穿椽传船喘串疮窗幢床闯创吹炊捶锤垂春椿醇唇淳纯蠢戳绰疵茨磁雌辞慈瓷词此刺赐次聪葱囱匆从丛凑粗醋簇促蹿篡窜摧崔催脆瘁粹淬翠村存寸磋撮搓措挫错搭达答瘩打大呆歹傣戴带殆代贷袋待逮怠耽担丹单郸掸胆旦氮但惮淡诞弹蛋当挡党荡档刀捣蹈倒岛祷导到稻悼道盗德得的蹬灯登等瞪凳邓堤低滴迪敌笛狄涤翟嫡抵底地蒂第帝弟递缔颠掂滇碘点典靛垫电佃甸店惦奠淀殿碉叼雕凋刁掉吊钓调跌爹碟蝶迭谍叠丁盯叮钉顶鼎锭定订丢东冬董懂动栋侗恫冻洞兜抖斗陡豆逗痘都督毒犊独读堵睹赌杜镀肚度渡妒端短锻段断缎堆兑队对墩吨蹲敦顿囤钝盾遁掇哆多夺垛躲朵跺舵剁惰堕蛾峨鹅俄额讹娥恶厄扼遏鄂饿恩而儿耳尔饵洱二贰发罚筏伐乏阀法珐藩帆番翻樊矾钒繁凡烦反返范贩犯饭泛坊芳方肪房防妨仿访纺放菲非啡飞肥匪诽吠肺废沸费芬酚吩氛分纷坟焚汾粉奋份忿愤粪丰封枫蜂峰锋风疯烽逢冯缝讽奉凤佛否夫敷肤孵扶拂辐幅氟符伏俘服浮涪福袱弗甫抚辅俯釜斧脯腑府腐赴副覆赋复傅付阜父腹负富讣附妇缚咐噶嘎该改概钙盖溉干甘杆柑竿肝赶感秆敢赣冈刚钢缸肛纲岗港杠篙皋高膏羔糕搞镐稿告哥歌搁戈鸽胳疙割革葛格蛤阁隔铬个各给根跟耕更庚羹埂耿梗工攻功恭龚供躬公宫弓巩汞拱贡共钩勾沟苟狗垢构购够辜菇咕箍估沽孤姑鼓古蛊骨谷股故顾固雇刮瓜剐寡挂褂乖拐怪棺关官冠观管馆罐惯灌贯光广逛瑰规圭硅归龟闺轨鬼诡癸桂柜跪贵刽辊滚棍锅郭国果裹过哈骸孩海氦亥害骇酣憨邯韩含涵寒函喊罕翰撼扞旱憾悍焊汗汉夯杭航壕嚎豪毫郝好耗号浩呵喝荷菏核禾和何合盒貉阂河涸赫褐鹤贺嘿黑痕很狠恨哼亨横衡恒轰哄烘虹鸿洪宏弘红喉侯猴吼厚候后呼乎忽瑚壶葫胡蝴狐糊湖弧虎唬护互沪户花哗华猾滑画划化话槐徊怀淮坏欢环桓还缓换患唤痪豢焕涣宦幻荒慌黄磺蝗簧皇凰惶煌晃幌恍谎灰挥辉徽恢蛔回毁悔慧卉惠晦贿秽会烩汇讳诲绘荤昏婚魂浑混豁活伙火获或惑霍货祸击圾基机畸稽积箕肌饥迹激讥鸡姬绩缉吉极棘辑籍集及急疾汲即嫉级挤几脊己蓟技冀季伎祭剂悸济寄寂计记既忌际妓继纪嘉枷夹佳家加荚颊贾甲钾假稼价架驾嫁歼监坚尖笺间煎兼肩艰奸缄茧检柬碱硷拣捡简俭剪减荐槛鉴践贱见键箭件健舰剑饯渐溅涧建僵姜将浆江疆蒋桨奖讲匠酱降蕉椒礁焦胶交郊浇骄娇嚼搅铰矫侥脚狡角饺缴绞剿教酵轿较叫窖揭接皆秸街阶截劫节茎睛晶鲸京惊精粳经井警景颈静境敬镜径痉靖竟竞净炯窘揪究纠玖韭久灸九酒厩救旧臼舅咎就疚鞠拘狙疽居驹菊局咀矩举沮聚拒据巨具距踞锯俱句惧炬剧捐鹃娟倦眷卷绢撅攫抉掘倔爵桔杰捷睫竭洁结解姐戒藉芥界借介疥诫届巾筋斤金今津襟紧锦仅谨进靳晋禁近烬浸尽劲荆兢觉决诀绝均菌钧军君峻俊竣浚郡骏喀咖卡咯开揩楷凯慨刊堪勘坎砍看康慷糠扛抗亢炕考拷烤靠坷苛柯棵磕颗科壳咳可渴克刻客课肯啃垦恳坑吭空恐孔控抠口扣寇枯哭窟苦酷库裤夸垮挎跨胯块筷侩快宽款匡筐狂框矿眶旷况亏盔岿窥葵奎魁傀馈愧溃坤昆捆困括扩廓阔垃拉喇蜡腊辣啦莱来赖蓝婪栏拦篮阑兰澜谰揽览懒缆烂滥琅榔狼廊郎朗浪捞劳牢老佬姥酪烙涝勒乐雷镭蕾磊累儡垒擂肋类泪棱楞冷厘梨犁黎篱狸离漓理李里鲤礼莉荔吏栗丽厉励砾历利傈例俐痢立粒沥隶力璃哩俩联莲连镰廉怜涟帘敛脸链恋炼练粮凉梁粱良两辆量晾亮谅撩聊僚疗燎寥辽潦了撂镣廖料列裂烈劣猎琳林磷霖临邻鳞淋凛赁吝拎玲菱零龄铃伶羚凌灵陵岭领另令溜琉榴硫馏留刘瘤流柳六龙聋咙笼窿隆垄拢陇楼娄搂篓漏陋芦卢颅庐炉掳卤虏鲁麓碌露路赂鹿潞禄录陆戮驴吕铝侣旅履屡缕虑氯律率滤绿峦挛孪滦卵乱掠略抡轮伦仑沦纶论萝螺罗逻锣箩骡裸落洛骆络妈麻玛码蚂马骂嘛吗埋买麦卖迈脉瞒馒蛮满蔓曼慢漫谩芒茫盲氓忙莽猫茅锚毛矛铆卯茂冒帽貌贸么玫枚梅酶霉煤没眉媒镁每美昧寐妹媚门闷们萌蒙檬盟锰猛梦孟眯醚靡糜迷谜弥米秘觅泌蜜密幂棉眠绵冕免勉娩缅面苗描瞄藐秒渺庙妙蔑灭民抿皿敏悯闽明螟鸣铭名命谬摸摹蘑模膜磨摩魔抹末莫墨默沫漠寞陌谋牟某拇牡亩姆母墓暮幕募慕木目睦牧穆拿哪呐钠那娜纳氖乃奶耐奈南男难囊挠脑恼闹淖呢馁内嫩能妮霓倪泥尼拟你匿腻逆溺蔫拈年碾撵捻念娘酿鸟尿捏聂孽啮镊镍涅您柠狞凝宁拧泞牛扭钮纽脓浓农弄奴努怒女暖虐疟挪懦糯诺哦欧鸥殴藕呕偶沤啪趴爬帕怕琶拍排牌徘湃派攀潘盘磐盼畔判叛乓庞旁耪胖抛咆刨炮袍跑泡呸胚培裴赔陪配佩沛喷盆砰抨烹澎彭蓬棚硼篷膨朋鹏捧碰坯砒霹批披劈琵毗啤脾疲皮匹痞僻屁譬篇偏片骗飘漂瓢票撇瞥拼频贫品聘乒坪苹萍平凭瓶评屏坡泼颇婆破魄迫粕剖扑铺仆莆葡菩蒲埔朴圃普浦谱曝瀑期欺栖戚妻七凄漆柒沏其棋奇歧畦崎脐齐旗祈祁骑起岂乞企启契砌器气迄弃汽泣讫掐洽牵扦钎铅千迁签仟谦乾黔钱钳前潜遣浅谴堑嵌欠歉枪呛腔羌墙蔷强抢橇锹敲悄桥瞧乔侨巧鞘撬翘峭俏窍切茄且怯窃钦侵亲秦琴勤芹擒禽寝沁青轻氢倾卿清擎晴氰情顷请庆琼穷秋丘邱球求囚酋泅趋区蛆曲躯屈驱渠取娶龋趣去圈颧权醛泉全痊拳犬券劝缺炔瘸却鹊榷确雀裙群然燃冉染瓤壤攘嚷让饶扰绕惹热壬仁人忍韧任认刃妊纫扔仍日戎茸蓉荣融熔溶容绒冗揉柔肉茹蠕儒孺如辱乳汝入褥软阮蕊瑞锐闰润若弱撒洒萨腮鳃塞赛三叁伞散桑嗓丧搔骚扫嫂瑟色涩森僧莎砂杀刹沙纱傻啥煞筛晒珊苫杉山删煽衫闪陕擅赡膳善汕扇缮墒伤商赏晌上尚裳梢捎稍烧芍勺韶少哨邵绍奢赊蛇舌舍赦摄射慑涉社设砷申呻伸身深娠绅神沈审婶甚肾慎渗声生甥牲升绳省盛剩胜圣师失狮施湿诗尸虱十石拾时什食蚀实识史矢使屎驶始式示士世柿事拭誓逝势是嗜噬适仕侍释饰氏市恃室视试收手首守寿授售受瘦兽蔬枢梳殊抒输叔舒淑疏书赎孰熟薯暑曙署蜀黍鼠属术述树束戍竖墅庶数漱恕刷耍摔衰甩帅栓拴霜双爽谁水睡税吮瞬顺舜说硕朔烁斯撕嘶思私司丝死肆寺嗣四伺似饲巳松耸怂颂送宋讼诵搜艘擞嗽苏酥俗素速粟僳塑溯宿诉肃酸蒜算虽隋随绥髓碎岁穗遂隧祟孙损笋蓑梭唆缩琐索锁所塌他它她塔獭挞蹋踏胎苔抬台泰酞太态汰坍摊贪瘫滩坛檀痰潭谭谈坦毯袒碳探叹炭汤塘搪堂棠膛唐糖倘躺淌趟烫掏涛滔绦萄桃逃淘陶讨套特藤腾疼誊梯剔踢锑提题蹄啼体替嚏惕涕剃屉天添填田甜恬舔腆挑条迢眺跳贴铁帖厅听烃汀廷停亭庭挺艇通桐酮瞳同铜彤童桶捅筒统痛偷投头透凸秃突图徒途涂屠土吐兔湍团推颓腿蜕褪退吞屯臀拖托脱鸵陀驮驼椭妥拓唾挖哇蛙洼娃瓦袜歪外豌弯湾玩顽丸烷完碗挽晚皖惋宛婉万腕汪王亡枉网往旺望忘妄威巍微危韦违桅围唯惟为潍维苇萎委伟伪尾纬未蔚味畏胃喂魏位渭谓尉慰卫瘟温蚊文闻纹吻稳紊问嗡翁瓮挝蜗涡窝我斡卧握沃巫呜钨乌污诬屋无芜梧吾吴毋武五捂午舞伍侮坞戊雾晤物勿务悟误昔熙析西硒矽晰嘻吸锡牺稀息希悉膝夕惜熄烯溪汐犀檄袭席习媳喜铣洗系隙戏细瞎虾匣霞辖暇峡侠狭下厦夏吓掀锨先仙鲜纤咸贤衔舷闲涎弦嫌显险现献县腺馅羡宪陷限线相厢镶香箱襄湘乡翔祥详想响享项巷橡像向象萧硝霄削哮嚣销消宵淆晓小孝校肖啸笑效楔些歇蝎鞋协挟携邪斜胁谐写械卸蟹懈泄泻谢屑薪芯锌欣辛新忻心信衅星腥猩惺兴刑型形邢行醒幸杏性姓兄凶胸匈汹雄熊休修羞朽嗅锈秀袖绣墟戌需虚嘘须徐许蓄酗叙旭序畜恤絮婿绪续轩喧宣悬旋玄选癣眩绚靴薛学穴雪血勋熏循旬询寻驯巡殉汛训讯逊迅压押鸦鸭呀丫芽牙蚜崖衙涯雅哑亚讶焉咽阉烟淹盐严研蜒岩延言颜阎炎沿奄掩眼衍演艳堰燕厌砚雁唁彦焰宴谚验殃央鸯秧杨扬佯疡羊洋阳氧仰痒养样漾邀腰妖瑶摇尧遥窑谣姚咬舀药要耀椰噎耶爷野冶也页掖业叶曳腋夜液一壹医揖铱依伊衣颐夷遗移仪胰疑沂宜姨彝椅蚁倚已乙矣以艺抑易邑屹亿役臆逸肄疫亦裔意毅忆义益溢诣议谊译异翼翌绎茵荫因殷音阴姻吟银淫寅饮尹引隐印英樱婴鹰应缨莹萤营荧蝇迎赢盈影颖硬映哟拥佣臃痈庸雍踊蛹咏泳涌永恿勇用幽优悠忧尤由邮铀犹油游酉有友右佑釉诱又幼迂淤于盂榆虞愚舆余俞逾鱼愉渝渔隅予娱雨与屿禹宇语羽玉域芋郁吁遇喻峪御愈欲狱育誉浴寓裕预豫驭鸳渊冤元垣袁原援辕园员圆猿源缘远苑愿怨院曰约越跃钥岳粤月悦阅耘云郧匀陨允运蕴酝晕韵孕匝砸杂栽哉灾宰载再在咱攒暂赞赃脏葬遭糟凿藻枣早澡蚤躁噪造皂灶燥责择则泽贼怎增憎曾赠扎喳渣札轧铡闸眨栅榨咋乍炸诈摘斋宅窄债寨瞻毡詹粘沾盏斩辗崭展蘸栈占战站湛绽樟章彰漳张掌涨杖丈帐账仗胀瘴障招昭找沼赵照罩兆肇召遮折哲蛰辙者锗蔗这浙珍斟真甄砧臻贞针侦枕疹诊震振镇阵蒸挣睁征狰争怔整拯正政帧症郑证芝枝支吱蜘知肢脂汁之织职直植殖执值侄址指止趾只旨纸志挚掷至致置帜峙制智秩稚质炙痔滞治窒中盅忠钟衷终种肿重仲众舟周州洲诌粥轴肘帚咒皱宙昼骤珠株蛛朱猪诸诛逐竹烛煮拄瞩嘱主着柱助蛀贮铸筑住注祝驻抓爪拽专砖转撰赚篆桩庄装妆撞壮状椎锥追赘坠缀谆准捉拙卓桌琢茁酌啄着灼浊兹咨资姿滋淄孜紫仔籽滓子自渍字鬃棕踪宗综总纵邹走奏揍租足卒族祖诅阻组钻纂嘴醉最罪尊遵昨左佐柞做作坐座";
         this.tableid=tableid;
         this.tableobject=$(tableid);
        
        this.tableobject.onmouseover=this.overIt.bind(this);
        this.tableobject.onmouseout=this.outIt.bind(this);
        this.tableobject.onclick=this.clickIt.bind(this);
        //this.tableobject.ondblclick=this.dblclickIt.bind(this);
    

        this.arrowUp=document.createElement("img");
        this.arrowUp.src="../images/default/arrow_up.gif";
    
        this.arrowDown=document.createElement("img");
        this.arrowDown.src="../images/default/arrow_down.gif";
        
        this.drag=document.createElement("div");
        this.drag.innerHTML="";
        //this.drag.style.textAlign="center";
        this.drag.style.position="absolute";
        this.drag.style.cursor="hand";
        this.drag.style.border="1 solid white";
        this.drag.style.filter="alpha(opacity=50)";
        this.drag.style.display="none";
        this.drag.style.zIndex="999";
        this.drag.className = "result_head";
        document.body.insertBefore(this.drag);
        
        document.onmouseup=this.drag_end.bind(this);
        document.onselectstart=function(){return false;};
        document.onclick=this.documentclick.bind(this);
     },
     
     documentclick:function(){
         window.status = "";
        this.clear_color();
        this.cur_row  = null;
        this.cur_col  = null;
        this.cur_cell = null;
     },
     
    // 读取table的信息，并替换其中相关的函数
    readdef:function(){
        for(var i=0;i<this.tableobject.rows.length;i++){
            for(var j=0;j<this.tableobject.rows[i].cells.length;j++){
                with(this.tableobject.rows[i]){
                    cells[j].oBgc = cells[j].currentStyle.backgroundColor;
                    cells[j].oFc  = cells[j].currentStyle.color;
                    if(i==0){
                        cells[j].onmousedown=this.drag_start.bind(this);
                        cells[j].onmouseup=this.drag_end.bind(this);
                    }
                }
            }
        }
        this.hasreadref=true;
    },
    
    get_Element:function(the_ele,the_tag){
        the_tag = the_tag.toLowerCase();
        if(the_ele.tagName.toLowerCase()==the_tag)return the_ele;
        while(the_ele=the_ele.offsetParent){
            if(the_ele.tagName.toLowerCase()==the_tag)return the_ele;
        }
        return(null);
    },

    dragmousemove:function(){
        if(!this.hasreadref){
             this.readdef();
         }
        this.drag.style.display="";
        this.drag.style.top=event.y - this.drag.offsetHeight/2;
        this.drag.style.left=event.x - this.drag.offsetWidth/2;
        for(var i=0;i<this.tableobject.rows[0].cells.length;i++){
        	if(this.tableobject.rows[0].cells[i].type == "fixed") continue;
            with(this.tableobject.rows[0].cells[i]){
                if((event.y>this.tabley+parseInt(this.tableobject.currentStyle.marginBottom)+parseInt(document.body.currentStyle.marginTop) && event.y<this.tabley+offsetHeight+parseInt(this.tableobject.currentStyle.marginBottom)+parseInt(document.body.currentStyle.marginTop)) && (event.x>offsetLeft+parseInt(document.body.currentStyle.marginLeft) && event.x<offsetLeft+offsetWidth+parseInt(document.body.currentStyle.marginLeft))){
                    runtimeStyle.backgroundColor=this.act_col_bgc;
                    this.dragColEnd=cellIndex;
                }
                else{
                    runtimeStyle.backgroundColor="";
                }
            }
        }
        if(!(event.y>this.tabley+parseInt(this.tableobject.currentStyle.marginBottom)+parseInt(document.body.currentStyle.marginTop) && event.y<this.tabley+parseInt(this.tableobject.currentStyle.marginBottom)+this.tableobject.rows[0].offsetHeight+parseInt(document.body.currentStyle.marginTop))) this.dragColEnd=null;
    },

    drag_start:function(){
        if(!this.hasreadref){
             this.readdef();
         }
        var the_td    = this.get_Element(event.srcElement,"td");
        if(the_td==null) return;
        if(the_td.type == "fixed") return;
        this.dragStart=true;
        this.dragColStart=the_td.cellIndex;
        this.drag.style.width=the_td.offsetWidth;
        this.drag.style.height=the_td.offsetHeight;
        this.tabley=event.y-this.tableobject.rows[0].offsetHeight;
        document.onmousemove=this.dragmousemove.bind(this);
        this.drag.innerHTML=the_td.innerHTML;
        this.drag.style.backgroundColor=the_td.oBgc;
        this.drag.style.color=the_td.oFc;
    },

    drag_end:function(){
        if(!this.hasreadref){
             this.readdef();
         }
        this.dragStart = false;
        this.drag.style.display="none";
        this.drag.innerHTML = "";
        this.drag.style.width = 0;
        this.drag.style.height = 0;
        for(var i=0;i<this.tableobject.rows[0].cells.length;i++){
            this.tableobject.rows[0].cells[i].runtimeStyle.backgroundColor="";
        }
        if(this.dragColStart!=null && this.dragColEnd!=null && this.dragColStart!=this.dragColEnd){
            this.change_col(this.tableobject,this.dragColStart,this.dragColEnd);
            if(this.dragColStart==this.sort_col)this.sort_col=this.dragColEnd;
            else if(this.dragColEnd==this.sort_col)this.sort_col=this.dragColStart;
            document.onclick();
        }
        this.dragColStart = null;
        this.dragColEnd = null;
        document.onmousemove=null;
    },
    
    clear_color:function(){
        if(!this.hasreadref){
             this.readdef();
         }
        the_table=this.tableobject;
        if(this.cur_col!=null){
            for(i=0;i<the_table.rows.length;i++){
                with(the_table.rows[i].cells[this.cur_col]){
                    style.backgroundColor=oBgc;
                    style.color=oFc;
                }
            }
        }
        if(this.cur_row!=null){
            for(i=0;i<the_table.rows[this.cur_row].cells.length;i++){
                with(the_table.rows[this.cur_row].cells[i]){
                    style.backgroundColor=oBgc;
                    style.color=oFc;
                }
            }
        }
        if(this.cur_cell!=null){
            this.cur_cell.children[0].contentEditable = false;
            with(this.cur_cell.children[0].runtimeStyle){
                borderLeft=borderTop="";
                borderRight=borderBottom="";
                backgroundColor="";
                paddingLeft="";
                textAlign="";
            }
        }
    },
    
    change_row:function(the_tab,line1,line2){
        the_tab.rows[line1].swapNode(the_tab.rows[line2])
    },

    change_col:function(the_tab,line1,line2){
        for(var i=0;i<the_tab.rows.length;i++){
            the_tab.rows[i].cells[line1].swapNode(the_tab.rows[i].cells[line2]);
        }
    },
    
    // 鼠标移过表格
    overIt:function(){
        if(this.dragStart)return;
        var the_obj = event.srcElement;
        var i = 0;
        if(the_obj.tagName.toLowerCase() != "table"){
            var the_td=this.get_Element(the_obj,"td");
            if(the_td==null) return;
            if(the_td.type  == "fixed") return;
            var the_tr=the_td.parentElement;
            var the_table=this.get_Element(the_td,"table");
            if(the_tr.rowIndex!=0){
                for(i=0;i<the_tr.cells.length;i++){
                    with(the_tr.cells[i]){
                        runtimeStyle.backgroundColor=this.act_bgc;
                        runtimeStyle.color=this.act_fc;                    
                    }
                }
            }
            else{
                /*for(i=1;i<the_table.rows.length;i++){
                    with(the_table.rows[i].cells(the_td.cellIndex)){
                        runtimeStyle.backgroundColor=this.act_bgc;
                        runtimeStyle.color=this.act_fc;
                    }
                }*/
                the_td.title="点击可排序";
                if(the_td.mode==undefined)the_td.mode = false;
                //the_td.style.cursor=the_td.mode?"n-resize":"s-resize";
            }
        }
    },
    
    // 鼠标移出表格
    outIt:function(){
        var the_obj = event.srcElement;
        var i=0;
        if(the_obj.tagName.toLowerCase() != "table"){
            var the_td=this.get_Element(the_obj,"td");
            if(the_td==null) return;
            if(the_td.type  == "fixed") return;
            var the_tr=the_td.parentElement;
            var the_table=this.get_Element(the_td,"table");
            if(the_tr.rowIndex!=0){
                for(i=0;i<the_tr.cells.length;i++){
                    with(the_tr.cells[i]){
                        runtimeStyle.backgroundColor='';
                        runtimeStyle.color='';                
                    }
                }
            }else{
            	the_td.title="";
            	/*
                var the_table=the_tr.parentElement.parentElement;
                for(i=0;i<the_table.rows.length;i++){
                    with(the_table.rows[i].cells(the_td.cellIndex)){
                        runtimeStyle.backgroundColor='';
                        runtimeStyle.color='';
                    }
                }*/
            }
        }
    },
    
    // 按拼音排序
    judge_CN:function(char1,char2,mode){
        var charSet=this.charPYStr;
        for(var n=0;n<(char1.length>char2.length?char1.length:char2.length);n++){
            if(char1.charAt(n)!=char2.charAt(n)){
                if(mode) return(charSet.indexOf(char1.charAt(n))>charSet.indexOf(char2.charAt(n))?1:-1);
                else     return(charSet.indexOf(char1.charAt(n))<charSet.indexOf(char2.charAt(n))?1:-1);
                break;
            }
        }
        return(0);
    },

    SortArr:function(mode) {
        return function (arr1, arr2){
            var flag;
            var a,b;
            a = arr1[0];
            b = arr2[0];
            if(/^(\+|-)?\d+($|\.\d+$)/.test(a) && /^(\+|-)?\d+($|\.\d+$)/.test(b)){
                a=eval(a);
                b=eval(b);
                flag=mode?(a>b?1:(a<b?-1:0)):(a<b?1:(a>b?-1:0));
            }
            else{
                a=a.toString();
                b=b.toString();
                //$('log').innerHTML += "string(a):" + a + "	string(b):" + b + "<br>";
                if(a.charCodeAt(0)>=19968 && b.charCodeAt(0)>=19968){
                    flag = this.judge_CN(a,b,mode);
                }
                else{
                    flag=mode?(a>b?1:(a<b?-1:0)):(a<b?1:(a>b?-1:0));
                }
            }
            return flag;
        };
    },

    // 表格排序
    sort_tab:function(the_tab,col,mode){
        if(!this.hasreadref){
             this.readdef();
         }
        var tab_arr = new Array();
        var i;
        for(i=1;i<the_tab.rows.length;i++){
        	var elmtValue = the_tab.rows[i].cells[col].value||the_tab.rows[i].cells[col].innerText|| '';
        	//$('log').innerHTML += (elmtValue.toLowerCase() + '	' + the_tab.rows[i]+ '<br>');
            tab_arr.push(new Array(elmtValue.toLowerCase(),the_tab.rows[i]));
            
        }
        tab_arr.sort(this.SortArr(mode).bind(this));
        
        // 为了避免排序后表格颜色和固定列也跟着错位
        var j;
        var oldRowColors={};
        var oldFixedValue={};
        var oldFixedText={};
        
        for(var i=0;i<this.tableobject.rows.length;i++){
            for(var j=0;j<this.tableobject.rows[i].cells.length;j++){
                oldRowColors[i+""+j]=this.tableobject.rows[i].cells[j].oBgc;
                if(this.tableobject.rows[0].cells[j].type == "fixed"){
                	oldFixedValue[i+""+j]=this.tableobject.rows[i].cells[j].value||"";
                	oldFixedText[i+""+j]=this.tableobject.rows[i].cells[j].innerText||"";
                }
            }
        }
        
        
        for(i=0;i<tab_arr.length;i++){
            var nowRow=tab_arr[i][1];
            var index=i+1;
            for(j=0;j<nowRow.cells.length;j++){
                nowRow.cells[j].style.backgroundColor=oldRowColors[index+""+j];
                nowRow.cells[j].style.color=the_tab.rows[index].cells[j].oFc;
                nowRow.cells[j].oBgc=nowRow.cells[j].style.backgroundColor;
                nowRow.cells[j].oFc=nowRow.cells[j].style.color;
                if(this.tableobject.rows[0].cells[j].type == "fixed"){
                	nowRow.cells[j].value = oldFixedValue[index+""+j];
                	nowRow.cells[j].innerText = oldFixedText[index+""+j];
                }
            }
            the_tab.lastChild.appendChild(nowRow);
        }
    },
    
    // 鼠标单击表格
    clickIt:function(){
        if(!this.hasreadref){
             this.readdef();
         }
        event.cancelBubble=true;
        var the_obj=event.srcElement;
        var i = 0 ,j = 0;

        if(this.cur_cell!=null && this.cur_row!=0){
            this.cur_cell.children[0].contentEditable = false;
            with(this.cur_cell.children[0].runtimeStyle){
                borderLeft=borderTop="";
                borderRight=borderBottom="";
                backgroundColor="";
                paddingLeft="";
                textAlign="";
            }
        }
        if(the_obj.tagName.toLowerCase() != "table" && the_obj.tagName.toLowerCase() != "tbody" && the_obj.tagName.toLowerCase() != "thead" && the_obj.tagName.toLowerCase() != "tr"){
            var the_td    = this.get_Element(the_obj,"td");
            if(the_td==null) return;
            if(the_td.type  == "fixed") return;
            var the_tr=the_td.parentElement;
            var the_table=this.get_Element(the_td,"table");
            var i=0;
            this.clear_color();
            this.cur_row = the_tr.rowIndex;
            this.cur_col = the_td.cellIndex;
            if(this.cur_row!=0){
                /*for(i=0;i<the_tr.cells.length;i++){
                    with(the_tr.cells[i]){
                        style.backgroundColor=this.cur_bgc;
                        style.color=this.cur_fc;
                    }
                }*/
            }
            else{
                /*if(this.show_col){
                    for(i=1;i<the_table.rows.length;i++){
                        with(the_table.rows[i].cells[this.cur_col]){
                            style.backgroundColor=this.cur_bgc;
                            style.color=this.cur_fc;
                        }
                    }
                }*/
                
                the_td.mode = !the_td.mode;
                if(this.sort_col!=null){
                    with(the_table.rows[0].cells[this.sort_col])
                        removeChild(lastChild);
                }
                with(the_table.rows[0].cells[this.cur_col]){
                	appendChild(the_td.mode?this.arrowUp:this.arrowDown);
                }
                //$('log').innerHTML +=(this.cur_row + ' ' + this.cur_col + ' ' + the_td.mode + '<br>');
                this.sort_tab(the_table,this.cur_col,the_td.mode);
                this.sort_col=this.cur_col;
            }
        }
    },
    
    // 鼠标双击表格
    dblclickIt:function(){
        if(!this.hasreadref){
             this.readdef();
         }
        event.cancelBubble=true;
        if(this.cur_row!=0){
            var the_obj = event.srcElement;
            if(the_obj.tagName.toLowerCase() != "table" && the_obj.tagName.toLowerCase() != "tbody" && the_obj.tagName.toLowerCase() != "tr"){
                var the_td    = this.get_Element(the_obj,"td");
                if(the_td==null) return;
                this.cur_cell=the_td;
                if(the_td.children.length!=1)
                    the_td.innerHTML="<div>" + the_td.innerHTML + "</div>";
                else if(the_td.children.length==1 && the_td.children[0].tagName.toLowerCase()!="div")
                    the_td.innerHTML="<div>" + the_td.innerHTML + "</div>";
                this.cur_cell.children[0].contentEditable = true;
                with(this.cur_cell.children[0].runtimeStyle){
                    borderRight=borderBottom="buttonhighlight 1px solid";
                    borderLeft=borderTop="black 1px solid";
                    backgroundColor="#dddddd";
                    paddingLeft="5px";
                }
            }
        }
    }
     
 }