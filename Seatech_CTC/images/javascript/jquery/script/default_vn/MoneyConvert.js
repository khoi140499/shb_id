﻿
var flag = true;
var arr1=new Array(""," nghìn"," triệu"," tỷ")
var arr2=new Array("không","mười","hai mươi","ba mươi","bốn mươi","năm mươi","sau mươi","bảy mươi","tám mươi","chín mươi")
var arr3=new Array("không","một","hai","ba","bốn","năm","sáu","bảy","tám","chín");
var arr4=new Array("mười","mười một","mười hai","mười ba","mười bốn","mười năm","mười sáu","mười bảy","mười tám","mười chín");
var _point="phẩy";

function English(num){
	strRet=""
	if((num.length==3) && (num.substr(0,3)!="000")){
		if((num.substr(0,1)!="0")){
			strRet+=arr3[num.substr(0,1)]+" trăm"
			if(num.substr(1,2)!="00")strRet+=" và "
		}
		num=num.substring(1);
	}
	if((num.length==2)){
		if((num.substr(0,1)=="0")){
			num=num.substring(1)
		}
		else if((num.substr(0,1)=="1")){
			strRet+=arr4[num.substr(1,2)]
		}
		else{
			strRet+=arr2[num.substr(0,1)]
			if(num.substr(1,1)!="0")strRet+="-"
			num=num.substring(1)
		}
	}
	if((num.length==1) && (num.substr(0,1)!="0")){
		strRet+=arr3[num.substr(0,1)]
	}
	return strRet;
}

function toEnglishCash(sAmount){
	if(sAmount==null||sAmount.length==0||!checkNumber(sAmount))
		return "";
	var amtLen=sAmount.length;
	for(var i=amtLen-1;i>=0;i--){
		if(sAmount[i]==","){
			var amtTmp=sAmount.substring(0,i);
			if(sAmount.length>i)
				amtTmp+=sAmount.substring(i+1,sAmount.length);
			sAmount=amtTmp;
		}
	}
	var roundAmount="",decimalAmount="";
	var pointPos=sAmount.indexOf(".");
	if(pointPos==-1){
		roundAmount=sAmount;
	}else{
		roundAmount=sAmount.substring(0,pointPos);
		decimalAmount=sAmount.substring(pointPos+1,sAmount.length);
	}
	var strRet="";
	//Round
	var len=roundAmount.length;
	var cols=Math.ceil(len/3);
	var first=len-cols*3;
	for(var i=first,j=0;i<len;i+=3){
		++j;
		if(i>=0)
			num3=roundAmount.substring(i,i+3);
		else
			num3=roundAmount.substring(0,first+3);
		var strEng=English(num3);
		if(strEng!=""){
			if(strRet!="")	strRet+=",";
			strRet+=strEng+arr1[cols-j];
		}
	}
	//Decimal
	if(decimalAmount!=""){
		var dcRet ="";
		while(decimalAmount!=""&&decimalAmount[decimalAmount.length-1]=="0"){
			decimalAmount=decimalAmount.substring(0, decimalAmount.length-1);
		}
		for(i=0;i<decimalAmount.length;i++){
			var strEng="";
			if(decimalAmount[i]=="0"){
				strEng=arr3[0];
			}
			else{
				strEng=English(decimalAmount[i]);
			}
			
			if(strEng!=""){
				dcRet+=" "+strEng;
			}
		}
		if(dcRet!=""){
			if(strRet=="")
				strRet=arr3[0];
			strRet+=" "+_point;
			strRet+=dcRet;
		}
			
	}
	return strRet
}

function checkNumber(sAmount){
	var amtExp=/^([0-9,.])*$/;
	return amtExp.test(sAmount);
}

function toStdAmount(sAmount)
{
	
	var sComma = /\,/gi;
	var sResult = sAmount.replace(sComma,"");
	var iDotIndex = sResult.indexOf('.');
	var iLength = sResult.length;
	var toMatchNaNum = /\D/;
	if ((iDotIndex!=-1&&(iLength-iDotIndex>3||toMatchNaNum.test(sResult.slice(0,iDotIndex))))
		||toMatchNaNum.test(sResult.slice(iDotIndex+1,iLength))){

		flag=false;
		return '错误的金额!';
	}
	else
	{
		if (iDotIndex==-1)
			sResult = sResult+'.00';
		else if (iDotIndex==0)
		{
			if (iLength-iDotIndex==1) sResult='0'+sResult+'00';
			if (iLength-iDotIndex==2) sResult='0'+sResult+'0';
			if (iLength-iDotIndex==3) sResult='0'+sResult;
		}
		else
		{
			if (iLength-iDotIndex==2) sResult=sResult+'0';
			if (iLength-iDotIndex==1) sResult=sResult+'00';
		}
		var sTemp = "";
		sTemp = sResult.slice(0,iDotIndex);
			
		var iTemp = new Number(sTemp);
		sTemp = iTemp.toString();
		if (sTemp.length>16) {flag = false;return '太长的金额!';}
		iDotIndex = sResult.indexOf('.');

		sResult = sTemp+sResult.slice(iDotIndex);
		return sResult;
	}
}

function toChineseCash(sAmount)
{
	var value = toStdAmount(sAmount);
	if(!flag) {flag = true;return value;}
	var sCN_Num = new Array("零","壹","贰","叁","肆","伍","陆","柒","捌","玖");
	var unit = new Array('元', '万', '亿', '万');
	var subunit = new Array('拾', '佰', '仟');
	var sCNzero = '零';

	var result = "";

	var iDotIndex = value.indexOf('.');

	var sBeforeDot = value.slice(0, iDotIndex);
	var sAfterDot = value.slice(iDotIndex);

	var len = 0;
	//before dot
	len = sBeforeDot.length;
	var i = 0, j = 0, k = 0; //j is use to subunit,k is use to unit
	var oldC = '3';
	var cc = '0';
	result = unit[0] + result;

	var oldHasN = false;  
	var hasN = false;
	var allZero = true;
		
	for (i = 0; i < len; i++) {
		if (j == 0 && i != 0) {
			if (!hasN) 
			{
				if ((k % 2) == 0) result = result.slice(1);
			}
			else
			{
				if (oldC == '0') result = sCNzero + result;
			}
			result = unit[k] + result;
			//oldC = '3';
			oldHasN = hasN;
			hasN = false;
		}
		cc = sBeforeDot.charAt(len - i - 1);
		if (oldC == '0' && cc != oldC) 
		{
			if (hasN) result = sCNzero + result;
		}
		if (cc != '0')
		{
			if (j != 0)
				result = subunit[j - 1] + result;
			var dig = '0';
			dig = sCN_Num[cc];

			if (dig == '0')
				return false;
			hasN = true;
			allZero = false;
			result = dig + result;
		}
		oldC = cc;
		j++;
		if (j == 4)
		{
			k++;
			j = 0;
		}
	}
	if (allZero) {
		result = "零元";
	}
	else {
		var bb = 0;
		if (!hasN) {
			bb++;
			if (!oldHasN) {
				bb++;
			}
		}
		if (bb != 0)
			result = result.slice(bb);
		if (result.charAt(0) == '零')
			result = result.slice(1);
	}

	//after dot
	sAfterDot = sAfterDot.slice(1);
	len = sAfterDot.length;
	var corn = new Array('0','0');			
	var cornunit = new Array('角', '分');
	var n = 0; //j is use to subunit,k is use to unit
	var dig = '0';
	corn[0] = sAfterDot.charAt(0);
	if (len > 1)
		corn[1] = sAfterDot.charAt(1);
	else
		corn[1] = '0';
	if ((corn[0] ==  '0') && (corn[1] == '0')) 
		return result += '整';
	else
		if (allZero) result = "";
	for (i = 0; i < 2; i++)
	{
		var curchar = corn[i];
		dig = sCN_Num[curchar];

		if (i==0)
		{
			if(result!=""||curchar!='0')
				result += dig;
			if(curchar!='0')
			{
				result += cornunit[0];
			}
		}
		if (i==1&&curchar!='0') result = result+dig+cornunit[1];
	}

	return result;
}

