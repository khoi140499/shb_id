(function($){
	$.extend($.fn,{
		compare:function(settings){
			var defaults = {displayArea:'#displayArea',clickEvent:function(disArea){return false;}};
			$.extend(defaults,settings);
			$(this).mouseover(function(){
			 $(defaults['displayArea']).show();
			});
			
			$(this).mouseout(function(){
				$(defaults['displayArea']).hide();
			});
			
			$(this).click(function(){
				return defaults['clickEvent'].call(this,defaults['displayArea']);
			});
		}
	});	
})(jQuery);