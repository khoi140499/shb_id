(function($){
	$.fn.tabs = function(param){
		var defaultParam = {choiced:'0',iframe:'#mainIframe'};
		$.extend(defaultParam,param);
		var _href = $('#flowtabs').find('li:eq('+defaultParam['choiced']+') a').addClass('current').attr('href');
		$(defaultParam['iframe']).attr('src',_href);
		var _this = this;
		$(_this).find('li a').click(function(){
			$(_this).find('li a').removeClass('current');
			$(this).addClass('current'); 
			$(defaultParam['iframe']).attr('src',$(this).attr('href'));
			return false;
		});
		return _this;
	}
})(jQuery);