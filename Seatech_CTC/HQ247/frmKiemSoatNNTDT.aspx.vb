﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.HQ247
Imports Business.Common.mdlCommon
Imports ETAX_GDT.ProcessMSG
Imports System.Collections.Generic
Imports System.Xml
Partial Class NTDT_frmKiemSoatNNTDT
    Inherits System.Web.UI.Page
    'Toanva create
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Load_TrangThai()
            Load_grid_DSNNTDT()
            hdfMA_NV.Value = Session.Item("User").ToString
        End If
        'btnKS.Attributes.Add("onclick", "javascript:disableButton() ;" + Page.ClientScript.GetPostBackEventReference(btnKS, "").ToString())

        btnChuyenTra.Attributes.Add("onclick", "this.disabled=true;" + Page.ClientScript.GetPostBackEventReference(btnChuyenTra, "").ToString())
    End Sub
    'Private Sub Load_TrangThai()
    '    Try
    '        Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_YEUCAU WHERE TRANG_THAI IN ('04','09','10','11','12','13','14','15') ORDER BY TRANG_THAI "
    '        Dim dr As OracleClient.OracleDataReader
    '        dr = DataAccess.ExecuteDataReader(sql, CommandType.Text)
    '        drpTrangThai.DataSource = dr
    '        drpTrangThai.DataTextField = "MOTA"
    '        drpTrangThai.DataValueField = "TRANG_THAI"
    '        drpTrangThai.DataBind()
    '        dr.Close()
    '        drpTrangThai.Items.Insert(0, "Tất cả")

    '    Catch ex As Exception
    '        clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái NNTDT. Lỗi: " & ex.Message)
    '    End Try
    'End Sub
    Private Sub Load_TrangThai()
        Try
            Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_YEUCAU WHERE TRANG_THAI IN ('04','09','10','11','12','13','15') ORDER BY TRANG_THAI "
            Dim ds As DataSet
            ds = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            drpTrangThai.DataSource = ds
            drpTrangThai.DataTextField = "MOTA"
            drpTrangThai.DataValueField = "TRANG_THAI"
            drpTrangThai.DataBind()
            drpTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái NNTDT. Lỗi: " & ex.Message)
        End Try
    End Sub
    'Lấy dữ liệu TRẠNG THÁI cho gird người nộp thuế
    Private Sub Load_grid_DSNNTDT(Optional ByVal strTrangThai As String = "99")
        Dim ds As DataSet
        Dim WhereClause As String = ""
        Try
            ds = TCS_DM_NTDT_HQ.getMaker312(, , "" + Session.Item("UserName").ToString + "")
            grdDSCT.DataSource = ds.Tables(0)
            grdDSCT.DataBind()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách người nộp thuế. Lỗi: " & ex.Message)
        End Try
    End Sub
    Private Function Load_Data_DSNNTDT1(Optional ByVal strTrangThai As String = "99") As DataSet
        Dim ds As New DataSet
        Dim WhereClause As String = ""
        Try
            If drpTrangThai.SelectedIndex <> 0 Then
                WhereClause = " AND A.TRANGTHAI = '" & strTrangThai.ToString() & "'"
            End If
            ds = TCS_DM_NTDT_HQ.getMaker312(WhereClause, , "" + Session.Item("UserName").ToString + "")
            Return ds
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách người nộp thuế. Lỗi: " & ex.Message)
            Return ds
        End Try
    End Function
    'Lấy thông tin tài khoản ngân hàng của khách hàng
    Private Sub Load_Grid_DS_TKNH()
        Dim ds As New DataSet
        Try
            Dim sql = "SELECT ID,CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH FROM TCS_DM_NTDT_HQ_311 WHERE ID = " & hdfNNTDT_ID.Value & " AND MA_DV = '" & txtMA_DV.Text.Trim & "'"
            sql &= " UNION ALL "
            sql &= " SELECT ID,CUR_TAIKHOAN_TH,CUR_TEN_TAIKHOAN_TH FROM TCS_DM_NTDT_HQ WHERE ID = " & hdfNNTDT_ID.Value & " AND MA_DV = '" & txtMA_DV.Text.Trim & "' AND TRANGTHAI IN ('13','15') "
            ds = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            grdTKNH.DataSource = ds
            grdTKNH.DataBind()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách tài khoản ngân hàng của khách hàng. Lỗi: " & ex.Message)
        End Try
    End Sub
    Protected Sub btnSelectCT_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim btnSelectCT As LinkButton = CType(sender, LinkButton)
        Dim v_strRetArgument As String() = btnSelectCT.CommandArgument.ToString.Split(",")
        If (v_strRetArgument.Length > 0) Then
            Dim v_strNNTDT_ID As String = v_strRetArgument(0)
            hdfNNTDT_ID.Value = v_strNNTDT_ID
            Dim ds As DataSet = Business_HQ.HQ247.TCS_DM_NTDT_HQ.getMakerMSG213_AndHoso15_16InDay(v_strNNTDT_ID) 'getMakerMSG213
            If ds.Tables(0).Rows.Count > 0 Then
                txtSO_HS.Text = ds.Tables(0).Rows(0)("So_HS").ToString()
                txtLOAI_HS.Value = ds.Tables(0).Rows(0)("Loai_HS").ToString()
                txtTENLOAI_HS.Text = ds.Tables(0).Rows(0)("TenLoai_HS").ToString()
                txtMA_DV.Text = ds.Tables(0).Rows(0)("Ma_DV").ToString()
                txtTEN_DV.Text = ds.Tables(0).Rows(0)("Ten_DV").ToString()
                txtDIACHI.Text = ds.Tables(0).Rows(0)("DiaChi").ToString()
                txtSO_CMT.Text = ds.Tables(0).Rows(0)("So_CMT").ToString()
                txtNGAYSINH.Text = ds.Tables(0).Rows(0)("NgaySinh").ToString()
                txtHoTen.Text = ds.Tables(0).Rows(0)("Ho_Ten").ToString()
                txtNGUYENQUAN.Text = ds.Tables(0).Rows(0)("NguyenQuan").ToString()
                txtSO_DT.Text = ds.Tables(0).Rows(0)("So_DT").ToString()
                txtEMAIL.Text = ds.Tables(0).Rows(0)("Email").ToString()
                txtSERIALNUMBER.Text = ds.Tables(0).Rows(0)("SerialNumber").ToString()
                txtNOICAP.Text = ds.Tables(0).Rows(0)("NOICAP").ToString()
                txtNGAY_HL.Text = ds.Tables(0).Rows(0)("Ngay_HL").ToString()
                txtNGAY_HHL.Text = ds.Tables(0).Rows(0)("Ngay_HHL").ToString()
                txtMA_NH_TH.Text = ds.Tables(0).Rows(0)("Ma_NH_TH").ToString()
                txtTEN_NH_TH.Text = ds.Tables(0).Rows(0)("Ten_NH_TH").ToString()
                txtTAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("TaiKhoan_TH").ToString()
                txtTEN_TAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("Ten_TaiKhoan_TH").ToString()
                txtNGAY_NHAN311.Text = ds.Tables(0).Rows(0)("NGAY_NHAN311").ToString()
                txtNOIDUNG_XL.Text = ds.Tables(0).Rows(0)("LY_DO_HUY").ToString()
                txtLY_DO_CHUYEN_TRA.Text = ds.Tables(0).Rows(0)("LY_DO_CHUYEN_TRA").ToString()
                txtNGAY_KY_HDUQ.Text = ds.Tables(0).Rows(0)("NGAY_KY_HDUQ")
                txtNOIDUNG_XL.ReadOnly = True
                txtLY_DO_CHUYEN_TRA.ReadOnly = False
                Dim strTMP As String = "<data>" & ds.Tables(0).Rows(0)("THONGTINLIENHE").ToString() & "</data>"
                If strTMP.Length > 0 Then
                    Dim strThongtin As String = ""
                    Dim xmldoc As XmlDocument = New XmlDocument()
                    xmldoc.LoadXml(strTMP)
                    Dim x As Integer = 0
                    For Each vnode As XmlNode In xmldoc.SelectNodes("data/ThongTinLienHe")
                        x = x + 1
                        If strThongtin.Length > 0 Then
                            strThongtin &= "#"
                        End If
                        strThongtin &= x
                        For i As Integer = 0 To vnode.ChildNodes.Count - 1
                            strThongtin &= "|" & vnode.ChildNodes.Item(i).InnerText
                        Next


                    Next

                    txtThongTin.Value = strThongtin
                End If
                Load_Grid_DS_TKNH()
                Select Case ds.Tables(0).Rows(0)("TRANGTHAI").ToString()
                    Case "04" 'Đã duyệt yêu cầu
                        btnDuyet.Enabled = False
                        btnChuyenTra.Enabled = False
                    Case "09" 'Chờ kiểm soát
                        btnDuyet.Enabled = True
                        btnChuyenTra.Enabled = True
                        '
                        txtLY_DO_CHUYEN_TRA.ReadOnly = False
                    Case "11" 'Đã kiểm soát - chờ xác nhận từ TCHQ
                        btnDuyet.Text = "Duyệt lại"
                        btnDuyet.Enabled = True
                        btnChuyenTra.Enabled = False
                        '
                    Case Else
                        btnDuyet.Enabled = False
                        btnChuyenTra.Enabled = False
                End Select
            End If
        End If

    End Sub
    ''' <summary>
    ''' Duyệt Đăng ký của NNT
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnDuyet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuyet.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        Try
            Dim strID As String = hdfNNTDT_ID.Value.ToString
            Dim strSo_HS = txtSO_HS.Text.Trim()
            Dim strLoai_HS As String = txtLOAI_HS.Value  '1: Khai mới; 2: Khai sửa, 3: Khai hủy
            Dim strNoiDung_XL As String = txtLY_DO_CHUYEN_TRA.Text ' Lý do Hủy, kèm hướng dẫn xử lý
            Dim strTenDN As String = Session.Item("UserName").ToString.ToUpper ' Tên đăng nhập
            Dim strLoai312 As String = "1" '1: Duyệt, 2: Chuyển trả
            Dim strHQErrorCode As String = "0"
            Dim strHQErrorMSG As String = ""

            If strLoai_HS.Equals("3") Then
                Dim errID As Decimal = TCS_DM_NTDT_HQ.CheckNhoThu(strSo_HS)
                If errID = 0 Then
                    Dim warningMessage As String = "Hủy tài khoản đăng ký HQ247 , đăng ký nhờ thu sẽ bị hủy"
                    ClientScript.RegisterStartupScript(Me.GetType(), "jsProcessWarningMessage", "jsProcessWarningMessage('" + warningMessage + "');", True)
                Else
                    ClientScript.RegisterStartupScript(Me.GetType(), "jsProcessKS", "jsProcessKS('OK');", True)
                End If
            Else
                ClientScript.RegisterStartupScript(Me.GetType(), "jsProcessKS", "jsProcessKS('OK');", True)
            End If
            If strLoai_HS.Equals("2") Then
                Dim errID As Decimal = TCS_DM_NTDT_HQ.CheckNhoThuHS2(strSo_HS)
                Dim errNT As Decimal = TCS_DM_NTDT_HQ.CheckerMSG312_SuaTK_NH_Ngung(strID, strNoiDung_XL, strLoai312, strTenDN)
                If errID = 0 And errNT = 0 Then
                    Dim warningMessage As String = "Sửa tài khoản đăng ký HQ247 , đăng ký nhờ thu sẽ bị hủy"
                    ClientScript.RegisterStartupScript(Me.GetType(), "jsProcessWarningMessage", "jsProcessWarningMessage('" + warningMessage + "');", True)
                Else
                    ClientScript.RegisterStartupScript(Me.GetType(), "jsProcessKS", "jsProcessKS('OK');", True)
                End If
            Else
                ClientScript.RegisterStartupScript(Me.GetType(), "jsProcessKS", "jsProcessKS('OK');", True)
            End If


        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi: " & ex.Message)

        End Try
        'Load_grid_DSNNTDT()
    End Sub
    Protected Sub btnDuyetBK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDuyetBK.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        Try
            Dim strID As String = hdfNNTDT_ID.Value.ToString
            Dim strSo_HS = txtSO_HS.Text.Trim()
            Dim strLoai_HS As String = txtLOAI_HS.Value  '1: Khai mới; 2: Khai sửa, 3: Khai hủy
            Dim strNoiDung_XL As String = txtLY_DO_CHUYEN_TRA.Text ' Lý do Hủy, kèm hướng dẫn xử lý
            Dim strTenDN As String = Session.Item("UserName").ToString.ToUpper ' Tên đăng nhập
            Dim strLoai312 As String = "1" '1: Duyệt, 2: Chuyển trả
            Dim strHQErrorCode As String = "0"
            Dim strHQErrorMSG As String = ""
            'Update TRANGTHAI của Đăng ký trên bảng HQ_311 thành '11' (Đã kiểm soát - chờ xác nhận từ TCHQ)
            Dim errID As Decimal = TCS_DM_NTDT_HQ.CheckerMSG312(strID, strNoiDung_XL, strLoai312, strTenDN) 'Nhận mã lỗi trả về
            If errID = 0 Then
                'Build msg 312 và gửi đi TCHQ
                'Trường hợp trả lời msg 311 (Đăng ký mới tại đầu TCHQ)
                If Not strSo_HS.Equals("") Then
                    'Neu sua tai khoan tu dau ngan hang thi huy nho thu
                    Dim errNT As Decimal = TCS_DM_NTDT_HQ.CheckerMSG312_SuaTK_NH(strID, strNoiDung_XL, strLoai312, strTenDN)
                    If strLoai_HS.Equals("2") And errNT = 0 Then
                        Dim decResult As Decimal = Business_HQ.nhothu.daTCS_HQ247_314.fnc_UpdateHuy314_fromHQ(strSo_HS, "3", "", "", strTenDN)
                    End If

                    errID = CustomsServiceV3.ProcessMSG.sendMSG312("311", strNoiDung_XL, hdfNNTDT_ID.Value.ToString(), strTenDN, strHQErrorCode, strHQErrorMSG)
                Else
                    'Trường hợp đăng ký mới tại đầu NHTM
                    errID = CustomsServiceV3.ProcessMSG.sendMSG312("", strNoiDung_XL, hdfNNTDT_ID.Value.ToString(), strTenDN, strHQErrorCode, strHQErrorMSG)
                End If
                If errID = 0 Then
                    If strLoai_HS.Equals("3") Then
                        Dim decResult As Decimal = Business_HQ.nhothu.daTCS_HQ247_314.fnc_UpdateHuy314_fromHQ(strSo_HS, "3", "", "", strTenDN)
                    End If
                    clsCommon.ShowMessageBoxHQ247(Me, "Cập nhật trạng thái thành công !", HttpContext.Current.Request.Url.AbsoluteUri.ToString)
                Else
                    If Not strHQErrorCode Is Nothing Or Not strHQErrorMSG Is Nothing Then
                        strHQErrorMSG = strHQErrorMSG.Replace("'", "")
                        clsCommon.ShowMessageBox(Me, "Lỗi: " & strHQErrorCode & ": " & strHQErrorMSG & " !")
                    Else
                        clsCommon.ShowMessageBox(Me, "Lỗi: " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(errID.ToString()))
                    End If
                End If
            Else
                clsCommon.ShowMessageBox(Me, "Lỗi: " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(errID.ToString()))
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi: " & ex.Message)

        End Try
        'Load_grid_DSNNTDT()
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnChuyenTra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChuyenTra.Click
        If txtLY_DO_CHUYEN_TRA.Text.Trim.Length <= 0 Then
            clsCommon.ShowMessageBoxHQ247(Me, "Xin mời nhập Lý do chuyển trả !")
            txtLY_DO_CHUYEN_TRA.Focus()
            txtLY_DO_CHUYEN_TRA.BorderColor = Drawing.Color.Orange
            Exit Sub
        End If
        'Chuyển trạng thái HS sang Chuyển trả (10)
        Dim strTenDN As String = Session.Item("UserName").ToString.ToUpper ' Tên đăng nhập
        Dim errorID As Decimal = Business_HQ.HQ247.daTCS_DM_NTDT_HQ.TCS_HQ247_HS_TRANGTHAI_CHECKER312_CHUYENTRA(hdfNNTDT_ID.Value, txtLY_DO_CHUYEN_TRA.Text, strTenDN)
        'Dim errorID As Decimal = Business_HQ.HQ247.daTCS_DM_NTDT_HQ.UPDATE_TRANGTHAI("TCS_DM_NTDT_HQ_311", "10", txtSO_HS.Text.Trim(), hdfNNTDT_ID.Value.Trim(), strTenDN)
        If errorID = 0 Then
            clsCommon.ShowMessageBox(Me, "Chuyển trả thành công !")
        Else
            clsCommon.ShowMessageBox(Me, "Xảy ra lỗi: " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(errorID.ToString()))
        End If
        Load_grid_DSNNTDT()
    End Sub

    'Chuyển trang DS_NNTDT
    Protected Sub grdDSCT_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdDSCT.PageIndexChanging
        Dim v_ds As DataSet = Load_Data_DSNNTDT1(drpTrangThai.SelectedValue)
        grdDSCT.PageIndex = e.NewPageIndex
        grdDSCT.DataSource = v_ds
        grdDSCT.DataBind()
    End Sub
    'Chọn drop trạng thái NNTDT
    Protected Sub drpTrangThai_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpTrangThai.SelectedIndexChanged
        Dim v_ds As DataSet = Load_Data_DSNNTDT1(drpTrangThai.SelectedValue)
        grdDSCT.DataSource = v_ds
        grdDSCT.DataBind()
        grdDSCT.PageIndex = 0
    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Load_grid_DSNNTDT()
    End Sub
End Class
