﻿Imports System.Data
Imports Business.NTDT.NNTDT
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao

Partial Class HQ247_frmTraCuuNNTDT
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Load_TrangThai()
            Load_Loai_HS()

            DM_DiemThu()
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub
    Private Sub load_dataGrid()
        Dim ds As DataSet

        Dim WhereClause As String = ""
        Try
            If ConvertDateToNumber(txtTuNgay.Value) > ConvertDateToNumber(txtDenNgay.Value) And txtDenNgay.Value.ToString.Length > 0 Then
                clsCommon.ShowMessageBox(Me, "Điều kiện Từ ngày phải nhỏ hơn hoặc bằng Đến ngày")
                Exit Sub
            End If
            WhereClause += " AND a.TRANGTHAI IN ('04','10','11','12','13','15')" '" AND a.TRANGTHAI NOT IN ('01','02')"
            If txtMST.Enabled Then
                If txtSo_HS.Text <> "" Then
                    WhereClause += " AND upper(a.SO_HS) like upper('%" & txtSo_HS.Text.Trim & "%')"
                End If
                If drpLoai_HS.SelectedIndex <> 0 Then
                    WhereClause += " AND a.LOAI_HS = '" & drpLoai_HS.SelectedValue.ToString() & "'"
                End If
                If txtMST.Text <> "" Then
                    WhereClause += " AND upper(a.MA_DV) like upper('%" & txtMST.Text.Trim & "%')"
                End If
                If txtTuNgay.Value <> "" Then
                    WhereClause += " AND ( to_char(a.NGAY_NHAN311,'RRRRMMDD') >=" & ConvertDateToNumber(txtTuNgay.Value) & " OR to_char(a.NGAY_HT,'RRRRMMDD') >=" & ConvertDateToNumber(txtTuNgay.Value) & " )"
                End If
                If txtDenNgay.Value <> "" Then
                    WhereClause += " AND (to_char(a.NGAY_NHAN311,'RRRRMMDD') <= " & ConvertDateToNumber(txtDenNgay.Value) & " OR to_char(a.NGAY_HT,'RRRRMMDD') <=" & ConvertDateToNumber(txtDenNgay.Value) & " )"
                End If
                If txtTenNNT.Text <> "" Then
                    WhereClause += " AND upper(a.TEN_DV) like upper('%" + txtTenNNT.Text + "%')"
                End If
                If txtTKNH.Text <> "" Then
                    WhereClause += " AND upper(a.TAIKHOAN_TH) like upper('%" + txtTKNH.Text + "%')"
                End If
                If drpTrangThai.SelectedIndex <> 0 Then
                    WhereClause += " AND a.TRANGTHAI = '" + drpTrangThai.SelectedValue + "'"
                End If
            End If
            ds = Business_HQ.HQ247.TCS_DM_NTDT_HQ.getMSG311(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgrd.CurrentPageIndex = 0
                dtgrd.DataSource = ds.Tables(0)
                dtgrd.DataBind()

                'grdExport.DataSource = ds.Tables(0)
                'grdExport.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub
    Private Sub Load_TrangThai()
        Try
            Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_YEUCAU WHERE TRANG_THAI IN ('04','10','11','12','13','15')" 'NOT IN ('01','02')"
            Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            drpTrangThai.DataSource = ds.Tables(0)
            drpTrangThai.DataTextField = "MOTA"
            drpTrangThai.DataValueField = "TRANG_THAI"
            drpTrangThai.DataBind()
            'Dim dr As OracleClient.OracleDataReader
            'dr = DataAccess.ExecuteDataReader(sql, CommandType.Text)
            'drpTrangThai.DataSource = dr
            'drpTrangThai.DataTextField = "MOTA"
            'drpTrangThai.DataValueField = "TRANG_THAI"
            'drpTrangThai.DataBind()
            'dr.Close()
            drpTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái yêu cầu. Lỗi: " & ex.Message)
        End Try
    End Sub
    Private Sub Load_Loai_HS()
        Try
            Dim sql = "SELECT * FROM TCS_DM_LOAI_HS_NNT ORDER BY LOAI_HS"
            Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            drpLoai_HS.DataSource = ds.Tables(0)
            'Dim dr As OracleClient.OracleDataReader
            'dr = DataAccess.ExecuteDataReader(sql, CommandType.Text)
            'drpLoai_HS.DataSource = dr
            drpLoai_HS.DataTextField = "MOTA"
            drpLoai_HS.DataValueField = "LOAI_HS"
            drpLoai_HS.DataBind()
            'dr.Close()
            drpLoai_HS.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách loại hồ sơ người nộp thuế. Lỗi: " & ex.Message)
        End Try
    End Sub


    Private Sub DM_DiemThu()
        Dim cnDiemThu As New DataAccess
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        If strcheckHSC = "1" Then
            'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
            ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
            strSQL = "SELECT a.id, a.id ||'-'||a.name nam FROM tcs_dm_chinhanh a order by a.id "
        Else
            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  a.branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' or a.id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' order by a.id"
        End If
    End Sub

    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        load_dataGrid()
        dtgrd.CurrentPageIndex = 0
        dtgrd.DataBind()
    End Sub

    Protected Sub dtgrd_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgrd.PageIndexChanged
        load_dataGrid()
        dtgrd.CurrentPageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub
    Protected Sub btnDangKyMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDangKyMoi.Click
        'Response.Redirect("~/HQ247/frmHoanThienNNTDT.aspx")
        Response.Redirect("~/HQ247/frmDKMOINNTDT.aspx")
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
End Class
