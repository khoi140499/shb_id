﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmDuyetYeuCauNNTDT.aspx.vb" 
    Inherits="NTDT_frmDuyetYeuCauNNTDT" title="Duyệt yêu cầu đăng kí trích nợ tài khoản của NNT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .txtHoanThien{width:80%;}
        .container{width:100%;padding-bottom:50px;}
    </style>
    <script type="text/javascript" language="javascript">
//     function DisableButton() {
//                document.forms[0].submit();
//                window.setTimeout("disableButton('" + 
//                   window.event.srcElement.id + "')", 200);
//            }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle"> Duyệt yêu cầu đăng kí trích nợ tài khoản của NNT </p>
    <div class="container">
        <table width="100%">
            <tr>
                <td style="width:30%" valign="top">
                    <asp:GridView ID="grdDSCT" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        ShowHeader="True" ShowFooter="True" BorderColor="#989898" CssClass="grid_data"
                        PageSize="15" Width="100%">
                        <PagerStyle CssClass="cssPager" />
                        <AlternatingRowStyle CssClass="grid_item_alter" />
                        <HeaderStyle CssClass="grid_header" HorizontalAlign="Left"></HeaderStyle>
                        <RowStyle CssClass="grid_item" />
                        <Columns>
                            <asp:TemplateField HeaderText="TT">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                     <%#Eval("TRANGTHAI")%>
                                </ItemTemplate>
                                 <FooterTemplate>
                                    Tổng số:
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="MST" FooterText="Tổng số:">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <FooterStyle HorizontalAlign="Center" ></FooterStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnSelectCT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MA_DV") %>'
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID")%>'
                                        OnClick="btnSelectCT_Click" />
                                </ItemTemplate>
                                <FooterTemplate>
                                    
                                </FooterTemplate>
                            </asp:TemplateField>
                          <asp:TemplateField HeaderText="Số TK" FooterText="">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <FooterStyle HorizontalAlign="Center" ></FooterStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <%#Eval("TAIKHOAN_TH")%>
                                </ItemTemplate>
                                <FooterTemplate>
                                    
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText ="Loại hồ sơ">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <%#Eval("TENLOAI_HS")%>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <%#grdDSCT.Rows.Count.ToString%>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Left" BackColor="#E4E5D7"></PagerStyle>
                    </asp:GridView>
                    <p class="form_label">Trạng thái:<br />
                    <asp:DropDownList ID="drpTrangThai" runat="server" Width="97%" CssClass="inputflat" AutoPostBack="true" >                                                                 
                    </asp:DropDownList></p>
                    <p><asp:Button ID="btnRefresh" runat="server" Text="Làm mới" Font-Bold="true" CssClass="buttonField" /></p>
                </td>
                <td>
                    <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
                    <asp:HiddenField runat="server" ID="txtID"/>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Số hồ sơ<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtSO_HS" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="false" MaxLength="20"></asp:TextBox></td>
                        </tr>
                         <asp:HiddenField runat="server" ID="txtLOAI_HS" />
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Loại hồ sơ <span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtTenLoai_HS" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="20"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Mã số thuế<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtMA_DV" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="14"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Tên đơn vị XNK<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtTEN_DV" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Địa chỉ đơn vị XNK<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtDIACHI" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Tên NNT<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtHO_TEN" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Số chứng minh thư<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtSO_CMT" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="10"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Ngày sinh<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNGAYSINH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="10"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Nguyên quán<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNGUYENQUAN" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
                <td align="left" style="width:25%" class="form_label">Thông tin liên hệ </td>
                <td class="form_control" style="text-align:left">
                <asp:DataGrid ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="80%" BorderColor="Black" CssClass="grid_data" 
            >
            <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
            <HeaderStyle  CssClass="grid_header"></HeaderStyle>
            <ItemStyle CssClass="grid_item" />
            <Columns>
                <asp:BoundColumn DataField="STT" HeaderText="STT">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="So_DT" HeaderText="Số Điện thoại">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Email" HeaderText="Email">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                
            </Columns>
          
        </asp:DataGrid>
                </td>
            </tr>
                        <tr style="display:none;">
                            <td align="left" style="width:25%" class="form_label">Số điện thoại</td>
                            <td class="form_control"><asp:TextBox ID="txtSO_DT" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="15"></asp:TextBox></td>
                        </tr>
                         <tr style="display:none;">
                            <td align="left" style="width:25%" class="form_label"></td>
                            <td class="form_control"><asp:TextBox ID="txtSO_DT1" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="15"></asp:TextBox></td>
                        </tr>
                        <tr style="display:none;">
                            <td align="left" style="width:25%" class="form_label">Email</td>
                            <td class="form_control"><asp:TextBox ID="txtEMAIL" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="50"></asp:TextBox></td>
                        </tr>
                         <tr style="display:none;">
                            <td align="left" style="width:25%" class="form_label"></td>
                            <td class="form_control"><asp:TextBox ID="txtEMAIL1" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="50"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Số Serial của chứng thư số<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtSERIALNUMBER" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="40"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Đơn vị cấp chứng thư số<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNOICAP" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Ngày hiệu lực của chứng thư số<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNGAY_HL" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="19"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Ngày hết hiệu lực chứng thư số<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNGAY_HHL" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="19"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Mã ngân hàng<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtMA_NH_TH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="7"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Tên ngân hàng<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtTEN_NH_TH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Số tài khoản đăng ký<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtTAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="50"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Tên tài khoản đăng ký<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtTEN_TAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Ngày gửi<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNGAY_NHAN311" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="19"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Số tài khoản sử dụng NTDT</td>
                            <td class="form_control" style="text-align:left"><asp:TextBox ID="txtCUR_TAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="50" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Tên tài khoản sử dụng NTDT</td>
                            <td class="form_control" style="text-align:left"><asp:TextBox ID="txtCUR_TEN_TAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ReadOnly="true"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Nội dung hướng dẫn NNT<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNOIDUNG_XL" runat="server" CssClass="inputflat txtHoanThien"  MaxLength="4000" TextMode="MultiLine" Height="40"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Lý do hủy</td>
                            <td class="form_control"><asp:TextBox ID="txtLY_DO_HUY" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="5"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Lý do chuyển trả</td>
                            <td class="form_control"><asp:TextBox ID="txtLY_DO_CHUYEN_TRA" TextMode="MultiLine" Height="40" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="5"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;
                            <asp:HiddenField ID="hdfNNTDT_ID" runat="server" />
                            <asp:HiddenField ID="hdrTRANG_THAI" runat="server" />
                            <asp:HiddenField ID="hdfMA_NV" runat="server" />
                            <asp:HiddenField ID="hdfSUBJECT_CERT_NTHUE" runat="server" />
                            <asp:HiddenField ID="hdfMA_GDICH" runat="server" />
                            <asp:HiddenField ID="hdfUpdate_TT" runat="server" />
                            </td>
                        </tr>
                         <tr>
                        <td align="left" width='100%'colspan="2">
                            <b>
                                <asp:Literal ID="lblStatus" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnDuyetYeuCau" runat="server" CssClass="buttonField" Text="Duyệt yêu cầu" Enabled="false"/>&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnDuyetHuy" runat="server" CssClass="buttonField" Text="Duyệt hủy" Enabled="false"/>&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnChuyenTra" runat="server" CssClass="buttonField" Text="Chuyển trả" Enabled="false"/>&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnExit" runat="server" CssClass="buttonField" Text="Thoát" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        
    </div>
</asp:Content>

