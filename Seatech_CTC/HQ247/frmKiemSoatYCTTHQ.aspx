﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmKiemSoatYCTTHQ.aspx.vb" Inherits="HQ247_frmKiemSoatYCTTHQ" title="KS yêu cầu thanh toán HQ 247" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
        function deleteConfirm(pubid) {
            var result = confirm('Bạn có thực sự muốn xóa tài khoản ' + pubid + ' ?');
            if (result) {
                return true;
            }
            else {
                return false;
            }
        }

    
    </script>
    <style type="text/css">
         .txtHoanThien80{ max-width:400px; width:80%; }
        .txtHoanThien{ width:400px; }
        .txtHoanThien01{width:557px; }
        .container{width:100%;padding-bottom:50px;}
        .dataView{padding-bottom:10px}
        .number{ text-align:right; }
        .formx
        {
        	color: #E7E7FF;
background: #eaedf0;
padding-left: 1px;
color:Black;
        	}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle">Duyệt chứng từ</p>
    <div class="container">
    <table width="100%" border="0" cellpadding="2">
    <tr>
        <td valign="top" style="width:200px;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr align="left">
                                                            <td>
                                                                <asp:GridView ID="grdDSCT" runat="server" AutoGenerateColumns="False" 
                                                                    AllowPaging="True" ShowFooter="True" BorderColor="#000000" CssClass="grid_data" PageSize="15"
                                                                    Width="100%">
                                                                    <PagerStyle CssClass="cssPager" />
                                                                    <AlternatingRowStyle CssClass="grid_item_alter" />
                                                                    <HeaderStyle BackColor="#0072BC" CssClass="grid_header" HorizontalAlign="Left"></HeaderStyle>
                                                                    <RowStyle CssClass="grid_item" />
                                                                    <Columns>
                                                                   <asp:TemplateField HeaderText="Trạng thái" FooterText="Tổng số CT:" >
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblTrangthai" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TRANG_THAI") %>' />
                                                                        
                                                                               
                                                                            </ItemTemplate>
                                                                             
                                                                        </asp:TemplateField>
                                                                        
                                                                      
                                                                       
                                                                        <asp:TemplateField HeaderText="Số CT" FooterText="Tổng số CT:">
                                                                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                            <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnSelectCT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "HQ_SO_CTU") %>'
                                                                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID") %>'
                                                                                    OnClick="btnSelectCT_Click" />
                                                                            </ItemTemplate>
                                                                              <FooterTemplate>
                                                                                <%#grdDSCT.Rows.Count.ToString%>
                                                                            </FooterTemplate>
                                                                        </asp:TemplateField>
                                                                       
                                                                    </Columns>
                                                                    <PagerStyle HorizontalAlign="Left" BackColor="#E4E5D7"></PagerStyle>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                <asp:Button ID="cmdRefresh" runat="server" Text="[Làm mới]" AccessKey="K" CssClass="ButtonCommand"
                                                                    UseSubmitBehavior="false" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                Trạng thái chứng từ:
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%'>
                                                                <asp:DropDownList ID="ddlTrangThai" runat="server" CssClass="inputflat" Width="100%"
                                                                    AutoPostBack="true">
                                                                 
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" width='100%' class="nav">
                                                                <br />
                                                            </td>
                                                        </tr>
                                                      
                                                      
                                                        <tr class="img">
                                                            <td align="left" style="width: 100%" colspan="3">
                                                                <hr style="width: 50%" />
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td style="width: 100%" colspan="3">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tr style="display: none">
                                                                        <td style="width: 10%">
                                                                            
                                                                            <img src="../../images/icons/ChuaKS.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">
                                                                            Chưa Kiểm soát
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/ChuyenKS.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">
                                                                            Chờ Duyệt
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/DaKS.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">
                                                                            Đã Duyệt
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/KSLoi.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">
                                                                            Chuyển trả
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/Huy.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">
                                                                            Giao dịch hủy
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/HuyHS.gif" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">
                                                                            Giao dịch hủy - chờ duyệt
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/Huy_KS.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">
                                                                            Kiểm soát duyệt hủy GD
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/TT_CThueLoi.gif" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">
                                                                            Chuyển thông tin thuế lỗi
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/Huy_CT_Loi.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">
                                                                            Duyệt hủy chuyển thuế lỗi
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="display: none">
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/Huy_CT_Loi.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">
                                                                            Hủy chuyển thuế/HQ lỗi
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 10%">
                                                                            <img src="../../images/icons/ChuaNhapBDS.png" />
                                                                        </td>
                                                                        <td style="width: 90%" colspan="2">
                                                                            Điều chỉnh chờ kiểm soát
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td align="left" style="width: 100%" colspan="3">
                                                                <hr style="width: 50%" />
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td style="width: 100%" colspan="3">
                                                                Alt + K : Kiểm soát<br />
                                                                Alt + C : Chuyển trả
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td align="left" style="width: 100%" colspan="3">
                                                                <hr style="width: 50%" />
                                                            </td>
                                                        </tr>
                                                        <tr class="img">
                                                            <td style="width: 100%" colspan="3">
                                                                Đối với Firefox
                                                                <br />
                                                                Alt + Shift + K : Kiểm soát<br />
                                                                Alt + Shift + C : Chuyển trả
                                                            </td>
                                                        </tr>
                                                    </table>
        </td>
        <td valign="top">
        <div style="text-align:right;color:Red" >Số CT:<asp:Label runat="server" ID="lblSoCTU"></asp:Label></div>
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
            <tr>
                <td align="left" style="width:25%" class="formx">MST / Tên đơn vị XNK <span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                    <asp:HiddenField runat="server" ID="txtID" />
                    <asp:TextBox ID="txtMST" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="false" Width="150px">
                    
                    </asp:TextBox>
                    <asp:TextBox ID="txtTen_DV" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true"></asp:TextBox>
               </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Mã chương<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                     <asp:TextBox ID="txtMaChuong" runat="server" CssClass="inputflat txtHoanThien01" ReadOnly="true" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Số hiệu KB/ Tên KB<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                     <asp:TextBox ID="txtSHKB" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="false" Width="150px"></asp:TextBox>
                     <asp:TextBox ID="txtTenKB" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Mã HQ PH<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMa_HQ_PH" runat="server" CssClass="inputflat txtHoanThien" Width="150px" ReadOnly="true"></asp:TextBox>
                <asp:TextBox ID="txtTen_HQ_PH" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Mã HQ CQT<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMa_HQ_CQT" runat="server" CssClass="inputflat txtHoanThien"  Width="150px" ReadOnly="true">
                </asp:TextBox>
                <asp:TextBox ID="txtTen_HQ_CQT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td align="left" style="width:25%" class="formx">TK có NSNN<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMa_NTK" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true"  Width="150px"></asp:TextBox>
                <asp:TextBox ID="txtTKKB" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true"  ></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td align="left" style="width:25%" class="formx">Ngày Lập/ Truyên CT<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtNgayLap_CT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                 <asp:TextBox ID="txtNgayTruyen_CT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Ký hiệu/ Số CT<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                 <asp:TextBox ID="txtKyHieu" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                <asp:TextBox ID="txtSo_CT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
                
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Loại CT<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtLoai_CT" runat="server" CssClass="inputflat txtHoanThien01"  ReadOnly="true" ></asp:TextBox>
                 
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Ngày CT/ Ngày Báo nợ<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtNgay_CT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                <asp:TextBox ID="txtNgay_BN" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" class="form_label" colspan="2">&nbsp;</td>
               
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Nguyên tệ/ Tỷ giá<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMa_NT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                <asp:TextBox ID="txtTyGia" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td align="left" style="width:25%" class="formx">Số TK/ Tên tài khoản<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtSOTKTT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                <asp:TextBox ID="txtTenTKTT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">MST/ Tên người nộp<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMST_NNT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                <asp:TextBox ID="txtTen_NNT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Số CMT Người nộp tiền<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtSo_CMT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
               
                </td>
            </tr>
              <tr>
                <td align="left" style="width:25%" class="formx">Địa chỉ Người nộp tiền<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtDiaChi" runat="server" CssClass="inputflat txtHoanThien01"  ReadOnly="true" ></asp:TextBox>
               
                </td>
            </tr>
             <tr>
                <td align="left" style="width:25%" class="formx">Thông tin khác<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtTT_Khac" runat="server" CssClass="inputflat txtHoanThien01"  ReadOnly="true" ></asp:TextBox>
               
                </td>
            </tr>
             <tr>
                <td align="left" style="width:25%" class="formx">Mã /Tên Ngân hàng chuyển<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMa_NH_TH" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                 <asp:TextBox ID="txtTen_NH_TH" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
               
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Mã /Tên Ngân hàng trực tiếp<span class="requiredField"></span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMa_NH_TT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                 <asp:TextBox ID="txtTen_NH_TT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
               
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Mã /Tên Ngân hàng thụ hưởng<span class="requiredField"></span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMa_NH_GTiep" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                 <asp:TextBox ID="txtTen_NH_GTiep" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
               
                </td>
            </tr>
           <tr >
                <td align="left" style="width:25%" class="formx">Diễn giải</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtDienGiai" runat="server" ReadOnly="true" CssClass="inputflat txtHoanThien01" TextMode="MultiLine" Rows="2" ></asp:TextBox></td>
            </tr>
           
        </table>
          <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" style="display:none" >
            <tr>
               
                <td align="right" style="width:25%" class="formx">
                 <span style="color:Red"><asp:Label  ID="lblErrorTaiKhoanTH" runat="server" ></asp:Label></span>
                <asp:Button ID="btnCheckTK" runat="server"   Text="Làm mới" />
                &nbsp;
                </td>
                <td align="center" style="width:30%"  class="form_label">Thông tin tài khoản trên CT </td>
                <td align="center" style="width:30%" class="form_label">Thông tin Tài khoản sử dụng NTDT   </td>
            </tr>
            <tr>
                <td align="left" class="formx">Tài khoản chuyển</td>
                <td class="form_label"><asp:TextBox ID="txtTaiKhoan_TH" runat="server" CssClass="inputflat txtHoanThien80" ReadOnly="true"></asp:TextBox></td>
                <td class="form_label"><asp:TextBox ID="txtCurrTK" runat="server" CssClass="inputflat txtHoanThien80"  ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="formx">Tên Tài khoản chuyển</td>
                <td class="form_label"><asp:TextBox ID="txtTen_TaiKhoan_TH" runat="server" CssClass="inputflat txtHoanThien80"  ReadOnly="true"></asp:TextBox></td>
                <td class="form_label"><asp:TextBox ID="txtCurTenTK" runat="server" CssClass="inputflat txtHoanThien80"  ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left"  class="formx">Tổng tiền TT thuế / Số dư tài khoản</td>
                <td class="form_label"><asp:TextBox ID="txtSoTienCanNop" runat="server" CssClass="inputflat txtHoanThien80 number"  ReadOnly="true"></asp:TextBox></td>
                <td class="form_label"><asp:TextBox ID="txtSoDuTK" runat="server" CssClass="inputflat txtHoanThien80 number"  ReadOnly="true"></asp:TextBox></td>
            </tr>
            </table>
         <div class="dataView">
        <asp:DataGrid ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="100%" BorderColor="Black" CssClass="grid_data" 
            >
            <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
            <HeaderStyle  CssClass="grid_header"></HeaderStyle>
            <ItemStyle CssClass="grid_item" />
            <Columns>
            <asp:BoundColumn DataField="TTButToan" HeaderText="STT bút toán ">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ID_HS" HeaderText="ID hồ sơ">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="So_TK" HeaderText="Số Tờ khai">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Ma_HQ" HeaderText="Mã HQ">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                 <asp:BoundColumn DataField="Ma_LH" HeaderText="Mã Loại hình">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                 <asp:BoundColumn DataField="Nam_DK" HeaderText="Năm ĐK">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Ma_LT" HeaderText="Mã Loại tiền">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Ma_ST" HeaderText="Sắc thuế">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                 <asp:BoundColumn DataField="MTMUC" HeaderText="Mã TM">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                  <asp:BoundColumn DataField="NDKT" HeaderText="Nội dung Kinh tế">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                   <asp:BoundColumn DataField="SoTien_VND" HeaderText="Số tiền VND">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundColumn>
                 <asp:BoundColumn DataField="SoTien_NT" HeaderText="Số tiền NT">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundColumn>
             
             
                
            </Columns>
          
        </asp:DataGrid>
            <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
            <tr>
                <td align="right"  class="formx">Tổng tiền </td>
                <td align="left" style="width:80%" class="form_label"><asp:TextBox ID="txtTongTien" runat="server" CssClass="inputflat txtHoanThien80 number"  ReadOnly="true"></asp:TextBox></td>
            </tr>
             <tr>
                <td align="right" style="width:30%"  class="formx">Bằng chữ </td>
                <td align="left" style="width:30%" class="form_label"><asp:Label ID="lblSotienBangCHu" runat="server" CssClass="inputflat txtHoanThien80" >cxxxxxxx</asp:Label></td>
            </tr>
            </table>
            <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
             <tr >
                <td align="left" style="width:25%" class="formx">Nội dung Hướng dẫn</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtLyDo_Huy" runat="server" CssClass="inputflat txtHoanThien01"  ></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Lý do chuyển trả</td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtLydo_ChuyenTra" runat="server" CssClass="inputflat txtHoanThien01"  ReadOnly="true"></asp:TextBox></td>
            </tr>
            </table>
    </div>
        <div>
        <br />
        <asp:Button ID="btnChuyenKS" runat="server"  Text="Duyệt CT" Width="120px" />&nbsp;&nbsp;
         <asp:Button ID="btnChuyenTra" runat="server"   Text="Chuyển trả" Width="120px"/>&nbsp;&nbsp;
                    <asp:Button ID="btnHuy" runat="server"   Text="Duyệt từ chối CT" Width="120px"/>&nbsp;&nbsp;
                     
                    <asp:Button ID="btnExit" runat="server"  Text="Thoát" Width="120px" />
        </div>
        </td>
    </tr>
    </table>
        
    </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtNgayGui").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>		
</asp:Content>

