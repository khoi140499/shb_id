﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.HQ247.NNTDT
Imports Business.Common.mdlCommon
Imports Business.BaoCao
Imports Business.Common
Imports System.IO
Imports System.Xml
Imports CustomsV3.MSG.MSG304
Imports System.Collections.Generic
Imports System.Diagnostics
Imports Business_HQ
Partial Class HQ247_frmKiemSoatYCTTHQ
    Inherits System.Web.UI.Page
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        loadDanhSachCTU()
        btnCheckTK.Enabled = False
        btnChuyenKS.Enabled = False
        btnHuy.Enabled = False
        btnChuyenTra.Enabled = False
        If Not IsPostBack Then
            loadDMTrangThai304()

           
        End If

    End Sub
    Public Function convertDateHQ(ByVal strDate As String) As String

        Dim str As String = ""
        Try
            str = strDate.Substring(8, 2) & "/" & strDate.Substring(5, 2) & "/" & strDate.Substring(0, 4)
            If strDate.Trim.Length > 10 Then
                str &= strDate.Substring(10, strDate.Length - 10)
            End If
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log.Error(sbErrMsg.ToString())

        End Try

        Return str
    End Function
    Protected Sub loadDMTrangThai304()
        Try
            Dim ds As DataSet = DataAccess.ExecuteReturnDataSet("SELECT * FROM TCS_DM_TRANGTHAI_TT_HQ247 ORDER BY TRANGTHAI", CommandType.Text)
            If Not ds Is Nothing Then
                If ds.Tables.Count > 0 Then
                    ddlTrangThai.DataSource = ds.Tables(0)
                    ddlTrangThai.DataTextField = "MOTA"
                    ddlTrangThai.DataValueField = "TRANGTHAI"
                    ddlTrangThai.DataBind()
                    ddlTrangThai.Items.Insert(0, "Tất cả")
                End If
            End If
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log.Error(sbErrMsg.ToString())

        End Try
    End Sub
    Protected Sub btnChuyenKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChuyenKS.Click

        Try
            Dim decResult As Decimal = Business_HQ.HQ247.daCTUHQ304.CheckValidTAIKHOAN_TH(txtMST.Text, txtTaiKhoan_TH.Text)
            If decResult > 0 Then
                clsCommon.ShowMessageBox(Me, "Duyệt chứng từ lỗi: " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()))
                Return
            End If
            decResult = Business_HQ.HQ247.daCTUHQ304.checkTrangthaiMSG304(txtID.Value, Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213)
            If decResult > 0 Then
                clsCommon.ShowMessageBox(Me, "Duyệt chứng từ lỗi: " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()))
                Return
            End If
            
          
            
            'doan nay em chuyen sang send HQ 213
            decResult = CustomsServiceV3.ProcessMSG.sendMSG213("304", "1", txtLyDo_Huy.Text, txtID.Value, Session.Item("UserName").ToString())
            If decResult > 0 Then
                clsCommon.ShowMessageBox(Me, "Duyệt chứng từ lỗi: khi chuyển MSG 213 sang HQ " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()))
                Return
            End If
            'h em chuyen sang maker 301 va send 301
            Dim objHDR As Business_HQ.NewChungTu.infChungTuHDR = Nothing
            Dim lstDTL As List(Of Business_HQ.NewChungTu.infChungTuDTL) = New List(Of Business_HQ.NewChungTu.infChungTuDTL)
            decResult = setData301(objHDR, lstDTL)
            If decResult > 0 Then
                clsCommon.ShowMessageBox(Me, "Duyệt chứng từ lỗi: khi Tao MSG 301 " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()))
                Return
            End If
            decResult = Business_HQ.HQ247.daCTUHQ304.Insert301(objHDR, lstDTL, txtID.Value, Session.Item("UserName").ToString())
            If decResult > 0 Then
                clsCommon.ShowMessageBox(Me, "Duyệt chứng từ lỗi: khi Tao MSG 301 " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()))
                'CAP NHAT TRANG THAI LA LOI THANH TOAN
                decResult = CustomsServiceV3.ProcessMSG.sendMSG213("304", "2", "Từ chối vì thanh toán lỗi hệ thống: " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()), txtID.Value, Session.Item("UserName").ToString())
                Business_HQ.HQ247.daCTUHQ304.TCS_HQ247_TRANGTHAI_304_MAKER301_ERROR(txtID.Value, Session.Item("UserName").ToString())
                Return
            End If
            Dim strErrorNum As String = "0"
            Dim strErrorMes As String = ""
            Dim v_strTransaction_id As String = ""
            Dim pv_so_tn_ct As String = ""
            Dim pv_ngay_tn_ct As String = ""

            CustomsServiceV3.ProcessMSG.guiCTThueHQ247(txtMST.Text, objHDR.So_CT, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
            If Not strErrorNum.Equals("0") Then
                Business_HQ.HQ247.daCTUHQ304.TCS_HQ247_TRANGTHAI_304_SEND301_ERROR(txtID.Value, Session.Item("UserName").ToString())
                clsCommon.ShowMessageBox(Me, "Chứng từ đã được hạch toán. Có lỗi khi gửi số liệu sang Hải Quan. Mã lỗi " & strErrorNum & " Mô tả lỗi: " & strErrorMes)
                LoadCTChiTietAll(txtID.Value)
                Return

            End If
            'doan nay cho doi chieu
            decResult = Business_HQ.HQ247.daCTUHQ304.TCS_HQ247_TRANGTHAI_304_SEND301_OK(txtID.Value, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct, Session.Item("UserName").ToString())
            If decResult > 0 Then
                clsCommon.ShowMessageBox(Me, "Chứng từ đã được hạch toán.Đã gửi HQ -> lỗi ghi nhận đối chiếu" & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()))
                Return
            End If

            btnChuyenKS.Enabled = False
            btnHuy.Enabled = False
            clsCommon.ShowMessageBox(Me, "Duyệt thành công chứng từ: " & txtSo_CT.Text)
            LoadCTChiTietAll(txtID.Value)
            loadDanhSachCTU()
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
            clsCommon.ShowMessageBox(Me, "Duyệt chứng từ lỗi: " & ex.Message)
        End Try
    End Sub
    Public Function setData301(ByRef objHdrHq As Business_HQ.NewChungTu.infChungTuHDR, ByRef listDTL As List(Of Business_HQ.NewChungTu.infChungTuDTL)) As Decimal
        Try
            objHdrHq = New Business_HQ.NewChungTu.infChungTuHDR()
            listDTL = New List(Of Business_HQ.NewChungTu.infChungTuDTL)


            objHdrHq.SHKB = txtSHKB.Text
            objHdrHq.Ma_Tinh = ""
            objHdrHq.Ma_Huyen = ""
            objHdrHq.Ma_Xa = ""
            objHdrHq.TK_Co = txtTKKB.Text
            objHdrHq.Ma_LThue = "04"
            objHdrHq.Ma_NNTien = txtMST_NNT.Text
            objHdrHq.Ten_NNTien = txtTen_NNT.Text
            objHdrHq.DC_NNTien = txtDiaChi.Text
            objHdrHq.MA_QUAN_HUYENNNTIEN = ""
            objHdrHq.MA_TINH_TPNNTIEN = ""
            objHdrHq.Ma_NNThue = txtMST.Text
            objHdrHq.Ten_NNThue = txtTen_DV.Text
            objHdrHq.DC_NNThue = txtDiaChi.Text
            objHdrHq.Ma_Huyen_NNThue = ""
            objHdrHq.Ma_Tinh_NNThue = ""
            objHdrHq.Ngay_CT = Date.Now.ToString("yyyyMMdd")
            objHdrHq.Ngay_HT = Date.Now.ToString("dd/MM/yyyy")
            objHdrHq.Ma_CQThu = txtMa_HQ_CQT.Text
            objHdrHq.PT_TT = "01"
            objHdrHq.Ma_NT = txtMa_NT.Text
            objHdrHq.Ty_Gia = txtTyGia.Text
            objHdrHq.TTien = txtTongTien.Text.Replace(".", "").Replace(",", ".")
            objHdrHq.TTien_NT = txtTongTien.Text.Replace(".", "").Replace(",", ".")
            objHdrHq.TT_TThu = txtTongTien.Text.Replace(".", "").Replace(",", ".")
            objHdrHq.TK_KH_NH = txtTaiKhoan_TH.Text
            objHdrHq.TEN_KH_NH = txtTen_TaiKhoan_TH.Text
            objHdrHq.TK_GL_NH = ""
            objHdrHq.MA_NH_A = txtMa_NH_TH.Text
            objHdrHq.MA_NH_TT = txtMa_NH_TT.Text
            objHdrHq.TEN_NH_TT = txtTen_NH_TT.Text
            objHdrHq.MA_NH_B = txtMa_NH_GTiep.Text
            objHdrHq.Ten_NH_B = txtTen_NH_GTiep.Text

            If objHdrHq.MA_NH_TT.Length = 0 Then
                Return Business_HQ.Common.mdlCommon.TCS_HQ247_HQ_NGANHANGTT_GIANTIEP_LOI
            End If
            objHdrHq.Trang_Thai = "05"
            'doan nay lien quan den tinh phi
            'chua thuc hien bao h lam thuc thi tinh
            objHdrHq.PT_TINHPHI = "O"
            objHdrHq.PHI_GD = "0" ' tFee;
            objHdrHq.PHI_VAT = "0" ' tVAT;
            objHdrHq.PHI_GD2 = "0"
            objHdrHq.PHI_VAT2 = "0"
            objHdrHq.TT_CTHUE = "0"

            objHdrHq.LOAI_TT = "" '    //Chuyen xuong HDR
            objHdrHq.MA_NTK = txtMa_NTK.Text '$('#ddlMa_NTK').val();
            objHdrHq.MA_HQ = txtMa_HQ_PH.Text ' $('#txtMaHQ').val();
            objHdrHq.TEN_HQ = txtTen_HQ_PH.Text '$('#txtTenMaHQ').val();
            objHdrHq.Ma_hq_ph = txtMa_HQ_PH.Text '$('#txtMaHQPH').val();
            objHdrHq.TEN_HQ_PH = txtTen_HQ_PH.Text '$('#txtTenMaHQPH').val();
            objHdrHq.Kenh_CT = "ETAX"
            objHdrHq.SAN_PHAM = "XXX" '$('#txtSanPham').val();
            objHdrHq.TT_CITAD = "0"
            objHdrHq.SO_CMND = txtSo_CMT.Text '$("#txtCmtnd").val();
            objHdrHq.SO_FONE = "" ' $("#txtDienThoai").val();
            objHdrHq.LOAI_CTU = "T"
            objHdrHq.REMARKS = ""
            objHdrHq.Ten_cqthu = txtTen_HQ_CQT.Text '$('#txtTenCQThu').val();
            objHdrHq.So_TK = ""
            '//sonmt: bo sung ithem cac truong cua mizuho

            objHdrHq.So_CT_NH = "" ' $("#txtSoRef").val();
            'objHdrHq.NGAY_IMPORT = $("#txtNgayImport").val();
            objHdrHq.Ghi_chu = txtDienGiai.Text
            objHdrHq.MA_CUC = "" ' $("#txtMaCuc").val();
            objHdrHq.TEN_CUC = "" ' $("#txtTenCuc").val();
            objHdrHq.REMARKS = txtDienGiai.Text ' $("#txtDienGiai").val();
            objHdrHq.DIENGIAI_HQ = txtDienGiai.Text ' $("#txtDienGiaiHQ").val();
            objHdrHq.Ten_kb = txtTenKB.Text '$("#txtTenKB").val();
            objHdrHq.Ma_NV = Integer.Parse(Session.Item("User").ToString())
            objHdrHq.KyHieu_CT = Business_HQ.HaiQuan.HaiQuanController.GetKyHieuChungTu(objHdrHq.SHKB, objHdrHq.Ma_NV, "01")
            objHdrHq.Ngay_KB = Business_HQ.HaiQuan.HaiQuanController.GetNgayKhoBac(objHdrHq.SHKB)
            objHdrHq.NGAY_BAONO = Business_HQ.Common.mdlCommon.ConvertStringToDate(txtNgay_BN.Text, "dd/mm/yyyy")

            Dim strTT_CUTOFFTIME As String = CTuCommon.GetValue_THAMSOHT("TT_CUTOFFTIME")
            If strTT_CUTOFFTIME.Equals("2") Then
                objHdrHq.NGAY_BAOCO = Date.Now
            Else
                Dim ngay_tiep_theo As DateTime = DateTime.Now.AddDays(1)
                While (True) 'Kiem tra xem ngay tiep theo co vao ngay nghi hay khong, neu vao ngay nghi thi cong them 1 ngay cho den ngay lam viec tiep theo
                    Dim dtNgayNghi As DataTable = DataAccess.ExecuteReturnDataSet("SELECT ngay_nghi FROM tcs_dm_ngaynghi WHERE ngay_nghi = to_date('" & ngay_tiep_theo.ToString("dd/MM/yyyy") & "', 'dd/MM/yyyy')", CommandType.Text).Tables(0)
                    If dtNgayNghi.Rows.Count > 0 Then
                        ngay_tiep_theo = ngay_tiep_theo.AddDays(1)
                    Else
                        Exit While
                    End If

                End While
                objHdrHq.NGAY_BAOCO = ngay_tiep_theo
            End If

            objHdrHq.Ma_DThu = clsCTU.TCS_GetMaDT(CType(Session("User"), Integer))
            Dim sSo_BT As String = String.Empty
            Dim sNam_KB As String
            Dim sThang_KB As String
            sNam_KB = Mid(objHdrHq.Ngay_KB, 3, 2)
            sThang_KB = Mid(objHdrHq.Ngay_KB, 5, 2)
            objHdrHq.So_CT = sNam_KB + sThang_KB + Business_HQ.CTuCommon.Get_SoCT()

            objHdrHq.Ma_CN = Session.Item("MA_CN_USER_LOGON").ToString()
            Dim ds As DataSet = Business_HQ.HQ247.daCTUHQ304.getMSG304Detail(txtID.Value)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim obj As MSG304 = New MSG304()
                obj.SETDATA(ds.Tables(0).Rows(0)("MSG_CONTENT").ToString())
                For Each objGNT_CT As CustomsV3.MSG.MSG304.GNT_CT In obj.Data.ThongTinChungTu.GNT_CT
                    For Each objToKhai_CT As CustomsV3.MSG.MSG304.ToKhai_CT In objGNT_CT.ToKhai_CT
                        Dim objDTL As Business_HQ.NewChungTu.infChungTuDTL = New Business_HQ.NewChungTu.infChungTuDTL()
                        objDTL.SO_TK = objGNT_CT.So_TK
                        objDTL.LH_XNK = objGNT_CT.Ma_LH
                        objDTL.SAC_THUE = objToKhai_CT.Ma_ST
                        objDTL.Ma_TMuc = objToKhai_CT.NDKT
                        objDTL.SoTien = objToKhai_CT.SoTien_VND
                        objDTL.SoTien_NT = objToKhai_CT.SoTien_NT
                        objDTL.Noi_Dung = Business_HQ.Common.mdlCommon.Get_TenMTM(objToKhai_CT.NDKT)
                        objDTL.MA_LT = objGNT_CT.Ma_LT
                        objDTL.Ma_Chuong = txtMaChuong.Text
                        objDTL.TT_BTOAN = objGNT_CT.TTButToan
                        objDTL.MA_HQ = txtMa_HQ_PH.Text
                        If objHdrHq.So_TK.Length = 0 Then
                            objHdrHq.So_TK = objGNT_CT.So_TK
                        Else
                            If objHdrHq.So_TK.IndexOf(objGNT_CT.So_TK) < 0 Then
                                objHdrHq.So_TK &= ";" & objGNT_CT.So_TK
                            End If
                        End If
                        'vi ko co ngay to khai nen tam dien them 01/01 cong nam to khai
                        objDTL.NGAY_TK = "01/01/" + objGNT_CT.Nam_DK
                        listDTL.Add(objDTL)

                    Next
                Next

            End If

            Return Business_HQ.Common.mdlCommon.TCS_HQ247_OK

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
        End Try
        Return Business_HQ.Common.mdlCommon.TCS_HQ247_SYSTEM_NOT_OK
    End Function

    Protected Sub btnChuyenTra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChuyenTra.Click

        Try
            If txtLydo_ChuyenTra.Text.Length = 0 Then
                clsCommon.ShowMessageBox(Me, "Chưa nhập lý do chuyển trả. ")
                txtLydo_ChuyenTra.Focus()
                Return
            End If

            Dim decResult As Decimal = Business_HQ.HQ247.daCTUHQ304.checkTrangthaiMSG304(txtID.Value, Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER213_TUCHOI)
            If decResult > 0 Then
                clsCommon.ShowMessageBox(Me, "Chuyển trả lỗi trạng thái chứng từ: " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()))
                Return
            End If
            decResult = Business_HQ.HQ247.daCTUHQ304.checkTrangthaiMSG304(txtID.Value, Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER213)
            If decResult > 0 Then
                clsCommon.ShowMessageBox(Me, "Chuyển trả lỗi trạng thái chứng từ: " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()))
                Return
            End If

            decResult = Business_HQ.HQ247.daCTUHQ304.TCS_HQ247_TRANGTHAI_304_CHUYENTRA(txtID.Value, txtLydo_ChuyenTra.Text, Session.Item("UserName").ToString())

            'doan nay em chuyen sang send HQ 213
            decResult = CustomsServiceV3.ProcessMSG.sendMSG213("304", "2", txtLyDo_Huy.Text, txtID.Value, Session.Item("UserName").ToString())
            If decResult > 0 Then
                clsCommon.ShowMessageBox(Me, "Chuyển trả lỗi :" & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()))
                Return
            End If
            'h em chuyen sang maker 301 va send 301
            btnCheckTK.Enabled = False
            btnChuyenKS.Enabled = False
            btnHuy.Enabled = False
            clsCommon.ShowMessageBox(Me, "Chuyển trả Thành công: " & txtSo_CT.Text)
            LoadCTChiTietAll(txtID.Value)
            loadDanhSachCTU()
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
            clsCommon.ShowMessageBox(Me, "Chuyển trả Lỗi: " & ex.Message)
        End Try
    End Sub
    Protected Sub btnHuy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHuy.Click

        Try

            Dim decResult As Decimal = Business_HQ.HQ247.daCTUHQ304.checkTrangthaiMSG304(txtID.Value, Business_HQ.Common.mdlCommon.TCS_HQ247_HS_TRANGTHAI_MAKER213_TUCHOI)
            If decResult > 0 Then
                clsCommon.ShowMessageBox(Me, "Duyệt từ chối chứng từ lỗi: " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()))
                Return
            End If
            'doan nay em chuyen sang send HQ 213
            decResult = CustomsServiceV3.ProcessMSG.sendMSG213("304", "2", txtLyDo_Huy.Text, txtID.Value, Session.Item("UserName").ToString())
            If decResult > 0 Then
                clsCommon.ShowMessageBox(Me, "Duyệt từ chối chứng từ lỗi: khi chuyển MSG 213 sang HQ " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decResult.ToString()))
                Return
            End If
            'h em chuyen sang maker 301 va send 301
            btnCheckTK.Enabled = False
            btnChuyenKS.Enabled = False
            btnHuy.Enabled = False
            clsCommon.ShowMessageBox(Me, "Duyệt từ chối thành công chứng từ: " & txtSo_CT.Text)
            LoadCTChiTietAll(txtID.Value)
            loadDanhSachCTU()
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
            clsCommon.ShowMessageBox(Me, "Duyệt từ chối chứng từ lỗi: " & ex.Message)
        End Try
    End Sub
    Protected Sub btnSelectCT_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnSelectCT As LinkButton = CType(sender, LinkButton)
            Dim v_strRetArgument As String = btnSelectCT.CommandArgument.ToString()
            If (v_strRetArgument.Length > 0) Then
                LoadCTChiTietAll(v_strRetArgument)
            End If
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log.Error(sbErrMsg.ToString())

        End Try
    End Sub
    Protected Sub loadDanhSachCTU()
        Try
            Dim strSQL As String = "SELECT ID,HQ_SO_CTU,NVL((SELECT MAX(MOTA) FROM TCS_DM_TRANGTHAI_TT_HQ247 WHERE TRANGTHAI=A.TRANGTHAI),'____') TRANG_THAI,TRANGTHAI"
            strSQL &= " FROM TCS_HQ247_304 A,TCS_DM_NHANVIEN B , TCS_DM_NHANVIEN C "
            strSQL &= " WHERE 1=1 "
            If ddlTrangThai.SelectedIndex > 0 Then
                strSQL &= " AND trangthai='" & ddlTrangThai.SelectedValue.ToString() & "' "
            End If
            'LAY NGAY LAM VIEC
            strSQL &= " AND ((HQ_NGAYLAP_CT=TRUNC(SYSDATE)  AND TRANGTHAI IN ('04','07','03','06')) OR TRANGTHAI IN ('02','05','10') ) "
            strSQL &= " AND UPPER(A.ID_MK_213)=UPPER(B.TEN_DN) AND B.MA_CN=C.MA_CN AND UPPER(C.TEN_DN)=UPPER('" & Session.Item("UserName") & "')"
            Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            grdDSCT.DataSource = ds.Tables(0)
            grdDSCT.DataBind()
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log.Error(sbErrMsg.ToString())

        End Try

    End Sub
    Private Sub LoadCTChiTietAll(ByVal pStrID As String)
        Try
            txtID.Value = pStrID

            Dim ds As DataSet = Business_HQ.HQ247.daCTUHQ304.getMSG304Detail(pStrID)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim obj As MSG304 = New MSG304()
                obj.SETDATA(ds.Tables(0).Rows(0)("MSG_CONTENT").ToString())
                txtMST.Text = obj.Data.ThongTinChungTu.Ma_DV
                txtTen_DV.Text = obj.Data.ThongTinChungTu.Ten_DV
                txtMaChuong.Text = obj.Data.ThongTinChungTu.Ma_Chuong
                txtSHKB.Text = obj.Data.ThongTinChungTu.Ma_KB
                txtTenKB.Text = obj.Data.ThongTinChungTu.Ten_KB
                txtMa_HQ_PH.Text = obj.Data.ThongTinChungTu.Ma_HQ_PH
                txtMa_HQ_CQT.Text = obj.Data.ThongTinChungTu.Ma_HQ_CQT
                txtMa_NTK.Text = obj.Data.ThongTinChungTu.Ma_NTK
                txtTKKB.Text = obj.Data.ThongTinChungTu.TKKB
                txtNgayLap_CT.Text = convertDateHQ(obj.Data.ThongTinChungTu.NgayLap_CT)
                txtNgayTruyen_CT.Text = convertDateHQ(obj.Data.ThongTinChungTu.NgayTruyen_CT)
                txtKyHieu.Text = obj.Data.ThongTinChungTu.KyHieu_CT
                txtSo_CT.Text = obj.Data.ThongTinChungTu.So_CT
                txtLoai_CT.Text = obj.Data.ThongTinChungTu.Loai_CT
                txtNgay_BN.Text = convertDateHQ(obj.Data.ThongTinChungTu.Ngay_BN)
                txtNgay_CT.Text = convertDateHQ(obj.Data.ThongTinChungTu.Ngay_CT)
                txtMa_NT.Text = obj.Data.ThongTinChungTu.Ma_NT
                txtTyGia.Text = obj.Data.ThongTinChungTu.Ty_Gia
                txtMST_NNT.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.Ma_ST
                txtTen_NNT.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.Ten_NNT
                txtSo_CMT.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.So_CMT
                txtDiaChi.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.DiaChi
                txtDienGiai.Text = obj.Data.ThongTinChungTu.DienGiai
                txtTT_Khac.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.TT_Khac
                txtMa_NH_TH.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ma_NH_TH
                txtTen_NH_TH.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ten_TaiKhoan_TH
                txtSoTienCanNop.Text = obj.Data.ThongTinChungTu.SoTien_TO
                txtTongTien.Text = obj.Data.ThongTinChungTu.SoTien_TO
                lblSotienBangCHu.Text = ConcertCurToText.ConvertCurr.So_chu(obj.Data.ThongTinChungTu.SoTien_TO)
                lblSoCTU.Text = txtSo_CT.Text & "-" & ds.Tables(0).Rows(0)("TRANG_THAI").ToString()
                txtLyDo_Huy.Text = ds.Tables(0).Rows(0)("LYDO").ToString()
                txtTaiKhoan_TH.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.TaiKhoan_TH
                txtTen_TaiKhoan_TH.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ten_TaiKhoan_TH
                txtSOTKTT.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.TaiKhoan_TH
                txtTenTKTT.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ten_TaiKhoan_TH

                txtTen_HQ_PH.Text = Business_HQ.HQ247.daCTUHQ304.getTenHQ_PH(obj.Data.ThongTinChungTu.Ma_HQ_PH)
                txtTen_HQ_CQT.Text = Business_HQ.HQ247.daCTUHQ304.getTenHQ_CQT(obj.Data.ThongTinChungTu.Ma_HQ_CQT)



                dtgrd.DataSource = obj.Data.ThongTinChungTu.convertGNT_CTToDS().Tables(0)
                dtgrd.DataBind()
                Dim dsx As DataSet = Business_HQ.HQ247.daCTUHQ304.getNHTrucTiepGianTiep(obj.Data.ThongTinChungTu.Ma_KB)
                If Not dsx Is Nothing Then
                    If dsx.Tables.Count > 0 Then
                        If dsx.Tables(0).Rows.Count > 0 Then
                            txtMa_NH_TT.Text = dsx.Tables(0).Rows(0)("MA_TRUCTIEP").ToString() '  $('#txtMA_NH_TT').val();
                            txtTen_NH_TT.Text = dsx.Tables(0).Rows(0)("TEN_TRUCTIEP").ToString()  '$('#txtTEN_NH_TT').val();
                            txtMa_NH_GTiep.Text = dsx.Tables(0).Rows(0)("MA_GIANTIEP").ToString() ' $('#txtMA_NH_B').val();
                            txtTen_NH_GTiep.Text = dsx.Tables(0).Rows(0)("TEN_GIANTIEP").ToString() ' $('#txtTenMA_NH_B').val();

                        End If
                    End If
                End If

                'moi nhan 01
                If ds.Tables(0).Rows(0)("TRANGTHAI").ToString().Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213) Then
                    checktaiKhoanNganHang()
                    btnChuyenKS.Enabled = True
                    btnChuyenTra.Enabled = True
                    btnCheckTK.Enabled = True
                ElseIf ds.Tables(0).Rows(0)("TRANGTHAI").ToString().Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_MAKER213_TUCHOI) Then
                    checktaiKhoanNganHang()
                    btnHuy.Enabled = True
                    btnChuyenTra.Enabled = True
                    btnCheckTK.Enabled = True

                Else
                    btnCheckTK.Enabled = False
                    btnChuyenKS.Enabled = False
                    btnHuy.Enabled = False
                    btnChuyenTra.Enabled = False
                End If
            End If
            'Me.txtMatKhau.Focus()
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log.Error(sbErrMsg.ToString())

            'Throw ex
        Finally
            'Catch ex As Exception
            '    logger.Error(ex.StackTrace)
        End Try
    End Sub
    Protected Sub btnCheckTK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckTK.Click
        checktaiKhoanNganHang()
    End Sub
    Public Sub checktaiKhoanNganHang()
        lblErrorTaiKhoanTH.Text = ""
        Try
            Dim decResult As Decimal = Business_HQ.HQ247.daCTUHQ304.CheckValidTAIKHOAN_TH(txtMST.Text, txtTaiKhoan_TH.Text)
            If decResult > 0 Then
                lblErrorTaiKhoanTH.Text = "Lỗi Tài khoản ngân hàng không đúng hoặc trạng thái hồ sơ không hợp lệ"
                Return
            End If
        Catch ex As Exception
            lblErrorTaiKhoanTH.Text = "Lỗi Tài khoản ngân hàng không đúng hoặc trạng thái hồ sơ không hợp lệ"
        End Try
        Dim strCoreOn As String = "0"
        'doan nay neu lam ket noi ngan hang thi cal den core bank de check so du
        If strCoreOn.Equals("1") Then
        Else
            txtCurrTK.Text = txtTaiKhoan_TH.Text
            txtCurTenTK.Text = txtTen_NNT.Text
            txtSoDuTK.Text = "9999999999"
        End If
    End Sub
End Class
