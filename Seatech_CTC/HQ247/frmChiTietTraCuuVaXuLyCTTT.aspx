﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmChiTietTraCuuVaXuLyCTTT.aspx.vb" Inherits="NTDTHQ247_frmChiTietTraCuuVaXuLyCTTT" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Chi tiết thông tin</title>
    <link href="../css/myStyles.css" type="text/css" rel="stylesheet" />
    <link href="../css/menuStyle.css" type="text/css" rel="stylesheet" />
     <style type="text/css">
        .txtHoanThien80{ max-width:400px; width:80%; }
        .txtHoanThien{ width:400px; }
        .txtHoanThien01{width:557px; }
        .container{width:100%;padding-bottom:50px;}
        .dataView{padding-bottom:10px}
        .number{ text-align:right; }
        .formx
        {
        	color: #E7E7FF;
background: #eaedf0;
padding-left: 1px;
color:Black;
        	}
    </style>
  
</head>
<body>
    <form id="form1" runat="server">
     <div class="container">
        <div style="text-align:right;color:Red" >Số CT:<asp:Label runat="server" ID="lblSoCTU"></asp:Label></div>
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
            <tr>
                <td align="left" style="width:25%" class="formx">MST / Tên đơn vị XNK <span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                    <asp:HiddenField runat="server" ID="txtID" />
                    <asp:TextBox ID="txtMST" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="false" Width="150px">
                    
                    </asp:TextBox>
                    <asp:TextBox ID="txtTen_DV" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true"></asp:TextBox>
               </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Mã chương<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                     <asp:TextBox ID="txtMaChuong" runat="server" CssClass="inputflat txtHoanThien01" ReadOnly="true" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Số hiệu KB/ Tên KB<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                     <asp:TextBox ID="txtSHKB" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="false" Width="150px"></asp:TextBox>
                     <asp:TextBox ID="txtTenKB" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Mã HQ PH<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMa_HQ_PH" runat="server" CssClass="inputflat txtHoanThien" Width="150px" ReadOnly="true"></asp:TextBox>
                <asp:TextBox ID="txtTen_HQ_PH" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Mã HQ CQT<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMa_HQ_CQT" runat="server" CssClass="inputflat txtHoanThien"  Width="150px" ReadOnly="true">
                </asp:TextBox>
                <asp:TextBox ID="txtTen_HQ_CQT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td align="left" style="width:25%" class="formx">TK có NSNN<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMa_NTK" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true"  Width="150px"></asp:TextBox>
                <asp:TextBox ID="txtTKKB" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true"  ></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td align="left" style="width:25%" class="formx">Ngày Lập/ Truyên CT<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtNgayLap_CT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                 <asp:TextBox ID="txtNgayTruyen_CT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Ký hiệu/ Số CT<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                 <asp:TextBox ID="txtKyHieu" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                <asp:TextBox ID="txtSo_CT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
                
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Loại CT<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtLoai_CT" runat="server" CssClass="inputflat txtHoanThien01"  ReadOnly="true" ></asp:TextBox>
                 
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Ngày CT/ Ngày Báo nợ<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtNgay_CT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                <asp:TextBox ID="txtNgay_BN" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" class="form_label" colspan="2">&nbsp;</td>
               
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Nguyên tệ/ Tỷ giá<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMa_NT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                <asp:TextBox ID="txtTyGia" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td align="left" style="width:25%" class="formx">Số TK/ Tên tài khoản<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtSOTKTT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                <asp:TextBox ID="txtTenTKTT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">MST/ Tên người nộp<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMST_NNT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                <asp:TextBox ID="txtTen_NNT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Số CMT Người nộp tiền<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtSo_CMT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
               
                </td>
            </tr>
              <tr>
                <td align="left" style="width:25%" class="formx">Địa chỉ Người nộp tiền<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtDiaChi" runat="server" CssClass="inputflat txtHoanThien01"  ReadOnly="true" ></asp:TextBox>
               
                </td>
            </tr>
             <tr>
                <td align="left" style="width:25%" class="formx">Thông tin khác<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtTT_Khac" runat="server" CssClass="inputflat txtHoanThien01"  ReadOnly="true" ></asp:TextBox>
               
                </td>
            </tr>
             <tr>
                <td align="left" style="width:25%" class="formx">Mã /Tên Ngân hàng chuyển<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMa_NH_TH" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                 <asp:TextBox ID="txtTen_NH_TH" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
               
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Mã /Tên Ngân hàng trực tiếp<span class="requiredField"></span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMa_NH_TT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                 <asp:TextBox ID="txtTen_NH_TT" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
               
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Mã /Tên Ngân hàng thụ hưởng<span class="requiredField"></span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMa_NH_GTiep" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" Width="150px"></asp:TextBox>
                 <asp:TextBox ID="txtTen_NH_GTiep" runat="server" CssClass="inputflat txtHoanThien"  ReadOnly="true" ></asp:TextBox>
               
                </td>
            </tr>
           <tr >
                <td align="left" style="width:25%" class="formx">Diễn giải</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtDienGiai" runat="server" ReadOnly="true" CssClass="inputflat txtHoanThien01" TextMode="MultiLine" Rows="2" ></asp:TextBox></td>
            </tr>
           
        </table>
          <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" style="display:none" >
            <tr>
               
                <td align="right" style="width:25%" class="formx">
                 <span style="color:Red"><asp:Label  ID="lblErrorTaiKhoanTH" runat="server" ></asp:Label></span>
                <asp:Button ID="btnCheckTK" runat="server"   Text="Làm mới" />
                &nbsp;
                </td>
                <td align="center" style="width:30%"  class="form_label">Thông tin tài khoản trên CT </td>
                <td align="center" style="width:30%" class="form_label">Thông tin Tài khoản sử dụng NTDT   </td>
            </tr>
            <tr>
                <td align="left" class="formx">Tài khoản chuyển</td>
                <td class="form_label"><asp:TextBox ID="txtTaiKhoan_TH" runat="server" CssClass="inputflat txtHoanThien80" ReadOnly="true"></asp:TextBox></td>
                <td class="form_label"><asp:TextBox ID="txtCurrTK" runat="server" CssClass="inputflat txtHoanThien80"  ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" class="formx">Tên Tài khoản chuyển</td>
                <td class="form_label"><asp:TextBox ID="txtTen_TaiKhoan_TH" runat="server" CssClass="inputflat txtHoanThien80"  ReadOnly="true"></asp:TextBox></td>
                <td class="form_label"><asp:TextBox ID="txtCurTenTK" runat="server" CssClass="inputflat txtHoanThien80"  ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left"  class="formx">Tổng tiền TT thuế / Số dư tài khoản</td>
                <td class="form_label"><asp:TextBox ID="txtSoTienCanNop" runat="server" CssClass="inputflat txtHoanThien80 number"  ReadOnly="true"></asp:TextBox></td>
                <td class="form_label"><asp:TextBox ID="txtSoDuTK" runat="server" CssClass="inputflat txtHoanThien80 number"  ReadOnly="true"></asp:TextBox></td>
            </tr>
            </table>
         <div class="dataView">
        <asp:DataGrid ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="100%" BorderColor="Black" CssClass="grid_data" >
            <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
            <HeaderStyle  CssClass="grid_header"></HeaderStyle>
            <ItemStyle CssClass="grid_item" />
            <Columns>
            <asp:BoundColumn DataField="TTButToan" HeaderText="STT bút toán ">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="ID_HS" HeaderText="ID hồ sơ">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="So_TK" HeaderText="Số Tờ khai">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Ma_HQ" HeaderText="Mã HQ">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                 <asp:BoundColumn DataField="Ma_LH" HeaderText="Mã Loại hình">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                 <asp:BoundColumn DataField="Nam_DK" HeaderText="Năm ĐK">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Ma_LT" HeaderText="Mã Loại tiền">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Ma_ST" HeaderText="Sắc thuế">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                 <asp:BoundColumn DataField="MTMUC" HeaderText="Mã TM">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                  <asp:BoundColumn DataField="NDKT" HeaderText="Nội dung Kinh tế">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                   <asp:BoundColumn DataField="SoTien_VND" HeaderText="Số tiền VND">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundColumn>
                 <asp:BoundColumn DataField="SoTien_NT" HeaderText="Số tiền NT">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundColumn>
             
             
                
            </Columns>
          
        </asp:DataGrid>
            <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
            <tr>
                <td align="right"  class="formx">Tổng tiền </td>
                <td align="left" style="width:80%" class="form_label"><asp:TextBox ID="txtTongTien" runat="server" CssClass="inputflat txtHoanThien80 number"  ReadOnly="true"></asp:TextBox></td>
            </tr>
             <tr>
                <td align="right" style="width:30%"  class="formx">Bằng chữ </td>
                <td align="left" style="width:30%" class="form_label"><asp:Label ID="lblSotienBangCHu" runat="server" CssClass="inputflat txtHoanThien80" >cxxxxxxx</asp:Label></td>
            </tr>
            </table>
            <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
             <tr >
                <td align="left" style="width:25%" class="formx">Nội dung Hướng dẫn</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtLyDo_Huy" runat="server" CssClass="inputflat txtHoanThien01"  ></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="formx">Lý do chuyển trả</td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtLydo_ChuyenTra" runat="server" CssClass="inputflat txtHoanThien01"  ReadOnly="true"></asp:TextBox></td>
            </tr>
            </table>
    </div>
            <div align="center"  >
            <asp:Button ID="btnChuyenKS" runat="server" CssClass="buttonField" Text="Xử lý CT" Width="120px" Visible="false"/>&nbsp;&nbsp;
            </div>
    </div>
    </form>
</body>
</html>
