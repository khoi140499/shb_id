﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmGuiLaiCTTT.aspx.vb" Inherits="HQ247_frmGuiLaiCTTT" title="Tiếp nhận thông tin thanh toán" %>

<%@ Import Namespace="Business.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
        function jsChiTietNNT(_ID) {
            //window.showModalDialog("../NTDT/frmChiTiet.aspx?ID=" + _ID, "", "dialogWidth:800px;dialogHeight:480px;help:0;status:0;", "_new");
        }
    </script>
     <script type="text/javascript">
       $(document).ready(function(){
         $(".JchkAll").click(function(){
        // alert($(this).is(':checked'));
            $('.JchkGrid').attr('checked',$(this).is(':checked'));
            GetListSoCT();
         });
          
         });
         function GetListSoCT() {
            var values = $('input:checkbox:checked.JchkGrid').map(function() {
                return this.value;
            }).get();
            
            
        
            if (values == '') {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);
               
                
            }
            else {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);
               

            }
        }
    </script>
    <style type="text/css">
        
        .container{width:100%; margin-bottom:30px}
        .dataView{padding-bottom:10px}
        .JchkAll,.JchkGrid{}
        
                 
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle"> Tiếp nhận thông tin thanh toán thuế điện tử HQ </p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
            <tr>
                <td align="left" style="width:15%" class="form_label">Từ ngày</td>
                <td class="form_control" style="width:35%"><span style="display:block; width:40%; position:relative;vertical-align:middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                    onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span></td>
                <td align="left" style="width:15%" class="form_label">Đến ngày</td>
                <td class="form_control" style="width:35%">
                <span style="display:block; width:40%; position:relative; vertical-align:middle;">
                    <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                     onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span></td>
            </tr>
            <tr>
                <td align="left" class="form_label">Từ Số CT Etax</td>
                <td class="form_control"><asp:TextBox ID="txtSoCTUTuSo" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <td align="left" class="form_label">Số CT HQ</td>
                 <td class="form_control">
                   <asp:TextBox ID="txtSoCTUDenSo" runat="server" class="inputflat" Width="89%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" class="form_label">Mã số thuế</td>
                <td class="form_control"><asp:TextBox ID="txtMST" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                 <td align="left" class="form_label">Loại Giao dịch</td>
                <td class="form_control">
                    <asp:DropDownList ID="cboLoaiMSG" runat="server" Width="90%" CssClass="selected inputflat" >
                    <asp:ListItem Selected="True" Value="" Text="Tất cả">                     
                    </asp:ListItem>
                  
                    </asp:DropDownList>
                </td>
               
            </tr>
            
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnTimKiem" CssClass="buttonField" runat="server" Text="Tìm kiếm"  />
                    <asp:Button ID="btnReSend" CssClass="buttonField" runat="server" Text="Gửi lại"  />
                    
                </td>
                
            </tr>
            <tr>
                 <td align="center" colspan="4">
                 <asp:Label ID="lblError" runat="server" ForeColor="Red" ></asp:Label>
                 </td>
            </tr>
        </table>
    </div>
    
    <div class="dataView">
    
    <asp:HiddenField id="ListCT_NoiDia" runat="server" />
     <asp:GridView ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="100%" BorderColor="Black" PageSize="10" AllowPaging="True"  >
            <Columns>
             <asp:TemplateField>
                    <HeaderTemplate>
                    <input type="checkbox" runat="server" ID="HeaderCheckBox" title="Chọn tất" class="JchkAll" />
                   
                    </HeaderTemplate>
                    <ItemTemplate>
                    
                     <input type="checkbox"   class="JchkGrid" value='<%# DataBinder.Eval(Container.DataItem, "ID") %>' form="form" onclick="GetListSoCT()" />
                    </ItemTemplate>
               </asp:TemplateField>
                <asp:BoundField HeaderText="STT" DataField="STT" Visible="true" ReadOnly="true" />
             
              
             
                <asp:BoundField DataField="HQ_SO_CTU" HeaderText="Số chứng từ HQ" 
                    ReadOnly="True" SortExpression="HQ_SO_CTU" />
                <asp:BoundField DataField="SO_CTU" HeaderText="Số chứng từ ETax" 
                    ReadOnly="True" SortExpression="SO_CTU" />
                <asp:BoundField HeaderText="Loại giao dịch" DataField="LOAI_THUE" ReadOnly="True" />
                <asp:BoundField DataField="MA_DV" HeaderText="Mã số thuế" ReadOnly="True" />
                <asp:BoundField DataField="TEN_DV" HeaderText="Tên Đơn vị" ReadOnly="True" />
                <asp:BoundField DataField="TAIKHOAN_TH" HeaderText="Số TK" ReadOnly="True" />
                <asp:BoundField DataField="HQ_NGAY_CT" HeaderText="Ngày Chứng từ" ReadOnly="True" />
                <asp:BoundField DataField="TRANG_THAI" HeaderText="Trạng thái" ReadOnly="True" />
            </Columns>
      
            <HeaderStyle BackColor="#999966" BorderColor="Black" BorderStyle="Solid" 
                BorderWidth="1px" Font-Bold="True" />
            <AlternatingRowStyle BackColor="#EFEDE4" />
      
        </asp:GridView>
       
    </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>		
</asp:Content>
