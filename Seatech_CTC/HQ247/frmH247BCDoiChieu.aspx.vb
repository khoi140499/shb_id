﻿Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports Business.BaoCao
Imports System.Data
Imports Business
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security

Partial Class frmH247BCDoiChieu
    Inherits System.Web.UI.Page
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private mv_strNgay As String

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenDiemThu As String
    Private mv_strTenKB As String
    Private mv_strLoaiBaoCao As String
    Private strMaNT As String

    Private ds As DataSet
    Private frm As infBaoCao
    Private somon As String
    Private str_DateDC_FromDate As String
    Private str_DateDC_ToDate As String
    Private str_FromDate As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session.Item("User") Is Nothing Then
            clsCTU.TCS_SetupParams(CType(Session("User"), Integer), mv_strSHKB, mv_strKyHieuCT, _
                                   mv_strMaDiemThu, mv_strTenDiemThu, mv_strDBThu, mv_strMaNHKB, mv_strTenKB)

        End If
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                'getChiNhanh()
                DM_DiemThu()
                txtNgay.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại

                clsCommon.get_NguyenTe(cboNguyenTe)
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function Get_TenCNNH(ByVal pv_strMaCN As String) As String

        If Not pv_strMaCN Is Nothing Then
            Return clsCTU.TCS_GetMaCN_NH(pv_strMaCN.Substring(0, 3))
        Else
            Return ""
        End If

    End Function
    Private Function checkHSC(ByVal strMa_CN As String) As String
        Dim returnValue = ""
        returnValue = buBaoCao.checkHSC_MaCN(strMa_CN)
        Return returnValue
    End Function
    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        LogApp.AddDebug("cmdIn_Click", "In bao cao doi chieu")
        Try
            'Dim strDonVi As String = cboDonVi.SelectedValue
            Dim strMaCN As String = txtMaCN.SelectedValue.ToString()
            strMaNT = cboNguyenTe.SelectedValue.ToString()
            Dim strMaCN_User = clsCTU.TCS_GetMaChiNhanh(Session("User").ToString)
            Dim strCheckHS As String = ""
            Dim d_DateDC_FromDate As Date = Business_HQ.Common.mdlCommon.ConvertStringToDate(txtNgay.Text, "dd/MM/yyyy")
            str_FromDate = d_DateDC_FromDate.ToString("yyyy-MM-dd")
            d_DateDC_FromDate = d_DateDC_FromDate.AddDays(-1)
            str_DateDC_FromDate = d_DateDC_FromDate.ToString("yyyy-MM-dd")
            str_DateDC_ToDate = Business_HQ.Common.mdlCommon.ConvertStringToDate(txtNgay.Text, "dd/MM/yyyy")
            'strCheckHS = checkHSC(strMaCN_User)
            'If strMaCN = "" And strCheckHS <> "1" Then
            '    strMaCN = strMaCN_User & ";" & strCheckHS
            'End If
            ds = New DataSet
            frm = New infBaoCao
            somon = "0"

            Select Case cboLoaiBaoCao.SelectedValue 'gLoaiBaoCao
                Case "0"
                    clsCommon.ShowMessageBox(Me, "Chưa chọn loại báo cáo!")
                    Exit Sub
                Case "1"
                    DOICHIEU_7()
                Case "2"
                    DOICHIEU_5()

            End Select

            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                'Return
            End If

            frm.TCS_DS = ds
            frm.TCS_Ngay_CT = txtNgay.Text.ToString()
            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../pages/BaoCao/frmShowReports.aspx?BC=" & mv_strLoaiBaoCao, "ManPower")

        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình lấy thông tin báo cáo!")
        Finally
            cmdIn.Enabled = True
        End Try
    End Sub

    

    Private Sub DOICHIEU_5()
        mv_strLoaiBaoCao = Report_Type.HQ247_DOI_CHIEU_2
        'ds = New DALTcs_dchieu_nhhq_hdr().getBCTHopCTTThueXNKhau("M47", "851", str_DateDC_FromDate, str_DateDC_ToDate, strMaCN, strMaNT)
        Dim STRWHERE As String = ""
        Dim STRWHERENHAN As String = ""
        Dim D As DateTime = Business_HQ.Common.mdlCommon.ConvertStringToDate(txtNgay.Text, "dd/MM/yyyy")
        Dim strIDREQuest As String = CustomsServiceV3.ProcessMSG.getReqId(D, "807", "22")
        Dim strTimeStart As String = Business_HQ.Common.mdlCommon.TCS_GETTS("GIO_DC_CT")
        If strTimeStart.Length < 0 Then
            strTimeStart = "160000"
        End If
        '20180222:Sửa vì tham số giờ đối chiếu chỉ có đến phút không có giây
        If strTimeStart.Length < 6 Then
            strTimeStart &= "00"
        End If
        strTimeStart = strTimeStart.Replace(":", "")
        STRWHERE = " AND  to_char( A.NGAYTRUYEN_CT,'RRRRMMDDHH24MISS' )> " & D.AddDays(-1).ToString("yyyyMMdd") & strTimeStart
        STRWHERE &= " AND to_char( A.NGAYTRUYEN_CT,'RRRRMMDDHH24MISS' )<= " & D.ToString("yyyyMMdd") & strTimeStart
        STRWHERENHAN = " AND  A.PARENT_TRANSACTION_ID='" & strIDREQuest & "' AND B.PARENT_TRANSACTION_ID='" & strIDREQuest & "'"
        If txtMaCN.SelectedIndex > 0 Then
            STRWHERENHAN &= " AND D.MA_CN='" & txtMaCN.SelectedValue & "' "
        End If
        ds = Business_HQ.HQ247.daBaoCaoHQ247.TCS_BC_CHITIET_DC(STRWHERENHAN)
    End Sub

   

    Private Sub DOICHIEU_7()
        mv_strLoaiBaoCao = Report_Type.HQ247_DOI_CHIEU_1
        'ds = New DALTcs_dchieu_nhhq_hdr().getBCTHopCTTThueXNKhau("M47", "851", str_DateDC_FromDate, str_DateDC_ToDate, strMaCN, strMaNT)
        Dim STRWHERE As String = ""
        Dim STRWHERENHAN As String = ""
        Dim D As DateTime = Business_HQ.Common.mdlCommon.ConvertStringToDate(txtNgay.Text, "dd/MM/yyyy")
        Dim strIDREQuest As String = CustomsServiceV3.ProcessMSG.getReqId(D, "807", "22")
        Dim strTimeStart As String = Business_HQ.Common.mdlCommon.TCS_GETTS("GIO_DC_CT")
        If strTimeStart.Length < 0 Then
            strTimeStart = "160000"
        End If
        strTimeStart = strTimeStart.Replace(":", "")
        '20180222:Sửa vì tham số giờ đối chiếu chỉ có đến phút không có giây
        If strTimeStart.Length < 6 Then
            strTimeStart &= "00"
        End If
        STRWHERE = " AND  to_char( A.NGAYTRUYEN_CT,'RRRRMMDDHH24MISS' )> " & D.AddDays(-1).ToString("yyyyMMdd") & strTimeStart
        STRWHERE &= " AND to_char( A.NGAYTRUYEN_CT,'RRRRMMDDHH24MISS' )<= " & D.ToString("yyyyMMdd") & strTimeStart
        STRWHERENHAN = " AND  A.PARENT_TRANSACTION_ID='" & strIDREQuest & "'"

        If txtMaCN.SelectedIndex > 0 Then
            STRWHERE &= " AND B.MA_CN='" & txtMaCN.SelectedValue & "'"
        End If
        If txtMaCN.SelectedIndex > 0 Then
            STRWHERENHAN &= " AND B.MA_CN='" & txtMaCN.SelectedValue & "'"
        End If
        ds = Business_HQ.HQ247.daBaoCaoHQ247.TCS_BC_TONG_DC(STRWHERE, STRWHERENHAN)
    End Sub

   
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function
    Private Sub DM_DiemThu()

        Dim dsDiemThu As DataSet
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        If strcheckHSC = "1" Then
            'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
            ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
            strSQL = "SELECT a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a  "
        ElseIf strcheckHSC = "2" Then

            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  (branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "' or a.id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "')"

        Else

            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        End If
        Try
            Dim dt As New DataTable
            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            dt = dsDiemThu.Tables(0)
            txtMaCN.DataSource = dt
            txtMaCN.DataTextField = "name"
            txtMaCN.DataValueField = "id"
            txtMaCN.DataBind()

            If strcheckHSC = "1" Then
                clsCommon.ComboBoxWithValue(txtMaCN, dsDiemThu, 1, 0, "Tất cả")
            End If

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Có lỗi trong quá trình lẫy thông tin chi nhánh", Session.Item("TEN_DN"))
            Throw ex
        Finally

        End Try



    End Sub
   
End Class
