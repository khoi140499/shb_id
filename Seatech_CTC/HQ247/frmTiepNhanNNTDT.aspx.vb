﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.HQ247.NNTDT
Imports Business.Common.mdlCommon
Imports Business.BaoCao
Imports Business.Common
Imports System.IO
Imports System.Xml
Imports System.Text

Partial Class HQ247_frmTiepNhanNNTDT
    Inherits System.Web.UI.Page
    Private Shared cls_corebank As New CorebankServiceESB.clsCoreBank
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then


            If Not Request.Params("ID") Is Nothing Then
                Dim strID As String = Request.Params("ID").ToString
                hdfNNTDT_ID.Value = strID

                Dim ds As DataSet = Business_HQ.HQ247.TCS_DM_NTDT_HQ.getMakerMSG213(strID)
                If ds.Tables(0).Rows.Count > 0 Then
                    hdnCheckBtnCheckTK_Click.Value = "0"
                    txtSo_HS.Text = ds.Tables(0).Rows(0)("So_HS").ToString()
                    txtLoai_HS.Value = ds.Tables(0).Rows(0)("Loai_HS").ToString()
                    txtID.Value = ds.Tables(0).Rows(0)("ID").ToString()
                    txtTenLoai_HS.Text = ds.Tables(0).Rows(0)("TenLoai_HS").ToString()
                    txtMST.Text = ds.Tables(0).Rows(0)("Ma_DV").ToString()
                    txtTen_DV.Text = ds.Tables(0).Rows(0)("Ten_DV").ToString()
                    txtDiaChi.Text = ds.Tables(0).Rows(0)("DiaChi").ToString()
                    txtHo_Ten.Text = ds.Tables(0).Rows(0)("Ho_Ten").ToString()
                    txtSo_CMT.Text = ds.Tables(0).Rows(0)("So_CMT").ToString()
                    txtNgaySinh.Text = ds.Tables(0).Rows(0)("NgaySinh").ToString()
                    txtNguyenQuan.Text = ds.Tables(0).Rows(0)("NguyenQuan").ToString()
                    txtSo_DT.Text = ds.Tables(0).Rows(0)("So_DT").ToString()
                    txtEmail.Text = ds.Tables(0).Rows(0)("Email").ToString()
                    txtSo_DT1.Text = ds.Tables(0).Rows(0)("So_DT1").ToString()
                    txtEmail1.Text = ds.Tables(0).Rows(0)("Email1").ToString()
                    txtSerialNumber.Text = ds.Tables(0).Rows(0)("SerialNumber").ToString()
                    txtNoi_Cap.Text = ds.Tables(0).Rows(0)("NoiCap").ToString()
                    txtNgay_HL.Text = ds.Tables(0).Rows(0)("Ngay_HL").ToString()
                    txtNgay_HHL.Text = ds.Tables(0).Rows(0)("Ngay_HHL").ToString()
                    txtMa_NH_TH.Text = ds.Tables(0).Rows(0)("Ma_NH_TH").ToString()
                    txtTen_NH_TH.Text = ds.Tables(0).Rows(0)("Ten_NH_TH").ToString()


                    txtTaiKhoan_TH.Text = ds.Tables(0).Rows(0)("TaiKhoan_TH").ToString()
                    txtTen_TaiKhoan_TH.Text = ds.Tables(0).Rows(0)("Ten_TaiKhoan_TH").ToString()
                    txtNGAY_NHAN311.Text = ds.Tables(0).Rows(0)("NGAY_NHAN311").ToString()
                    txtCUR_TAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("CUR_TAIKHOAN_TH").ToString()
                    txtCUR_TEN_TAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("CUR_TEN_TAIKHOAN_TH").ToString()
                    txtCHK_TAIKHOAN_TH.Text = txtTaiKhoan_TH.Text
                    txtNoiDung_XL.Text = ds.Tables(0).Rows(0)("LY_DO_HUY").ToString()
                    Dim strTMP As String = "<data>" & ds.Tables(0).Rows(0)("THONGTINLIENHE").ToString() & "</data>"
                    If strTMP.Length > 0 Then
                        Dim tbl As DataTable = New DataTable()
                        tbl.Columns.Add("STT")
                        tbl.Columns.Add("So_DT")
                        tbl.Columns.Add("Email")
                        Dim xmldoc As XmlDocument = New XmlDocument()
                        xmldoc.LoadXml(strTMP)
                        Dim x As Integer = 0
                        For Each vnode As XmlNode In xmldoc.SelectNodes("data/ThongTinLienHe")
                            Dim vrow As DataRow = tbl.NewRow()
                            For i As Integer = 0 To vnode.ChildNodes.Count - 1
                                vrow(vnode.ChildNodes.Item(i).Name) = vnode.ChildNodes.Item(i).InnerText
                            Next
                            x = x + 1
                            vrow("STT") = x
                            tbl.Rows.Add(vrow)
                        Next
                        dtgrd.DataSource = tbl
                        dtgrd.DataBind()
                    End If






                    Select Case ds.Tables(0).Rows(0)("TRANGTHAI").ToString()
                        Case "01" 'Mới nhận yêu cầu từ TCHQ
                            btnCheckTK.Enabled = True
                            btnCheckCK.Enabled = False
                            btnChuyenKS.Enabled = True
                            btnHuy.Enabled = True
                            '
                            txtNoiDung_XL.ReadOnly = False
                        Case "08" 'Chuyển trả yêu cầu
                            btnCheckTK.Enabled = True
                            btnCheckCK.Enabled = False
                            btnChuyenKS.Enabled = True
                            btnHuy.Enabled = True
                            '
                            txtNoiDung_XL.ReadOnly = False
                        Case Else
                            btnCheckTK.Enabled = False
                            btnCheckCK.Enabled = False
                            btnChuyenKS.Enabled = False
                            btnHuy.Enabled = False
                    End Select
                End If

            End If
        End If

    End Sub
    Protected Sub btnCheckTK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckTK.Click
        If txtCHK_TAIKHOAN_TH.Text = "" Then
            txtCHK_TAIKHOAN_TH.Focus()
            clsCommon.ShowMessageBox(Me, "Số tài khoản ngân hàng không được để trống")
            Exit Sub
        End If
        Try
            'Dim ten_tk As String
            'LogDebug.WriteLog("vào ws", Diagnostics.EventLogEntryType.Information)
            'Dim core As New clsCoreBankUtilDAB
            'LogDebug.WriteLog("12", Diagnostics.EventLogEntryType.Information)
            'ten_tk = clsCoreBankUtilDAB.GetCustomerByAccount(txtSoTKNH.Text.Trim)
            'LogDebug.WriteLog(ten_tk, Diagnostics.EventLogEntryType.Information)
            'Test Core Off
            'huynt_test
            hdnCheckBtnCheckTK_Click.Value = "1"
            If ConfigurationManager.AppSettings("CORE.ON").ToString().Equals("0") Then
                txtCHK_TEN_TAIKHOAN_TH.Text = txtTen_TaiKhoan_TH.Text.ToUpper()

                Exit Sub
            Else
                Dim strReturn As String = ""
                Dim strMaLoi As String = ""
                Dim strSodu As String = ""
                Try
                    'Thực hiện lấy tên tài khoản của NNT từ Core
                    'strReturn = cls_corebank.GetTK_KH_NH(txtCHK_TAIKHOAN_TH.Text.Replace("-", ""), strMaLoi, strSodu)
                    strReturn = cls_corebank.GetTen_TK_KH_NH(txtCHK_TAIKHOAN_TH.Text.Replace("-", ""))
                    If strReturn = "" Then
                        clsCommon.ShowMessageBox(Me, "Không lấy được thông tin tài khoản của NNT")
                    Else
                        txtCHK_TEN_TAIKHOAN_TH.Text = strReturn
                    End If
                Catch ex As Exception
                    clsCommon.ShowMessageBox(Me, "Lỗi CoreBank, Không lấy được thông tin tài khoản của NNT")
                End Try
            End If
            If txtTen_TaiKhoan_TH.Text.ToUpper().Equals(txtCHK_TEN_TAIKHOAN_TH.Text.ToUpper()) Then
                btnChuyenKS.Visible = False
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được tên khách hàng. Lỗi: " & ex.Message)
        End Try
    End Sub
    Protected Sub btnChuyenKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChuyenKS.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        'Trường hợp Tiếp nhận Đăng Ký thuộc loại Khai Hủy
        If txtLoai_HS.Value = "3" Then
            'Kiểm tra xem có chứng từ nào đang trong quá trình xử lý dở hay không
            Dim checkReceipt As Boolean = True
            'Giả sử tồn tại chứng từ đang trong quá trình xử lý
            If Business_HQ.HQ247.TCS_DM_NTDT_HQ.CheckReceipts(checkReceipt, txtSo_HS.Text.Trim(), "") = 0 Then
                If Not checkReceipt Then
                    'Không có chứng từ nào đang trong quá trình xử lý => đủ điều kiện hủy đăng ký
                    Dim decOk As Decimal = Business_HQ.HQ247.TCS_DM_NTDT_HQ.MakerMSG213(hdfNNTDT_ID.Value, txtNoiDung_XL.Text, "1", Session.Item("UserName").ToString)
                    If decOk = Business_HQ.Common.mdlCommon.TCS_HQ247_OK Then
                        clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát duyệt yêu cầu thành công !")
                        btnChuyenKS.Visible = False
                        btnHuy.Visible = False
                    Else
                        clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi:  " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decOk.ToString()))
                        btnChuyenKS.Visible = True
                        btnHuy.Visible = True
                    End If
                Else
                    'Thông báo tồn tại chứng từ đang trong quá trình xử lý
                End If
            Else
                'Hàm Business_HQ.HQ247.TCS_DM_NTDT_HQ.CheckReceipts Lỗi truy vấn
            End If

        End If
        'Trường hợp Tiếp nhận Đăng Ký thuộc loại Khai Mới
        If txtLoai_HS.Value = "1" Then
            If txtCHK_TAIKHOAN_TH.Text = "" Then
                txtCHK_TAIKHOAN_TH.BorderColor = Drawing.Color.DarkOrange
                txtCHK_TAIKHOAN_TH.Focus()
                clsCommon.ShowMessageBox(Me, "Ô Số tài khoản ngân hàng không được để trống !")
                Exit Sub
            End If
            Try
                If hdnCheckBtnCheckTK_Click.Value = "0" Then
                    clsCommon.ShowMessageBox(Me, "Chưa bấm nút Check TK !")
                    txtCHK_TAIKHOAN_TH.BorderColor = Drawing.Color.DarkOrange
                    btnCheckTK.BorderColor = Drawing.Color.DarkOrange
                    txtCHK_TAIKHOAN_TH.Focus()
                    Exit Sub
                End If

                If txtCHK_TEN_TAIKHOAN_TH.Text.ToUpper().Trim() <> txtTen_TaiKhoan_TH.Text.ToUpper().Trim() Then
                    clsCommon.ShowMessageBox(Me, "Tên tài khoản đăng ký tại TCHQ không khớp với Tên tài khoản tại NH !")
                    Exit Sub
                End If

                If txtNoiDung_XL.Text.Trim().Length <= 0 Then
                    clsCommon.ShowMessageBox(Me, "Chưa nhập nội dung hướng dẫn NNT ! ")
                    txtNoiDung_XL.BorderColor = Drawing.Color.DarkOrange
                    txtNoiDung_XL.Focus()
                    Exit Sub
                End If


                Dim decOk As Decimal = Business_HQ.HQ247.TCS_DM_NTDT_HQ.MakerMSG213(hdfNNTDT_ID.Value, txtNoiDung_XL.Text, "1", Session.Item("UserName").ToString)
                If decOk = Business_HQ.Common.mdlCommon.TCS_HQ247_OK Then
                    clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát thành công. Đã gửi thông tin tới KSV")
                    btnChuyenKS.Visible = False
                    btnHuy.Visible = False
                Else
                    clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi:  " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decOk.ToString()))
                    btnChuyenKS.Visible = True
                    btnHuy.Visible = True

                End If
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi: " & ex.Message)
            End Try
        End If
        If txtLoai_HS.Value = "2" Then
            If txtCHK_TAIKHOAN_TH.Text = "" Then
                txtCHK_TAIKHOAN_TH.BorderColor = Drawing.Color.DarkOrange
                txtCHK_TAIKHOAN_TH.Focus()
                clsCommon.ShowMessageBox(Me, "Ô Số tài khoản ngân hàng không được để trống !")
                Exit Sub
            End If
            Try
                If hdnCheckBtnCheckTK_Click.Value = "0" Then
                    clsCommon.ShowMessageBox(Me, "Chưa bấm nút Check TK !")
                    txtCHK_TAIKHOAN_TH.BorderColor = Drawing.Color.DarkOrange
                    btnCheckTK.BorderColor = Drawing.Color.DarkOrange
                    txtCHK_TAIKHOAN_TH.Focus()
                    Exit Sub
                End If

                If txtCHK_TEN_TAIKHOAN_TH.Text.ToUpper().Trim() <> txtTen_TaiKhoan_TH.Text.ToUpper().Trim() Then
                    clsCommon.ShowMessageBox(Me, "Tên tài khoản đăng ký tại TCHQ không khớp với Tên tài khoản tại NH !")
                    Exit Sub
                End If

                If txtNoiDung_XL.Text.Trim().Length <= 0 Then
                    clsCommon.ShowMessageBox(Me, "Chưa nhập nội dung hướng dẫn NNT ! ")
                    txtNoiDung_XL.BorderColor = Drawing.Color.DarkOrange
                    txtNoiDung_XL.Focus()
                    Exit Sub
                End If


                Dim decOk As Decimal = Business_HQ.HQ247.TCS_DM_NTDT_HQ.MakerMSG213(hdfNNTDT_ID.Value, txtNoiDung_XL.Text, "1", Session.Item("UserName").ToString)
                If decOk = Business_HQ.Common.mdlCommon.TCS_HQ247_OK Then
                    clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát thành công. Đã gửi thông tin tới KSV")
                    btnChuyenKS.Visible = False
                    btnHuy.Visible = False
                Else
                    clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi:  " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decOk.ToString()))
                    btnChuyenKS.Visible = True
                    btnHuy.Visible = True

                End If
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi: " & ex.Message)
            End Try
        End If
    End Sub
    Protected Sub btnHuy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHuy.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        Try
            If txtNoiDung_XL.Text.Trim().Length <= 0 Then
                clsCommon.ShowMessageBox(Me, "Phải nhập ô Nội dung hướng dẫn NNT trước khi Hủy phiếu ! ")
                txtNoiDung_XL.BorderColor = Drawing.Color.DarkOrange
                txtNoiDung_XL.Focus()
                Exit Sub
            End If

            Dim decOk As Decimal = Business_HQ.HQ247.TCS_DM_NTDT_HQ.MakerMSG213(hdfNNTDT_ID.Value, txtNoiDung_XL.Text, "2", Session.Item("UserName").ToString)
            If decOk = Business_HQ.Common.mdlCommon.TCS_HQ247_OK Then
                clsCommon.ShowMessageBox(Me, "Yêu cầu Hủy phiếu đã gửi tới Kiểm soát viên thành công !")
                btnChuyenKS.Visible = False
                btnHuy.Visible = False
            Else
                clsCommon.ShowMessageBox(Me, "Yêu cầu Hủy phiếu lỗi:  " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decOk.ToString()))
                btnChuyenKS.Visible = True
                btnHuy.Visible = True
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Yêu cầu Hủy phiếu lỗi: " & ex.Message)
        End Try
    End Sub

    Protected Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Response.Redirect("~/pages/frmHomeNV.aspx", False)
    End Sub
End Class
