﻿Imports System.Data
Imports Business.NTDT.NNTDT

Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao
Imports log4net
Imports Business_HQ.Common.mdlCommon

Partial Class HQ247_frmTraCuuCTTT
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If

        If Not IsPostBack Then
            Load_TrangThai()
            Load_Loai_HS()
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub
    Private Sub load_dataGrid()
        Dim ds As DataSet

        'Dim WhereClause As String = " AND A.TRANGTHAI='" & Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_SEND301_ERROR & "' "
        'tam thoi ko choi ma CN
        ' Dim WhereClause As String = " AND MA_CN='" & Session.Item("MA_CN_USER_LOGON").ToString() & "' AND A.LOAIMSG='304'  "
        Dim WhereClause As String = "  AND A.LOAIMSG='304'  "
        Try
            dtgrd.DataSource = Nothing
            dtgrd.DataBind()
            If ConvertDateToNumber(txtTuNgay.Value) > ConvertDateToNumber(txtDenNgay.Value) And txtDenNgay.Value.ToString.Length > 0 Then
                clsCommon.ShowMessageBox(Me, "Điều kiện Từ ngày phải nhỏ hơn hoặc bằng Đến ngày")
                Exit Sub
            End If
            If txtMST.Text <> "" Then
                WhereClause += " AND upper(a.MA_DV) like upper('%" & txtMST.Text.Trim & "%')"
            End If
            If txtTen_NNT.Text <> "" Then
                WhereClause += " AND upper(a.TEN_DV) like upper('%" & txtTen_NNT.Text.Trim & "%')"
            End If
            If txtSoTK.Text <> "" Then
                WhereClause += " AND upper(a.TAIKHOAN_TH) like upper('%" & txtSoTK.Text.Trim & "%')"
            End If
            If txtTuNgay.Value <> "" Then
                WhereClause += " AND to_char(a.NGAYNHANMSG,'RRRRMMDD') >=" & ConvertDateToNumber(txtTuNgay.Value)
            End If
            If txtDenNgay.Value <> "" Then
                WhereClause += " AND to_char(a.NGAYNHANMSG,'RRRRMMDD') <= " & ConvertDateToNumber(txtDenNgay.Value)
            End If
            If txtSoCTUDenSo.Text.Length > 0 Then
                WhereClause += " AND HQ_SO_CTU <= " & txtSoCTUDenSo.Text.Trim() & " "
            End If
            If txtSoCTUTuSo.Text.Length > 0 Then
                WhereClause += " AND HQ_SO_CTU >= " & txtSoCTUTuSo.Text.Trim() & " "
            End If
            If cboTrangThai.SelectedIndex > 0 Then
                WhereClause += " AND a.TRANGTHAI = '" & cboTrangThai.SelectedValue.ToString() & "' "
            End If

            'Nhamlt 20180123: Thêm trường số ref trên màn hình tìm kiếm. Số ref này chính là số CT NH sau khi hạch toán thành công tại NH thì core trả ra thông tin.
            If txtSoRef.Text.Length > 0 Then
                WhereClause += " AND SO_CT_NH = '" & txtSoRef.Text.Trim() & "' "
            End If

            ds = Business_HQ.HQ247.daCTUHQ304.getMSG304_Tracuu(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                ' dtgrd.CurrentPageIndex = 0

                dtgrd.DataSource = ds.Tables(0)
                'dtgrd.DataBind()
                dtgrd.PageIndex = 0
                dtgrd.DataBind()

                grdExport.DataSource = ds.Tables(0)
                grdExport.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub
    Private Sub Load_TrangThai()
        Try

            Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_TT_HQ247  WHERE TRANGTHAI NOT IN ('08','09')"
            Dim dr As DataSet = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            cboTrangThai.DataSource = dr.Tables(0)
            cboTrangThai.DataTextField = "MOTA"
            cboTrangThai.DataValueField = "TRANGTHAI"
            cboTrangThai.DataBind()
            cboTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái yêu cầu. Lỗi: " & ex.Message)
        End Try
    End Sub

    Private Sub Load_Loai_HS()
        Try
           


        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách loại hồ sơ người nộp thuế. Lỗi: " & ex.Message)
        End Try
    End Sub



    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        load_dataGrid()

    End Sub

    Protected Sub btnReSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReSend.Click
        Try
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If ListCT_NoiDia.Value.Length <= 0 Then
                clsCommon.ShowMessageBox(Me, "Chưa chọn chứng từ để gửi lại! ")
                Return
            End If
            Dim soCTULoi As Integer = 0
            'lblError.Text = ListCT_NoiDia.Value

            Dim strListID As String() = ListCT_NoiDia.Value.Split(",")
            For i As Integer = 0 To strListID.Length - 1
                soCTULoi += ReSENDHQ(strListID(i))
            Next

            clsCommon.ShowMessageBox(Me, "Gửi lại thành công " & (strListID.Length - soCTULoi) & " / " & strListID.Length.ToString() & " chứng từ!")
            load_dataGrid()

        Catch ex As Exception
            lblError.Text = "Loi " & ex.Message
        End Try



    End Sub
    Protected Function ReSENDHQ(ByVal strID As String) As Integer
        Try
            Dim strErrorNum As String = "0"
            Dim strErrorMes As String = ""
            Dim v_strTransaction_id As String = ""
            Dim pv_so_tn_ct As String = ""
            Dim pv_ngay_tn_ct As String = ""
            Dim ds As DataSet = Business_HQ.HQ247.daCTUHQ304.getMSG304Detail(strID)
            If ds Is Nothing Then
                Return 1
            End If
            If ds.Tables.Count <= 0 Then
                Return 1
            End If
            If ds.Tables(0).Rows.Count <= 0 Then
                Return 1
            End If
            Dim strMST As String = ds.Tables(0).Rows(0)("MA_DV").ToString()
            Dim strSO_CTU As String = ds.Tables(0).Rows(0)("SO_CTU").ToString()
            CustomsServiceV3.ProcessMSG.guiCTThueHQ247(strMST, strSO_CTU, strErrorNum, strErrorMes, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct)
            If Not strErrorNum.Equals("0") Then
                Business_HQ.HQ247.daCTUHQ304.TCS_HQ247_TRANGTHAI_304_SEND301_ERROR(strID, Session.Item("UserName").ToString())
                Return 1
            End If
            'doan nay cho doi chieu
            Dim decResult As Decimal = Business_HQ.HQ247.daCTUHQ304.TCS_HQ247_TRANGTHAI_304_SEND301_OK(strID, v_strTransaction_id, pv_so_tn_ct, pv_ngay_tn_ct, Session.Item("UserName").ToString())
            If decResult > 0 Then
                Return 1
            End If
            Return 0
        Catch ex As Exception
            Return 1
        End Try
        Return 1
    End Function
    Protected Sub dtgrd_PageIndexChanging(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dtgrd.PageIndexChanging
        load_dataGrid()
        dtgrd.PageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        grdExport.Visible = True
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=ExportNNTDT.xls")
        Response.Charset = "utf-8"
        Response.ContentType = "application/vnd.ms-excel"
        Using sw1 As New StringWriter()
            Dim hw1 As New HtmlTextWriter(sw1)
            grdExport.RenderControl(hw1)
            Dim style As String = "<style> .textmode { } </style>"
            Response.Write(style)
            Response.Output.Write(sw1.ToString)
            Response.Flush()
            Response.End()
        End Using
        grdExport.Visible = False
    End Sub
End Class
