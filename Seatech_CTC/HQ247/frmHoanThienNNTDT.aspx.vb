﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.HQ247.NNTDT
Imports Business.Common.mdlCommon
Imports Business.BaoCao
Imports Business.Common
Imports System.IO
Imports System.Xml
Imports CorebankServiceESB

''' <summary>
''' Hoàn thiện thông tin đăng ký của NNT
''' Chức năng Tạo mới: Chỉ dùng trong trường hợp NNT đăng kí thông tin ủy quyền trích nợ tài khoản tại NHTM 
''' Chức năng Ghi và chuyển KS: sử dụng đối với những đăng ký ở trạng thái ĐÃ DUYỆT YÊU CẦU
''' </summary>
''' <remarks>Số tài khoản phải được kiểm tra và lấy được Tên tài khoản</remarks>
Partial Class HQ247_frmHoanThienNNTDT
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        txtMA_DV.Enabled = False
        txtMA_DV.Enabled = True
        txtTEN_DV.Enabled = False
        txtDIACHI.Enabled = False
        txtHO_TEN.Enabled = False
        txtSO_CMT.Enabled = False
        txtNGAYSINH.Enabled = False
        txtNGUYENQUAN.Enabled = False
        txtSO_DT.Enabled = False
        txtEMAIL.Enabled = False
        txtMA_NH_TH.Enabled = False
        txtTEN_NH_TH.Enabled = False
        txtNoiDung_XL.Enabled = False
        txtCHK_TAIKHOAN_TH.Enabled = True 'False
        btnCheckCK.Enabled = False
        txtMA_NH_TH.Enabled = False
        txtTEN_NH_TH.Enabled = False
        btnLuuSoDT.Visible = True
        btnLuuEmail.Visible = True

        'Kết nối với BankCore để lấy ra thông tin Tên tài khoản

        'Kết nối với BankCore để lấy ra tên ngân hàng thụ hưởng và mã ngân hàng thụ hưởng hoặc để mặc định
        'huynt_test

        '
        If Not IsPostBack Then
            If Not Request.Params("ID") Is Nothing Then
                'Session.Add("THONGTINLIENHE", "")
                Dim strID As String = Request.Params("ID").ToString
                hdfNNTDT_ID.Value = strID
                dieuChinhConfirm.Value = "Cancel"
                btnLuuSoDT.Visible = False
                btnLuuEmail.Visible = False

                Dim ds As DataSet = Business_HQ.HQ247.TCS_DM_NTDT_HQ.getMakerMSG213(strID)
                'Nếu không tìm thấy hồ sơ trong bảng HQ_311 thì tìm tiếp trong bảng HQ
                If ds.Tables(0).Rows.Count <= 0 Then
                    ds = Business_HQ.HQ247.TCS_DM_NTDT_HQ.getDangKyNNT_HQ(strID)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("04") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("10") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("11") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("12") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("13") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("14") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("15") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("16") Or ds.Tables(0).Rows(0)("TRANGTHAI").ToString.Equals("09") Then
                        txtSO_HS.Text = ds.Tables(0).Rows(0)("So_HS").ToString()
                        txtLoai_HS.Value = ds.Tables(0).Rows(0)("Loai_HS").ToString()
                        txtID.Value = ds.Tables(0).Rows(0)("ID").ToString()
                        txtTenLoai_HS.Text = ds.Tables(0).Rows(0)("TenLoai_HS").ToString()
                        txtMA_DV.Text = ds.Tables(0).Rows(0)("Ma_DV").ToString()
                        txtTEN_DV.Text = ds.Tables(0).Rows(0)("Ten_DV").ToString()
                        txtDIACHI.Text = ds.Tables(0).Rows(0)("DiaChi").ToString()
                        txtHO_TEN.Text = ds.Tables(0).Rows(0)("Ho_Ten").ToString()
                        txtSO_CMT.Text = ds.Tables(0).Rows(0)("So_CMT").ToString()
                        txtNGAYSINH.Text = ds.Tables(0).Rows(0)("NgaySinh").ToString()
                        txtNGUYENQUAN.Text = ds.Tables(0).Rows(0)("NguyenQuan").ToString()
                        txtSO_DT.Text = ds.Tables(0).Rows(0)("So_DT").ToString()
                        txtEMAIL.Text = ds.Tables(0).Rows(0)("Email").ToString()
                        txtSERIALNUMBER.Text = ds.Tables(0).Rows(0)("SerialNumber").ToString()
                        txtNOICAP.Text = ds.Tables(0).Rows(0)("NOICAP").ToString()
                        txtNGAY_HL.Text = ds.Tables(0).Rows(0)("Ngay_HL").ToString()
                        txtNGAY_HHL.Text = ds.Tables(0).Rows(0)("Ngay_HHL").ToString()
                        txtMA_NH_TH.Text = ds.Tables(0).Rows(0)("Ma_NH_TH").ToString()
                        txtTEN_NH_TH.Text = ds.Tables(0).Rows(0)("Ten_NH_TH").ToString()
                        txtTAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("TaiKhoan_TH").ToString()
                        txtTEN_TAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("Ten_TaiKhoan_TH").ToString()
                        txtNGAY_NHAN311.Text = ds.Tables(0).Rows(0)("NGAY_NHAN311").ToString()
                        txtNoiDung_XL.Text = ds.Tables(0).Rows(0)("LY_DO_HUY").ToString()
                        txtLY_DO_CHUYEN_TRA.Text = ds.Tables(0).Rows(0)("LY_DO_CHUYEN_TRA").ToString()
                        txtCHK_TAIKHOAN_TH.Text = txtTAIKHOAN_TH.Text
                        txtCHK_TEN_TAIKHOAN_TH.Text = txtTEN_TAIKHOAN_TH.Text
                        If ds.Tables(0).Rows(0)("TRANGTHAI").ToString().Equals("10") Then
                            txtCHK_TAIKHOAN_TH.Text = ds.Tables(0).Rows(0)("Cur_TaiKhoan_TH").ToString()
                            txtCHK_TEN_TAIKHOAN_TH.Text = ""
                        End If
                        'Ngày hiệu lực được thay bằng ngày hiệu lực trên biên bản đã ký với NNT
                        txtNGAY_KY_HDUQ.Text = DateTime.Now.ToString("dd/MM/yyyy")
                        'Truong hop ho so da duoc ky ngay hieu luc truoc do roi thi load ngay hieu kuc do len
                        If Not ds.Tables(0).Rows(0)("NGAY_KY_HDUQ").ToString() Is Nothing And ds.Tables(0).Rows(0)("NGAY_KY_HDUQ").ToString().Length > 0 Then
                            txtNGAY_KY_HDUQ.Text = ds.Tables(0).Rows(0)("NGAY_KY_HDUQ").ToString()
                        End If

                        txtNoiDung_XL.ReadOnly = True
                        txtLY_DO_CHUYEN_TRA.Enabled = False
                        txtLY_DO_CHUYEN_TRA.ReadOnly = False
                        btnTaoMoi.Enabled = False

                        Dim strTMP As String = "<data>" & ds.Tables(0).Rows(0)("THONGTINLIENHE").ToString() & "</data>"
                        If strTMP.Length > 0 Then
                            Dim strThongtin As String = ""
                            Dim xmldoc As XmlDocument = New XmlDocument()
                            xmldoc.LoadXml(strTMP)
                            Dim x As Integer = 0
                            For Each vnode As XmlNode In xmldoc.SelectNodes("data/ThongTinLienHe")
                                x = x + 1
                                If strThongtin.Length > 0 Then
                                    strThongtin &= "#"
                                End If
                                strThongtin &= x
                                For i As Integer = 0 To vnode.ChildNodes.Count - 1
                                    strThongtin &= "|" & vnode.ChildNodes.Item(i).InnerText
                                Next


                            Next
                            Session.Add("THONGTINLIENHE", strThongtin)
                            txtThongTin.Value = strThongtin
                        End If
                        Select Case ds.Tables(0).Rows(0)("TRANGTHAI").ToString()
                            Case "04" 'Đã duyệt yêu cầu
                                btnCheckTK.Enabled = True
                                btnCheckCK.Enabled = False
                                btnTaoMoi.Enabled = True
                                btnChuyenKS.Enabled = True
                                btnDieuChinh.Enabled = False


                                '
                                txtNGAY_KY_HDUQ.ReadOnly = False
                                txtCHK_TAIKHOAN_TH.ReadOnly = False
                                txtCHK_TAIKHOAN_TH.Enabled = True
                                txtNGAY_KY_HDUQ.Enabled = True
                                txtstrdisabled.Value = "disabled"
                                btnHuyDK.Enabled = False
                            Case "09" 'Chờ kiểm soát
                                'btnCheckTK.Enabled = False
                                'btnLuu.Enabled = False
                                'btnTaoMoi.Enabled = True
                                'btnChuyenKS.Enabled = False
                                'btnDieuChinh.Enabled = False
                                'btnExit.Enabled = True
                                btnCheckTK.Enabled = False
                                btnCheckCK.Enabled = False
                                btnTaoMoi.Enabled = True
                                btnChuyenKS.Enabled = False
                                btnDieuChinh.Enabled = False
                                txtstrdisabled.Value = "disabled"
                                btnHuyDK.Enabled = False
                                '
                            Case "10" 'Chuyển trả

                                btnCheckTK.Enabled = True
                                btnCheckCK.Enabled = False
                                btnTaoMoi.Enabled = True
                                btnChuyenKS.Enabled = True
                                btnDieuChinh.Enabled = False
                                btnHuyDK.Enabled = False

                                txtNGAY_HL.ReadOnly = False
                                'Nhamltl-----------------Cho phép nhập các trường
                                Dim soHS As String = ds.Tables(0).Rows(0)("So_HS").ToString()
                                Dim loaiHS As String = ds.Tables(0).Rows(0)("Loai_HS").ToString()

                                txtNGAY_KY_HDUQ.ReadOnly = False
                                txtNGAY_KY_HDUQ.Enabled = True
                                txtCHK_TAIKHOAN_TH.ReadOnly = False
                                txtCHK_TAIKHOAN_TH.Enabled = True
                                txtstrdisabled.Value = "disabled"

                                '1.Chuyển trả của đăng ký mới đầu NHTM
                                If loaiHS = "1" And soHS.Length <= 0 Then
                                    txtMA_DV.ReadOnly = False
                                    txtMA_DV.Enabled = True
                                    txtTEN_DV.ReadOnly = False
                                    txtTEN_DV.Enabled = True
                                    txtDIACHI.ReadOnly = False
                                    txtDIACHI.Enabled = True
                                    txtHO_TEN.ReadOnly = False
                                    txtHO_TEN.Enabled = True
                                    txtSO_CMT.ReadOnly = False
                                    txtSO_CMT.Enabled = True
                                    txtNGAYSINH.ReadOnly = False
                                    txtNGAYSINH.Enabled = True
                                    txtNGUYENQUAN.ReadOnly = False
                                    txtNGUYENQUAN.Enabled = True
                                    txtNGAY_KY_HDUQ.ReadOnly = False
                                    txtNGAY_KY_HDUQ.Enabled = True
                                    txtCHK_TAIKHOAN_TH.ReadOnly = False
                                    txtCHK_TAIKHOAN_TH.Enabled = True
                                    '----------Thông tin đăng ký: số điện thoại , email cho phép sửa
                                    txtstrdisabled.Value = ""
                                End If
                                '2. Chuyển trả của đăng ký 'Hủy' đầu NHTM
                                If loaiHS = "3" Then
                                    txtCHK_TAIKHOAN_TH.ReadOnly = True
                                    txtCHK_TAIKHOAN_TH.Enabled = False
                                    btnCheckTK.Enabled = False
                                    txtstrdisabled.Value = "disabled"
                                End If

                            Case "11" 'Đã kiểm soát - Chờ xác nhận từ TCHQ
                                'btnCheckTK.Enabled = False
                                'btnLuu.Enabled = False
                                'btnTaoMoi.Enabled = True
                                'btnChuyenKS.Enabled = False
                                'btnDieuChinh.Enabled = False
                                'btnExit.Enabled = True
                                btnCheckTK.Enabled = False
                                btnCheckCK.Enabled = False
                                btnTaoMoi.Enabled = True
                                btnChuyenKS.Enabled = False
                                btnDieuChinh.Enabled = False
                                txtstrdisabled.Value = "disabled"
                                '
                            Case "12" 'Đã kiểm soát – Chờ bổ sung TT NNT
                                'btnCheckTK.Enabled = False
                                'btnLuu.Enabled = False
                                'btnTaoMoi.Enabled = True
                                'btnChuyenKS.Enabled = False
                                'btnDieuChinh.Enabled = False
                                'btnExit.Enabled = True
                                btnCheckTK.Enabled = False
                                btnCheckCK.Enabled = False
                                btnTaoMoi.Enabled = True
                                btnChuyenKS.Enabled = False
                                btnDieuChinh.Enabled = False
                                txtstrdisabled.Value = "disabled"
                                btnHuyDK.Enabled = False
                                '
                            Case "13" 'Thành công
                                'btnCheckTK.Enabled = False
                                'btnCheckCK.Enabled = False
                                'btnLuu.Enabled = False
                                'btnTaoMoi.Enabled = True
                                'btnChuyenKS.Enabled = False
                                'btnDieuChinh.Enabled = True
                                'btnExit.Enabled = True
                                'btnLuuEmail.Enabled = False
                                'btnLuuSoDT.Enabled = False
                                'btnTaoMoi.Enabled = False
                                btnCheckTK.Enabled = False
                                btnCheckCK.Enabled = False
                                btnTaoMoi.Enabled = True
                                btnChuyenKS.Enabled = False
                                btnDieuChinh.Enabled = True
                                '
                                txtstrdisabled.Value = "disabled"
                                btnHuyDK.Enabled = True

                            Case "14" 'Điều chỉnh
                                'btnCheckTK.Enabled = True
                                'btnLuu.Enabled = False
                                'btnTaoMoi.Enabled = True
                                'btnChuyenKS.Enabled = True
                                'btnDieuChinh.Enabled = False
                                'btnExit.Enabled = True
                                btnCheckTK.Enabled = True
                                btnCheckCK.Enabled = False
                                btnTaoMoi.Enabled = True
                                btnChuyenKS.Enabled = True
                                btnDieuChinh.Enabled = False
                                '
                                txtNGAY_HL.Enabled = True
                                txtCHK_TAIKHOAN_TH.Enabled = True
                                txtstrdisabled.Value = ""
                                btnHuyDK.Enabled = False
                            Case "15" 'Ngừng đăng ký
                                'btnCheckTK.Enabled = False
                                'btnLuu.Enabled = False
                                'btnTaoMoi.Enabled = True
                                'btnChuyenKS.Enabled = False
                                'btnDieuChinh.Enabled = False
                                'btnExit.Enabled = True
                                btnCheckTK.Enabled = False
                                btnCheckCK.Enabled = False
                                btnTaoMoi.Enabled = True
                                btnChuyenKS.Enabled = False
                                btnDieuChinh.Enabled = False
                                txtstrdisabled.Value = "disabled"
                                btnHuyDK.Enabled = False
                            Case "16" 'Ngừng đăng ký
                                btnCheckTK.Enabled = False
                                btnCheckCK.Enabled = False
                                btnTaoMoi.Enabled = True
                                btnChuyenKS.Enabled = False
                                btnDieuChinh.Enabled = False
                                txtstrdisabled.Value = "disabled"
                                btnHuyDK.Enabled = False
                            Case Else
                        End Select
                    End If
                End If
            Else
                txtMA_NH_TH.Text = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString()
                txtTEN_NH_TH.Text = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString()
                txtMA_DV.Enabled = True
                txtMA_DV.Enabled = True
                txtTEN_DV.Enabled = True
                txtDIACHI.Enabled = True
                txtHO_TEN.Enabled = True
                txtSO_CMT.Enabled = True
                txtNGAYSINH.Enabled = True
                txtNGUYENQUAN.Enabled = True
                txtSO_DT.Enabled = True
                txtEMAIL.Enabled = True
                txtMA_NH_TH.Enabled = False
                txtTEN_NH_TH.Enabled = False
                txtNoiDung_XL.Enabled = False

                txtCHK_TAIKHOAN_TH.Enabled = True
                btnCheckCK.Enabled = False
                txtMA_NH_TH.Enabled = False
                txtTEN_NH_TH.Enabled = False
                btnLuuSoDT.Visible = True
                btnLuuEmail.Visible = True
            End If

        End If
        If hdfNNTDT_ID.Value.Length <= 0 Then
            txtMA_NH_TH.Text = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString()
            txtTEN_NH_TH.Text = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString()
            txtMA_DV.Enabled = True
            txtMA_DV.Enabled = True
            txtTEN_DV.Enabled = True
            txtDIACHI.Enabled = True
            txtHO_TEN.Enabled = True
            txtSO_CMT.Enabled = True
            txtNGAYSINH.Enabled = True
            txtNGUYENQUAN.Enabled = True
            txtSO_DT.Enabled = True
            txtEMAIL.Enabled = True
            txtMA_NH_TH.Enabled = False
            txtTEN_NH_TH.Enabled = False
            txtNoiDung_XL.Enabled = False

            txtCHK_TAIKHOAN_TH.Enabled = True
            btnCheckCK.Enabled = False
            txtMA_NH_TH.Enabled = False
            txtTEN_NH_TH.Enabled = False
            btnLuuSoDT.Visible = True
            btnLuuEmail.Visible = True
        End If
    End Sub
    ''' <summary>
    ''' Chỉ dùng trong trường hợp NNT đăng kí thông tin ủy quyền trích nợ tài khoản tại NHTM
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTaoMoi.Click
        'Response.Redirect("~/HQ247/frmHoanThienNNTDT.aspx")
        Response.Redirect("~/HQ247/frmDKMOINNTDT.aspx")
        txtstrdisabled.Value = ""
    End Sub
    ''' <summary>
    ''' Kiểm tra sự tồn tại của tài khoản NNT tại NH
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCheckTK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckTK.Click
        If txtCHK_TAIKHOAN_TH.Text = "" Then
            txtCHK_TAIKHOAN_TH.Focus()
            clsCommon.ShowMessageBox(Me, "Số tài khoản ngân hàng không được để trống")
            Exit Sub
        End If
        Try
            'Dim ten_tk As String
            'LogDebug.WriteLog("vào ws", Diagnostics.EventLogEntryType.Information)
            'Dim core As New clsCoreBankUtilDAB
            'LogDebug.WriteLog("12", Diagnostics.EventLogEntryType.Information)
            'ten_tk = clsCoreBankUtilDAB.GetCustomerByAccount(txtSoTKNH.Text.Trim)
            'LogDebug.WriteLog(ten_tk, Diagnostics.EventLogEntryType.Information)
            'Test Core Off
            'Trường hợp đăng ký tại TCHQ
            'Chỉ enable nút Check CK khi lấy được Tên tài khoản ở ô Tên tài khoản.
            'huynt_test
            If Not txtID.Value.Equals("") And Not txtSO_HS.Text.Trim.Equals("") Then
               
                    'Xử lý Core để lấy ra thông tin Tên tài khoản TH
                    '...txtCHK_TAIKHOAN_TH
                    Dim strTenTaiKhoanTuCore As String = ""
                    strTenTaiKhoanTuCore = clsCoreBank.GetTen_TK_KH_NH(txtCHK_TAIKHOAN_TH.Text.Replace("-", ""))
                    Dim tmp As String = txtCHK_TAIKHOAN_TH.Text
                    txtCHK_TEN_TAIKHOAN_TH.Text = strTenTaiKhoanTuCore
                    hdfCifno.Value = txtCHK_TAIKHOAN_TH.Text
                    hdfMA_CNTK.Value = "00"
                    tmp = ""

            
            Else
               
                    'Xử lý Core để lấy ra thông tin Tên tài khoản TH
                    '...
                    Dim strTenTaiKhoanTuCore As String = ""
                    strTenTaiKhoanTuCore = clscorebank.GetTen_TK_KH_NH(txtCHK_TAIKHOAN_TH.Text.Replace("-", ""))
                    txtCHK_TEN_TAIKHOAN_TH.Text = strTenTaiKhoanTuCore
                    'Sau đó kiểm tra điều kiện
                    If Not txtCHK_TEN_TAIKHOAN_TH.Text.Trim.Equals("") Then
                        txtTAIKHOAN_TH.Text = txtCHK_TAIKHOAN_TH.Text
                        txtTEN_TAIKHOAN_TH.Text = txtCHK_TEN_TAIKHOAN_TH.Text
                        hdfCifno.Value = txtCHK_TAIKHOAN_TH.Text
                        hdfMA_CNTK.Value = "00"
                        ' btnCheckCK.Enabled = False
                    Else
                        clsCommon.ShowMessageBox(Me, "Không tìm thấy thông tin tài khoản tại Ngân Hàng !")
                        Exit Sub
                    End If

            End If

           
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được tên khách hàng. Lỗi: " & ex.Message)
        End Try
    End Sub
    ''' <summary>
    ''' Hoàn thiện thông tin NNT và chuyển sang cho KSV
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnChuyenKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChuyenKS.Click
        Dim strThongTinLineHe As String = ""
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If txtCHK_TAIKHOAN_TH.Text = "" Then
            txtCHK_TAIKHOAN_TH.Focus()
            clsCommon.ShowMessageBox(Me, "Số tài khoản ngân hàng không được để trống")
            Exit Sub
        End If
        '20180119 Nham Sua Them dieu kien ngay hieu luc---------------------------------
        If txtNGAY_KY_HDUQ.Text <> "" Then
            Try
                Dim isDate As Date = DateTime.ParseExact(txtNGAY_KY_HDUQ.Text, "dd/MM/yyyy", Nothing)
                If isDate.Date < DateTime.Now.Date Then
                    txtNGAY_KY_HDUQ.Focus()
                    clsCommon.ShowMessageBox(Me, "Ngày hiệu lực phải lớn hơn hoặc bằng ngày hiện tại ")
                    Exit Sub
                End If
            Catch ex As Exception
                txtNGAY_KY_HDUQ.Focus()
                clsCommon.ShowMessageBox(Me, "Ngày hiệu lực phải đúng định dạng dd/mm/yyyy")
                Exit Sub
            End Try
        Else
            txtNGAY_KY_HDUQ.Focus()
            clsCommon.ShowMessageBox(Me, "Chưa nhập ngày hiệu lực")
            Exit Sub
        End If
        '--------------------------------
        Try
            If Not txtLoai_HS.Value.Equals("3") Then
                If txtCHK_TEN_TAIKHOAN_TH.Text.Trim.Length <= 0 Then
                    clsCommon.ShowMessageBox(Me, "Chưa kiểm tra tài khoản đăng ký ")
                    txtCHK_TAIKHOAN_TH.Focus()
                    Exit Sub
                End If
            End If
            'If txtNoiDung_XL.Text.Trim().Length <= 0 And Not txtSO_HS.Text.Trim.Equals("") Then
            '    clsCommon.ShowMessageBox(Me, "Chưa nhập nội dung hướng dẫn NNT ")
            '    txtNoiDung_XL.Focus()
            '    Exit Sub
            'End If
            If txtThongTin.Value.Length <= 0 Then
                clsCommon.ShowMessageBox(Me, "Chưa nhập Thông tin liên hệ ")
                Exit Sub
            Else
                Dim strDataLienHe As String = txtThongTin.Value
                Dim StrArrTMP As String() = strDataLienHe.Split("#")
                Dim x1 As Integer = 0
                Dim x2 As Integer = 0
                For i As Integer = 0 To StrArrTMP.Length - 1
                    Dim strTMPAS As String() = StrArrTMP(i).Split("|")
                    If (strTMPAS(1).Length > 0 And strTMPAS(2).Length > 0) Then
                        strThongTinLineHe += "<ThongTinLienHe><So_DT>" & strTMPAS(1) & "</So_DT><Email>" & strTMPAS(2) & "</Email></ThongTinLienHe>"
                        If (x1 = 0) And strTMPAS(1).Length > 0 Then
                            x1 = 1
                            txtSO_DT.Text = strTMPAS(1)
                        End If
                        If (x2 = 0) And strTMPAS(2).Length > 0 Then
                            x2 = 1
                            txtEMAIL.Text = strTMPAS(2)
                        End If
                    Else
                        clsCommon.ShowMessageBox(Me, "Thong tin lien he dong" & (i + 1) & " thieu thong tin dien thoai hoac email")
                        Exit Sub

                    End If
                Next


            End If
            ' Them phan kiem tra trung tai khoan
            If txtLoai_HS.Value.Equals("1") Or txtLoai_HS.Value.Equals("2") Then
                Dim strSQL As String = "SELECT SO_HS FROM TCS_DM_NTDT_HQ_311 WHERE MA_DV='" & txtMA_DV.Text.Trim() & "' AND CUR_TAIKHOAN_TH='" & txtCHK_TAIKHOAN_TH.Text.Trim & "' and SO_HS<>'" & txtSO_HS.Text & "'"
                strSQL &= "UNION ALL SELECT SO_HS FROM TCS_DM_NTDT_HQ WHERE MA_DV='" & txtMA_DV.Text.Trim() & "' AND CUR_TAIKHOAN_TH='" & txtCHK_TAIKHOAN_TH.Text.Trim & "' AND TRANGTHAI NOT IN ('15','16') "
                Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 Then
                        If ds.Tables(0).Rows.Count > 0 Then
                            txtCHK_TAIKHOAN_TH.Focus()
                            clsCommon.ShowMessageBox(Me, "Mã đơn vị " & txtMA_DV.Text.Trim() & " Tài khoản" & txtCHK_TAIKHOAN_TH.Text.Trim & " đã có đăng ký trong hệ thống!")
                            Exit Sub
                        End If
                    End If
                End If
            End If

            'Chuyển trạng thái của Đăng ký
            'Do có cả 2 phương thức: đăng ký mới tại TCHQ và đăng ký mới tại NHTM đều sử dụng chung Form này, do đó để xác đinh
            'được đâu là đăng ký mới tại TCHQ và đâu là tại NHTM ta kiểm tra ID và Số hồ sơ
            '-Xác định đây là trường hợp đăng ký mới từ TCHQ
            If Not hdfNNTDT_ID.Value.Trim.Equals("") And Not txtSO_HS.Text.Trim.Equals("") Then
                'check xem dang la hoan thien loai 1 hay loai 2
                If txtLoai_HS.Value.Equals("1") Then
                    Dim decOk As Decimal = Business_HQ.HQ247.TCS_DM_NTDT_HQ.MakerMSG312(hdfNNTDT_ID.Value, txtNGAY_KY_HDUQ.Text, txtCHK_TAIKHOAN_TH.Text.Trim, txtCHK_TEN_TAIKHOAN_TH.Text.Trim, strThongTinLineHe, Session.Item("UserName").ToString, hdfCifno.Value, hdfMA_CNTK.Value)
                    If decOk = Business_HQ.Common.mdlCommon.TCS_HQ247_OK Then
                        clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát thành công. Đã gửi thông tin tới KSV")
                        btnChuyenKS.Visible = False
                    Else
                        clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi:  " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decOk.ToString()))
                        btnChuyenKS.Visible = True
                    End If
                ElseIf txtLoai_HS.Value.Equals("2") Then
                    'kiem tra xem co phai dieu chinh tu bank khong
                    If Business_HQ.HQ247.daTCS_DM_NTDT_HQ.fnc_CheckDieuChinhTaiNH(txtSO_HS.Text) = 0 Then

                        Dim decOk As Decimal = Business_HQ.HQ247.daTCS_DM_NTDT_HQ.fnc_Maker312_SuaTaiNH(txtSO_HS.Text, txtCHK_TAIKHOAN_TH.Text, txtCHK_TEN_TAIKHOAN_TH.Text, txtNGAY_KY_HDUQ.Text, Session.Item("UserName").ToString, hdfCifno.Value, hdfMA_CNTK.Value)
                        If decOk = Business_HQ.Common.mdlCommon.TCS_HQ247_OK Then
                            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát thành công. Đã gửi thông tin tới KSV")
                            btnChuyenKS.Visible = False
                        Else
                            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi:  " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decOk.ToString()))
                            btnChuyenKS.Visible = True
                        End If
                        'neu da co roi thì thực hien maker312 binh thuong
                        'neu chua co thi phai copy ve 311 truoc
                    Else
                        Dim decOk As Decimal = Business_HQ.HQ247.TCS_DM_NTDT_HQ.MakerMSG312Sua(hdfNNTDT_ID.Value, txtNGAY_KY_HDUQ.Text, txtCHK_TAIKHOAN_TH.Text.Trim, txtCHK_TEN_TAIKHOAN_TH.Text.Trim, strThongTinLineHe, Session.Item("UserName").ToString, hdfCifno.Value, hdfMA_CNTK.Value)
                        If decOk = Business_HQ.Common.mdlCommon.TCS_HQ247_OK Then
                            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát thành công. Đã gửi thông tin tới KSV")
                            btnChuyenKS.Visible = False
                        Else
                            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi:  " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decOk.ToString()))
                            btnChuyenKS.Visible = True
                        End If
                    End If
                ElseIf txtLoai_HS.Value.Equals("3") Then
                    'khai huy

                    Dim decOk As Decimal = Business_HQ.HQ247.daTCS_DM_NTDT_HQ.fnc_Maker312_HuyTaiNH(txtSO_HS.Text, Session.Item("UserName").ToString)

                    If decOk = Business_HQ.Common.mdlCommon.TCS_HQ247_OK Then
                        clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát thành công. Đã gửi thông tin tới KSV")
                        btnChuyenKS.Visible = False
                    Else
                        clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi:  " & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decOk.ToString()))
                        btnChuyenKS.Visible = True
                    End If
                End If

            Else
                'Xác định đây là trường hợp đăng ký mới từ NHTM

                If hdfNNTDT_ID.Value.Trim.Equals("") And txtSO_HS.Text.Trim.Equals("") Then
                    'Xác định đây là trường hợp Thêm mới từ NHTM
                    If txtTenLoai_HS.Text.Equals("") Or txtMA_DV.Text.Equals("") Or txtTEN_DV.Text.Equals("") Or txtDIACHI.Text.Equals("") Or txtHO_TEN.Text.Equals("") Or txtSO_CMT.Text.Equals("") Or txtNGAYSINH.Text.Equals("") Or txtNGUYENQUAN.Text.Equals("") Or txtMA_NH_TH.Text.Equals("") Or txtTEN_NH_TH.Text.Equals("") Or txtNGAY_KY_HDUQ.Text.Equals("") Or txtCHK_TAIKHOAN_TH.Text.Equals("") Or txtCHK_TEN_TAIKHOAN_TH.Text.Equals("") Then
                        clsCommon.ShowMessageBox(Me, "Các mục đánh dấu sao (*) là bắt buộc nhập !")
                        Exit Sub
                    End If
                    Dim decErrID As Decimal = Business_HQ.HQ247.TCS_DM_NTDT_HQ.MakerMSG312H(txtMA_DV.Text.Trim(), txtTEN_DV.Text.Trim(), txtDIACHI.Text.Trim(), txtSO_CMT.Text.Trim(), txtHO_TEN.Text, txtNGAYSINH.Text, txtNGUYENQUAN.Text, txtSO_DT.Text, txtEMAIL.Text, "", "", txtTAIKHOAN_TH.Text.Trim(), txtTEN_TAIKHOAN_TH.Text.Trim(), txtMA_NH_TH.Text, txtTEN_NH_TH.Text, Session.Item("UserName").ToString, txtCHK_TAIKHOAN_TH.Text, txtCHK_TEN_TAIKHOAN_TH.Text, txtNGAY_KY_HDUQ.Text, strThongTinLineHe, hdfCifno.Value, hdfMA_CNTK.Value)
                    If decErrID = 0 Then
                        txtMA_DV.ReadOnly = True
                        txtTEN_DV.ReadOnly = True
                        txtDIACHI.ReadOnly = True
                        txtSO_CMT.ReadOnly = True
                        txtHO_TEN.ReadOnly = True
                        txtNGAYSINH.ReadOnly = True
                        txtNGUYENQUAN.ReadOnly = True
                        txtSO_DT.ReadOnly = True
                        txtEMAIL.ReadOnly = True
                        txtMA_NH_TH.ReadOnly = True
                        txtTEN_NH_TH.ReadOnly = True
                        txtCHK_TAIKHOAN_TH.ReadOnly = True
                        txtCHK_TEN_TAIKHOAN_TH.ReadOnly = True
                        txtNGAY_KY_HDUQ.ReadOnly = True
                        btnChuyenKS.Enabled = False
                        clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát thành công !")
                    End If
                Else
                    If Not hdfNNTDT_ID.Value.Trim.Equals("") And txtSO_HS.Text.Trim.Equals("") Then
                        ' Xác định đây là trường hợp NSD đã đăng ký mới từ NHTM
                        ' sau đó chuyển KSV nhưng bị Chuyển trả
                        ' Dẫn đến việc Sửa đổi cập nhật thông tin của đăng ký
                        If txtTenLoai_HS.Text.Equals("") Or txtMA_DV.Text.Equals("") Or txtTEN_DV.Text.Equals("") Or txtDIACHI.Text.Equals("") Or txtHO_TEN.Text.Equals("") Or txtSO_CMT.Text.Equals("") Or txtNGAYSINH.Text.Equals("") Or txtNGUYENQUAN.Text.Equals("") Or txtMA_NH_TH.Text.Equals("") Or txtTEN_NH_TH.Text.Equals("") Or txtNGAY_KY_HDUQ.Text.Equals("") Or txtCHK_TAIKHOAN_TH.Text.Equals("") Or txtCHK_TEN_TAIKHOAN_TH.Text.Equals("") Then
                            clsCommon.ShowMessageBox(Me, "Các mục đánh dấu sao (*) là bắt buộc nhập !")
                            Exit Sub
                        End If
                        Dim decErrID As Decimal = Business_HQ.HQ247.TCS_DM_NTDT_HQ.Update_HQ311(hdfNNTDT_ID.Value.Trim(), txtMA_DV.Text.Trim(), txtTEN_DV.Text.Trim(), txtDIACHI.Text.Trim(), txtSO_CMT.Text.Trim(), txtHO_TEN.Text, txtNGAYSINH.Text, txtNGUYENQUAN.Text, txtSO_DT.Text, txtEMAIL.Text, "", "", txtMA_NH_TH.Text, txtTEN_NH_TH.Text, Session.Item("UserName").ToString, txtCHK_TAIKHOAN_TH.Text, txtCHK_TEN_TAIKHOAN_TH.Text, txtNGAY_KY_HDUQ.Text, strThongTinLineHe, hdfCifno.Value, hdfMA_CNTK.Value)
                        If decErrID = 0 Then
                            txtMA_DV.ReadOnly = True
                            txtTEN_DV.ReadOnly = True
                            txtDIACHI.ReadOnly = True
                            txtSO_CMT.ReadOnly = True
                            txtHO_TEN.ReadOnly = True
                            txtNGAYSINH.ReadOnly = True
                            txtNGUYENQUAN.ReadOnly = True
                            txtSO_DT.ReadOnly = True
                            txtEMAIL.ReadOnly = True
                            txtMA_NH_TH.ReadOnly = True
                            txtTEN_NH_TH.ReadOnly = True
                            txtCHK_TAIKHOAN_TH.ReadOnly = True
                            txtCHK_TEN_TAIKHOAN_TH.ReadOnly = True
                            txtNGAY_KY_HDUQ.ReadOnly = True
                            btnChuyenKS.Enabled = False
                            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát thành công !")
                        Else
                            clsCommon.ShowMessageBox(Me, "Lỗi !" & Business_HQ.Common.mdlCommon.Get_ErrorDesciption(decErrID.ToString()))
                        End If
                    End If
                End If


            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi: " & ex.Message)
        End Try
    End Sub
    Shared dt As New DataTable
    Shared dem As Integer = 0
    Protected Sub btnLuuEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLuuEmail.Click
        If txtMA_DV.Text.Trim.Length <= 0 Then
            clsCommon.ShowMessageBox(Me, "Mã số thuế phải được nhập trước")
            txtMA_DV.BorderColor = Drawing.Color.DarkOrange
            txtMA_DV.Focus()
            Exit Sub
        End If
        Try
            Dim _id As String = DateTime.Now.ToString("yyyyMMddHHmmssff")
            Dim _ma_dv As String = txtMA_DV.Text.Trim
            Dim strSQLInsert As String = "INSERT INTO tc_dm_hq247_temp (ID,MA_DV,SO_DT,EMAIL) VALUES(" & _id & ",'" & _ma_dv & "','','" & txtEMAIL.Text.Trim() & "') "
            DataAccess.ExecuteNonQuery(strSQLInsert, CommandType.Text)
            Dim strSQL As String = "SELECT * from tc_dm_hq247_temp where MA_DV = '" & _ma_dv & "' AND EMAIL IS NOT NULL"
            Dim ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            GridEmail.DataSource = ds
            GridEmail.DataBind()
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub GridEmail_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridEmail.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim stor_id As String = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ID"))
            Dim _so_tk_nh As String = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Email"))
            Dim lnkbtnresult As Button = DirectCast(e.Row.FindControl("ButtonDeleteEmail"), Button)
            If lnkbtnresult IsNot Nothing Then
                lnkbtnresult.Attributes.Add("onclick", (Convert.ToString("javascript:return deleteConfirm('" & _so_tk_nh.Trim & "')")))
            End If
        End If
        If hdrTRANG_THAI.Value = "00" Or hdrTRANG_THAI.Value = "05" Or hdrTRANG_THAI.Value = "07" Then
            'e.Row.Cells(2).Visible = True
        Else
            'e.Row.Cells(2).Visible = False
        End If
    End Sub
    Protected Sub GridEmail_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridEmail.RowDeleting
        Try
            Dim _id As String = GridEmail.DataKeys(e.RowIndex).Values("ID").ToString()
            Dim _ma_dv As String = txtMA_DV.Text.Trim()
            Dim sqlupdate As String = "Delete tc_dm_hq247_temp where ID = " & _id & " AND MA_DV = '" & _ma_dv & "' AND EMAIL IS NOT NULL"
            DataAccess.ExecuteNonQuery(sqlupdate, CommandType.Text)
            Dim strSQL As String = "SELECT * from tc_dm_hq247_temp where MA_DV = '" & _ma_dv & "' AND EMAIL IS NOT NULL"
            Dim ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            GridEmail.DataSource = ds
            GridEmail.DataBind()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không xóa được dữ liệu. Lỗi: " & ex.Message)
        End Try

    End Sub

    Protected Sub btnLuuSoDT_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLuuSoDT.Click
        If txtMA_DV.Text.Trim.Length <= 0 Then
            clsCommon.ShowMessageBox(Me, "Mã số thuế phải được nhập trước")
            txtMA_DV.BorderColor = Drawing.Color.DarkOrange
            txtMA_DV.Focus()
            Exit Sub
        End If
        Try
            Dim _id As String = DateTime.Now.ToString("yyyyMMddHHmmssff")
            Dim _ma_dv As String = txtMA_DV.Text.Trim
            Dim strSQLInsert As String = "INSERT INTO tc_dm_hq247_temp (ID,MA_DV,SO_DT,EMAIL) VALUES(" & _id & ",'" & _ma_dv & "','" & txtSO_DT.Text.Trim() & "','') "
            DataAccess.ExecuteNonQuery(strSQLInsert, CommandType.Text)
            Dim strSQL As String = "SELECT * from tc_dm_hq247_temp where MA_DV = '" & _ma_dv & "' AND SO_DT IS NOT NULL"
            Dim ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            GridSoDT.DataSource = ds
            GridSoDT.DataBind()
        Catch ex As Exception

        End Try

    End Sub
    Protected Sub GridSoDT_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridSoDT.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim stor_id As String = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ID"))
            Dim _so_tk_nh As String = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Email"))
            Dim lnkbtnresult As Button = DirectCast(e.Row.FindControl("ButtonDeleteSoDT"), Button)
            If lnkbtnresult IsNot Nothing Then
                lnkbtnresult.Attributes.Add("onclick", (Convert.ToString("javascript:return deleteConfirm('" & _so_tk_nh.Trim & "')")))
            End If
        End If
        If hdrTRANG_THAI.Value = "00" Or hdrTRANG_THAI.Value = "05" Or hdrTRANG_THAI.Value = "07" Then
            'e.Row.Cells(2).Visible = True
        Else
            'e.Row.Cells(2).Visible = False
        End If
    End Sub
    Protected Sub GridSoDT_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridSoDT.RowDeleting
        Try
            Dim _id As String = GridSoDT.DataKeys(e.RowIndex).Values("ID").ToString()
            Dim _ma_dv As String = txtMA_DV.Text.Trim()
            Dim sqlupdate As String = "Delete tc_dm_hq247_temp where ID = " & _id & " AND MA_DV = '" & _ma_dv & "' AND SO_DT IS NOT NULL"
            DataAccess.ExecuteNonQuery(sqlupdate, CommandType.Text)
            Dim strSQL As String = "SELECT * from tc_dm_hq247_temp where MA_DV = '" & _ma_dv & "' AND SO_DT IS NOT NULL"
            Dim ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            GridSoDT.DataSource = ds
            GridSoDT.DataBind()
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không xóa được dữ liệu. Lỗi: " & ex.Message)
        End Try

    End Sub

    Protected Sub btnDieuChinh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDieuChinh.Click

        'cho phep sua so tai khoan
        txtCHK_TAIKHOAN_TH.Enabled = True
        txtMA_DV.Enabled = False
        txtTEN_DV.Enabled = False
        txtDIACHI.Enabled = False
        txtHO_TEN.Enabled = False
        txtSO_CMT.Enabled = False
        txtNGAYSINH.Enabled = False
        txtNGUYENQUAN.Enabled = False
        txtSERIALNUMBER.Enabled = False
        txtNGAY_HHL.Enabled = False
        txtNGAY_HL.Enabled = False
        txtNOICAP.Enabled = False
        txtMA_NH_TH.Enabled = False
        txtTEN_NH_TH.Enabled = False
        txtLoai_HS.Value = "2"
        txtTenLoai_HS.Text = "Khai sửa"
        txtNGAY_KY_HDUQ.Enabled = True
        txtNGAY_KY_HDUQ.ReadOnly = False
        btnDieuChinh.Enabled = False
        btnChuyenKS.Enabled = True
        btnCheckTK.Enabled = True

    End Sub
    Protected Sub btnHuyDK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHuyDK.Click
        txtLoai_HS.Value = "3"
        txtTenLoai_HS.Text = "Khai Hủy"
        btnTaoMoi.Enabled = False
        btnDieuChinh.Enabled = False
        btnChuyenKS.Enabled = True
        btnHuyDK.Enabled = False
        txtSO_HS.Enabled = False
        txtMA_DV.Enabled = False
        txtCHK_TAIKHOAN_TH.Enabled = False
        txtCHK_TEN_TAIKHOAN_TH.Enabled = False
        txtTAIKHOAN_TH.Enabled = False
        txtTEN_TAIKHOAN_TH.Enabled = False

        txtCHK_TAIKHOAN_TH.Text = txtTAIKHOAN_TH.Text
        txtCHK_TEN_TAIKHOAN_TH.Text = txtTEN_TAIKHOAN_TH.Text

        'Sau đó kiểm tra điều kiện
        If txtCHK_TEN_TAIKHOAN_TH.Text.Trim.Equals("") Then
            Dim strTenTaiKhoanTuCore As String = clsCoreBank.GetTen_TK_KH_NH(txtCHK_TAIKHOAN_TH.Text.Replace("-", ""))
            txtCHK_TEN_TAIKHOAN_TH.Text = strTenTaiKhoanTuCore
        End If
        txtNGAY_KY_HDUQ.Enabled = True
        txtNGAY_KY_HDUQ.ReadOnly = False
        btnChuyenKS.Focus()
    End Sub

    Protected Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Response.Redirect("~/HQ247/frmTraCuuNNTDT.aspx")
    End Sub
End Class
