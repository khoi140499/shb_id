﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmKiemSoatNNTDT.aspx.vb" 
    Inherits="NTDT_frmKiemSoatNNTDT" title="Duyệt đăng ký mới/điều chỉnh thông tin đăng ký dịch vụ NTDT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../javascript/jquery/jquery-1.3.2.js" type="text/javascript"></script>
    <style type="text/css">
        .txtHoanThien{width:80%;}
        .container{width:100%;padding-bottom:50px;}
    </style>
    <script type="text/javascript" language="javascript">
//     function DisableButton() {
//                document.forms[0].submit();
//                window.setTimeout("disableButton('" + 
//                   window.event.srcElement.id + "')", 200);
//            }

//20180119--------------------------------------------------------------------------------------
         function showDisable()
         {
            var loaiHS=document .getElementById("<%=txtLOAI_HS.ClientID %>").value;
            var soHS=document .getElementById("<%=txtSO_HS.ClientID %>").value;
            if(loaiHS =="1" && soHS.toString().length <=0){
                    //Ẩn các trường về thông tin chữ ký số//Ẩn số Tài khoản đăng ký, tên tài khoản đăng ký, Ngày gửi, Nội dung hướng dẫn NNT.
                    
                    document .getElementById("tdTAIKHOAN_TH").style.display = "none"
                    document .getElementById("tdTEN_TAIKHOAN_TH").style.display = "none"
                    document .getElementById("tdNGAY_NHAN311").style.display = "none"
                    document .getElementById("tdSERIALNUMBER").style.display="none";
                    document .getElementById("tdNOICAP").style.display = "none"
                    document .getElementById("tdNGAY_HL").style.display = "none"
                    document .getElementById("tdNGAY_HHL").style.display = "none"
            }
            
         }
         function showHightLight()
         {
            //Ngày hiệu lực và Số TK sử dụng NTDT.
            
            var loaiHS=document .getElementById("<%=txtLOAI_HS.ClientID %>").value;
            var soHS=document .getElementById("<%=txtSO_HS.ClientID %>").value;
           if(loaiHS=="1" || loaiHS =="2"){
                var gird=document.getElementById("<%=grdTKNH.ClientID %>");
                for(var i=0;i<gird.rows.length;i++){
                   gird.rows[i].cells[0].classList.remove('form_label');
                   gird.rows[i].cells[0].classList.add('form_label_hightlight');
                }
                document .getElementById("<%=txtNGAY_KY_HDUQ.ClientID %>").classList.remove('inputflat');
                
                document .getElementById("<%=txtNGAY_KY_HDUQ.ClientID %>").classList.add('input_highlight');
            }
            if(loaiHS =="1" && soHS.toString().length <=0){
                
                    // Mã số thuế, Tên đơn vị XNK, Địa chỉ đơn vị XNK, Họ tên NNT, Số chứng minh thư, Ngày sinh, Nguyên quán, Thông tin liên hệ, Mã Ngân hàng, Tên ngân hàng, Ngày hiệu lực, Số TK sử dụng NTDT.
                    document .getElementById("<%=txtMA_DV.ClientID %>").classList.remove('inputflat');
                    document .getElementById("<%=txtMA_DV.ClientID %>").classList.add('input_highlight');
                    document .getElementById("<%=txtTEN_DV.ClientID %>").classList.remove('inputflat');
                    document .getElementById("<%=txtTEN_DV.ClientID %>").classList.add('input_highlight');
                    document .getElementById("<%=txtDIACHI.ClientID %>").classList.remove('inputflat');
                    document .getElementById("<%=txtDIACHI.ClientID %>").classList.add('input_highlight');
                    document .getElementById("<%=txtSO_CMT.ClientID %>").classList.remove('inputflat');
                    document .getElementById("<%=txtSO_CMT.ClientID %>").classList.add('input_highlight');
                    document .getElementById("<%=txtNGAYSINH.ClientID %>").classList.remove('inputflat');
                    document .getElementById("<%=txtNGAYSINH.ClientID %>").classList.add('input_highlight');
                    document .getElementById("<%=txtHoTen.ClientID %>").classList.remove('inputflat');
                    document .getElementById("<%=txtHoTen.ClientID %>").classList.add('input_highlight');
                    document .getElementById("<%=txtNGUYENQUAN.ClientID %>").classList.remove('inputflat');
                    document .getElementById("<%=txtNGUYENQUAN.ClientID %>").classList.add('input_highlight');
//                    document.querySelectorAll('#divThongtin td').classList.remove('form_label');
//                    document.querySelectorAll('#divThongtin td').classList.add('form_label_highlight');
                
            }

         }
//---------------------------------------------------------------------------------------------
var strThongtin ='';
        //alert(strThongtin);
        var strHeader=' <table width="80%"  cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">';
            strHeader +='<tr><td  class="form_label">STT</td><td  class="form_label">Số điện thoại</td><td  class="form_label">Email</td>';
            strHeader +='</tr>';
         var strFooter = '</table>';
         function showThongtin()
         {
            if (strThongtin.length>0)
            {
                var tbl =strHeader;
                var arr = strThongtin.split('#');
                var i=0;
                
                var loaiHS=document .getElementById("<%=txtLOAI_HS.ClientID %>").value;
                var soHS=document .getElementById("<%=txtSO_HS.ClientID %>").value;
                
                for(i=0;i<arr.length;i++)
                {
                    if(loaiHS =="1" && soHS.toString().length <=0){
                        var arrtmp=arr[i].split('|');
                        tbl+='<tr>'; 
                        tbl+=' <td  class="form_label" style="background-color:#CDCD00;color:#000">'+arrtmp[0] +'</td>';
                        tbl+='<td  class="form_label" style="background-color:#CDCD00;color:#000">'+arrtmp[1] +'</td>';
                        tbl+='<td  class="form_label" style="background-color:#CDCD00;color:#000">'+arrtmp[2] +'</td>'; 
                        tbl+='</tr>'; 
                    }else{
                        var arrtmp=arr[i].split('|');
                        tbl+='<tr>'; 
                        tbl+=' <td  class="form_label">'+arrtmp[0] +'</td>';
                        tbl+='<td  class="form_label">'+arrtmp[1] +'</td>';
                        tbl+='<td  class="form_label">'+arrtmp[2] +'</td>'; 
                        tbl+='</tr>'; 
                    }
                   
                }
                document.getElementById('divThongtin').innerHTML=tbl+strFooter;
                
            }
         }

        function jsProcessWarningMessage(message) {
            var xxx = confirm("Thông báo : " + message + ' .Bạn có muốn tiếp tục duyệt lệnh?');
            if (xxx == true) {
                document.getElementById("<%= btnDuyetBK.ClientID%>").click();
            }
        };

        function jsProcessKS(message) {
            document.getElementById("<%= btnDuyetBK.ClientID%>").click();
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle"> Duyệt yêu cầu đăng kí trích nợ tài khoản của NNT </p>
    <div class="container">
        <table width="100%">
            <tr>
                <td style="width:30%" valign="top">
                    <asp:GridView ID="grdDSCT" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        ShowHeader="True" ShowFooter="True" BorderColor="#989898" CssClass="grid_data"
                        PageSize="15" Width="100%">
                        <PagerStyle CssClass="cssPager" />
                        <AlternatingRowStyle CssClass="grid_item_alter" />
                        <HeaderStyle CssClass="grid_header" HorizontalAlign="Left"></HeaderStyle>
                        <RowStyle CssClass="grid_item" />
                        <Columns>
                            <asp:TemplateField HeaderText="TT">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                     <%#Eval("TRANGTHAI")%>
                                </ItemTemplate>
                                 <FooterTemplate>
                                    Tổng số:
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="MST" FooterText="Tổng số:">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnSelectCT" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "MA_DV") %>'
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ID")%>'
                                        OnClick="btnSelectCT_Click" />
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Số TK" FooterText="">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <%#DataBinder.Eval(Container.DataItem, "CUR_TAIKHOAN_TH")%>
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Loại HS" >
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "TENLOAI_HS") %>
                                </ItemTemplate>
                                <FooterTemplate>
                               <%#grdDSCT.Rows.Count.ToString%>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Left" BackColor="#E4E5D7"></PagerStyle>
                    </asp:GridView>
                    <p class="form_label">Trạng thái:<br />
                    <asp:DropDownList ID="drpTrangThai" runat="server" Width="97%" CssClass="inputflat" AutoPostBack="true" >                                                                 
                    </asp:DropDownList></p>
                    <p><asp:Button ID="btnRefresh" runat="server" Text="Làm mới" Font-Bold="true" CssClass="buttonField" /></p>
                </td>
                <td>
                 <asp:HiddenField runat="server" ID="txtThongTin"  />
                    <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Số hồ sơ</td>
                            <td class="form_control"><asp:TextBox ID="txtSO_HS" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="false" MaxLength="20"></asp:TextBox></td>
                        </tr>
                        <tr>
                        <asp:HiddenField ID="txtLOAI_HS" runat="server" />
                            <td align="left" style="width:25%" class="form_label">Loại hồ sơ <span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtTENLOAI_HS" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="20"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Mã số thuế<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtMA_DV" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="14"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Tên đơn vị XNK<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtTEN_DV" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Địa chỉ đơn vị XNK<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtDIACHI" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Số chứng minh thư<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtSO_CMT" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="10"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Ngày sinh<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNGAYSINH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="10"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Họ tên<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtHoTen" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="10"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Nguyên quán<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNGUYENQUAN" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr>
             <td align="left" style="width:25%" class="form_label">Thông tin liên hệ </td>
                <td class="form_control" style="text-align:left">
                    <div id="divThongtin">
                    
                    <table width="100%"  cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
                    <tr>
                        <td  class="form_label">STT</td>
                        <td  class="form_label">Số điện thoại</td>
                        <td  class="form_label">Email</td>
                      
                    </tr>
                  
                    </table>
                    </div>
                </td>
            </tr>
                        <tr style="display:none;">
                            <td align="left" style="width:25%" class="form_label">Số điện thoại</td>
                            <td class="form_control"><asp:TextBox ID="txtSO_DT" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="15"></asp:TextBox></td>
                        </tr>
                       <tr style="display:none;">
                            <td align="left" style="width:25%" class="form_label">Email</td>
                            <td class="form_control"><asp:TextBox ID="txtEMAIL" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="50"></asp:TextBox></td>
                        </tr>
                        <tr id="tdSERIALNUMBER">
                            <td align="left" style="width:25%" class="form_label">Số Seri của chứng thư<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtSERIALNUMBER" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="40"></asp:TextBox></td>
                        </tr>
                        <tr id="tdNOICAP">
                            <td align="left" style="width:25%" class="form_label">Đơn vị cấp chứng thư<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNOICAP" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr id="tdNGAY_HL">
                            <td align="left" style="width:25%" class="form_label">Ngày hiệu lực của chứng thư số<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNGAY_HL" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="19"></asp:TextBox></td>
                        </tr>
                        <tr id="tdNGAY_HHL">
                            <td align="left" style="width:25%" class="form_label">Ngày hết hiệu lực chứng thư số<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNGAY_HHL" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="19"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Mã ngân hàng<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtMA_NH_TH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="7"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Tên ngân hàng<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtTEN_NH_TH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr id="tdTAIKHOAN_TH">
                            <td align="left" style="width:25%" class="form_label">Số tài khoản đăng ký<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtTAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="50"></asp:TextBox></td>
                        </tr>
                        <tr id="tdTEN_TAIKHOAN_TH">
                            <td align="left" style="width:25%" class="form_label">Tên tài khoản đăng ký<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtTEN_TAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        <tr id="tdNGAY_NHAN311">
                            <td align="left" style="width:25%" class="form_label">Ngày gửi<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNGAY_NHAN311" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="19"></asp:TextBox></td>
                        </tr>
                        <tr style="display:none">
                            <td align="left" style="width:25%" class="form_label">Nội dung hướng dẫn NNT<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNOIDUNG_XL" runat="server" CssClass="inputflat txtHoanThien"  MaxLength="4000" TextMode="MultiLine" Height="40"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Ngày hiệu lực<span class="requiredField">(*)</span></td>
                            <td class="form_control"><asp:TextBox ID="txtNGAY_KY_HDUQ" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="10"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Số tài khoản sử dụng NTDT <span class="requiredField">(*)</span></td>
                            <td>
                        <asp:GridView ID="grdTKNH" runat="server"  Width="80%" DataKeyNames="ID" ShowHeader="false"
                        AutoGenerateColumns="False" >
                        <Columns>
                            <asp:BoundField DataField="ID" Visible="false"/>
                            <asp:TemplateField HeaderText="Tài khoản ngân hàng">
                                <EditItemTemplate><asp:TextBox ID="txtSoTKNH_Edit" runat="server"  Text='<%#Eval("CUR_TAIKHOAN_TH") %>'></asp:TextBox></EditItemTemplate>
                                <ItemTemplate><asp:Label ID="lblSoTKNH" runat="server"   Text='<%#Eval("CUR_TAIKHOAN_TH") %>'></asp:Label></ItemTemplate>
                                <ItemStyle Width="40%" CssClass="form_label"/>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tên tài khoản">
                                <ItemTemplate><asp:Label ID="lblCongTy" runat="server" Text='<%#Eval("CUR_TEN_TAIKHOAN_TH") %>'></asp:Label></ItemTemplate>
                                <ItemStyle Width="40%" CssClass="form_label"/>
                            </asp:TemplateField>
                            
                           <%-- <asp:TemplateField>--%>
                                <%--<EditItemTemplate>
                                    <asp:Button ID="ButtonUpdate" runat="server" CommandName="Update"  Text="Lưu"  />
                                    <asp:Button ID="ButtonCancel" runat="server" CommandName="Cancel"  Text="Không lưu" />
                                </EditItemTemplate>--%>
                                <%--<ItemTemplate>--%>
                                    <%--<asp:Button ID="ButtonEdit" runat="server" CommandName="Edit"  Text="Sửa"  />--%>
                                   <%-- <asp:Button ID="ButtonDelete" runat="server" CommandName="Delete"  Text="Xóa" Font-Bold="true"  />--%>
                                <%--</ItemTemplate>--%>
                                <%--<ItemStyle CssClass="form_label"/>
                             </asp:TemplateField>--%>
                        </Columns>
                    </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width:25%" class="form_label">Lý do chuyển trả</td>
                            <td class="form_control"><asp:TextBox ID="txtLY_DO_CHUYEN_TRA" TextMode="MultiLine" Height="40" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="255"></asp:TextBox></td>
                        </tr>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <tr>
                            <td colspan="2">&nbsp;
                            <asp:HiddenField ID="hdfNNTDT_ID" runat="server" />
                            <asp:HiddenField ID="hdrTRANG_THAI" runat="server" />
                            <asp:HiddenField ID="hdfMA_NV" runat="server" />
                            <asp:HiddenField ID="hdfSUBJECT_CERT_NTHUE" runat="server" />
                            <asp:HiddenField ID="hdfMA_GDICH" runat="server" />
                            <asp:HiddenField ID="hdfUpdate_TT" runat="server" />
                            </td>
                        </tr>
                         <tr>
                        <td align="left" width='100%'colspan="2">
                            <b>
                                <asp:Literal ID="lblStatus" runat="server"></asp:Literal></b>
                        </td>
                    </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnDuyet" runat="server" CssClass="buttonField" Text="Duyệt" Enabled=false/>&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnChuyenTra" runat="server" CssClass="buttonField" Text="Chuyển trả" Enabled=false />&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnThoat" runat="server" CssClass="buttonField" Text="Thoát"  Enabled=false/>
                                 <div style="display:none">
                                  <asp:Button ID="btnDuyetBK" runat="server" Text="Duyệt BK" CssClass="ButtonCommand" UseSubmitBehavior="false"/>
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        
    </div>
     <script type="text/javascript" charset="utf-8">
        
        strThongtin= document.getElementById('<%=txtThongTin.ClientID %>').value;
        showThongtin();
        showHightLight();
        showDisable()
    </script>		
</asp:Content>

