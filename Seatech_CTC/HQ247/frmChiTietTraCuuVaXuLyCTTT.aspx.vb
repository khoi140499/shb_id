﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.HQ247.NNTDT
Imports Business.Common.mdlCommon
Imports Business.BaoCao
Imports Business.Common
Imports System.IO
Imports System.Xml
Imports CustomsV3.MSG.MSG304
Imports System.Collections.Generic
Imports System.Diagnostics
Imports Business_HQ
Imports HQ247XulyMSG

Partial Class NTDTHQ247_frmChiTietTraCuuVaXuLyCTTT
    Inherits System.Web.UI.Page
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim v_strRootPath As String = ConfigurationManager.AppSettings("APP_ROOT_PATH").ToString
        Dim iResult As Integer = 0
        Dim strSql As String = " SELECT COUNT (1) KIEMTRA   FROM ("
        strSql &= "  SELECT A.MA_CN           FROM TCS_DM_CHUCNANG A , TCS_PHANQUYEN B , TCS_DM_NHANVIEN C , TCS_SITE_MAP D         "
        strSql &= "   WHERE UPPER(C.TEN_DN)=UPPER('" & Session.Item("UserName").ToString() & "') AND D.STATUS = 1  AND D.NAVIGATEURL = '" + HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") + "' "
        strSql &= "  AND (A.MA_CN=D.NODEID OR (A.MA_CN=D.PARENTNODEID ) )"
        strSql &= "  AND A.MA_CN=B.MA_CN AND B.MA_NHOM =C.MA_NHOM ) "

        If HttpContext.Current.Request.Url.AbsolutePath.Replace(v_strRootPath, "~") <> "~/pages/frmHomeNV.aspx" Then
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
            If Not Globals.IsNullOrEmpty(dt) Then
                iResult = CInt(dt.Rows(0)(0).ToString())
            End If
            'If iResult <= 0 Then
            '    Response.Redirect("~/pages/frmNoAuthorize.aspx", False)
            '    Exit Sub
            'End If
        End If

        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                If Not Request.Params("ID") Is Nothing Then
                    Dim NNTDT_ID As String = Request.Params("ID").ToString
                    Dim strErrMsg As String = ""
                    If NNTDT_ID <> "" Then
                        LoadCTChiTietAll(NNTDT_ID)
                    End If
                End If
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Lỗi: " & ex.Message)
            End Try
        End If
        Dim sb As StringBuilder = New StringBuilder()
        sb.Append(ClientScript.GetPostBackEventReference(btnChuyenKS, ""))
        sb.Append(";this.disabled = true;")
        btnChuyenKS.Attributes.Add("onclick", sb.ToString())
    End Sub
    Private Sub LoadCTChiTietAll(ByVal pStrID As String)
        Try
            txtID.Value = pStrID
            Dim cls_songphuong As New CorebankServiceESB.clsSongPhuong
            Dim ds As DataSet = Business_HQ.HQ247.daCTUHQ304.getMSG304Detail(pStrID)
            If ds.Tables(0).Rows.Count > 0 Then
                Dim obj As MSG304 = New MSG304()
                obj.SETDATA(ds.Tables(0).Rows(0)("MSG_CONTENT").ToString())
                txtMST.Text = obj.Data.ThongTinChungTu.Ma_DV
                txtTen_DV.Text = obj.Data.ThongTinChungTu.Ten_DV
                txtMaChuong.Text = obj.Data.ThongTinChungTu.Ma_Chuong
                txtSHKB.Text = obj.Data.ThongTinChungTu.Ma_KB
                txtTenKB.Text = obj.Data.ThongTinChungTu.Ten_KB
                txtMa_HQ_PH.Text = obj.Data.ThongTinChungTu.Ma_HQ_PH
                txtMa_HQ_CQT.Text = obj.Data.ThongTinChungTu.Ma_HQ_CQT
                txtMa_NTK.Text = obj.Data.ThongTinChungTu.Ma_NTK
                txtTKKB.Text = obj.Data.ThongTinChungTu.TKKB
                txtNgayLap_CT.Text = convertDateHQ(obj.Data.ThongTinChungTu.NgayLap_CT)
                txtNgayTruyen_CT.Text = convertDateHQ(obj.Data.ThongTinChungTu.NgayTruyen_CT)
                txtKyHieu.Text = obj.Data.ThongTinChungTu.KyHieu_CT
                txtSo_CT.Text = obj.Data.ThongTinChungTu.So_CT
                txtLoai_CT.Text = obj.Data.ThongTinChungTu.Loai_CT
                txtNgay_BN.Text = convertDateHQ(obj.Data.ThongTinChungTu.Ngay_BN)
                txtNgay_CT.Text = convertDateHQ(obj.Data.ThongTinChungTu.Ngay_CT)
                txtMa_NT.Text = obj.Data.ThongTinChungTu.Ma_NT
                txtTyGia.Text = obj.Data.ThongTinChungTu.Ty_Gia
                txtMST_NNT.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.Ma_ST
                txtTen_NNT.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.Ten_NNT
                txtSo_CMT.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.So_CMT
                txtDiaChi.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.DiaChi
                'txtDienGiai.Text = obj.Data.ThongTinChungTu.DienGiai
                'Nhamlt_20180514_ Dien giai build theo ngan hang
                If Not ds.Tables(0).Rows(0)("DIEN_GIAI") Is Nothing Then
                    txtDienGiai.Text = ds.Tables(0).Rows(0)("DIEN_GIAI").ToString()
                Else
                    txtDienGiai.Text = ""
                End If
                txtTT_Khac.Text = obj.Data.ThongTinGiaoDich.NguoiNopTien.TT_Khac
                txtMa_NH_TH.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ma_NH_TH
                txtTen_NH_TH.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ten_NH_TH
                txtSoTienCanNop.Text = obj.Data.ThongTinChungTu.SoTien_TO
                txtTongTien.Text = obj.Data.ThongTinChungTu.SoTien_TO
                lblSotienBangCHu.Text = ConcertCurToText.ConvertCurr.So_chu(obj.Data.ThongTinChungTu.SoTien_TO)
                lblSoCTU.Text = txtSo_CT.Text & "-" & ds.Tables(0).Rows(0)("TRANG_THAI").ToString()
                txtLyDo_Huy.Text = ds.Tables(0).Rows(0)("LYDO").ToString()
                txtTaiKhoan_TH.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.TaiKhoan_TH
                txtTen_TaiKhoan_TH.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ten_TaiKhoan_TH
                txtSOTKTT.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.TaiKhoan_TH
                txtTenTKTT.Text = obj.Data.ThongTinGiaoDich.TaiKhoan_NopTien.Ten_TaiKhoan_TH

                txtTen_HQ_PH.Text = Business_HQ.HQ247.daCTUHQ304.getTenHQ_PH(obj.Data.ThongTinChungTu.Ma_HQ_PH)
                txtTen_HQ_CQT.Text = Business_HQ.HQ247.daCTUHQ304.getTenHQ_CQT(obj.Data.ThongTinChungTu.Ma_HQ_CQT, obj.Data.ThongTinChungTu.Ma_HQ_PH)
                dtgrd.DataSource = obj.Data.ThongTinChungTu.convertGNT_CTToDS().Tables(0)
                dtgrd.DataBind()
                Dim dsx As DataSet
                Dim strResult As String = cls_songphuong.ValiadSHB(obj.Data.ThongTinChungTu.Ma_KB)

                Dim strNHB As String = ""
                Dim strACC_NHB As String = ""
                If strResult.Length > 8 Then
                    Dim strarr() As String
                    strarr = strResult.Split(";")
                    strNHB = strarr(0).ToString
                    strACC_NHB = strarr(1).ToString
                End If

                If strNHB.Length = 8 Then
                    dsx = Business_HQ.HQ247.daCTUHQ304.getNHTrucTiepGianTiepKB_SHB(obj.Data.ThongTinChungTu.Ma_KB, strNHB)
                Else
                    dsx = Business_HQ.HQ247.daCTUHQ304.getNHTrucTiepGianTiep(obj.Data.ThongTinChungTu.Ma_KB)
                End If

                If Not dsx Is Nothing Then
                    If dsx.Tables.Count > 0 Then
                        If dsx.Tables(0).Rows.Count > 0 Then
                            txtMa_NH_TT.Text = dsx.Tables(0).Rows(0)("MA_TRUCTIEP").ToString() '  $('#txtMA_NH_TT').val();
                            txtTen_NH_TT.Text = dsx.Tables(0).Rows(0)("TEN_TRUCTIEP").ToString()  '$('#txtTEN_NH_TT').val();
                            txtMa_NH_GTiep.Text = dsx.Tables(0).Rows(0)("MA_GIANTIEP").ToString() ' $('#txtMA_NH_B').val();
                            txtTen_NH_GTiep.Text = dsx.Tables(0).Rows(0)("TEN_GIANTIEP").ToString() ' $('#txtTenMA_NH_B').val();

                        End If
                    End If
                End If
                Dim strTrangthai As String = ds.Tables(0).Rows(0)("TRANGTHAI").ToString()
                If Not strTrangthai.Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_SEND301_OK) And _
                Not strTrangthai.Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_CHEKER213_RESPONSE_TUCHOI) And _
                Not strTrangthai.Equals(Business_HQ.Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_CHO_CORE_RESPONSE) Then
                    btnChuyenKS.Visible = True
                End If

            End If
            'Me.txtMatKhau.Focus()
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log.Error(sbErrMsg.ToString())


            'Throw ex
        Finally
            'Catch ex As Exception
            '    logger.Error(ex.StackTrace)
        End Try
    End Sub
    Public Function convertDateHQ(ByVal strDate As String) As String

        Dim str As String = ""
        Try
            str = strDate.Substring(8, 2) & "/" & strDate.Substring(5, 2) & "/" & strDate.Substring(0, 4)
            If strDate.Trim.Length > 10 Then
                str &= strDate.Substring(10, strDate.Length - 10)
            End If
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log.Error(sbErrMsg.ToString())

        End Try

        Return str
    End Function
    Protected Sub btnChuyenKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChuyenKS.Click
        'btnChuyenKS.Enabled = False
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            'btnChuyenKS.Enabled = True
            Exit Sub
        End If
        Try
            Dim strSender_Code As String = ConfigurationSettings.AppSettings.Get("CUSTOMS.Sender_Code").ToString()
            Dim obj As XULY_MSG304 = New XULY_MSG304(Session.Item("UserName").ToString(), Session.Item("User").ToString(), strSender_Code)
            clsCommon.ShowMessageBox(Me, obj.ProcessData(txtID.Value))
            LoadCTChiTietAll(txtID.Value)
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình xử lý chứng từ 11.3.7")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log.Error(sbErrMsg.ToString())

        End Try
        'btnChuyenKS.Enabled = True
    End Sub
End Class
