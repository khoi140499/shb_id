﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmTiepNhanNNTDT.aspx.vb" Inherits="HQ247_frmTiepNhanNNTDT" title="Tiếp nhận yêu cầu đăng kí trích nợ tài khoản của NNT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
        function deleteConfirm(pubid) {
            var result = confirm('Bạn có thực sự muốn xóa tài khoản ' + pubid + ' ?');
            if (result) {
                return true;
            }
            else {
                return false;
            }
        }

    
    </script>
    <style type="text/css">
        .txtHoanThien{width:80%;}
        .container{width:100%;padding-bottom:50px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle"> Tiếp nhận yêu cầu đăng ký trích nợ tài khoản của NNT</p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
            <tr>
                <td align="left" style="width:25%" class="form_label">Số hồ sơ <span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                    <asp:HiddenField runat="server" ID="txtID" />
                <asp:TextBox ID="txtSo_HS" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" Enabled="false" MaxLength="20"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Loại hồ sơ<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:HiddenField runat="server" ID="txtLoai_HS" />
                <asp:TextBox ID="txtTenLoai_HS" runat="server" CssClass="inputflat txtHoanThien" ReadOnly="true" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Mã số thuế <span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtMST" runat="server" CssClass="inputflat txtHoanThien"  MaxLength="14" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên đơn vị XNK<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtTen_DV" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ReadOnly="true"></asp:TextBox></td>
            </tr>
            
            <tr>
                <td align="left" style="width:25%" class="form_label">Địa chỉ đơn vị XNK<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtDiaChi" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Họ tên NNT<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtHo_Ten" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số chứng minh thư<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtSo_CMT" runat="server" CssClass="inputflat txtHoanThien" MaxLength="10" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Ngày sinh<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtNgaySinh" runat="server" CssClass="inputflat txtHoanThien" MaxLength="10" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Nguyên quán<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtNguyenQuan" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Thông tin liên hệ </td>
                <td class="form_control" style="text-align:left">
                <asp:DataGrid ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="80%" BorderColor="Black" CssClass="grid_data" 
            >
            <AlternatingItemStyle CssClass="grid_item_alter"></AlternatingItemStyle>
            <HeaderStyle  CssClass="grid_header"></HeaderStyle>
            <ItemStyle CssClass="grid_item" />
            <Columns>
          <asp:BoundColumn DataField="STT" HeaderText="STT">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="So_DT" HeaderText="Số Điện thoại">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Email" HeaderText="Email">
                    <HeaderStyle></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                
            </Columns>
          
        </asp:DataGrid>
                </td>
            </tr>
            <tr style="display:none;">
                <td align="left" style="width:25%" class="form_label"></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtSo_DT1" runat="server" CssClass="inputflat txtHoanThien" MaxLength="15" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr style="display:none;">
                <td align="left" style="width:25%" class="form_label">Email</td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtSo_DT" runat="server" CssClass="inputflat txtHoanThien" MaxLength="15" ReadOnly="true"></asp:TextBox>
                <asp:TextBox ID="txtEmail" runat="server" CssClass="inputflat txtHoanThien" MaxLength="50" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr style="display:none;">
                <td align="left" style="width:25%" class="form_label"></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtEmail1" runat="server" CssClass="inputflat txtHoanThien" MaxLength="50" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số serial của chứng thư số<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtSerialNumber" runat="server" CssClass="inputflat txtHoanThien" MaxLength="40" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Đơn vị cấp chứng thư số<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtNoi_Cap" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ReadOnly="true"></asp:TextBox></td>
            </tr>
            
                        <tr>
                <td align="left" style="width:25%" class="form_label">Ngày hiệu lực chứng thư số<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtNgay_HL" runat="server" CssClass="inputflat txtHoanThien" MaxLength="19" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Ngày hết hiệu lực chứng thư số<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtNgay_HHL" runat="server" CssClass="inputflat txtHoanThien" MaxLength="19" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Mã ngân hàng<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtMa_NH_TH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="7" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên ngân hàng<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtTen_NH_TH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số tài khoản đăng kí<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtTaiKhoan_TH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="50" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên tài khoản đăng kí<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtTen_TaiKhoan_TH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Ngày gửi<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtNGAY_NHAN311" runat="server" CssClass="inputflat txtHoanThien" MaxLength="19" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Check số tài khoản</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtCHK_TAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox><asp:Button ID="btnCheckTK" runat="server" Text="Check TK" CssClass="buttonField" />
               </td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên tài khoản</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtCHK_TEN_TAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="200" ReadOnly="true"></asp:TextBox><asp:Button ID="btnCheckCK" runat="server" Text="Check CK" CssClass="buttonField" /></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Số tài khoản sử dụng NTDT</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtCUR_TAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="50" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left" style="width:25%" class="form_label">Tên tài khoản sử dụng NTDT</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtCUR_TEN_TAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255" ReadOnly="true"></asp:TextBox></td>
            </tr>
             <tr>
                <td align="left" style="width:25%" class="form_label">Nội dung hướng dẫn NNT<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtNoiDung_XL" runat="server" CssClass="inputflat txtHoanThien" MaxLength="4000" TextMode="MultiLine" Height="40" ReadOnly="true"></asp:TextBox></td>
            </tr>
            <tr style="display:none;">
                <td align="left" style="width:25%" class="form_label">Lý do hủy</td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtLyDo_Huy" runat="server" CssClass="inputflat txtHoanThien" MaxLength="4000"  TextMode="MultiLine" Height="30" ReadOnly="true" ></asp:TextBox></td>
            </tr>
            
            
            <tr>
                <td colspan="2">&nbsp;
                <asp:HiddenField ID="hdfNNTDT_ID" runat="server" />
                <asp:HiddenField ID="hdrTRANG_THAI" runat="server" />
                <asp:HiddenField ID="hdfMA_CN" runat="server" />
                <asp:HiddenField ID="hdfMA_NV" runat="server" />
                <asp:HiddenField ID="hdrMax_SoTKNH" runat="server" />
                <asp:HiddenField ID="hdnCifno" runat="server" />
                <asp:HiddenField ID="hdnCheckBtnCheckTK_Click" runat="server" /></td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:Button ID="btnChuyenKS" runat="server" CssClass="buttonField" Text="Chuyển KS duyệt yêu cầu" />&nbsp;&nbsp;
                    <asp:Button ID="btnHuy" runat="server"  CssClass="buttonField" Text="Hủy phiếu" Width="90px"/>&nbsp;&nbsp;
                    <asp:Button ID="btnExit" runat="server" CssClass="buttonField" Text="Thoát" Width="90px" />
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
        </table>
    </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtNgayGui").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
    </script>		
</asp:Content>

