﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmHoanThienNNTDT.aspx.vb" Inherits="HQ247_frmHoanThienNNTDT" Title="Hoàn thiện thông tin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>

    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>

    <script type="text/javascript">
       
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
        function deleteConfirm(pubid) {
            var result = confirm('Bạn có thực sự muốn xóa tài khoản ' + pubid + ' ?');
            if (result) {
                return true;
            }
            else {
                return false;
            }
        }
        function OnKeyPress(e) {  
                 var k = (window.event ? e.keyCode : e.which);          
                
                 return ((k > 47 && k < 58)  || k==0 ||k==8 );       
        }
        var strdisabled='';
        var strThongtin ='';
        //alert(strThongtin);
        var strHeader=' <table width="80%"  cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">';
            strHeader +='<tr><td  class="form_label">STT</td><td  class="form_label">Số điện thoại</td><td  class="form_label">Email</td>';
            strHeader +='<td  class="form_label"><input type="button" id="btnAdd" value="Thêm" onclick="AddThongtin()" __DISABLED__  /> </td></tr>';
         var strFooter = '</table>';
         function showThongtin()
         {
            strThongtin= document.getElementById('<%=txtThongTin.ClientID %>').value;
            strdisabled=document.getElementById('<%=txtstrdisabled.ClientID %>').value ;
            if (strThongtin.length>0)
            {
                var tbl =strHeader.replace('__DISABLED__',strdisabled);
                var arr = strThongtin.split('#');
                var i=0;
                for(i=0;i<arr.length;i++)
                {
                    var arrtmp=arr[i].split('|');
                    tbl+='<tr>'; 
                    tbl+=' <td  class="form_label"><input type="hidden" id="STT'+arrtmp[0] +'" value="'+arrtmp[0] +'"  />'+arrtmp[0] +' </td>';
                    tbl+='<td  class="form_label"><input type="text" id="txtSDT'+arrtmp[0] +'" value="'+arrtmp[1] +'"  style="width:90%" '+strdisabled+' onkeypress="return OnKeyPress(event);" onblur="OnSave()" /> </td>';
                    tbl+='<td  class="form_label"><input type="text" id="txtEMAIL'+arrtmp[0] +'" value="'+arrtmp[2] +'" style="width:90%" onblur="OnSave()"  '+strdisabled+' /> </td>';
                    tbl+='<td  class="form_label"><input type="button" id="Button2" value="Xóa" onclick="delThongTin('+arrtmp[0] +')"  '+strdisabled+' /></td>';
             
                    tbl+='</tr>'; 
                }
                document.getElementById('divThongtin').innerHTML=tbl+strFooter;
                
            }else
            {
          //  alert(strdisabled);
                strThongtin='1||';
                 var tbl =strHeader.replace('__DISABLED__',strdisabled);
                var arr = strThongtin.split('#');
                var i=0;
                for(i=0;i<arr.length;i++)
                {
                    var arrtmp=arr[i].split('|');
                    tbl+='<tr>'; 
                    tbl+=' <td  class="form_label"><input type="hidden" id="STT'+arrtmp[0] +'" value="'+arrtmp[0] +'"  />'+arrtmp[0] +' </td>';
                    tbl+='<td  class="form_label"><input type="text" id="txtSDT'+arrtmp[0] +'" value="'+arrtmp[1] +'" style="width:90%" onkeypress="return OnKeyPress(event);" onblur="OnSave()"  "'+strdisabled+' /> </td>';
                    tbl+='<td  class="form_label"><input type="text" id="txtEMAIL'+arrtmp[0] +'" value="'+arrtmp[2] +'" style="width:90%" onblur="OnSave()"  "'+strdisabled+' /> </td>';
                    tbl+='<td  class="form_label"><input type="button" id="Button2" value="Xóa" onclick="delThongTin('+arrtmp[0] +')"  '+strdisabled+' /></td>';
          
                    tbl+='</tr>'; 
                }
                document.getElementById('divThongtin').innerHTML=tbl+strFooter;
            
            }
         }
         function AddThongtin()
         {
          strThongtin=document.getElementById('<%=txtThongTin.ClientID %>').value;
          if (strThongtin.length>0)
            {
                var strTMP="";
              //  var tbl =strHeader;
                var arr = strThongtin.split('#');
                var i=1;
                var x=0;
                for(i=1;i<=arr.length;i++)
                {
                  
                      x=x+1;
                      if(strTMP.length>0)
                       strTMP+="#";
                      strTMP+=x;
                      if(document.getElementById("txtSDT"+i).value!=null)
                       strTMP+="|"+document.getElementById("txtSDT"+i).value
                       if(document.getElementById("txtEMAIL"+i).value!=null)
                      strTMP+="|" +document.getElementById("txtEMAIL"+i).value
                   
                }
                strThongtin=strTMP;
               
                
            }
            if (strThongtin.length>0)
            {
                 var tbl =strHeader.replace('__DISABLED__',strdisabled);
                var arr = strThongtin.split('#');
                var i=0;
                for(i=0;i<arr.length;i++)
                {
                    var arrtmp=arr[i].split('|');
                    tbl+='<tr>'; 
                    tbl+=' <td  class="form_label"><input type="text" id="STT'+arrtmp[0] +'" value="'+arrtmp[0] +'" CssClass="inputflat txtHoanThien" readonly="readonly" /> </td>';
                    tbl+='<td  class="form_label"><input type="text" id="txtSDT'+arrtmp[0] +'" value="'+arrtmp[1] +'" style="width:90%" "'+strdisabled+' onkeypress="return OnKeyPress(event);" onblur="OnSave()" /> </td>';
                    tbl+='<td  class="form_label"><input type="text" id="txtEMAIL'+arrtmp[0] +'" value="'+arrtmp[2] +'" style="width:90%" '+strdisabled+' onblur="OnSave()" /> </td>';
                    tbl+='<td  class="form_label"><input type="button" id="Button2" value="Xóa" onclick="delThongTin('+arrtmp[0] +')" style="width:90%" '+strdisabled+' /></td>';
                
                    tbl+='</tr>'; 
                }
                i=i+1;
                 tbl+='<tr>'; 
                    tbl+=' <td  class="form_label"><input type="text" id="STT'+i +'" value="'+i+'" CssClass="inputflat txtHoanThien" readonly="readonly" /> </td>';
                    tbl+='<td  class="form_label"><input type="text" id="txtSDT'+i+'" value="" style="width:90%" '+strdisabled+' onkeypress="return OnKeyPress(event);" onblur="OnSave()" /> </td>';
                    tbl+='<td  class="form_label"><input type="text" id="txtEMAIL'+i+'" value="" style="width:90%" '+strdisabled+' onblur="OnSave()" /> </td>';
                    tbl+='<td  class="form_label"><input type="button" id="Button2" value="Xóa" onclick="delThongTin('+i+')" style="width:90%" '+strdisabled+' /></td>';
                  
                    tbl+='</tr>'; 
                document.getElementById('divThongtin').innerHTML=tbl+strFooter;
                strThongtin +='#'+i+'||';   
            }else
            {
                 var tbl =strHeader.replace('__DISABLED__',strdisabled);
               
                 tbl+='<tr>'; 
                    tbl+=' <td  class="form_label"><input type="text" id="STT1" value="1" CssClass="inputflat txtHoanThien" readonly="readonly" /> </td>';
                    tbl+='<td  class="form_label"><input type="text" id="txtSDT1" value="" style="width:90%" '+strdisabled+' onkeypress="return OnKeyPress(event);" onblur="OnSave()" /> </td>';
                    tbl+='<td  class="form_label"><input type="text" id="txtEMAIL1" value="" style="width:90%" '+strdisabled+' onblur="OnSave()" /> </td>';
                    tbl+='<td  class="form_label"><input type="button" id="Button2" value="Xóa" onclick="delThongTin(1)" onblur="OnSave()" style="width:90%" '+strdisabled+' /></td>';
                 
                    tbl+='</tr>'; 
                document.getElementById('divThongtin').innerHTML=tbl+strFooter;
                strThongtin +='#1||'; 
            }
            
         }
         function delThongTin(pos)
         {
       
            if (strThongtin.length>0)
            {
                var strTMP="";
              //  var tbl =strHeader;
                var arr = strThongtin.split('#');
                var i=1;
                var x=0;
                for(i=1;i<=arr.length;i++)
                {
                   if(i!=pos)
                   {
                      x=x+1;
                      if(strTMP.length>0)
                       strTMP+="#";
                      strTMP+=x;
                      if(document.getElementById("txtSDT"+i).value!=null)
                       strTMP+="|"+document.getElementById("txtSDT"+i).value
                       if(document.getElementById("txtEMAIL"+i).value!=null)
                      strTMP+="|" +document.getElementById("txtEMAIL"+i).value
                   }
                }
                strThongtin=strTMP;
                document.getElementById('<%=txtThongTin.ClientID %>').value=strThongtin;
                showThongtin();
                
            }
         }
         function OnSave()
         {
            var strTMP="";
            if (strThongtin.length>0)
            {
               
              //  var tbl =strHeader;
                var arr = strThongtin.split('#');
                var i=1;
                var x=0;
                for(i=1;i<=arr.length;i++)
                {
                   
                      x=x+1;
                      if(strTMP.length>0)
                       strTMP+="#";
                      strTMP+=x;
                      if(document.getElementById("txtSDT"+i).value!=null)
                       strTMP+="|"+document.getElementById("txtSDT"+i).value
                       if(document.getElementById("txtEMAIL"+i).value!=null)
                      strTMP+="|" +document.getElementById("txtEMAIL"+i).value
                   
                }
              
               
                
            }
            strThongtin=strTMP;
            document.getElementById('<%=txtThongTin.ClientID %>').value=strThongtin;
         }

    
    </script>

    <style type="text/css">
        .txtHoanThien
        {
            width: 80%;
        }
        .container
        {
            width: 100%;
            padding-bottom: 50px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <p class="pageTitle">
        Hoàn thiện thông tin người NTDT</p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
            <asp:HiddenField runat="server" ID="txtID" />
            <asp:HiddenField runat="server" ID="txtThongTin" />
            <asp:HiddenField runat="server" ID="txtstrdisabled" />
            <asp:HiddenField ID="dieuChinhConfirm" runat="server" Value="" />
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Số hồ sơ
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtSO_HS" runat="server" MaxLength="20" CssClass="inputflat txtHoanThien"
                        ReadOnly="true" Enabled="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <asp:HiddenField runat="server" ID="txtLoai_HS" />
                <td align="left" style="width: 25%" class="form_label">
                    Loại hồ sơ<span class="requiredField"> (*)</span>
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtTenLoai_HS" Text="Khai mới" MaxLength="20" runat="server" CssClass="inputflat txtHoanThien"
                        ReadOnly="true" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Mã số thuế <span class="requiredField">(*)</span>
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtMA_DV" runat="server" CssClass="inputflat txtHoanThien" MaxLength="14"
                        Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Tên đơn vị XNK<span class="requiredField">(*)</span>
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtTEN_DV" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Địa chỉ đơn vị XNK<span class="requiredField">(*)</span>
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtDIACHI" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Họ tên NNT<span class="requiredField">(*)</span>
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtHO_TEN" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Số chứng minh thư<span class="requiredField">(*)</span>
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtSO_CMT" runat="server" onkeypress="return OnKeyPress(event);" CssClass="inputflat txtHoanThien" MaxLength="10"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Ngày sinh<span class="requiredField">(*)</span>
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtNGAYSINH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="10"></asp:TextBox>
                    <span style="color: red;">&nbsp;&nbsp;&nbsp;(Ngày/Tháng/Năm)</span>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Nguyên quán<span class="requiredField">(*)</span>
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtNGUYENQUAN" runat="server" CssClass="inputflat txtHoanThien"
                        MaxLength="255"></asp:TextBox>
                </td>
            </tr>
            <tr style="display: none;">
                <td align="left" style="width: 25%" class="form_label">
                    Nhập số điện thoại
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtSO_DT" runat="server" CssClass="inputflat txtHoanThien" MaxLength="15"></asp:TextBox>&nbsp;&nbsp;&nbsp;<asp:Button
                        runat="server" ID="btnLuuSoDT" Text="Lưu số điện thoại" CssClass="buttonField"
                        Visible="false" />
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Thông tin liên hệ
                </td>
                <td class="form_control" style="text-align: left">
                    <div id="divThongtin">
                        <table width="80%" cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
                            <tr>
                                <td class="form_label">
                                    STT
                                </td>
                                <td class="form_label">
                                    Số điện thoại
                                </td>
                                <td class="form_label">
                                    Email
                                </td>
                                <td class="form_label">
                                    <input type="button" id="btnAdd" value="Add" />
                                    <input type="button" id="Button1" value="Save" />
                                </td>
                            </tr>
                            <tr>
                                <td class="form_label">
                                    <input type="hidden" id="STT1" value="1" />1
                                </td>
                                <td class="form_label">
                                    <input type="text" id="txtSDT1" value="1" />
                                </td>
                                <td class="form_label">
                                    <input type="text" id="txtEMAIL1" value="1" />
                                </td>
                                <td class="form_label">
                                    <input type="button" id="Button2" value="Del" onclick="delThongTin(1)" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr style="display: none">
                <td align="left" style="width: 25%" class="form_label">
                    Số điện thoại
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:GridView ID="GridSoDT" runat="server" Width="80%" DataKeyNames="ID" ShowHeader="false"
                        AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="ID" Visible="false" />
                            <asp:BoundField DataField="MA_DV" Visible="false" />
                            <asp:TemplateField HeaderText="Tên Email">
                                <ItemTemplate>
                                    <asp:Label ID="lblCongTy" runat="server" Text='<%#Eval("SO_DT") %>'></asp:Label></ItemTemplate>
                                <ItemStyle Width="40%" CssClass="form_label" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <%--<EditItemTemplate>
                                    <asp:Button ID="ButtonUpdate" runat="server" CommandName="Update"  Text="Lưu"  />
                                    <asp:Button ID="ButtonCancel" runat="server" CommandName="Cancel"  Text="Không lưu" />
                                </EditItemTemplate>--%>
                                <ItemTemplate>
                                    <%--<asp:Button ID="ButtonEdit" runat="server" CommandName="Edit"  Text="Sửa"  />--%>
                                    <asp:Button ID="ButtonDeleteSoDT" runat="server" CommandName="Delete" Text="Xóa"
                                        Font-Bold="true" />
                                </ItemTemplate>
                                <ItemStyle CssClass="form_label" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr style="display: none">
                <td align="left" style="width: 25%" class="form_label">
                    Nhập Email
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtEMAIL" runat="server" CssClass="inputflat txtHoanThien" MaxLength="50"
                        ReadOnly="true"></asp:TextBox>&nbsp;&nbsp;&nbsp;<asp:Button runat="server" ID="btnLuuEmail"
                            Text="Lưu Email" CssClass="buttonField" Visible="false" />
                </td>
            </tr>
            <tr style="display: none">
                <td align="left" style="width: 25%" class="form_label">
                    Email
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:GridView ID="GridEmail" runat="server" Width="80%" DataKeyNames="ID" ShowHeader="false"
                        AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="ID" Visible="false" />
                            <asp:BoundField DataField="MA_DV" Visible="false" />
                            <asp:TemplateField HeaderText="Tên Email">
                                <ItemTemplate>
                                    <asp:Label ID="lblCongTy" runat="server" Text='<%#Eval("EMAIL") %>'></asp:Label></ItemTemplate>
                                <ItemStyle Width="40%" CssClass="form_label" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <%--<EditItemTemplate>
                                    <asp:Button ID="ButtonUpdate" runat="server" CommandName="Update"  Text="Lưu"  />
                                    <asp:Button ID="ButtonCancel" runat="server" CommandName="Cancel"  Text="Không lưu" />
                                </EditItemTemplate>--%>
                                <ItemTemplate>
                                    <%--<asp:Button ID="ButtonEdit" runat="server" CommandName="Edit"  Text="Sửa"  />--%>
                                    <asp:Button ID="ButtonDeleteEmail" runat="server" CommandName="Delete" Text="Xóa"
                                        Font-Bold="true" />
                                </ItemTemplate>
                                <ItemStyle CssClass="form_label" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Số serial của chứng thư số
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtSERIALNUMBER" runat="server" CssClass="inputflat txtHoanThien"
                        MaxLength="40" ReadOnly="true" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Đơn vị cấp chứng thư số
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtNOICAP" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255"
                        ReadOnly="true" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Ngày hiệu lực chứng thư số
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtNGAY_HL" runat="server" CssClass="inputflat txtHoanThien" MaxLength="19"
                        ReadOnly="true" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Ngày hết hiệu lực chứng thư số
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtNGAY_HHL" runat="server" CssClass="inputflat txtHoanThien" MaxLength="19"
                        ReadOnly="true" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Mã ngân hàng<span class="requiredField">(*)</span>
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtMA_NH_TH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="7"
                        ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Tên ngân hàng<span class="requiredField">(*)</span>
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtTEN_NH_TH" runat="server" CssClass="inputflat txtHoanThien" MaxLength="255"
                        ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Số tài khoản đăng kí
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtTAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien"
                        MaxLength="50" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Tên tài khoản đăng kí
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtTEN_TAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien"
                        MaxLength="255" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Ngày gửi
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtNGAY_NHAN311" runat="server" CssClass="inputflat txtHoanThien"
                        MaxLength="19" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Ngày hiệu lực <span class="requiredField">(*)</span>
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtNGAY_KY_HDUQ" runat="server" CssClass="inputflat txtHoanThien"
                        MaxLength="10"></asp:TextBox>
                    <span style="color: red;">&nbsp;&nbsp;&nbsp;(Ngày/Tháng/Năm)</span>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Check số tài khoản <span class="requiredField">(*)</span>
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtCHK_TAIKHOAN_TH" runat="server"
                        CssClass="inputflat txtHoanThien" MaxLength="50"></asp:TextBox><asp:Button ID="btnCheckTK"
                            runat="server" Text="Check TK" CssClass="buttonField" />
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Tên tài khoản <span class="requiredField">(*)</span>
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtCHK_TEN_TAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThien"
                        MaxLength="255" ReadOnly="true"></asp:TextBox>
                    <asp:Button ID="btnCheckCK" runat="server" Text="Check CK" CssClass="buttonField"
                        Visible="false" />
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Số tài khoản sử dụng NTDT
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:GridView ID="grdTKNH" runat="server" Width="80%" DataKeyNames="ID" ShowHeader="false"
                        AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundField DataField="ID" Visible="false" />
                            <asp:TemplateField HeaderText="Tài khoản ngân hàng">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtSoTKNH_Edit" runat="server" Text=''></asp:TextBox></EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblSoTKNH" runat="server" Text=''></asp:Label></ItemTemplate>
                                <ItemStyle Width="40%" CssClass="form_label" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tên tài khoản">
                                <ItemTemplate>
                                    <asp:Label ID="lblCongTy" runat="server" Text=''></asp:Label></ItemTemplate>
                                <ItemStyle Width="40%" CssClass="form_label" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="ButtonDelete" runat="server" CommandName="Delete" Text="Xóa" Font-Bold="true" />
                                </ItemTemplate>
                                <ItemStyle CssClass="form_label" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr style="display: none">
                <td align="left" style="width: 25%" class="form_label">
                    Nội dung hướng dẫn NNT
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtNoiDung_XL" runat="server" CssClass="inputflat txtHoanThien"
                        TextMode="MultiLine" Height="30" MaxLength="4000"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left" style="width: 25%" class="form_label">
                    Lý do chuyển trả
                </td>
                <td class="form_control" style="text-align: left">
                    <asp:TextBox ID="txtLY_DO_CHUYEN_TRA" runat="server" CssClass="inputflat txtHoanThien"
                        MaxLength="255" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                    <asp:HiddenField ID="hdfNNTDT_ID" runat="server" />
                    <asp:HiddenField ID="hdrTRANG_THAI" runat="server" />
                    <asp:HiddenField ID="hdfMA_CN" runat="server" />
                    <asp:HiddenField ID="hdfMA_NV" runat="server" />
                    <asp:HiddenField ID="hdrMax_SoTKNH" runat="server" />
                    <asp:HiddenField ID="hdfCifno" runat="server" />
                    <asp:HiddenField ID="hdfMA_CNTK" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <asp:Button ID="btnTaoMoi" runat="server" CssClass="buttonField" Text="Tạo mới" Enabled="true" />&nbsp;&nbsp;
                    <asp:Button ID="btnChuyenKS" runat="server" CssClass="buttonField" Text="Ghi và chuyển KS" />&nbsp;&nbsp;
                    <asp:Button ID="btnDieuChinh" runat="server" CssClass="buttonField" Enabled="false"
                        Text="Điều chỉnh" Width="90px" />&nbsp;&nbsp;
                    <asp:Button ID="btnHuyDK" runat="server" CssClass="buttonField" Enabled="false" Text="ĐK Ngừng"
                        Width="90px" />&nbsp;&nbsp;
                    <asp:Button ID="btnExit" runat="server" CssClass="buttonField" Text="Thoát" Width="90px" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    <!-- required plugins -->

    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>

    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->
    <!-- jquery.datePicker.js -->

    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>

    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtNgayGui").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
        strThongtin= document.getElementById('<%=txtThongTin.ClientID %>').value;
        strdisabled=document.getElementById('<%=txtstrdisabled.ClientID %>').value ;
        showThongtin();
    </script>

</asp:Content>
