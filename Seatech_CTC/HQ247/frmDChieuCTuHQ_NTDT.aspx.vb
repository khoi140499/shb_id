﻿Imports Business_HQ.Common.mdlCommon
Imports System.Data
Partial Class HQ247_frmDChieuCTuHQ_NTDT
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If

        If Not IsPostBack Then
            txtNgayDC.Text = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            LoadTTDCTheoNgay(ConvertDateToNumber(txtNgayDC.Text))
        End If
    End Sub
    Public Shared Sub ShowMessageBoxConfirmDoiChieu(ByVal pPage As System.Web.UI.Page, ByVal pMessage As String, ByVal strDate As String)
        Dim strScript As String = "<script language=JavaScript>"
        strScript += "var result = confirm('" & pMessage & "');"
        strScript += "if (result) {"
        strScript += " document.getElementById('ctl00_MstPg05_MainContent_dieuChinhConfirm').value = 'OK';"
        strScript += " document.getElementById('ctl00_MstPg05_MainContent_cmdDC').click();"
        strScript += "}else { "
        strScript += " document.getElementById('ctl00_MstPg05_MainContent_dieuChinhConfirm').value = '';}"
        strScript += "</script>"
        If Not pPage.ClientScript.IsStartupScriptRegistered("clientScript") Then
            pPage.ClientScript.RegisterStartupScript(GetType(String), "clientScript", strScript)
        End If
    End Sub
    
    Protected Sub cmdDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDC.Click
        LogApp.AddDebug("cmdDC_Click", "DOI CHIEU XAC NHAN NOP THUE: Gui bang ke doi chieu")
        Dim strErrMessage As String = ""
        Dim strMessage As String = ""
        Dim strNgayDC As String = txtNgayDC.Text
        
        Try
            'Validate
            lblError.Text = ""
            lblSuccess.Text = ""
            Dim dtNgayDC As Date
            Dim strErrorNum As String = ""
            Dim strErrorMes As String

            If strNgayDC = "" Then
                lblError.Text = "Bạn chưa nhập ngày đối chiếu"
                txtNgayDC.Focus()
                Return
            End If
            dtNgayDC = DateTime.ParseExact(strNgayDC, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat)
            strErrorNum = ""
            strErrorMes = ""
            If TestDoiChieuLai(txtNgayDC.Text) And dieuChinhConfirm.Value <> "OK" Then

                ShowMessageBoxConfirmDoiChieu(Me, "Bảng kê đã đuợc gửi thành công truớc đó. Bạn có muốn gửi lai bảng kê không ?", txtNgayDC.Text)
                Return

            Else
                dieuChinhConfirm.Value = ""
                'Gui bang ke doi chieu
                CustomsServiceV3.ProcessMSG.GuiTDDoiChieuThuePhi_HQ_NTDT(dtNgayDC, strErrorNum, strErrorMes)
                'Xu lý xac nhan cua HQ
                If strErrorNum <> "0" Then
                    strErrMessage = "Gửi bảng kê không thành công (Mã lỗi: " + strErrorNum + "; Mô tả: " + strErrorMes + ")"
                Else
                    strErrMessage = "Đã gửi bảng kê " + " thành công"
                End If
            End If

        Catch ex As Exception
            strErrMessage = ex.Message
        End Try
        'ResetField()
        If strErrMessage <> "" Then
            lblError.Text = strErrMessage
        Else
            Response.Redirect(Request.Url.AbsolutePath.ToString() & "?msg=" & strMessage & "&errmsg=" & strErrMessage, False)
        End If
        LoadTTDCTheoNgay(ConvertDateToNumber(txtNgayDC.Text))
    End Sub
    ' <System.Web.Services.WebMethod()> _
    Public Shared Function TestDoiChieuLai(ByVal strDate As String) As Boolean
        Dim dt As New DataTable
        Dim strSQL As String
        Dim strTrangThai As String = ""
        strDate = ConvertDateToNumber(strDate)
        strSQL = "   SELECT A.* FROM TCS_QLY_DC A   ,( SELECT MAX(ID) ID     FROM   TCS_QLY_DC"
        strSQL &= "  WHERE   TO_CHAR (NGAY_DC, 'YYYYMMDD') = '" & strDate & "'" & " AND LOAI_GD='807' AND ERRNUM='0' "
        strSQL &= "  GROUP BY   LOAI_GD) B WHERE  TO_CHAR (A.NGAY_DC, 'YYYYMMDD') = '" & strDate & "' AND  A.ID=B.ID"
        Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)

        If Not ds Is Nothing And ds.Tables.Count > 0 And ds.Tables(0).Rows.Count > 0 Then
            Return True
        End If
        Return False
    End Function
    Protected Sub cmdNhanKQDC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNhanKQDC.Click
        LogApp.AddDebug("cmdNhanKQDC_Click", "DOI CHIEU XAC NHAN NOP THUE: Nhan ket qua doi chieu")
        Dim strErrMessage As String = ""
        Dim strMessage As String = ""
        Try
            Dim strNgayDC As String = txtNgayDC.Text
            Dim dtNgayDC As Date = DateTime.ParseExact(strNgayDC, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat)

            lblError.Text = ""
            lblSuccess.Text = ""
            Dim strErrorNum As String = ""
            Dim strErrorMes As String = ""
            'Nhan ket qua doi chieu tu HQ

            Dim strGDNhanDL As String = ""

            CustomsServiceV3.ProcessMSG.getKetQuaDC_HQ247(dtNgayDC, strErrorNum, strErrorMes)




            'Xu ly xac nhan HQ
            If strErrorNum = "0" Then
                strErrMessage = "Đã gửi bảng kê (807) và nhận dữ liệu đối chiếu (" + strGDNhanDL + ") từ Hải Quan thành công."
            Else
                strErrMessage = "Nhận dữ liệu đối chiếu từ Hải Quan không thành công (Lỗi từ HQ: " + strErrorNum + " - " + strErrorMes + ")"
            End If

        Catch ex As Exception
            strErrMessage = ex.Message
        End Try
        'ResetField()
        If strErrMessage <> "" Then
            lblError.Text = strErrMessage
        Else
            Response.Redirect(Request.Url.AbsolutePath.ToString() & "?msg=" & strMessage & "&errmsg=" & strErrMessage, False)
        End If
        LoadTTDCTheoNgay(ConvertDateToNumber(txtNgayDC.Text))
        'Response.Redirect(Request.Url.AbsolutePath.ToString() & "?msg=" & strMessage & "&errmsg=" & strErrMessage, False)
    End Sub
    Protected Sub LoadTTDCTheoNgay(ByVal strDate As String)
        Dim dt As New DataTable
        Dim strSQL As String
        Dim strTrangThai As String = ""
        Try
            strSQL = "   SELECT A.* FROM TCS_QLY_DC A   ,( SELECT MAX(ID) ID     FROM   TCS_QLY_DC"
            strSQL &= "  WHERE   TO_CHAR (NGAY_DC, 'YYYYMMDD') = '" & strDate & "'"
            strSQL &= "  GROUP BY   LOAI_GD) B WHERE  TO_CHAR (A.NGAY_DC, 'YYYYMMDD') = '" & strDate & "' AND  A.ID=B.ID"
            Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)

            If Not ds Is Nothing And ds.Tables.Count > 0 And ds.Tables(0).Rows.Count > 0 Then
                For Each row As DataRow In ds.Tables(0).Rows
                    If row("LOAI_GD").ToString() = "807" Then
                        strTrangThai = "Y"
                        If row("ERRNUM").ToString() = "0" Then
                            lblSend41.Text = "Gửi thành công"
                        ElseIf row("ERRNUM").ToString() <> "0" Then
                            lblSend41.Text = "Gửi không thành công"
                        End If
                    ElseIf row("LOAI_GD").ToString() = "857" Then
                        If row("ERRNUM").ToString() = "0" Then
                            lblReceive44.Text = "Nhận thành công"
                        ElseIf row("ERRNUM").ToString() <> "0" Then
                            lblReceive44.Text = "Nhận không thành công"
                        End If
                    End If
                Next
            Else
                lblSend41.Text = "Chưa đối chiếu"
                lblReceive44.Text = "Chưa đối chiếu"
            End If
            dtgrd.DataSource = Nothing

            If strTrangThai.Equals("Y") Then
                Dim STRWHERE As String = ""
                Dim STRWHERENHAN As String = ""
                Dim D As DateTime = Business_HQ.Common.mdlCommon.ConvertStringToDate(txtNgayDC.Text, "dd/MM/yyyy")
                Dim strIDREQuest As String = CustomsServiceV3.ProcessMSG.getReqId(D, "807", "22")
                Dim strTimeStart As String = Business_HQ.Common.mdlCommon.TCS_GETTS("GIO_DC_CT")
                If strTimeStart.Length < 0 Then
                    strTimeStart = "160000"
                End If
                strTimeStart = strTimeStart.Replace(":", "")
                STRWHERE = " AND  to_char( A.NGAYTRUYEN_CT,'RRRRMMDDHH24MISS' )> " & D.AddDays(-1).ToString("yyyyMMdd") & strTimeStart
                STRWHERE &= " AND to_char( A.NGAYTRUYEN_CT,'RRRRMMDDHH24MISS' )<= " & D.ToString("yyyyMMdd") & strTimeStart
                STRWHERENHAN = " AND  PARENT_TRANSACTION_ID='" & strIDREQuest & "'"
                Dim dsx As DataSet = Business_HQ.HQ247.daBaoCaoHQ247.TCS_BC_TONG_DC(STRWHERE, STRWHERENHAN)
                If dsx.Tables.Count > 0 Then
                    If dsx.Tables(0).Rows.Count > 0 Then
                        dtgrd.DataSource = dsx.Tables(0)
                    End If
                End If
            End If
            dtgrd.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub cmdTruyVan_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdTruyVan.Click

        LogApp.AddDebug("cmdTruyVan_Click", "DOI CHIEU XAC NHAN NOP THUE HQ247: Truy van doi chieu")
        LoadTTDCTheoNgay(ConvertDateToNumber(txtNgayDC.Text))
    End Sub
End Class
