﻿Imports VBOracleLib
Imports System.Data
Imports Business_HQ.HQ247.NNTDT
Imports Business.Common.mdlCommon
Imports Business.BaoCao
Imports Business.Common
Imports System.IO
Imports System.Xml
Imports CoreBankSV
''' <summary>
''' Hoàn thiện thông tin đăng ký của NNT
''' Chức năng Tạo mới: Chỉ dùng trong trường hợp NNT đăng kí thông tin ủy quyền trích nợ tài khoản tại NHTM 
''' Chức năng Ghi và chuyển KS: sử dụng đối với những đăng ký ở trạng thái ĐÃ DUYỆT YÊU CẦU
''' </summary>
''' <remarks>Số tài khoản phải được kiểm tra và lấy được Tên tài khoản</remarks>
Partial Class frmDKMOINNTDT
    Inherits System.Web.UI.Page
    Private Shared cls_corebank As New CorebankServiceESB.clsCoreBank
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If

        txtMA_DV.Enabled = True
        txtTEN_DV.Enabled = True
        txtDIACHI.Enabled = True
        txtHO_TEN.Enabled = True
        txtSO_CMT.Enabled = True
        txtNGAYSINH.Enabled = True
        txtNGUYENQUAN.Enabled = True
      
        txtMA_NH_TH.Enabled = False
        txtTEN_NH_TH.Enabled = False

        txtCHK_TAIKHOAN_TH.Enabled = True

        txtMA_NH_TH.Enabled = False
        txtTEN_NH_TH.Enabled = False
        txtMA_NH_TH.Text = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString()
        txtTEN_NH_TH.Text = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString()


        If Not IsPostBack Then
            txtNGAY_KY_HDUQ.Text = DateTime.Now.ToString("dd/MM/yyyy")
        End If
    End Sub
   
    ''' <summary>
    ''' Kiểm tra sự tồn tại của tài khoản NNT tại NH
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCheckTK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckTK.Click
        If txtCHK_TAIKHOAN_TH.Text = "" Then
            txtCHK_TAIKHOAN_TH.Focus()
            clsCommon.ShowMessageBox(Me, "Số tài khoản ngân hàng không được để trống")
            Exit Sub
        End If
        Try
            Dim strDSTAIKHOAN As String = hdfDSTAIKHOAN.Value
            Dim intCount As Integer = 0
            If strDSTAIKHOAN.Length > 0 Then
                Dim strarr As String() = strDSTAIKHOAN.Split("#")
                For Each vstr As String In strarr
                    Dim strTemp As String() = vstr.Split("|")
                    If strTemp(1).Equals(txtCHK_TAIKHOAN_TH.Text.Trim()) Then
                        txtCHK_TAIKHOAN_TH.Focus()
                        clsCommon.ShowMessageBox(Me, "Trung Số tài khoản ngân hàng!")
                        Exit Sub
                    End If
                Next
                intCount = strarr.Length
            End If
            Dim objBank As String = cls_corebank.GetTen_TK_KH_NH(txtCHK_TAIKHOAN_TH.Text.Replace("-", ""))
            If Not objBank.Equals("") Then

                intCount += 1
                If strDSTAIKHOAN.Length > 0 Then
                    hdfDSTAIKHOAN.Value = strDSTAIKHOAN & "#" & intCount & "|" & txtCHK_TAIKHOAN_TH.Text.Replace("-", "") & "|" & objBank
                Else
                    hdfDSTAIKHOAN.Value = intCount & "|" & txtCHK_TAIKHOAN_TH.Text.Replace("-", "") & "|" & objBank
                End If
            Else
                clsCommon.ShowMessageBox(Me, "Tai khoan khach hang khong dung ")
            End If
            'If objBank._RETCODE.Equals("0") Then

            '    intCount += 1
            '    If strDSTAIKHOAN.Length > 0 Then
            '        hdfDSTAIKHOAN.Value = strDSTAIKHOAN & "#" & intCount & "|" & objBank._CIFNO & "|" & objBank._BANKACCOUNT & "|" & objBank._ACCOUNTNAME & "|" & objBank._BRANCH_CODE
            '    Else
            '        hdfDSTAIKHOAN.Value = intCount & "|" & objBank._CIFNO & "|" & objBank._BANKACCOUNT & "|" & objBank._ACCOUNTNAME & "|" & objBank._BRANCH_CODE
            '    End If
            'Else
            '    clsCommon.ShowMessageBox(Me, "Tai khoan khach hang khong dung ")
            'End If


        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được tên khách hàng. Lỗi: " & ex.Message)
        End Try
    End Sub

    Protected Sub btnTaoMoi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTaoMoi.Click
        clearData()
    End Sub
    ''' <summary>
    ''' Hoàn thiện thông tin NNT và chuyển sang cho KSV
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnChuyenKS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChuyenKS.Click
        Try
            Dim strThongTinLineHe As String = ""
            Dim strEmail As String = ""
            Dim strSO_DT As String = ""
            Dim intCount As Integer = 0
            Dim intDSCount As Integer = 0
            If Session.Item("User") Is Nothing Then
                Response.Redirect("~/pages/frmLogin.aspx", False)
                Exit Sub
            End If
            If txtMA_DV.Text.Trim().Length <= 0 Then
                txtMA_DV.Focus()
                clsCommon.ShowMessageBox(Me, "Chưa nhập Mã số thuế")
                Exit Sub
            End If
            If txtTEN_DV.Text.Trim().Length <= 0 Then
                txtTEN_DV.Focus()
                clsCommon.ShowMessageBox(Me, "Chưa nhập Tên đơn vị")
                Exit Sub
            End If
            If txtDIACHI.Text.Trim().Length <= 0 Then
                txtDIACHI.Focus()
                clsCommon.ShowMessageBox(Me, "Chưa nhập Địa chỉ")
                Exit Sub
            End If
            If txtSO_CMT.Text.Trim().Length <= 0 Then
                txtSO_CMT.Focus()
                clsCommon.ShowMessageBox(Me, "Chưa nhập Số CMT")
                Exit Sub
            End If
            If txtHO_TEN.Text.Trim().Length <= 0 Then
                txtHO_TEN.Focus()
                clsCommon.ShowMessageBox(Me, "Chưa nhập Họ tên")
                Exit Sub
            End If
            If txtNGAYSINH.Text.Trim().Length <= 0 Then
                txtNGAYSINH.Focus()
                clsCommon.ShowMessageBox(Me, "Chưa nhập Ngày Sinh")
                Exit Sub
            End If
            If txtNGUYENQUAN.Text.Trim().Length <= 0 Then
                txtNGUYENQUAN.Focus()
                clsCommon.ShowMessageBox(Me, "Chưa nhập Nguyên Quán")
                Exit Sub
            End If
            If txtNGAY_KY_HDUQ.Text.Trim().Length <= 0 Then
                txtNGAY_KY_HDUQ.Focus()
                clsCommon.ShowMessageBox(Me, "Chưa nhập Ngày ký ủy quyền trích nợ")
                Exit Sub
            End If

            If hdfDSTAIKHOAN.Value = "" Then
                txtCHK_TAIKHOAN_TH.Focus()
                clsCommon.ShowMessageBox(Me, "Chưa nhập Số tài khoản ngân hàng")
                Exit Sub
            End If
            '20180119 Nham Sua Them dieu kien ngay hieu luc---------------------------------
            If txtNGAY_KY_HDUQ.Text <> "" Then
                Try
                    Dim isDate As Date = DateTime.ParseExact(txtNGAY_KY_HDUQ.Text, "dd/MM/yyyy", Nothing)
                    If isDate.Date < DateTime.Now.Date Then
                        txtNGAY_KY_HDUQ.Focus()
                        clsCommon.ShowMessageBox(Me, "Ngày hiệu lực phải lớn hơn hoặc bằng ngày hiện tại ")
                        Exit Sub
                    End If
                Catch ex As Exception
                    txtNGAY_KY_HDUQ.Focus()
                    clsCommon.ShowMessageBox(Me, "Ngày hiệu lực phải đúng định dạng dd/mm/yyyy")
                    Exit Sub
                End Try

            End If
            '--------------------------------
            'Dim a = txtThongTin.Value
            If txtThongTin.Value.Length <= 3 Then   'Nhamltl_Sửa phải lớn hơn 3
                clsCommon.ShowMessageBox(Me, "Chưa nhập Thông tin liên hệ ")
                Exit Sub
            Else
                Dim strDataLienHe As String = txtThongTin.Value
                Dim StrArrTMP As String() = strDataLienHe.Split("#")
                Dim x1 As Integer = 0
                Dim x2 As Integer = 0
                For i As Integer = 0 To StrArrTMP.Length - 1
                    Dim strTMPAS As String() = StrArrTMP(i).Split("|")
                    If (strTMPAS(1).Length > 0 And strTMPAS(2).Length > 0) Then
                        strThongTinLineHe += "<ThongTinLienHe><So_DT>" & strTMPAS(1) & "</So_DT><Email>" & strTMPAS(2) & "</Email></ThongTinLienHe>"
                        If (x1 = 0) And strTMPAS(1).Length > 0 Then
                            x1 = 1
                            strSO_DT = strTMPAS(1)
                        End If
                        If (x2 = 0) And strTMPAS(2).Length > 0 Then
                            x2 = 1
                            strEmail = strTMPAS(2)
                        End If
                    Else
                        clsCommon.ShowMessageBox(Me, "Thong tin lien he dong " & (i + 1) & " thieu thong tin dien thoai hoac email")
                        Exit Sub
                    End If
                Next
                Dim strDSTAIKHOAN As String = hdfDSTAIKHOAN.Value
                'DOAN NAY DANH CHO INSERT

                If strDSTAIKHOAN.Length > 0 Then
                    Dim strarr As String() = strDSTAIKHOAN.Split("#")
                    intDSCount = strarr.Length
                    For Each vstr As String In strarr
                        Dim strTemp As String() = vstr.Split("|")
                        'Dim strSQL As String = "SELECT SO_HS FROM TCS_DM_NTDT_HQ_311 WHERE MA_DV='" & txtMA_DV.Text.Trim() & "' AND CUR_TAIKHOAN_TH='" & strTemp(2) & "' AND SO_CMT='" & txtSO_CMT.Text.Trim() & "'"
                        'strSQL &= "UNION ALL SELECT SO_HS FROM TCS_DM_NTDT_HQ WHERE MA_DV='" & txtMA_DV.Text.Trim() & "' AND CUR_TAIKHOAN_TH='" & strTemp(2) & "' AND TRANGTHAI<>'15' AND SO_CMT='" & txtSO_CMT.Text.Trim() & "'"
                        Dim strSQL As String = "SELECT SO_HS FROM TCS_DM_NTDT_HQ_311 WHERE MA_DV='" & txtMA_DV.Text.Trim() & "' AND CUR_TAIKHOAN_TH='" & strTemp(1) & "'"
                        strSQL &= "UNION ALL SELECT SO_HS FROM TCS_DM_NTDT_HQ WHERE MA_DV='" & txtMA_DV.Text.Trim() & "' AND CUR_TAIKHOAN_TH='" & strTemp(1) & "' AND TRANGTHAI<>'15' "
                        Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
                        If Not ds Is Nothing Then
                            If ds.Tables.Count > 0 Then
                                If ds.Tables(0).Rows.Count > 0 Then
                                    txtCHK_TAIKHOAN_TH.Focus()
                                    clsCommon.ShowMessageBox(Me, "Mã đơn vị " & txtMA_DV.Text.Trim() & " Tài khoản" & strTemp(1) & " đã có đăng ký trong hệ thống!")
                                    Exit Sub
                                End If
                            End If
                        End If

                    Next
                    'DOAN NAY DE THUC HIEN MAKER 312
                    For Each vstr As String In strarr
                        Dim strTemp As String() = vstr.Split("|")
                        Dim decErrID As Decimal = Business_HQ.HQ247.TCS_DM_NTDT_HQ.MakerMSG312H(txtMA_DV.Text.Trim(), txtTEN_DV.Text.Trim(), txtDIACHI.Text.Trim(), txtSO_CMT.Text.Trim(), txtHO_TEN.Text, txtNGAYSINH.Text, txtNGUYENQUAN.Text, strSO_DT, strEmail, "", "", strTemp(1), strTemp(2), txtMA_NH_TH.Text, txtTEN_NH_TH.Text, Session.Item("UserName").ToString, strTemp(1), strTemp(2), txtNGAY_KY_HDUQ.Text, strThongTinLineHe, strTemp(1), "00")
                        If decErrID = 0 Then
                            intCount += 1
                        End If
                    Next

                End If
            End If
            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát thành công " & intCount & " / " & intDSCount)
            If (intCount = intDSCount) Then
                clearData()
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Chuyển kiểm soát lỗi: " & ex.Message)
        End Try
    End Sub
    Private Sub clearData()
        txtMA_DV.Text = ""
        txtTEN_DV.Text = ""
        txtDIACHI.Text = ""
        txtHO_TEN.Text = ""
        txtSO_CMT.Text = ""
        txtNGAYSINH.Text = ""
        txtNGUYENQUAN.Text = ""
        txtCHK_TAIKHOAN_TH.Text = ""
        hdfDSTAIKHOAN.Value = ""
        txtThongTin.Value = ""
    End Sub






End Class
