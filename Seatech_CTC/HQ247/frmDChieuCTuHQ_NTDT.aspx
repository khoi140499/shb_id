﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"   CodeFile="frmDChieuCTuHQ_NTDT.aspx.vb" Inherits="HQ247_frmDChieuCTuHQ_NTDT" Title="ĐỐI CHIẾU" %>
<%@ Import Namespace="Business_HQ.Common" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var errorMSG;
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
                }
                textbox.value = str
        }

        
    </script>

    <script language="javascript" src="../javascript/CheckDate.js" type="text/javascript"></script>

    <script language="javascript" src="../javascript/popup.js" type="text/javascript"></script>

    <script language="javascript" src="../javascript/popcalendar.js" type="text/javascript"></script>

    <style type="text/css">
        .ButtonCommand
        {
            height: 26px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
 <asp:HiddenField ID="dieuChinhConfirm" runat="server" Value="" />
    <table cellspacing="0" cellpadding="0" width="100%" border="0" >
        <tr>
            <td valign="top">
                <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="pageTitle">
                            <asp:Label ID="Label1" runat="server">ĐỐI CHIỀU CUỐI NGÀY</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="errorMessage">
                            <asp:Label ID="lblError" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: Blue" align="left">
                            <asp:Label ID="lblSuccess" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">
                            <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                                <tr>
                                    <td>
                                        <table id="Table2" cellpadding="2" cellspacing="1" class="form_input" width="100%">
                                            <tr align="left">
                                                <td class="form_label" style="width: 33%">
                                                    <asp:Label ID="Label10" runat="server" Text="Ngày đối chiếu" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="form_control" style="width: 51%">
                                                    <span style="display:block; width:150px; position:relative;vertical-align:middle;"><asp:TextBox runat="server" ID="txtNgayDC"  CssClass="inputflat date-pick dp-applied" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"
                                                        onblur="CheckDate(this);" MaxLength="10" ></asp:TextBox>
                                                        </span>
                                                   
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="middle" align="center" style="height: 40px">
                            <asp:Button ID="cmdDC" runat="server" CssClass="ButtonCommand" Text="[Gửi bảng kê ĐC]" />&nbsp;&nbsp;
                            <asp:Button ID="cmdNhanKQDC" runat="server" CssClass="ButtonCommand" Text="[Nhận kết quả ĐC]"  />&nbsp;&nbsp;
                            <asp:Button ID="cmdExit" runat="server" CssClass="ButtonCommand" Visible="false"
                                Text="[Thoát]"></asp:Button>&nbsp;&nbsp;
                                <asp:Button ID="cmdTruyVan" runat="server" CssClass="ButtonCommand" Text="[Truy vấn ĐC]" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
          <tr>
            <td valign="top"  >
                <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
                    <tr>
                        <td colspan="3">
                            <b>THÔNG TIN ĐỐI CHIẾU</b>
                        </td>
                    </tr>
                    <tr align="left">
                        <td rowspan="2" width="30%" class="form_label">
                            <asp:Label ID="Label7" runat="server" Text="Xác nhận nộp thuế XNK" CssClass="label"></asp:Label>
                        </td>
                        <td width="20%" class="form_label">
                            <asp:Label ID="Label5" runat="server" Text="Gửi bảng kê" CssClass="label"></asp:Label>
                        </td>
                        <td width="40%" class="form_label">
                            <asp:Label ID="lblSend41" runat="server" Text="Chưa đối chiếu" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    <tr align="left" >
                        <td class="form_label">
                            <asp:Label ID="Label8" runat="server" Text="Nhận kế quả ĐC" CssClass="label"></asp:Label>
                        </td>
                        <td class="form_label">
                            <asp:Label ID="lblReceive44" runat="server" Text="Chưa đối chiếu" CssClass="label"></asp:Label>
                        </td>
                    </tr>
                    
                </table>
            </td>
        </tr>
          <tr style="display:none">
            <td valign="top"  >
             <asp:GridView ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="100%" BorderColor="Black"    >
            <Columns>
            
                <asp:BoundField HeaderText="STT" DataField="STT" Visible="true" ReadOnly="true" />
                <asp:BoundField HeaderText="Ma CQT" DataField="MCQTHU" Visible="true" ReadOnly="true" ItemStyle-HorizontalAlign="Center"   />              
                <asp:BoundField DataField="SMONTRUYEN" HeaderText="Số Món Truyền" 
                    ReadOnly="True"  />
                <asp:BoundField DataField="STIENTRUYEN" HeaderText="Số Tiền truyền" 
                    ReadOnly="True"  />
                     <asp:BoundField DataField="SMONNHAN" HeaderText="Số Món Nhận" 
                    ReadOnly="True"  />
                <asp:BoundField DataField="STIENNHAN" HeaderText="Số Tiền Nhận" 
                    ReadOnly="True"  />
                    
                 <asp:BoundField DataField="CLECH" HeaderText="Chênh Lệch " 
                    ReadOnly="True"  />
      
              
                
            </Columns>
      
            <HeaderStyle BackColor="#999966" BorderColor="Black" BorderStyle="Solid" 
                BorderWidth="1px" Font-Bold="True" />
            <AlternatingRowStyle BackColor="#EFEDE4" />
           
        </asp:GridView>
            </td>
            </tr>
    </table>
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            
        });
    </script>		
</asp:Content>
