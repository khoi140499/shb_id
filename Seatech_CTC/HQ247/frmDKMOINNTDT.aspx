﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" CodeFile="frmDKMOINNTDT.aspx.vb" Inherits="frmDKMOINNTDT" title="Hoàn thiện thông tin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
        function deleteConfirm(pubid) {
            var result = confirm('Bạn có thực sự muốn xóa tài khoản ' + pubid + ' ?');
            if (result) {
                return true;
            }
            else {
                return false;
            }
        }
        function OnKeyPress(e) {  
                 var k = (window.event ? e.keyCode : e.which);          
                
                 return ((k > 47 && k < 58)  || k==0 ||k==8 );       
        }
        var strdisabled='';
        var strThongtin ='';
        //alert(strThongtin);
        var strHeader=' <table width="99%"  cellpadding="2" cellspacing="1" border="0"  class="form_input">';
            strHeader +='<tr><td  class="form_label" style="width:30px;">STT</td><td  class="form_label">Số điện thoại</td><td  class="form_label">Email</td>';
            strHeader +='<td  class="form_label" style="width:70px;"><input type="button" id="btnAdd" value="Thêm" style="width:60px;" onclick="AddThongtin()" __DISABLED__  /> </td></tr>';
         var strFooter = '</table>';
         function showThongtin()
         {
            strdisabled=document.getElementById('<%=txtstrdisabled.ClientID %>').value ;
               strThongtin=document.getElementById('<%=txtThongTin.ClientID %>').value;
            if (strThongtin.length>0)
            {
                var tbl =strHeader.replace('__DISABLED__',strdisabled);
                var arr = strThongtin.split('#');
                var i=0;
                for(i=0;i<arr.length;i++)
                {
                    var arrtmp=arr[i].split('|');
                    tbl+='<tr>'; 
                    tbl+=' <td  class="form_label" style="width:30px;"><input type="hidden" id="STT'+arrtmp[0] +'" value="'+arrtmp[0] +'"  />'+arrtmp[0] +' </td>';
                    tbl+='<td  class="form_control"><input type="text" Class="inputflat" id="txtSDT'+arrtmp[0] +'" value="'+arrtmp[1] +'"  style="width:90%" '+strdisabled+'  onkeypress="return OnKeyPress(event);" onblur="OnSave()" /> </td>';
                    tbl+='<td  class="form_control"><input type="text" Class="inputflat" id="txtEMAIL'+arrtmp[0] +'" value="'+arrtmp[2] +'" style="width:90%" onblur="OnSave()"  '+strdisabled+' /> </td>';
                    tbl+='<td  class="form_control" style="width:70px;"><input type="button" id="Button2" value="Xóa" style="width:60px;" onclick="delThongTin('+arrtmp[0] +')"  '+strdisabled+' /></td>';
             
                    tbl+='</tr>'; 
                }
                document.getElementById('divThongtin').innerHTML=tbl+strFooter;
                
            }else
            {
          //  alert(strdisabled);
                strThongtin='1||';
                 document.getElementById('<%=txtThongTin.ClientID %>').value=strThongtin;
                 var tbl =strHeader.replace('__DISABLED__',strdisabled);
                var arr = strThongtin.split('#');
                var i=0;
                for(i=0;i<arr.length;i++)
                {
                    var arrtmp=arr[i].split('|');
                    tbl+='<tr>'; 
                    tbl+=' <td  class="form_label" style="width:30px;"><input type="hidden" id="STT'+arrtmp[0] +'" value="'+arrtmp[0] +'"  />'+arrtmp[0] +' </td>';
                    tbl+='<td  class="form_control"><input type="text" Class="inputflat" id="txtSDT'+arrtmp[0] +'" value="'+arrtmp[1] +'" style="width:90%" onkeypress="return OnKeyPress(event);" onblur="OnSave()"  "'+strdisabled+' /> </td>';
                    tbl+='<td  class="form_control"><input type="text" Class="inputflat" id="txtEMAIL'+arrtmp[0] +'" value="'+arrtmp[2] +'" style="width:90%" onblur="OnSave()"  "'+strdisabled+' /> </td>';
                    tbl+='<td  class="form_control" style="width:70px;"><input type="button" id="Button2" style="width:60px;" value="Xóa" onclick="delThongTin('+arrtmp[0] +')"  '+strdisabled+' /></td>';
          
                    tbl+='</tr>'; 
                }
                document.getElementById('divThongtin').innerHTML=tbl+strFooter;
            
            }
         }
         function AddThongtin()
         {
                strThongtin=document.getElementById('<%=txtThongTin.ClientID %>').value;
          if (strThongtin.length>0)
            {
                var strTMP="";
              //  var tbl =strHeader;
                var arr = strThongtin.split('#');
                var i=1;
                var x=0;
                for(i=1;i<=arr.length;i++)
                {
                  
                      x=x+1;
                      if(strTMP.length>0)
                       strTMP+="#";
                      strTMP+=x;
                      if(document.getElementById("txtSDT"+i).value!=null)
                       strTMP+="|"+document.getElementById("txtSDT"+i).value
                       if(document.getElementById("txtEMAIL"+i).value!=null)
                      strTMP+="|" +document.getElementById("txtEMAIL"+i).value
                   
                }
                strThongtin=strTMP;
               
                
            }
            if (strThongtin.length>0)
            {
                 var tbl =strHeader.replace('__DISABLED__',strdisabled);
                var arr = strThongtin.split('#');
                var i=0;
                for(i=0;i<arr.length;i++)
                {
                    var arrtmp=arr[i].split('|');
                    tbl+='<tr>'; 
                    tbl+=' <td  class="form_label" style="width:30px;"><input type="hidden" id="STT'+arrtmp[0] +'" value="'+arrtmp[0] +'"  />'+arrtmp[0] +' </td>';
                    tbl+='<td  class="form_control"><input type="text" Class="inputflat" id="txtSDT'+arrtmp[0] +'" value="'+arrtmp[1] +'" style="width:90%" "'+strdisabled+' onkeypress="return OnKeyPress(event);" onblur="OnSave()" /> </td>';
                    tbl+='<td  class="form_control"><input type="text" Class="inputflat" id="txtEMAIL'+arrtmp[0] +'" value="'+arrtmp[2] +'" style="width:90%" '+strdisabled+' onblur="OnSave()" /> </td>';
                    tbl+='<td  class="form_control" style="width:70px;"><input type="button" id="Button2" value="Xóa" style="width:60px;" onclick="delThongTin('+arrtmp[0] +')" style="width:90%" '+strdisabled+' /></td>';
                
                    tbl+='</tr>'; 
                }
                i=i+1;
                 tbl+='<tr>'; 
                     tbl+=' <td  class="form_label" style="width:30px;"><input type="hidden" id="STT'+i +'" value="'+i +'"  />'+i +' </td>';
                    tbl+='<td  class="form_control"><input type="text" Class="inputflat" id="txtSDT'+i+'" value="" style="width:90%" '+strdisabled+' onkeypress="return OnKeyPress(event);" onblur="OnSave()" /> </td>';
                    tbl+='<td  class="form_control"><input type="text" Class="inputflat" id="txtEMAIL'+i+'" value="" style="width:90%" '+strdisabled+' onblur="OnSave()" /> </td>';
                    tbl+='<td  class="form_control" style="width:70px;"><input type="button" id="Button2" value="Xóa" style="width:60px;" onclick="delThongTin('+i+')" style="width:90%" '+strdisabled+' /></td>';
                  
                    tbl+='</tr>'; 
                document.getElementById('divThongtin').innerHTML=tbl+strFooter;
                strThongtin +='#'+i+'||';   
            }else
            {
//               
                strThongtin +='#1||'; 
                document.getElementById('<%=txtThongTin.ClientID %>').value=strThongtin;
                showThongtin();
                return;
                
            }
             document.getElementById('<%=txtThongTin.ClientID %>').value=strThongtin;
            
         }
         function delThongTin(pos)
         {
       
            if (strThongtin.length>0)
            {
                var strTMP="";
              //  var tbl =strHeader;
                var arr = strThongtin.split('#');
                var i=1;
                var x=0;
                for(i=1;i<=arr.length;i++)
                {
                   if(i!=pos)
                   {
                      x=x+1;
                      if(strTMP.length>0)
                       strTMP+="#";
                      strTMP+=x;
                      if(document.getElementById("txtSDT"+i).value!=null)
                       strTMP+="|"+document.getElementById("txtSDT"+i).value
                       if(document.getElementById("txtEMAIL"+i).value!=null)
                      strTMP+="|" +document.getElementById("txtEMAIL"+i).value
                   }
                }
                strThongtin=strTMP;
                document.getElementById('<%=txtThongTin.ClientID %>').value=strThongtin;
                showThongtin();
                
            }
         }
         function OnSave()
         {
            var strTMP="";
            if (strThongtin.length>0)
            {
               
              //  var tbl =strHeader;
                var arr = strThongtin.split('#');
                var i=1;
                var x=0;
                for(i=1;i<=arr.length;i++)
                {
                   
                      x=x+1;
                      if(strTMP.length>0)
                       strTMP+="#";
                      strTMP+=x;
                      if(document.getElementById("txtSDT"+i).value!=null)
                       strTMP+="|"+document.getElementById("txtSDT"+i).value
                       if(document.getElementById("txtEMAIL"+i).value!=null)
                      strTMP+="|" +document.getElementById("txtEMAIL"+i).value
                   
                }
              
               
                
            }
            strThongtin=strTMP;
            document.getElementById('<%=txtThongTin.ClientID %>').value=strThongtin;
         }
var strDSTAIKHOAN_TH='';

    
    </script>
    <style type="text/css">
      
        .txtHoanThienMST{width:200px;}
        .txtHoanThienfull{width:500px;}
        .lblTTNT{width:150px;}
        .container{width:100%;padding-bottom:50px;}
        .form_label{ width:200px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle"> Đăng ký mới thông tin NTDT</p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
            <asp:HiddenField runat="server" ID="txtID" />
            <asp:HiddenField runat="server" ID="txtThongTin"  />
            <asp:HiddenField runat="server" ID="txtstrdisabled"  />
            <asp:HiddenField ID="dieuChinhConfirm" runat="server" Value="" />
         
            
            <tr>
                <td align="left" class="form_label">Mã số thuế <span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtMA_DV" runat="server" CssClass="inputflat txtHoanThienMST" MaxLength="14"  Enabled="false"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="left"  class="form_label">Tên đơn vị XNK<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtTEN_DV" runat="server" CssClass="inputflat txtHoanThienfull" MaxLength="255" ></asp:TextBox></td>
            </tr>
            
            <tr>
                <td align="left"  class="form_label">Địa chỉ đơn vị XNK<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtDIACHI" runat="server" CssClass="inputflat txtHoanThienfull" MaxLength="255" ></asp:TextBox></td>
            </tr>
             <tr>
                <td align="left"  class="form_label">Ngày hiệu lực <span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <span style="display:block; width:200px; position:relative;vertical-align:middle;">
                <asp:TextBox ID="txtNGAY_KY_HDUQ" runat="server" onblur="CheckDate(this);" onfocus="this.select()" CssClass="inputflat txtHoanThienMST  date-pick dp-applied" MaxLength="10" ></asp:TextBox>
                </span>
                 </td>
            </tr>
             <tr>
                <td align="left"  class="form_label">Mã ngân hàng<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                <asp:TextBox ID="txtMA_NH_TH" runat="server" CssClass="inputflat txtHoanThienMST" MaxLength="10" ></asp:TextBox>
                <asp:TextBox ID="txtTEN_NH_TH" runat="server" CssClass="inputflat txtHoanThienfull" style="width:400px;" ></asp:TextBox>
                </td>
            </tr>
            </table>
             <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
               <tr>
               <td colspan="4"  class="form_label" >THÔNG TIN NGƯỜI NỘP</td>
               </tr>
            <tr>
             <td align="left"  class="form_label">Số chứng minh thư<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtSO_CMT" runat="server" onkeypress="return OnKeyPress(event);" CssClass="inputflat txtHoanThienMST" MaxLength="10" ></asp:TextBox></td>
                <td align="left"  class="form_label" style="width:100px;">Họ tên NNT<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtHO_TEN" runat="server" CssClass="inputflat txtHoanThienMST" MaxLength="255" ></asp:TextBox></td>
                
            </tr>
           
            <tr>
                <td align="left"  class="form_label">Ngày sinh<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                 <span style="display:block; width:200px; position:relative;vertical-align:middle;">
                <asp:TextBox ID="txtNGAYSINH" runat="server"  onblur="CheckDate(this);" onfocus="this.select()" CssClass="inputflat txtHoanThienMST  date-pick dp-applied" MaxLength="10" ></asp:TextBox>
                 </span>
                 </td>
                <td align="left"  class="form_label" style="width:100px;">Nguyên quán<span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtNGUYENQUAN" runat="server" CssClass="inputflat txtHoanThienMST" MaxLength="255" ></asp:TextBox></td>
            </tr>
          
            </table>
             <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
            <tr>
             <td align="left"  class="form_label">Thông tin liên hệ <span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left">
                    <div id="divThongtin" >
                    
                    <table   cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
                    <tr>
                        <td  class="form_label">STT</td>
                        <td  class="form_label">Số điện thoại</td>
                        <td  class="form_label">Email</td>
                        <td  class="form_label"><input type="button" id="btnAdd" value="Add" /> <input type="button" id="Button1" value="Save" /> </td>
                    </tr>
                    <tr>
                        <td  class="form_label"><input type="hidden" id="STT1" value="1"  />1 </td>
                        <td  class="form_label"><input type="text" id="txtSDT1" value="1" Class="inputflat"  /> </td>
                        <td  class="form_label"><input type="text" id="txtEMAIL1" value="1" Class="inputflat" /> </td>
                        <td  class="form_label"><input type="button" id="Button2" value="Del" onclick="delThongTin(1)" /></td>
                    </tr>
                    </table>
                    </div>
                </td>
            </tr>
             </table>
             <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input" >
            <tr>
                <td align="left"  class="form_label">Số TK sử dụng NTDT <span class="requiredField">(*)</span></td>
                <td class="form_control" style="text-align:left"><asp:TextBox ID="txtCHK_TAIKHOAN_TH" runat="server" CssClass="inputflat txtHoanThienMST" MaxLength="50" ></asp:TextBox><asp:Button ID="btnCheckTK" runat="server" Text="Thêm tài khoản"  />
                </td>
            </tr>
           
            
            <tr>
                <td align="left" colspan="2"  class="form_label">DANH SÁCH TÀI KHOẢN ĐĂNG KÝ</td>
              
            </tr>
            <tr>
                <td colspan="2" class="form_control">
                <asp:HiddenField ID="hdfDSTAIKHOAN" runat="server" />
                <div id="divDSTAIKHOAN" >
                    
                    <table   cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
                    <tr>
                        <td  class="form_label" style="width:30px;">STT</td>
                        <td  class="form_label">Số tài khoản</td>
                         <td  class="form_label">Tên tài khoản</td>
                        <td  class="form_label" style="width:60px;">Thao tác</td>
                    </tr>
                    <tr>
                        <td  class="form_label" style="width:30px;"><input type="hidden" id="Hidden1" value="1"  />1 </td>
                        <td  class="form_control"><input type="text" id="TXTTAIKHOAN_TH1" value="1"  class="inputflat" disabled="disabled"/> </td>
                         <td  class="form_control"><input type="text" id="TXTTEN_TAIKHOAN_TH" value="1" class="inputflat" disabled="disabled"/> </td>
                        <td  class="form_label" style="width:60px;"><input type="button" id="Button5" value="Del" onclick="delThongTin(1)" /></td>
                    </tr>
                    </table>
                    </div></td>
                    
            </tr>
            
            
            <tr>
                <td colspan="2">&nbsp;
                <asp:HiddenField ID="hdfNNTDT_ID" runat="server" />
                <asp:HiddenField ID="hdrTRANG_THAI" runat="server" />
                <asp:HiddenField ID="hdfMA_CN" runat="server" />
                <asp:HiddenField ID="hdfMA_NV" runat="server" />
                <asp:HiddenField ID="hdrMax_SoTKNH" runat="server" />
                <asp:HiddenField ID="hdfCifno" runat="server" />
                <asp:HiddenField ID="hdfMA_CNTK" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                   
                    <asp:Button ID="btnChuyenKS" runat="server"  Text="Ghi và chuyển KS" />&nbsp;&nbsp;
                    <asp:Button ID="btnTaoMoi" runat="server"  Text="Tạo mới" />
                   
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
        </table>
    </div>

    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($('#<%=txtNGAYSINH.ClientID %>').val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
            if ($('#<%=txtNGAY_KY_HDUQ.ClientID %>').val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
            
        });
        strThongtin= document.getElementById('<%=txtThongTin.ClientID %>').value;
        strdisabled=document.getElementById('<%=txtstrdisabled.ClientID %>').value ;
        showThongtin();
        strDSTAIKHOAN_TH= document.getElementById('<%=hdfDSTAIKHOAN.ClientID %>').value;
        fncShow_DSTAIKHOAN_TH();
        function fncShow_DSTAIKHOAN_TH()
        {
            var tblheaderTK='<table   cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">';
                    tblheaderTK+='<tr>';
                        tblheaderTK+='<td  class="form_label" style="width:30px;">STT</td>';
//                        tblheaderTK+='<td  class="form_label">CifNo</td>';
                        tblheaderTK+='<td  class="form_label">Số tài khoản</td>';
                         tblheaderTK+='<td  class="form_label">Tên tài khoản</td>';
//                         tblheaderTK+='<td  class="form_label">Mã chi nhánh</td>';
                        tblheaderTK+='<td  class="form_label" style="width:60px;">Thao tác</td>';
                    tblheaderTK+='</tr>';
                    if(strDSTAIKHOAN_TH.length>0)
                    {
                        var arr = strDSTAIKHOAN_TH.split('#');
                        for(var i=0; i< arr.length;i++)
                        {
                            var objTemp =arr[i].split('|'); 
                            tblheaderTK+='<tr>';
                                tblheaderTK+='<td  class="form_label" style="width:30px;"><input type="hidden" id="txtSTTTK'+objTemp[0]+'" value="'+objTemp[0]+'"  />'+objTemp[0]+' </td>';
//                                tblheaderTK+='<td  class="form_control"><input type="text" id="TXTCIFNO'+objTemp[0]+'" value="'+objTemp[1]+'" class="inputflat" disabled="disabled"  /> </td>';
                                tblheaderTK+='<td  class="form_control"><input type="text" id="TXTTAIKHOAN_TH'+objTemp[0]+'" value="'+objTemp[1]+'"  class="inputflat" disabled="disabled"/> </td>';
                                tblheaderTK+='<td  class="form_control"><input type="text" id="TXTTEN_TAIKHOAN_TH'+objTemp[0]+'" value="'+objTemp[2]+'" class="inputflat" disabled="disabled"/> </td>';
//                                tblheaderTK+='<td  class="form_control"><input type="text" id="TXTMA_CN_TK'+objTemp[0]+'" value="'+objTemp[4]+'" class="inputflat" disabled="disabled"/> </td>';
                                tblheaderTK+='<td  class="form_label" style="width:60px;"><input type="button" id="Button5" value="Xóa" onclick="delThongTinTK('+objTemp[0]+')" /></td>';
                            tblheaderTK+='</tr>';
                        }
                    }else
                    {
                        tblheaderTK+='<tr>';
                            tblheaderTK+='<td  class="form_label" colspan="6">Chưa thêm danh sách tài khoản thanh toán </td>';
                        tblheaderTK+='</tr>';
                    }
                    tblheaderTK+='</table>';
             document.getElementById('divDSTAIKHOAN').innerHTML=tblheaderTK                           
        }
        function delThongTinTK(pos)
         {
            strDSTAIKHOAN_TH= document.getElementById('<%=hdfDSTAIKHOAN.ClientID %>').value;
            if (strDSTAIKHOAN_TH.length>0)
            {
                var strTMP="";
              //  var tbl =strHeader;
                var arr = strDSTAIKHOAN_TH.split('#');
                var i=1;
                var x=0;
                for(i=1;i<=arr.length;i++)
                {
                   if(i!=pos)
                   {
                      x=x+1;
                      if(strTMP.length>0)
                       strTMP+="#";
                      strTMP+=x;
//                      if(document.getElementById("TXTCIFNO"+i).value!=null)
//                       strTMP+="|"+document.getElementById("TXTCIFNO"+i).value
                       if(document.getElementById("TXTTAIKHOAN_TH"+i).value!=null)
                      strTMP+="|" +document.getElementById("TXTTAIKHOAN_TH"+i).value
                      
                      if(document.getElementById("TXTTEN_TAIKHOAN_TH"+i).value!=null)
                      strTMP+="|" +document.getElementById("TXTTEN_TAIKHOAN_TH"+i).value
                      
//                      if(document.getElementById("TXTMA_CN_TK"+i).value!=null)
//                      strTMP+="|" +document.getElementById("TXTMA_CN_TK"+i).value
                      
                   }
                }
                strDSTAIKHOAN_TH=strTMP;
                document.getElementById('<%=hdfDSTAIKHOAN.ClientID %>').value=strDSTAIKHOAN_TH;
                fncShow_DSTAIKHOAN_TH();
                
            }
         }        
    </script>		
</asp:Content>

