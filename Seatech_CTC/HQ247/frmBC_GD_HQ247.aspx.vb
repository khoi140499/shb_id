﻿Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports Business.BaoCao
Imports System.Data
Imports Business
Imports VBOracleLib
Imports VBOracleLib.Globals
Imports VBOracleLib.Security
Imports Signature

Partial Class frmBC_GD_HQ247
    Inherits System.Web.UI.Page
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private mv_strNgay As String

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenDiemThu As String
    Private mv_strTenKB As String
    Private mv_strLoaiBaoCao As String
    Private strMaNT As String

    Private ds As DataSet
    Private frm As infBaoCao
    Private somon As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        'If Not clsCommon.GetSessionByID(Session.Item("User")) Then
        '    Response.Redirect("~/pages/Warning.html", False)
        '    Exit Sub
        'End If
        If Not IsPostBack Then
            Try
                DM_TrangThai()
                DM_LoaiThue()
                DM_DiemThu()
                txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub
    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        Dim str_where As String = ""

        If txtTuNgay.Value <> "" Then
            str_where &= " AND  TO_CHAR(A.HQ_NGAY_CT,'yyyyMMdd') >=" & ConvertDateToNumber(txtTuNgay.Value)
        Else
            clsCommon.ShowMessageBox(Me, "Bạn cần nhập vào thời gian từ ngày đến ngày cho báo cáo")
            txtTuNgay.Focus()
            Return
        End If
        If txtDenNgay.Value <> "" Then
            str_where &= " AND  TO_CHAR(A.HQ_NGAY_CT,'yyyyMMdd') <=" & ConvertDateToNumber(txtDenNgay.Value)
        Else
            clsCommon.ShowMessageBox(Me, "Bạn cần nhập vào thời gian từ ngày đến ngày cho báo cáo")
            txtDenNgay.Focus()
            Return
        End If
        If ConvertDateToNumber(txtTuNgay.Value) > ConvertDateToNumber(txtDenNgay.Value) And txtDenNgay.Value.ToString.Length > 0 Then
            clsCommon.ShowMessageBox(Me, "Điều kiện Từ ngày phải nhỏ hơn hoặc bằng Đến ngày")
            Exit Sub
        End If
        If drpLoaiThue.SelectedIndex > 0 Then
            str_where &= " AND A.LOAIMSG ='" & drpLoaiThue.SelectedValue & "' "
        End If
        If drpTrangThai.SelectedIndex > 0 Then
            str_where &= " AND A.TRANGTHAI ='" & drpTrangThai.SelectedValue & "' "
        End If

        'sonmt: ma chi nhanh
        If txtMaCN.SelectedValue.Trim.Length > 0 Then
            str_where += " AND A.MA_CN = '" & txtMaCN.SelectedValue & "' "
        End If

        Dim sql As String = "SELECT A.HQ_SO_CTU SO_CT,NVL(A.SO_CT_NH,'_') SO_FT,A.TEN_DV TEN_NNT,A.MA_DV MST,A.HQ_SOTIEN_TO SO_TIEN," & _
                            " NVL((SELECT MAX(C.REMARKS) FROM TCS_CTU_NTDT_HQ247_HDR C WHERE C.SO_CT = A.SO_CTU),'_') NOI_DUNG," & _
                            " TO_CHAR(A.NGAYNHANMSG,'DD/MM/RRRR HH24:MI:SS') TT_CT_SHB,B.MOTA TRANG_THAI" & _
                            " FROM TCS_HQ247_304 A,TCS_DM_TRANGTHAI_TT_HQ247 B WHERE A.TRANGTHAI = B.TRANGTHAI " & str_where
        Try
            ds = New DataSet
            frm = New infBaoCao
            ds = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            If (mdlCommon.IsEmptyDataSet(ds)) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If
            frm.TCS_DS = ds
            frm.TCS_Tungay = txtTuNgay.Value.ToString()
            frm.TCS_Denngay = txtDenNgay.Value.ToString()
            Session("objBaoCao") = frm 'Gọi form show báo cáo
            clsCommon.OpenNewWindow(Me, "../pages/BaoCao/frmShowReports.aspx?BC=" & Report_Type.BC_CHITIET_GD_NTDT_HQ247, "ManPower")

        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình Lập bảng kê chứng từ!")
        Finally
            cmdIn.Enabled = True
        End Try
    End Sub
    Private Sub DM_TrangThai()

        Dim dsTrangThai As DataSet
        Dim strSQL As String = ""
        strSQL = "SELECT a.TRANGTHAI, a.MOTA  FROM TCS_DM_TRANGTHAI_TT_HQ247 a  WHERE TRANGTHAI NOT IN ('08','09') "
        Try
            dsTrangThai = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(drpTrangThai, dsTrangThai, 1, 0, "Tất cả")
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Có lỗi trong quá trình lấy thông tin trạng thái chứng từ", Session.Item("TEN_DN"))
            Throw ex
        Finally

        End Try
    End Sub
    Private Sub DM_LoaiThue()

        Dim dsLoaiThue As DataSet
        Dim strSQL As String = ""
        strSQL = "SELECT A.MALOAI MA_LTHUE, A.TENLOAI TEN  FROM TCS_HQ247_LOAITHUE A"
        Try
            dsLoaiThue = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(drpLoaiThue, dsLoaiThue, 1, 0, "Tất cả")
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Có lỗi trong quá trình lấy thông tin loại thuế", Session.Item("TEN_DN"))
            Throw ex
        Finally

        End Try
    End Sub
    Private Sub DM_DiemThu()

        Dim dsDiemThu As DataSet
        Dim strwhere As String = ""
        Dim strcheckHSC As String = ""
        Dim strSQL As String = ""
        ' Dim item As ListItem
        strcheckHSC = checkHSC()
        If strcheckHSC = "1" Then
            'strwhere = " where ma_dthu =(select ban_lv from TCS_DM_NhanVien where ma_NV='" & Session.Item("User").ToString & "')"
            ' strwhere = "where ma_dthu ='" & mv_strMaDiemThu & "'"
            strSQL = "SELECT a.id, a.id ||'-'||a.name nam FROM tcs_dm_chinhanh a  "

        Else

            strSQL = "Select a.id,  a.id ||'-'||a.name name from tcs_dm_chinhanh a where  branch_id ='" & Session.Item("MA_CN_USER_LOGON").ToString & "'"
        End If
        Try
            dsDiemThu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            clsCommon.ComboBoxWithValue(txtMaCN, dsDiemThu, 1, 0, "Tất cả")
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Có lỗi trong quá trình lẫy thông tin chi nhánh", Session.Item("TEN_DN"))
            Throw ex
        Finally

        End Try

    End Sub

    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function
End Class
