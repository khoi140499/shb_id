﻿Imports System.Data
Imports Business.NTDT.NNTDT
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao
Imports log4net

Partial Class HQ247_frmTiepNhanYCTT
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Load_TrangThai()
            Load_Loai_HS()

            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            Dim strMST As String = ""
            Dim strTen_DV As String = ""
            Dim strTuNgay As String = ""
            Dim strDenNgay As String = ""
            Dim strTRANGTHAI As String = ""
            Dim strLOAIMSG As String = ""
            Dim strTKNH As String = ""
            If Not Session.Item("TIEPNHANYCTT_MA_DV") Is Nothing Then
                strMST = Session.Item("TIEPNHANYCTT_MA_DV").ToString()
                txtMST.Text = strMST
            End If

            If Not Session.Item("TIEPNHANYCTT_TU_NGAY") Is Nothing Then
                strTuNgay = Session.Item("TIEPNHANYCTT_TU_NGAY").ToString()
                If strTuNgay.Equals("19000101") Then
                    txtTuNgay.Value = ""
                Else
                    txtTuNgay.Value = strTuNgay

                End If

            End If
            If Not Session.Item("TIEPNHANYCTT_DEN_NGAY") Is Nothing Then
                strDenNgay = Session.Item("TIEPNHANYCTT_DEN_NGAY").ToString()
                If strDenNgay.Equals("19000101") Then
                    txtDenNgay.Value = ""
                Else
                    txtDenNgay.Value = strDenNgay

                End If
            End If
            If Not Session.Item("TIEPNHANYCTT_TEN_DV") Is Nothing Then
                strTen_DV = Session.Item("TIEPNHANYCTT_TEN_DV").ToString()
                txtTenNNT.Text = strTen_DV
            End If

            If Not Session.Item("TIEPNHANYCTT_TRANGTHAI") Is Nothing Then
                strTRANGTHAI = Session.Item("TIEPNHANYCTT_TRANGTHAI").ToString()
                drpTrangThai.SelectedValue = strTRANGTHAI
            End If

            If Not Session.Item("TIEPNHANYCTT_LOAIMSG") Is Nothing Then
                strLOAIMSG = Session.Item("TIEPNHANYCTT_LOAIMSG").ToString()
                cboLoaiMSG.SelectedValue = strLOAIMSG
            End If

            If Not Session.Item("TIEPNHANYCTT_TKNH") Is Nothing Then
                strTKNH = Session.Item("TIEPNHANYCTT_TKNH").ToString()
                txtTKNH.Text = strTKNH
            End If

            If (strMST.Length + strTen_DV.Length + strTRANGTHAI.Length + strLOAIMSG.Length + strTKNH.Length + strDenNgay.Length + strTuNgay.Length) > 0 Then
                load_dataGrid()
            End If
        End If
    End Sub
    Private Sub load_dataGrid()
        Dim ds As DataSet

        Dim WhereClause As String = ""
        Try
            If txtMST.Enabled Then
                If txtMST.Text <> "" Then
                    WhereClause += " AND upper(a.MA_DV) like upper('%" & txtMST.Text.Trim & "%')"
                    Session.Add("TIEPNHANYCTT_MA_DV", txtMST.Text.Trim())
                Else
                    Session.Add("TIEPNHANYCTT_MA_DV", "")
                End If
                If txtTuNgay.Value <> "" Then
                    WhereClause += " AND to_char(a.HQ_NGAYLAP_CT,'RRRRMMDD') >=" & ConvertDateToNumber(txtTuNgay.Value)
                    Session.Add("TIEPNHANYCTT_TU_NGAY", txtTuNgay.Value)
                Else
                    Session.Add("TIEPNHANYCTT_TU_NGAY", "19000101")
                End If
                If txtDenNgay.Value <> "" Then
                    WhereClause += " AND to_char(a.HQ_NGAYLAP_CT,'RRRRMMDD') <= " & ConvertDateToNumber(txtDenNgay.Value)
                    Session.Add("TIEPNHANYCTT_DEN_NGAY", txtDenNgay.Value)
                Else
                    Session.Add("TIEPNHANYCTT_DEN_NGAY", "19000101")
                End If
                If txtTenNNT.Text <> "" Then
                    WhereClause += " AND upper(a.TEN_DV) like upper('%" + txtTenNNT.Text + "%')"
                    Session.Add("TIEPNHANYCTT_TEN_DV", txtTenNNT.Text)
                Else
                    Session.Add("TIEPNHANYCTT_TEN_DV", "")
                End If
                If drpTrangThai.SelectedIndex <> 0 Then
                    WhereClause += " AND a.TRANGTHAI = '" + drpTrangThai.SelectedValue + "'"
                    Session.Add("TIEPNHANYCTT_TRANGTHAI", drpTrangThai.SelectedValue)
                Else
                    Session.Add("TIEPNHANYCTT_TRANGTHAI", "")
                End If
                If cboLoaiMSG.SelectedIndex <> 0 Then
                    WhereClause += " AND a.LOAIMSG = '" + cboLoaiMSG.SelectedValue + "'"
                    Session.Add("TIEPNHANYCTT_LOAIMSG", cboLoaiMSG.SelectedValue)
                Else
                    Session.Add("TIEPNHANYCTT_LOAIMSG", "")
                End If
                If txtTKNH.Text <> "" Then
                    WhereClause += " AND upper(a.TAIKHOAN_TH) like upper('%" + txtTKNH.Text + "%')"
                    Session.Add("TIEPNHANYCTT_TKNH", txtTKNH.Text)
                Else
                    Session.Add("TIEPNHANYCTT_TKNH", "")
                End If
            End If
            ds = Business_HQ.HQ247.daCTUHQ304.getMSG304(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                dtgrd.CurrentPageIndex = 0
                dtgrd.DataSource = ds.Tables(0)
                dtgrd.DataBind()

                'grdExport.DataSource = ds.Tables(0)
                'grdExport.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub
    Private Sub Load_TrangThai()
        Try
            Dim sql = "SELECT * FROM TCS_DM_TRANGTHAI_TT_HQ247 "
            Dim dr As DataSet = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            drpTrangThai.DataSource = dr.Tables(0)
            drpTrangThai.DataTextField = "MOTA"
            drpTrangThai.DataValueField = "TRANGTHAI"
            drpTrangThai.DataBind()
            drpTrangThai.Items.Insert(0, "Tất cả")

        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái yêu cầu. Lỗi: " & ex.Message)
        End Try
    End Sub
    
    Private Sub Load_Loai_HS()
        Try
            Dim sql = "SELECT MALOAI,TENLOAI  FROM TCS_HQ247_LOAITHUE A"
            Dim dr As DataSet = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            cboLoaiMSG.DataSource = dr.Tables(0)
            cboLoaiMSG.DataTextField = "TENLOAI"
            cboLoaiMSG.DataValueField = "MALOAI"
            cboLoaiMSG.DataBind()
            cboLoaiMSG.Items.Insert(0, "Tất cả")


        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách loại hồ sơ người nộp thuế. Lỗi: " & ex.Message)
        End Try
    End Sub



    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        load_dataGrid()
        dtgrd.CurrentPageIndex = 0
        dtgrd.DataBind()
    End Sub

    Protected Sub dtgrd_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dtgrd.PageIndexChanged
        load_dataGrid()
        dtgrd.CurrentPageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub
   
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
End Class
