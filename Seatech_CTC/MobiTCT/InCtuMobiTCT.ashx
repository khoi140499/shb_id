﻿<%@ WebHandler Language="VB" Class="InCtuMobiTCT" %>

Imports System
Imports System.Web
Imports Business.BaoCao
Imports Business
Imports VBOracleLib
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports System.Data
Imports Business.NewChungTu
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO

Public Class InCtuMobiTCT : Implements IHttpHandler, System.Web.SessionState.IReadOnlySessionState
    ' Private mv_strSHKB As String
    ' Private mv_strKyHieuCT As String
    ' Private mv_strMaDiemThu As String
    Private mv_SoCT As String = ""
    'Private Shared strTT_TIENMAT As String = ConfigurationManager.AppSettings.Get("TK_TIENMAT").ToString()
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    ' Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenKB As String
    ' Private mv_strTenTinhTP As String
    Private mv_strMaTinhTP As String
    Private mv_strSHKBDEF As String
    Private pdvTCS As New CrystalDecisions.Shared.ParameterDiscreteValue
    Private rptTCS As New ReportDocument
    Private rptTCS_PHT As New ReportDocument
    Private pcTCS As New CrystalDecisions.Shared.ParameterValues
    ' Private Shared strTT_TIENMAT As String = ConfigurationManager.AppSettings.Get("TK_TIENMAT").ToString()
    'Private ten_NH As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name")

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        If Not context.Session("User") Is Nothing Then
            log.Info("0.InCtuMobiTCT")
        End If
        Try
            log.Info("1.InCtuMobiTCT")
            Dim strMa_Lthue As String = "01"
            Dim TEN_HUYEN_NNT As String = ""
            Dim TEN_TINH_NNT As String = ""
            Dim strMaThamChieu As String = ""
            '  Dim key As New KeyCTu
            Dim objHDR As New NewChungTu.infChungTuHDR
            Dim isBS As String = Nothing
            If context.Request.QueryString.Item("SoCT") Is Nothing Then
                Exit Sub
            Else
                mv_SoCT = context.Request.QueryString.Item("SoCT").ToString()
                mv_SoCT = FilterSQLi(mv_SoCT)
                Dim strSQLCTU As String = "SELECT MATHAMCHIEU,SO_CT,MALOAIHINHTHUE,HUYENNGUOINOP,TINHNGUOINOP FROM SET_CTU_HDR_MOBITCT A WHERE A.MATHAMCHIEU='" & mv_SoCT & "' AND TRANGTHAI IN ('05','06','09','10') "
                Dim dsctu As DataSet = DataAccess.ExecuteReturnDataSet(strSQLCTU, CommandType.Text)
                log.Info("2.InCtuMobiTCT " & strSQLCTU)
                mv_SoCT = ""
                If IsEmptyDataSet(dsctu) Then
                    Exit Sub
                End If
                mv_SoCT = dsctu.Tables(0).Rows(0)("SO_CT").ToString()
                If dsctu.Tables(0).Rows(0)("MALOAIHINHTHUE").ToString().Equals("02") Then
                    strMa_Lthue = "03"
                End If
                If mv_SoCT.Length <= 0 Then
                    Exit Sub
                End If
                TEN_HUYEN_NNT = dsctu.Tables(0).Rows(0)("HUYENNGUOINOP").ToString()
                TEN_TINH_NNT = dsctu.Tables(0).Rows(0)("TINHNGUOINOP").ToString()
                strMaThamChieu = dsctu.Tables(0).Rows(0)("MATHAMCHIEU").ToString()
                log.Info("3.InCtuMobiTCT - mv_SoCT " & mv_SoCT & " - strMa_Lthue " & strMa_Lthue & " - TEN_HUYEN_NNT " & TEN_HUYEN_NNT & " - TEN_TINH_NNT " & TEN_TINH_NNT & " - strMaThamChieu " & strMaThamChieu)
            End If
           
            Dim hdr As New NewChungTu.infChungTuHDR
            ' Dim dtl As NewChungTu.infChungTuDTL()
            Dim objCTU As Business.MobiTCT.MobiTCTCTU = New MobiTCT.MobiTCTCTU()
            hdr = objCTU.CTU_HeaderIN(mv_SoCT)
            ' dtl = objCTU.CTU_ChiTietIN(mv_SoCT, hdr.Ngay_BK)
            '  Dim v_strMa_CN_Uer As String = clsCTU.TCS_GetMaChiNhanh(hdr.Ma_NV)
            ' Dim v_strBranchCode As String = clsCTU.TCS_GetMaCN_NH(v_strMa_CN_Uer)
         
            Dim v_strMa_CN_Uer As String = clsCTU.TCS_GetMaChiNhanh(hdr.Ma_NV)

            Dim v_strBranchCode As String = clsCTU.TCS_GetMaCN_NH(v_strMa_CN_Uer)
            Dim v_strNameCN As String = clsCTU.TCS_GetTenCNNH(v_strMa_CN_Uer)
            log.Info("4.InCtuMobiTCT " & " - v_strMa_CN_Uer " & v_strMa_CN_Uer & " - v_strBranchCode " & v_strBranchCode & " - v_strNameCN " & v_strNameCN)
            Dim ds As DataSet = Nothing
            If strMa_Lthue = "03" Then
                ds = objCTU.CTU_IN_TRUOCBA(mv_SoCT)
            Else
                ds = objCTU.CTU_IN_LASER(mv_SoCT, hdr.Ngay_KB)
            End If
            
            'gLoaiBaoCao = Report_Type.TCS_CTU_LASER
            Dim strFileName As String = ""
            Dim v_strReportName As String = "In chứng từ thu ngân sách"
            log.Info("5.InCtuMobiTCT " & v_strReportName)
            If hdr.Ma_NT.Equals("VND") Then
                ' strFileName = Server.MapPath("RPT\TCS_CTU_LASER_T7.rpt")
                If hdr.Ma_LThue = "04" Then
                    strFileName = context.Server.MapPath("..\Pages\BaoCao\RPT\C109HQ.rpt")
                Else
                    'strFileName = Server.MapPath("RPT\C102TCT.rpt")
                    strFileName = context.Server.MapPath("..\Pages\BaoCao\RPT\C102TCTNEW.rpt")
                End If
            Else
                strFileName = context.Server.MapPath("..\Pages\BaoCao\RPT\TCS_CTU_NguyenTe.rpt")
            End If
            log.Info("6.InCtuMobiTCT " & strFileName)
            If (Not File.Exists(strFileName)) Then
            End If
            
            
            
            rptTCS.Load(strFileName)
            ds.Tables(0).TableName = "CTU"
            'ds.WriteXml("D:\gntnguyente.xml", XmlWriteMode.WriteSchema)
            rptTCS.SetDataSource(ds)
            Dim mCopy As String = ""
            isBS = hdr.Lan_In ' Business.ChungTu.buChungTu.CTU_Get_LanIn(key)
            If Not isBS Is Nothing Then
                If isBS.Equals("0") Then
                    mCopy = "Bản Chính"
                Else
                    mCopy = "Bản Sao"
                End If
            End If
            
            Dim ma_nnthue As String = clsCommon.gf_CorrectString(hdr.Ma_NNThue)
            Dim ma_nntien As String = clsCommon.gf_CorrectString(hdr.Ma_NNTien)
            
            log.Info("7.InCtuMobiTCT " & mCopy)
            AddParam("TienPhi", Globals.Format_Number(hdr.PHI_GD.ToString, "."))
            AddParam("TienVAT", Globals.Format_Number(hdr.PHI_VAT.ToString, "."))
            
            AddParam("KHCT", hdr.KyHieu_CT)   '
            AddParam("SoCT", hdr.So_CT)       '
            AddParam("TK_KN_NH", clsCommon.gf_CorrectString(hdr.TK_KH_NH))     '
            AddParam("Ten_NNThue", hdr.Ten_NNThue.ToString())     '
            AddParam("Ma_NNThue", clsCommon.gf_CorrectString(hdr.Ma_NNThue))    '
            AddParam("DC_NNThue", clsCommon.gf_CorrectString(hdr.DC_NNThue))    '
            
            '
            If (TEN_HUYEN_NNT.Trim.Length <=0 ) Then
                AddParam("Huyen_NNThue", "")
            Else
                Dim strTenHuyen_nnthue As String() = TEN_HUYEN_NNT.Split("-")
                AddParam("Huyen_NNThue", clsCommon.gf_CorrectString(strTenHuyen_nnthue(1).Trim))
            End If
          
            If (TEN_TINH_NNT.Trim.Length <= 0) Then
                AddParam("Tinh_NNThue", "")   
            Else
                Dim strTenTinh_nnthue As String() = TEN_TINH_NNT.Split("-")
                AddParam("Tinh_NNThue", clsCommon.gf_CorrectString(strTenTinh_nnthue(1).Trim))
            End If
            
            If ma_nnthue.Equals(ma_nntien) Then
                AddParam("Ten_NNTien", "")      '
                AddParam("Ma_NNTien", "")        '
                AddParam("DC_NNTien", "")          '
                AddParam("Huyen_NNTien", "")     '
                AddParam("Tinh_NNTien", "")        '
            Else
                AddParam("Ten_NNTien", clsCommon.gf_CorrectString(hdr.Ten_NNTien))      '
                AddParam("Ma_NNTien", clsCommon.gf_CorrectString(hdr.Ma_NNTien))        '
                AddParam("DC_NNTien", clsCommon.gf_CorrectString(hdr.DC_NNTien))          '
                AddParam("Huyen_NNTien", clsCommon.gf_CorrectString(""))     '
                AddParam("Tinh_NNTien", clsCommon.gf_CorrectString(""))        '
            End If
            '

            AddParam("NHA", "NH TMCP Sài Gòn - Hà Nội")    '
            AddParam("NHB", hdr.Ten_NH_B.ToString)        '

            AddParam("Ten_KB", Get_TenKhoBac(hdr.SHKB))       '
            AddParam("Tinh_KB", clsCommon.gf_CorrectString(hdr.QUAN_HUYEN_NNTIEN))       '
            AddParam("Ma_CQThu", clsCommon.gf_CorrectString(hdr.Ma_CQThu))             '
            'AddParam("MaCQThu", clsCommon.gf_CorrectString(hdr.Ma_CQThu))     '
            AddParam("Ten_CQThu", Get_TenCQThu(clsCommon.gf_CorrectString(hdr.Ma_CQThu)))     '

            AddParam("So_TK", hdr.So_TK) '
            If Not hdr.Ngay_TK Is Nothing And hdr.Ngay_TK <> "" Then
                AddParam("Ngay_TK", Convert.ToDateTime(hdr.Ngay_TK).ToString("yyyy"))  '
            Else
                AddParam("Ngay_TK", "")   '
            End If
         
          
            If hdr.Ma_NT.Equals("VND") Then
                AddParam("Tien_Bang_Chu", TCS_Dich_So(hdr.TTien, "đồng"))    '
            Else
                AddParam("Tien_Bang_Chu", TCS_Dich_So((hdr.TTien * hdr.Ty_Gia), "đồng"))    '
            End If


            AddParam("SO_FT", hdr.So_CT_NH)      '

            AddParam("Ten_KeToan", Get_HoTenNV(clsCommon.gf_CorrectString(hdr.Ma_NV)))     '
            If hdr.PT_TT.Equals("01") Then
                AddParam("Key_MauCT", "1")     '
            Else
                AddParam("Key_MauCT", "0")      '
            End If

            If hdr.PT_TT.ToString().Equals("01") Then
                hdr.PT_TT = "01"
            End If
            If hdr.PT_TT.Equals("05") Then
                hdr.PT_TT = "00"
            End If

            AddParam("PT_TT", hdr.PT_TT.ToString())       '
            ' AddParam("Ma_CN", hdr.Ma_CN)       '
            AddParam("Ma_CN", v_strBranchCode)
            ' Dim dtGetTenCN As DataTable = DataAccess.ExecuteToTable("select name from tcs_dm_chinhanh where branch_id='" & hdr.Ma_CN & "'")

            ' If dtGetTenCN.Rows.Count > 0 Then
            AddParam("ten_cn", v_strNameCN)            '
            ' End If


            AddParam("ngay_GD", Convert.ToDateTime(hdr.Ngay_HT).ToString("dd/MM/yyyy  HH:mm:ss"))    '

            AddParam("tienTKNo", Globals.Format_Number(Double.Parse(hdr.TTien.ToString) + Double.Parse(hdr.PHI_GD.ToString) + Double.Parse(hdr.PHI_VAT.ToString), ","))  '
            AddParam("TienTKNH", Globals.Format_Number(hdr.TTien.ToString, ","))   '

            AddParam("TKNo", clsCommon.gf_CorrectString(hdr.TK_KH_NH))    '
            If Not hdr.Ma_NT.Equals("VND") And Not hdr.Ma_NT.Equals("USD") Then
                AddParam("Ma_NT", clsCommon.gf_CorrectString(hdr.Ma_NT))
            Else
                AddParam("Ma_NT", "")
            End If

            AddParam("LoaiTien", clsCommon.gf_CorrectString(hdr.Ma_NT))      '

            ' AddParam("SO_FCC", hdr.So_CT_NH)           '
            'ma tham chieu TCT tra ve
            AddParam("SO_FCC", strMaThamChieu)
            Dim dtGetTenGDV As DataTable = DataAccess.ExecuteToTable("select ten from tcs_dm_nhanvien where ma_nv='" & hdr.Ma_NV & "'")

            If dtGetTenGDV.Rows.Count > 0 Then
                AddParam("TEN_GDV", dtGetTenGDV.Rows(0)(0).ToString)       '
            End If
            AddParam("IsTamThu", hdr.MA_NTK)
            'AddParam("TKCo", hdr.TK_Co)
            log.Info("8.InCtuMobiTCT close add param")
            objCTU.CTU_Update_LanIn(mv_SoCT)
            log.Info("9.InCtuMobiTCT update_lanin")
            Try
                rptTCS.PrintOptions.PaperSize = CrystalDecisions.[Shared].PaperSize.PaperA4
                rptTCS.PrintOptions.PaperOrientation = CrystalDecisions.[Shared].PaperOrientation.Portrait
                
                'rptTCS.ExportToHttpResponse(ExportFormatType.PortableDocFormat, context.Response, False, "Giaynoptien")
                'Try
                '    rptTCS.ExportToHttpResponse(ExportFormatType.PortableDocFormat, context.Response, False, "Giaynoptien")
                'Catch ex As System.Threading.ThreadAbortException
                '    'log va ko làm gi
                '    log.Info("9.1.InCtuMobiTCT error" & ex.Message & " - " & ex.StackTrace)
                'End Try

                Try
                    'rptTCS.Load(Server.MapPath(@"MyReport.rpt"))
                    Dim oStream As System.IO.Stream
                    Dim byteArray As Byte() = New Byte() {}
                    oStream = rptTCS.ExportToStream(ExportFormatType.PortableDocFormat)
                    byteArray = New Byte(oStream.Length) {}
                    oStream.Read(byteArray, 0, Convert.ToInt32(oStream.Length - 1))
                    context.Response.ClearContent()
                    context.Response.ClearHeaders()
                    context.Response.ContentType = "application/pdf"
                    
                    context.Response.BinaryWrite(byteArray)
                    context.Response.Flush()
                    context.Response.Close()
                Catch ex As Exception
                    log.Info("9.1.InCtuMobiTCT error" & ex.Message & " - " & ex.StackTrace)
                End Try

            Catch ex As Exception
                log.Info("9.2.InCtuMobiTCT error" & ex.Message & " - " & ex.StackTrace)
            Finally
                rptTCS.Close()
                rptTCS.Dispose()
            End Try
            log.Info("10.InCtuMobiTCT success" & mCopy)
        Catch ex As Exception
            log.Info("9.1.1.InCtuMobiTCT error" & ex.Message & " - " & ex.StackTrace)
            Dim xxx = "Lỗi trong quá trình in báo cáo : " + ex.Message
            context.Response.Write("<script language=" + "javascript" + ">alert('" + xxx + "');</script>")
        Finally

        End Try
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    Private Function GetMaDThuBySHKB(ByVal shkb As String) As String
        Dim strSql As String = "SELECT * FROM TCS_THAMSO where ten_ts='SHKB' and giatri_ts='" & shkb & "'"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSql)
        If Not dt Is Nothing And dt.Rows.Count > 0 Then
            Return dt.Rows(0)("ma_dthu").ToString
        Else
            Return ""
        End If
    End Function
   
    
    Private Function CreateTableSub() As DataTable
        Dim v_dt As New DataTable()
        v_dt.Columns.Add(New DataColumn("SO_BT"))
        v_dt.Columns.Add(New DataColumn("NO_CO"))
        v_dt.Columns.Add(New DataColumn("MA_CN"))
        v_dt.Columns.Add(New DataColumn("SO_TK"))
        v_dt.Columns.Add(New DataColumn("TEN_TK"))
        v_dt.Columns.Add(New DataColumn("SO_TIEN"))
        v_dt.Columns.Add(New DataColumn("NOI_DUNG"))
        Return v_dt
    End Function
    Private Sub AddParam(ByVal strName As String, ByVal strValue As String)
        Try
            pdvTCS.Value = strValue
            pcTCS.Add(pdvTCS)
            rptTCS.DataDefinition.ParameterFields(strName).ApplyCurrentValues(pcTCS)
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub
End Class