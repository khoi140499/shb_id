﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" 
CodeFile="frmTracuuLogMobiTCT.aspx.vb" Inherits="MobiTCT_frmTracuuLogMobiTCT" title="TRA CỨU LOG MOBI TCT" %>

<%@ Import Namespace="Business.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
     <link href="../css/dialog.css" rel="stylesheet" />
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
        function jsChiTietNNT(_ID) {
       
            PageMethods.GetLogDetail(_ID, Get_LogDetail_Complete, Get_LogDetail_Error);
        }
        function Get_LogDetail_Complete(pdata)
        {
           // console.log(pdata);
            if (pdata.length > 0)
            {
               // console.log(pdata);
                var objData = JSON.parse(pdata);
                console.log(objData);
                $('#Detail_ID').html(objData.ID);
                $('#Detail_REQ_TYPE').html(objData.REQ_TYPE);
                $('#Detail_REQ_TIME').html(objData.REQ_TIME);
                $('#Detail_RES_TIME').html(objData.RES_TIME);
                $('#Detail_ERRCODE').html(objData.ERRCODE);
                $('#Detail_ERRMSG').html(objData.ERRMSG);
                $('#Detail_DATA').val(objData.Data);
                $('#Detail_RES_MSG').html(objData.RES_MSG);
                $('#Detail_REQ_MSG').html(objData.REQ_MSG);
                jsDiaglog();
            }
        }
        function Get_LogDetail_Error()
        {
            alert("Có lỗi khi lấy dữ liệu");
        }
    </script>
     <script type="text/javascript">
//       $(document).ready(function(){
//         $(".JchkAll").click(function(){
//        // alert($(this).is(':checked'));
//            $('.JchkGrid').attr('checked',$(this).is(':checked'));
//            GetListSoCT();
//         });
//          
//         });
         function GetListSoCT() {
            var values = $('input:checkbox:checked.JchkGrid').map(function() {
                return this.value;
            }).get();
            
            
        
            if (values == '') {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);
               
                
            }
            else {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);
               

            }
        }
    </script>

    <style type="text/css">
        
        .container{width:100%; margin-bottom:30px}
        .dataView{padding-bottom:10px}
        .JchkAll,.JchkGrid{}
        
                 
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle">TRA CỨU LOG MOBI TCT</p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
            <tr>
                <td align="left" style="width:15%" class="form_label">Từ ngày</td>
                <td class="form_control" style="width:35%"><span style="display:block; width:40%; position:relative;vertical-align:middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                    onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span></td>
                <td align="left" style="width:15%" class="form_label">Đến ngày</td>
                <td class="form_control" style="width:35%">
                <span style="display:block; width:40%; position:relative; vertical-align:middle;">
                    <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                     onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span></td>
            </tr>
           
            <tr>
                <td align="left" class="form_label">Mã số thuế</td>
                <td class="form_control"><asp:TextBox ID="txtMST" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <%--<td align="left" class="form_label">Tên Người nộp thuế</td>
                <td class="form_control"><asp:TextBox ID="txtTen_NNT" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>--%>
                <td align="left" class="form_label">
                    Loại Giao Dịch
                </td>
                <td class="form_control">
                    <asp:DropDownList ID="cboLoaiMSG" runat="server" Width="90%" CssClass="selected inputflat">
                        <asp:ListItem Selected="True" Value="" Text="Tất cả">
                                               
                        </asp:ListItem>
                        <asp:ListItem Value="MOLKTAIKHOAN" Text="Mở tài khoản liên kết"> </asp:ListItem>
                        <asp:ListItem Value="DONGLKTAIKHOAN" Text="Đóng tài khoản liên kết"> </asp:ListItem>
                        <asp:ListItem Value="NOPTHUE" Text="Thông tin khoản nộp"> </asp:ListItem>
                        <asp:ListItem Value="XACTHUCOTP" Text="Xác thực OTP"> </asp:ListItem>
                        <asp:ListItem Value="XACTHUCTRANGTHAI" Text="Xác thực trạng thái"> </asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
           
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnTimKiem" CssClass="buttonField" runat="server" Text="Tìm kiếm"  />
                    
                    
                </td>
                
            </tr>
            <tr>
                 <td align="center" colspan="4">
                 <asp:Label ID="lblError" runat="server" ForeColor="Red" ></asp:Label>
                 </td>
            </tr>
        </table>
    </div>

    <div class="dataView">
    
    <asp:HiddenField id="ListCT_NoiDia" runat="server" />
     <asp:GridView ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="100%" BorderColor="Black" AllowPaging="True"   PageSize="30">
            <Columns>
            
                <asp:BoundField HeaderText="STT" DataField="STT" Visible="true" ReadOnly="true" />
                <%--<asp:BoundField HeaderText="Ngày Nhận MSG" DataField="NGAYNHANMSG" Visible="true" ReadOnly="true" ItemStyle-HorizontalAlign="Center"   />--%>              
                <asp:TemplateField  HeaderText="ID">
              
                <ItemTemplate >
                
                <a href="#" onclick="jsChiTietNNT('<%#Eval("ID").ToString %>')"><%#Eval("ID").ToString%></a>
                </ItemTemplate>

                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:BoundField DataField="MST" HeaderText="Mã số thuế" ReadOnly="True" />
                 <asp:BoundField DataField="REQ_TYPE" HeaderText="Loại message" 
                    ReadOnly="True"  />
                <asp:BoundField DataField="REQ_TIME" HeaderText="Thời gian yêu cầu" 
                    ReadOnly="True" />
               
                 <asp:BoundField DataField="RES_TIME" HeaderText="Thời gian phản hồi" 
                    ReadOnly="True"  />
                <asp:BoundField DataField="ERRCODE" HeaderText="Mã Lỗi" ReadOnly="True" />
                <asp:BoundField DataField="ERRMSG" HeaderText="Ghi chú" ReadOnly="True" />
                
            </Columns>
      
            <HeaderStyle BackColor="#999966" BorderColor="Black" BorderStyle="Solid" 
                BorderWidth="1px" Font-Bold="True" />
            <AlternatingRowStyle BackColor="#EFEDE4" />
           
        </asp:GridView>
       
    </div>
    <div id="dialog-popup" style="display: none;">
        </div>
    <div id="wapaler" style="display: none;">
        </div>
    <div class="msgBox" id="msgBox_Detail" style=" display:none; opacity: 1; min-height: 100px;width:800px;">
            <div class="msgBoxTitle">Thông tin chi tiết message</div>
            <div>
                <div>
                    <div class="msgBoxContent" id="msgBox_SHKB_CONTENT">
                     
                      <table border="0" cellpadding="2" cellspacing="1" class="form_input" style="width:100%">
        <tr class="grid-heading">
            <td  style ="width:15%" >
                <b>ID</b> 
            </td>
            <td id="Detail_ID"  style ="width:35%">
                
            </td>
            <td  style ="width:15%" >
                <b>Loại MSG</b> 
            </td>
            <td id="Detail_REQ_TYPE"  style ="width:35%">
                
            </td>
        </tr>
       <tr >
            <td >
                <b>Request Time</b> 
            </td>
            <td id="Detail_REQ_TIME"  style ="width:35%">
                
            </td>
            <td  style ="width:15%" >
                <b>Response Time</b> 
            </td>
            <td id="Detail_RES_TIME"  style ="width:35%">
            </td>
        </tr>
       <tr >
            <td >
                <b>Mã lỗi</b> 
            </td>
            <td id="Detail_ERRCODE"  style ="width:35%">
                
            </td>
            <td  style ="width:15%" >
                <b>Mô tả lỗi</b> 
            </td>
            <td id="Detail_ERRMSG"  style ="width:35%">
            </td>
        </tr>
      <tr >
            <td colspan="2" >
                <b>Request Object</b> 
            </td>
           
            <td  colspan="2" >
                <b>Response Object</b> 
            </td>
          
        </tr>
                          <tr >
            <td colspan="2" >
               <textarea id="Detail_DATA" readonly="readonly" disabled="disabled" style="width:99%" rows="7"></textarea>
            </td>
           
            <td  colspan="2" >
               <textarea id="Detail_RES_MSG" readonly="readonly" disabled="disabled" style="width:99%" rows="7"></textarea> 
            </td>
        </tr>
 <tr >
            <td colspan="4" >
              <textarea id="Detail_REQ_MSG" readonly="readonly" disabled="disabled" style="width:99%" rows="7"></textarea> 
            </td>
           
           
          
        </tr>
    </table>           
                    </div>
                     
                </div>
                   <div class="msgBoxButtons" id="msgBox_SHKB_ConfirmDivButton">
                       <input type="hidden" id="msgBox_result" value="" />
                        <input id="msgBox-btn-close" class="btn btn-primary" onclick="jsMsgBoxClose();" type="button" name="Ok" value="Đóng" />
                  </div>
                
            </div>
        </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
        function jsDiaglog() {
          //  $('#msgBox_result').val('');
         //   document.getElementById('msgBox_txtMaDanhMuc').value = "";
         //   document.getElementById('msgBox_txtTenDanhMuc').value = "";
            // console.log('Start');
            $('#wapaler').css("display", "block");
            $('#msgBox_Detail').css("display", "block");

            jsDivAutoCenter('#msgBox_Detail');
           // msgBox_jsSearch();
        }
        function jsMsgBoxClose() {

            $('#wapaler').css("display", "none");
            $('#msgBox_Detail').css("display", "none");
        }
        function jsDivAutoCenter(divName) {
            //      console.log('em chay phat' + divName);
            $(divName).css({
                position: 'fixed',
                left: ($(window).width() - $(divName).outerWidth()) / 2,
                top: ($(window).height() - $(divName).outerHeight()) / 2,
                height: 'auto'
            });
        }
        //pop-up
        $(window).resize(function () {

            jsDivAutoCenter('.msgBox');
        });
    </script>		
</asp:Content>
