﻿Imports System.Data
Imports Business.NTDT.NNTDT

Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao
Imports log4net
Imports Business.Common.mdlCommon
Imports Newtonsoft.Json
Imports Business.MobiTCT
Partial Class MobiTCT_frmTracuuLogMobiTCT
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If

        If Not IsPostBack Then
           
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub
    Private Sub load_dataGrid()
        Dim ds As DataSet

        'Dim WhereClause As String = " AND A.TRANGTHAI='" & Business.Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_SEND301_ERROR & "' "
        'tam thoi ko choi ma CN
        ' Dim WhereClause As String = " AND MA_CN='" & Session.Item("MA_CN_USER_LOGON").ToString() & "' AND A.LOAIMSG='304'  "
        Dim WhereClause As String = "   "
        Try
            dtgrd.DataSource = Nothing
            dtgrd.DataBind()
            If txtMST.Text <> "" Then
                WhereClause += " AND upper(a.MST) like upper('%" & txtMST.Text.Trim & "%')"
            End If
          
            If txtTuNgay.Value <> "" Then
                WhereClause += " AND to_char(a.REQ_TIME,'RRRRMMDD') >=" & ConvertDateToNumber(txtTuNgay.Value)
            End If
            If txtDenNgay.Value <> "" Then
                WhereClause += " AND to_char(a.REQ_TIME,'RRRRMMDD') <= " & ConvertDateToNumber(txtDenNgay.Value)
            End If
            
            If cboLoaiMSG.SelectedValue.Length > 0 Then
                WhereClause += " AND A.REQ_TYPE = '" & cboLoaiMSG.SelectedValue.ToString() & "' "
            End If

            Dim vobjSearch As Business.MobiTCT.MobiTCT_LogMSG = New Business.MobiTCT.MobiTCT_LogMSG()
            ds = vobjSearch.fnc_tracuuLog(WhereClause, " ORDER BY REQ_TIME DESC") 'Business.HQ247.daCTUHQ304.getMSG304_TracuuXuly(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                ' dtgrd.CurrentPageIndex = 0

                dtgrd.DataSource = ds.Tables(0)
                'dtgrd.DataBind()
                dtgrd.PageIndex = 0
                dtgrd.DataBind()

                'grdExport.DataSource = ds.Tables(0)
                'grdExport.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        load_dataGrid()

    End Sub


    
    Protected Sub dtgrd_PageIndexChanging(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dtgrd.PageIndexChanging
        load_dataGrid()
        dtgrd.PageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetLogDetail(ByVal pv_strID As String) As String
        Dim strResult As String = ""
        Try
            Dim objLog As Business.MobiTCT.MobiTCT_LogMSG = New Business.MobiTCT.MobiTCT_LogMSG()
            Dim objResult As Business.MobiTCT.objMobiTCTInfo = New Business.MobiTCT.objMobiTCTInfo()
            Dim ds As DataSet = objLog.fnc_GetLogRequest(pv_strID)
            If Not IsEmptyDataSet(ds) Then
                objResult.ID = ds.Tables(0).Rows(0)("ID").ToString()
                objResult.MST = ds.Tables(0).Rows(0)("MST").ToString()
                objResult.ERRCODE = ds.Tables(0).Rows(0)("ERRCODE").ToString()
                objResult.ERRMSG = ds.Tables(0).Rows(0)("ERRMSG").ToString()
                objResult.REQ_MSG = ds.Tables(0).Rows(0)("REQ_MSG").ToString()
                objResult.REQ_TIME = ds.Tables(0).Rows(0)("REQ_TIME").ToString()
                objResult.REQ_TYPE = ds.Tables(0).Rows(0)("REQ_TYPE").ToString()
                objResult.RES_MSG = ds.Tables(0).Rows(0)("RES_MSG").ToString()
                objResult.RES_TIME = ds.Tables(0).Rows(0)("RES_TIME").ToString()
                If objResult.REQ_MSG.Length > 0 Then
                    Dim vobjTCT As Business.MobiTCT.TCTObjectIn = JsonConvert.DeserializeObject(Of TCTObjectIn)(objResult.REQ_MSG)
                    If Not vobjTCT.duLieu Is Nothing Then
                        objResult.Data = TCTObjectIn.Base64Decode(vobjTCT.duLieu)
                    End If
                End If

                ' objResult.Data = ds.Tables(0).Rows(0)("RES_TIME").ToString()
                ' TCTObjectIn vobjTCT = JsonConvert.DeserializeObject<TCTObjectIn>(pIn.ToString());
                Return JsonConvert.SerializeObject(objResult)
            End If

        Catch ex As Exception
            Return ""
        End Try
        Return strResult
    End Function
End Class
