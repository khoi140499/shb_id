﻿Imports System.Data
Imports Business.NTDT.NNTDT

Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao
Imports log4net
Imports Business.Common.mdlCommon
Imports Newtonsoft.Json
Imports Business.MobiTCT
Partial Class MobiTCT_frmCapnhatTT_LKTK_MobiTCT
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If

        If Not IsPostBack Then
            Load_TrangThai()
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub
    Private Sub load_dataGrid()
        Dim ds As DataSet

        'Dim WhereClause As String = " AND A.TRANGTHAI='" & Business.Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_SEND301_ERROR & "' "
        'tam thoi ko choi ma CN
        ' Dim WhereClause As String = " AND MA_CN='" & Session.Item("MA_CN_USER_LOGON").ToString() & "' AND A.LOAIMSG='304'  "
        Dim WhereClause As String = "   "
        Try
            dtgrd.DataSource = Nothing
            dtgrd.DataBind()
            If txtMST.Text <> "" Then
                WhereClause += " AND upper(a.MST) like upper('%" & txtMST.Text.Trim & "%')"
            End If

            If txtTuNgay.Value <> "" Then
                WhereClause += " AND to_char(a.NGAYMO,'RRRRMMDD') >=" & ConvertDateToNumber(txtTuNgay.Value)
            End If
            If txtDenNgay.Value <> "" Then
                WhereClause += " AND to_char(a.NGAYMO,'RRRRMMDD') <= " & ConvertDateToNumber(txtDenNgay.Value)
            End If

            If cboLoaiMSG.SelectedValue.Length > 0 Then
                WhereClause += " AND A.LOAI = '" & cboLoaiMSG.SelectedValue.ToString() & "' "
            End If
            If cboTrangThai.SelectedIndex > 0 Then
                WhereClause += " AND A.TRANGTHAI = '" & cboTrangThai.SelectedValue.ToString() & "' "
            End If
            If txtSoTK.Text <> "" Then
                WhereClause += " AND upper(a.SOTAIKHOAN_THE) like upper('%" & txtSoTK.Text.Trim & "%')"
            End If

            Dim vobjSearch As Business.MobiTCT.MobiTCTLienKetTK = New Business.MobiTCT.MobiTCTLienKetTK()
            ds = vobjSearch.fnc_tracuuTaiKhoanLienKet(WhereClause) 'Business.HQ247.daCTUHQ304.getMSG304_TracuuXuly(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                ' dtgrd.CurrentPageIndex = 0

                dtgrd.DataSource = ds.Tables(0)
                'dtgrd.DataBind()
                dtgrd.PageIndex = 0
                dtgrd.DataBind()

                'grdExport.DataSource = ds.Tables(0)
                'grdExport.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        load_dataGrid()

    End Sub



    Protected Sub dtgrd_PageIndexChanging(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dtgrd.PageIndexChanging
        load_dataGrid()
        dtgrd.PageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetLogDetail(ByVal pv_strID As String) As String
        Dim strResult As String = ""
        Try
            Dim vobjSearch As Business.MobiTCT.MobiTCTLienKetTK = New Business.MobiTCT.MobiTCTLienKetTK()

            Dim objResult As Business.MobiTCT.objDSTAIKHOANLIENKETInfo = New Business.MobiTCT.objDSTAIKHOANLIENKETInfo()
            Dim ds As DataSet = vobjSearch.fnc_tracuuTaiKhoanLienKet(" AND ID='" & pv_strID & "' ") 'objLog.fnc_GetLogRequest(pv_strID)
            If Not IsEmptyDataSet(ds) Then
                ' objResult.ID = ds.Tables(0).Rows(0)("ID").ToString()
                'objResult.MST = ds.Tables(0).Rows(0)("MST").ToString()
                objResult.ID = ds.Tables(0).Rows(0)("ID").ToString()
                objResult.MADOITAC = ds.Tables(0).Rows(0)("MADOITAC").ToString()
                objResult.LOAI = ds.Tables(0).Rows(0)("LOAI").ToString()
                objResult.MST = ds.Tables(0).Rows(0)("MST").ToString()
                objResult.TEN_NNT = ds.Tables(0).Rows(0)("TEN_NNT").ToString()
                objResult.LOAIGIAYTO = ds.Tables(0).Rows(0)("LOAIGIAYTO").ToString()
                objResult.SOGIAYTO = ds.Tables(0).Rows(0)("SOGIAYTO").ToString()
                objResult.DIENTHOAI = ds.Tables(0).Rows(0)("DIENTHOAI").ToString()
                objResult.PHUONGTHUC = ds.Tables(0).Rows(0)("PHUONGTHUC").ToString()
                objResult.SOTAIKHOAN_THE = ds.Tables(0).Rows(0)("SOTAIKHOAN_THE").ToString()
                objResult.NGAYPHATHANH = ds.Tables(0).Rows(0)("NGAYPHATHANH").ToString()
                objResult.NGAYHT = ds.Tables(0).Rows(0)("NGAYHT").ToString()
                objResult.TRANGTHAI = ds.Tables(0).Rows(0)("TRANGTHAI").ToString()
                objResult.LOAIOTP = ds.Tables(0).Rows(0)("LOAIOTP").ToString()
                objResult.MAGIAODICH = ds.Tables(0).Rows(0)("MAGIAODICH").ToString()
                objResult.NGAYMO = ds.Tables(0).Rows(0)("NGAYMO").ToString()
                objResult.TTHAI_TCT = ds.Tables(0).Rows(0)("TTHAI_TCT").ToString()
                objResult.IDREF = ds.Tables(0).Rows(0)("IDREF").ToString()
                objResult.MOTA = ds.Tables(0).Rows(0)("MOTA").ToString()
                objResult.GHICHU = ds.Tables(0).Rows(0)("GHICHU").ToString()
                objResult.EMAIL = ds.Tables(0).Rows(0)("EMAIL").ToString()

                ' objResult.Data = ds.Tables(0).Rows(0)("RES_TIME").ToString()
                ' TCTObjectIn vobjTCT = JsonConvert.DeserializeObject<TCTObjectIn>(pIn.ToString());
                Return JsonConvert.SerializeObject(objResult)
            End If

        Catch ex As Exception
            Return ""
        End Try
        Return strResult
    End Function
    Private Sub Load_TrangThai()
        Try

            Dim sql = "select * from TCS_DM_TRANGTHAI A where a.loai='DSTKLIENKET' "
            Dim dr As DataSet = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            cboTrangThai.DataSource = dr.Tables(0)
            cboTrangThai.DataTextField = "MOTA"
            cboTrangThai.DataValueField = "TRANGTHAI"
            cboTrangThai.DataBind()
            cboTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái Lỗi: " & ex.Message)
        End Try
    End Sub
    '
    <System.Web.Services.WebMethod()> _
    Public Shared Function jsUpdateTTLK(ByVal pv_strID As String, ByVal pv_strTrangThai As String, ByVal pv_StrGhiChu As String) As String
        Dim objResult As objResponse = New objResponse()

        Try
            objResult.TRANGTHAI = "-1"
            objResult.MOTA = "Hết phiên làm việc"
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return JsonConvert.SerializeObject(objResult)
            End If
            Dim vobjSearch As Business.MobiTCT.MobiTCTLienKetTK = New Business.MobiTCT.MobiTCTLienKetTK()

            ' Dim objResult As Business.MobiTCT.objDSTAIKHOANLIENKETInfo = New Business.MobiTCT.objDSTAIKHOANLIENKETInfo()
            Dim ds As DataSet = vobjSearch.fnc_tracuuTaiKhoanLienKet(" AND ID='" & pv_strID & "' ") 'objLog.fnc_GetLogRequest(pv_strID)
            If Not IsEmptyDataSet(ds) Then
                ' objResult.ID = ds.Tables(0).Rows(0)("ID").ToString()
                'objResult.MST = ds.Tables(0).Rows(0)("MST").ToString()
                'xu ly cho loai open

                'neu cap nhat ve thanh cong la 06
                If pv_strTrangThai.Equals("06") Then
                    objResult.TRANGTHAI = vobjSearch.MOLIENKETTK_06(pv_strID, pv_StrGhiChu)
                ElseIf pv_strTrangThai.Equals("07") Then
                    objResult.TRANGTHAI = vobjSearch.MOLIENKETTK_07(pv_strID, ds.Tables(0).Rows(0)("IDREF").ToString(), pv_StrGhiChu)
                ElseIf pv_strTrangThai.Equals("04") Then
                    objResult.TRANGTHAI = vobjSearch.MOLIENKETTK_04(pv_strID, pv_StrGhiChu)
                ElseIf pv_strTrangThai.Equals("03") Then
                    objResult.TRANGTHAI = vobjSearch.MOLIENKETTK_04(pv_strID, pv_StrGhiChu)
                End If
                ' Return JsonConvert.SerializeObject(objResult)
            End If
            If Not objResult.TRANGTHAI.Equals("00") Then
                objResult.MOTA = "Lỗi khi cập nhật dữ liệu"
            End If

        Catch ex As Exception
            logger.Error(ex.Message & "-" & ex.StackTrace)
            objResult.TRANGTHAI = "-1"
            objResult.MOTA = "Hết phiên làm việc"
        End Try
        Return JsonConvert.SerializeObject(objResult)
    End Function
End Class
