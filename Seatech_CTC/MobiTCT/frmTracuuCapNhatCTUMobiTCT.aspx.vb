﻿Imports System.Data
Imports Business.NTDT.NNTDT

Imports VBOracleLib
Imports System.IO
Imports Business.BaoCao
Imports log4net
Imports Business.Common.mdlCommon
Imports Newtonsoft.Json
Imports Business.MobiTCT
Partial Class MobiTCT_frmTracuuCapNhatCTUMobiTCT
    Inherits System.Web.UI.Page
    Private Shared logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If

        If Not IsPostBack Then
            Load_TrangThai()
            txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
            txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User").ToString))
        End If
    End Sub
    Private Sub load_dataGrid()
        Dim ds As DataSet

        'Dim WhereClause As String = " AND A.TRANGTHAI='" & Business.Common.mdlCommon.TCS_HQ247_TRANGTHAI_304_SEND301_ERROR & "' "
        'tam thoi ko choi ma CN
        ' Dim WhereClause As String = " AND MA_CN='" & Session.Item("MA_CN_USER_LOGON").ToString() & "' AND A.LOAIMSG='304'  "
        Dim WhereClause As String = "   "
        Try
            dtgrd.DataSource = Nothing
            dtgrd.DataBind()
            If txtMST.Text <> "" Then
                WhereClause += " AND upper(a.MST) like upper('%" & txtMST.Text.Trim & "%')"
            End If

            If txtTuNgay.Value <> "" Then
                WhereClause += " AND to_char(a.NGAYMSG,'RRRRMMDD') >=" & ConvertDateToNumber(txtTuNgay.Value)
            End If
            If txtDenNgay.Value <> "" Then
                WhereClause += " AND to_char(a.NGAYMSG,'RRRRMMDD') <= " & ConvertDateToNumber(txtDenNgay.Value)
            End If

            If cboLoaiMSG.SelectedValue.Length > 0 Then
                WhereClause += " AND A.MALOAIHINHTHUE = '" & cboLoaiMSG.SelectedValue.ToString() & "' "
            End If
            If cboTrangThai.SelectedIndex > 0 Then
                WhereClause += " AND A.TRANGTHAI = '" & cboTrangThai.SelectedValue.ToString() & "' "
            End If
            If txtSoTK.Text <> "" Then
                WhereClause += " AND upper(a.SOTAIKHOAN) like upper('%" & txtSoTK.Text.Trim & "%')"
            End If
            If txtMATHAMCHIEU.Text <> "" Then
                WhereClause += " AND upper(a.MATHAMCHIEU) like upper('%" & txtMATHAMCHIEU.Text.Trim & "%')"
            End If

            Dim vobjSearch As Business.MobiTCT.MobiTCTCTU = New Business.MobiTCT.MobiTCTCTU()
            ds = vobjSearch.fnc_tracuuCTU(WhereClause) 'Business.HQ247.daCTUHQ304.getMSG304_TracuuXuly(WhereClause)
            If Not IsEmptyDataSet(ds) Then
                ' dtgrd.CurrentPageIndex = 0

                dtgrd.DataSource = ds.Tables(0)
                'dtgrd.DataBind()
                dtgrd.PageIndex = 0
                dtgrd.DataBind()

                'grdExport.DataSource = ds.Tables(0)
                'grdExport.DataBind()
            Else
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu")
            End If
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách thông tin NNTDT. Lỗi:" & ex.Message)
        End Try
    End Sub
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

    Protected Sub btnTimKiem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTimKiem.Click
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        load_dataGrid()

    End Sub



    Protected Sub dtgrd_PageIndexChanging(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dtgrd.PageIndexChanging
        load_dataGrid()
        dtgrd.PageIndex = e.NewPageIndex
        dtgrd.DataBind()
    End Sub

    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function GetLogDetail(ByVal pv_strID As String) As String
        Dim strResult As String = ""
        Try
            Dim vobjSearch As Business.MobiTCT.MobiTCTCTU = New Business.MobiTCT.MobiTCTCTU()

            Dim objResult As Business.MobiTCT.objMobiTCTCTUInfo = New Business.MobiTCT.objMobiTCTCTUInfo()
            objResult = vobjSearch.fnc_tracuuCTUDetail(pv_strID)
            If Not objResult Is Nothing Then
                Return JsonConvert.SerializeObject(objResult)
            End If
        Catch ex As Exception
            Return ""
        End Try
        Return strResult
    End Function
    Private Sub Load_TrangThai()
        Try

            Dim sql = "select * from TCS_DM_TRANGTHAI A where a.loai='CTUMOBITCT' "
            Dim dr As DataSet = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text)
            cboTrangThai.DataSource = dr.Tables(0)
            cboTrangThai.DataTextField = "MOTA"
            cboTrangThai.DataValueField = "TRANGTHAI"
            cboTrangThai.DataBind()
            cboTrangThai.Items.Insert(0, "Tất cả")
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Không lấy được danh sách trạng thái Lỗi: " & ex.Message)
        End Try
    End Sub
    <System.Web.Services.WebMethod()> _
    Public Shared Function jsReSendCTU(ByVal strMaThamChieu As String) As String
        Dim objResult As objResponse = New objResponse()
        Try
            objResult.TRANGTHAI = "-1"
            objResult.MOTA = "Hết phiên làm việc"
            If Not clsCommon.GetSessionByID(HttpContext.Current.Session("User")) Then
                Return JsonConvert.SerializeObject(objResult)
            End If
            '05: cho tct phan hôi
            '09: thanh cong- da gui chung tu di thue
            '06: cho gui chung tu di thue
            Dim strSQL = "select * from set_ctu_hdr_mobitct where MaThamChieu='" & strMaThamChieu & "' AND TRANGTHAI in ('06','05','09')"
            Dim strSo_CT = ""
            Dim ds As DataSet = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If ds IsNot Nothing Then
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        strSo_CT = ds.Tables(0).Rows(0)("so_ct").ToString()
                    End If
                End If
            End If
            If strSo_CT.Length <= 0 Then
                logger.Error("Prc_SendCTUToTCT mathamchieu[" & strMaThamChieu & "]: loi khi gui chung tu di thue, khong tim thay chung tu hoac tran thai chung tu khong dung")
                objResult.TRANGTHAI = "1"
                objResult.MOTA = "Không tìm thấy chứng từ"
                Return JsonConvert.SerializeObject(objResult)
            End If
            Dim strErr = ""
            Dim strErrMSG = ""
            ProcMegService.ProMsg.SendMSG21_MobiTCT(strMaThamChieu, strSo_CT, strErr, strErrMSG)
            If strErr.Equals("01") Then
                MobiTCTCTU.UPDATE_CTU_THANHTOAN_TRANGTHAI_10(strMaThamChieu, strSo_CT)
                objResult.TRANGTHAI = "00"
                objResult.MOTA = "thành công"
                Return JsonConvert.SerializeObject(objResult)
            Else
                MobiTCTCTU.UPDATE_CTU_THANHTOAN_TRANGTHAI_09(strMaThamChieu, strSo_CT)
                logger.Error("Prc_SendCTUToTCT mathamchieu[" & strMaThamChieu & "]: " & strErr & "-" & strErrMSG)
                objResult.TRANGTHAI = "06"
                objResult.MOTA = "Lỗi khi gửi chứng từ đến TCT"
                Return JsonConvert.SerializeObject(objResult)
            End If
        Catch ex As Exception
        End Try
        Return ""
    End Function
End Class
