﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmTracuuLienKetTKMobiTCT.aspx.vb" Inherits="MobiTCT_frmTracuuLienKetTKMobiTCT" Title="TRA CỨU TÀI KHOẢN LIÊN KẾT" %>

<%@ Import Namespace="Business.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
    <link href="../css/dialog.css" rel="stylesheet" />
    <script type="text/javascript">
        function mask(str, textbox, loc, delim) {
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++) {
                for (var k = 0; k <= str.length; k++) {
                    if (k == locs[i]) {
                        if (str.substring(k, k + 1) != delim) {
                            str = str.substring(0, k) + delim + str.substring(k, str.length)
                        }
                    }
                }
            }
            textbox.value = str
        }
        function jsChiTietNNT(_ID) {

            PageMethods.GetLogDetail(_ID, Get_LogDetail_Complete, Get_LogDetail_Error);
        }
        function Get_LogDetail_Complete(pdata) {
            console.log(pdata);
            if (pdata.length > 0) {
                // console.log(pdata);
                var objData = JSON.parse(pdata);
                console.log(objData);
                $('#Detail_ID').html(objData.ID);
                $('#Detail_LOAI').html(objData.LOAI);
                $('#Detail_NGAYMO').html(objData.NGAYMO);
                $('#Detail_MOTA').html(objData.MOTA);
                $('#Detail_LOAIGIAYTO').html(objData.LOAIGIAYTO);
                $('#Detail_SOGIAYTO').html(objData.SOGIAYTO);
                $('#Detail_DIENTHOAI').html(objData.DIENTHOAI);
                $('#Detail_EMAIL').html(objData.EMAIL);
                $('#Detail_PHUONGTHUC').html(objData.PHUONGTHUC);
                $('#Detail_SOTAIKHOAN_THE').html(objData.SOTAIKHOAN_THE);


                jsDiaglog();
            }
        }
        function Get_LogDetail_Error() {
            alert("Có lỗi khi lấy dữ liệu");
        }
    </script>
    <script type="text/javascript">

        function GetListSoCT() {
            var values = $('input:checkbox:checked.JchkGrid').map(function () {
                return this.value;
            }).get();



            if (values == '') {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);


            }
            else {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);


            }
        }
    </script>

    <style type="text/css">
        .container {
            width: 100%;
            margin-bottom: 30px;
        }

        .dataView {
            padding-bottom: 10px;
        }

        .JchkAll, .JchkGrid {
        }

        .tdDetail {
            font-size: 10pt;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <p class="pageTitle">TRA CỨU TÀI KHOẢN LIÊN KẾT</p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
            <tr>
                <td align="left" style="width: 15%" class="form_label">Từ ngày</td>
                <td class="form_control" style="width: 35%"><span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                        onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                </span></td>
                <td align="left" style="width: 15%" class="form_label">Đến ngày</td>
                <td class="form_control" style="width: 35%">
                    <span style="display: block; width: 40%; position: relative; vertical-align: middle;">
                        <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display: block; float: left; margin-right: 1px;"
                            onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}" />
                    </span></td>
            </tr>

            <tr>
                <td align="left" class="form_label">Mã số thuế</td>
                <td class="form_control">
                    <asp:TextBox ID="txtMST" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <%--<td align="left" class="form_label">Tên Người nộp thuế</td>
                <td class="form_control"><asp:TextBox ID="txtTen_NNT" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>--%>
                <td align="left" class="form_label">Loại Liên kết
                </td>
                <td class="form_control">
                    <asp:DropDownList ID="cboLoaiMSG" runat="server" Width="90%" CssClass="selected inputflat">
                        <asp:ListItem Selected="True" Value="" Text="Tất cả">
                                               
                        </asp:ListItem>
                        <asp:ListItem Value="Open" Text="Mở tài khoản liên kết"> </asp:ListItem>
                        <asp:ListItem Value="Close" Text="Đóng tài khoản liên kết"> </asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left" class="form_label">Số Tài khoản/thẻ</td>
                <td class="form_control">
                    <asp:TextBox ID="txtSoTK" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <td align="left" class="form_label">Trạng thái</td>
                <td class="form_control">
                    <asp:DropDownList ID="cboTrangThai" runat="server" Width="90%" CssClass="selected inputflat">
                        <asp:ListItem Selected="True" Value="" Text="Tất cả">                     
                        </asp:ListItem>

                    </asp:DropDownList>
                </td>

            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnTimKiem" CssClass="buttonField" runat="server" Text="Tìm kiếm" />&nbsp;&nbsp;
                     <asp:Button ID="btnExport" CssClass="buttonField" runat="server" Text="Export" />

                </td>

            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </div>

    <div class="dataView">

        <asp:HiddenField ID="ListCT_NoiDia" runat="server" />
        <asp:GridView ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="100%" BorderColor="Black" AllowPaging="True" PageSize="30">
            <Columns>

                <asp:BoundField HeaderText="STT" DataField="STT" Visible="true" ReadOnly="true" />
                <%--<asp:BoundField HeaderText="Ngày Nhận MSG" DataField="NGAYNHANMSG" Visible="true" ReadOnly="true" ItemStyle-HorizontalAlign="Center"   />--%>
                <asp:TemplateField HeaderText="Mã tham chiếu">

                    <ItemTemplate>

                        <a href="#" onclick="jsChiTietNNT('<%#Eval("ID").ToString %>')"><%#Eval("ID").ToString%></a>
                    </ItemTemplate>

                    <ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
                </asp:TemplateField>
                <asp:BoundField DataField="NGAYMO" HeaderText="Thời gian"
                    ReadOnly="True">
                    <ItemStyle HorizontalAlign="Center" Width="70px" />
                </asp:BoundField>
                <asp:BoundField DataField="LOAI" HeaderText="Loại liên kết"
                    ReadOnly="True">
                    <ItemStyle Width="60px" />
                </asp:BoundField>
                <asp:BoundField DataField="MST" HeaderText="Mã số thuế" ReadOnly="True">

                    <ItemStyle Width="70px" />
                </asp:BoundField>

                <asp:BoundField DataField="TEN_NNT" HeaderText="Tên người nộp thuế"
                    ReadOnly="True" />

                <asp:BoundField DataField="SOTAIKHOAN_THE" HeaderText="Tài khoản/Thẻ" ReadOnly="True" />
                <asp:BoundField DataField="DIENTHOAI" HeaderText="Số điện thoại" ReadOnly="True" />
                <asp:BoundField DataField="MOTA" HeaderText="Trạng thái" ReadOnly="True" />
                <asp:BoundField DataField="GHICHU" HeaderText="Ghi chú" ReadOnly="True" />

            </Columns>

            <HeaderStyle BackColor="#999966" BorderColor="Black" BorderStyle="Solid"
                BorderWidth="1px" Font-Bold="True" />
            <AlternatingRowStyle BackColor="#EFEDE4" />

        </asp:GridView>
        <asp:GridView ID="grdExport" runat="server" AutoGenerateColumns="False" ShowFooter="true"
            Width="100%" BorderColor="#000" CssClass="grid_data" Visible="false">
            <AlternatingRowStyle CssClass="grid_item_alter"></AlternatingRowStyle>
            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
            <RowStyle CssClass="grid_item" />
            <Columns>
                <asp:BoundField DataField="ID" HeaderText="ID" />
                <asp:BoundField DataField="LOAI" HeaderText="Loại MSG" />
                <asp:BoundField DataField="NGAYMO" HeaderText="Request time" />
                <asp:BoundField DataField="LOAIGIAYTO" HeaderText="Loại giấy tờ" />
                <asp:BoundField DataField="SOGIAYTO" HeaderText="Số giấy tờ" />
                <asp:BoundField DataField="DIENTHOAI" HeaderText="Số điện thoại" />
                <asp:BoundField DataField="EMAIL" HeaderText="Email" />
                <asp:BoundField DataField="PHUONGTHUC" HeaderText="Phương thức" />
                <asp:BoundField DataField="SOTAIKHOAN_THE" HeaderText="Tài khoản/thẻ" />
                <asp:BoundField DataField="MOTA" HeaderText="Trạng thái" />
                <asp:BoundField DataField="GHICHU" HeaderText="Ghi chú" />

            </Columns>

            <HeaderStyle BackColor="#999966" BorderColor="Black" BorderStyle="Solid"
                BorderWidth="1px" Font-Bold="True" />
            <AlternatingRowStyle BackColor="#EFEDE4" />

        </asp:GridView>
    </div>
    <div id="dialog-popup" style="display: none;">
    </div>
    <div id="wapaler" style="display: none;">
    </div>
    <div class="msgBox" id="msgBox_Detail" style="display: none; opacity: 1; min-height: 100px; width: 600px;">
        <div class="msgBoxTitle">Thông tin chi tiết Liên kết</div>
        <div>
            <div>
                <div class="msgBoxContent" id="msgBox_SHKB_CONTENT">

                    <table border="0" cellpadding="2" cellspacing="1" class="form_input" style="width: 100%">
                        <tr class="grid-heading tdDetail">
                            <td class="tdDetail" style="width: 15%">
                                <b>ID</b>
                            </td>
                            <td class="tdDetail" id="Detail_ID" style="width: 35%"></td>
                        </tr>
                        <tr>
                            <td class="tdDetail" style="width: 15%">
                                <b>Loại MSG</b>
                            </td>
                            <td class="tdDetail" id="Detail_LOAI" style="width: 35%"></td>
                        </tr>
                        <tr>
                            <td class="tdDetail">
                                <b>Request Time</b>
                            </td>
                            <td class="tdDetail" id="Detail_NGAYMO" style="width: 35%"></td>
                        </tr>
                        <tr>
                            <td class="tdDetail" style="width: 15%">
                                <b>Trạng thái</b>
                            </td>
                            <td class="tdDetail" id="Detail_MOTA" style="width: 35%"></td>
                        </tr>
                        <tr>
                            <td class="tdDetail">
                                <b>Loại giấy tờ</b>
                            </td>
                            <td class="tdDetail" id="Detail_LOAIGIAYTO" style="width: 35%"></td>
                        </tr>
                        <tr>
                            <td class="tdDetail" style="width: 15%">
                                <b>Số giấy tờ</b>
                            </td>
                            <td class="tdDetail" id="Detail_SOGIAYTO" style="width: 35%"></td>
                        </tr>
                        <tr>
                            <td class="tdDetail">
                                <b>Điện thoại</b>
                            </td>
                            <td class="tdDetail" id="Detail_DIENTHOAI" style="width: 35%"></td>
                        </tr>
                        <tr>
                            <td class="tdDetail" style="width: 15%">
                                <b>Email</b>
                            </td>
                            <td class="tdDetail" id="Detail_EMAIL" style="width: 35%"></td>
                        </tr>
                        <tr>
                            <td class="tdDetail">
                                <b>Phương thức</b>
                            </td>
                            <td class="tdDetail" id="Detail_PHUONGTHUC" style="width: 35%"></td>
                        </tr>
                        <tr>
                            <td class="tdDetail" style="width: 15%">
                                <b>Số tài Khoản/Thẻ</b>
                            </td>
                            <td class="tdDetail" id="Detail_SOTAIKHOAN_THE" style="width: 35%"></td>
                        </tr>

                    </table>
                </div>

            </div>
            <div class="msgBoxButtons" id="msgBox_SHKB_ConfirmDivButton">
                <input type="hidden" id="msgBox_result" value="" />
                <input id="msgBox-btn-close" class="btn btn-primary" onclick="jsMsgBoxClose();" type="button" name="Ok" value="Đóng" />
            </div>

        </div>
    </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function () {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
        function jsDiaglog() {
            //  $('#msgBox_result').val('');
            //   document.getElementById('msgBox_txtMaDanhMuc').value = "";
            //   document.getElementById('msgBox_txtTenDanhMuc').value = "";
            // console.log('Start');
            $('#wapaler').css("display", "block");
            $('#msgBox_Detail').css("display", "block");

            jsDivAutoCenter('#msgBox_Detail');
            // msgBox_jsSearch();
        }
        function jsMsgBoxClose() {

            $('#wapaler').css("display", "none");
            $('#msgBox_Detail').css("display", "none");
        }
        function jsDivAutoCenter(divName) {
            //      console.log('em chay phat' + divName);
            $(divName).css({
                position: 'fixed',
                left: ($(window).width() - $(divName).outerWidth()) / 2,
                top: ($(window).height() - $(divName).outerHeight()) / 2,
                height: 'auto'
            });
        }
        //pop-up
        $(window).resize(function () {

            jsDivAutoCenter('.msgBox');
        });
    </script>
</asp:Content>
