﻿Imports System.Data
Imports VBOracleLib
Imports Business.BaoCao
Imports Business.XLCuoiNgay
Imports Business.Common
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports OpenXMLExcel.SLExcelUtility
Imports System.IO
Partial Class MobiTCT_frmBC_Tong
    Inherits System.Web.UI.Page

    Private mv_strNgay As String

    Private mv_strSHKB As String
    Private mv_strKyHieuCT As String
    Private mv_strMaDiemThu As String

    Private mv_strDBThu As String
    Private mv_strMaNHKB As String
    Private mv_strTenDiemThu As String
    Private mv_strTenKB As String
    Private mv_strLoaiBaoCao As String

#Region "Page event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session.Item("User") Is Nothing Then
            clsCTU.TCS_SetupParams(CType(Session("User"), Integer), mv_strSHKB, mv_strKyHieuCT, _
                                   mv_strMaDiemThu, mv_strTenDiemThu, mv_strDBThu, mv_strMaNHKB, mv_strTenKB)

        End If
        If Session.Item("User") Is Nothing Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Session.Item("User").ToString.Length = 0 Then
            Response.Redirect("~/pages/frmLogin.aspx", False)
            Exit Sub
        End If
        If Not clsCommon.GetSessionByID(Session.Item("User")) Then
            Response.Redirect("~/pages/Warning.html", False)
            Exit Sub
        End If
        If Not IsPostBack Then
            Try
                txtTuNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                txtDenNgay.Value = ConvertNumbertoString(clsCTU.TCS_GetNgayLV(Session.Item("User"))) 'Gán ngày làm việc hiện tại
                'get_NguyenTe()

                LoadDMChiNhanh()
                LoadDMTrangThai()
            Catch ex As Exception
                clsCommon.ShowMessageBox(Me, "Có lỗi xảy ra trong quá trình lấy thông tin!")
            End Try
        End If
    End Sub

    Protected Sub cmdIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIn.Click
        LogApp.AddDebug("cmdExit_Click", "BC TINH HINH THU NSNN: In BC")
        Try
            Dim strTuNgay As String = txtTuNgay.Value
            Dim strDenNgay As String = txtDenNgay.Value

            Dim strMaCN As String = CboChiNhanh.SelectedValue
            Dim strTrangThai As String = cboNguyenTe.SelectedValue
            If strTuNgay.Length <= 0 Then
                clsCommon.ShowMessageBox(Me, "Chưa nhập Từ ngày!")
                txtTuNgay.Focus()
                Return
            End If
            If strDenNgay.Length <= 0 Then
                clsCommon.ShowMessageBox(Me, "Chưa nhập Đến ngày!")
                txtDenNgay.Focus()
                Return
            End If
            If strMaCN.Equals("0") Then
                strMaCN = ""
            End If
            If strTrangThai.Equals("0") Then
                strTrangThai = ""
            End If
            TCS_BC_TONGCN(strTuNgay, strDenNgay, strMaCN, strTrangThai)
        Catch ex As Exception
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình Lập bảng kê chứng từ!")
        Finally
            cmdIn.Enabled = True
        End Try
    End Sub

    Protected Sub cmdExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        LogApp.AddDebug("cmdExit_Click", "BC TINH HINH THU NSNN: Thoat")
        Response.Redirect("../pages/frmHomeNV.aspx", False)
    End Sub
  
    Private Sub LoadDMChiNhanh()
        Try
            Dim strSQL As String = "select branch_id MA_CN,branch_id||'-'|| name TEN_CN from tcs_dm_chinhanh  where tinh_trang=1 order by branch_id"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)

            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then


               Dim vrow As DataRow = dt.NewRow()
                vrow(0) = "0"
                vrow(1) = "--Tất cả--"
                dt.Rows.Add(vrow)
                CboChiNhanh.DataSource = dt
                CboChiNhanh.DataValueField = "MA_CN"
                CboChiNhanh.DataTextField = "TEN_CN"
                CboChiNhanh.SelectedValue = "0"
                CboChiNhanh.DataBind()
            End If
           

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub LoadDMTrangThai()
        Try
            Dim strSQL As String = "select a.trangthai,a.trangthai||'-'||a.mota Mota from tcs_dm_trangthai a where loai='CTUMOBITCT' order by a.trangthai"
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)

            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then


                Dim vrow As DataRow = dt.NewRow()
                vrow(0) = "0"
                vrow(1) = "--Tất cả--"
                dt.Rows.Add(vrow)
                cboNguyenTe.DataSource = dt
                cboNguyenTe.DataValueField = "trangthai"
                cboNguyenTe.DataTextField = "Mota"
                cboNguyenTe.SelectedValue = "0"
                cboNguyenTe.DataBind()
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Call Report"
    Private Sub TCS_BC_TONGCN(ByVal pStrTuNgay As String, ByVal pStrDenNgay As String, ByVal pStrMA_CN As String, ByVal pStrTrangThai As String)
        Try
            Dim dsLog As DataSet = Business.MobiTCT.MobiTCTCTU.BC_CHITIET_CTU_MOBITCT(pStrTuNgay, pStrDenNgay, pStrMA_CN, pStrTrangThai)
            If IsEmptyDataSet(dsLog) Then
                clsCommon.ShowMessageBox(Me, "Không có dữ liệu báo cáo!")
                Return
            End If

            Const tableName As String = "RptData"
            Dim strExpFileUrl As String = Server.MapPath("~\MobiTCT\RPT_BAOCAO_CTU_MOBITCT_DETAIL.xls")
            Dim strFileLog As String = Server.MapPath("~\FileExcels") & "\" & Session.Item("User").ToString() & "_RPT_BAOCAO_CTU_MOBITCT_DETAIL_" & DateTime.Now.ToString("yyyyMMdd") & ".xls"
            Dim lstAliases As List(Of ExcelTitleAlias) = New List(Of ExcelTitleAlias)
            Dim objExcelTitleAlias As ExcelTitleAlias = New ExcelTitleAlias()
            objExcelTitleAlias.AliasName = "RPT_TITLE"
            objExcelTitleAlias.AliasValue = "BÁO CÁO GIAO DỊCH NỘP THUẾ QUA ETAX MOBILE"
            lstAliases.Add(objExcelTitleAlias)

            '   lstAliases.Add(New ExcelTitleAlias("RPT_TUNGAY"))
            dsLog.Tables(0).TableName = tableName
            'If ExportFileDownloadActionResult.CreateFileReport(strExpFileUrl, strFileLog, dsLog.Tables(0), tableName, lstAliases).Length > 0 Then
            If ExportFileDownloadActionResult.CreateFileReportDefault(strExpFileUrl, strFileLog, dsLog.Tables(0), tableName, lstAliases).Length > 0 Then
                Dim vArrByte As Byte() = File.ReadAllBytes(strFileLog)
                File.Delete(strFileLog)
                Dim sFileNameExport As String() = strFileLog.Split("\")
                '  File.Delete(strFileLog)
                Response.Clear()
                Response.Buffer = True
                Response.Buffer = True
                Response.AddHeader("content-disposition", "attachment;filename=" & sFileNameExport(sFileNameExport.Length - 1))
                Response.Charset = "utf-8"
                Response.ContentType = "application/vnd.ms-excel"

                Response.BinaryWrite(vArrByte)
                Response.Flush()

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



#End Region

#Region "Where Clause"
    Private Function checkHSC() As String
        Dim ds As DataSet
        Dim returnValue = ""
        If Not Session.Item("User") Is Nothing Then
            ds = buBaoCao.checkHSC(Session.Item("User").ToString)
            If Not ds Is Nothing Then
                returnValue = ds.Tables(0).Rows(0)("phan_cap").ToString
            End If
        End If
        Return returnValue
    End Function

   
#End Region

End Class
