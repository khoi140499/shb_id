﻿Imports System.Data
Imports System.Diagnostics
Imports System.Xml
Imports System.Xml.XmlDocument
Imports Business
Imports Business.Common.mdlCommon
Imports Business.Common.mdlSystemVariables
Imports VBOracleLib
Imports Business.ChungTu
Imports Business.NewChungTu
Imports Business.BaoCao
Imports System.Collections.Generic
Imports System.Xml.Linq
Imports ConcertCurToText
Imports System.IO
Imports System.Linq

Partial Class MobiTCT_frmChiTietGuiLaiTTSPMobile
    Inherits System.Web.UI.Page
    Private illegalChars As String = "[&]"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LogApp.AddDebug("Chon chung tu xem chi tiet", "TRA CUU CHUNG TU: Xem chi tiet chung tu")
        If Not IsPostBack Then
            LoadLoaiPhi()
            If Not Request.QueryString.Item("soct") Is Nothing Then
                Dim so_ct As String = Request.QueryString.Item("soct").ToString
                    GetChungTu(so_ct)
            End If
        End If
    End Sub
    Protected Sub GetChungTu(ByVal soct As String, Optional ByVal datatype As String = "")
        Dim SHKB As String = String.Empty
        Dim NGAY_KB As String = String.Empty
        Dim SO_CT As String = String.Empty
        Dim KYHIEU_CT As String = String.Empty
        Dim SO_BT As String = String.Empty
        Dim MA_DTHU As String = String.Empty
        Dim MA_NV As String = String.Empty
        Dim TRANG_THAI As String = String.Empty
        Dim MA_CN_USER As String = String.Empty
        Dim TT_SP As String = String.Empty
        Dim key As New KeyCTu
        Dim strSQL As String = ""
        strSQL = "SELECT SHKB,NGAY_KB,SO_CT,KYHIEU_CT,SO_BT,MA_DTHU,MA_NV,TRANG_THAI,MA_CN,NGAY_CT,TT_SP FROM tcs_ctu_hdr_mobitct WHERE SO_CT='" & soct & "'"
        Dim dt As DataTable = DataAccess.ExecuteToTable(strSQL)

        If Not dt Is Nothing Then
            SHKB = dt.Rows(0)("SHKB").ToString
            NGAY_KB = dt.Rows(0)("NGAY_KB").ToString
            SO_CT = dt.Rows(0)("SO_CT").ToString
            KYHIEU_CT = dt.Rows(0)("KYHIEU_CT").ToString
            SO_BT = dt.Rows(0)("SO_BT").ToString
            MA_DTHU = dt.Rows(0)("MA_DTHU").ToString
            MA_NV = dt.Rows(0)("MA_NV").ToString
            TRANG_THAI = dt.Rows(0)("TRANG_THAI").ToString
            MA_CN_USER = dt.Rows(0)("MA_CN").ToString
            TT_SP = dt.Rows(0)("TT_SP").ToString
        End If
        If ((Not SHKB Is Nothing) And (Not MA_NV Is Nothing) _
                     And (Not SO_BT Is Nothing) And (Not SO_CT Is Nothing)) Then
            'Dim key As New KeyCTu
            key.Ma_Dthu = MA_DTHU
            key.Kyhieu_CT = KYHIEU_CT
            key.Ma_NV = MA_NV
            key.Ngay_KB = NGAY_KB
            key.SHKB = SHKB
            key.So_BT = SO_BT
            key.So_CT = SO_CT
            'key.MA_CN = MA_CN_USER
            hdnMA_NV.Value = MA_NV
            Dim d_NgayLV As Date = ConvertNumberToDate(NGAY_KB)
            hdnNGAYLV.Value = d_NgayLV.ToString("dd/MM/yyyy")
            hdnSHKB.Value = SHKB
            hdnSO_BT.Value = SO_BT
            hdnSO_CTU.Value = SO_CT
            LoadCTChiTietAll(key)
        End If
        lbSoCT.Text = "Số CT: " & SO_CT
        lbSoCT.Visible = True

        btnGuiLaiTTSP.Enabled = False
        If (TRANG_THAI = "01" Or TRANG_THAI = "07") And TT_SP = "1" Then
            btnGuiLaiTTSP.Enabled = True
        End If
    End Sub
    Private Sub LoadCTChiTietAll(ByVal key As KeyCTu)
        Try
            Dim v_strTT As String = String.Empty
            Dim v_strTTThue As String = String.Empty
            Dim v_strLoaiThue As String = String.Empty
            Dim v_strPhuongThucTT As String = String.Empty
            Dim v_str_Ma_NNT As String = String.Empty
            Dim v_str_MA_NT As String = String.Empty
            'Load chứng từ header
            Dim dsCT As New DataSet
          
            dsCT = ChungTu.buChungTu.CTU_Header_Set_Mobile(key)

            If (dsCT.Tables(0).Rows.Count > 0) Then
                ShowHideCTRow(True)
                FillDataToCTHeader(dsCT)
                v_strTT = dsCT.Tables(0).Rows(0)("TRANG_THAI").ToString
                v_strLoaiThue = dsCT.Tables(0).Rows(0)("MA_LTHUE").ToString
                v_strPhuongThucTT = dsCT.Tables(0).Rows(0)("PT_TT").ToString
                v_str_Ma_NNT = dsCT.Tables(0).Rows(0)("Ma_NNThue").ToString
                v_strTTThue = dsCT.Tables(0).Rows(0)("TT_CTHUE").ToString
                v_str_MA_NT = dsCT.Tables(0).Rows(0)("MA_NT").ToString
            Else
                lblStatus.Text = "Không tìm thấy chứng từ cần tìm.Hãy thử lại"
                Exit Sub
            End If

            'Load thông tin chi tiết
            Dim dsCTChiTiet As New DataSet
            dsCTChiTiet = ChungTu.buChungTu.CTU_ChiTiet_Set_Mobile(key)
            If dsCTChiTiet.Tables(0).Rows.Count > 0 Then
                FillDataToCTDetail(dsCTChiTiet, v_str_Ma_NNT)
                CaculateTongTien(dsCTChiTiet.Tables(0))
            Else
                lblStatus.Text = "Không lấy được thông tin chi tiết của chứng từ.Hãy kiểm tra lại"
                Exit Sub
            End If


            ShowHideControlByLoaiThue(v_strLoaiThue)
            ShowHideControlByPT_TT(v_strPhuongThucTT, v_strTT)
            'Me.txtMatKhau.Focus()
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

            If Not ex.InnerException Is Nothing Then
                sbErrMsg = New StringBuilder()

                sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ ")
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.Message)
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.StackTrace)

                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)
            End If

            'Throw ex
        Finally
            'Catch ex As Exception
            '    logger.Error(ex.StackTrace)
        End Try
    End Sub
    'Duc anh: Load loai phi 08/07/2015
    Private Sub LoadLoaiPhi()
        Dim dtLoaiPhi As DataTable = DataAccess.ExecuteToTable("SELECT * from tcs_dm_phi where tinh_trang='1'")
        drlSanPham.DataSource = dtLoaiPhi
        drlSanPham.DataBind()
        drlSanPham.Items.Insert(0, New ListItem("Không xác định", "0", True))
    End Sub
    'Duc anh: Load loai phi

    Private Sub FillDataToCTHeader(ByVal dsCT As DataSet, Optional ByVal loaiThue As String = "")
        Try
            Dim strTenVT = String.Empty
            Dim strTenLH As String = String.Empty
            Dim strMaLH As String = String.Empty
            Dim dr As DataRow = dsCT.Tables(0).Rows(0)
            Dim v_strTrangThai As String = dr("TRANG_THAI").ToString()
            Dim v_strHTTT As String = GetString(dr("PT_TT")).ToString
            txtSHKB.Text = dr("SHKB").ToString()
            txtTenKB.Text = dr("TEN_KB").ToString()
            txtNGAY_KH_NH.Text = ConvertNumbertoString(dr("NGAY_KB").ToString())
            txtTKCo.Text = dr("TK_Co").ToString()
            txtTKNo.Text = dr("TK_No").ToString()
            txtMaDBHC.Text = dr("MA_XA").ToString()
            txtTenDBHC.Text = Get_TenTinh(dr("MA_XA").ToString())
            txtMaNNT.Text = dr("MA_NNTHUE").ToString()
            txtTenNNT.Text = dr("TEN_NNTHUE").ToString()
            txtDChiNNT.Text = dr("DC_NNTHUE").ToString()
            txtMaNNTien.Text = dr("Ma_NNTien").ToString()
            txtTenNNTien.Text = dr("TEN_NNTIEN").ToString()
            txtDChiNNTien.Text = dr("DC_NNTIEN").ToString()
            txtMaCQThu.Text = dr("MA_CQTHU").ToString()
            txtTenCQThu.Text = dr("TEN_CQTHU").ToString()
            Dim dt As New DataTable
            dt.Columns.Add("ID_HQ", Type.GetType("System.String"))
            dt.Columns.Add("Name", Type.GetType("System.String"))
            dt.Columns.Add("Value", Type.GetType("System.String"))
            dt.Columns(0).AutoIncrement = True
            Dim R As DataRow = dt.NewRow
            R("ID_HQ") = dr("MA_LTHUE").ToString()
            R("Name") = GetTenLoaiThue(dr("MA_LTHUE").ToString()) 'dr("MA_LTHUE").ToString() & "-" & GetTenLoaiThue(dr("MA_LTHUE").ToString())
            R("Value") = dr("MA_LTHUE").ToString()
            dt.Rows.Add(R)
            ddlLoaiThue.DataSource = dt
            ddlLoaiThue.DataTextField = "Name"
            ddlLoaiThue.DataValueField = "Value"
            ddlLoaiThue.DataBind()

            'txtDescLoaiThue.Text = GetTenLoaiThue(dr("MA_LTHUE").ToString())
            strMaLH = GetString(dr("LH_XNK").ToString())
            Get_LHXNK_ALL(strMaLH, strTenLH, strTenVT)
            txtLHXNK.Text = strMaLH
            txtDescLHXNK.Text = strTenVT & "-" & strTenLH
            txtSoKhung.Text = GetString(dr("SO_KHUNG").ToString())
            txtSoMay.Text = GetString(dr("SO_MAY").ToString())
            txtToKhaiSo.Text = GetString(dr("SO_TK").ToString())
            txtNgayDK.Text = GetString(dr("NGAY_TK"))
            'txtSo_BK.Text = GetString(dr("So_BK"))
            'txtNgay_BK.Text = GetString(dr("Ngay_BK"))
            ddlHTTT.SelectedValue = v_strHTTT
            ddlHTTT.SelectedItem.Text = Get_TenPTTT(v_strHTTT)
            'rowTKGL.Visible = False
            'If v_strHTTT = "03" Then
            '    rowTKGL.Visible = True
            '    rowSoDuNH.Visible = False
            '    rowTK_KH_NH.Visible = False
            '    txtTKGL.Text = GetString(dr("TK_KH_NH"))
            '    txtTenTKGL.Text = dr("TEN_KH_NH").ToString
            'End If
            If dr("ma_ntk").ToString = "1" Then
                txtTenTKCo.Text = "Tài khoản thu NSNN"
            ElseIf dr("ma_ntk").ToString = "2" Then
                txtTenTKCo.Text = "Tài khoản tạm thu"
            Else
                txtTenTKCo.Text = "Thu hoàn thuế GTGT"
            End If

            rowLHXNK.Visible = False

            txtTK_KH_NH.Text = GetString(dr("TK_KH_NH"))
            txtTenTK_KH_NH.Text = dr("TEN_KH_NH").ToString
            txtNGAY_KH_NH.Text = GetString(dr("NGAY_KH_NH"))
            txtMA_NH_A.Text = GetString(dr("MA_NH_A"))
            txtTen_NH_A.Text = GetString(dr("TEN_NH_A"))
            txtMA_NH_B.Text = GetString(dr("MA_NH_B"))
            txtTen_NH_B.Text = GetString(dr("TEN_NH_B"))
            txtDienGiaiNH.Text = GetString(dr("NH_HUONG"))
            txtDienGiaiHQ.Text = GetString(dr("DIENGIAI_HQ"))
            txtMA_NH_TT.Text = GetString(dr("MA_NH_TT"))
            txtTEN_NH_TT.Text = GetString(dr("TEN_NH_TT"))
            txtMaNT.Text = GetString(dr("Ma_NT"))
            txtTenNT.Text = GetString(dr("Ten_NT"))
            txtTimeLap.Text = GetString(dr("ngay_ht"))
            txtDacDiemPhuongTien.Text = GetString(dr("dac_diem_ptien"))
            txtSO_QD.Text = GetString(dr("so_qd"))
            txtCQ_QD.Text = GetString(dr("cq_qd"))
            txtNGAY_QD.Text = GetString(dr("ngay_qd"))
            ' txtTenTKNo.Text = dr("TEN_TKNO").ToString()
            'txtTenTKCo.Text = ""
            'tynk-comment phí và VAT - 08-04-2013*********
            Dim strPTTP As String = ""
            strPTTP = dr("pt_tinhphi").ToString()
            If strPTTP = "O" Then
                rdTypePN.Checked = True
                rdTypeMP.Checked = False
            ElseIf strPTTP = "W" Then
                rdTypeMP.Checked = True
                rdTypePN.Checked = False
            End If
            Dim strTT_CITAD As String = ""
            strTT_CITAD = dr("TT_CITAD").ToString()
            If strTT_CITAD = "1" Then
                RadioButton1.Checked = True
            Else
                RadioButton2.Checked = True
            End If
            If loaiThue.Equals("NTDT") Then
                If GetString(dr("Ma_NT")).Equals("USD") Then
                    txtCharge.Text = VBOracleLib.Globals.Format_Number2(dr("PHI_GD").ToString(), ",", ".")
                    txtVAT.Text = VBOracleLib.Globals.Format_Number2(dr("PHI_VAT").ToString(), ",", ".")
                    txtCharge2.Text = VBOracleLib.Globals.Format_Number2(dr("PHI_GD2").ToString(), ",", ".")
                    txtVat2.Text = VBOracleLib.Globals.Format_Number2(dr("PHI_VAT2").ToString(), ",", ".")
                Else
                    txtCharge.Text = VBOracleLib.Globals.Format_Number(dr("PHI_GD").ToString(), ".")
                    txtVAT.Text = VBOracleLib.Globals.Format_Number(dr("PHI_VAT").ToString(), ".")
                    txtCharge2.Text = VBOracleLib.Globals.Format_Number(dr("PHI_GD2").ToString(), ".")
                    txtVat2.Text = VBOracleLib.Globals.Format_Number(dr("PHI_VAT2").ToString(), ".")
                End If
            Else
                txtCharge.Text = VBOracleLib.Globals.Format_Number(dr("PHI_GD").ToString(), ".")
                txtVAT.Text = VBOracleLib.Globals.Format_Number(dr("PHI_VAT").ToString(), ".")
                txtCharge2.Text = VBOracleLib.Globals.Format_Number(dr("PHI_GD2").ToString(), ".")
                txtVat2.Text = VBOracleLib.Globals.Format_Number(dr("PHI_VAT2").ToString(), ".")
            End If
            '*********************************************
            txtRM_REF_NO.Text = dr("RM_REF_NO").ToString()
            txtREF_NO.Text = dr("REF_NO").ToString()
            txtSoCT_NH.Text = dr("So_CT_NH").ToString()
            txtQuan_HuyenNNTien.Text = dr("QUAN_HUYENNNTIEN").ToString()
            txtTinh_TphoNNTien.Text = dr("TINH_TPNNTIEN").ToString()
            txtKHCT.Text = dr("KyHieu_CT").ToString()
            txtSoCT.Text = dr("So_CT").ToString()
            If dr("LY_DO_HUY").ToString().Trim <> "" Then
                txtLyDoHuy.Text = dr("LY_DO_HUY").ToString().Trim
            Else
                txtLyDoHuy.Text = ""
            End If
            'hdfTrangThai_CT.Value = dr("trang_thai").ToString().Trim
            'hdfTrangThai_CThue.Value = dr("tt_cthue").ToString().Trim
            Dim strTenTinhTP As String = ""
            If dr("Quan_huyennntien").ToString().Trim <> "" Then
                txtQuanNNThue.Text = dr("Quan_huyennntien").ToString().Trim
                txtTenQuanNNThue.Text = clsCTU.Get_TenQuanHuyen(txtQuanNNThue.Text.ToString())
                strTenTinhTP = clsCTU.Get_TenTinhTP(txtQuanNNThue.Text.ToString())
                txtTinhNNThue.Text = strTenTinhTP.Split(";")(0).ToString()
                txtTenTinhNNThue.Text = strTenTinhTP.Split(";")(1).ToString()
            Else
                txtQuanNNThue.Text = dr("Quan_huyennntien").ToString().Trim
                txtTenQuanNNThue.Text = ""
                txtTinhNNThue.Text = ""
                txtTenTinhNNThue.Text = ""
            End If
            If dr("ma_tinh").ToString().Trim <> "" Then
                txtQuanNNTien.Text = dr("ma_tinh").ToString().Trim
                txtTenQuanNNTien.Text = clsCTU.Get_TenQuanHuyen(txtQuanNNTien.Text.ToString())
                strTenTinhTP = clsCTU.Get_TenTinhTP(txtQuanNNTien.Text.ToString())
                txtTinhNNTien.Text = strTenTinhTP.Split(";")(0).ToString()
                txtTenTinhNNTien.Text = strTenTinhTP.Split(";")(1).ToString()
            Else
                txtQuanNNTien.Text = ""
                txtTenQuanNNTien.Text = ""
                txtTinhNNTien.Text = ""
                txtTenTinhNNTien.Text = ""
            End If

            Dim strLoaiPhi As String = dr("ma_sanpham").ToString()
            Dim strPhiGD As String = ""
            Dim strPhiKD As String = ""
            Try
                If strLoaiPhi.LastIndexOf("|") Then
                    strPhiGD = strLoaiPhi.Split("|")(1).ToString()
                    strPhiKD = strLoaiPhi.Split("|")(0).ToString()
                    Dim strSQL As String = "SELECT NAME from tcs_dm_phi where tinh_trang='1' and CODE='" & strPhiGD & "'"
                    dt = DataAccess.ExecuteToTable(strSQL)
                    If dt.Rows.Count > 0 Then
                        txtTenLoaiPhi.Text = dt.Rows(0)(0).ToString()
                    End If
                    strSQL = "SELECT NAME from tcs_dm_phi where tinh_trang='1' and CODE='" & strPhiKD & "'"
                    dt = DataAccess.ExecuteToTable(strSQL)
                    If dt.Rows.Count > 0 Then
                        txtTenLoaiPhiKD.Text = dt.Rows(0)(0).ToString()
                    End If
                Else
                    Dim strSQL As String = "SELECT NAME from tcs_dm_phi where tinh_trang='1' and CODE='" & strLoaiPhi & "'"
                    dt = DataAccess.ExecuteToTable(strSQL)
                    If dt.Rows.Count > 0 Then
                        txtTenLoaiPhi.Text = dt.Rows(0)(0).ToString()
                    End If
                End If
            Catch ex As Exception
            End Try

            txtPhi.Text = dr("phi_gd").ToString()
            txtPhiVAT.Text = dr("phi_vat").ToString()
            txtPhiKD.Text = dr("phi_gd2").ToString()
            txtPhiKDVAT.Text = dr("phi_vat2").ToString()

            If Not drlSanPham.Items.FindByValue(strLoaiPhi) Is Nothing Then
                drlSanPham.SelectedValue = strLoaiPhi
            Else
                drlSanPham.SelectedValue = "0"
            End If

            'txtSanPham.Text = dr("ma_sanpham").ToString()
            txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number(dr("tt_tthu").ToString().Trim, ".")
            If dr("SEQ_NO").ToString() <> "" Then
                lbSoFT.Text = "Số FT:" & dr("SEQ_NO").ToString()
                If dr("so_xcard").ToString() <> "" Then
                    lbSoFT.Text &= " Số XCARD :" & dr("so_xcard").ToString()
                End If
            Else
                lbSoFT.Text = ""
            End If

            If dr("ref_core_ttsp").ToString() <> "" Then
                lbSoThamChieu.Text = "Số RefCoreTTSP:" & dr("ref_core_ttsp").ToString()

            Else
                lbSoThamChieu.Text = ""
            End If

            txtCK_SoCMND.Text = dr("SO_CMND").ToString()
            txtCK_SoDT.Text = dr("SO_FONE").ToString()
        Catch ex As Exception
            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("Lỗi trong quá trình FillDataToCTHeader")
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

            If Not ex.InnerException Is Nothing Then
                sbErrMsg = New StringBuilder()

                sbErrMsg.Append("Lỗi trong quá trình lấy được thông tin chi tiết của chứng từ ")
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.Message)
                sbErrMsg.Append(vbCrLf)
                sbErrMsg.Append(ex.InnerException.StackTrace)

                LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)
            End If

            ' Throw ex
        Finally
        End Try
    End Sub
    Private Sub FillDataToCTDetail(ByVal pv_dsCT As DataSet, ByVal v_strMa_NNT As String)
        Try
            Dim v_dt As DataTable = pv_dsCT.Tables(0)
            Dim dr As DataRow
            Dim sodathu As String
            grdChiTiet.DataSource = v_dt
            grdChiTiet.DataBind()
            For i As Integer = 0 To v_dt.Rows.Count - 1
                dr = v_dt.Rows(i)
                sodathu = clsCTU.TCS_CalSoDaThu(v_strMa_NNT, dr("MaQuy").ToString(), dr("Ma_Chuong").ToString(), dr("Ma_Khoan").ToString(), dr("Ma_TMuc").ToString(), dr("Ky_Thue").ToString())
                grdChiTiet.Items(i).Cells(7).Text = sodathu
                grdChiTiet.Items(i).Cells(7).Enabled = False
                CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text = VBOracleLib.Globals.Format_Number(CType(grdChiTiet.Items(i).Controls(6).Controls(1), TextBox).Text, ".")
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CaculateTongTien(ByVal pv_dt As DataTable)
        'Tính toán và cập nhật lại trường tổng tiền
        Dim dblTongTien As Double = 0
        Dim dblTongTrichNo As Double = 0
        For i As Integer = 0 To pv_dt.Rows.Count - 1
            dblTongTien += pv_dt.Rows(i)("TTIEN")
        Next
        'tynk-commennt phí và VAT-08-04-2013*******
        Dim dblCharge As Double = CDbl(IIf((txtCharge.Text.Replace(".", "").Length > 0), txtCharge.Text.Replace(".", ""), "0"))
        Dim dblVAT As Double = CDbl(IIf((txtVAT.Text.Replace(".", "").Length > 0), txtVAT.Text.Replace(".", ""), "0"))

        Dim dblCharge2 As Double = CDbl(IIf((txtCharge2.Text.Replace(".", "").Length > 0), txtCharge2.Text.Replace(".", ""), "0"))
        Dim dblVAT2 As Double = CDbl(IIf((txtVat2.Text.Replace(".", "").Length > 0), txtVat2.Text.Replace(".", ""), "0"))
        '******************************************
        txtTTIEN.Text = VBOracleLib.Globals.Format_Number2(dblTongTien, ",", ".")
        dblTongTrichNo = dblTongTien + dblCharge + dblVAT + dblCharge2 + dblVAT2
        txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number2(dblTongTrichNo, ",", ".")
        lblTTien_Chu.Text = ConcertCurToText.ConvertCurr.So_chu(txtTongTrichNo.Text.Trim.Replace(".", ""))
    End Sub
    Private Sub CaculateTongTien(ByVal pv_dt As DataTable, ByVal strMA_NT As String)
        'Tính toán và cập nhật lại trường tổng tiền
        Dim dblTongTien As Double = 0
        Dim dblTongTrichNo As Double = 0
        For i As Integer = 0 To pv_dt.Rows.Count - 1
            dblTongTien += pv_dt.Rows(i)("TTIEN")
        Next
        'tynk-commennt phí và VAT-08-04-2013*******

        If strMA_NT.Equals("VND") Then
            Dim dblCharge As Double = CDbl(IIf((txtCharge2.Text.Replace(".", "").Length > 0), txtCharge2.Text.Replace(".", ""), "0"))
            Dim dblVAT As Double = CDbl(IIf((txtVat2.Text.Replace(".", "").Length > 0), txtVat2.Text.Replace(".", ""), "0"))
            '******************************************
            txtTTIEN.Text = VBOracleLib.Globals.Format_Number2(dblTongTien, ",", ".")
            dblTongTrichNo = dblTongTien + dblCharge + dblVAT
            txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number2(dblTongTrichNo, ",", ".")
            lblTTien_Chu.Text = ConcertCurToText.ConvertCurr.So_chu(txtTTIEN.Text.Trim.Replace(".", ""))
        ElseIf strMA_NT.Equals("USD") Then
            Dim dblCharge As Double = CDbl(IIf((txtCharge2.Text.Length > 0), txtCharge2.Text, "0"))
            Dim dblVAT As Double = CDbl(IIf((txtVat2.Text.Length > 0), txtVat2.Text, "0"))
            '******************************************
            txtTTIEN.Text = VBOracleLib.Globals.Format_Number2(dblTongTien, ",", ".")
            dblTongTrichNo = dblTongTien + dblVAT
            txtTongTrichNo.Text = VBOracleLib.Globals.Format_Number2(dblTongTrichNo, ",", ".")
            'TCS_Dich_So(hdr.TTien, "đô la mỹ", "xen")
            lblTTien_Chu.Text = TCS_Dich_So(dblTongTien, "đô la mỹ", "xen")
        End If
        'lblTTien_Chu.Text = ConcertCurToText.ConvertCurr.So_chu(txtTTIEN.Text.Trim.Replace(".", ""))
        'lbTongTrichNoBangChu.Text = ConcertCurToText.ConvertCurr.So_chu(txtTongTrichNo.Text.Trim.Replace(".", ""))
    End Sub
    Private Function GetTenLoaiThue(ByVal pv_strMaLoaiThue As String) As String
        Select Case pv_strMaLoaiThue
            Case "01"
                Return "Thuế nội địa"
            Case "02"
                Return "Thuế thu nhập cá nhân"
            Case "03"
                Return "Thuế trước bạ"
            Case "04"
                Return "Thuế Hải Quan"
            Case "05"
                Return "Thu khác"
            Case "06"
                Return "Thu tài chính"
            Case Else '05'
                Return "Phạt vi phạm hành chính"
        End Select
    End Function

    Private Sub ShowHideCTRow(ByVal pv_blnDisplayRow As Boolean)
        rowDChiNNT.Visible = pv_blnDisplayRow
        rowNNTien.Visible = pv_blnDisplayRow
        rowDChiNNTien.Visible = False
        rowQuan_HuyenNNTien.Visible = False
        rowTinhNNTien.Visible = pv_blnDisplayRow
        rowDBHC.Visible = pv_blnDisplayRow
        rowCQThu.Visible = pv_blnDisplayRow
        rowTKNo.Visible = False
        rowTKCo.Visible = pv_blnDisplayRow
        rowHTTT.Visible = pv_blnDisplayRow
        rowTK_KH_NH.Visible = pv_blnDisplayRow
        rowSoDuNH.Visible = False
        rowNgay_KH_NH.Visible = pv_blnDisplayRow
        rowMaNT.Visible = pv_blnDisplayRow
        rowLoaiThue.Visible = pv_blnDisplayRow
        rowToKhaiSo.Visible = pv_blnDisplayRow
        rowNgayDK.Visible = pv_blnDisplayRow
        rowLHXNK.Visible = pv_blnDisplayRow
        rowSoKhung.Visible = pv_blnDisplayRow
        rowSoMay.Visible = pv_blnDisplayRow
        rowDacDiemPhuongTien.Visible = pv_blnDisplayRow
        'rowSoBK.Visible = pv_blnDisplayRow
        'rowNgayBK.Visible = pv_blnDisplayRow
        rowNganHangChuyen.Visible = pv_blnDisplayRow
        rowNganHangNhan.Visible = pv_blnDisplayRow
        'rowTKGL.Visible = pv_blnDisplayRow
        rowNNTien.Visible = pv_blnDisplayRow
        rowSoCMND.Visible = pv_blnDisplayRow
        rowDChiNNTien.Visible = pv_blnDisplayRow
        rowSoDT.Visible = pv_blnDisplayRow
        rowTinhNNThue.Visible = pv_blnDisplayRow
        rowTinhNNTien.Visible = pv_blnDisplayRow
        rowQuanNNThue.Visible = pv_blnDisplayRow
        rowQuanNNTien.Visible = pv_blnDisplayRow
        rowSO_QD.Visible = pv_blnDisplayRow
        rowCQ_QD.Visible = False
        rowNgay_QD.Visible = pv_blnDisplayRow

    End Sub

    Private Sub ShowHideControlByLoaiThue(ByVal pv_strLoaiThue As String)
        Select Case pv_strLoaiThue
            Case "01" 'NguonNNT.NNThue_CTN, NguonNNT.NNThue_VL, NguonNNT.SoThue_CTN, NguonNNT.VangLai
                'An thong tin To khai
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False

                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case "02"
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case "03" 'NguonNNT.TruocBa
                'Show thong tin Truoc ba
                rowSoKhung.Visible = True
                rowSoMay.Visible = True

                'an thong tin to khai
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
            Case "04" 'NguonNNT.HaiQuan
                'Show thong tin to khai
                rowLHXNK.Visible = True
                rowToKhaiSo.Visible = True
                rowNgayDK.Visible = True

                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case "05"
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case "06"
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
            Case Else '"05" Khong xac dinh
                rowLHXNK.Visible = False
                rowToKhaiSo.Visible = False
                rowNgayDK.Visible = False
                'An thong tin Truoc ba
                rowSoKhung.Visible = False
                rowSoMay.Visible = False
                rowSO_QD.Visible = True
                rowCQ_QD.Visible = True
                rowNgay_QD.Visible = True

        End Select
    End Sub

    Private Sub ShowHideControlByPT_TT(ByVal pv_strPT_TT As String, ByVal pv_strTT As String)
        If pv_strPT_TT = "01" Or pv_strPT_TT = "03" Then 'Chuyển khoản
            'If pv_strPT_TT = "03" Then ' Chỉ GL mới lấy ra số dư vì chuyển khoản đã trừ tiền trước rồi
            rowTK_KH_NH.Visible = True
            rowNganHangNhan.Visible = True
            rowNganHangChuyen.Visible = True
            rowNNTien.Visible = True
            rowSoCMND.Visible = False
            rowDChiNNTien.Visible = True
            rowSoDT.Visible = False
            'rowTKGL.Visible = False
            If pv_strTT.Equals("05") Then
                rowSoDuNH.Visible = True
                rowTK_KH_NH.Visible = True
                Dim v_strSoTKKH As String = txtTK_KH_NH.Text
                Dim v_strLoaiTien As String = txtMaNT.Text
                If v_strSoTKKH.Length > 0 Then
                    ' Dim v_objCoreBank As New clsCoreBankUtilABB
                    Dim v_strDescTKKH As String = String.Empty
                    Dim v_strSoDuNH As String = String.Empty
                    Dim v_strErrorCode As String = String.Empty
                    Dim v_strErrorMsg As String = String.Empty

                    If (txtTK_KH_NH.Text.Trim.Length > 0) Then
                        Dim xmlDoc As New XmlDocument

                        Dim v_strReturn As String = ""
                        v_strReturn = Regex.Replace(v_strReturn, illegalChars, "")
                        xmlDoc.LoadXml(v_strReturn)
                        Dim ds As New DataSet
                        ds.ReadXml(New XmlTextReader(New StringReader(xmlDoc.InnerXml)))
                        If ds.Tables.Count > 0 Then
                            txtTenTK_KH_NH.Text = ds.Tables(0).Rows(0)("CIFNO") & "|" & ds.Tables(0).Rows(0)("BRANCH_CODE") & "|" & ds.Tables(0).Rows(0)("ACCOUNTNAME") & "|" & ds.Tables(0).Rows(0)("CUSTOMER_TYPE") & "|" & ds.Tables(0).Rows(0)("CUSTOMER_CATEGORY")
                            txtSoDu_KH_NH.Text = VBOracleLib.Globals.Format_Number(ds.Tables(0).Rows(0)("ACY_AVL_BAL"), ".")
                        Else
                            lblStatus.Text = "Không truy vấn được thông tin tài khoản.Mã lỗi:" & v_strErrorCode & ",mô tả:" & clsCTU.MappingErrorCode(v_strErrorCode)
                        End If
                    End If
                End If
            Else
                rowSoDuNH.Visible = False
            End If
        ElseIf pv_strPT_TT = "00" Then  'Tiền mặt
            rowTK_KH_NH.Visible = False
            'rowNganHangNhan.Visible = False
            'rowNganHangChuyen.Visible = False
            rowSoDuNH.Visible = False
            'rowTKGL.Visible = False

            rowNNTien.Visible = True
            rowSoCMND.Visible = True
            rowDChiNNTien.Visible = True
            rowSoDT.Visible = True
            'ElseIf pv_strPT_TT = "01" Then  'chuyên khoan KH
            '    rowSoDuNH.Visible = False
        Else 'Báo có
            rowTK_KH_NH.Visible = False
            rowNganHangNhan.Visible = False
            rowNganHangChuyen.Visible = False
            rowSoDuNH.Visible = False
            'rowTKGL.Visible = True
            rowNNTien.Visible = False
            rowSoCMND.Visible = False
            rowDChiNNTien.Visible = False
            rowSoDT.Visible = False
        End If
    End Sub

    Protected Sub btnGuiLaiTTSP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuiLaiTTSP.Click
        Dim strSoCT As String = hdnSO_CTU.Value
        Dim songphuong As CorebankServiceESB.clsSongPhuong = New CorebankServiceESB.clsSongPhuong
        Dim result As String = ""
        btnGuiLaiTTSP.Enabled = False
        Try
            'gửi lại TTSP ở đây
            'Gửi lại song phương tại đây
            result = songphuong.SendChungTuSP(strSoCT, "MOBILE")
            lblStatus.Text = result

        Catch ex As Exception
            LogDebug.WriteLog("btnGuiLaiTTSP_Click: " & ex.Message & "-- \n --" & ex.StackTrace, EventLogEntryType.Error)
            '_logger.Error(ex.Message & "-- \n --" & ex.StackTrace)
            clsCommon.ShowMessageBox(Me, "Có lỗi trong quá trình gửi song phương!")
        End Try
    End Sub
End Class
