﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false" 
CodeFile="frmTracuuCTUMobiTCT.aspx.vb" Inherits="MobiTCT_frmTracuuCTUMobiTCT" title="TRA CỨU CHỨNG TỪ MOBI TCT" %>

<%@ Import Namespace="Business.Common" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script src="../javascript/CheckDate.js" type="text/javascript"></script>
    <script src="../javascript/DatePicker/jquery.js" type="text/javascript"></script>
     <link href="../css/dialog.css" rel="stylesheet" />
    <script type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
            }
            textbox.value = str
        }
        function jsChiTietNNT(_ID) {
       
            PageMethods.GetLogDetail(_ID, Get_LogDetail_Complete, Get_LogDetail_Error);
        }
        function Get_LogDetail_Complete(pdata)
        {
            //console.log(pdata);
            if (pdata.length > 0)
            {
               // console.log(pdata);
                var objData = JSON.parse(pdata);
                console.log(objData);
                $('#Detail_MATHAMCHIEU').html(objData.MATHAMCHIEU);
                $('#Detail_NGAYMSG').html(objData.NGAYMSG);
                $('#Detail_THONGTINGIAODICH').html(objData.THONGTINGIAODICH);
                $('#Detail_MOTA').html(objData.MOTA);
                $('#Detail_MST').html(objData.MST);
                $('#Detail_HOTENNGUOINOP').html(objData.HOTENNGUOINOP);
                $('#Detail_MA_NNTIEN').html(objData.MA_NNTIEN);
                $('#Detail_TEN_NNTIEN').html(objData.TEN_NNTIEN);
                $('#Detail_SOTAIKHOAN').html(objData.SOTAIKHOAN_THE_DESCRYPT);
                $('#Detail_MACQTHU').html(objData.MACQTHU + "-" + objData.TENCQTHU);
                $('#Detail_DIACHINGUOINOP').html(objData.DIACHINGUOINOP);
                $('#Detail_TKTHUHUONG').html(objData.TKTHUHUONG);
                $('#Detail_SHKB').html(objData.SHKB + '-' + objData.TEN_KB);
                $('#Detail_MADBHC').html(objData.MADBHC);
                $('#Detail_MATIENTE').html(objData.MATIENTE);
                $('#Detail_SOTIEN').html(objData.SOTIEN);
                $('#Detail_MA_NH_A').html(objData.MA_NH_A + "-" + objData.TEN_NH_A);
                $('#Detail_MA_NH_B').html(objData.MA_NH_B + "-" + objData.TEN_NH_B);
                
                if (objData.MALOAIHINHTHUE == '02')
                {
                    document.getElementById('TrSokhung').style.display = '';
                    document.getElementById('TrDacDiemPT').style.display = '';
                    var arrDSKHOANNOP = JSON.parse(objData.DSKHOANNOP);
                    $('#Detail_DacDiemPT').html(arrDSKHOANNOP[0].noiDung);
                }
                if (objData.MALOAIHINHTHUE == '01') {
                    document.getElementById('TrNhaDat').style.display = '';
                    var arrDSKHOANNOP = JSON.parse(objData.DSKHOANNOP);
                    $('#Detail_DacDiemPT').html(arrDSKHOANNOP[0].noiDung);
                    $('#Detail_DiachiTS').html(arrDSKHOANNOP[0].noiDung);
                }
                $('#Detail_SOKHUNG').html(objData.SOKHUNG);
                $('#Detail_SOMAY').html(objData.SOMAY);
                
                //doan nay xu ly cho dtl
                var strTBL = '<table class="detailTbl"  style="width:100%;">';
                strTBL +='<tr><th>Chương</th><th>Tiểu mục</th><th>Nội dung</th><th>Số tiền</th></tr>';
                for(var i=0; i<objData.DTL.length;i++)
                {
                    strTBL +='<tr>';
                    strTBL +='<td>'+ objData.DTL[i].MACHUONG +'</td>';
                    strTBL +='<td>'+ objData.DTL[i].MATMUC +'</td>';
                    strTBL +='<td>'+ objData.DTL[i].TENPHILEPHI +'</td>';
                    strTBL += '<td style="text-align:right">' + objData.DTL[i].SOTIEN + '</td>';
                    strTBL +='</tr>';
                }
                strTBL +=' </table>';   
                $('#Detail_DTL').html(strTBL);
                if (objData.TRANGTHAI == '05' || objData.TRANGTHAI == '06' || objData.TRANGTHAI == '09'|| objData.TRANGTHAI == '10') {
                    document.getElementById('msgBox-btn-update').style.display = '';
                } else {
                    document.getElementById('msgBox-btn-update').style.display = 'none';
                }
            }
            jsDiaglog();
        }
        function Get_LogDetail_Error()
        {
            alert("Có lỗi khi lấy dữ liệu");
        }
        function jsIn_CT() {
            
            var width = screen.availWidth - 100;
            var height = screen.availHeight - 10;
            var left = 0;
            var top = 0;
            var params = 'width=' + width + ', height=' + height;
            params += ', top=' + top + ', left=' + left;
            params += ', directories=no';
            params += ', location=no';
            params += ', menubar=yes';
            params += ', resizable=no';
            params += ', scrollbars=yes';
            params += ', status=no';
            params += ', toolbar=no';
            var strSoCT = $('#Detail_MATHAMCHIEU').html().trim();
            //window.open("../../pages/Baocao/frmInGNT.aspx?SoCT=" + strSoCT + "&SHKB=" + strSHKB + "&SoBT=" + strSo_BT + "&NgayKB=" + ngayLV + "&MaNV=" + MaNV + "&BS=" + isBS, "", params);
            //
            window.open("InCtuMobiTCT.ashx?SoCT=" + strSoCT , "", params);
        }
    </script>
     <script type="text/javascript">

         function GetListSoCT() {
            var values = $('input:checkbox:checked.JchkGrid').map(function() {
                return this.value;
            }).get();
            
            
        
            if (values == '') {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);
               
                
            }
            else {
                $('#<%=ListCT_NoiDia.ClientID %>').val(values);
               

            }
        }
    </script>

    <style type="text/css">
        
        .container{width:100%; margin-bottom:30px}
        .dataView{padding-bottom:10px}
        .JchkAll,.JchkGrid{}
       
         .detailTbl{
             border:solid 1px #000;
             border-collapse:collapse;
             font-size:10pt;
             
         }
            .detailTbl td, th {
                border-right:solid 1px #000;
                border-bottom:solid 1px #000;
                font-size:10pt;
            }
        .tdData {
            background-color:#EFEDE4;
            color:#000;
           
        }
        .detailTbl  th {
               background:#999966;
               color:#000;
               text-align:center;
            }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" Runat="Server">
    <p class="pageTitle">TRA CỨU CHỨNG TỪ MOBI TCT</p>
    <div class="container">
        <table cellpadding="2" cellspacing="1" border="0" width="100%" class="form_input">
            <tr>
                <td align="left" style="width:15%" class="form_label">Từ ngày</td>
                <td class="form_control" style="width:35%"><span style="display:block; width:40%; position:relative;vertical-align:middle;">
                    <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                    onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span></td>
                <td align="left" style="width:15%" class="form_label">Đến ngày</td>
                <td class="form_control" style="width:35%">
                <span style="display:block; width:40%; position:relative; vertical-align:middle;">
                    <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                     onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                </span></td>
            </tr>
           
            <tr>
                <td align="left" class="form_label">Mã số thuế</td>
                <td class="form_control"><asp:TextBox ID="txtMST" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                <%--<td align="left" class="form_label">Tên Người nộp thuế</td>
                <td class="form_control"><asp:TextBox ID="txtTen_NNT" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>--%>
                <td align="left" class="form_label">
                    Loại Liên kết
                </td>
                <td class="form_control">
                    <asp:DropDownList ID="cboLoaiMSG" runat="server" Width="90%" CssClass="selected inputflat">
                        <asp:ListItem Selected="True" Value="" Text="Tất cả">
                                               
                        </asp:ListItem>
                        
                        <asp:ListItem Value="01" Text="01 – thanh toán NVTC thuế đất"> </asp:ListItem>
                        <asp:ListItem Value="02" Text="02 – Thanh toán LPTB ô tô, xe máy"> </asp:ListItem>
                        <asp:ListItem Value="03" Text="03- Thuế khác"> </asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
           <tr>
                <td align="left" class="form_label">Số Tài khoản</td>
                <td class="form_control"><asp:TextBox ID="txtSoTK" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
             <td align="left" class="form_label">Trạng thái</td>
                <td class="form_control">
                    <asp:DropDownList ID="cboTrangThai" runat="server" Width="90%" CssClass="selected inputflat" >
                    <asp:ListItem Selected="True" Value="" Text="Tất cả">                     
                    </asp:ListItem>
                  
                    </asp:DropDownList>
                </td>
                
            </tr>
             <tr>
                <td align="left" class="form_label">Số tham chiếu Kore</td>
                <td class="form_control"><asp:TextBox ID="txtThamChieuKore" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
             <td align="left" class="form_label">CCY</td>
              <td class="form_control"><asp:TextBox ID="txtCCY" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
                
            </tr>
            <tr>
                <td align="left" class="form_label">Số tham chiếu</td>
                <td class="form_control"><asp:TextBox ID="txtMATHAMCHIEU" runat="server" class="inputflat" Width="89%"></asp:TextBox></td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Button ID="btnTimKiem" CssClass="buttonField" runat="server" Text="Tìm kiếm"  />
                    &nbsp;&nbsp;
                     <asp:Button ID="btnExport" CssClass="buttonField" runat="server" Text="Export" />
                    
                </td>
                
            </tr>
            <tr>
                 <td align="center" colspan="4">
                 <asp:Label ID="lblError" runat="server" ForeColor="Red" ></asp:Label>
                 </td>
            </tr>
        </table>
    </div>

    <div class="dataView">
    
    <asp:HiddenField id="ListCT_NoiDia" runat="server" />
     <asp:GridView ID="dtgrd" runat="server" AutoGenerateColumns="False"
            Width="100%" BorderColor="Black" AllowPaging="True"   PageSize="30">
            <Columns>
            
                <asp:BoundField HeaderText="STT" DataField="STT" Visible="true" ReadOnly="true" />
                <%--<asp:BoundField HeaderText="Ngày Nhận MSG" DataField="NGAYNHANMSG" Visible="true" ReadOnly="true" ItemStyle-HorizontalAlign="Center"   />--%>              
                <asp:TemplateField  HeaderText="Mã tham chiếu">
              
                <ItemTemplate >
                
                <a href="#" onclick="jsChiTietNNT('<%#Eval("MATHAMCHIEU").ToString%>')"><%#Eval("MATHAMCHIEU").ToString%></a>
                </ItemTemplate>

                <ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
                </asp:TemplateField>
                 <asp:BoundField DataField="NGAYMSG" HeaderText="Thời gian" 
                    ReadOnly="True" >
                 <ItemStyle HorizontalAlign="Center" Width="70px" />
                </asp:BoundField>
                <asp:BoundField DataField="MST" HeaderText="Mã số thuế" ReadOnly="True" >
                
                <ItemStyle Width="70px" />
                </asp:BoundField>
                
                <asp:BoundField DataField="HOTENNGUOINOP" HeaderText="Tên người nộp thuế" ReadOnly="True" />
                 <asp:BoundField DataField="so_ct_nh" HeaderText="Số tham chiếu Kore" ReadOnly="True" />
                
               
                <%--<asp:BoundField DataField="SOTIEN" HeaderText="Số tiền" ReadOnly="True" />--%>

                <asp:TemplateField  HeaderText="Số tiền">
              
                <ItemTemplate >
                
                    <asp:Label ID="SOTIEN" runat="server" Text='<%# mdlCommon.NumberToStringVN2(DataBinder.Eval(Container.DataItem, "SOTIEN"))%>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                 <FooterTemplate><b><asp:Label ID="lblTongSotien" runat="server"></asp:Label></b></FooterTemplate>
                    <FooterStyle HorizontalAlign="Right" />
                </asp:TemplateField>

                 <asp:BoundField DataField="MATIENTE" HeaderText="CCY" ReadOnly="True" />

                <asp:BoundField DataField="THONGTINGIAODICH" HeaderText="Thông tin GD" ReadOnly="True" />
                 <asp:BoundField DataField="MOTA" HeaderText="Trạng thái" ReadOnly="True" />
                <asp:BoundField DataField="GHICHU" HeaderText="Ghi chú" ReadOnly="True" />
                
            </Columns>
      
            <HeaderStyle BackColor="#999966" BorderColor="Black" BorderStyle="Solid" 
                BorderWidth="1px" Font-Bold="True" />
            <AlternatingRowStyle BackColor="#EFEDE4" />
           
        </asp:GridView>
        <asp:GridView ID="grdExport" runat="server" AutoGenerateColumns="False" ShowFooter="true"
            Width="100%" BorderColor="#000" CssClass="grid_data" Visible="false">
            <AlternatingRowStyle CssClass="grid_item_alter"></AlternatingRowStyle>
            <HeaderStyle BackColor="#E4E5D7" CssClass="grid_header"></HeaderStyle>
            <RowStyle CssClass="grid_item" />
            <Columns>
                <asp:BoundField DataField="MATHAMCHIEU" HeaderText="Mã tham chiếu"  />
                <asp:BoundField DataField="NGAYMSG" HeaderText="Ngày nhận CT"  />
                <asp:BoundField DataField="THONGTINGIAODICH" HeaderText="Thông tin GD"  />
                <asp:BoundField DataField="MOTA" HeaderText="Trạng thái"  />
                <asp:BoundField DataField="MST" HeaderText="Mã số thuế" />
                <asp:BoundField DataField="HOTENNGUOINOP" HeaderText="Người nộp"  />
                <asp:BoundField DataField="SOTAIKHOAN_THE" HeaderText="Số tài khoản/thẻ"  />
                <asp:BoundField DataField="DIACHINGUOINOP" HeaderText="Địa chỉ"  />
                <asp:BoundField DataField="MACQTHU" HeaderText="Cơ quan thu"  />
                <asp:BoundField DataField="TKTHUHUONG" HeaderText="TK ngân sách"  />
                <asp:BoundField DataField="SHKB" HeaderText="Kho bạc"  />
                <asp:BoundField DataField="MADBHC" HeaderText="Mã ĐBHC"  />
                <asp:BoundField DataField="MATIENTE" HeaderText="Nguyên tệ"  />
                <asp:BoundField DataField="SOTIEN" HeaderText="Số tiền tổng"  />
                <asp:BoundField DataField="NH_HUONG" HeaderText="Ngân hàng hưởng"  />
                <asp:BoundField DataField="SOKHUNG" HeaderText="Số khung"  />
                <asp:BoundField DataField="SOMAY" HeaderText="Số máy"  />
            </Columns>
      
            <HeaderStyle BackColor="#999966" BorderColor="Black" BorderStyle="Solid" 
                BorderWidth="1px" Font-Bold="True" />
            <AlternatingRowStyle BackColor="#EFEDE4" />
           
        </asp:GridView>
    </div>
    <div id="dialog-popup" style="display: none;">
        </div>
    <div id="wapaler" style="display: none;">
        </div>
    <div class="msgBox" id="msgBox_Detail" style=" display:none; opacity: 1; min-height: 100px;width:800px;">
            <div class="msgBoxTitle">Thông tin chi tiết chứng từ</div>
            <div>
                <div>
                    <div class="msgBoxContent" id="msgBox_SHKB_CONTENT">
                        <table   class="detailTbl"  style="width:100%;">
	                        <tr >
		                        <td class="tdDetail"  style ="width:15%"  >
			                        <b>Mã tham chiếu</b> 
		                        </td>
		                        <td class="tdData" id="Detail_MATHAMCHIEU"  style ="width:35%">
                                     
		                        </td>

		                        <td  style ="width:15%" >
			                        <b>Ngày nhận CT</b> 
		                        </td>
		                        <td class="tdData" id="Detail_NGAYMSG"  style ="width:35%">
                                    
		                        </td>
	                        </tr>
	                        <tr >
		                        <td >
			                        <b>Thông tin GD</b> 
		                        </td>
		                        <td class="tdData" id="Detail_THONGTINGIAODICH"  style ="width:35%">
                                        Lệ phí trước bạ ô tô_ma ho so 11521501010000010_ma so thue 0106194683
		                        </td>

		                        <td >
			                        <b>Trạng thái</b> 
		                        </td>
		                        <td class="tdData" id="Detail_MOTA"  >
			                        
		                        </td>
	                        </tr>
	                        <tr >
		                        <td >
			                        <b>Mã số thuế</b> 
		                        </td>
		                        <td class="tdData" id="Detail_MST"  >
                                      
		                        </td>

		                        <td  >
			                        <b>Người nộp</b> 
		                        </td>
		                        <td class="tdData" id="Detail_HOTENNGUOINOP" >
			                       
		                        </td>
	                        </tr>
                            <tr >
		                        <td >
			                        <b>Mã người nộp tiền</b> 
		                        </td>
		                        <td class="tdData" id="Detail_MA_NNTIEN"  >
                                      
		                        </td>

		                        <td  >
			                        <b>Tên người nộp tiền</b> 
		                        </td>
		                        <td class="tdData" id="Detail_TEN_NNTIEN" >
			                       
		                        </td>
	                        </tr>
	                        <tr >
		                        <td >
			                        <b>Số Tài khoản</b> 
		                        </td>
		                        <td class="tdData" id="Detail_SOTAIKHOAN" >
                                    
		                        </td>

		                        <td  >
			                        <b>Địa chỉ NNT</b> 
		                        </td>
		                        <td class="tdData" id="Detail_DIACHINGUOINOP" >
			                        
		                        </td>
	                        </tr>
	                        <tr >
		                        <td >
			                        <b>Cơ quan thu</b> 
		                        </td>
		                        <td class="tdData" id="Detail_MACQTHU"  >
                                   
		                        </td>

                                <td >
			                        <b>TK Ngân sách</b> 
		                        </td>
		                        <td  class="tdData" id="Detail_TKTHUHUONG" >
			                       
		                        </td>
	                        </tr>
	                        <tr >
		                        <td >
			                        <b>Kho bạc</b> 
		                        </td>
		                        <td class="tdData" id="Detail_SHKB" >
                                       
		                        </td>
		                        <td >
			                        <b>Mã DBHC</b> 
		                        </td>
		                        <td  class="tdData" id="Detail_MADBHC" >
				                        
		                        </td>
	                        </tr>
	                        <tr >
		                        <td >
			                        <b>Mã nguyên tệ</b> 
		                        </td>
		                        <td class="tdData" id="Detail_MATIENTE" >
                                        
		                        </td>

		                        <td  >
			                        <b>Số tiền</b> 
		                        </td>
		                        <td class="tdData" id="Detail_SOTIEN" >
			                        
		                        </td>
	                        </tr>
                            <tr >
		                        <td >
			                        <b>Ngân hàng Chuyển</b> 
		                        </td>
		                        <td class="tdData" id="Detail_MA_NH_A" >
                                        
		                        </td>

		                        <td  >
			                        <b>Ngân hàng hưởng</b> 
		                        </td>
		                        <td class="tdData" id="Detail_MA_NH_B" >
			                        
		                        </td>
	                        </tr>
	                        <tr id="TrSokhung" style="display:none">
		                        <td >
			                        <b>Số khung</b> 
		                        </td>
		                        <td class="tdData" id="Detail_SOKHUNG" >
                                       
		                        </td>

		                        <td  >
			                        <b>Số máy</b> 
		                        </td>
		                        <td class="tdData" id="Detail_SOMAY"  >
			                       
		                        </td>
	                        </tr>
	                        <tr id="TrDacDiemPT" style="display:none">
		                        <td >
			                        <b>Đặc điểm phương tiện</b> 
		                        </td>
		                        <td class="tdData" id="Detail_DacDiemPT" colspan="3">
                                        
		                        </td>
	                        </tr>
	                        <tr id="TrNhaDat" style="display:none">
		                        <td >
			                        <b>Địa chỉ TS</b> 
		                        </td>
		                        <td class="tdData" id="Detail_DiachiTS"  colspan="3">
                                       
		                        </td>
	                        </tr>
	                        
                        </table>
                        <h6>Chi tiết</h6>    
                        <div id="Detail_DTL">
                            
                               <table class="detailTbl"  style="width:100%;">
				                        <tr>
					                        <th>Mã chương</th>
					                        <th>Tiểu mục</th>
					                        <th>Nội dung</th>
					                        <th>Số tiền</th>
				                        </tr>
				                        <tr>
					                        <td></td>
					                        <td></td>
					                        <td></td>
					                        <td style="text-align:right"></td>
				                        </tr>
			                        </table>   
                            </div>
                    </div>
                     
                </div>
                   <div class="msgBoxButtons" id="msgBox_SHKB_ConfirmDivButton">
                       <input type="hidden" id="msgBox_result" value="" />
                       <input id="msgBox-btn-update" class="btn btn-tertiary" style="display:none" onclick="jsIn_CT();" type="button"  value="In CT" />
                        <input id="msgBox-btn-close" class="btn btn-primary" onclick="jsMsgBoxClose();" type="button" name="Ok" value="Đóng" />
                  </div>
                
            </div>
        </div>
    <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
            if ($("#txtTuNgay").val() == "" && $("#txtDenNgay").val() == "") {
                $('.date-pick').datePicker({
                    clickInput: true
                })
            }
        });
        function jsDiaglog() {
          //  $('#msgBox_result').val('');
         //   document.getElementById('msgBox_txtMaDanhMuc').value = "";
         //   document.getElementById('msgBox_txtTenDanhMuc').value = "";
            // console.log('Start');
            $('#wapaler').css("display", "block");
            $('#msgBox_Detail').css("display", "block");

            jsDivAutoCenter('.msgBox');

           

                jsDivAutoCenter('.msgBox');
           
            //setInterval(function () {

            //    jsDivAutoCenter('.msgBox');
            //}, 1000);
           // msgBox_jsSearch();
        }
        function jsMsgBoxClose() {

            $('#wapaler').css("display", "none");
            $('#msgBox_Detail').css("display", "none");
        }
        function jsDivAutoCenter(divName) {
            //      console.log('em chay phat' + divName);
            $(divName).css({
                position: 'fixed',
                left: ($(window).width() - $(divName).outerWidth()) / 2,
                top: ($(window).height() - $(divName).outerHeight()) / 2,
                height: 'auto'
            });
        }
        //pop-up
        $(window).resize(function () {

            jsDivAutoCenter('.msgBox');
        });
    </script>		
</asp:Content>
