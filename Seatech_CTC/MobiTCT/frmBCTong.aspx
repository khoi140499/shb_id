﻿<%@ Page Language="VB" MasterPageFile="~/shared/MasterPage05.master" AutoEventWireup="false"
    CodeFile="frmBCTong.aspx.vb" Inherits="MobiTCT_frmBC_Tong" Title="LẬP BẢNG KÊ CHỨNG TỪ TRONG NGÀY" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="../javascript/jquery/script/jquery/jquery-1.3.2.min.js"></script>
    <script src="../javascript/select2/dist/jquery-3.2.1.min.js"></script>
    <script language="javascript" type="text/javascript">
        function mask(str,textbox,loc,delim){
            var locs = loc.split(',');
            for (var i = 0; i <= locs.length; i++){
	            for (var k = 0; k <= str.length; k++){
	                if (k == locs[i]){
	                if (str.substring(k, k+1) != delim){
	                        str = str.substring(0,k) + delim + str.substring(k,str.length)
	                    }
	                }
	            }
                }
                textbox.value = str
        }
        function loadCBoCHinhanh()
        {
           
        }
    </script>
  
    <script src="../javascript/select2/dist/js/select2.min.js" type="text/javascript"></script>
    <link href="../javascript/select2/dist/css/select2.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MstPg05_MainContent" runat="Server">
    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="pageTitle">
                <asp:Label ID="Label1" runat="server">BÁO CÁO CHI TIẾT ETAX MOBILE </asp:Label>
            </td>
        </tr>
        <tr>
            <td class="errorMessage">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
                <table border='0' cellpadding='0' cellspacing='0' width='100%'>
                    <tr>
                        <td>
                            <table id="Table2" cellpadding="2" cellspacing="1" class="form_input" width="100%">
                               <tr align="left">
                                    <td width='15%' class="form_label" style="height: 23px">
                                        <asp:Label ID="Label10" runat="server" Text="Từ ngày" CssClass="label"></asp:Label>
                                    </td>
                                    <td width='35%' class="form_control" style="height: 23px">
                                       <span style="display:block; width:45%; position:relative;vertical-align:middle;">
                                            <input type="text" id='txtTuNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                                            onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                                        </span>
                                    </td>
                                    <td width='15%' class="form_label" style="height: 23px">
                                        <asp:Label ID="Label2" runat="server" Text="Đến ngày" CssClass="label"></asp:Label>
                                    </td>
                                    <td width='35%' class="form_control" style="height: 23px">
                                        <span style="display:block; width:45%; position:relative;vertical-align:middle;">
                                            <input type="text" id='txtDenNgay' runat="server" maxlength='10' class="inputflat date-pick dp-applied" style="width: 80%; display:block; float:left; margin-right:1px;"
                                            onblur="CheckDate(this);" onfocus="this.select()" onkeyup="if (event.keyCode!=8){javascript:return mask(this.value,this,'2,5','/');}"/>
                                        </span> 
                                    </td> 
                                </tr>
                                <tr align="left">
                                    <td  width='15%' class="form_label">
                                       <asp:Label ID="Label5" runat="server" Text="Đơn vị kinh doanh" CssClass="label"></asp:Label>
                                    </td>
                                    <td width='35%' class="form_control" >
                                        <asp:DropDownList ID="CboChiNhanh" runat="server" CssClass="inputflat" Width="80%"></asp:DropDownList>
                                       
                                    </td>
                                   <td width='15%' class="form_label">
                                        <asp:Label ID="Label4" runat="server" Text="Trạng thái" CssClass="label"></asp:Label>
                                    </td>
                                    <td width='35%' class="form_control"  style="width:30%">
                                        <asp:DropDownList ID="cboNguyenTe" Width="80%" runat="server" CssClass="inputflat">
                                            <%--<asp:ListItem Text="Tất cả" Value="1" Selected="True"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                       
                                    </td> 
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="middle" align="center" style="height: 40px">
                <asp:Button ID="cmdIn" runat="server" CssClass="ButtonCommand" Text="[In BC]"></asp:Button>&nbsp;&nbsp;
                <asp:Button ID="cmdExit" runat="server" CssClass="ButtonCommand" Text="[Thoát]">
                </asp:Button>&nbsp;
            </td>
        </tr>
    </table>
      <!-- required plugins -->
    <script src="../javascript/DatePicker/date.js" type="text/javascript"></script>
    <!--[if IE]><script type="text/javascript" src="../../javascript/DatePicker/jquery.bgiframe.min.js"></script><![endif]-->    
    <!-- jquery.datePicker.js -->
    <script src="../javascript/DatePicker/datePicker.js" type="text/javascript"></script>
    <!-- datePicker required styles -->
    <link href="../javascript/DatePicker/datePicker.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">
        Date.firstDayOfWeek = 1;
        Date.format = 'dd/mm/yyyy';
        var sDate = new Date();
        var eDate = new Date();
        sDate.setDate(sDate.getDate() - 3650);
        eDate.setDate(eDate.getDate() + 3650);
        $(function() {
            $('.date-pick').datePicker({
                clickInput: true,
                startDate: sDate.asString(),
                endDate: eDate.asString()
            })
        });
        $(document).ready(function () {
            
            $('#<%=CboChiNhanh.ClientID%>').select2();
            $('#<%=cboNguyenTe.ClientID%>').select2();
            
        });
    </script>	
</asp:Content>
