﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Security.Principal
Imports System.Threading
Imports System.Net
Imports System.Diagnostics
Imports System.Collections
Imports System.ComponentModel
Imports System.Web
Imports System.Web.Mail
Imports System.Web.Caching
Imports System.Web.SessionState
Imports System.IO
Imports Business
Imports System

Namespace Seatech_CTC
    ''' <summary>
    ''' Summary description for Global.
    ''' </summary>
    Public Class [Global]
        Inherits System.Web.HttpApplication
        Private DummyPageUrl As String = ""
        '"http://localhost:4439/Seatech_CTC/pages/DoiChieu/AutoServices.aspx"
        Private Const LOG_FILE As String = "c:\temp\Cachecallback1.txt"
        Private Const MSMQ_NAME As String = ".\private$\ASPNETService"
        Private Const DummyCacheItemKey As String = "EOIRUEWDLJSKDLFJDS"
        Private GIO_BAT_DAU As String = "16"
        Private PHUT_BAT_DAU As String = "30"
        Private GIO_KET_THUC As String = "17"
        Private TS_NHAN_DC As String = "5"
        Private TT_GUI As Boolean = False
        Private TT_NHAN As Boolean = False
        Private AUTO_RUN As Boolean = False

        Private GUI_XN_NOPTIEN As Boolean = False
        Private NHAN_XN_NOPTIEN As Boolean = False

        Private GUI_HUY_NOPTIEN As Boolean = False
        Private NHAN_HUY_NOPTIEN As Boolean = False

        Private GUI_XN_BAOLANH As Boolean = False
        Private NHAN_XN_BAOLANH As Boolean = False

        Private GUI_HUY_BAOLANH As Boolean = False
        Private NHAN_HUY_BAOLANH As Boolean = False
        Private ts As Thread = Nothing

        Private threadCitad As Thread = Nothing


        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        Public Sub New()
            InitializeComponent()
        End Sub

        Protected Sub Application_Start(ByVal sender As [Object], ByVal e As EventArgs)
            log4net.Config.XmlConfigurator.Configure()
            System.Net.ServicePointManager.CertificatePolicy = New CustomCertificatePolicy


            Try
                ts = New Thread(AddressOf ExecDataDongBoNgayNghi)
                ts.Start()
            Catch ex As Exception

            End Try

            Try
                threadCitad = New Thread(AddressOf ExecDataGetCITAD)
                threadCitad.Start()
            Catch ex As Exception

            End Try
            'END VINHVV - get dien CITAD

            'DummyPageUrl = FullyQualifiedApplicationPath("pages/DoiChieu/AutoServices.aspx")
            'InitialValue()
            'RegisterCacheEntry()
            'Dim ht As Hashtable
            'ht = Application("SESSION_LIST")
            'If ht Is Nothing Then
            '    ht = New Hashtable()
            '    Application.Lock()
            '    Application("SESSION_LIST") = ht
            '    Application.UnLock()
            'End If
            'MvcHandler.DisableMvcResponseHeader = True

        End Sub

        'Protected Sub Application_PreSendRequestHeaders(sender As Object, e As EventArgs)
        '    HttpContext.Current.Response.Headers.Remove("Server")
        'End Sub
        Private Shared Sub ExecDataDongBoNgayNghi()
            Try
                While (True)
                    runExecDongBoNgayNghi()
                    Thread.Sleep(60000)
                End While
            Catch ex As Exception

            End Try
        End Sub
        Private Shared Sub runExecDongBoNgayNghi()
            Try
                'Fix thời gian chạy đồng bộ từ 18h20 đến 18h25
                Dim timeNote As String = DateTime.Now.ToString("HHmmss")
                Dim clsCore As CorebankServiceESB.clsCoreBank = New CorebankServiceESB.clsCoreBank
                If Integer.Parse(timeNote) > 182000 And Integer.Parse(timeNote) < 182500 Then
                    clsCore.DongBoNgayNghi()
                End If

            Catch ex As Exception

            End Try
        End Sub


        Private Shared Sub ExecDataGetCITAD()
            Try
                Dim timeGet As Integer = 1
                Try
                    timeGet = Integer.Parse(clsCTU.TCS_GETTS("CONFIG_TIME_CITAD"))  'tinh băng phut
                Catch ex As Exception
                    timeGet = 1
                End Try

                While (True)
                    runExecDtaGetCITAD()
                    Thread.Sleep(timeGet * 60000)
                End While
            Catch ex As Exception

            End Try
        End Sub
        Private Shared Sub runExecDtaGetCITAD()
            Try
                Dim clsCore As CorebankServiceESB.clsCoreBank = New CorebankServiceESB.clsCoreBank
                clsCore.GetDienCITAD()

            Catch ex As Exception

            End Try
        End Sub


        ''' ***************BEGIN****************************************'
        ''' ---------------CREATE--------------------------
        '''  Tên:			Initials the value.
        '''  Người viết:	ANHLD
        '''  Ngày viết:		10/03/2012 00:00:00
        '''  Mục đích:	    Khoi tao cac gia tri tham so can thiet
        ''' ---------------MODIFY--------------------------
        '''  Người sửa:	
        '''  Ngày sửa: 	
        '''  Mục đích:	
        ''' ---------------PARAM--------------------------
        Private Sub InitialValue()

            Try
                Dim v_dc_ts As String = clsCTU.Get_DC_TS()

                If v_dc_ts.Trim().Length > 0 Then
                    Dim arrValue As Array = v_dc_ts.Split("|")

                    If Not arrValue Is Nothing Then

                        If arrValue.Length > 0 Then
                            Application("GIO_BAT_DAU") = arrValue(0).ToString()
                            Application("PHUT_BAT_DAU") = arrValue(8).ToString()
                            Application("GIO_KET_THUC") = arrValue(6).ToString()
                            Application("TS_NHAN_DC") = arrValue(1).ToString()

                            If arrValue(7).ToString() = "0" Then
                                AUTO_RUN = True
                                Application("AUTO_RUN") = "0"
                            Else
                                AUTO_RUN = False
                                Application("AUTO_RUN") = "1"
                            End If
                            Application("CHO_PHEP_GUI") = "0"

                        End If

                    End If

                End If

            Catch ex As ApplicationException

            End Try

        End Sub

        ''' <summary>
        ''' Gets the fully qualified application path.
        ''' </summary>
        ''' <value>The fully qualified application path.</value>
        Public ReadOnly Property FullyQualifiedApplicationPath(ByVal p_strUrl As String) As String
            Get
                'Return variable declaration
                Dim appPath As String = String.Empty

                'Getting the current context of HTTP request
                Dim context As System.Web.HttpContext = HttpContext.Current

                'Checking the current context content
                If Not context Is Nothing Then
                    'Formatting the fully qualified website url/name                    
                    Dim port As String = ""
                    If context.Request.Url.Port = 80 Then
                        port = String.Empty
                    Else
                        port = ":" & context.Request.Url.Port
                    End If
                    appPath = String.Format("{0}://{1}{2}{3}", context.Request.Url.Scheme, context.Request.Url.Host, port, context.Request.ApplicationPath)
                End If

                If Not appPath.EndsWith("/") Then
                    appPath += "/"
                End If

                Return appPath & p_strUrl
            End Get
        End Property


        ''' <summary>
        ''' Register a cache entry which expires in 1 minute and gives us a callback.
        ''' </summary>
        ''' <returns></returns>
        Private Sub RegisterCacheEntry()
            ' Prevent duplicate key addition
            If HttpContext.Current.Cache(DummyCacheItemKey) IsNot Nothing Then
                Return
            End If
            'If (IsNothing(HttpContext.Current.Cache(DummyCacheItemKey))) Then
            HttpContext.Current.Cache.Add(DummyCacheItemKey, "DC", Nothing, DateTime.MaxValue, TimeSpan.FromSeconds(5), CacheItemPriority.NotRemovable, _
             New CacheItemRemovedCallback(AddressOf CacheItemRemovedCallback))
            'End If
        End Sub

        ''' <summary>
        ''' Callback method which gets invoked whenever the cache entry expires.
        ''' We can do our "service" works here.
        ''' </summary>
        ''' <param name="key"></param>
        ''' <param name="value"></param>
        ''' <param name="reason"></param>
        Public Sub CacheItemRemovedCallback(ByVal key As String, ByVal value As Object, ByVal reason As CacheItemRemovedReason)
            'Debug.WriteLine("Cache item callback: " & DateTime.Now.ToString())

            ' Do the service works
            'DoWork()

            ' We need to register another cache item which will expire again in one
            ' minute. However, as this callback occurs without any HttpContext, we do not
            ' have access to HttpContext and thus cannot access the Cache object. The
            ' only way we can access HttpContext is when a request is being processed which
            ' means a webpage is hit. So, we need to simulate a web page hit and then 
            ' add the cache item.
            HitPage()
        End Sub

        ''' <summary>
        ''' Hits a local webpage in order to add another expiring item in cache
        ''' </summary>
        Private Sub HitPage()
            Dim client As New WebClient()
            client.DownloadData(DummyPageUrl)
        End Sub

        ''' <summary>
        ''' Asynchronously do the 'service' works
        ''' </summary>
        'Private Sub DoWork()
        '    Try

        '        If Not Application("AUTO_RUN") Is Nothing Then
        '            If Application("AUTO_RUN").ToString() = "0" Then
        '                AUTO_RUN = True
        '            ElseIf Application("AUTO_RUN").ToString() = "1" Then
        '                AUTO_RUN = False
        '            End If
        '        End If
        '        If Not Application("GIO_BAT_DAU") Is Nothing Then
        '            GIO_BAT_DAU = Application("GIO_BAT_DAU").ToString
        '        End If
        '        If Not Application("PHUT_BAT_DAU") Is Nothing Then
        '            PHUT_BAT_DAU = Application("PHUT_BAT_DAU").ToString
        '        End If
        '        If Not Application("TS_NHAN_DC") Is Nothing Then
        '            TS_NHAN_DC = Application("TS_NHAN_DC").ToString
        '        End If
        '        If Not Application("GIO_KET_THUC") Is Nothing Then
        '            GIO_KET_THUC = Application("GIO_KET_THUC").ToString
        '        End If


        '        If AUTO_RUN Then
        '            '*******************************************************************'
        '            'Debug.WriteLine("Begin DoWork...")
        '            'Debug.WriteLine("Running as: " & WindowsIdentity.GetCurrent().Name)
        '            Dim v_strErrorCode As String = ""
        '            Dim v_strErrorMsg As String = ""

        '            Dim v_GUI_XN_NOPTIEN As String = ""
        '            Dim v_GUI_HUY_NOPTIEN As String = ""
        '            Dim v_GUI_XN_BAOLANH As String = ""
        '            Dim v_GUI_HUY_BAOLANH As String = ""
        '            Dim v_NHAN_XN_NOPTIEN As String = ""
        '            Dim v_NHAN_HUY_NOPTIEN As String = ""
        '            Dim v_NHAN_XN_BAOLANH As String = ""
        '            Dim v_NHAN_HUY_BAOLANH As String = ""

        '            'Thiet lap trang thai RUNNING
        '            Application("AUTO_RUN") = "3"

        '            '*********************************GUI_DOI_CHIEU_TU_DONG****************************************'
        '            'Gui doi chieu
        '            If (TT_GUI = False) And (System.DateTime.Now.Hour * 60 + System.DateTime.Now.Minute) >= (CType(GIO_BAT_DAU, Integer) * 60 + CType(PHUT_BAT_DAU, Integer)) And System.DateTime.Now.Hour <= CType(GIO_KET_THUC, Integer) Then
        '                Dim v_bSuccess As Boolean = False
        '                If Application("CHO_PHEP_GUI").ToString = "0" Then
        '                    'Goi gui doi chieu
        '                    '***********************************************************'
        '                    'Gui doi chieu xac nhan nop tien
        '                    If GUI_XN_NOPTIEN = False Then
        '                        CustomsService.ProcessMSG.getMSG42(System.DateTime.Now, v_GUI_XN_NOPTIEN, v_strErrorMsg)
        '                        If v_GUI_XN_NOPTIEN = "0" Then
        '                            GUI_XN_NOPTIEN = True
        '                            clsCTU.INSERT_LOG_AUTO_DC("0", "42", v_GUI_XN_NOPTIEN, v_strErrorMsg)
        '                        Else
        '                            clsCTU.INSERT_LOG_AUTO_DC("1", "42", v_GUI_XN_NOPTIEN, v_strErrorMsg)
        '                        End If
        '                    End If

        '                    '***********************************************************'
        '                    'Gui doi chieu huy nop tien
        '                    If GUI_HUY_NOPTIEN = False Then
        '                        CustomsService.ProcessMSG.getMSG52(System.DateTime.Now, v_GUI_HUY_NOPTIEN, v_strErrorMsg)
        '                        If v_GUI_HUY_NOPTIEN = "0" Then
        '                            GUI_HUY_NOPTIEN = True
        '                            clsCTU.INSERT_LOG_AUTO_DC("0", "52", v_GUI_HUY_NOPTIEN, v_strErrorMsg)
        '                        Else
        '                            clsCTU.INSERT_LOG_AUTO_DC("1", "52", v_GUI_HUY_NOPTIEN, v_strErrorMsg)
        '                        End If
        '                    End If
        '                    '***********************************************************'
        '                    'Gui doi chieu xac nhan bao lanh
        '                    If GUI_XN_BAOLANH = False Then
        '                        CustomsService.ProcessMSG.getMSG82(System.DateTime.Now, v_GUI_XN_BAOLANH, v_strErrorMsg)
        '                        If v_GUI_XN_BAOLANH = "0" Then
        '                            GUI_XN_BAOLANH = True
        '                            clsCTU.INSERT_LOG_AUTO_DC("0", "82", v_GUI_XN_BAOLANH, v_strErrorMsg)
        '                        Else
        '                            clsCTU.INSERT_LOG_AUTO_DC("1", "82", v_GUI_XN_BAOLANH, v_strErrorMsg)
        '                        End If
        '                    End If
        '                    '***********************************************************'
        '                    'Gui doi chieu huy bao lanh
        '                    If GUI_HUY_BAOLANH = False Then
        '                        CustomsService.ProcessMSG.getMSG92(System.DateTime.Now, v_GUI_HUY_BAOLANH, v_strErrorMsg)
        '                        If v_GUI_HUY_BAOLANH = "0" Then
        '                            GUI_HUY_BAOLANH = True
        '                            clsCTU.INSERT_LOG_AUTO_DC("0", "92", v_GUI_HUY_NOPTIEN, v_strErrorMsg)
        '                        Else
        '                            clsCTU.INSERT_LOG_AUTO_DC("1", "92", v_GUI_HUY_NOPTIEN, v_strErrorMsg)
        '                        End If
        '                    End If
        '                    '***********************************************************'
        '                    'Chan ko cho gui lai DC
        '                    Application("CHO_PHEP_GUI") = "1"
        '                End If
        '                'Kiem tra tat ca cac buoc tren thanh cong hay khong
        '                If GUI_XN_NOPTIEN And GUI_HUY_NOPTIEN And GUI_XN_BAOLANH And GUI_HUY_BAOLANH Then
        '                    v_bSuccess = True
        '                End If
        '                If v_bSuccess Then
        '                    'Neu gui dc thanh cong
        '                    'Cap nhat vao bang TCS_THAMSO_DC
        '                    clsCTU.Update_DC_TS_GUI("0")
        '                    TT_GUI = True
        '                    'clsCTU.INSERT_LOG_AUTO_DC("0", "GUI")
        '                Else
        '                    '***********************************************************'
        '                    'Gui khong thanh cong thi update trang thai thanh failed de thuc hien lai
        '                    clsCTU.Update_DC_TS_GUI("1")
        '                    TT_GUI = False
        '                    'clsCTU.INSERT_LOG_AUTO_DC("1", "GUI")
        '                End If

        '                'DoSomeFileWritingStuff()
        '            ElseIf System.DateTime.Now.Hour > CType(GIO_KET_THUC, Integer) Then
        '                '***********************************************************'
        '                'Qua 18h thi reset va khong thuc hien lai
        '                TT_GUI = False
        '                GUI_XN_NOPTIEN = False
        '                GUI_HUY_NOPTIEN = False
        '                GUI_XN_BAOLANH = False
        '                GUI_HUY_BAOLANH = False

        '                v_GUI_XN_NOPTIEN = ""
        '                v_GUI_HUY_NOPTIEN = ""
        '                v_GUI_XN_BAOLANH = ""
        '                v_GUI_HUY_BAOLANH = ""
        '                Application("CHO_PHEP_GUI") = "0"
        '            End If

        '            '*********************NHAN_KET_QUA_DOI_CHIEU*******************************************************'
        '            'Nhan ket qua doi chieu
        '            If (TT_NHAN = False) And (System.DateTime.Now.Hour * 60 + System.DateTime.Now.Minute) >= (CType(GIO_BAT_DAU, Integer) * 60 + CType(PHUT_BAT_DAU, Integer) + CType(TS_NHAN_DC, Integer)) And System.DateTime.Now.Hour <= CType(GIO_KET_THUC, Integer) Then
        '                Dim v_bSuccess As Boolean = False

        '                'Goi nhan doi chieu
        '                '***********************************************************'
        '                'Nhan doi chieu nop tien
        '                If NHAN_XN_NOPTIEN = False And GUI_XN_NOPTIEN = True Then
        '                    CustomsService.ProcessMSG.getMSG44(System.DateTime.Now, v_NHAN_XN_NOPTIEN, v_strErrorMsg)

        '                    If Not v_NHAN_XN_NOPTIEN = "" Then
        '                        If v_NHAN_XN_NOPTIEN = "0" Then
        '                            NHAN_XN_NOPTIEN = True
        '                            clsCTU.INSERT_LOG_AUTO_DC("0", "44", v_NHAN_XN_NOPTIEN, v_strErrorMsg)
        '                        Else
        '                            clsCTU.INSERT_LOG_AUTO_DC("1", "44", v_NHAN_XN_NOPTIEN, v_strErrorMsg)
        '                        End If
        '                    End If
        '                End If
        '                '***********************************************************'
        '                'Nhan doi chieu huy nop tien
        '                If NHAN_HUY_NOPTIEN = False And GUI_HUY_NOPTIEN = True Then
        '                    CustomsService.ProcessMSG.getMSG54(System.DateTime.Now, v_NHAN_HUY_NOPTIEN, v_strErrorMsg)

        '                    If Not v_NHAN_HUY_NOPTIEN = "" Then
        '                        If v_NHAN_HUY_NOPTIEN = "0" Then
        '                            NHAN_HUY_NOPTIEN = True
        '                            clsCTU.INSERT_LOG_AUTO_DC("0", "54", v_NHAN_HUY_NOPTIEN, v_strErrorMsg)
        '                        Else
        '                            clsCTU.INSERT_LOG_AUTO_DC("1", "54", v_NHAN_HUY_NOPTIEN, v_strErrorMsg)
        '                        End If
        '                    End If
        '                End If
        '                '***********************************************************'
        '                'Nhan doi chieu bao lanh
        '                If NHAN_XN_BAOLANH = False And GUI_XN_BAOLANH = True Then
        '                    CustomsService.ProcessMSG.getMSG84(System.DateTime.Now, v_NHAN_XN_BAOLANH, v_strErrorMsg)

        '                    If Not v_NHAN_XN_BAOLANH = "" Then
        '                        If v_NHAN_XN_BAOLANH = "0" Then
        '                            NHAN_XN_BAOLANH = True
        '                            clsCTU.INSERT_LOG_AUTO_DC("0", "84", v_NHAN_XN_BAOLANH, v_strErrorMsg)
        '                        Else
        '                            clsCTU.INSERT_LOG_AUTO_DC("1", "84", v_NHAN_XN_BAOLANH, v_strErrorMsg)
        '                        End If
        '                    End If
        '                End If
        '                '***********************************************************'
        '                'Nhan doi chieu huy bao lanh
        '                If NHAN_HUY_BAOLANH = False And GUI_HUY_BAOLANH = True Then
        '                    CustomsService.ProcessMSG.getMSG94(System.DateTime.Now, v_NHAN_HUY_BAOLANH, v_strErrorMsg)

        '                    If Not v_NHAN_HUY_BAOLANH = "" Then
        '                        If v_NHAN_HUY_BAOLANH = "0" Then
        '                            NHAN_HUY_BAOLANH = True
        '                            clsCTU.INSERT_LOG_AUTO_DC("0", "94", v_NHAN_HUY_NOPTIEN, v_strErrorMsg)
        '                        Else
        '                            clsCTU.INSERT_LOG_AUTO_DC("1", "94", v_NHAN_HUY_NOPTIEN, v_strErrorMsg)
        '                        End If
        '                    End If
        '                End If
        '                '***********************************************************'
        '                'Kiem tra tat ca thao tac nhan tren co thanh cong hay khong
        '                If NHAN_XN_NOPTIEN And NHAN_HUY_NOPTIEN And NHAN_XN_BAOLANH And NHAN_HUY_BAOLANH Then
        '                    v_bSuccess = True
        '                End If

        '                If v_bSuccess Then
        '                    'Neu gui dc thanh cong
        '                    'Cap nhat vao bang TCS_THAMSO_DC
        '                    clsCTU.Update_DC_TS_NHAN("0")
        '                    TT_NHAN = True
        '                    'clsCTU.INSERT_LOG_AUTO_DC("0", "NHAN")
        '                Else
        '                    '***********************************************************'
        '                    'Neu nhan khong thanh cong, update trang thai failed de nhan lai
        '                    clsCTU.Update_DC_TS_NHAN("1")
        '                    TT_NHAN = False
        '                    'clsCTU.INSERT_LOG_AUTO_DC("0", "NHAN")
        '                End If

        '                'DoSomeFileWritingStuff()
        '            ElseIf System.DateTime.Now.Hour > CType(GIO_KET_THUC, Integer) Then
        '                '***********************************************************'
        '                'Qua 18h thi reset va khogn thuc hien lai
        '                TT_NHAN = False
        '                NHAN_XN_NOPTIEN = False
        '                NHAN_HUY_NOPTIEN = False
        '                NHAN_XN_BAOLANH = False
        '                NHAN_HUY_BAOLANH = False

        '                v_NHAN_XN_NOPTIEN = ""
        '                v_NHAN_HUY_NOPTIEN = ""
        '                v_NHAN_XN_BAOLANH = ""
        '                v_NHAN_HUY_BAOLANH = ""
        '                Application("CHO_PHEP_GUI") = "0"
        '            End If
        '        End If

        '        'Thiet lap lai trang thai TRUE OR FALSE

        '        If AUTO_RUN Then
        '            Application("AUTO_RUN") = "0"
        '        Else
        '            Application("AUTO_RUN") = "1"
        '        End If

        '        'Debug.WriteLine("End DoWork...")
        '    Catch ex As Exception
        '        Debug.WriteLine("Exception...")
        '    End Try
        'End Sub

        Protected Sub Session_Start(ByVal sender As [Object], ByVal e As EventArgs)
            'If Request.IsSecureConnection = True Then
            'Response.Cookies("ASP.NET_SessionID").Secure = True
            'End If

        End Sub

        Protected Sub Application_BeginRequest(ByVal sender As [Object], ByVal e As EventArgs)
            ' If the dummy page is hit, then it means we want to add another item
            ' in cache
            'DummyPageUrl = FullyQualifiedApplicationPath("pages/DoiChieu/AutoServices.aspx")
            'If HttpContext.Current.Request.Url.ToString() = DummyPageUrl Then
            '    ' Add the item in cache and when succesful, do the work.
            '    RegisterCacheEntry()
            'End If
            Dim app = TryCast(sender, HttpApplication)

            'If app IsNot Nothing AndAlso app.Context IsNot Nothing Then
            '    app.Context.Response.Headers.Remove("Server")
            'End If
        End Sub
       
        Protected Sub Application_EndRequest(ByVal sender As [Object], ByVal e As EventArgs)

        End Sub

        Protected Sub Application_AuthenticateRequest(ByVal sender As [Object], ByVal e As EventArgs)

        End Sub
       
        Protected Sub Application_Error(ByVal sender As [Object], ByVal e As EventArgs)
            Debug.WriteLine(Server.GetLastError())
            Dim ErrorDescription As String = Server.GetLastError.ToString
            Dim Log As New EventLog()
            Log.Source = "Loi global"
            'Log.WriteEntry(ErrorDescription, EventLogEntryType.Error)

        End Sub

        Protected Sub Session_End(ByVal sender As [Object], ByVal e As EventArgs)
            'If Not Session.Item("User") Is Nothing Then
            '    Dim user As String = Session.Item("User").ToString()
            '    HeThong.buUsers.UpdateTTLogin(user, "0")
            '    Dim ht As Hashtable
            '    ht = DirectCast(Application("SESSION_LIST"), Hashtable)
            '    If ht.Count > 0 And ht.ContainsKey(user) = True Then
            '        ht.Remove(user)
            '    End If
            '    Session.RemoveAll()
            'End If

        End Sub
        'Public Sub Application_PreRequestHandlerExecute(ByVal sender As Object, ByVal e As EventArgs)
        '    Dim hasPostParams = (If(Request.QueryString("__EVENTTARGET"), If(Request.QueryString("__VIEWSTATE"), If(Request.QueryString("__EVENTARGUMENT"), Request.QueryString("__EVENTVALIDATION"))))) IsNot Nothing

        '    If hasPostParams Then
        '        Response.Redirect("~/AppError.html")
        '    End If
        'End Sub
        Protected Sub Application_End(ByVal sender As [Object], ByVal e As EventArgs)
            Application.RemoveAll()
            Application("SESSION_LIST") = Nothing

        End Sub

#Region "Web Form Designer generated code"
        ''' <summary>
        ''' Required method for Designer support - do not modify
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
        End Sub
#End Region
    End Class
End Namespace

