﻿Imports System
Imports System.Data
Imports BIDVCRYPTOLIBLib
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports System.Diagnostics

Public Class clsCoreBankUtil

    '    Private Const gc_FORMAT_REFNO As String = "00000000"
    '    Private Const MSG_CODE_BALANCE_INQ As String = "01"
    '    Private Const MSG_CODE_PAYMENT As String = "02"
    '    Private Const MSG_CODE_CANCEL_PAYMENT As String = "03"
    '    Private Const MSG_CODE_COMPARE_RESULT As String = "04"
    '    Private Const MSG_CODE_CHK_TRAN_STATUS As String = "05"
    '    Private Const MSG_CODE_GL_PAYMENT As String = "06"
    '    Private Const MSG_CODE_PAYMENT_BY_NOTE As String = "07"

    '    Private Const MAX_EXEC_TIME As Integer = 3
    '    Private Const MSG_PAYMENT_REMARK_HDR As String = "THNS-"
    '    Private Const MSG_GL_PAYMENT_REMARK_HDR As String = "THNS-"


    '    Private mv_strBIDVUserName As String 'Ten dang nhap webservice 
    '    Private mv_strBIDVPass As String 'Mat khau dang nhap webserice 
    '    Private mv_strP12File As String 'Duong dan den file ky 
    '    Private mv_strPasswordP12 As String 'Password cua file ky 

    '    Public Sub New()
    '        InitParam()
    '    End Sub

    '    Private Sub InitParam()
    '        Try
    '            mv_strBIDVUserName = Configuration.ConfigurationManager.AppSettings("BIDV_UID").ToString 'Username truy cap WebService
    '            mv_strBIDVPass = Configuration.ConfigurationManager.AppSettings("BIDV_PASS").ToString 'Password truy cap WebService
    '            mv_strP12File = Configuration.ConfigurationManager.AppSettings("BIDV_KFILE_PATH").ToString
    '            mv_strPasswordP12 = Configuration.ConfigurationManager.AppSettings("BIDV_KFILE_PASS").ToString 'Password den file ky
    '        Catch ex As Exception
    '            LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '                  & "Error code: System error!" & vbNewLine _
    '                  & "Error message: " & ex.Message, EventLogEntryType.Error)
    '        End Try
    '    End Sub

    '    Public Function GetCustomerInfo(ByVal accountNo As String, ByRef descAccount As String, ByRef availBalance As String) As String
    '        Dim ws As New BIDVWebService.OrderServiceClient
    '        Try

    '            'Corebank: Log request message

    '            Dim strXMLString As String = String.Empty
    '            For i As Integer = 0 To MAX_EXEC_TIME
    '                strXMLString = String.Empty
    '                strXMLString = ws.submit("<Message>" + BuildMsgGetBalance(accountNo) + "</Message>")
    '                If Not strXMLString Is Nothing Then
    '                    If strXMLString.Length > 0 Then
    '                        Exit For
    '                    End If
    '                End If
    '            Next

    '            If strXMLString Is Nothing Then Return False
    '            If strXMLString.Length = 0 Then Return False

    '            'Corebank: Log response message

    '            'strXMLString = "<Message>" + strXMLString + "</Message>"
    '            Dim v_xmlDocument As New Xml.XmlDocument
    '            v_xmlDocument.LoadXml(ReplaceSpecialCharacter(strXMLString))
    '            Dim v_strRetMsgCode As String
    '            Dim v_strRetRef As String
    '            Dim v_strRetResult As String
    '            Dim v_strRetAccountName As String
    '            Dim v_strPersonalID As String
    '            Dim v_strBalance As String

    '            If Not v_xmlDocument.SelectNodes("/Message/Data/Result").Item(0).ChildNodes(0) Is Nothing Then
    '                v_strRetResult = v_xmlDocument.SelectNodes("/Message/Data/Result").Item(0).ChildNodes(0).InnerText
    '                If v_strRetResult = "0" Then 'Co tai khoan
    '                    v_strRetMsgCode = v_xmlDocument.SelectNodes("/Message/Header/MessageCode").Item(0).ChildNodes(0).InnerText
    '                    v_strRetRef = v_xmlDocument.SelectNodes("/Message/Data/Ref").Item(0).ChildNodes(0).InnerText

    '                    v_strRetAccountName = v_xmlDocument.SelectNodes("/Message/Data/AccountName").Item(0).ChildNodes(0).InnerText
    '                    v_strPersonalID = "" 'v_xmlDocument.SelectNodes("/Message/Data/PersonalID").Item(0).ChildNodes(0).InnerText
    '                    v_strBalance = v_xmlDocument.SelectNodes("/Message/Data/Balance").Item(0).ChildNodes(0).InnerText

    '                    descAccount = v_strRetAccountName '& "-" & v_strPersonalID
    '                    availBalance = CDbl(v_strBalance).ToString
    '                    Return "0"
    '                Else
    '                    LogDebug.WriteLog("Error source: Lỗi khi querry dữ liệu corebank" & vbNewLine _
    '                          & "Error code: " & v_strRetResult & vbNewLine _
    '                          & "Error message: " & getErrorMessage(), EventLogEntryType.Error)
    '                    Return v_strRetResult
    '                End If
    '            Else
    '                LogDebug.WriteLog("Error source: Lỗi khi querry dữ liệu corebank" & vbNewLine _
    '                        & "Error code: " & "-1" & vbNewLine _
    '                        & "Error message: " & getErrorMessage(), EventLogEntryType.Error)
    '                Return "-1"
    '            End If
    '            Return "0"
    '        Catch ex As Exception
    '            LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '                    & "Error code: System error!" & vbNewLine _
    '                    & "Error message: " & ex.Message, EventLogEntryType.Error)
    '            Return "-1"
    '        Finally
    '            ws.Close()
    '        End Try
    '    End Function

    '    Public Function PaymentByNote(ByVal GLBranch As String, _
    '          ByVal TellerId As String, ByVal creditAccount As String, ByVal amount As String, ByVal PayAmount As String, ByVal curr As String, _
    '          ByVal remark As String, ByRef refNo As String, ByRef seqNo As String) As Boolean
    '        Dim ws As New BIDVWebService.OrderServiceClient
    '        Try
    '            Dim strXMLString As String = String.Empty
    '            For i As Integer = 0 To MAX_EXEC_TIME
    '                strXMLString = String.Empty
    '                strXMLString = ws.submit(BuildMsgPaymentByNote(GLBranch, TellerId, creditAccount, amount, PayAmount, curr, remark))
    '                If Not strXMLString Is Nothing Then
    '                    If strXMLString.Length > 0 Then
    '                        Exit For
    '                    End If
    '                End If
    '            Next

    '            If strXMLString Is Nothing Then Return False
    '            If strXMLString.Length = 0 Then Return False

    '            strXMLString = "<Message>" + strXMLString + "</Message>"

    '            Dim v_xmlDocument As New Xml.XmlDocument
    '            v_xmlDocument.LoadXml(ReplaceSpecialCharacter(strXMLString))
    '            Dim v_strRetMsgCode As String
    '            Dim v_strRetRef As String = String.Empty  'So ref tra ve
    '            Dim v_strRetSeq As String = String.Empty  'So seq tra ve
    '            Dim v_strRetResult As String

    '            v_strRetResult = v_xmlDocument.SelectNodes("/Message/Data/Result").Item(0).ChildNodes(0).InnerText
    '            If v_strRetResult = "0" Then
    '                v_strRetMsgCode = v_xmlDocument.SelectNodes("/Message/Header/MessageCode").Item(0).ChildNodes(0).InnerText
    '                v_strRetRef = v_xmlDocument.SelectNodes("/Message/Data/Ref").Item(0).ChildNodes(0).InnerText
    '                v_strRetSeq = v_xmlDocument.SelectNodes("/Message/Data/Seq").Item(0).ChildNodes(0).InnerText
    '            End If

    '            If v_strRetResult <> "0" Then
    '                LogDebug.WriteLog("Error source: Lỗi khi querry dữ liệu corebank" & vbNewLine _
    '                      & "Error code: " & v_strRetResult & vbNewLine _
    '                      & "Error message: " & getErrorMessage(), EventLogEntryType.Error)
    '                refNo = ""
    '                seqNo = ""
    '                Return False
    '            Else
    '                refNo = v_strRetRef
    '                seqNo = v_strRetSeq
    '            End If
    '            Return True
    '        Catch ex As Exception
    '            LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '                  & "Error code: System error!" & vbNewLine _
    '                  & "Error message: " & ex.Message, EventLogEntryType.Error)
    '            Return False
    '        Finally
    '            ws.Close()
    '        End Try
    '    End Function

    '    Private Function BuildMsgPaymentByNote(ByVal GLBranch As String, ByVal TellerId As String, ByVal creditAccount As String, ByVal Amount As String, ByVal payAmount As String, _
    '                                           ByVal curr As String, ByVal remark As String) As String
    '        Try
    '            Dim v_strRefNo As String = GenREF()
    '            Dim signData As String = String.Empty
    '            signData += "<Data>"
    '            signData += "<Ref>" & v_strRefNo & "</Ref>"
    '            signData += "<GLBranch>" & GLBranch & "</GLBranch>"
    '            signData += "<CreditAccount>" & creditAccount & "</CreditAccount>"
    '            signData += "<TellerID>" & TellerId & "</TellerID>"
    '            signData += "<Amount>" & Amount & "</Amount>"
    '            signData += "<PayAmount>" & payAmount & "</PayAmount>"
    '            signData += "<Curr>" & curr & "</Curr>"
    '            signData += "<Remark>" & MSG_PAYMENT_REMARK_HDR & remark & " " & v_strRefNo & "</Remark>"
    '            signData += "</Data>"

    '            Dim strMessage As String = String.Empty
    '            strMessage += "<Header>"
    '            strMessage += "<MessageCode>" & MSG_CODE_PAYMENT_BY_NOTE & "</MessageCode>"
    '            strMessage += "<Username>" & mv_strBIDVUserName & "</Username>"
    '            strMessage += "<Password>" & mv_strBIDVPass & "</Password>"
    '            strMessage += "<Signature>$</Signature>"
    '            strMessage += "</Header>"
    '            strMessage += "<Data>"
    '            strMessage += "<Ref>" & v_strRefNo & "</Ref>"
    '            strMessage += "<GLBranch>" & GLBranch & "</GLBranch>"
    '            strMessage += "<CreditAccount>" & creditAccount & "</CreditAccount>"
    '            strMessage += "<TellerID>" & TellerId & "</TellerID>"
    '            strMessage += "<Amount>" & Amount & "</Amount>"
    '            strMessage += "<PayAmount>" & payAmount & "</PayAmount>"
    '            strMessage += "<Curr>" & curr & "</Curr>"
    '            strMessage += "<Remark>" & MSG_PAYMENT_REMARK_HDR & remark & " " & v_strRefNo & "</Remark>"
    '            strMessage += "</Data>"

    '            Dim v_data As String = GenSignedData(signData)
    '            strMessage = strMessage.Replace("$", v_data)

    '            Return strMessage
    '        Catch ex As Exception
    '            LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '                 & "Error code: System error!" & vbNewLine _
    '                 & "Error message: " & ex.Message, EventLogEntryType.Error)
    '            Return ""
    '        End Try
    '    End Function

    '    Private Function getErrorMessage() As String
    '        Return ""
    '    End Function

    '    'Private Function LogCoreBankMessage() As Boolean
    '    '    Try
    '    '        Dim v_strSQL As String = "INSERT INTO TCS_MESSAGELOG() VALUES()"
    '    '        Dim dt As DataTable = DatabaseHelp.Execute.ExecuteToTable(v_strSQL)
    '    '        Return True
    '    '    Catch ex As Exception
    '    '        Throw ex
    '    '        Return False
    '    '    End Try
    '    'End Function

    '    Private Function ReplaceSpecialCharacter(ByVal pv_strInput As String) As String
    '        If pv_strInput Is Nothing Then
    '            pv_strInput.Replace("&", "")
    '        End If
    '        Return pv_strInput
    '    End Function

    '    Public Function GLPayment(ByVal glBranch As String, ByVal GLDebitAccount As String, _
    '            ByVal CreditAccount As String, ByVal Amount As String, ByVal Curr As String, _
    '            ByVal Remark As String, ByRef RefNo As String) As Boolean
    '        Dim ws As New BIDVWebService.OrderServiceClient
    '        Try
    '            Dim strXMLString As String = String.Empty
    '            For i As Integer = 0 To MAX_EXEC_TIME
    '                strXMLString = String.Empty
    '                strXMLString = ws.submit(BuildMsgGLPayment(glBranch, GLDebitAccount, CreditAccount, Amount, Curr, Remark))
    '                If Not strXMLString Is Nothing Then
    '                    If strXMLString.Length > 0 Then
    '                        Exit For
    '                    End If
    '                End If
    '            Next


    '            If strXMLString Is Nothing Then Return False
    '            If strXMLString.Length = 0 Then Return False

    '            strXMLString = "<Message>" + strXMLString + "</Message>"

    '            Dim v_xmlDocument As New Xml.XmlDocument
    '            v_xmlDocument.LoadXml(ReplaceSpecialCharacter(strXMLString))
    '            Dim v_strRetMsgCode As String = String.Empty
    '            Dim v_strRetRef As String = String.Empty
    '            Dim v_strRetResult As String = String.Empty

    '            v_strRetResult = v_xmlDocument.SelectNodes("/Message/Data/Result").Item(0).ChildNodes(0).InnerText
    '            If v_strRetResult = "0" Then
    '                v_strRetMsgCode = v_xmlDocument.SelectNodes("/Message/Header/MessageCode").Item(0).ChildNodes(0).InnerText
    '                v_strRetRef = v_xmlDocument.SelectNodes("/Message/Data/Ref").Item(0).ChildNodes(0).InnerText
    '            End If

    '            If v_strRetResult <> "0" Then
    '                LogDebug.WriteLog("Error source: Lỗi khi querry dữ liệu corebank" & vbNewLine _
    '                      & "Error code: " & v_strRetResult & vbNewLine _
    '                      & "Error message: " & getErrorMessage(), EventLogEntryType.Error)
    '                RefNo = ""
    '                Return False
    '            Else
    '                RefNo = v_strRetRef
    '            End If
    '            Return True
    '        Catch ex As Exception
    '            LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '                  & "Error code: System error!" & vbNewLine _
    '                  & "Error message: " & ex.Message, EventLogEntryType.Error)
    '            Return False
    '        Finally
    '            ws.Close()
    '        End Try
    '    End Function

    '    Public Function Payment(ByVal creAccountNo As String, _
    '            ByVal debAccountNo As String, ByVal amount As String, ByVal curr As String, _
    '            ByVal remark As String, ByRef refNo As String) As Boolean
    '        Dim ws As New BIDVWebService.OrderServiceClient
    '        Try
    '            Dim strXMLString As String = String.Empty
    '            For i As Integer = 0 To MAX_EXEC_TIME
    '                strXMLString = String.Empty
    '                'Kienvt * 100 Don vi
    '                strXMLString = ws.submit("<Message>" + BuildMsgPayment(creAccountNo, debAccountNo, amount * 100, curr, remark) + "</Message>")
    '                If Not strXMLString Is Nothing Then
    '                    If strXMLString.Length > 0 Then
    '                        Exit For
    '                    End If
    '                End If
    '            Next

    '            If strXMLString Is Nothing Then Return False
    '            If strXMLString.Length = 0 Then Return False

    '            'strXMLString = "<Message>" + strXMLString + "</Message>"

    '            Dim v_xmlDocument As New Xml.XmlDocument
    '            v_xmlDocument.LoadXml(ReplaceSpecialCharacter(strXMLString))
    '            Dim v_strRetMsgCode As String = String.Empty
    '            Dim v_strRetRef As String = String.Empty
    '            Dim v_strRetResult As String = String.Empty

    '            v_strRetResult = v_xmlDocument.SelectNodes("/Message/Data/Result").Item(0).ChildNodes(0).InnerText
    '            If v_strRetResult = "0" Then
    '                v_strRetMsgCode = v_xmlDocument.SelectNodes("/Message/Header/MessageCode").Item(0).ChildNodes(0).InnerText
    '                v_strRetRef = v_xmlDocument.SelectNodes("/Message/Data/Ref").Item(0).ChildNodes(0).InnerText
    '            End If

    '            If v_strRetResult <> "0" Then
    '                LogDebug.WriteLog("Error source: Lỗi khi querry dữ liệu corebank" & vbNewLine _
    '                      & "Error code: " & v_strRetResult & vbNewLine _
    '                      & "Error message: " & getErrorMessage(), EventLogEntryType.Error)
    '                refNo = ""
    '                Return False
    '            Else
    '                refNo = v_strRetRef
    '            End If
    '            Return True
    '        Catch ex As Exception
    '            LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '                  & "Error code: System error!" & vbNewLine _
    '                  & "Error message: " & ex.Message, EventLogEntryType.Error)
    '            Return False
    '        Finally
    '            ws.Close()
    '        End Try
    '    End Function

    '    Public Function CompareResult() As String
    '        Dim ws As New BIDVWebService.OrderServiceClient
    '        Try
    '            Dim strXMLString As String = String.Empty
    '            For i As Integer = 0 To MAX_EXEC_TIME
    '                strXMLString = String.Empty
    '                strXMLString = ws.submit(BuildMsgCompareResult())
    '                If Not strXMLString Is Nothing Then
    '                    If strXMLString.Length > 0 Then
    '                        Exit For
    '                    End If
    '                End If
    '            Next

    '            If strXMLString Is Nothing Then Return False
    '            If strXMLString.Length = 0 Then Return False

    '            strXMLString = "<Message>" + strXMLString + "</Message>"
    '            Dim v_xmlDocument As New Xml.XmlDocument
    '            v_xmlDocument.LoadXml(ReplaceSpecialCharacter(strXMLString))
    '            Dim v_strRetMsgCode As String
    '            Dim v_strRetRef As String
    '            Dim v_strRetResult As String


    '            v_strRetResult = v_xmlDocument.SelectNodes("/Message/Data/Result").Item(0).ChildNodes(0).InnerText
    '            If v_strRetResult = "0" Then 'Thanh cong
    '                v_strRetMsgCode = v_xmlDocument.SelectNodes("/Message/Header/MessageCode").Item(0).ChildNodes(0).InnerText
    '                v_strRetRef = v_xmlDocument.SelectNodes("/Message/Data/Ref").Item(0).ChildNodes(0).InnerText
    '                Return True
    '            Else
    '                LogDebug.WriteLog("Error source: Lỗi khi querry dữ liệu corebank" & vbNewLine _
    '                      & "Error code: " & v_strRetResult & vbNewLine _
    '                      & "Error message: " & getErrorMessage(), EventLogEntryType.Error)
    '                Return False
    '            End If
    '            Return True
    '        Catch ex As Exception
    '            LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '                 & "Error code: System error!" & vbNewLine _
    '                 & "Error message: " & ex.Message, EventLogEntryType.Error)
    '            Return False
    '        Finally
    '            ws.Close()
    '        End Try
    '    End Function

    '    Public Function CancelPayment(ByVal creAccountNo As String, ByVal oriRefNo As String, _
    '            ByVal debAccountNo As String, ByVal amount As String, ByVal curr As String, _
    '            ByVal remark As String, ByRef refNo As String) As Boolean
    '        Dim ws As New BIDVWebService.OrderServiceClient
    '        Try
    '            Dim strXMLString As String = String.Empty
    '            For i As Integer = 0 To MAX_EXEC_TIME
    '                strXMLString = String.Empty
    '                strXMLString = ws.submit(BuildMsgCancelPayment(creAccountNo, oriRefNo, debAccountNo, amount, curr, remark))
    '                If Not strXMLString Is Nothing Then
    '                    If strXMLString.Length > 0 Then
    '                        Exit For
    '                    End If
    '                End If
    '            Next

    '            If strXMLString Is Nothing Then Return False
    '            If strXMLString.Length = 0 Then Return False

    '            strXMLString = "<Message>" + strXMLString + "</Message>"

    '            Dim v_xmlDocument As New Xml.XmlDocument
    '            v_xmlDocument.LoadXml(ReplaceSpecialCharacter(strXMLString))
    '            Dim v_strRetMsgCode As String = String.Empty
    '            Dim v_strRetRef As String = String.Empty
    '            Dim v_strRetResult As String = String.Empty

    '            v_strRetResult = v_xmlDocument.SelectNodes("/Message/Data/Result").Item(0).ChildNodes(0).InnerText
    '            If v_strRetResult = "0" Then
    '                v_strRetMsgCode = v_xmlDocument.SelectNodes("/Message/Header/MessageCode").Item(0).ChildNodes(0).InnerText
    '                v_strRetRef = v_xmlDocument.SelectNodes("/Message/Data/Ref").Item(0).ChildNodes(0).InnerText
    '            End If

    '            If v_strRetResult <> "0" Then
    '                LogDebug.WriteLog("Error source: Lỗi khi querry dữ liệu corebank" & vbNewLine _
    '                      & "Error code: " & v_strRetResult & vbNewLine _
    '                      & "Error message: " & getErrorMessage(), EventLogEntryType.Error)
    '                refNo = ""
    '                Return False
    '            Else
    '                refNo = v_strRetRef
    '            End If
    '            Return True
    '        Catch ex As Exception
    '            LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '                  & "Error code: System error!" & vbNewLine _
    '                  & "Error message: " & ex.Message, EventLogEntryType.Error)
    '            Return False
    '        Finally
    '            ws.Close()
    '        End Try
    '    End Function

    '#Region "Private Function"

    '    Private Function BuildMsgGetBalance(ByVal accountNo As String) As String
    '        Dim v_strRefNo As String = GenREF()
    '        Dim signData As String = String.Empty
    '        signData += "<Data>"
    '        signData += "<Ref>" & v_strRefNo & "</Ref>"
    '        signData += "<Account>" & accountNo & "</Account>"
    '        signData += "</Data>"

    '        Dim strMessage As String = String.Empty
    '        strMessage += "<Header>"
    '        strMessage += "<MessageCode>" & MSG_CODE_BALANCE_INQ & "</MessageCode>"
    '        strMessage += "<Username>" & mv_strBIDVUserName & "</Username>"
    '        strMessage += "<Password>" & mv_strBIDVPass & "</Password>"
    '        strMessage += "<Signature>$</Signature>"
    '        strMessage += "</Header>"

    '        strMessage += "<Data>"
    '        strMessage += "<Ref>" & v_strRefNo & "</Ref>"
    '        strMessage += "<Account>" & accountNo & "</Account>"
    '        strMessage += "</Data>"

    '        Dim v_data As String = GenSignedData(signData)
    '        strMessage = strMessage.Replace("$", v_data)
    '        Return strMessage
    '    End Function

    '    Private Function BuildMsgPayment(ByVal creAccountNo As String, _
    '            ByVal debAccountNo As String, ByVal amount As String, ByVal curr As String, ByVal remark As String) As String
    '        Try
    '            Dim v_strRefNo As String = GenREF()
    '            Dim signData As String = String.Empty
    '            signData += "<Data>"
    '            signData += "<Ref>" & v_strRefNo & "</Ref>"
    '            signData += "<DebitAccount>" & debAccountNo & "</DebitAccount>"
    '            signData += "<CreditAccount>" & creAccountNo & "</CreditAccount>"
    '            signData += "<Amount>" & amount & "</Amount>"
    '            signData += "<Curr>" & curr & "</Curr>"
    '            signData += "<Remark>" & MSG_PAYMENT_REMARK_HDR & remark & " " & v_strRefNo & "</Remark>"
    '            signData += "</Data>"

    '            Dim strMessage As String = String.Empty
    '            strMessage += "<Header>"
    '            strMessage += "<MessageCode>" & MSG_CODE_PAYMENT & "</MessageCode>"
    '            strMessage += "<Username>" & mv_strBIDVUserName & "</Username>"
    '            strMessage += "<Password>" & mv_strBIDVPass & "</Password>"
    '            strMessage += "<Signature>$</Signature>"
    '            strMessage += "</Header>"
    '            strMessage += "<Data>"
    '            strMessage += "<Ref>" & v_strRefNo & "</Ref>"
    '            strMessage += "<DebitAccount>" & debAccountNo & "</DebitAccount>"
    '            strMessage += "<CreditAccount>" & creAccountNo & "</CreditAccount>"
    '            strMessage += "<Amount>" & amount & "</Amount>"
    '            strMessage += "<Curr>" & curr & "</Curr>"
    '            strMessage += "<Remark>" & MSG_PAYMENT_REMARK_HDR & remark & " " & v_strRefNo & "</Remark>"
    '            strMessage += "</Data>"

    '            Dim v_data As String = GenSignedData(signData)
    '            strMessage = strMessage.Replace("$", v_data)

    '            Return strMessage
    '        Catch ex As Exception
    '            LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '                 & "Error code: System error!" & vbNewLine _
    '                 & "Error message: " & ex.Message, EventLogEntryType.Error)
    '            Return ""
    '        End Try
    '    End Function

    '    Private Function BuildMsgCancelPayment(ByVal creAccountNo As String, ByVal strOriRefNo As String, _
    '            ByVal debAccountNo As String, ByVal amount As String, ByVal curr As String, ByVal remark As String) As String
    '        Try
    '            Dim v_strRefNo As String = GenREF()
    '            Dim signData As String = String.Empty
    '            signData += "<Data>"
    '            signData += "<Ref>" & v_strRefNo & "</Ref>"
    '            signData += "<OriRef>" & strOriRefNo & "</OriRef>"
    '            signData += "<DebitAccount>" & debAccountNo & "</DebitAccount>"
    '            signData += "<CreditAccount>" & creAccountNo & "</CreditAccount>"
    '            signData += "<Amount>" & amount & "</Amount>"
    '            signData += "<Curr>" & curr & "</Curr>"
    '            signData += "<Remark>" & MSG_PAYMENT_REMARK_HDR & remark & " " & v_strRefNo & "</Remark>"
    '            signData += "</Data>"

    '            Dim strMessage As String = String.Empty
    '            strMessage += "<Header>"
    '            strMessage += "<MessageCode>" & MSG_CODE_CANCEL_PAYMENT & "</MessageCode>"
    '            strMessage += "<Username>" & mv_strBIDVUserName & "</Username>"
    '            strMessage += "<Password>" & mv_strBIDVPass & "</Password>"
    '            strMessage += "<Signature>$</Signature>"
    '            strMessage += "</Header>"
    '            strMessage += "<Data>"
    '            strMessage += "<Ref>" & v_strRefNo & "</Ref>"
    '            strMessage += "<OriRef>" & strOriRefNo & "</OriRef>"
    '            strMessage += "<DebitAccount>" & debAccountNo & "</DebitAccount>"
    '            strMessage += "<CreditAccount>" & creAccountNo & "</CreditAccount>"
    '            strMessage += "<Amount>" & amount & "</Amount>"
    '            strMessage += "<Curr>" & curr & "</Curr>"
    '            strMessage += "<Remark>" & MSG_PAYMENT_REMARK_HDR & remark & " " & v_strRefNo & "</Remark>"
    '            strMessage += "</Data>"

    '            Dim v_data As String = GenSignedData(signData)
    '            strMessage = strMessage.Replace("$", v_data)

    '            Return strMessage
    '        Catch ex As Exception
    '            LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '                 & "Error code: System error!" & vbNewLine _
    '                 & "Error message: " & ex.Message, EventLogEntryType.Error)
    '            Return ""
    '        End Try
    '    End Function

    '    Private Function BuildMsgCheckTranStatus(ByVal orirefNo As String) As String
    '        Try

    '            Dim v_strRefNo As String = GenREF()
    '            Dim signData As String = String.Empty
    '            signData += "<Data>"
    '            signData += "<Ref>" & v_strRefNo & "</Ref>"
    '            signData += "<OriRef>" & orirefNo & "</OriRef>"
    '            signData += "</Data>"

    '            Dim strMessage As String = String.Empty
    '            strMessage += "<Header>"
    '            strMessage += "<MessageCode>" & MSG_CODE_CHK_TRAN_STATUS & "</MessageCode>"
    '            strMessage += "<Username>" & mv_strBIDVUserName & "</Username>"
    '            strMessage += "<Password>" & mv_strBIDVUserName & "</Password>"
    '            strMessage += "<Signature>" & GenSignedData(signData) & "</Signature>"
    '            strMessage += "</Header>"
    '            strMessage += "<Data>"
    '            strMessage += "<Ref>" & v_strRefNo & "</Ref>"
    '            strMessage += "<OriRef>" & orirefNo & "</OriRef>"
    '            strMessage += "</Data>"
    '            Return strMessage

    '        Catch ex As Exception
    '            LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '                 & "Error code: System error!" & vbNewLine _
    '                 & "Error message: " & ex.Message, EventLogEntryType.Error)
    '            Return ""
    '        End Try
    '    End Function

    '    Private Function BuildMsgCompareResult() As String
    '        Try
    '            Dim v_strRefNo As String = GenREF()
    '            Dim signData As String = String.Empty
    '            signData += "<Data>"
    '            signData += "<Ref>" & v_strRefNo & "</Ref>"
    '            signData += "</Data>"

    '            Dim strMessage As String = String.Empty
    '            strMessage += "<Header>"
    '            strMessage += "<MessageCode>" & MSG_CODE_COMPARE_RESULT & "</MessageCode>"
    '            strMessage += "<Username>" & mv_strBIDVUserName & "</Username>"
    '            strMessage += "<Password>" & mv_strBIDVUserName & "</Password>"
    '            strMessage += "<Signature>" & GenSignedData(signData) & "</Signature>"
    '            strMessage += "</Header>"
    '            strMessage += "<Data>"
    '            strMessage += "<Ref>" & v_strRefNo & "</Ref>"
    '            strMessage += "</Data>"
    '            Return strMessage
    '        Catch ex As Exception
    '            LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '                            & "Error code: System error!" & vbNewLine _
    '                            & "Error message: " & ex.Message, EventLogEntryType.Error)
    '            Return ""
    '        End Try
    '    End Function

    '    Private Function BuildMsgGLPayment(ByVal GLBranch As String, ByVal GLDebitAccount As String, _
    '        ByVal CreditAccount As String, ByVal Amount As String, ByVal Curr As String, ByVal Remark As String) As String
    '        Try
    '            Dim v_strRefNo As String = GenREF()
    '            Dim signData As String = String.Empty
    '            signData += "<Data>"
    '            signData += "<Ref>" & v_strRefNo & "</Ref>"
    '            signData += "<GLBranch>" & GLBranch & "</GLBranch>"
    '            signData += "<GLDebitAccount>" & GLDebitAccount & "</GLDebitAccount>"
    '            signData += "<CreditAccount>" & CreditAccount & "</CreditAccount>"
    '            signData += "<Amount>" & Amount & "</Amount>"
    '            signData += "<Curr>" & Curr & "</Curr>"
    '            signData += "<Remark>" & MSG_GL_PAYMENT_REMARK_HDR & Remark & " " & v_strRefNo & "</Remark>"
    '            signData += "</Data>"

    '            Dim strMessage As String = String.Empty
    '            strMessage += "<Header>"
    '            strMessage += "<MessageCode>" & MSG_CODE_GL_PAYMENT & "</MessageCode>"
    '            strMessage += "<Username>" & mv_strBIDVUserName & "</Username>"
    '            strMessage += "<Password>" & mv_strBIDVPass & "</Password>"
    '            strMessage += "<Signature>$</Signature>"
    '            strMessage += "</Header>"
    '            strMessage += "<Data>"
    '            strMessage += "<Ref>" & v_strRefNo & "</Ref>"
    '            strMessage += "<GLBranch>" & GLBranch & "</GLBranch>"
    '            strMessage += "<GLDebitAccount>" & GLDebitAccount & "</GLDebitAccount>"
    '            strMessage += "<CreditAccount>" & CreditAccount & "</CreditAccount>"
    '            strMessage += "<Amount>" & Amount & "</Amount>"
    '            strMessage += "<Curr>" & Curr & "</Curr>"
    '            strMessage += "<Remark>" & MSG_GL_PAYMENT_REMARK_HDR & Remark & " " & v_strRefNo & "</Remark>"
    '            strMessage += "</Data>"

    '            Dim v_data As String = GenSignedData(signData)
    '            strMessage = strMessage.Replace("$", v_data)

    '            Return strMessage
    '        Catch ex As Exception
    '            LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '                 & "Error code: System error!" & vbNewLine _
    '                 & "Error message: " & ex.Message, EventLogEntryType.Error)
    '            Return ""
    '        End Try
    '    End Function

    '#End Region

    '#Region "Util Function"

    '    Private Function GenSignedData(ByVal strDataTosign As String) As String
    '        Try
    '            Dim crypt As New BIDVCRYPTOLIBLib.CryptoLibClass()
    '            Dim szSignedData As String = ""
    '            Dim result As Integer = crypt.SignDataByP12File(mv_strP12File, mv_strPasswordP12, strDataTosign, szSignedData)
    '            If result = 0 Then
    '                Return szSignedData
    '            Else
    '                Return String.Empty
    '            End If
    '        Catch ex As Exception
    '            LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '                           & "Error code: System error!" & vbNewLine _
    '                           & "Error message: " & ex.Message, EventLogEntryType.Error)
    '            Return String.Empty
    '        End Try
    '    End Function

    '    Private Function GenREF() As String
    '        Try
    '            Dim v_strTempREF As String = String.Empty
    '            Dim v_strWorkingDate As String = String.Empty
    '            Dim v_strAutoID As String = String.Empty

    '            'Dim v_strSQL As String = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV'"
    '            'select to_char(sysdate,'dd/mm/yyyy') ngay_lv from dual
    '            Dim v_strSQL As String = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') NGAY_LV FROM DUAL"

    '            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(v_strSQL)
    '            If Not Globals.IsNullOrEmpty(dt) Then
    '                v_strWorkingDate = dt.Rows(0)(0).ToString
    '            End If

    '            v_strSQL = "SELECT TCS_REF_NO_SEQ.NEXTVAL FROM DUAL"
    '            dt = DatabaseHelp.ExecuteToTable(v_strSQL)
    '            If Not Globals.IsNullOrEmpty(dt) Then
    '                v_strAutoID = dt.Rows(0)(0).ToString
    '            End If

    '            Dim v_lngTempDate As Long = ConvertDateToNumber(v_strWorkingDate)
    '            Dim v_strTempDate As String = v_lngTempDate.ToString.Substring(2, v_lngTempDate.ToString.Length - 2)
    '            v_strTempREF = v_strTempDate & Strings.Right(gc_FORMAT_REFNO & CStr(v_strAutoID), Len(gc_FORMAT_REFNO))
    '            Return v_strTempREF
    '        Catch ex As Exception
    '            LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '                          & "Error code: System error!" & vbNewLine _
    '                          & "Error message: " & ex.Message, EventLogEntryType.Error)
    '            Return String.Empty
    '        End Try
    '    End Function

    '    'Public Function GetAppletPaymentMsg(ByVal TellerID As String, ByVal SEQ As String, ByVal PayAmount As String, ByVal CashDeno As String, _
    '    '                                        ByVal Account As String, ByVal AmountCredit As String, ByVal Remark As String) As String
    '    '    Dim s As String = ""
    '    '    s += "<applet  name='tstapp' code='etax.Etax.class' archive='etax.jar,axlua2.jar,bds.jar,Help.jar,m.jar,mysql-connector-java-3.0.9-stable-bin.jar' height='30'  width='30' alt='Applet'>"
    '    '    s += "<param name='Account' VALUE='" & Account & "' />"
    '    '    s += "<param name='TellerID' value='" & TellerID & "' />"
    '    '    s += "<param name='SEQ' VALUE='" & SEQ & "'/>"
    '    '    s += "<param name='PayAmount' value='" & PayAmount & "' />"
    '    '    s += "<param name='AmountCredit' VALUE='" & AmountCredit & "' />"
    '    '    s += "<param name='Remark' VALUE='" & Remark & "' />"
    '    '    s += "<param name='CashDeno' VALUE='" & CashDeno & "' />"
    '    '    s += "</applet>"
    '    '    Return s
    '    'End Function

    '    Public Function GetAppletPaymentMsg(ByVal TellerID As String, ByVal SEQ As String, ByVal PayAmount As String, ByVal CashDeno As String, _
    '                                          ByVal Account As String, ByVal AmountCredit As String, ByVal Remark As String) As String

    '        Dim v_strServerName As String = String.Empty
    '        Dim v_strUrlBds As String = String.Empty
    '        Dim v_strUrlBdsVb As String = String.Empty
    '        Dim s As String = String.Empty
    '        GetBDSParam(TellerID, v_strServerName, v_strUrlBds, v_strUrlBdsVb) 'Get param
    '        s += "<APPLET name='tstapp' code='etax.Etax.class' archive='etax.jar,axlua2.jar,bds.jar,Help.jar,m.jar,mysql-connector-java-3.0.9-stable-bin.jar' height='0'  width='0'>"
    '        s += "<param name='Account' VALUE='" & Account & "' />"
    '        s += "<param name='TellerID' value='" & TellerID & "' />"
    '        s += "<param name='SEQ' VALUE='" & SEQ & "'/>"
    '        s += "<param name='PayAmount' value='" & PayAmount & "' />"
    '        s += "<param name='AmountCredit' VALUE='" & AmountCredit & "' />"
    '        s += "<param name='Remark' VALUE='" & Remark & "' />"
    '        s += "<param name='Host_Prop' VALUE='"
    '        s += "<database.driver>org.gjt.mm.mysql.Driver</database.driver>"
    '        s += "<database.urlbds>" + v_strUrlBds + "</database.urlbds>"
    '        s += "<database.urlbdsvb>" + v_strUrlBdsVb + "<database.urlbdsvb>"
    '        s += "<server.servername>" + v_strServerName + "<server.servername>"
    '        s += "<sna.cicsname>SFS<sna.cicsname>"
    '        s += "<sna.luname>JAVT<sna.luname>"
    '        s += "<settings.Timeout_Open>15<settings.Timeout_Open>"
    '        s += "<settings.timeout>300<settings.timeout>"
    '        s += "<svs.displayflag>0<svs.displayflag>"
    '        s += "<svs.svsindicator>0<svs.svsindicator>"
    '        s += "<timesocklocal.portno>2333<timesocklocal.portno>"
    '        s += "<timesockserver.portno>2444<timesockserver.portno>"
    '        s += " <timesockserver.hostip>10.53.20.15<timesockserver.hostip>"
    '        s += " ' />"
    '        s += "<param name='CashDeno' VALUE='" & CashDeno & "' />"
    '        s += "</applet>"
    '        Return s
    '    End Function


    '    'Public Function GetAppletAvailBDSNote(ByVal TellerID As String) As String
    '    '    Dim s As String = ""
    '    '    s += "<applet  name='tstapp' code='etax2.Etax2.class' archive='etax2.jar,mysql-connector-java-3.0.9-stable-bin.jar' height=30  width=300 alt='Applet'>"
    '    '    s += "</applet>"
    '    '    Return s
    '    'End Function


    '    Public Function GetAppletAvailBDSNote(ByVal TellerID As String) As String
    '        Dim v_strServerName As String = String.Empty
    '        Dim v_strUrlBds As String = String.Empty
    '        Dim v_strUrlBdsVb As String = String.Empty
    '        Dim s As String = String.Empty
    '        GetBDSParam(TellerID, v_strServerName, v_strUrlBds, v_strUrlBdsVb) 'Get param
    '        s += "<applet  name='tstapp' code='etax2.Etax2.class' archive='etax2.jar,mysql-connector-java-3.0.9-stable-bin.jar' height='1'  width='1'>"
    '        s += "<param name='Host_Prop' VALUE='"
    '        s += "<database.driver>org.gjt.mm.mysql.Driver</database.driver>"
    '        s += "<database.urlbds>" + v_strUrlBds + "</database.urlbds>"
    '        s += "<database.urlbdsvb>" + v_strUrlBdsVb + "<database.urlbdsvb>"
    '        s += "<server.servername>" + v_strServerName + "<server.servername>"
    '        s += "<sna.cicsname>SFS<sna.cicsname>"
    '        s += "<sna.luname>JAVT<sna.luname>"
    '        s += "<settings.Timeout_Open>15<settings.Timeout_Open>"
    '        s += "<settings.timeout>300<settings.timeout>"
    '        s += "<svs.displayflag>0<svs.displayflag>"
    '        s += "<svs.svsindicator>0<svs.svsindicator>"
    '        s += "<timesocklocal.portno>2333<timesocklocal.portno>"
    '        s += "<timesockserver.portno>2444<timesockserver.portno>"
    '        s += " <timesockserver.hostip>10.53.20.15<timesockserver.hostip>"
    '        s += " ' />"
    '        s += "</applet>"
    '        Return s
    '    End Function

    '    'Kienvt : Message de test moi truong bds chi khac tstapp2
    '    Public Function GetAppletAvailBDSNoteTest(ByVal TellerID As String) As String
    '        Dim v_strServerName As String = String.Empty
    '        Dim v_strUrlBds As String = String.Empty
    '        Dim v_strUrlBdsVb As String = String.Empty
    '        Dim s As String = String.Empty
    '        GetBDSParam(TellerID, v_strServerName, v_strUrlBds, v_strUrlBdsVb) 'Get param
    '        s += "<applet  name='tstapp2' code='etax2.Etax2.class' archive='etax2.jar,mysql-connector-java-3.0.9-stable-bin.jar' height='1'  width='1'>"
    '        s += "<param name='Host_Prop' VALUE='"
    '        s += "<database.driver>org.gjt.mm.mysql.Driver</database.driver>"
    '        s += "<database.urlbds>" + v_strUrlBds + "</database.urlbds>"
    '        s += "<database.urlbdsvb>" + v_strUrlBdsVb + "<database.urlbdsvb>"
    '        s += "<server.servername>" + v_strServerName + "<server.servername>"
    '        s += "<sna.cicsname>SFS<sna.cicsname>"
    '        s += "<sna.luname>JAVT<sna.luname>"
    '        s += "<settings.Timeout_Open>15<settings.Timeout_Open>"
    '        s += "<settings.timeout>300<settings.timeout>"
    '        s += "<svs.displayflag>0<svs.displayflag>"
    '        s += "<svs.svsindicator>0<svs.svsindicator>"
    '        s += "<timesocklocal.portno>2333<timesocklocal.portno>"
    '        s += "<timesockserver.portno>2444<timesockserver.portno>"
    '        s += " <timesockserver.hostip>10.53.20.15<timesockserver.hostip>"
    '        s += " ' />"
    '        s += "</applet>"
    '        Return s
    '    End Function

    '    Private Sub GetBDSParam(ByVal TellerID As String, ByRef v_strServerName As String, ByRef v_strUrlBds As String, ByRef v_strUrlBdsVb As String)
    '        Dim v_strSQL As String = "SELECT * FROM TCS_BANK_THAMSO WHERE MA_CN=(SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE TELLER_ID='" & TellerID & "')"
    '        Dim dt As DataTable = DatabaseHelp.ExecuteToTable(v_strSQL)
    '        If Not Globals.IsNullOrEmpty(dt) Then
    '            v_strServerName = dt.Rows(0)("ServerName").ToString '"10.53.18.146"
    '            v_strUrlBds = dt.Rows(0)("UrlBds").ToString '"jdbc:mysql://10.53.18.146/PBDS120"
    '            v_strUrlBdsVb = dt.Rows(0)("UrlBdsVb").ToString '"jdbc:mysql://10.53.18.146/bdsvb"
    '        End If
    '    End Sub

    '#End Region

End Class
