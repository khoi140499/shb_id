﻿Imports Business_HQ.Common.mdlCommon
Imports Business_HQ.Common.mdlSystemVariables
Imports Business_HQ.NewChungTu
Imports Business_HQ.ChungTu
Imports Business_HQ
Imports VBOracleLib
Imports System.Data
Imports System.Diagnostics
Imports GIPBankService
Imports ProcMegService
Imports Business_HQ.Common
Imports System
Imports System.Web
Imports System.Configuration
Imports System.Text
Imports Microsoft.VisualBasic

Public Class clsCTU
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private Shared mNguonNNT As NguonNNT = NguonNNT.Khac

    Private Const gc_XML_HEADER_TAG As String = "<?xml version='1.0' encoding='UTF-8'?>"
    Private Const gc_XML_START_CTU_HDR_TAG As String = "<CTU_HDR>"
    Private Const gc_XML_END_CTU_HDR_TAG As String = "</CTU_HDR>"

    Private Const gc_XML_START_CTU_DTL_TAG As String = "<CTU_DTL>"
    Private Const gc_XML_END_CTU_DTL_TAG As String = "</CTU_DTL>"

    Private Const gc_XML_START_NGAYLVV_TAG As String = "<NGAYLV>"
    Private Const gc_XML_END_NGAYLV_TAG As String = "</NGAYLV>"

    Private Const gc_XML_START_SOCT_TAG As String = "<SOCT>"
    Private Const gc_XML_END_SOCT_TAG As String = "</SOCT>"

    Private Const gc_XML_START_SHKB_TAG As String = "<SHKB>"
    Private Const gc_XML_END_SHKB_TAG As String = "</SHKB>"

    Private Const gc_XML_START_TENKB_TAG As String = "<TENKB>"
    Private Const gc_XML_END_TENKB_TAG As String = "</TENKB>"

    Private Const gc_XML_START_CTU_ROOT_TAG As String = "<CTC_CTU>"
    Private Const gc_XML_END_CTU_ROOT_TAG As String = "</CTC_CTU>"

    Public Shared gc_MaQuyMacDinh As String = "01"
    Public Shared mblnNhapThucThu As Boolean = True

    'Public Enum ColHDR As Byte
    '    NgayLV = 0
    '    SoCT = 1
    '    So_BT = 2
    '    SHKB = 3
    '    TenKB = 4
    '    MaNNT = 5
    '    TenNNT = 6
    '    DChiNNT = 7
    '    MaNNTien = 8
    '    TenNNTien = 9
    '    DChiNNTien = 10
    '    MaCQThu = 11
    '    TenCQThu = 12
    '    MaDBHC = 13
    '    TenDBHC = 14
    '    TKNo = 15
    '    TenTKNo = 16
    '    TKCo = 17
    '    TenTKCo = 18
    '    HTTT = 19
    '    SoCTNH = 20
    '    TK_KH_NH = 21
    '    TenTK_KH_NH = 22
    '    SoDu_KH_NH = 23
    '    NGAY_KH_NH = 24

    '    MA_NH_A = 21
    '    Ten_NH_A = 22
    '    MA_NH_B = 23
    '    Ten_NH_B = 24

    '    LoaiThue = 25
    '    DescLoaiThue = 26
    '    MaNT = 27
    '    TenNT = 28
    '    ToKhaiSo = 29
    '    NgayDK = 30
    '    LHXNK = 31
    '    DescLHXNK = 32
    '    SoKhung = 33
    '    SoMay = 34
    '    So_BK = 35
    '    Ngay_BK = 36
    '    TRANG_THAI = 37
    '    TT_BDS = 38
    '    TT_NSTT = 39 'Trang thai nhap so thuc thu
    '    TTIEN = 40
    '    TT_TTHU = 41
    '    MA_NV = 42
    '    DSToKhai = 43
    'End Enum

    Public Enum ColHDR As Byte
        NgayLV = 0
        SoCT = 1
        So_BT = 2
        SHKB = 3
        TenKB = 4
        MaNNT = 5
        TenNNT = 6
        DChiNNT = 7
        MaNNTien = 8
        TenNNTien = 9
        DChiNNTien = 10
        MaCQThu = 11
        TenCQThu = 12
        MaDBHC = 13

        TenDBHC = 14
        TKNo = 15
        TenTKNo = 16
        TKCo = 17
        TenTKCo = 18
        HTTT = 19
        SoCTNH = 20
        TK_KH_NH = 21
        TenTK_KH_NH = 22
        SoDu_KH_NH = 23
        NGAY_KH_NH = 24

        MA_NH_A = 25
        Ten_NH_A = 26
        MA_NH_B = 27
        Ten_NH_B = 28

        LoaiThue = 29
        DescLoaiThue = 30
        MaNT = 31
        TenNT = 32
        ToKhaiSo = 33
        NgayDK = 34
        LHXNK = 35
        DescLHXNK = 36
        SoKhung = 37
        SoMay = 38
        So_BK = 39
        Ngay_BK = 40
        TRANG_THAI = 41
        TT_BDS = 42
        TT_NSTT = 43
        PHI_GD = 44
        PHI_VAT = 45
        PT_TINHPHI = 46
        Huyen_NNTIEN = 47
        Tinh_NNTIEN = 48
        TTIEN = 49
        TT_TTHU = 50
        MA_NV = 51
        DSToKhai = 52
        MA_HQ = 53
        LOAI_TT = 54
        TEN_HQ = 55
        MA_HQ_PH = 56
        TEN_HQ_PH = 57
        MA_NH_TT = 58
        TEN_NH_TT = 59

        MA_KS = 60
        KHCT = 61
        TT_CTHUE = 62
        MA_SANPHAM = 63
        TTIEN_TN = 64
        TT_CITAD = 65
        MA_NTK = 66
        DIEN_GIAI = 67

        'sonmt
        MA_HUYEN = 68
        TEN_HUYEN = 69
        MA_TINH = 70
        TEN_TINH = 71
        HUYEN_NNTHAY = 72
        TINH_NNTHAY = 73
        SO_QD = 74
        CQ_QD = 75
        NGAY_QD = 76
        GHI_CHU = 77

        'sonmt
        LOAI_NNT = 78
        TEN_LOAI_NNT = 79

        SO_CMND = 80
        SO_FONE = 81
    End Enum
    '//' ****************************************************************************
    '//' Người thêm: Anhld
    '//' Ngày 22.09.2012
    '//' Mục đích: Them Enum cho phan bao lanh va tien thue hai quan
    '//'***************************************************************************** */
    Public Enum ColHDR_HQ As Byte
        NgayLV = 0
        SoCT = 1
        So_BT = 2
        SHKB = 3
        TenKB = 4
        MaNNT = 5
        TenNNT = 6
        DChiNNT = 7
        MaNNTien = 8
        TenNNTien = 9
        DChiNNTien = 10
        MaCQThu = 11
        TenCQThu = 12
        MaDBHC = 13
        TenDBHC = 14
        TKNo = 15
        TenTKNo = 16
        TKCo = 17
        TenTKCo = 18
        HTTT = 19
        SoCTNH = 20
        TK_KH_NH = 21
        TenTK_KH_NH = 22
        SoDu_KH_NH = 23
        NGAY_KH_NH = 24

        MA_NH_A = 25
        Ten_NH_A = 26
        MA_NH_B = 27
        Ten_NH_B = 28

        LoaiThue = 29
        DescLoaiThue = 30
        MaNT = 31
        TenNT = 32
        ToKhaiSo = 33
        NgayDK = 34
        LHXNK = 35
        DescLHXNK = 36
        SoKhung = 37
        SoMay = 38
        So_BK = 39
        Ngay_BK = 40
        TRANG_THAI = 41
        TT_BDS = 42
        TT_NSTT = 43
        PHI_GD = 44
        PHI_VAT = 45
        PT_TINHPHI = 46
        Huyen_NNTIEN = 47
        Tinh_NNTIEN = 48
        TTIEN = 49
        TT_TTHU = 50
        MA_NV = 51
        DSToKhai = 52
        MA_HQ = 53
        LOAI_TT = 54
        TEN_HQ = 55
        MA_HQ_PH = 56
        TEN_HQ_PH = 57

        MA_NH_TT = 58
        TEN_NH_TT = 59
        MA_KS = 60
        TT_CTHUE = 62
        MA_SANPHAM = 63
        TT_CITAD = 64
        LY_DO_HUY = 65
        PHI_GD2 = 66
        PHI_VAT2 = 67
        SO_CMND = 68
        SO_FONE = 69
        MA_NTK = 70
        DIEN_GIAI = 71
        SO_QD = 72
        CQ_QD = 73
        NGAY_QD = 74
        GHI_CHU = 75
    End Enum

    Public Enum ColDTL As Byte
        ChkRemove = 0
        MaQuy = 1
        CChuong = 2
        LKhoan = 3
        MTieuMuc = 4
        NoiDung = 5
        Tien = 6
        SoDT = 7
        KyThue = 8
    End Enum

#Region "Public Function"

    ''' <summary>
    ''' Lay thong tin chi tiet cua chung tu dang XML
    ''' </summary>
    ''' <param name="pv_strSHKB"></param>
    ''' <param name="pv_strSoCT"></param>
    ''' <param name="pv_strSoBT"></param>
    ''' <param name="pv_strMaNV"></param>
    ''' <param name="strNgayKB"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function TCS_GetCTU(ByVal pv_strSHKB As String, ByVal pv_strSoCT As String, _
                                      ByVal pv_strSoBT As String, ByVal pv_strMaNV As Integer, _
                                      Optional ByVal strNgayKB As String = "") As String
        Dim key As New KeyCTu
        key.Ma_Dthu = TCS_GetMaDT(pv_strMaNV)
        key.Kyhieu_CT = TCS_GetKHCT(pv_strSHKB)
        key.Ma_NV = pv_strMaNV
        If strNgayKB.Length <> 0 Then
            key.Ngay_KB = strNgayKB
        Else
            key.Ngay_KB = TCS_GetNgayLV(pv_strMaNV)
        End If
        key.SHKB = pv_strSHKB
        key.So_BT = pv_strSoBT
        key.So_CT = pv_strSoCT
        Return LoadCTChiTiet(key)
    End Function
    Public Shared Function BuilContent_citad(ByVal strMST As String, ByVal strMaCQThu As String, ByVal tenCQThu As String, ByVal maLoaiThue As String, ByVal ngayNopThue As String, ByRef _
                                    soKhung As String, ByVal soMay As String, ByVal soToKhai As String, ByVal ngayTK As String, ByVal lHXNK As String, _
                                    ByVal cqphat As String, ByVal tenKH As String, ByVal str_dtlXML As String) As String
        Dim strReturn As String = ""
        ' strReturn & =" <?xml version=\"1.0\"?>"
        strReturn &= "<VST>"
        strReturn &= "<MST>" & strMST & "</MST>"
        strReturn &= "<CQT>" & strMaCQThu & "</CQT>"
        strReturn &= "<TCQ>" & tenCQThu & "</TCQ>"
        strReturn &= "<LTH>" & maLoaiThue & "</LTH>"
        strReturn &= "<NNT>" & ngayNopThue & "</NNT>"
        strReturn &= "<SKH>" & soKhung & "</SKH>"
        strReturn &= "<SMA>" & soMay & "</SMA>"
        strReturn &= "<STK>" & soToKhai & "</STK>"
        strReturn &= "<NTK>" & ngayTK & "</NTK>"
        strReturn &= "<XNK>" & lHXNK & "</XNK>"
        strReturn &= "<CQP>" & cqphat & "</CQP>"
        strReturn &= "<TKH>" & tenKH & "</TKH>"
        strReturn &= str_dtlXML
        strReturn &= "</VST>"
        Return strReturn
    End Function
    Public Shared Sub TCS_GetBaoLanh(ByVal pv_strSHKB As String, ByVal pv_strSoCT As String, _
                                     ByVal pv_strSoBT As String, ByVal pv_strMaNV As Integer, _
                                     ByRef hdr As DataTable, ByRef dtls As DataTable, _
                                     Optional ByVal strNgayKB As String = "")
        Dim key As New KeyCTu
        Try
            key.Ma_Dthu = TCS_GetMaDT(pv_strMaNV)
            key.Kyhieu_CT = TCS_GetKHCT(pv_strSHKB)
            key.Ma_NV = pv_strMaNV
            If strNgayKB.Length <> 0 Then
                key.Ngay_KB = strNgayKB
            Else
                key.Ngay_KB = TCS_GetNgayLV(pv_strMaNV)
            End If
            key.SHKB = pv_strSHKB
            key.So_BT = pv_strSoBT
            key.So_CT = pv_strSoCT
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi load danh mục người nộp thuế", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source:" & ex.StackTrace & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
        LoadBaoLanhChiTiet(key, hdr, dtls)
    End Sub
    Public Shared Sub TCS_GetBaoLanhDetail(ByVal pv_strSHKB As String, ByVal pv_strSoCT As String, _
                                     ByVal pv_strSoBT As String, ByVal pv_strMaNV As Integer, _
                                     ByRef hdr As DataTable, ByRef dtls As DataTable, _
                                     Optional ByVal strNgayKB As String = "")
        Dim key As New KeyCTu
        Try
            key.Ma_Dthu = "" 'TCS_GetMaDT(pv_strMaNV)
            key.Kyhieu_CT = TCS_GetKHCT(pv_strSHKB)
            key.Ma_NV = 0 'pv_strMaNV
            If strNgayKB.Length <> 0 Then
                key.Ngay_KB = strNgayKB
            Else
                key.Ngay_KB = TCS_GetNgayLV(pv_strMaNV)
            End If
            key.SHKB = "" 'pv_strSHKB
            key.So_BT = pv_strSoBT
            key.So_CT = pv_strSoCT
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin bảo lãnh", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source:" & ex.StackTrace & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
        LoadBaoLanhChiTiet(key, hdr, dtls)
    End Sub

    Public Shared Sub SetObjectToDoiChieuNHHQ(ByVal pv_strSHKB As String, ByVal pv_strSoCT As String, _
                                        ByVal pv_strSoBT As String, ByVal pv_strMaNV As String, _
                                        ByVal pv_strType As String, _
                                        ByRef hdr As Business.infDoiChieuNHHQ_HDR, ByRef dtls() As Business.infDoiChieuNHHQ_DTL, _
                                        Optional ByVal pv_strMaDT As String = "", _
                                        Optional ByVal pv_strErroCodeHQ As String = "", _
                                        Optional ByVal pv_strTransactionType As String = "", _
                                        Optional ByVal pv_strNgayKB As String = "", Optional ByVal pv_strSo_TN_CT As String = "", _
                                         Optional ByVal pv_strNgay_TN_CT As String = "", Optional ByVal p_flagBL As Boolean = False)

        Dim strSender_Code As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString()
        Dim strSender_Name As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString()

        Dim hdrBL As New DataTable
        Dim dtlsBL As New DataTable
        Dim objDTL As Business.infDoiChieuNHHQ_DTL
        Dim k As Integer = 0
        Dim key As New KeyCTu
        Try
            'Tao key chung tu
            If pv_strMaDT <> "" Then
                key.Ma_Dthu = pv_strMaDT
            Else
                key.Ma_Dthu = TCS_GetMaDT(pv_strMaNV)
            End If
            key.Kyhieu_CT = TCS_GetKHCT(pv_strSHKB)
            key.Ma_NV = pv_strMaNV
            If pv_strNgayKB.Length <> 0 Then
                key.Ngay_KB = pv_strNgayKB
            Else
                key.Ngay_KB = TCS_GetNgayLV(pv_strMaNV)
            End If
            key.SHKB = pv_strSHKB
            key.So_BT = pv_strSoBT
            key.So_CT = pv_strSoCT

            'Lay thong tin ChungTu
            If pv_strType = "CTU" Then
                hdrBL = ChungTu.buChungTu.SelectCT_HDR(key.SHKB, key.Ngay_KB, key.Ma_NV, key.So_BT, key.Ma_Dthu).Tables(0)
                dtlsBL = ChungTu.buChungTu.SelectCT_DTL(key.SHKB, key.Ngay_KB, key.Ma_NV, key.So_BT, key.Ma_Dthu).Tables(0)
            Else
                'Lay thong tin BaoLanh
                LoadBaoLanhChiTiet(key, hdrBL, dtlsBL)
            End If

            If hdrBL.Rows.Count > 0 Then
                hdr.shkb = hdrBL.Rows(0)("shkb").ToString()
                hdr.ngay_kb = hdrBL.Rows(0)("ngay_kb").ToString()
                hdr.so_bt = hdrBL.Rows(0)("so_bt").ToString()
                hdr.kyhieu_ct = hdrBL.Rows(0)("kyhieu_ct").ToString()
                hdr.so_ct = hdrBL.Rows(0)("so_ct").ToString()
                hdr.ngay_ct = hdrBL.Rows(0)("ngay_ct").ToString()
                hdr.ma_nnthue = hdrBL.Rows(0)("ma_nnthue").ToString()
                hdr.ten_nnthue = hdrBL.Rows(0)("ten_nnthue").ToString()
                hdr.ma_cqthu = hdrBL.Rows(0)("ma_cqthu").ToString()
                hdr.ma_hq_cqt = hdrBL.Rows(0)("ma_cqthu").ToString()
                hdr.so_tk = hdrBL.Rows(0)("so_tk").ToString()
                hdr.ttien = hdrBL.Rows(0)("ttien").ToString()
                hdr.ttien_nt = hdrBL.Rows(0)("ttien_nt").ToString()
                hdr.trang_thai = hdrBL.Rows(0)("trang_thai").ToString()
                hdr.ly_do_huy = hdrBL.Rows(0)("ly_do_huy").ToString()
                hdr.ma_nt = hdrBL.Rows(0)("ma_nt").ToString()
                hdr.ten_nh_ph = strSender_Name
                ' hdr.ma_ntk = hdrBL.Rows(0)("ma_ntk").ToString()
                ' hdr.ty_gia = hdrBL.Rows(0)("ty_gia").ToString()
                If p_flagBL = False Then
                    ' hdr.tg_id = hdrBL.Rows(0)("tg_id").ToString()
                End If


                If hdrBL.Rows(0)("ngay_tk").ToString() <> "" Then
                    hdr.ngay_tk = "to_date('" & hdrBL.Rows(0)("ngay_tk").ToString() & "','dd/mm/yyyy')"
                Else
                    hdr.ngay_tk = "''"
                End If
                '21/09/2012-manhnv
                If hdrBL.Columns.Contains("lh_xnk") Then
                    hdr.lhxnk = hdrBL.Rows(0)("lh_xnk").ToString()
                End If
                '***
                '27/09/2012-manhnv
                If hdrBL.Columns.Contains("ma_nv") Then
                    hdr.ma_nv = hdrBL.Rows(0)("ma_nv").ToString()
                End If
                '***
                If hdrBL.Columns.Contains("lhxnk") Then
                    hdr.lhxnk = hdrBL.Rows(0)("lhxnk").ToString()
                End If
                If hdrBL.Columns.Contains("vt_lhxnk") Then
                    hdr.vt_lhxnk = hdrBL.Rows(0)("vt_lhxnk").ToString()
                End If
                If hdrBL.Columns.Contains("ten_lhxnk") Then
                    hdr.ten_lhxnk = hdrBL.Rows(0)("ten_lhxnk").ToString()
                End If
                If hdrBL.Columns.Contains("tk_ns") Then
                    hdr.tk_ns = hdrBL.Rows(0)("tk_ns").ToString()
                End If
                If hdrBL.Columns.Contains("ten_tk_ns") Then
                    hdr.ten_tk_ns = hdrBL.Rows(0)("ten_tk_ns").ToString()
                End If
                If hdrBL.Columns.Contains("so_bl") Then
                    hdr.so_bl = hdrBL.Rows(0)("so_bl").ToString()
                End If
                If hdrBL.Columns.Contains("ngay_batdau") Then
                    If hdrBL.Rows(0)("ngay_batdau").ToString() <> "" Then
                        hdr.ngay_bl_batdau = "to_date('" & hdrBL.Rows(0)("ngay_batdau").ToString() & "','dd/mm/yyyy')"
                    Else
                        hdr.ngay_bl_batdau = "''"
                    End If
                Else
                    hdr.ngay_bl_batdau = "''"
                End If
                If hdrBL.Columns.Contains("ngay_ketthuc") Then
                    If hdrBL.Rows(0)("ngay_ketthuc").ToString() <> "" Then
                        hdr.ngay_bl_ketthuc = "to_date('" & hdrBL.Rows(0)("ngay_ketthuc").ToString() & "','dd/mm/yyyy')"
                    Else
                        hdr.ngay_bl_ketthuc = "''"
                    End If
                Else
                    hdr.ngay_bl_ketthuc = "''"
                End If
                If hdrBL.Columns.Contains("songay_bl") Then
                    hdr.songay_bl = hdrBL.Rows(0)("songay_bl").ToString()
                End If
                If hdrBL.Columns.Contains("kieu_bl") Then
                    hdr.kieu_bl = hdrBL.Rows(0)("kieu_bl").ToString()
                End If
                If hdrBL.Columns.Contains("dien_giai") Then
                    hdr.dien_giai = hdrBL.Rows(0)("dien_giai").ToString()
                End If
                If hdrBL.Columns.Contains("ma_hq_ph") Then
                    hdr.ma_hq_ph = hdrBL.Rows(0)("ma_hq_ph").ToString()
                End If

                '21/09/2012-manhnv
                If hdrBL.Columns.Contains("ten_hq_ph") Then
                    hdr.ten_hq_ph = hdrBL.Rows(0)("ten_hq_ph").ToString()
                End If
                If hdrBL.Columns.Contains("ten_hq") Then
                    hdr.ten_hq = hdrBL.Rows(0)("ten_hq").ToString()
                End If
                If hdrBL.Columns.Contains("ma_hq") Then
                    hdr.ma_hq = hdrBL.Rows(0)("ma_hq").ToString()
                End If
                '******
                If hdrBL.Columns.Contains("ma_loaitien") Then
                    hdr.ma_loaitien = hdrBL.Rows(0)("ma_loaitien").ToString()
                End If
                If hdrBL.Columns.Contains("ma_hq_cqt") Then
                    hdr.ma_hq_cqt = hdrBL.Rows(0)("ma_hq_cqt").ToString()
                End If
                If hdrBL.Columns.Contains("so_bt_hq") Then
                    If hdrBL.Rows(0)("so_bt_hq").ToString().Trim() <> "" Then
                        hdr.so_bt_hq = hdrBL.Rows(0)("so_bt_hq").ToString()
                    End If
                End If
                If hdrBL.Columns.Contains("ma_ntk") Then
                    If hdrBL.Rows(0)("ma_ntk").ToString().Trim() <> "" Then
                        hdr.ma_ntk = hdrBL.Rows(0)("ma_ntk").ToString()
                    End If
                End If
                'Cac truong khac
                hdr.error_code_hq = pv_strErroCodeHQ
                hdr.transaction_type = pv_strTransactionType
                If pv_strErroCodeHQ = "0" Then
                    hdr.accept_yn = "Y"
                Else
                    hdr.accept_yn = "N"
                End If
                hdr.ten_kb = VBOracleLib.Globals.RemoveSign4VietnameseString(mdlCommon.Get_TenKhoBac(hdr.shkb))
                hdr.ten_cqthu = mdlCommon.Get_TenCQThu(hdr.ma_cqthu)
                hdr.ngay_bc = "sysdate"
                hdr.ngay_bn = "sysdate"
                hdr.parent_transaction_id = ""
                hdr.tk_ns_hq = ""
                '22/09/2012-manhnv----------------------------------------------------------
                If hdrBL.Columns.Contains("ma_nh_a") Then
                    hdr.ma_nh_ph = hdrBL.Rows(0)("ma_nh_a").ToString()
                End If
                If hdrBL.Columns.Contains("ma_nh_b") Then
                    hdr.ma_nh_th = hdrBL.Rows(0)("ma_nh_b").ToString()
                End If
                If hdrBL.Columns.Contains("tk_co") Then
                    hdr.tkkb_ct = hdrBL.Rows(0)("tk_co").ToString()
                End If
                '----------------------------------------------------------------------------
                '13/09/14
                hdr.so_tn_ct = pv_strSo_TN_CT
                hdr.ngay_tn_ct = pv_strNgay_TN_CT
                '13/09/14
                If hdrBL.Columns.Contains("ngay_hdon") Then
                    hdr.ngay_hd = "to_date('" & hdrBL.Rows(0)("ngay_hdon").ToString() & "','dd/mm/yyyy')"
                Else
                    hdr.ngay_hd = "''"
                End If
                If hdrBL.Columns.Contains("hoa_don") Then
                    hdr.so_hd = hdrBL.Rows(0)("hoa_don").ToString()
                End If

                If hdrBL.Columns.Contains("ngay_vtd") Then
                    hdr.ngay_vtd = "to_date('" & hdrBL.Rows(0)("ngay_vtd").ToString() & "','dd/mm/yyyy')"
                Else
                    hdr.ngay_vtd = "''"
                End If
                If hdrBL.Columns.Contains("vt_don") Then
                    hdr.vtd = hdrBL.Rows(0)("vt_don").ToString()
                End If
                If hdrBL.Columns.Contains("ngay_vtd2") Then
                    hdr.ngay_vtd2 = "to_date('" & hdrBL.Rows(0)("ngay_vtd2").ToString() & "','dd/mm/yyyy')"
                Else
                    hdr.ngay_vtd2 = "''"
                End If
                If hdrBL.Columns.Contains("vt_don2") Then
                    hdr.vtd2 = hdrBL.Rows(0)("vt_don2").ToString()
                End If
                If hdrBL.Columns.Contains("ngay_vtd3") Then
                    hdr.ngay_vtd3 = "to_date('" & hdrBL.Rows(0)("ngay_vtd3").ToString() & "','dd/mm/yyyy')"
                Else
                    hdr.ngay_vtd3 = "''"
                End If
                If hdrBL.Columns.Contains("vt_don3") Then
                    hdr.vtd3 = hdrBL.Rows(0)("vt_don3").ToString()
                End If
                If hdrBL.Columns.Contains("ngay_vtd4") Then
                    hdr.ngay_vtd4 = "to_date('" & hdrBL.Rows(0)("ngay_vtd4").ToString() & "','dd/mm/yyyy')"
                Else
                    hdr.ngay_vtd4 = "''"
                End If
                If hdrBL.Columns.Contains("vt_don4") Then
                    hdr.vtd4 = hdrBL.Rows(0)("vt_don4").ToString()
                End If
                If hdrBL.Columns.Contains("ngay_vtd5") Then
                    hdr.ngay_vtd5 = "to_date('" & hdrBL.Rows(0)("ngay_vtd5").ToString() & "','dd/mm/yyyy')"
                Else
                    hdr.ngay_vtd5 = "''"
                End If
                If hdrBL.Columns.Contains("vt_don5") Then
                    hdr.vtd5 = hdrBL.Rows(0)("vt_don5").ToString()
                End If

            End If

            If dtlsBL.Rows.Count > 0 Then
                For Each row As DataRow In dtlsBL.Rows
                    Try
                        objDTL = New Business.infDoiChieuNHHQ_DTL
                        objDTL.ma_cap = row("ma_cap").ToString()
                        objDTL.ma_chuong = row("ma_chuong").ToString()
                        objDTL.noi_dung = row("noi_dung").ToString()
                        objDTL.ma_dp = row("ma_dp").ToString()
                        objDTL.ky_thue = row("ky_thue").ToString()
                        objDTL.sotien = row("sotien").ToString()

                        If dtlsBL.Columns.Contains("ma_quy") Then
                            objDTL.ma_quy = row("ma_quy").ToString()
                        End If
                        If dtlsBL.Columns.Contains("ma_nkt") Then
                            objDTL.ma_nkt = row("ma_nkt").ToString()
                        End If
                        If dtlsBL.Columns.Contains("ma_tmuc") Then
                            objDTL.ma_ndkt = row("ma_tmuc").ToString()
                        End If
                        If dtlsBL.Columns.Contains("ma_nkt_cha") Then
                            objDTL.ma_nkt_cha = row("ma_nkt_cha").ToString()
                        End If
                        If dtlsBL.Columns.Contains("ma_ndkt") Then
                            objDTL.ma_ndkt = row("ma_ndkt").ToString()
                        End If
                        If dtlsBL.Columns.Contains("ma_khoan") Then
                            objDTL.ma_nkt = row("ma_khoan").ToString()
                        End If
                        If dtlsBL.Columns.Contains("ma_ndkt_cha") Then
                            objDTL.ma_ndkt_cha = row("ma_ndkt_cha").ToString()
                        End If
                        If dtlsBL.Columns.Contains("ma_tlpc") Then
                            If row("ma_tlpc").ToString().Trim() <> "" Then
                                objDTL.ma_tlpc = row("ma_tlpc").ToString()
                            End If
                        End If
                        If dtlsBL.Columns.Contains("ma_khtk") Then
                            objDTL.ma_khtk = row("ma_khtk").ToString()
                        End If

                        ReDim Preserve dtls(k)
                        dtls(k) = objDTL
                        k = k + 1
                    Catch ex As Exception
                        log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin chứng từ", HttpContext.Current.Session.Item("TEN_DN"))
                        Throw ex
                        '      LogDebug.WriteLog("Error source:" & ex.StackTrace & vbNewLine _
                        '& "Error code: System error!" & vbNewLine _
                        '& "Error message: " & ex.Message, EventLogEntryType.Error)
                    End Try
                Next
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin chứng từ", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source:" & ex.StackTrace & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Public Shared Sub SetObjectToDoiChieuNHHQ(ByVal pv_strSHKB As String, ByVal pv_strSoCT As String, _
                                        ByVal pv_strSoBT As String, ByVal pv_strMaNV As String, _
                                        ByVal pv_strType As String, _
                                        ByRef hdr As Business_HQ.infDoiChieuNHHQ_HDR, ByRef dtls() As Business_HQ.infDoiChieuNHHQ_DTL, _
                                        Optional ByVal pv_strMaDT As String = "", _
                                        Optional ByVal pv_strErroCodeHQ As String = "", _
                                        Optional ByVal pv_strTransactionType As String = "", _
                                        Optional ByVal pv_strNgayKB As String = "", Optional ByVal pv_strSo_TN_CT As String = "", _
                                         Optional ByVal pv_strNgay_TN_CT As String = "", Optional ByVal p_flagBL As Boolean = False)

        Dim strSender_Code As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Code").ToString()
        Dim strSender_Name As String = ConfigurationManager.AppSettings.Get("CUSTOMS.Sender_Name").ToString()

        Dim hdrBL As New DataTable
        Dim dtlsBL As New DataTable
        Dim objDTL As Business_HQ.infDoiChieuNHHQ_DTL
        Dim k As Integer = 0
        Dim key As New KeyCTu
        Try
            'Tao key chung tu
            If pv_strMaDT <> "" Then
                key.Ma_Dthu = pv_strMaDT
            Else
                key.Ma_Dthu = TCS_GetMaDT(pv_strMaNV)
            End If
            key.Kyhieu_CT = TCS_GetKHCT(pv_strSHKB)
            key.Ma_NV = pv_strMaNV
            If pv_strNgayKB.Length <> 0 Then
                key.Ngay_KB = pv_strNgayKB
            Else
                key.Ngay_KB = TCS_GetNgayLV(pv_strMaNV)
            End If
            key.SHKB = pv_strSHKB
            key.So_BT = pv_strSoBT
            key.So_CT = pv_strSoCT

            'Lay thong tin ChungTu
            If pv_strType = "CTU" Then
                hdrBL = ChungTu.buChungTu.SelectCT_HDR(key.SHKB, key.Ngay_KB, key.Ma_NV, key.So_BT, key.Ma_Dthu).Tables(0)
                dtlsBL = ChungTu.buChungTu.SelectCT_DTL(key.SHKB, key.Ngay_KB, key.Ma_NV, key.So_BT, key.Ma_Dthu).Tables(0)
            Else
                'Lay thong tin BaoLanh
                LoadBaoLanhChiTiet(key, hdrBL, dtlsBL)
            End If

            If hdrBL.Rows.Count > 0 Then
                hdr.shkb = hdrBL.Rows(0)("shkb").ToString()
                hdr.ngay_kb = hdrBL.Rows(0)("ngay_kb").ToString()
                hdr.so_bt = hdrBL.Rows(0)("so_bt").ToString()
                hdr.kyhieu_ct = hdrBL.Rows(0)("kyhieu_ct").ToString()
                hdr.so_ct = hdrBL.Rows(0)("so_ct").ToString()
                hdr.ngay_ct = hdrBL.Rows(0)("ngay_ct").ToString()
                hdr.ma_nnthue = hdrBL.Rows(0)("ma_nnthue").ToString()
                hdr.ten_nnthue = hdrBL.Rows(0)("ten_nnthue").ToString()
                hdr.ma_cqthu = hdrBL.Rows(0)("ma_cqthu").ToString()
                hdr.ma_hq_cqt = hdrBL.Rows(0)("ma_cqthu").ToString()
                hdr.so_tk = hdrBL.Rows(0)("so_tk").ToString()
                hdr.ttien = hdrBL.Rows(0)("ttien").ToString()
                hdr.ttien_nt = hdrBL.Rows(0)("ttien_nt").ToString()
                hdr.trang_thai = hdrBL.Rows(0)("trang_thai").ToString()
                hdr.ly_do_huy = hdrBL.Rows(0)("ly_do_huy").ToString()
                hdr.ma_nt = hdrBL.Rows(0)("ma_nt").ToString()
                hdr.ten_nh_ph = strSender_Name
                hdr.ma_ntk = hdrBL.Rows(0)("ma_ntk").ToString()
                hdr.ty_gia = hdrBL.Rows(0)("ty_gia").ToString()
                If p_flagBL = False Then
                    ' hdr.tg_id = hdrBL.Rows(0)("tg_id").ToString()
                End If


                If hdrBL.Rows(0)("ngay_tk").ToString() <> "" Then
                    hdr.ngay_tk = "to_date('" & hdrBL.Rows(0)("ngay_tk").ToString() & "','dd/mm/yyyy')"
                Else
                    hdr.ngay_tk = "''"
                End If
                '21/09/2012-manhnv
                If hdrBL.Columns.Contains("lh_xnk") Then
                    hdr.lhxnk = hdrBL.Rows(0)("lh_xnk").ToString()
                End If
                '***
                '27/09/2012-manhnv
                If hdrBL.Columns.Contains("ma_nv") Then
                    hdr.ma_nv = hdrBL.Rows(0)("ma_nv").ToString()
                End If
                '***
                If hdrBL.Columns.Contains("lhxnk") Then
                    hdr.lhxnk = hdrBL.Rows(0)("lhxnk").ToString()
                End If
                If hdrBL.Columns.Contains("vt_lhxnk") Then
                    hdr.vt_lhxnk = hdrBL.Rows(0)("vt_lhxnk").ToString()
                End If
                If hdrBL.Columns.Contains("ten_lhxnk") Then
                    hdr.ten_lhxnk = hdrBL.Rows(0)("ten_lhxnk").ToString()
                End If
                If hdrBL.Columns.Contains("tk_ns") Then
                    hdr.tk_ns = hdrBL.Rows(0)("tk_ns").ToString()
                End If
                If hdrBL.Columns.Contains("ten_tk_ns") Then
                    hdr.ten_tk_ns = hdrBL.Rows(0)("ten_tk_ns").ToString()
                End If
                If hdrBL.Columns.Contains("so_bl") Then
                    hdr.so_bl = hdrBL.Rows(0)("so_bl").ToString()
                End If
                If hdrBL.Columns.Contains("ngay_batdau") Then
                    If hdrBL.Rows(0)("ngay_batdau").ToString() <> "" Then
                        hdr.ngay_bl_batdau = "to_date('" & hdrBL.Rows(0)("ngay_batdau").ToString() & "','dd/mm/yyyy')"
                    Else
                        hdr.ngay_bl_batdau = "''"
                    End If
                Else
                    hdr.ngay_bl_batdau = "''"
                End If
                If hdrBL.Columns.Contains("ngay_ketthuc") Then
                    If hdrBL.Rows(0)("ngay_ketthuc").ToString() <> "" Then
                        hdr.ngay_bl_ketthuc = "to_date('" & hdrBL.Rows(0)("ngay_ketthuc").ToString() & "','dd/mm/yyyy')"
                    Else
                        hdr.ngay_bl_ketthuc = "''"
                    End If
                Else
                    hdr.ngay_bl_ketthuc = "''"
                End If
                If hdrBL.Columns.Contains("songay_bl") Then
                    hdr.songay_bl = hdrBL.Rows(0)("songay_bl").ToString()
                End If
                If hdrBL.Columns.Contains("kieu_bl") Then
                    hdr.kieu_bl = hdrBL.Rows(0)("kieu_bl").ToString()
                End If
                If hdrBL.Columns.Contains("dien_giai") Then
                    hdr.dien_giai = hdrBL.Rows(0)("dien_giai").ToString()
                End If
                If hdrBL.Columns.Contains("ma_hq_ph") Then
                    hdr.ma_hq_ph = hdrBL.Rows(0)("ma_hq_ph").ToString()
                End If

                '21/09/2012-manhnv
                If hdrBL.Columns.Contains("ten_hq_ph") Then
                    hdr.ten_hq_ph = hdrBL.Rows(0)("ten_hq_ph").ToString()
                End If
                If hdrBL.Columns.Contains("ten_hq") Then
                    hdr.ten_hq = hdrBL.Rows(0)("ten_hq").ToString()
                End If
                If hdrBL.Columns.Contains("ma_hq") Then
                    hdr.ma_hq = hdrBL.Rows(0)("ma_hq").ToString()
                End If
                '******
                If hdrBL.Columns.Contains("ma_loaitien") Then
                    hdr.ma_loaitien = hdrBL.Rows(0)("ma_loaitien").ToString()
                End If
                If hdrBL.Columns.Contains("ma_hq_cqt") Then
                    hdr.ma_hq_cqt = hdrBL.Rows(0)("ma_hq_cqt").ToString()
                End If
                If hdrBL.Columns.Contains("so_bt_hq") Then
                    If hdrBL.Rows(0)("so_bt_hq").ToString().Trim() <> "" Then
                        hdr.so_bt_hq = hdrBL.Rows(0)("so_bt_hq").ToString()
                    End If
                End If
                If hdrBL.Columns.Contains("ma_ntk") Then
                    If hdrBL.Rows(0)("ma_ntk").ToString().Trim() <> "" Then
                        hdr.ma_ntk = hdrBL.Rows(0)("ma_ntk").ToString()
                    End If
                End If
                'Cac truong khac
                hdr.error_code_hq = pv_strErroCodeHQ
                hdr.transaction_type = pv_strTransactionType
                If pv_strErroCodeHQ = "0" Then
                    hdr.accept_yn = "Y"
                Else
                    hdr.accept_yn = "N"
                End If
                hdr.ten_kb = VBOracleLib.Globals.RemoveSign4VietnameseString(mdlCommon.Get_TenKhoBac(hdr.shkb))
                hdr.ten_cqthu = mdlCommon.Get_TenCQThu(hdr.ma_cqthu)
                hdr.ngay_bc = "sysdate"
                hdr.ngay_bn = "sysdate"
                hdr.parent_transaction_id = ""
                hdr.tk_ns_hq = ""
                '22/09/2012-manhnv----------------------------------------------------------
                If hdrBL.Columns.Contains("ma_nh_a") Then
                    hdr.ma_nh_ph = hdrBL.Rows(0)("ma_nh_a").ToString()
                End If
                If hdrBL.Columns.Contains("ma_nh_b") Then
                    hdr.ma_nh_th = hdrBL.Rows(0)("ma_nh_b").ToString()
                End If
                If hdrBL.Columns.Contains("tk_co") Then
                    hdr.tkkb_ct = hdrBL.Rows(0)("tk_co").ToString()
                End If
                '----------------------------------------------------------------------------
                '13/09/14
                hdr.so_tn_ct = pv_strSo_TN_CT
                hdr.ngay_tn_ct = pv_strNgay_TN_CT
                '13/09/14
                If hdrBL.Columns.Contains("ngay_hdon") Then
                    hdr.ngay_hd = "to_date('" & hdrBL.Rows(0)("ngay_hdon").ToString() & "','dd/mm/yyyy')"
                Else
                    hdr.ngay_hd = "''"
                End If
                If hdrBL.Columns.Contains("hoa_don") Then
                    hdr.so_hd = hdrBL.Rows(0)("hoa_don").ToString()
                End If

                If hdrBL.Columns.Contains("ngay_vtd") Then
                    hdr.ngay_vtd = "to_date('" & hdrBL.Rows(0)("ngay_vtd").ToString() & "','dd/mm/yyyy')"
                Else
                    hdr.ngay_vtd = "''"
                End If
                If hdrBL.Columns.Contains("vt_don") Then
                    hdr.vtd = hdrBL.Rows(0)("vt_don").ToString()
                End If
                If hdrBL.Columns.Contains("ngay_vtd2") Then
                    hdr.ngay_vtd2 = "to_date('" & hdrBL.Rows(0)("ngay_vtd2").ToString() & "','dd/mm/yyyy')"
                Else
                    hdr.ngay_vtd2 = "''"
                End If
                If hdrBL.Columns.Contains("vt_don2") Then
                    hdr.vtd2 = hdrBL.Rows(0)("vt_don2").ToString()
                End If
                If hdrBL.Columns.Contains("ngay_vtd3") Then
                    hdr.ngay_vtd3 = "to_date('" & hdrBL.Rows(0)("ngay_vtd3").ToString() & "','dd/mm/yyyy')"
                Else
                    hdr.ngay_vtd3 = "''"
                End If
                If hdrBL.Columns.Contains("vt_don3") Then
                    hdr.vtd3 = hdrBL.Rows(0)("vt_don3").ToString()
                End If
                If hdrBL.Columns.Contains("ngay_vtd4") Then
                    hdr.ngay_vtd4 = "to_date('" & hdrBL.Rows(0)("ngay_vtd4").ToString() & "','dd/mm/yyyy')"
                Else
                    hdr.ngay_vtd4 = "''"
                End If
                If hdrBL.Columns.Contains("vt_don4") Then
                    hdr.vtd4 = hdrBL.Rows(0)("vt_don4").ToString()
                End If
                If hdrBL.Columns.Contains("ngay_vtd5") Then
                    hdr.ngay_vtd5 = "to_date('" & hdrBL.Rows(0)("ngay_vtd5").ToString() & "','dd/mm/yyyy')"
                Else
                    hdr.ngay_vtd5 = "''"
                End If
                If hdrBL.Columns.Contains("vt_don5") Then
                    hdr.vtd5 = hdrBL.Rows(0)("vt_don5").ToString()
                End If

            End If

            If dtlsBL.Rows.Count > 0 Then
                For Each row As DataRow In dtlsBL.Rows
                    Try
                        objDTL = New Business_HQ.infDoiChieuNHHQ_DTL
                        objDTL.ma_cap = row("ma_cap").ToString()
                        objDTL.ma_chuong = row("ma_chuong").ToString()
                        objDTL.noi_dung = row("noi_dung").ToString()
                        objDTL.ma_dp = row("ma_dp").ToString()
                        objDTL.ky_thue = row("ky_thue").ToString()
                        objDTL.sotien = row("sotien").ToString()

                        If dtlsBL.Columns.Contains("ma_quy") Then
                            objDTL.ma_quy = row("ma_quy").ToString()
                        End If
                        If dtlsBL.Columns.Contains("ma_nkt") Then
                            objDTL.ma_nkt = row("ma_nkt").ToString()
                        End If
                        If dtlsBL.Columns.Contains("ma_tmuc") Then
                            objDTL.ma_ndkt = row("ma_tmuc").ToString()
                        End If
                        If dtlsBL.Columns.Contains("ma_nkt_cha") Then
                            objDTL.ma_nkt_cha = row("ma_nkt_cha").ToString()
                        End If
                        If dtlsBL.Columns.Contains("ma_ndkt") Then
                            objDTL.ma_ndkt = row("ma_ndkt").ToString()
                        End If
                        If dtlsBL.Columns.Contains("ma_khoan") Then
                            objDTL.ma_nkt = row("ma_khoan").ToString()
                        End If
                        If dtlsBL.Columns.Contains("ma_ndkt_cha") Then
                            objDTL.ma_ndkt_cha = row("ma_ndkt_cha").ToString()
                        End If
                        If dtlsBL.Columns.Contains("ma_tlpc") Then
                            If row("ma_tlpc").ToString().Trim() <> "" Then
                                objDTL.ma_tlpc = row("ma_tlpc").ToString()
                            End If
                        End If
                        If dtlsBL.Columns.Contains("ma_khtk") Then
                            objDTL.ma_khtk = row("ma_khtk").ToString()
                        End If

                        ReDim Preserve dtls(k)
                        dtls(k) = objDTL
                        k = k + 1
                    Catch ex As Exception
                        log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin chứng từ", HttpContext.Current.Session.Item("TEN_DN"))
                        Throw ex
                        '      LogDebug.WriteLog("Error source:" & ex.StackTrace & vbNewLine _
                        '& "Error code: System error!" & vbNewLine _
                        '& "Error message: " & ex.Message, EventLogEntryType.Error)
                    End Try
                Next
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin chứng từ", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source:" & ex.StackTrace & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub
    'Public Shared Sub TCS_UpdateTrangThai(ByVal pv_strSHKB As String, ByVal pv_strSoCT As String, _
    '                                ByVal pv_strSoBT As String, ByVal pv_strMaNV As Integer, ByVal trangThai As String, _
    '                                Optional ByVal strLyDoHuy As String = "", Optional ByVal strNgayKB As String = "")
    '    Dim key As New KeyCTu
    '    key.Ma_Dthu = TCS_GetMaDT(pv_strMaNV)
    '    key.Kyhieu_CT = TCS_GetKHCT(pv_strSHKB)
    '    key.Ma_NV = pv_strMaNV
    '    If strNgayKB.Length <> 0 Then
    '        key.Ngay_KB = strNgayKB
    '    Else
    '        key.Ngay_KB = TCS_GetNgayLV(pv_strMaNV)
    '    End If
    '    key.SHKB = pv_strSHKB
    '    key.So_BT = pv_strSoBT
    '    key.So_CT = pv_strSoCT

    '    UpdateTrangThaiBaoLanh(key, trangThai, strLyDoHuy)
    'End Sub

    Public Shared Sub TCS_UpdateTrangThai(ByVal pv_strSHKB As String, ByVal pv_strSoCT As String, _
                                ByVal pv_strSoBT As String, ByVal pv_strMaNV As Integer, ByVal trangThai As String, _
                                Optional ByVal strLyDoHuy As String = "", Optional ByVal strNgayKB As String = "", Optional ByVal strLyDoCTra As String = "")
        Dim key As New KeyCTu
        Try
            'key.Ma_Dthu = TCS_GetMaDT(pv_strMaNV)
            key.Kyhieu_CT = TCS_GetKHCT(pv_strSHKB)
            key.Ma_NV = pv_strMaNV
            If strNgayKB.Length <> 0 Then
                key.Ngay_KB = strNgayKB
            Else
                key.Ngay_KB = TCS_GetNgayLV(pv_strMaNV)
            End If
            key.SHKB = pv_strSHKB
            key.So_BT = pv_strSoBT
            key.So_CT = pv_strSoCT
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi cập nhật trạng thái chứng từ", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source:" & ex.StackTrace & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
        UpdateTrangThaiBaoLanh(key, trangThai, strLyDoHuy, strLyDoCTra)
    End Sub

    Public Shared Sub LoadBaoLanhChiTiet(ByVal key As KeyCTu, ByRef hdr As DataTable, ByRef dtls As DataTable)
        Try
            hdr = ChungTu.buChungTu.BaoLanh_Header_Set(key).Tables(0)
            If hdr.Rows.Count > 0 Then
                key.Ngay_KB = hdr.Rows(0)("ngay_kb").ToString()
                key.SHKB = hdr.Rows(0)("shkb").ToString()
                key.Ma_NV = hdr.Rows(0)("ma_nv").ToString()
                key.Ma_Dthu = hdr.Rows(0)("ma_dthu").ToString()
            End If

            dtls = ChungTu.buChungTu.BaoLanh_ChiTiet_Set(key).Tables(0)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin chi tiết", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source:" & ex.StackTrace & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Public Shared Sub UpdateTrangThaiBaoLanh(ByVal key As KeyCTu, ByVal trangThai As String, ByVal lyDoHuy As String, ByVal lyDoCTra As String)
        Try
            ChungTu.buChungTu.UpdateTrangThaiBaoLanh(key, trangThai, lyDoHuy, lyDoCTra)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi cập nhật trạng thái bảo lãnh", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source:" & ex.StackTrace & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    ''' <summary>
    ''' Tim kiem thong tin chung tu trong db vao build lai chuoi xml de tra ve cho client
    ''' </summary>
    ''' <param name="key">Thong tin chung tu</param>
    ''' <returns>Chuoi xml chua thong tin cua chung tu</returns>
    ''' <remarks></remarks>
    Public Shared Function LoadCTChiTiet(ByVal key As KeyCTu) As String
        Try
            Dim v_dtHDR As New DataTable
            Dim v_dtDTL As New DataTable
            Dim v_strTrangThaiCT As String = "01"

            Dim s, strTKCo, strTKNo As String
            Dim dsCT As New DataSet
            dsCT = ChungTu.buChungTu.CTU_Header_Set(key)
            Dim iCount As Integer

            If (IsEmptyDataSet(dsCT)) Then Return Nothing 'Không có dữ liệu

            v_dtHDR = TCS_BuildCTU_HDR_TBL()

            Dim dr As DataRow = dsCT.Tables(0).Rows(0)

            iCount = dsCT.Tables(0).Rows.Count

            v_dtHDR.Rows(0)(ColHDR.TRANG_THAI) = dr("TRANG_THAI").ToString()
            v_strTrangThaiCT = dr("TRANG_THAI").ToString()
            v_dtHDR.Rows(0)(ColHDR.TT_BDS) = IIf(dr("ISACTIVE").ToString() = "1", "O", "F")
            v_dtHDR.Rows(0)(ColHDR.TT_NSTT) = IIf(dr("TT_TTHU") <> 0, "Y", "N")
            v_dtHDR.Rows(0)(ColHDR.TT_TTHU) = dr("TT_TTHU").ToString
            v_dtHDR.Rows(0)(ColHDR.MA_NV) = dr("MA_NV").ToString
            v_dtHDR.Rows(0)(ColHDR.So_BT) = dr("So_BT").ToString()
            v_dtHDR.Rows(0)(ColHDR.SHKB) = dr("SHKB").ToString()
            v_dtHDR.Rows(0)(ColHDR.TenKB) = dr("TEN_KB").ToString()
            v_dtHDR.Rows(0)(ColHDR.TT_CTHUE) = dr("TT_CTHUE").ToString()
            v_dtHDR.Rows(0)(ColHDR.TTIEN_TN) = dr("TTIEN_NT").ToString()


            ' ****************************************************************************
            ' Người sửa: Anhld
            ' Ngày 14/09/2012
            ' Mục đích: Them ma hai quan va loai tien thue
            '*****************************************************************************
            'v_dtHDR.Rows(0)(ColHDR.MA_HQ) = dr("MA_HQ").ToString()
            'v_dtHDR.Rows(0)(["LOAI_TT"] = dr("LOAI_TT").ToString()  
            v_dtHDR.Rows(0).Item("MA_HQ") = dr("MA_HQ").ToString()
            v_dtHDR.Rows(0).Item("LOAI_TT") = dr("LOAI_TT").ToString()
            v_dtHDR.Rows(0).Item("TEN_HQ") = dr("TEN_HQ").ToString()
            v_dtHDR.Rows(0).Item("MA_HQ_PH") = dr("MA_HQ_PH").ToString()
            v_dtHDR.Rows(0).Item("TEN_HQ_PH") = dr("TEN_HQ_PH").ToString()

            v_dtHDR.Rows(0)(ColHDR.NgayLV) = ConvertNumbertoString(dr("NGAY_KB").ToString())
            'CType(grdHeader.Rows(RowHeader.NgayLV).Cells.Item(1).Controls(0), TextBox).Text = ConvertNumbertoString(dr("NGAY_KB").ToString())
            'CType(grdHeader.Rows(RowHeader.NgayLV).Cells.Item(1).Controls(0), TextBox).Text = "Mã NV: " & GetString(dr("MA_NV")).PadLeft(3, "0") & "  -  Số BT: " & GetString(dr("so_bt")).PadLeft(5, "0")

            v_dtHDR.Rows(0)(ColHDR.TKCo) = dr("TK_Co").ToString()
            v_dtHDR.Rows(0)(ColHDR.TenTKCo) = dr("Ten_TKCo").ToString()

            v_dtHDR.Rows(0)(ColHDR.TKNo) = dr("TK_No").ToString()
            v_dtHDR.Rows(0)(ColHDR.TenTKNo) = ""

            strTKCo = dr("TK_Co").ToString()
            strTKNo = dr("TK_No").ToString()

            'DBHC
            s = dr("MA_XA").ToString()
            If dr("MA_XA").ToString().Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaDBHC) = s
                v_dtHDR.Rows(0)(ColHDR.TenDBHC) = dr("TENXA").ToString()
            End If


            v_dtHDR.Rows(0)(ColHDR.SoCT) = dr("SO_CT").ToString()
            v_dtHDR.Rows(0)(ColHDR.SoCTNH) = dr("SO_CT").ToString()

            Dim v_strMa_NNT As String = dr("MA_NNTHUE").ToString()
            v_dtHDR.Rows(0)(ColHDR.MaNNT) = dr("MA_NNTHUE").ToString()
            v_dtHDR.Rows(0)(ColHDR.TenNNT) = gc_BLANK_CHARACTER & dr("TEN_NNTHUE").ToString()
            v_dtHDR.Rows(0)(ColHDR.DChiNNT) = dr("DC_NNTHUE").ToString()

            v_dtHDR.Rows(0)(ColHDR.MaNNTien) = dr("Ma_NNTien").ToString()
            v_dtHDR.Rows(0)(ColHDR.TenNNTien) = dr("TEN_NNTIEN").ToString()
            v_dtHDR.Rows(0)(ColHDR.DChiNNTien) = dr("DC_NNTIEN").ToString()
            v_dtHDR.Rows(0)(ColHDR.KHCT) = dr("KyHieu_CT").ToString()

            v_dtHDR.Rows(0)(ColHDR.MaNT) = dr("Ma_NT").ToString()
            v_dtHDR.Rows(0)(ColHDR.TenNT) = dr("Ten_NT").ToString()



            s = dr("MA_CQTHU").ToString()
            If dr("MA_CQTHU").ToString().Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaCQThu) = s
                v_dtHDR.Rows(0)(ColHDR.TenCQThu) = dr("TEN_CQTHU").ToString()
            End If


            v_dtHDR.Rows(0)(ColHDR.TenTKCo) = dr("TEN_TKCO").ToString()

            v_dtHDR.Rows(0)(ColHDR.TenTKNo) = ""




            Dim strMaLH As String = GetString(dr("LH_XNK").ToString())
            Dim strTenVT, strTenLH As String
            Get_LHXNK_ALL(strMaLH, strTenLH, strTenVT)
            v_dtHDR.Rows(0)(ColHDR.LHXNK) = strMaLH ' strTenVT
            v_dtHDR.Rows(0)(ColHDR.DescLHXNK) = strTenLH 'strMaLH & "-" &

            v_dtHDR.Rows(0)(ColHDR.SoKhung) = dr("SO_KHUNG").ToString()
            v_dtHDR.Rows(0)(ColHDR.SoMay) = dr("SO_MAY").ToString()
            v_dtHDR.Rows(0)(ColHDR.ToKhaiSo) = dr("SO_TK").ToString()
            v_dtHDR.Rows(0)(ColHDR.NgayDK) = dr("NGAY_TK").ToString()
            v_dtHDR.Rows(0)(ColHDR.So_BK) = dr("So_BK").ToString()
            v_dtHDR.Rows(0)(ColHDR.Ngay_BK) = dr("Ngay_BK").ToString()


            s = dr("SO_CT_NH").ToString()
            v_dtHDR.Rows(0)(ColHDR.SoCTNH) = s
            v_dtHDR.Rows(0)(ColHDR.TK_KH_NH) = dr("TK_KH_NH").ToString()
            v_dtHDR.Rows(0)(ColHDR.TenTK_KH_NH) = dr("TEN_KH_NH").ToString()
            v_dtHDR.Rows(0)(ColHDR.NGAY_KH_NH) = dr("NGAY_KH_NH").ToString()

            If dr("TK_KH_NH").ToString().Length > 0 Then
                If v_strTrangThaiCT = "05" Then
                    If GetString(dr("PT_TT")).ToString = "01" Then
                        v_dtHDR.Rows(0)(ColHDR.SoDu_KH_NH) = GetSoDu_KH_NH(dr("TK_KH_NH").ToString().Replace("-", ""))
                    End If
                End If
            End If

            'Gan lai phuong thu thanh toan
            v_dtHDR.Rows(0)(ColHDR.HTTT) = GetString(dr("PT_TT")).ToString


            v_dtHDR.Rows(0)(ColHDR.MA_NH_A) = GetString(dr("MA_NH_A").ToString())
            v_dtHDR.Rows(0)(ColHDR.Ten_NH_A) = GetString(dr("Ten_NH_A").ToString())
            v_dtHDR.Rows(0)(ColHDR.MA_NH_B) = GetString(dr("MA_NH_B").ToString())
            v_dtHDR.Rows(0)(ColHDR.Ten_NH_B) = GetString(dr("Ten_NH_B").ToString())
            v_dtHDR.Rows(0)(ColHDR.MA_NH_TT) = GetString(dr("MA_NH_TT").ToString())
            v_dtHDR.Rows(0)(ColHDR.TEN_NH_TT) = GetString(dr("TEN_NH_TT").ToString())
            v_dtHDR.Rows(0)(ColHDR.TT_CTHUE) = GetString(dr("TT_CTHUE").ToString())
            'CType(grdHeader.Rows(RowHeader.TKKHNhan).Cells.Item(1).Controls(0), TextBox).Text = dr("TK_KH_NHAN").ToString()
            'CType(grdHeader.Rows(RowHeader.TenKHNhan).Cells.Item(1).Controls(0), TextBox).Text = dr("TEN_KH_NHAN").ToString()
            'CType(grdHeader.Rows(RowHeader.DCKHNhan).Cells.Item(1).Controls(0), TextBox).Text = dr("DIACHI_KH_NHAN").ToString()

            s = dr("MA_LTHUE").ToString()
            v_dtHDR.Rows(0)(ColHDR.LoaiThue) = s
            v_dtHDR.Rows(0)(ColHDR.DescLoaiThue) = GetString(dr("TEN_LTHUE").ToString())

            v_dtHDR.Rows(0)(ColHDR.PHI_GD) = GetString(dr("PHI_GD").ToString())
            v_dtHDR.Rows(0)(ColHDR.PHI_VAT) = GetString(dr("PHI_VAT").ToString())
            v_dtHDR.Rows(0)(ColHDR.PT_TINHPHI) = GetString(dr("PT_TINHPHI").ToString())
            v_dtHDR.Rows(0)(ColHDR.Huyen_NNTIEN) = dr("ten_tinh").ToString()
            v_dtHDR.Rows(0)(ColHDR.Tinh_NNTIEN) = dr("ma_tinh").ToString()

            v_dtHDR.Rows(0)(ColHDR.MA_KS) = dr("MA_KS").ToString()
            'MA_SANPHAM
            v_dtHDR.Rows(0)(ColHDR.MA_SANPHAM) = GetString(dr("MA_SANPHAM").ToString())
            v_dtHDR.Rows(0)(ColHDR.TT_CITAD) = GetString(dr("TT_CITAD").ToString())
            v_dtHDR.Rows(0)(ColHDR.MA_NTK) = GetString(dr("MA_NTK").ToString())
            v_dtHDR.Rows(0)(ColHDR.DIEN_GIAI) = GetString(dr("DIEN_GIAI").ToString())

            'sonmt 
            v_dtHDR.Rows(0)(ColHDR.MA_HUYEN) = GetString(dr("MAHUYEN_NNT").ToString())
            v_dtHDR.Rows(0)(ColHDR.TEN_HUYEN) = GetString(dr("TENHUYEN_NNT").ToString())
            v_dtHDR.Rows(0)(ColHDR.MA_TINH) = GetString(dr("MATINH_NNT").ToString())
            v_dtHDR.Rows(0)(ColHDR.TEN_TINH) = GetString(dr("TENTINH_NNT").ToString())
            v_dtHDR.Rows(0)(ColHDR.HUYEN_NNTHAY) = GetString(dr("TENHUYEN_NNTHAY").ToString())
            v_dtHDR.Rows(0)(ColHDR.TINH_NNTHAY) = GetString(dr("TENTINH_NNTHAY").ToString())
            v_dtHDR.Rows(0)(ColHDR.SO_QD) = GetString(dr("SO_QD").ToString())
            v_dtHDR.Rows(0)(ColHDR.CQ_QD) = GetString(dr("CQ_QD").ToString())
            v_dtHDR.Rows(0)(ColHDR.NGAY_QD) = GetString(dr("NGAY_QD").ToString())
            v_dtHDR.Rows(0)(ColHDR.GHI_CHU) = GetString(dr("GHI_CHU").ToString())


            'sonmt
            v_dtHDR.Rows(0)(ColHDR.LOAI_NNT) = dr("LOAI_NNT").ToString()
            v_dtHDR.Rows(0)(ColHDR.TEN_LOAI_NNT) = dr("TEN_LOAI_NNT").ToString()

            ' Lấy thông tin chi tiết
            Dim dblTien As Double = 0

            Dim dblTongTien As Double = 0

            'dsCT = ChungTu.buChungTu.CTU_ChiTiet_Set(key, blnTHOP)
            dsCT = ChungTu.buChungTu.CTU_ChiTiet_Set(key)

            If (IsEmptyDataSet(dsCT)) Then Return Nothing 'Không có dữ liệu
            v_dtDTL = TCS_BuildCTU_DTL_TBL()

            iCount = dsCT.Tables(0).Rows.Count      'Lấy số bản ghi
            Dim strIDList(iCount - 1) As String

            Dim i As Integer

            'grdChiTiet.Rows.Count = 1
            For i = 0 To iCount - 1
                dr = dsCT.Tables(0).Rows(i)
                strIDList(i) = dr("ID").ToString()

                v_dtDTL.Rows(i)(ColDTL.MaQuy) = GetString(dr("MaQuy").ToString())
                v_dtDTL.Rows(i)(ColDTL.CChuong) = GetString(dr("Ma_Chuong").ToString())
                v_dtDTL.Rows(i)(ColDTL.LKhoan) = GetString(dr("Ma_Khoan").ToString())
                v_dtDTL.Rows(i)(ColDTL.MTieuMuc) = GetString(dr("Ma_TMuc").ToString())
                v_dtDTL.Rows(i)(ColDTL.NoiDung) = GetString(dr("Noi_Dung").ToString())

                'grdChiTiet.Items(i).Visible = True
                'CType(grdChiTiet.Items(i).Controls(clsGridCTU.ColDTL.MaQuy).Controls(1), TextBox).Text = dr("MaQuy").ToString()
                'CType(grdChiTiet.Items(i).Controls(clsGridCTU.ColDTL.CChuong).Controls(1), TextBox).Text = dr("Ma_Chuong").ToString()
                'CType(grdChiTiet.Items(i).Controls(clsGridCTU.ColDTL.LKhoan).Controls(1), TextBox).Text = dr("Ma_Khoan").ToString()
                'CType(grdChiTiet.Items(i).Controls(clsGridCTU.ColDTL.MTieuMuc).Controls(1), TextBox).Text = dr("Ma_TMuc").ToString()
                'CType(grdChiTiet.Items(i).Controls(clsGridCTU.ColDTL.NoiDung).Controls(1), TextBox).Text = dr("Noi_Dung").ToString()

                s = dr("SoTien_NT").ToString()
                If (IsNumeric(s)) Then
                    dblTien = Convert.ToDouble(s)
                Else
                    dblTien = 0
                End If

                s = dr("TTien").ToString()

                If (IsNumeric(s)) Then
                    dblTien = Convert.ToDouble(s)
                Else
                    dblTien = 0
                End If
                dblTongTien += dblTien

                v_dtDTL.Rows(i)(ColDTL.Tien) = VBOracleLib.Globals.Format_Number(dblTien.ToString, ".")
                v_dtDTL.Rows(i)(ColDTL.KyThue) = dr("Ky_Thue").ToString()

                'TO DO :Kienvt tinh truc tiep thong tin nay bang thu tuc trong db
                If v_dtHDR.Rows(0)(ColHDR.LoaiThue).ToString <> "04" Then
                    v_dtDTL.Rows(i)(ColDTL.SoDT) = TCS_CalSoDaThu(v_strMa_NNT, dr("MaQuy").ToString(), dr("Ma_Chuong").ToString(), dr("Ma_Khoan").ToString(), dr("Ma_TMuc").ToString(), dr("Ky_Thue").ToString())
                Else
                    v_dtDTL.Rows(i)(ColDTL.SoDT) = TCS_CalSoDaThu_TKhai(v_strMa_NNT, dr("Ma_Chuong").ToString(), dr("Ma_TMuc").ToString(), v_dtHDR.Rows(0)(ColHDR.ToKhaiSo).ToString)
                End If


                'CType(grdChiTiet.Items(i).Controls(clsGridCTU.ColDTL.Tien).Controls(1), TextBox).Text = VBOracleLib.Globals.Format_Number(dblTien.ToString, ".")
                'CType(grdChiTiet.Items(i).Controls(clsGridCTU.ColDTL.KyThue).Controls(1), TextBox).Text = dr("Ky_Thue").ToString()
                'CType(grdChiTiet.Items(i).Controls(clsGridCTU.ColDTL.SoDT).Controls(1), TextBox).Text = CalSoDaThu(v_strMa_NNT, dr("MaQuy").ToString(), dr("Ma_Chuong").ToString(), dr("Ma_Khoan").ToString(), dr("Ma_TMuc").ToString(), dr("Ky_Thue").ToString())
            Next

            'Cap nhat truong tong tien            
            v_dtHDR.Rows(0)(ColHDR.TTIEN) = VBOracleLib.Globals.Format_Number(dblTongTien, ".")

            Return TCS_BuildXML_FROM_TBL(v_dtHDR, v_dtDTL)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin chi tiết chứng từ", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source:" & ex.StackTrace & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function

    ''' <summary>
    ''' Lay thong tin ve tai khoan cua khach hang tai ngan hang(thong tin ca nhan,so du...)
    ''' </summary>
    ''' <param name="pv_strSoTKNH"></param>
    ''' <param name="pv_strAccType"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>


    Public Shared Function MappingErrorCode(ByVal errCode As String) As String
        Dim errMsg As String = ""
        Select Case (errCode)
            Case "1236" '236 == DATE VERIFICATION ERROR
                Return "DATE VERIFICATION ERROR"
            Case "2001" '1 == SO TK KHONG TIM THAY
                Return "ACCOUNT NOT EXIST"
            Case "1204"
                Return "SYSTEM ERROR...CONTACT DP STAFF"
            Case "1002"
                Return "TK DA BI PHONG TOA"
            Case "1001"
                Return "ACCOUNT NOT EXIST"
            Case "123"
                Return "TK DA BI DONG"
            Case "DSP003"
                Return "HE THONG DANG CHAY BATCH"
            Case ""
            Case ""
            Case ""
            Case ""
            Case Else
                Return "SYSTEM_ERROR"
        End Select
        Return errMsg
    End Function

    '''' <summary>
    '''' Lay thong tin cua nguoi nop thue trong db (trong so thue,dm to khai)
    '''' </summary>
    '''' <param name="strMaNNT"></param>
    '''' <returns></returns>
    '''' <remarks></remarks>
    Public Shared Function TCS_Check_NNThue(ByVal strMaNNT As String, ByVal strNgayLV As String) As String
        Try
            ProcMegService.ProMsg.getMSG05(strMaNNT)
            'GIPBankInterface.getTINData(strMaNNT)

            Dim nnt As New daNNThue
            If (nnt.Exists_SoThue_CTN(strMaNNT)) Then
                Return TCS_CheckSoThue_CTN(strMaNNT, strNgayLV)
            Else
                Return TCS_CheckDM_NNThue(strMaNNT, False, strNgayLV)
            End If
            Return String.Empty
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '    clsCommon.WriteLog(ex, "Lỗi check thông tin NNT", HttpContext.Current.Session.Item("TEN_DN"))
            Dim nnt As New daNNThue
            If (nnt.Exists_SoThue_CTN(strMaNNT)) Then
                Return TCS_CheckSoThue_CTN(strMaNNT, strNgayLV)
            Else
                Return TCS_CheckDM_NNThue(strMaNNT, False, strNgayLV)
            End If
            'Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function
    Public Shared Function TCS_Check_NNThue_NEW(ByVal strMaNNT As String, ByVal strNgayLV As String) As String
        Try
            ProcMegService.ProMsg.getMSG05(strMaNNT)
            'GIPBankInterface.getTINData(strMaNNT)

            Dim nnt As New daNNThue
            If (nnt.Exists_SoThue_CTN(strMaNNT)) Then
                Return TCS_CheckSoThue_CTN_NEW(strMaNNT, strNgayLV)
            Else
                Return TCS_CheckDM_NNThue_NEW(strMaNNT, False, strNgayLV)
            End If
            Return String.Empty
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi check thông tin NNT", HttpContext.Current.Session.Item("TEN_DN"))
            Dim nnt As New daNNThue
            If (nnt.Exists_SoThue_CTN(strMaNNT)) Then
                Return TCS_CheckSoThue_CTN_NEW(strMaNNT, strNgayLV)
            Else
                Return TCS_CheckDM_NNThue_NEW(strMaNNT, False, strNgayLV)
            End If
            'Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function
#End Region

#Region "HaiQuan"
    Public Shared Function BuildHQGridDetail(ByVal strXMLString As String) As String
        Try
            Dim v_dtHDR As DataTable
            Dim v_dtDTL As DataTable

            Dim v_xmlDocument As New System.Xml.XmlDocument
            v_xmlDocument.LoadXml(strXMLString)

            Dim v_strRetXML As String = String.Empty

            v_dtHDR = TCS_BuildCTU_HDR_TBL()

            v_dtHDR.Rows(0)(ColHDR.MaNNT) = v_xmlDocument.SelectNodes("/CUSTOMS/Data/Ma_DV").Item(0).ChildNodes(0).InnerText
            v_dtHDR.Rows(0)(ColHDR.TenNNT) = v_xmlDocument.SelectNodes("/CUSTOMS/Data/Ten_DV").Item(0).ChildNodes(0).InnerText
            v_dtHDR.Rows(0)(ColHDR.DescLHXNK) = v_xmlDocument.SelectNodes("/CUSTOMS/Data/Ten_LH").Item(0).ChildNodes(0).InnerText

            '2)Xay dung grid detail
            v_dtDTL = TCS_BuildCTU_DTL_TBL()

            Dim v_strMa_HTVCHH As String = v_xmlDocument.SelectNodes("/CUSTOMS/Data/Ma_HTVCHH").Item(0).ChildNodes(0).InnerText
            Dim v_dt As DataTable = TCS_MAP_SACTHUE_MLNS(v_strMa_HTVCHH)
            Dim i As Integer = 0

            For Each v_dr As DataRow In v_dt.Rows
                If CDbl(v_xmlDocument.SelectNodes("/CUSTOMS/Data/" & v_dr("xml_tag").ToString).Item(0).ChildNodes(0).InnerText) <> 0 Then
                    v_dtDTL.Rows(i)(ColDTL.MaQuy) = clsCTU.gc_MaQuyMacDinh
                    v_dtDTL.Rows(i)(ColDTL.CChuong) = v_dr("ma_chuong")
                    v_dtDTL.Rows(i)(ColDTL.LKhoan) = v_dr("ma_khoan")
                    v_dtDTL.Rows(i)(ColDTL.MTieuMuc) = v_dr("ma_tmuc")
                    v_dtDTL.Rows(i)(ColDTL.NoiDung) = v_dr("noi_dung")
                    v_dtDTL.Rows(i)(ColDTL.Tien) = CDbl(v_xmlDocument.SelectNodes("/CUSTOMS/Data/" & v_dr("xml_tag").ToString).Item(0).ChildNodes(0).InnerText).ToString
                    v_dtDTL.Rows(i)(ColDTL.SoDT) = "0"
                    v_dtDTL.Rows(i)(ColDTL.KyThue) = "" 'CTuCommon.GetKyThue(gdtmNgayLV)
                    i += 1
                End If
            Next
            Return TCS_BuildXML_FROM_TBL(v_dtHDR, v_dtDTL)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin chứng từ chi tiết", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function

    Public Shared Function TCS_MAP_SACTHUE_MLNS(ByVal strMaHTVCHH As String) As DataTable
        Try
            Dim strSQL As String = String.Empty
            strSQL = "select * from tcs_map_sacthue_mlns " & IIf(strMaHTVCHH.Length > 0, " where Ma_HTVCHH='" & strMaHTVCHH & "'", "") & " order by lstorder,ma_htvchh"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi TCS_MAP_SACTHUE_MLNS", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            Return Nothing
        End Try
    End Function

    'Public Shared Sub TCS_LogWSMessage(ByVal pv_strMessageCode As String, ByVal pv_strSoCT As String, ByVal pv_strOrgRefNo As String, ByVal pv_strRefNo As String, ByVal pv_strDestination As String, _
    '                    ByVal pv_strError_Number As String, ByVal pv_strError_Message As String, _
    '                    ByVal pv_strDateRequest As String, ByVal pv_strDateResponse As String, ByVal pv_strTranDTL As String, _
    '                    ByVal pv_strOrgRequestMsg As String, ByVal pv_strOrgResponseMsg As String)

    '    Dim strSQL As String = ""
    '    Dim conn As DataAccess
    '    Dim transCT As IDbTransaction
    '    Try
    '        conn = New DataAccess
    '        transCT = conn.BeginTransaction
    '        strSQL = "INSERT INTO TCS_MESSAGELOG(ID,MESSAGECODE,SO_CT,ORGREF,REF,DESTINATION,ERROR_NUMBER,ERROR_MESSAGE,DATEREQUEST,DATERESPONSE,TRAN_DTL,REQUEST,RESPONSE)VALUES" _
    '                    & "(TCS_MESSAGECODE_SEQ.NEXTVAL,'" & pv_strMessageCode & "','" & pv_strSoCT & "','" & pv_strOrgRefNo & "','" & pv_strRefNo & "','" & pv_strDestination & "','" _
    '                    & pv_strError_Number & "','" & pv_strError_Message & "'," & pv_strDateRequest & "," & pv_strDateResponse & ",'" & pv_strTranDTL & "','" & pv_strOrgRequestMsg & "','" & pv_strOrgResponseMsg & "')"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        transCT.Commit()
    '    Catch ex As Exception
    '        transCT.Rollback()
    '        Throw ex
    '    Finally
    '        ' Giải phóng Connection tới CSDL
    '        If Not transCT Then
    '            transCT.Dispose()
    '        End If
    '        If Not conn Then
    '            conn.Dispose()
    '        End If
    '    End Try
    'End Sub

    'Public Shared Sub TCS_LogWSTranMessage(ByVal pv_strMessageCode As String, ByVal pv_strSoCT As String, ByVal pv_strOrgRefNo As String, ByVal pv_strRefNo As String, ByVal pv_strDestination As String, _
    '                       ByVal pv_strError_Number As String, ByVal pv_strError_Message As String, _
    '                       ByVal pv_strDateRequest As String, ByVal pv_strDateResponse As String, ByVal pv_strTranDTL As String, _
    '                       ByVal pv_strOrgRequestMsg As String, ByVal pv_strOrgResponseMsg As String)

    '    Dim strSQL As String = ""
    '    Dim conn As DataAccess
    '    Dim transCT As IDbTransaction
    '    Try
    '        conn = New DataAccess
    '        transCT = conn.BeginTransaction
    '        strSQL = "INSERT INTO TCS_MgsLog_Hdr(txnum,txdate,tltxcd,so_ct,src_transaction_id,des_transaction_id,ERROR_NUMBER,ERROR_MESSAGE,time_request,time_response,msg_REQUEST,msg_RESPONSE) VALUES " _
    '                    & "(TCS_MESSAGECODE_SEQ.NEXTVAL,Trunc(sysdate),'" & pv_strMessageCode & "','" & pv_strSoCT & "','" & pv_strOrgRefNo & "','" & pv_strRefNo & "','" _
    '                    & pv_strError_Number & "','" & pv_strError_Message & "'," & pv_strDateRequest & "," & pv_strDateResponse & ",'" & pv_strOrgRequestMsg & "','" & pv_strOrgResponseMsg & "')"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        'strSQL = "INSERT INTO TCS_MgsLog_dtl(txnum,txdate,tltxcd,rors,message) VALUES " _
    '        '           & "(TCS_MESSAGECODE_SEQ.NEXTVAL,Trunc(sysdate),'" & pv_strMessageCode & "','" & pv_strSoCT & "','" & pv_strOrgRefNo & "','" & pv_strRefNo & "','" _
    '        '           & pv_strError_Number & "','" & pv_strError_Message & "'," & pv_strDateRequest & "," & pv_strDateResponse & ",'" & pv_strOrgRequestMsg & "','" & pv_strOrgResponseMsg & "')"
    '        'conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        transCT.Commit()
    '    Catch ex As Exception
    '        transCT.Rollback()
    '        Throw ex
    '    Finally
    '        ' Giải phóng Connection tới CSDL
    '        If Not transCT Then
    '            transCT.Dispose()
    '        End If
    '        If Not conn Then
    '            conn.Dispose()
    '        End If
    '    End Try
    'End Sub

    'Public Shared Sub TCS_LogWSCompareMessage(ByVal pv_strMessageCode As String, ByVal pv_strDateRequest As String, _
    '                                          ByVal pv_strDateResponse As String, ByVal pv_strMessage As String)

    '    Dim strSQL As String = ""
    '    Dim conn As DataAccess
    '    Dim transCT As IDbTransaction
    '    Try
    '        conn = New DataAccess
    '        transCT = conn.BeginTransaction
    '        Dim v_xmlDocument As New System.Xml.XmlDocument
    '        v_xmlDocument.LoadXml(pv_strMessage)

    '        For Each v_node As XmlNode In v_xmlDocument.SelectNodes("/CUSTOMS/Data").Item(0).ChildNodes
    '            Dim v_strTranDTL As String = v_xmlDocument.SelectNodes("/CUSTOMS/Data").Item(0).ChildNodes.Item(0).InnerXml
    '            Dim v_strSo_CT As String = v_xmlDocument.SelectNodes("/CUSTOMS/Data/Transaction/So_CT").Item(0).ChildNodes(0).InnerText
    '            Dim v_strOrgREF As String = v_xmlDocument.SelectNodes("/CUSTOMS/Data/Transaction/Transaction_ID").Item(0).ChildNodes(0).InnerText
    '            Dim v_strError_Number As String = v_xmlDocument.SelectNodes("/CUSTOMS/Error/Error_Number").Item(0).ChildNodes(0).InnerText
    '            Dim v_strErrorMessage As String = v_xmlDocument.SelectNodes("/CUSTOMS/Error/Error_Message").Item(0).ChildNodes(0).InnerText

    '            strSQL = "INSERT INTO TCS_CompareLog(ID,MESSAGECODE,SO_CT,ORGREF,DESTINATION,ERROR_NUMBER,ERROR_MESSAGE,DATEREQUEST,DATERESPONSE,TRAN_DTL)VALUES" _
    '                        & "(TCS_MESSAGECODE_SEQ.NEXTVAL,'" & pv_strMessageCode & "','" & v_strSo_CT & "','" & v_strOrgREF & "','HQ','" & v_strError_Number & "','" & v_strErrorMessage & "'," & pv_strDateRequest & "," & pv_strDateResponse & ",'" & v_strTranDTL & "')"
    '            conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)
    '        Next
    '        transCT.Commit()
    '    Catch ex As Exception
    '        transCT.Rollback()
    '        Throw ex
    '    Finally
    '        ' Giải phóng Connection tới CSDL
    '        If Not transCT Then
    '            transCT.Dispose()
    '        End If
    '        If Not conn Then
    '            conn.Dispose()
    '        End If
    '    End Try
    'End Sub

    'Public Shared Sub TCS_LogMessageM11(ByVal v_strError_Number As String, ByVal v_strError_Message As String, _
    '                                ByVal v_strDateRequest As String, ByVal v_strDateResponse As String, _
    '                                ByVal v_strOrgRequestMsg11 As String, ByVal v_strOrgResponseMsg11 As String)
    '    Dim conn As DataAccess
    '    Dim transCT As IDbTransaction
    '    Try
    '        conn = New DataAccess
    '        transCT = conn.BeginTransaction

    '        Dim v_xmlDocument As New System.Xml.XmlDocument
    '        v_xmlDocument.LoadXml(v_strOrgResponseMsg11)
    '        Dim v_strDes_Transaction_Type As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_Type").Item(0).FirstChild.Value
    '        Dim v_strDes_Transaction_ID As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_ID").Item(0).FirstChild.Value

    '        v_xmlDocument.LoadXml(v_strOrgRequestMsg11)
    '        Dim v_strSrc_Transaction_ID As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_ID").Item(0).FirstChild.Value
    '        Dim v_strSrc_Transaction_Type As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_Type").Item(0).FirstChild.Value
    '        Dim v_strRequestMsg As String = gc_XML_HEADER_TAG & "<CUSTOMS>" & v_xmlDocument.SelectNodes("/CUSTOMS/Header").Item(0).InnerXml & "$" _
    '                    & v_xmlDocument.SelectNodes("/CUSTOMS/Security").Item(0).InnerXml & "</CUSTOMS>"
    '        Dim v_strRequestDataMsg As String = v_xmlDocument.SelectNodes("/CUSTOMS/Data").Item(0).OuterXml


    '        Dim strSQL As String = String.Empty

    '        Dim v_strTXNUM, v_strTXDATE As String
    '        strSQL = "SELECT TCS_MESSAGECODE_SEQ.NEXTVAL FROM DUAL"
    '        v_strTXNUM = conn.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString

    '        strSQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL"
    '        v_strTXDATE = conn.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString

    '        '1)Log message hdr
    '        strSQL = "INSERT INTO tcs_msglog_hdr(TXNUM,TXDATE,TLTXCD,SO_CT,SRC_TRANSACTION_ID,DES_TRANSACTION_ID,ERROR_NUMBER,ERROR_MESSAGE,TIME_REQUEST,TIME_RESPONSE,MSG_REQUEST,MSG_RESPONSE) VALUES " _
    '                 & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strSrc_Transaction_Type & "',NULL,'" & v_strSrc_Transaction_ID & "','" & v_strDes_Transaction_ID & "','" _
    '                 & v_strError_Number & "','" & v_strError_Message & "'," & v_strDateRequest & "," & v_strDateResponse & ",'" & v_strRequestMsg & "','" & v_strOrgResponseMsg11 & "')"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        '2)Log message dtl
    '        strSQL = "INSERT INTO TCS_MsgLOG_DTL(TXNUM,TXDATE,TLTXCD,RORS,MESSAGE) VALUES " _
    '                   & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strSrc_Transaction_Type & "','S','" & v_strRequestDataMsg & "')"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        strSQL = "INSERT INTO TCS_MsgLOG_DTL(TXNUM,TXDATE,TLTXCD,RORS,MESSAGE) VALUES " _
    '                   & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strDes_Transaction_Type & "','R','$')"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        transCT.Commit()
    '    Catch ex As Exception
    '        transCT.Rollback()
    '        Throw ex
    '    Finally
    '        ' Giải phóng Connection tới CSDL
    '        If Not transCT Then
    '            transCT.Dispose()
    '        End If
    '        If Not conn Then
    '            conn.Dispose()
    '        End If
    '    End Try
    'End Sub

    'Public Shared Sub TCS_LogMessageM21(ByVal v_strError_Number As String, ByVal v_strError_Message As String, _
    '                                    ByVal v_strDateRequest As String, ByVal v_strDateResponse As String, _
    '                                    ByVal v_strOrgRequestMsg21 As String, ByVal v_strOrgResponseMsg21 As String)
    '    Dim conn As DataAccess
    '    Dim transCT As IDbTransaction
    '    Try
    '        conn = New DataAccess
    '        transCT = conn.BeginTransaction

    '        Dim v_xmlDocument As New System.Xml.XmlDocument
    '        v_xmlDocument.LoadXml(v_strOrgResponseMsg21)
    '        Dim v_strDes_Transaction_Type As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_Type").Item(0).FirstChild.Value
    '        Dim v_strDes_Transaction_ID As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_ID").Item(0).FirstChild.Value

    '        v_xmlDocument.LoadXml(v_strOrgRequestMsg21)
    '        Dim v_strSrc_Transaction_ID As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_ID").Item(0).FirstChild.Value
    '        Dim v_strSrc_Transaction_Type As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_Type").Item(0).FirstChild.Value
    '        Dim v_strSrc_So_CT As String = v_xmlDocument.SelectNodes("/CUSTOMS/Data/So_CT").Item(0).FirstChild.Value
    '        Dim v_strRequestMsg As String = gc_XML_HEADER_TAG & "<CUSTOMS>" & v_xmlDocument.SelectNodes("/CUSTOMS/Header").Item(0).InnerXml & "$" _
    '                    & v_xmlDocument.SelectNodes("/CUSTOMS/Security").Item(0).InnerXml & "</CUSTOMS>"
    '        Dim v_strRequestDataMsg As String = v_xmlDocument.SelectNodes("/CUSTOMS/Data").Item(0).OuterXml


    '        Dim strSQL As String = String.Empty

    '        Dim v_strTXNUM, v_strTXDATE As String
    '        strSQL = "SELECT TCS_MESSAGECODE_SEQ.NEXTVAL FROM DUAL"
    '        v_strTXNUM = conn.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString

    '        strSQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL"
    '        v_strTXDATE = conn.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString

    '        '1)Log message hdr
    '        strSQL = "INSERT INTO tcs_msglog_hdr(TXNUM,TXDATE,TLTXCD,SO_CT,SRC_TRANSACTION_ID,DES_TRANSACTION_ID,ERROR_NUMBER,ERROR_MESSAGE,TIME_REQUEST,TIME_RESPONSE,MSG_REQUEST,MSG_RESPONSE) VALUES " _
    '                 & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strSrc_Transaction_Type & "','" & v_strSrc_So_CT & "','" & v_strSrc_Transaction_ID & "','" & v_strDes_Transaction_ID & "','" _
    '                 & v_strError_Number & "','" & v_strError_Message & "'," & v_strDateRequest & "," & v_strDateResponse & ",'" & v_strRequestMsg & "','" & v_strOrgResponseMsg21 & "')"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        '2)Log message dtl
    '        strSQL = "INSERT INTO TCS_MsgLOG_DTL(TXNUM,TXDATE,TLTXCD,RORS,MESSAGE) VALUES " _
    '                   & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strSrc_Transaction_Type & "','S','" & v_strRequestDataMsg & "')"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        strSQL = "INSERT INTO TCS_MsgLOG_DTL(TXNUM,TXDATE,TLTXCD,RORS,MESSAGE) VALUES " _
    '                   & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strDes_Transaction_Type & "','R','$')"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)


    '        '3)Log message temp
    '        Dim v_strColumnList As String = String.Empty
    '        Dim v_strValueList As String = String.Empty
    '        For Each v_node As XmlNode In v_xmlDocument.SelectNodes("/CUSTOMS/Data").Item(0).ChildNodes
    '            v_strColumnList &= v_node.Name & ","
    '            If Not v_node.FirstChild Then
    '                v_strValueList &= "'" & v_node.FirstChild.Value.Trim & "',"
    '            Else
    '                v_strValueList &= "NULL,"
    '            End If
    '        Next
    '        strSQL = "INSERT INTO TCS_NH_MESSAGE_TMP"
    '        strSQL &= "(TXNUM,TXDATE,TRANSACTION_ID," & v_strColumnList.Remove(v_strColumnList.LastIndexOf(",")) & ")"
    '        strSQL &= " VALUES ('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strSrc_Transaction_ID & "'," & v_strValueList.Remove(v_strValueList.LastIndexOf(",")) & ")"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        transCT.Commit()
    '    Catch ex As Exception
    '        transCT.Rollback()
    '        Throw ex
    '    Finally
    '        ' Giải phóng Connection tới CSDL
    '        If Not transCT Then
    '            transCT.Dispose()
    '        End If
    '        If Not conn Then
    '            conn.Dispose()
    '        End If
    '    End Try
    'End Sub

    'Public Shared Sub TCS_LogMessageM31(ByVal v_strError_Number As String, ByVal v_strError_Message As String, _
    '                                   ByVal v_strDateRequest As String, ByVal v_strDateResponse As String, _
    '                                   ByVal v_strOrgRequestMsg31 As String, ByVal v_strOrgResponseMsg31 As String)
    '    Dim conn As DataAccess
    '    Dim transCT As IDbTransaction
    '    Try
    '        conn = New DataAccess
    '        transCT = conn.BeginTransaction

    '        Dim v_xmlDocument As New System.Xml.XmlDocument
    '        v_xmlDocument.LoadXml(v_strOrgResponseMsg31)
    '        Dim v_strDes_Transaction_Type As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_Type").Item(0).FirstChild.Value
    '        Dim v_strDes_Transaction_ID As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_ID").Item(0).FirstChild.Value

    '        v_xmlDocument.LoadXml(v_strOrgRequestMsg31)
    '        Dim v_strSrc_Transaction_ID As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_ID").Item(0).FirstChild.Value
    '        Dim v_strSrc_Transaction_Type As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_Type").Item(0).FirstChild.Value
    '        Dim v_strSrc_So_CT As String = v_xmlDocument.SelectNodes("/CUSTOMS/Data/So_CT").Item(0).FirstChild.Value
    '        Dim v_strRequestMsg As String = gc_XML_HEADER_TAG & "<CUSTOMS>" & v_xmlDocument.SelectNodes("/CUSTOMS/Header").Item(0).InnerXml & "$" _
    '                    & v_xmlDocument.SelectNodes("/CUSTOMS/Security").Item(0).InnerXml & "</CUSTOMS>"
    '        Dim v_strRequestDataMsg As String = v_xmlDocument.SelectNodes("/CUSTOMS/Data").Item(0).OuterXml


    '        Dim strSQL As String = String.Empty

    '        Dim v_strTXNUM, v_strTXDATE As String
    '        strSQL = "SELECT TCS_MESSAGECODE_SEQ.NEXTVAL FROM DUAL"
    '        v_strTXNUM = conn.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString

    '        strSQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL"
    '        v_strTXDATE = conn.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString

    '        '1)Log message hdr
    '        strSQL = "INSERT INTO tcs_msglog_hdr(TXNUM,TXDATE,TLTXCD,SO_CT,SRC_TRANSACTION_ID,DES_TRANSACTION_ID,ERROR_NUMBER,ERROR_MESSAGE,TIME_REQUEST,TIME_RESPONSE,MSG_REQUEST,MSG_RESPONSE) VALUES " _
    '                 & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strSrc_Transaction_Type & "','" & v_strSrc_So_CT & "','" & v_strSrc_Transaction_ID & "','" & v_strDes_Transaction_ID & "','" _
    '                 & v_strError_Number & "','" & v_strError_Message & "'," & v_strDateRequest & "," & v_strDateResponse & ",'" & v_strRequestMsg & "','" & v_strOrgResponseMsg31 & "')"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        '2)Log message dtl
    '        strSQL = "INSERT INTO TCS_MsgLOG_DTL(TXNUM,TXDATE,TLTXCD,RORS,MESSAGE) VALUES " _
    '                   & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strSrc_Transaction_Type & "','S','" & v_strRequestDataMsg & "')"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        strSQL = "INSERT INTO TCS_MsgLOG_DTL(TXNUM,TXDATE,TLTXCD,RORS,MESSAGE) VALUES " _
    '                   & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strDes_Transaction_Type & "','R','$')"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        'Lay chung tu
    '        strSQL = "INSERT INTO tcs_nh_message_tmp " _
    '            & " (txnum, txdate, ma_nh_ph, ma_dv, ten_dv, ma_hq_ph, ma_hq, ma_lh," _
    '            & " sotk, ngay_dk, ma_ntk, kyhieu_ct, so_ct, ttbuttoan, ma_kb, " _
    '            & " ten_kb, tkkb, ngay_bn, ngay_bc, ngay_ct, diengiai, chuong_xk, " _
    '            & " khoan_xk, tieumuc_xk, thue_xk, chuong_nk, khoan_nk, tieumuc_nk, " _
    '            & " thue_nk, chuong_va, khoan_va, tieumuc_va, thue_va, chuong_td, " _
    '            & " khoan_td, tieumuc_td, thue_td, chuong_tv, khoan_tv, tieumuc_tv, " _
    '            & " thue_tv, chuong_ph, khoan_ph, tieumuc_ph, thue_ph, chuong_lp, " _
    '            & " khoan_lp, tieumuc_lp, thue_lp, chuong_kh, khoan_kh, tieumuc_kh, " _
    '            & " thue_kh, transaction_id)" _
    '            & " SELECT hdr.txnum, hdr.txdate, tmp.ma_nh_ph, tmp.ma_dv, tmp.ten_dv," _
    '            & " tmp.ma_hq_ph, tmp.ma_hq, tmp.ma_lh, tmp.sotk, tmp.ngay_dk," _
    '            & " tmp.ma_ntk, tmp.kyhieu_ct, tmp.so_ct, tmp.ttbuttoan, tmp.ma_kb," _
    '            & " tmp.ten_kb, tmp.tkkb, tmp.ngay_bn, tmp.ngay_bc, tmp.ngay_ct," _
    '            & " tmp.diengiai, tmp.chuong_xk, tmp.khoan_xk, tmp.tieumuc_xk," _
    '            & " tmp.thue_xk, tmp.chuong_nk, tmp.khoan_nk, tmp.tieumuc_nk," _
    '            & " tmp.thue_nk, tmp.chuong_va, tmp.khoan_va, tmp.tieumuc_va," _
    '            & " tmp.thue_va, tmp.chuong_td, tmp.khoan_td, tmp.tieumuc_td," _
    '            & " tmp.thue_td, tmp.chuong_tv, tmp.khoan_tv, tmp.tieumuc_tv," _
    '            & " tmp.thue_tv, tmp.chuong_ph, tmp.khoan_ph, tmp.tieumuc_ph," _
    '            & " tmp.thue_ph, tmp.chuong_lp, tmp.khoan_lp, tmp.tieumuc_lp," _
    '            & " tmp.thue_lp, tmp.chuong_kh, tmp.khoan_kh, tmp.tieumuc_kh," _
    '            & " tmp.thue_kh, hdr.src_transaction_id FROM tcs_nh_message_tmp tmp," _
    '            & " (SELECT txnum, txdate, so_ct, src_transaction_id" _
    '            & " FROM tcs_msglog_hdr WHERE tltxcd = '31' AND so_ct = '" & v_strSrc_So_CT & "') hdr WHERE (tmp.txnum, tmp.txdate) =" _
    '            & " (SELECT txnum, txdate FROM tcs_msglog_hdr WHERE so_ct = '" & v_strSrc_So_CT & "' AND tltxcd = '21' AND error_number='0') AND tmp.so_ct = tmp.so_ct"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        transCT.Commit()
    '    Catch ex As Exception
    '        transCT.Rollback()
    '        Throw ex
    '    Finally
    '        ' Giải phóng Connection tới CSDL
    '        If Not transCT Then
    '            transCT.Dispose()
    '        End If
    '        If Not conn Then
    '            conn.Dispose()
    '        End If
    '    End Try
    'End Sub

    'Public Shared Sub TCS_LogMessageM41(ByVal v_strError_Number As String, ByVal v_strError_Message As String, _
    '                                ByVal v_strDateRequest As String, ByVal v_strDateResponse As String, _
    '                                ByVal v_strOrgRequestMsg41 As String, ByVal v_strOrgResponseMsg41 As String)

    '    Dim conn As DataAccess
    '    Dim transCT As IDbTransaction
    '    Try
    '        conn = New DataAccess
    '        transCT = conn.BeginTransaction
    '        Dim v_xmlDocument As New System.Xml.XmlDocument

    '        'Doc thong tin tu message goc
    '        v_xmlDocument.LoadXml(v_strOrgRequestMsg41)
    '        Dim v_strSrc_Transaction_ID As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_ID").Item(0).FirstChild.Value
    '        Dim v_strSrc_Transaction_Type As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_Type").Item(0).FirstChild.Value
    '        Dim v_strRequestMsg As String = gc_XML_HEADER_TAG & "<CUSTOMS>" & v_xmlDocument.SelectNodes("/CUSTOMS/Header").Item(0).InnerXml & "$" _
    '                    & v_xmlDocument.SelectNodes("/CUSTOMS/Security").Item(0).InnerXml & "</CUSTOMS>"
    '        Dim v_strRequestDataMsg As String = v_xmlDocument.SelectNodes("/CUSTOMS/Data").Item(0).OuterXml

    '        'Doc thong tin tu message nhan ve
    '        v_xmlDocument.LoadXml(v_strOrgResponseMsg41)
    '        Dim v_strDes_Transaction_Type As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_Type").Item(0).FirstChild.Value
    '        Dim v_strDes_Transaction_ID As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_ID").Item(0).FirstChild.Value
    '        Dim v_strResponseMsg As String = gc_XML_HEADER_TAG & "<CUSTOMS>" & v_xmlDocument.SelectNodes("/CUSTOMS/Header").Item(0).InnerXml & "<Data>$</Data>" _
    '                    & v_xmlDocument.SelectNodes("/CUSTOMS/Security").Item(0).InnerXml & "</CUSTOMS>"


    '        Dim strSQL As String = String.Empty

    '        Dim v_strTXNUM, v_strTXDATE As String
    '        strSQL = "SELECT TCS_MESSAGECODE_SEQ.NEXTVAL FROM DUAL"
    '        v_strTXNUM = conn.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString

    '        strSQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL"
    '        v_strTXDATE = conn.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString

    '        '1)Log message hdr
    '        strSQL = "INSERT INTO tcs_msglog_hdr(TXNUM,TXDATE,TLTXCD,SO_CT,SRC_TRANSACTION_ID,DES_TRANSACTION_ID,ERROR_NUMBER,ERROR_MESSAGE,TIME_REQUEST,TIME_RESPONSE,MSG_REQUEST,MSG_RESPONSE) VALUES " _
    '                 & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strSrc_Transaction_Type & "','','" & v_strSrc_Transaction_ID & "','" & v_strDes_Transaction_ID & "','" _
    '                 & v_strError_Number & "','" & v_strError_Message & "'," & v_strDateRequest & "," & v_strDateResponse & ",'" & v_strRequestMsg & "','" & v_strResponseMsg & "')"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        '2)Log message dtl message gui di
    '        strSQL = "INSERT INTO TCS_MsgLOG_DTL(TXNUM,TXDATE,TLTXCD,RORS,MESSAGE) VALUES " _
    '                   & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strSrc_Transaction_Type & "','S','" & v_strRequestDataMsg & "')"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)


    '        'xoa message doi chieu trong ngay
    '        strSQL = "delete tcs_hq_message_tmp where txdate=to_date('" & v_strTXDATE & "','dd/mm/yyyy') AND TLTXCD='42'"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        For i As Integer = 0 To v_xmlDocument.SelectNodes("/CUSTOMS/Data").Item(0).ChildNodes.Count - 1
    '            Dim v_strColumnList As String = String.Empty
    '            Dim v_strValueList As String = String.Empty
    '            For Each v_node As XmlNode In v_xmlDocument.SelectNodes("/CUSTOMS/Data").Item(0).ChildNodes.Item(i).ChildNodes
    '                v_strColumnList &= v_node.Name & ","
    '                If Not v_node.FirstChild Then
    '                    v_strValueList &= "'" & v_node.FirstChild.Value.Trim & "',"
    '                Else
    '                    v_strValueList &= "NULL,"
    '                End If
    '            Next
    '            Dim v_strTransDetail As String = v_xmlDocument.SelectNodes("/CUSTOMS/Data/Transaction").Item(0).OuterXml
    '            strSQL = "INSERT INTO TCS_MsgLOG_DTL(TXNUM,TXDATE,TLTXCD,RORS,MESSAGE) VALUES " _
    '                   & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strDes_Transaction_Type & "','R','" & v_strTransDetail & "')"
    '            conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '            strSQL = "insert into tcs_hq_message_tmp"
    '            strSQL &= "(txnum,txdate,tltxcd," & v_strColumnList.Remove(v_strColumnList.LastIndexOf(",")) & ")"
    '            strSQL &= " values ('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'42'," & v_strValueList.Remove(v_strValueList.LastIndexOf(",")) & ")"
    '            conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)
    '        Next

    '        transCT.Commit()
    '    Catch ex As Exception
    '        transCT.Rollback()
    '        Throw ex
    '    Finally
    '        ' Giải phóng Connection tới CSDL
    '        If Not transCT Then
    '            transCT.Dispose()
    '        End If
    '        If Not conn Then
    '            conn.Dispose()
    '        End If
    '    End Try
    'End Sub

    'Public Shared Sub TCS_LogMessageM51(ByVal v_strError_Number As String, ByVal v_strError_Message As String, _
    '                                    ByVal v_strDateRequest As String, ByVal v_strDateResponse As String, _
    '                                    ByVal v_strOrgRequestMsg51 As String, ByVal v_strOrgResponseMsg51 As String)

    '    Dim conn As DataAccess
    '    Dim transCT As IDbTransaction
    '    Try
    '        conn = New DataAccess
    '        transCT = conn.BeginTransaction
    '        Dim v_xmlDocument As New System.Xml.XmlDocument

    '        'Doc thong tin tu message goc
    '        v_xmlDocument.LoadXml(v_strOrgRequestMsg51)
    '        Dim v_strSrc_Transaction_ID As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_ID").Item(0).FirstChild.Value
    '        Dim v_strSrc_Transaction_Type As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_Type").Item(0).FirstChild.Value
    '        Dim v_strRequestMsg As String = gc_XML_HEADER_TAG & "<CUSTOMS>" & v_xmlDocument.SelectNodes("/CUSTOMS/Header").Item(0).InnerXml & "$" _
    '                    & v_xmlDocument.SelectNodes("/CUSTOMS/Security").Item(0).InnerXml & "</CUSTOMS>"
    '        Dim v_strRequestDataMsg As String = v_xmlDocument.SelectNodes("/CUSTOMS/Data").Item(0).OuterXml

    '        'Doc thong tin tu message nhan ve
    '        v_xmlDocument.LoadXml(v_strOrgResponseMsg51)
    '        Dim v_strDes_Transaction_Type As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_Type").Item(0).FirstChild.Value
    '        Dim v_strDes_Transaction_ID As String = v_xmlDocument.SelectNodes("/CUSTOMS/Header/Transaction_ID").Item(0).FirstChild.Value
    '        Dim v_strResponseMsg As String = gc_XML_HEADER_TAG & "<CUSTOMS>" & v_xmlDocument.SelectNodes("/CUSTOMS/Header").Item(0).InnerXml & "<Data><Accept_Transactions>$</Accept_Transactions><Reject_Transactions>$</Reject_Transactions></Data>" _
    '                    & v_xmlDocument.SelectNodes("/CUSTOMS/Security").Item(0).InnerXml & "</CUSTOMS>"


    '        Dim strSQL As String = String.Empty

    '        Dim v_strTXNUM, v_strTXDATE As String
    '        strSQL = "SELECT TCS_MESSAGECODE_SEQ.NEXTVAL FROM DUAL"
    '        v_strTXNUM = conn.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString

    '        strSQL = "SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY') FROM DUAL"
    '        v_strTXDATE = conn.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString

    '        '1)Log message hdr
    '        strSQL = "INSERT INTO tcs_msglog_hdr(TXNUM,TXDATE,TLTXCD,SO_CT,SRC_TRANSACTION_ID,DES_TRANSACTION_ID,ERROR_NUMBER,ERROR_MESSAGE,TIME_REQUEST,TIME_RESPONSE,MSG_REQUEST,MSG_RESPONSE) VALUES " _
    '                 & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strSrc_Transaction_Type & "','','" & v_strSrc_Transaction_ID & "','" & v_strDes_Transaction_ID & "','" _
    '                 & v_strError_Number & "','" & v_strError_Message & "'," & v_strDateRequest & "," & v_strDateResponse & ",'" & v_strRequestMsg & "','" & v_strResponseMsg & "')"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        '2)Log message dtl
    '        strSQL = "INSERT INTO TCS_MsgLOG_DTL(TXNUM,TXDATE,TLTXCD,RORS,MESSAGE) VALUES " _
    '                   & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strSrc_Transaction_Type & "','S','" & v_strRequestDataMsg & "')"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        strSQL = "delete tcs_hq_message_tmp where txdate=to_date('" & v_strTXDATE & "','yyyy-mm-dd') AND TLTXCD='52'"
    '        conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '        'Log Accept_Transactions
    '        For i As Integer = 0 To v_xmlDocument.SelectNodes("/CUSTOMS/Data/Accept_Transactions").Item(0).ChildNodes.Count - 1
    '            Dim v_strColumnList As String = String.Empty
    '            Dim v_strValueList As String = String.Empty
    '            For Each v_node As XmlNode In v_xmlDocument.SelectNodes("/CUSTOMS/Data/Accept_Transactions").Item(0).ChildNodes.Item(i).ChildNodes
    '                v_strColumnList &= v_node.Name & ","
    '                If Not v_node.FirstChild Then
    '                    v_strValueList &= "'" & v_node.FirstChild.Value.Trim & "',"
    '                Else
    '                    v_strValueList &= "NULL,"
    '                End If
    '            Next
    '            Dim v_strTransDetailAccept As String = v_xmlDocument.SelectNodes("/CUSTOMS/Data/Accept_Transactions/Transaction").Item(0).OuterXml
    '            strSQL = "INSERT INTO TCS_MsgLOG_DTL(TXNUM,TXDATE,TLTXCD,RORS,MESSAGE) VALUES " _
    '                   & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strDes_Transaction_Type & "','R','" & v_strTransDetailAccept & "')"
    '            conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)



    '            strSQL = "insert into tcs_hq_message_tmp"
    '            strSQL &= "(txnum,txdate,tltxcd," & v_strColumnList.Remove(v_strColumnList.LastIndexOf(",")) & ")"
    '            strSQL &= " values ('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'52'," & v_strValueList.Remove(v_strValueList.LastIndexOf(",")) & ")"
    '            conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)
    '        Next

    '        'Log Reject_Transactions
    '        For i As Integer = 0 To v_xmlDocument.SelectNodes("/CUSTOMS/Data/Reject_Transactions").Item(0).ChildNodes.Count - 1
    '            Dim v_strColumnList As String = String.Empty
    '            Dim v_strValueList As String = String.Empty
    '            For Each v_node As XmlNode In v_xmlDocument.SelectNodes("/CUSTOMS/Data/Reject_Transactions").Item(0).ChildNodes.Item(i).ChildNodes
    '                v_strColumnList &= v_node.Name & ","
    '                If Not v_node.FirstChild Then
    '                    v_strValueList &= "'" & v_node.FirstChild.Value.Trim & "',"
    '                Else
    '                    v_strValueList &= "NULL,"
    '                End If
    '            Next
    '            Dim v_strTransDetailReject As String = v_xmlDocument.SelectNodes("/CUSTOMS/Data/Reject_Transactions/Transaction").Item(0).OuterXml
    '            strSQL = "INSERT INTO TCS_MsgLOG_DTL(TXNUM,TXDATE,TLTXCD,RORS,MESSAGE) VALUES " _
    '                   & "('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'" & v_strDes_Transaction_Type & "','R','" & v_strTransDetailReject & "')"
    '            conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)

    '            strSQL = "insert into tcs_hq_message_tmp"
    '            strSQL &= "(txnum,txdate,tltxcd," & v_strColumnList.Remove(v_strColumnList.LastIndexOf(",")) & ")"
    '            strSQL &= " values ('" & v_strTXNUM & "',TO_DATE('" & v_strTXDATE & "','DD/MM/YYYY'),'52'," & v_strValueList.Remove(v_strValueList.LastIndexOf(",")) & ")"
    '            conn.ExecuteNonQuery(strSQL, CommandType.Text, transCT)
    '        Next

    '        transCT.Commit()
    '    Catch ex As Exception
    '        transCT.Rollback()
    '        Throw ex
    '    Finally
    '        ' Giải phóng Connection tới CSDL
    '        If Not transCT Then
    '            transCT.Dispose()
    '        End If
    '        If Not conn Then
    '            conn.Dispose()
    '        End If
    '    End Try
    'End Sub

#End Region

#Region "Check NNT"

    Private Shared Function TCS_CheckDM_NNThue(ByVal strMaNNT As String, ByVal fblnVangLai As Boolean, ByVal strNgayLV As String) As String
        Try
            Dim v_dtHDR As DataTable
            Dim v_dtDTL As DataTable

            Dim v_strRetXML As String = String.Empty

            Dim dsNNT As DataSet

            If (Not fblnVangLai) Then
                dsNNT = ChungTu.buNNThue.NNThue(strMaNNT)
            Else
                dsNNT = ChungTu.buNNThue.NNT_VL(strMaNNT)
            End If

            If (IsEmptyDataSet(dsNNT)) Then
                Return String.Empty
            End If

            mNguonNNT = NguonNNT.NNThue_CTN

            v_dtHDR = TCS_BuildCTU_HDR_TBL()

            If (Not fblnVangLai) Then
                mNguonNNT = NguonNNT.NNThue_CTN
            Else
                mNguonNNT = NguonNNT.NNThue_VL
            End If

            Dim r As DataRow = dsNNT.Tables(0).Rows(0)
            If GetString(r("shkb")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.SHKB) = GetString(r("shkb"))
                v_dtHDR.Rows(0)(ColHDR.TenKB) = Get_TenKhoBac(r("shkb"))
            End If
            v_dtHDR.Rows(0)(ColHDR.TenNNT) = GetString(r("ten_nnt"))
            v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))

            If GetString(r("ma_xa")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaDBHC) = GetString(r("ma_xa"))
                v_dtHDR.Rows(0)(ColHDR.TenDBHC) = Get_TenDBHC(r("ma_xa"))
            End If
            v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))
            If GetString(r("ma_cqthu")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaCQThu) = GetString(r("ma_cqthu"))
                v_dtHDR.Rows(0)(ColHDR.TenCQThu) = Get_TenCQThu(GetString(r("ma_cqthu")))
            End If
            Dim strSoLanLap_CT As String = ChungTu.buChungTu.CTU_Check_Exist_NNT(strMaNNT, ConvertDateToNumber(strNgayLV))
            v_dtHDR.Rows(0)(ColHDR.TT_TTHU) = strSoLanLap_CT
            'Chi dien thong tin khi tim thay ban ghi,con khong se dien thong tin mac dinh
            'If GetString(r("ma_cqthu")).Length > 0 Then
            '    CType(grdHeader.Rows(ColHDR.MaCQThu).Cells.Item(1).Controls(0), TextBox).Text = GetString(r("ma_cqthu"))
            '    CType(grdHeader.Rows(ColHDR.MaCQThu).Cells.Item(2).Controls(0), TextBox).Text = Get_TenCQThu(GetString(r("ma_cqthu")))
            'End If

            'sonmt
            If GetString(r("ma_huyen")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MA_HUYEN) = GetString(r("ma_huyen"))
                v_dtHDR.Rows(0)(ColHDR.TEN_HUYEN) = Get_TenDBHC(r("ma_huyen"))
            End If
            If GetString(r("ma_tinh")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MA_TINH) = GetString(r("ma_tinh"))
                v_dtHDR.Rows(0)(ColHDR.TEN_TINH) = Get_TenDBHC(r("ma_tinh"))
            End If

            'sonmt
            If GetString(r("LOAI_NNT")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.LOAI_NNT) = GetString(r("LOAI_NNT"))
            End If
            If GetString(r("TEN_LOAI_NNT")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.TEN_LOAI_NNT) = GetString(r("TEN_LOAI_NNT"))
            End If

            v_dtDTL = TCS_BuildCTU_DTL_TBL()

            v_dtDTL.Rows(0)(ColDTL.MaQuy) = clsCTU.gc_MaQuyMacDinh
            v_dtDTL.Rows(0)(ColDTL.CChuong) = GetString(r("ma_chuong"))
            v_dtDTL.Rows(0)(ColDTL.LKhoan) = GetString(r("ma_khoan"))
            v_dtDTL.Rows(0)(ColDTL.KyThue) = CTuCommon.GetKyThue(ConvertDateToNumber(strNgayLV))

            'v_dtDTL.Rows(0)(ColDTL.SoDT) = TCS_CalSoDaThu(strMaNNT, _
            '    clsCTU.gc_MaQuyMacDinh, GetString(r("ma_chuong")), GetString(r("ma_khoan")), r("Ma_TMuc").ToString(), _
            '             v_dtDTL.Rows(0)(ColDTL.KyThue))

            'txtTongTien.Text = Globals.Format_Number(TinhTongTien, ".")
            'If (Not fblnVangLai) Then
            '    lblStatus.Text = "Tìm thấy NNThuế trong Danh mục"
            'Else
            '    lblStatus.Text = "Tìm thấy NNThuế trong Danh mục Vãng lai"
            'End If


            Return TCS_BuildXML_FROM_TBL(v_dtHDR, v_dtDTL)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi check danh mục người nộp thuế", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function

    Private Shared Function TCS_CheckSoThue_CTN(ByVal strMaNNT As String, ByVal strNgayLV As String) As String
        Try
            Dim v_dtHDR As DataTable
            Dim v_dtDTL As DataTable

            Dim dsNNT As DataSet = ChungTu.buNNThue.SoThue_CTN_20180423(strMaNNT)
            If (IsEmptyDataSet(dsNNT)) Then Return ""

            mNguonNNT = NguonNNT.SoThue_CTN
            Dim strTemp As String
            Dim r As DataRow = dsNNT.Tables(0).Rows(0)

            v_dtHDR = TCS_BuildCTU_HDR_TBL()

            'strTemp = GetString(r("cq_qd")).Trim()
            'If (strTemp <> "") Then
            '    v_dtHDR.Rows(0)(ColHDR.CQBH) = strTemp
            'End If

            'strTemp = ChuanHoa_Date(GetString(r("ngay_qd")))
            'If (strTemp <> "") Then
            '    v_dtHDR.Rows(0)(ColHDR.NgayBHQD) = strTemp
            'End If

            'strTemp = GetString(r("so_qd"))
            'If (strTemp <> "") Then
            '    v_dtHDR.Rows(0)(ColHDR.SoQD) = strTemp
            'End If

            'strTemp = GetString(r("ma_cqthu"))
            'If (strTemp <> "") Then
            '    Me.txtMaCQThu.Text = strTemp
            '    Me.txtTenCQThu.Text = Get_TenCQThu(strTemp)
            'End If

            '
            v_dtHDR.Rows(0)(ColHDR.TKCo) = strFormatTK(GetString(r("tk_thu_ns")))
            ' v_dtHDR.Rows(0)(ColHDR.TenTKCo) = Get_TenTKhoan(GetString(r("tk_thu_ns")))
            If GetString(r("ma_cqthu")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaCQThu) = GetString(r("ma_cqthu"))
                v_dtHDR.Rows(0)(ColHDR.TenCQThu) = Get_TenCQThu(GetString(r("ma_cqthu")))
            End If
            If GetString(r("ma_xa")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaDBHC) = GetString(r("ma_xa"))
                v_dtHDR.Rows(0)(ColHDR.TenDBHC) = Get_TenDBHC(GetString(r("ma_xa")))
            End If


            'Nếu chưa có trong danh mục NNThue thì mới lấy thông tin này ra
            'If (Not mblnExistsNNT) Then
            If GetString(r("SHKB")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.SHKB) = GetString(r("SHKB"))
                v_dtHDR.Rows(0)(ColHDR.TenKB) = Get_TenKhoBac(GetString(r("SHKB")))
            End If
            v_dtHDR.Rows(0)(ColHDR.TenNNT) = GetString(r("ten_nnt"))
            If GetString(r("dia_chi")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))
            Else
                v_dtHDR.Rows(0)(ColHDR.DChiNNT) = TCS_GetDCNNT(strMaNNT)
            End If
            Dim strSoLanLap_CT As String = ChungTu.buChungTu.CTU_Check_Exist_NNT(strMaNNT, ConvertDateToNumber(strNgayLV))
            v_dtHDR.Rows(0)(ColHDR.TT_TTHU) = strSoLanLap_CT
            'End If
            Dim i As Integer
            Dim intMaxRow As Integer

            'Cho phép hiển thị tối đa 4 dòng chi tiết
            If (dsNNT.Tables(0).Rows.Count > 4) Then
                intMaxRow = 4
            Else
                intMaxRow = dsNNT.Tables(0).Rows.Count
            End If

            'sonmt


            If GetString(r("ma_huyen")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MA_HUYEN) = GetString(r("ma_huyen"))
                v_dtHDR.Rows(0)(ColHDR.TEN_HUYEN) = Get_TenDBHC(GetString(r("ma_huyen")))
            End If
            If GetString(r("ma_tinh")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MA_TINH) = GetString(r("ma_tinh"))
                v_dtHDR.Rows(0)(ColHDR.TEN_TINH) = Get_TenDBHC(GetString(r("ma_tinh")))
            End If


            'sonmt
            If GetString(r("LOAI_NNT")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.LOAI_NNT) = GetString(r("LOAI_NNT"))
            End If
            'If GetString(r("TEN_LOAI_NNT")).Length > 0 Then
            '    v_dtHDR.Rows(0)(ColHDR.TEN_LOAI_NNT) = GetString(r("TEN_LOAI_NNT"))
            'End If
            'nhamlt_20180423
            v_dtHDR.Rows(0)(ColHDR.TEN_LOAI_NNT) = ""
            v_dtDTL = TCS_BuildCTU_DTL_TBL()

            For i = 0 To intMaxRow - 1
                r = dsNNT.Tables(0).Rows(i)
                v_dtDTL.Rows(i)(ColDTL.MaQuy) = clsCTU.gc_MaQuyMacDinh

                strTemp = GetString(r("ma_chuong")).Trim()
                If (strTemp <> "") Then
                    v_dtDTL.Rows(i)(ColDTL.CChuong) = strTemp
                End If


                strTemp = GetString(r("ma_khoan")).Trim()
                If (strTemp <> "") Then
                    v_dtDTL.Rows(i)(ColDTL.LKhoan) = strTemp
                End If

                strTemp = GetString(r("ma_tmuc")).Trim()
                If (strTemp <> "") Then
                    v_dtDTL.Rows(i)(ColDTL.MTieuMuc) = strTemp
                    v_dtDTL.Rows(i)(ColDTL.NoiDung) = Get_TenTieuMuc(strTemp)
                End If

                If (strTemp <> "") Then
                    'grdChiTiet(i + 1, ColDTL.TLDT) = ""
                End If

                Dim strTien As String = GetString(r("so_phainop"))
                If (IsNumeric(strTien)) Then
                    v_dtDTL.Rows(i)(ColDTL.Tien) = VBOracleLib.Globals.Format_Number(strTien, ".")
                End If
                If (GetString(r("kythue")) <> "") Then
                    v_dtDTL.Rows(i)(ColDTL.KyThue) = GetString(r("kythue"))
                Else
                    v_dtDTL.Rows(i)(ColDTL.KyThue) = GetKyThue(ConvertDateToNumber(strNgayLV))
                End If
                'ChuanHoa_KyThue(GetString(r("kythue")))
                'Kienvt them truong ngay so da thu
                v_dtDTL.Rows(i)(ColDTL.SoDT) = TCS_CalSoDaThu(strMaNNT, clsCTU.gc_MaQuyMacDinh, _
                    GetString(r("ma_chuong")).Trim(), GetString(r("ma_khoan")).Trim(), GetString(r("ma_tmuc")).Trim(), v_dtDTL.Rows(i)(ColDTL.KyThue))
            Next

            'txtTongTien.Text = Globals.Format_Number(TinhTongTien(), ".")
            ''clsGridCTU.TCS_CapNhatTongTien(grdChiTiet, lblTongtien, lblTongtienNT)

            ''Đặt format theo loại Thuế Công thương nghiệp
            'CType(grdHeader.Rows(RowHeader.LoaiThue).Cells.Item(1).Controls(0), DropDownList).SelectedValue = "01"
            'CType(grdHeader.Rows(RowHeader.LoaiThue).Cells.Item(2).Controls(0), TextBox).Text = Get_TenLoaithue("01")

            'lblStatus.Text = "Tìm thấy NNThuế trong Sổ Thuế Công thương nghiệp"
            'mblnBindData = False

            ' ''Đặt forcus lên thông tin người nộp tiền
            'CType(grdHeader.Rows(RowHeader.DChiNNT).Cells.Item(1).Controls(0), TextBox).Focus()

            v_dtHDR.Rows(0)(ColHDR.LoaiThue) = "01"
            v_dtHDR.Rows(0)(ColHDR.DescLoaiThue) = Get_TenLoaithue("01")

            Return TCS_BuildXML_FROM_TBL(v_dtHDR, v_dtDTL)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi kiểm tra sổ thuế", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function
    Private Shared Function TCS_CheckDM_NNThue_NEW(ByVal strMaNNT As String, ByVal fblnVangLai As Boolean, ByVal strNgayLV As String) As String
        Try
            Dim v_dtHDR As DataTable
            Dim v_dtDTL As DataTable

            Dim v_strRetXML As String = String.Empty

            Dim dsNNT As DataSet

            If (Not fblnVangLai) Then
                dsNNT = ChungTu.buNNThue.NNThue(strMaNNT)
            Else
                dsNNT = ChungTu.buNNThue.NNT_VL(strMaNNT)
            End If

            If (IsEmptyDataSet(dsNNT)) Then
                Return String.Empty
            End If

            mNguonNNT = NguonNNT.NNThue_CTN

            v_dtHDR = TCS_BuildCTU_HDR_TBL()

            If (Not fblnVangLai) Then
                mNguonNNT = NguonNNT.NNThue_CTN
            Else
                mNguonNNT = NguonNNT.NNThue_VL
            End If

            Dim r As DataRow = dsNNT.Tables(0).Rows(0)
            If GetString(r("shkb")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.SHKB) = GetString(r("shkb"))
                v_dtHDR.Rows(0)(ColHDR.TenKB) = Get_TenKhoBac(r("shkb"))
            End If
            v_dtHDR.Rows(0)(ColHDR.TenNNT) = GetString(r("ten_nnt"))
            v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))

            If GetString(r("ma_xa")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaDBHC) = GetString(r("ma_xa"))
                v_dtHDR.Rows(0)(ColHDR.TenDBHC) = Get_TenDBHC(r("ma_xa"))
            End If
            v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))
            If GetString(r("ma_cqthu")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaCQThu) = GetString(r("ma_cqthu"))
                v_dtHDR.Rows(0)(ColHDR.TenCQThu) = Get_TenCQThu(GetString(r("ma_cqthu")))
            End If
            Dim strSoLanLap_CT As String = ChungTu.buChungTu.CTU_Check_Exist_NNT(strMaNNT, ConvertDateToNumber(strNgayLV))
            v_dtHDR.Rows(0)(ColHDR.TT_TTHU) = strSoLanLap_CT
            'Chi dien thong tin khi tim thay ban ghi,con khong se dien thong tin mac dinh
            'If GetString(r("ma_cqthu")).Length > 0 Then
            '    CType(grdHeader.Rows(ColHDR.MaCQThu).Cells.Item(1).Controls(0), TextBox).Text = GetString(r("ma_cqthu"))
            '    CType(grdHeader.Rows(ColHDR.MaCQThu).Cells.Item(2).Controls(0), TextBox).Text = Get_TenCQThu(GetString(r("ma_cqthu")))
            'End If

            'sonmt
            If GetString(r("ma_huyen")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MA_HUYEN) = GetString(r("ma_huyen"))
                v_dtHDR.Rows(0)(ColHDR.TEN_HUYEN) = Get_TenDBHC(r("ma_huyen"))
            End If
            If GetString(r("ma_tinh")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MA_TINH) = GetString(r("ma_tinh"))
                v_dtHDR.Rows(0)(ColHDR.TEN_TINH) = Get_TenDBHC(r("ma_tinh"))
            End If

            'sonmt
            If GetString(r("LOAI_NNT")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.LOAI_NNT) = GetString(r("LOAI_NNT"))
            End If
            If GetString(r("TEN_LOAI_NNT")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.TEN_LOAI_NNT) = GetString(r("TEN_LOAI_NNT"))
            End If

            v_dtDTL = TCS_BuildCTU_DTL_TBL()

            v_dtDTL.Rows(0)(ColDTL.MaQuy) = clsCTU.gc_MaQuyMacDinh
            v_dtDTL.Rows(0)(ColDTL.CChuong) = GetString(r("ma_chuong"))
            v_dtDTL.Rows(0)(ColDTL.LKhoan) = GetString(r("ma_khoan"))
            v_dtDTL.Rows(0)(ColDTL.KyThue) = strNgayLV 'CTuCommon.GetKyThue(ConvertDateToNumber(strNgayLV))

            'v_dtDTL.Rows(0)(ColDTL.SoDT) = TCS_CalSoDaThu(strMaNNT, _
            '    clsCTU.gc_MaQuyMacDinh, GetString(r("ma_chuong")), GetString(r("ma_khoan")), r("Ma_TMuc").ToString(), _
            '             v_dtDTL.Rows(0)(ColDTL.KyThue))

            'txtTongTien.Text = Globals.Format_Number(TinhTongTien, ".")
            'If (Not fblnVangLai) Then
            '    lblStatus.Text = "Tìm thấy NNThuế trong Danh mục"
            'Else
            '    lblStatus.Text = "Tìm thấy NNThuế trong Danh mục Vãng lai"
            'End If


            Return TCS_BuildXML_FROM_TBL(v_dtHDR, v_dtDTL)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi check danh mục người nộp thuế", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function

    Private Shared Function TCS_CheckSoThue_CTN_NEW(ByVal strMaNNT As String, ByVal strNgayLV As String) As String
        Try
            Dim v_dtHDR As DataTable
            Dim v_dtDTL As DataTable

            Dim dsNNT As DataSet = ChungTu.buNNThue.SoThue_CTN_20180423(strMaNNT)
            If (IsEmptyDataSet(dsNNT)) Then Return ""

            mNguonNNT = NguonNNT.SoThue_CTN
            Dim strTemp As String
            Dim r As DataRow = dsNNT.Tables(0).Rows(0)

            v_dtHDR = TCS_BuildCTU_HDR_TBL()

            'strTemp = GetString(r("cq_qd")).Trim()
            'If (strTemp <> "") Then
            '    v_dtHDR.Rows(0)(ColHDR.CQBH) = strTemp
            'End If

            'strTemp = ChuanHoa_Date(GetString(r("ngay_qd")))
            'If (strTemp <> "") Then
            '    v_dtHDR.Rows(0)(ColHDR.NgayBHQD) = strTemp
            'End If

            'strTemp = GetString(r("so_qd"))
            'If (strTemp <> "") Then
            '    v_dtHDR.Rows(0)(ColHDR.SoQD) = strTemp
            'End If

            'strTemp = GetString(r("ma_cqthu"))
            'If (strTemp <> "") Then
            '    Me.txtMaCQThu.Text = strTemp
            '    Me.txtTenCQThu.Text = Get_TenCQThu(strTemp)
            'End If

            '
            v_dtHDR.Rows(0)(ColHDR.TKCo) = strFormatTK(GetString(r("tk_thu_ns")))
            ' v_dtHDR.Rows(0)(ColHDR.TenTKCo) = Get_TenTKhoan(GetString(r("tk_thu_ns")))
            If GetString(r("ma_cqthu")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaCQThu) = GetString(r("ma_cqthu"))
                v_dtHDR.Rows(0)(ColHDR.TenCQThu) = Get_TenCQThu(GetString(r("ma_cqthu")))
            End If
            If GetString(r("ma_xa")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaDBHC) = GetString(r("ma_xa"))
                v_dtHDR.Rows(0)(ColHDR.TenDBHC) = Get_TenDBHC(GetString(r("ma_xa")))
            End If


            'Nếu chưa có trong danh mục NNThue thì mới lấy thông tin này ra
            'If (Not mblnExistsNNT) Then
            If GetString(r("SHKB")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.SHKB) = GetString(r("SHKB"))
                v_dtHDR.Rows(0)(ColHDR.TenKB) = Get_TenKhoBac(GetString(r("SHKB")))
            End If
            v_dtHDR.Rows(0)(ColHDR.TenNNT) = GetString(r("ten_nnt"))
            If GetString(r("dia_chi")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))
            Else
                v_dtHDR.Rows(0)(ColHDR.DChiNNT) = TCS_GetDCNNT(strMaNNT)
            End If
            Dim strSoLanLap_CT As String = ChungTu.buChungTu.CTU_Check_Exist_NNT(strMaNNT, ConvertDateToNumber(strNgayLV))
            v_dtHDR.Rows(0)(ColHDR.TT_TTHU) = strSoLanLap_CT
            'End If
            Dim i As Integer
            Dim intMaxRow As Integer

            'Cho phép hiển thị tối đa 4 dòng chi tiết
            If (dsNNT.Tables(0).Rows.Count > 4) Then
                intMaxRow = 4
            Else
                intMaxRow = dsNNT.Tables(0).Rows.Count
            End If

            'sonmt


            If GetString(r("ma_huyen")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MA_HUYEN) = GetString(r("ma_huyen"))
                v_dtHDR.Rows(0)(ColHDR.TEN_HUYEN) = Get_TenDBHC(GetString(r("ma_huyen")))
            End If
            If GetString(r("ma_tinh")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MA_TINH) = GetString(r("ma_tinh"))
                v_dtHDR.Rows(0)(ColHDR.TEN_TINH) = Get_TenDBHC(GetString(r("ma_tinh")))
            End If


            'sonmt
            If GetString(r("LOAI_NNT")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.LOAI_NNT) = GetString(r("LOAI_NNT"))
            End If
            'If GetString(r("TEN_LOAI_NNT")).Length > 0 Then
            '    v_dtHDR.Rows(0)(ColHDR.TEN_LOAI_NNT) = GetString(r("TEN_LOAI_NNT"))
            'End If
            'nhamlt_20180423
            v_dtHDR.Rows(0)(ColHDR.TEN_LOAI_NNT) = ""
            v_dtDTL = TCS_BuildCTU_DTL_TBL()

            For i = 0 To intMaxRow - 1
                r = dsNNT.Tables(0).Rows(i)
                v_dtDTL.Rows(i)(ColDTL.MaQuy) = clsCTU.gc_MaQuyMacDinh

                strTemp = GetString(r("ma_chuong")).Trim()
                If (strTemp <> "") Then
                    v_dtDTL.Rows(i)(ColDTL.CChuong) = strTemp
                End If


                strTemp = GetString(r("ma_khoan")).Trim()
                If (strTemp <> "") Then
                    v_dtDTL.Rows(i)(ColDTL.LKhoan) = strTemp
                End If

                strTemp = GetString(r("ma_tmuc")).Trim()
                If (strTemp <> "") Then
                    v_dtDTL.Rows(i)(ColDTL.MTieuMuc) = strTemp
                    v_dtDTL.Rows(i)(ColDTL.NoiDung) = Get_TenTieuMuc(strTemp)
                End If

                If (strTemp <> "") Then
                    'grdChiTiet(i + 1, ColDTL.TLDT) = ""
                End If

                Dim strTien As String = GetString(r("so_phainop"))
                If (IsNumeric(strTien)) Then
                    v_dtDTL.Rows(i)(ColDTL.Tien) = VBOracleLib.Globals.Format_Number(strTien, ".")
                End If
                If (GetString(r("kythue")) <> "") Then
                    v_dtDTL.Rows(i)(ColDTL.KyThue) = GetString(r("kythue"))
                Else
                    v_dtDTL.Rows(i)(ColDTL.KyThue) = strNgayLV
                End If
                'ChuanHoa_KyThue(GetString(r("kythue")))
                'Kienvt them truong ngay so da thu
                v_dtDTL.Rows(i)(ColDTL.SoDT) = TCS_CalSoDaThu(strMaNNT, clsCTU.gc_MaQuyMacDinh, _
                    GetString(r("ma_chuong")).Trim(), GetString(r("ma_khoan")).Trim(), GetString(r("ma_tmuc")).Trim(), v_dtDTL.Rows(i)(ColDTL.KyThue))
            Next

            'txtTongTien.Text = Globals.Format_Number(TinhTongTien(), ".")
            ''clsGridCTU.TCS_CapNhatTongTien(grdChiTiet, lblTongtien, lblTongtienNT)

            ''Đặt format theo loại Thuế Công thương nghiệp
            'CType(grdHeader.Rows(RowHeader.LoaiThue).Cells.Item(1).Controls(0), DropDownList).SelectedValue = "01"
            'CType(grdHeader.Rows(RowHeader.LoaiThue).Cells.Item(2).Controls(0), TextBox).Text = Get_TenLoaithue("01")

            'lblStatus.Text = "Tìm thấy NNThuế trong Sổ Thuế Công thương nghiệp"
            'mblnBindData = False

            ' ''Đặt forcus lên thông tin người nộp tiền
            'CType(grdHeader.Rows(RowHeader.DChiNNT).Cells.Item(1).Controls(0), TextBox).Focus()

            v_dtHDR.Rows(0)(ColHDR.LoaiThue) = "01"
            v_dtHDR.Rows(0)(ColHDR.DescLoaiThue) = Get_TenLoaithue("01")

            Return TCS_BuildXML_FROM_TBL(v_dtHDR, v_dtDTL)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi kiểm tra sổ thuế", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function

    'Private Shared Function TCS_CheckSoThue_CTN(ByVal strMaNNT As String, ByVal strSoQD As String) As String
    '    Try
    '        Dim v_dtHDR As DataTable
    '        Dim v_dtDTL As DataTable

    '        Dim dsNNT As DataSet = ChungTu.buNNThue.SoThue_CTN(strMaNNT, strSoQD)
    '        If (IsEmptyDataSet(dsNNT)) Then Return String.Empty
    '        mNguonNNT = NguonNNT.SoThue_CTN

    '        Dim strTemp As String

    '        Dim r As DataRow = dsNNT.Tables(0).Rows(0)

    '        v_dtHDR = TCS_BuildCTU_HDR_TBL()

    '        v_dtHDR.Rows(0)(ColHDR.TKCo) = strFormatTK(GetString(r("tk_thu_ns")))
    '        v_dtHDR.Rows(0)(ColHDR.TenTKCo) = Get_TenTKhoan(GetString(r("tk_thu_ns")))

    '        If GetString(r("ma_xa")).Length > 0 Then
    '            v_dtHDR.Rows(0)(ColHDR.MaDBHC) = GetString(r("ma_xa"))
    '            v_dtHDR.Rows(0)(ColHDR.TenDBHC) = Get_TenDBHC(GetString(r("ma_xa")))
    '        End If

    '        'v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))
    '        'v_dtHDR.Rows(0)(ColHDR.TenNNT) = GetString(r("ten_nnt"))
    '        v_dtHDR.Rows(0)(ColHDR.TenNNT) = GetString(r("ten_nnt"))
    '        If GetString(r("dia_chi")).Length > 0 Then
    '            v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))
    '        Else
    '            v_dtHDR.Rows(0)(ColHDR.DChiNNT) = TCS_GetDCNNT(strMaNNT)
    '        End If

    '        Dim i As Integer
    '        Dim intMaxRow As Integer

    '        'Cho phép hiển thị tối đa 4 dòng chi tiết
    '        If (dsNNT.Tables(0).Rows.Count > 4) Then
    '            intMaxRow = 4
    '        Else
    '            intMaxRow = dsNNT.Tables(0).Rows.Count
    '        End If

    '        v_dtDTL = TCS_BuildCTU_DTL_TBL()

    '        For i = 0 To intMaxRow - 1
    '            r = dsNNT.Tables(0).Rows(i)
    '            v_dtDTL.Rows(i)(ColDTL.MaQuy) = clsCTU.gc_MaQuyMacDinh

    '            strTemp = GetString(r("ma_chuong")).Trim()
    '            If (strTemp <> "") Then
    '                v_dtDTL.Rows(i)(ColDTL.CChuong) = strTemp
    '            End If

    '            strTemp = GetString(r("ma_khoan")).Trim()
    '            If (strTemp <> "") Then
    '                v_dtDTL.Rows(i)(ColDTL.LKhoan) = strTemp
    '            End If

    '            strTemp = GetString(r("ma_tmuc")).Trim()
    '            If (strTemp <> "") Then
    '                v_dtDTL.Rows(i)(ColDTL.MTieuMuc) = strTemp
    '                v_dtDTL.Rows(i)(ColDTL.NoiDung) = Get_TenTieuMuc(strTemp)
    '            End If

    '            Dim strTien As String = GetString(r("so_phainop"))
    '            If (IsNumeric(strTien)) Then
    '                v_dtDTL.Rows(i)(ColDTL.Tien) = Globals.Format_Number(strTien, ".")
    '            End If

    '            v_dtDTL.Rows(i)(ColDTL.KyThue) = GetString(r("kythue")) ' ChuanHoa_KyThue(GetString(r("kythue")))

    '            v_dtDTL.Rows(i)(ColDTL.KyThue) = TCS_CalSoDaThu(strMaNNT, clsCTU.gc_MaQuyMacDinh, _
    '                GetString(r("ma_chuong")).Trim(), GetString(r("ma_khoan")).Trim(), GetString(r("ma_tmuc")).Trim(), GetString(r("kythue")))

    '        Next

    '        v_dtHDR.Rows(0)(ColHDR.LoaiThue) = "01"
    '        v_dtHDR.Rows(0)(ColHDR.DescLoaiThue) = Get_TenLoaithue("01")

    '        Return TCS_BuildXML_FROM_TBL(v_dtHDR, v_dtDTL)
    '    Catch ex As Exception
    '        clsCommon.WriteLog(ex, "Lỗi check sổ thuế", HttpContext.Current.Session.Item("TEN_DN"))
    '        Throw ex
    '        'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '        '     & "Error code: System error!" & vbNewLine _
    '        '     & "Error message: " & ex.Message, EventLogEntryType.Error)
    '        Return String.Empty
    '    End Try
    'End Function

    Public Shared Function TCS_CheckToKhai(ByVal strMaNNT As String, Optional ByVal strSoTK As String = "", Optional ByVal strLoaiTT As String = "") As String
        Try
            Dim v_dtHDR As DataTable
            Dim v_dtDTL As DataTable
            Dim strDSToKhai As String = ""
            Dim strSoToKhai As String = ""
            Dim dsNNT As DataSet = Nothing
            'dsNNT = ChungTu.buNNThue.ToKhai(strMaNNT, strSoTK, strLoaiTT)
            'strDSToKhai = getDSToKhai(dsNNT.Tables(0))
            If strSoTK <> "" Then
                dsNNT = ChungTu.buNNThue.ToKhai(strMaNNT, strSoTK, strLoaiTT)
            End If
            If (IsEmptyDataSet(dsNNT)) Then Return String.Empty
            mNguonNNT = NguonNNT.HaiQuan
            Dim r As DataRow = dsNNT.Tables(0).Rows(0)

            v_dtHDR = TCS_BuildCTU_HDR_TBL()
            v_dtDTL = TCS_BuildCTU_DTL_TBL()

            'v_dtHDR.Rows(0)(ColHDR.TenNNT) = GetString(r("ten_nnt"))
            'v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))
            v_dtHDR.Rows(0)(ColHDR.TenNNT) = GetString(r("ten_nnt"))
            If GetString(r("dia_chi")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))
            Else
                v_dtHDR.Rows(0)(ColHDR.DChiNNT) = TCS_GetDCNNT(strMaNNT)
            End If

            If GetString(r("ma_cqthu")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaCQThu) = GetString(r("ma_cqthu"))
                v_dtHDR.Rows(0)(ColHDR.TenCQThu) = GetString(r("ten_hq")) 'Get_TenCQThu(GetString(r("ma_cqthu")))
            End If
            If GetString(r("ma_xa")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaDBHC) = GetString(r("ma_xa"))
                v_dtHDR.Rows(0)(ColHDR.TenDBHC) = Get_TenDBHC(GetString(r("ma_xa")))
            End If

            v_dtHDR.Rows(0)(ColHDR.ToKhaiSo) = GetString(r("so_tk"))
            v_dtHDR.Rows(0)(ColHDR.NgayDK) = GetString(r("ngay_tk"))
            v_dtHDR.Rows(0)(ColHDR.MA_NTK) = GetString(r("ma_ntk"))
            v_dtHDR.Rows(0)(ColHDR.DSToKhai) = strDSToKhai
            Dim strMaLH As String = GetString(r("LH_XNK").ToString())
            Dim strTenVT = "", strTenLH As String = ""
            Get_LHXNK_ALL(strMaLH, strTenLH, strTenVT)

            v_dtHDR.Rows(0)(ColHDR.LOAI_TT) = GetString(r("ma_lt").ToString())
            v_dtHDR.Rows(0)(ColHDR.LHXNK) = strTenVT
            v_dtHDR.Rows(0)(ColHDR.DescLHXNK) = strMaLH & "-" & strTenLH

            If GetString(r("ma_xa")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaDBHC) = GetString(r("ma_xa"))
                v_dtHDR.Rows(0)(ColHDR.TenDBHC) = Get_TenDBHC(GetString(r("ma_xa")))
            End If
            v_dtHDR.Rows(0).Item(ColHDR_HQ.MA_HQ_PH.ToString()) = GetString(r("ma_hq_ph"))
            v_dtHDR.Rows(0).Item(ColHDR_HQ.TEN_HQ_PH.ToString()) = GetString(r("ten_hq_ph"))

            v_dtHDR.Rows(0).Item(ColHDR_HQ.MA_HQ.ToString()) = GetString(r("ma_hq"))
            v_dtHDR.Rows(0).Item(ColHDR_HQ.TEN_HQ.ToString()) = GetString(r("ten_hq"))
            Dim strTKCo As String = GetString(r("tk_thu_ns"))
            If strTKCo.Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.TKCo) = strFormatTK(strTKCo)
                v_dtHDR.Rows(0)(ColHDR.TenTKCo) = Get_TenTKhoan(strTKCo)
            End If
            v_dtHDR.Rows(0).Item(ColHDR_HQ.MA_NTK.ToString()) = GetString(r("ma_ntk"))
            v_dtHDR.Rows(0).Item(ColHDR_HQ.SHKB.ToString()) = GetString(r("ma_kb"))
            v_dtHDR.Rows(0).Item(ColHDR_HQ.DIEN_GIAI.ToString()) = GetString(r("dien_giai"))
            'Bind du lieu vao list to khai
            'LoadDSToKhai(dsNNT.Tables(0), CType(grdHeader.Rows(ColHDR.ToKhaiSo).Cells.Item(2).Controls(0), DropDownList))
            'If CType(grdHeader.Rows(ColHDR.ToKhaiSo).Cells.Item(2).Controls(0), DropDownList).Items.Count > 1 Then
            '    CType(grdHeader.Rows(ColHDR.ToKhaiSo).Cells.Item(2).Controls(0), DropDownList).Enabled = True
            '    CType(grdHeader.Rows(ColHDR.ToKhaiSo).Cells.Item(2).Controls(0), DropDownList).Visible = True
            '    CType(grdHeader.Rows(ColHDR.ToKhaiSo).Cells.Item(1).Controls(0), TextBox).Enabled = False
            '    TCS_CheckToKhai(txtMaNNT.Text, txtToKhaiSo.Text, txtMaCQThu.Text)
            'Else
            '    CType(grdHeader.Rows(ColHDR.ToKhaiSo).Cells.Item(2).Controls(0), DropDownList).Enabled = False
            '    CType(grdHeader.Rows(ColHDR.ToKhaiSo).Cells.Item(2).Controls(0), DropDownList).Visible = False
            '    CType(grdHeader.Rows(ColHDR.ToKhaiSo).Cells.Item(1).Controls(0), TextBox).Enabled = True


            '    ClearGridDetail() 'Xoa thong tin grid chi tiet
            '    CType(grdChiTiet.Items(0).Controls(ColDTL.MaQuy).Controls(1), TextBox).Text = clsCTU.gc_MaQuyMacDinh
            '    CType(grdChiTiet.Items(0).Controls(ColDTL.KyThue).Controls(1), TextBox).Text = GetKyThue(mlngNgayLV)


            '    'Đặt format theo loại Thuế Hải Quan
            '    CType(grdHeader.Rows(ColHDR.LoaiThue).Cells.Item(1).Controls(0), DropDownList).SelectedValue = gstrLT_HaiQuan
            '    CType(grdHeader.Rows(ColHDR.LoaiThue).Cells.Item(2).Controls(0), TextBox).Text = Get_TenLoaithue(gstrLT_HaiQuan)
            'End If

            v_dtHDR.Rows(0)(ColHDR.LoaiThue) = gstrLT_HaiQuan
            v_dtHDR.Rows(0)(ColHDR.DescLoaiThue) = Get_TenLoaithue(gstrLT_HaiQuan)
            Dim strSoLanLap_CT As String = ChungTu.buChungTu.CTU_Check_Exist_NNT_HQ(strMaNNT, strSoTK)
            v_dtHDR.Rows(0)(ColHDR.TT_TTHU) = strSoLanLap_CT
            If strSoTK <> "" Then
                strSoToKhai = strSoTK
            Else
                strSoToKhai = GetString(r("so_tk"))
            End If
            Return TCS_CheckToKhai(v_dtHDR, v_dtDTL, strMaNNT, strSoToKhai, strLoaiTT)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi kiểm tra tờ khai", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function

    Public Shared Function TCS_CheckToKhai(ByVal strMaNNT As String, ByVal strCQT As String, ByVal strSoTK As String, ByVal strLHXNK As String) As String
        Try
            Dim v_dtHDR As DataTable
            Dim v_dtDTL As DataTable
            Dim strDSToKhai As String = ""
            Dim strSoToKhai As String
            Dim dsNNT As DataSet
            dsNNT = ChungTu.buNNThue.ToKhai(strMaNNT, strCQT, strSoTK, strLHXNK)
            strDSToKhai = getDSToKhai(dsNNT.Tables(0))

            If (IsEmptyDataSet(dsNNT)) Then Return String.Empty
            mNguonNNT = NguonNNT.HaiQuan
            Dim r As DataRow = dsNNT.Tables(0).Rows(0)

            v_dtHDR = TCS_BuildCTU_HDR_TBL()
            v_dtDTL = TCS_BuildCTU_DTL_TBL()

            'v_dtHDR.Rows(0)(ColHDR.TenNNT) = GetString(r("ten_nnt"))
            'v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))
            v_dtHDR.Rows(0)(ColHDR.TenNNT) = GetString(r("ten_nnt"))
            If GetString(r("dia_chi")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))
            Else
                v_dtHDR.Rows(0)(ColHDR.DChiNNT) = TCS_GetDCNNT(strMaNNT)
            End If

            If GetString(r("ma_cqthu")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaCQThu) = GetString(r("ma_cqthu"))
                v_dtHDR.Rows(0)(ColHDR.TenCQThu) = Get_TenCQThu(GetString(r("ma_cqthu")))
            End If
            If GetString(r("ma_xa")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaDBHC) = GetString(r("ma_xa"))
                v_dtHDR.Rows(0)(ColHDR.TenDBHC) = Get_TenDBHC(GetString(r("ma_xa")))
            End If

            v_dtHDR.Rows(0)(ColHDR.ToKhaiSo) = GetString(r("so_tk"))
            v_dtHDR.Rows(0)(ColHDR.NgayDK) = GetString(r("ngay_tk"))
            v_dtHDR.Rows(0)(ColHDR.DSToKhai) = strDSToKhai
            Dim strMaLH As String = GetString(r("LH_XNK").ToString())
            Dim strTenVT = "", strTenLH As String = ""
            Get_LHXNK_ALL(strMaLH, strTenLH, strTenVT)


            v_dtHDR.Rows(0)(ColHDR.LHXNK) = strTenVT
            v_dtHDR.Rows(0)(ColHDR.DescLHXNK) = strMaLH & "-" & strTenLH

            If GetString(r("ma_xa")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaDBHC) = GetString(r("ma_xa"))
                v_dtHDR.Rows(0)(ColHDR.TenDBHC) = Get_TenDBHC(GetString(r("ma_xa")))
            End If

            Dim strTKCo As String = GetString(r("tk_thu_ns"))
            If strTKCo.Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.TKCo) = strFormatTK(strTKCo)
                v_dtHDR.Rows(0)(ColHDR.TenTKCo) = Get_TenTKhoan(strTKCo)
            End If

            v_dtHDR.Rows(0)(ColHDR.LoaiThue) = gstrLT_HaiQuan
            v_dtHDR.Rows(0)(ColHDR.DescLoaiThue) = Get_TenLoaithue(gstrLT_HaiQuan)
            If strSoTK <> "" Then
                strSoToKhai = strSoTK
            Else
                strSoToKhai = GetString(r("so_tk"))
            End If
            Return TCS_CheckToKhai(v_dtHDR, v_dtDTL, strMaNNT, strSoToKhai)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi kiểm tra tờ khai", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function


    ''' ******************************************************'
    ''' Name: TCs the s_ check to khai.
    ''' Author:ANHLD
    ''' Goals: Lay to khai theo loai tien thue
    ''' Date: 21.09.2012
    ''' Event: Add
    ''' ************************BEGIN******************************'
    ''' <param name="strMaNNT">The STR ma NNT.</param>
    ''' <param name="strCQT">The STR CQT.</param>
    ''' <param name="strSoTK">The STR so TK.</param>
    ''' <param name="strLHXNK">The STR LHXNK.</param>
    ''' <param name="strLtt">The STR LTT.</param>
    ''' <returns>System.String.</returns>
    Public Shared Function TCS_CheckToKhai(ByVal strMaNNT As String, ByVal strCQT As String, ByVal strSoTK As String, ByVal strLHXNK As String, ByVal strLtt As String, ByVal pv_ngayLV As String, ByVal pv_TruyVan As String) As String
        Try
            Dim v_dtHDR As DataTable
            Dim v_dtDTL As DataTable
            Dim strDSToKhai As String = ""
            Dim strSoToKhai As String
            Dim dsNNT As DataSet
            If pv_TruyVan = "1" Then
                dsNNT = ChungTu.buNNThue.ToKhai(strMaNNT, strCQT, strSoTK, strLHXNK, strLtt)
            Else
                dsNNT = ChungTu.buNNThue.ToKhaiHQ(strMaNNT, strCQT, strSoTK, strLHXNK, strLtt)
            End If

            strDSToKhai = getDSToKhai(dsNNT.Tables(0))

            If (IsEmptyDataSet(dsNNT)) Then Return String.Empty
            mNguonNNT = NguonNNT.HaiQuan
            Dim r As DataRow = dsNNT.Tables(0).Rows(0)

            v_dtHDR = TCS_BuildCTU_HDR_TBL()
            v_dtDTL = TCS_BuildCTU_DTL_TBL()

            'v_dtHDR.Rows(0)(ColHDR_HQ.TenNNT) = GetString(r("ten_nnt"))
            'v_dtHDR.Rows(0)(ColHDR_HQ.DChiNNT) = GetString(r("dia_chi"))
            v_dtHDR.Rows(0).Item(ColHDR_HQ.TenNNT.ToString()) = GetString(r("ten_nnt"))
            If GetString(r("dia_chi")).Length > 0 Then
                v_dtHDR.Rows(0).Item(ColHDR_HQ.DChiNNT.ToString()) = GetString(r("dia_chi"))
            Else
                v_dtHDR.Rows(0).Item(ColHDR_HQ.DChiNNT.ToString()) = TCS_GetDCNNT(strMaNNT)
            End If

            If GetString(r("ma_cqthu")).Length > 0 Then
                v_dtHDR.Rows(0).Item(ColHDR_HQ.MaCQThu.ToString()) = GetString(r("ma_cqthu"))
                v_dtHDR.Rows(0).Item(ColHDR_HQ.TenCQThu.ToString()) = Get_TenCQThu(GetString(r("ma_cqthu")))
            End If
            If GetString(r("ma_xa")).Length > 0 Then
                v_dtHDR.Rows(0).Item(ColHDR_HQ.MaDBHC.ToString()) = "" 'GetString(r("ma_xa")) '01/10/2012-manhnv
                v_dtHDR.Rows(0).Item(ColHDR_HQ.TenDBHC.ToString()) = "" 'Get_TenDBHC(GetString(r("ma_xa")))'01/10/2012-manhnv
            End If

            v_dtHDR.Rows(0).Item(ColHDR_HQ.ToKhaiSo.ToString()) = GetString(r("so_tk"))
            v_dtHDR.Rows(0).Item(ColHDR_HQ.NgayDK.ToString()) = GetString(r("ngay_tk"))
            v_dtHDR.Rows(0).Item(ColHDR_HQ.DSToKhai.ToString()) = strDSToKhai
            v_dtHDR.Rows(0).Item(ColHDR_HQ.SHKB.ToString()) = GetString(r("ma_kb"))
            Dim strMaLH As String = GetString(r("LH_XNK").ToString())
            Dim strTenVT = "", strTenLH As String = ""
            Get_LHXNK_ALL(strMaLH, strTenLH, strTenVT)

            '//' ****************************************************************************
            '//' Người sửa: Anhld
            '//' Ngày 22.09.2012
            '//' Mục đích: Them ma hai quan phat hanh, ten hq ph
            '//'***************************************************************************** */
            v_dtHDR.Rows(0).Item(ColHDR_HQ.MA_HQ_PH.ToString()) = GetString(r("ma_hq_ph"))
            v_dtHDR.Rows(0).Item(ColHDR_HQ.TEN_HQ_PH.ToString()) = GetString(r("ten_hq_ph"))

            v_dtHDR.Rows(0).Item(ColHDR_HQ.MA_HQ.ToString()) = GetString(r("ma_hq"))
            v_dtHDR.Rows(0).Item(ColHDR_HQ.TEN_HQ.ToString()) = GetString(r("ten_hq"))

            v_dtHDR.Rows(0).Item(ColHDR_HQ.LHXNK.ToString()) = strTenVT
            v_dtHDR.Rows(0).Item(ColHDR_HQ.DescLHXNK.ToString()) = strMaLH & "-" & strTenLH

            If GetString(r("ma_xa")).Length > 0 Then
                v_dtHDR.Rows(0).Item(ColHDR_HQ.MaDBHC.ToString()) = GetString(r("ma_xa"))
                v_dtHDR.Rows(0).Item(ColHDR_HQ.TenDBHC.ToString()) = Get_TenDBHC(GetString(r("ma_xa")))
            End If

            Dim strTKCo As String = GetString(r("tk_thu_ns"))
            If strTKCo.Length > 0 Then
                v_dtHDR.Rows(0).Item(ColHDR_HQ.TKCo.ToString()) = strFormatTK(strTKCo)
                v_dtHDR.Rows(0).Item(ColHDR_HQ.TenTKCo.ToString()) = Get_TenTKhoan(strTKCo)
            End If

            v_dtHDR.Rows(0).Item(ColHDR_HQ.LoaiThue.ToString()) = gstrLT_HaiQuan
            v_dtHDR.Rows(0).Item(ColHDR_HQ.DescLoaiThue.ToString()) = Get_TenLoaithue(gstrLT_HaiQuan)
            Dim strSoLanLap_CT As String = ChungTu.buChungTu.CTU_Check_Exist_NNT_HQ(strMaNNT, strSoTK)
            v_dtHDR.Rows(0)(ColHDR.TT_TTHU) = strSoLanLap_CT
            If strSoTK <> "" Then
                strSoToKhai = strSoTK
            Else
                strSoToKhai = GetString(r("so_tk"))
            End If
            strLtt = GetString(r("ma_lt"))
            v_dtHDR.Rows(0).Item(ColHDR_HQ.LOAI_TT.ToString()) = GetString(r("ma_lt"))
            Return TCS_CheckToKhai(v_dtHDR, v_dtDTL, strMaNNT, strSoToKhai, strCQT, strLHXNK, strLtt)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi kiểm tra tờ khai", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function
    ''' ************************END********************************'
    ''' 
    Private Shared Function TCS_CheckToKhai(ByVal v_dtHDR As DataTable, ByVal v_dtDTL As DataTable, _
                                            ByVal strMaNNT As String, ByVal strSoTK As String, ByVal strLtt As String) As String
        Try
            Dim dsNNT As DataSet = ChungTu.buNNThue.ToKhai(strMaNNT, strSoTK, strLtt)
            If (IsEmptyDataSet(dsNNT)) Then Return String.Empty
            mNguonNNT = NguonNNT.HaiQuan

            Dim strTemp As String
            Dim r As DataRow = dsNNT.Tables(0).Rows(0)

            strTemp = GetString(r("ngay_tk"))
            If (strTemp <> "") Then
                v_dtHDR.Rows(0)(ColHDR.NgayDK) = strTemp
            End If

            strTemp = GetString(r("lh_xnk")).Trim()
            If (strTemp <> "") Then
                Dim strTenVT = "", strTenLH As String = ""
                Get_LHXNK_ALL(strTemp, strTenLH, strTenVT)
                v_dtHDR.Rows(0)(ColHDR.LHXNK) = strTemp
                v_dtHDR.Rows(0)(ColHDR.DescLHXNK) = strTemp & "-" & strTenLH
            End If

            Dim intMaxRow As Integer = 0

            'Cho phép hiển thị tối đa 4 dòng chi tiết
            '20/09/2012-ManhNV sua 4 -> 7
            If (dsNNT.Tables(0).Rows.Count > 7) Then
                intMaxRow = 7
            Else
                intMaxRow = dsNNT.Tables(0).Rows.Count
            End If

            For i As Integer = 0 To intMaxRow - 1 'dsNNT.Tables(0).Rows.Count - 1
                r = dsNNT.Tables(0).Rows(i)

                v_dtDTL.Rows(i)(ColDTL.MaQuy) = clsCTU.gc_MaQuyMacDinh
                v_dtDTL.Rows(i)(ColDTL.CChuong) = GetString(r("ma_chuong"))
                v_dtDTL.Rows(i)(ColDTL.LKhoan) = GetString(r("ma_khoan"))
                v_dtDTL.Rows(i)(ColDTL.MTieuMuc) = GetString(r("ma_tmuc"))
                v_dtDTL.Rows(i)(ColDTL.NoiDung) = Get_TenTieuMuc(GetString(r("ma_tmuc")))

                Dim strTien As String = GetString(dsNNT.Tables(0).Rows(i).Item("so_phainop"))
                If (IsNumeric(strTien)) Then
                    v_dtDTL.Rows(i)(ColDTL.Tien) = strTien
                End If

                ' v_dtDTL.Rows(i)(ColDTL.KyThue) = GetKyThue(gdtmNgayLV)

                'Kienvt them truong ngay so da thu
                'v_dtDTL.Rows(i)(ColDTL.SoDT) = TCS_CalSoDaThu(strMaNNT, clsCTU.gc_MaQuyMacDinh, _
                '   GetString(r("ma_chuong")).Trim(), GetString(r("ma_khoan")).Trim(), GetString(r("ma_tmuc")).Trim(), GetKyThue(gdtmNgayLV))
            Next

            Return TCS_BuildXML_FROM_TBL(v_dtHDR, v_dtDTL)
        Catch ex As Exception
            ex.Source = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi kiểm tra tờ khai", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function

    ''' ******************************************************'
    ''' Name: TCs the s_ check to khai.
    ''' Author:ANHLD
    ''' Goals: Lay to khai theo loai tien thue
    ''' Date: 21.09.2012
    ''' Event: Add
    ''' ************************BEGIN******************************'
    ''' <param name="v_dtHDR">The V_DT HDR.</param>
    ''' <param name="v_dtDTL">The V_DT DTL.</param>
    ''' <param name="strMaNNT">The STR ma NNT.</param>
    ''' <param name="strSoTK">The STR so TK.</param>
    ''' <param name="strCQT">The STR CQT.</param>
    ''' <param name="strLhxnk">The STR LHXNK.</param>
    ''' <param name="strLtt">The STR LTT.</param>
    ''' <returns>System.String.</returns>
    Private Shared Function TCS_CheckToKhai(ByVal v_dtHDR As DataTable, ByVal v_dtDTL As DataTable, _
                                            ByVal strMaNNT As String, ByVal strSoTK As String, ByVal strCQT As String, ByVal strLhxnk As String, ByVal strLtt As String) As String
        Try
            Dim dsNNT As DataSet = ChungTu.buNNThue.ToKhai(strMaNNT, strCQT, strSoTK, strLhxnk, strLtt)
            If (IsEmptyDataSet(dsNNT)) Then Return String.Empty
            mNguonNNT = NguonNNT.HaiQuan

            Dim strTemp As String
            Dim r As DataRow = dsNNT.Tables(0).Rows(0)

            strTemp = GetString(r("ngay_tk"))
            If (strTemp <> "") Then
                v_dtHDR.Rows(0)(ColHDR.NgayDK) = strTemp
            End If

            strTemp = GetString(r("lh_xnk")).Trim()
            If (strTemp <> "") Then
                Dim strTenVT = "", strTenLH As String = ""
                Get_LHXNK_ALL(strTemp, strTenLH, strTenVT)
                v_dtHDR.Rows(0)(ColHDR.LHXNK) = strTemp
                v_dtHDR.Rows(0)(ColHDR.DescLHXNK) = strTemp & "-" & strTenLH
            End If

            Dim intMaxRow As Integer = 0

            'Cho phép hiển thị tối đa 4 dòng chi tiết
            '20/09/2012-ManhNV sua 4 -> 7
            If (dsNNT.Tables(0).Rows.Count > 7) Then
                intMaxRow = 7
            Else
                intMaxRow = dsNNT.Tables(0).Rows.Count
            End If

            For i As Integer = 0 To intMaxRow - 1 'dsNNT.Tables(0).Rows.Count - 1
                r = dsNNT.Tables(0).Rows(i)

                v_dtDTL.Rows(i)(ColDTL.MaQuy) = clsCTU.gc_MaQuyMacDinh
                v_dtDTL.Rows(i)(ColDTL.CChuong) = GetString(r("ma_chuong"))
                v_dtDTL.Rows(i)(ColDTL.LKhoan) = GetString(r("ma_khoan"))
                v_dtDTL.Rows(i)(ColDTL.MTieuMuc) = GetString(r("ma_tmuc"))
                v_dtDTL.Rows(i)(ColDTL.NoiDung) = Get_TenTieuMuc(GetString(r("ma_tmuc")))

                Dim strTien As String = GetString(dsNNT.Tables(0).Rows(i).Item("so_phainop"))
                If (IsNumeric(strTien)) Then
                    v_dtDTL.Rows(i)(ColDTL.Tien) = strTien
                End If

                ' v_dtDTL.Rows(i)(ColDTL.KyThue) = GetKyThue(gdtmNgayLV)

                v_dtDTL.Rows(i)(ColDTL.SoDT) = TCS_CalSoDaThu_TKhai(strMaNNT, GetString(r("ma_chuong")).Trim(), GetString(r("ma_tmuc")).Trim(), strSoTK)
            Next

            Return TCS_BuildXML_FROM_TBL(v_dtHDR, v_dtDTL)
        Catch ex As Exception
            ex.Source = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi kiểm tra tờ khai", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function
    ''' ************************END********************************'
    Private Shared Function TCS_CheckToKhai(ByVal v_dtHDR As DataTable, ByVal v_dtDTL As DataTable, _
                                            ByVal strMaNNT As String, ByVal strSoTK As String) As String
        Try
            Dim dsNNT As DataSet = ChungTu.buNNThue.ToKhai(strMaNNT, strSoTK)
            If (IsEmptyDataSet(dsNNT)) Then Return String.Empty
            mNguonNNT = NguonNNT.HaiQuan

            Dim strTemp As String
            Dim r As DataRow = dsNNT.Tables(0).Rows(0)

            strTemp = GetString(r("ngay_tk"))
            If (strTemp <> "") Then
                v_dtHDR.Rows(0)(ColHDR.NgayDK) = strTemp
            End If

            strTemp = GetString(r("lh_xnk")).Trim()
            If (strTemp <> "") Then
                Dim strTenVT = "", strTenLH As String = ""
                Get_LHXNK_ALL(strTemp, strTenLH, strTenVT)
                v_dtHDR.Rows(0)(ColHDR.LHXNK) = strTemp
                v_dtHDR.Rows(0)(ColHDR.DescLHXNK) = strTemp & "-" & strTenLH
            End If

            Dim intMaxRow As Integer = 0

            'Cho phép hiển thị tối đa 4 dòng chi tiết
            '20/09/2012-ManhNV sua 4 -> 7
            If (dsNNT.Tables(0).Rows.Count > 7) Then
                intMaxRow = 7
            Else
                intMaxRow = dsNNT.Tables(0).Rows.Count
            End If

            For i As Integer = 0 To intMaxRow - 1 'dsNNT.Tables(0).Rows.Count - 1
                r = dsNNT.Tables(0).Rows(i)

                v_dtDTL.Rows(i)(ColDTL.MaQuy) = clsCTU.gc_MaQuyMacDinh
                v_dtDTL.Rows(i)(ColDTL.CChuong) = GetString(r("ma_chuong"))
                v_dtDTL.Rows(i)(ColDTL.LKhoan) = GetString(r("ma_khoan"))
                v_dtDTL.Rows(i)(ColDTL.MTieuMuc) = GetString(r("ma_tmuc"))
                v_dtDTL.Rows(i)(ColDTL.NoiDung) = Get_TenTieuMuc(GetString(r("ma_tmuc")))

                Dim strTien As String = GetString(dsNNT.Tables(0).Rows(i).Item("so_phainop"))
                If (IsNumeric(strTien)) Then
                    v_dtDTL.Rows(i)(ColDTL.Tien) = strTien
                End If

                ' v_dtDTL.Rows(i)(ColDTL.KyThue) = GetKyThue(gdtmNgayLV)

                'Kienvt them truong ngay so da thu
                'v_dtDTL.Rows(i)(ColDTL.SoDT) = TCS_CalSoDaThu(strMaNNT, clsCTU.gc_MaQuyMacDinh, _
                '   GetString(r("ma_chuong")).Trim(), GetString(r("ma_khoan")).Trim(), GetString(r("ma_tmuc")).Trim(), GetKyThue(gdtmNgayLV))
            Next

            Return TCS_BuildXML_FROM_TBL(v_dtHDR, v_dtDTL)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  ex.Source = New System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name
            '  clsCommon.WriteLog(ex, "Lỗi kiểm tra tờ khai", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function

    Private Shared Function TCS_CheckVangLai(ByVal strNgayLV As String) As String
        Try
            Dim v_dtHDR As DataTable
            Dim v_dtDTL As DataTable

            Dim dsNNT As DataSet = ChungTu.buNNThue.VangLai()
            If (IsEmptyDataSet(dsNNT)) Then Return String.Empty
            mNguonNNT = NguonNNT.VangLai

            Dim r As DataRow = dsNNT.Tables(0).Rows(0)

            v_dtHDR = TCS_BuildCTU_HDR_TBL()

            v_dtHDR.Rows(0)(ColHDR.TenNNT) = GetString(r("ten_nnt"))
            v_dtHDR.Rows(0)(ColHDR.DChiNNT) = GetString(r("dia_chi"))

            Dim strXa As String = r("ma_xa").ToString()
            If (strXa.Trim() <> "") Then
                v_dtHDR.Rows(0)(ColHDR.MaDBHC) = GetString(r("ma_xa"))
                v_dtHDR.Rows(0)(ColHDR.TenDBHC) = Get_TenDBHC(r("ma_xa").ToString())
            End If

            'If GetString(r("ma_cqthu")).Length > 0 Then
            '    CType(grdHeader.Rows(ColHDR.MaCQThu).Cells.Item(1).Controls(0), TextBox).Text = GetString(r("ma_cqthu"))
            '    CType(grdHeader.Rows(ColHDR.MaCQThu).Cells.Item(2).Controls(0), TextBox).Text = Get_TenCQThu(GetString(r("ma_cqthu")))
            'End If

            v_dtDTL = TCS_BuildCTU_DTL_TBL()

            v_dtDTL.Rows(0)(ColDTL.MaQuy) = clsCTU.gc_MaQuyMacDinh
            v_dtDTL.Rows(0)(ColDTL.CChuong) = GetString(r("ma_chuong"))
            v_dtDTL.Rows(0)(ColDTL.LKhoan) = GetString(r("ma_khoan"))
            v_dtDTL.Rows(0)(ColDTL.KyThue) = CTuCommon.GetKyThue(strNgayLV)

            Return TCS_BuildXML_FROM_TBL(v_dtHDR, v_dtDTL)

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi kiểm tra vãng lai", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function

    Private Shared Function TCS_CheckDM_TruocBa(ByVal strMaNNT As String, ByVal strNgayLV As String) As String
        Try
            Dim v_dtHDR, v_dtDTL As DataTable

            Dim dsNNT As DataSet = ChungTu.buNNThue.TruocBa(strMaNNT)
            If (IsEmptyDataSet(dsNNT)) Then Return String.Empty

            mNguonNNT = NguonNNT.TruocBa

            Dim r As DataRow = dsNNT.Tables(0).Rows(0)

            'mblnBindData = True

            v_dtHDR.Rows(0)(ColHDR.TenNNT) = FontUtil.convertFromTCVN(GetString(r("TENDTNT")))
            v_dtHDR.Rows(0)(ColHDR.DChiNNT) = FontUtil.convertFromTCVN(GetString(r("DIACHI")))


            v_dtHDR.Rows(0)(ColHDR.SoKhung) = GetString(GetString(r("SOKHUNG")))
            v_dtHDR.Rows(0)(ColHDR.SoMay) = GetString(GetString(r("SOMAY")))

            If GetString(r("MADBTHU")).Length > 0 Then
                v_dtHDR.Rows(0)(ColHDR.MaDBHC) = strFormatDBHC(GetString(r("MADBTHU")))
                v_dtHDR.Rows(0)(ColHDR.TenDBHC) = Get_TenDBHC(GetString(r("MADBTHU")))
            End If

            v_dtHDR.Rows(0)(ColHDR.LoaiThue) = gstrLT_TB_XeMay
            v_dtHDR.Rows(0)(ColHDR.DescLoaiThue) = Get_TenLoaithue(gstrLT_TB_XeMay)

            v_dtDTL = TCS_BuildCTU_DTL_TBL()

            v_dtDTL.Rows(0)(ColDTL.MaQuy) = clsCTU.gc_MaQuyMacDinh
            v_dtDTL.Rows(0)(ColDTL.MaQuy) = GetString(r("MACHUONG"))
            v_dtDTL.Rows(0)(ColDTL.LKhoan) = GetString(r("MAKHOAN"))
            v_dtDTL.Rows(0)(ColDTL.KyThue) = CTuCommon.GetKyThue(strNgayLV)

            Return TCS_BuildXML_FROM_TBL(v_dtHDR, v_dtDTL)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi kiểm tra danh mục trước bạ", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.StackTrace & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function

    Private Shared Function TCS_CheckSoThue_TruocBa(ByVal strMaNNT As String) As String
        Try
            Dim v_dtHDR, v_dtDTL As DataTable

            Dim dsNNT As DataSet = ChungTu.buNNThue.SoThue_TruocBa(strMaNNT)
            If (IsEmptyDataSet(dsNNT)) Then Return String.Empty
            mNguonNNT = NguonNNT.TruocBa


            Dim r As DataRow

            v_dtHDR = TCS_BuildCTU_HDR_TBL()
            v_dtDTL = TCS_BuildCTU_DTL_TBL()

            Dim i As Integer
            For i = 0 To dsNNT.Tables(0).Rows.Count - 1
                r = dsNNT.Tables(0).Rows(i)

                v_dtDTL.Rows(i)(ColDTL.MaQuy) = ""

                'CType(grdChiTiet.Items(i).Controls(ColDTL.MaQuy).Controls(1), TextBox).Text = CType(grdChiTiet.Items(1).Controls(ColDTL.MaQuy).Controls(1), TextBox).Text
                'CType(grdChiTiet.Items(i).Controls(ColDTL.CChuong).Controls(1), TextBox).Text = CType(grdChiTiet.Items(1).Controls(ColDTL.CChuong).Controls(1), TextBox).Text
                'CType(grdChiTiet.Items(i).Controls(ColDTL.LKhoan).Controls(1), TextBox).Text = CType(grdChiTiet.Items(1).Controls(ColDTL.LKhoan).Controls(1), TextBox).Text

                'CType(grdChiTiet.Items(i).Controls(ColDTL.NoiDung).Controls(1), TextBox).Text = "" 'Get_TenTieuMuc(GetString(r("matm")))

                'Dim strTien As String = GetString(dsNNT.Tables(0).Rows(i).Item("thue"))
                'If (IsNumeric(strTien)) Then
                '    CType(grdChiTiet.Items(i).Controls(ColDTL.Tien).Controls(1), TextBox).Text = Globals.Format_Number(strTien, ".")
                'End If

                'CType(grdChiTiet.Items(i).Controls(ColDTL.KyThue).Controls(1), TextBox).Text = ChuanHoa_KyThue(GetString(r("kythue")))
                'grdChiTiet.Items(i + 1).Visible = True
            Next

            'txtTongTien.Text = Globals.Format_Number(TinhTongTien(), ".")

            ''GopChiTiet(grdChitiet) :????

            ''Đặt format theo loại Thuế Trước bạ
            'CType(grdHeader.Rows(ColHDR.LoaiThue).Cells.Item(1).Controls(0), DropDownList).SelectedValue = gstrLT_TB_XeMay
            'CType(grdHeader.Rows(ColHDR.LoaiThue).Cells.Item(2).Controls(0), TextBox).Text = Get_TenLoaithue(gstrLT_TB_XeMay)


            'lblStatus.Text = "Tìm thấy NNThuế trong Sổ thuế Trước bạ"

            ''Đặt forcus lên thông tin người nộp tiền
            'CType(grdHeader.Rows(ColHDR.DChiNNT).Cells.Item(1).Controls(0), TextBox).Focus()

            v_dtHDR.Rows(0)(ColHDR.LoaiThue) = gstrLT_TB_XeMay
            v_dtHDR.Rows(0)(ColHDR.DescLoaiThue) = Get_TenLoaithue(gstrLT_TB_XeMay)

            Return String.Empty
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) 'clsCommon.WriteLog(ex, "Lỗi kiểm tra sổ thuế trước bạ", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.StackTrace & vbNewLine _
            '     & "Error code: System error!" & vbNewLine _
            '     & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return String.Empty
        End Try
    End Function

#End Region

#Region "BuildXML Message"
    Private Shared Function TCS_BuildCTU_HDR_TBL() As DataTable
        Try
            Dim v_dtHDR As New DataTable("CTU_HDR")

            v_dtHDR.Columns.Add("NgayLV", GetType(System.String)) '0
            v_dtHDR.Columns.Add("SoCT", GetType(System.String)) '1
            v_dtHDR.Columns.Add("So_BT", GetType(System.String)) '1
            v_dtHDR.Columns.Add("SHKB", GetType(System.String)) '2
            v_dtHDR.Columns.Add("TenKB", GetType(System.String)) '3
            v_dtHDR.Columns.Add("MaNNT", GetType(System.String)) '4
            v_dtHDR.Columns.Add("TenNNT", GetType(System.String)) '5
            v_dtHDR.Columns.Add("DChiNNT", GetType(System.String)) '6
            v_dtHDR.Columns.Add("MaNNTien", GetType(System.String)) '7

            v_dtHDR.Columns.Add("TenNNTien", GetType(System.String)) '8
            v_dtHDR.Columns.Add("DChiNNTien", GetType(System.String)) '9
            v_dtHDR.Columns.Add("MaCQThu", GetType(System.String)) '10
            v_dtHDR.Columns.Add("TenCQThu", GetType(System.String)) '11

            v_dtHDR.Columns.Add("MaDBHC", GetType(System.String)) '12
            v_dtHDR.Columns.Add("TenDBHC", GetType(System.String)) '13
            v_dtHDR.Columns.Add("TKNo", GetType(System.String)) '14
            v_dtHDR.Columns.Add("TenTKNo", GetType(System.String)) '15
            v_dtHDR.Columns.Add("TKCo", GetType(System.String)) '16
            v_dtHDR.Columns.Add("TenTKCo", GetType(System.String)) '17

            v_dtHDR.Columns.Add("HTTT", GetType(System.String)) '18
            v_dtHDR.Columns.Add("SoCTNH", GetType(System.String)) '19
            v_dtHDR.Columns.Add("TK_KH_NH", GetType(System.String)) '20
            v_dtHDR.Columns.Add("TenTK_KH_NH", GetType(System.String)) '20
            v_dtHDR.Columns.Add("SoDu_KH_NH", GetType(System.String)) '21
            v_dtHDR.Columns.Add("NGAY_KH_NH", GetType(System.String)) '22

            'PHAN THONG TIN NGAN HANG
            v_dtHDR.Columns.Add("MA_NH_A", GetType(System.String)) '20
            v_dtHDR.Columns.Add("Ten_NH_A", GetType(System.String)) '20
            v_dtHDR.Columns.Add("MA_NH_B", GetType(System.String)) '21
            v_dtHDR.Columns.Add("Ten_NH_B", GetType(System.String)) '22

            v_dtHDR.Columns.Add("LoaiThue", GetType(System.String)) '23
            v_dtHDR.Columns.Add("DescLoaiThue", GetType(System.String)) '24
            v_dtHDR.Columns.Add("MaNT", GetType(System.String)) '25
            v_dtHDR.Columns.Add("TenNT", GetType(System.String)) '26
            v_dtHDR.Columns.Add("ToKhaiSo", GetType(System.String)) '27
            v_dtHDR.Columns.Add("NgayDK", GetType(System.String)) '28
            v_dtHDR.Columns.Add("LHXNK", GetType(System.String)) '29
            v_dtHDR.Columns.Add("DescLHXNK", GetType(System.String)) '30
            v_dtHDR.Columns.Add("SoKhung", GetType(System.String)) '31
            v_dtHDR.Columns.Add("SoMay", GetType(System.String)) '32
            v_dtHDR.Columns.Add("So_BK", GetType(System.String)) '33
            v_dtHDR.Columns.Add("Ngay_BK", GetType(System.String)) '34
            v_dtHDR.Columns.Add("TRANG_THAI", GetType(System.String)) '0
            v_dtHDR.Columns.Add("TT_BDS", GetType(System.String)) '0
            v_dtHDR.Columns.Add("TT_NSTT", GetType(System.String)) '0

            v_dtHDR.Columns.Add("PHI_GD", GetType(System.String)) '0
            v_dtHDR.Columns.Add("PHI_VAT", GetType(System.String)) '0
            v_dtHDR.Columns.Add("PT_TINHPHI", GetType(System.String)) '0
            v_dtHDR.Columns.Add("Huyen_NNTIEN", GetType(System.String)) '0
            v_dtHDR.Columns.Add("Tinh_NNTIEN", GetType(System.String)) '0

            v_dtHDR.Columns.Add("TTIEN", GetType(System.String)) '0
            v_dtHDR.Columns.Add("TT_TTHU", GetType(System.String)) '0
            v_dtHDR.Columns.Add("MA_NV", GetType(System.String)) '0
            v_dtHDR.Columns.Add("DSToKhai", GetType(System.String)) '0
            v_dtHDR.Columns.Add("MA_HQ", GetType(System.String)) '0
            v_dtHDR.Columns.Add("LOAI_TT", GetType(System.String)) '0
            v_dtHDR.Columns.Add("TEN_HQ", GetType(System.String)) '0
            v_dtHDR.Columns.Add("MA_HQ_PH", GetType(System.String)) '0
            v_dtHDR.Columns.Add("TEN_HQ_PH", GetType(System.String)) '0
            v_dtHDR.Columns.Add("MA_NH_TT", GetType(System.String)) '0
            v_dtHDR.Columns.Add("TEN_NH_TT", GetType(System.String)) '0
            v_dtHDR.Columns.Add("MA_KS", GetType(System.String)) '0
            v_dtHDR.Columns.Add("KHCT", GetType(System.String)) '0
            v_dtHDR.Columns.Add("TT_CTHUE", GetType(System.String)) '0
            v_dtHDR.Columns.Add("MA_SANPHAM", GetType(System.String))
            v_dtHDR.Columns.Add("TTIEN_TN", GetType(System.String))
            v_dtHDR.Columns.Add("TT_CITAD", GetType(System.String))
            v_dtHDR.Columns.Add("MA_NTK", GetType(System.String))
            v_dtHDR.Columns.Add("DIEN_GIAI", GetType(System.String))

            'sonmt
            v_dtHDR.Columns.Add("MA_HUYEN", GetType(System.String))
            v_dtHDR.Columns.Add("TEN_HUYEN", GetType(System.String))
            v_dtHDR.Columns.Add("MA_TINH", GetType(System.String))
            v_dtHDR.Columns.Add("TEN_TINH", GetType(System.String))
            v_dtHDR.Columns.Add("HUYEN_NNTHAY", GetType(System.String))
            v_dtHDR.Columns.Add("TINH_NNTHAY", GetType(System.String))
            v_dtHDR.Columns.Add("SO_QD", GetType(System.String))
            v_dtHDR.Columns.Add("CQ_QD", GetType(System.String))
            v_dtHDR.Columns.Add("NGAY_QD", GetType(System.String))
            v_dtHDR.Columns.Add("GHI_CHU", GetType(System.String))

            'sonmt
            v_dtHDR.Columns.Add("LOAI_NNT", GetType(System.String))
            v_dtHDR.Columns.Add("TEN_LOAI_NNT", GetType(System.String))

            Dim v_drRow1 As DataRow = v_dtHDR.NewRow()
            v_dtHDR.Rows.Add(v_drRow1)
            Return v_dtHDR
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi build chứng từ HDR", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function

    Private Shared Function TCS_BuildCTU_DTL_TBL() As DataTable
        Try
            Dim v_dtDTL As New DataTable("CTU_DTL")
            v_dtDTL.Columns.Add("CHK", GetType(System.String)) '0
            v_dtDTL.Columns.Add("MaQuy", GetType(System.String)) '1
            v_dtDTL.Columns.Add("CChuong", GetType(System.String)) '2
            v_dtDTL.Columns.Add("LKhoan", GetType(System.String)) '3
            v_dtDTL.Columns.Add("MTieuMuc", GetType(System.String)) '4
            v_dtDTL.Columns.Add("NoiDung", GetType(System.String)) '5
            v_dtDTL.Columns.Add("Tien", GetType(System.String)) '6
            v_dtDTL.Columns.Add("SoDT", GetType(System.String)) '7
            v_dtDTL.Columns.Add("KyThue", GetType(System.String)) '8

            Dim v_drRow1 As DataRow = v_dtDTL.NewRow()
            v_dtDTL.Rows.Add(v_drRow1)

            Dim v_drRow2 As DataRow = v_dtDTL.NewRow()
            v_dtDTL.Rows.Add(v_drRow2)

            Dim v_drRow3 As DataRow = v_dtDTL.NewRow()
            v_dtDTL.Rows.Add(v_drRow3)

            Dim v_drRow4 As DataRow = v_dtDTL.NewRow()
            v_dtDTL.Rows.Add(v_drRow4)
            '---ManhNV them, 20/09/2012
            Dim v_drRow5 As DataRow = v_dtDTL.NewRow()
            v_dtDTL.Rows.Add(v_drRow5)

            Dim v_drRow6 As DataRow = v_dtDTL.NewRow()
            v_dtDTL.Rows.Add(v_drRow6)

            Dim v_drRow7 As DataRow = v_dtDTL.NewRow()
            v_dtDTL.Rows.Add(v_drRow7)
            '---
            Return v_dtDTL
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi build chứng từ DTL", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function

    Private Shared Function TCS_BuildXML_FROM_TBL(ByVal pv_dtHDR As DataTable, ByVal pv_dtDTL As DataTable) As String
        Try
            Dim v_strRtXML As New StringBuilder
            v_strRtXML.Append(gc_XML_HEADER_TAG)
            v_strRtXML.Append(gc_XML_START_CTU_ROOT_TAG)

            '1)Build hdr chi co 1 dong
            v_strRtXML.Append(gc_XML_START_CTU_HDR_TAG)
            For Each v_dc As DataColumn In pv_dtHDR.Columns
                v_strRtXML.Append("<" & v_dc.ColumnName & ">")
                v_strRtXML.Append(pv_dtHDR.Rows(0)(v_dc.ColumnName).ToString.Replace("&", "&amp;"))
                v_strRtXML.Append("</" & v_dc.ColumnName & ">")
            Next
            v_strRtXML.Append(gc_XML_END_CTU_HDR_TAG)

            v_strRtXML.Append(gc_XML_START_CTU_DTL_TAG)

            ''2)Build dtl co nhieu dong
            For Each v_dr As DataRow In pv_dtDTL.Rows
                v_strRtXML.Append("<ITEMS>")
                For Each v_dc As DataColumn In pv_dtDTL.Columns
                    v_strRtXML.Append("<" & v_dc.ColumnName & ">")
                    v_strRtXML.Append(v_dr(v_dc.ColumnName))
                    v_strRtXML.Append("</" & v_dc.ColumnName & ">")
                Next
                v_strRtXML.Append("</ITEMS>")
            Next
            v_strRtXML.Append(gc_XML_END_CTU_DTL_TAG)

            ''3)Build the error cho biet trang thai error
            v_strRtXML.Append(gc_XML_END_CTU_ROOT_TAG)

            Return v_strRtXML.ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin từ XML DTL", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function
    Public Shared Function TCS_BuildCITAD_HDR_XML() As DataTable
        Dim v_dtHDR As New DataTable("VST")
        Try
            v_dtHDR.Columns.Add("MST", GetType(System.String)) '0
            v_dtHDR.Columns.Add("CQT", GetType(System.String)) '1
            v_dtHDR.Columns.Add("TCQ", GetType(System.String)) '1
            v_dtHDR.Columns.Add("LTH", GetType(System.String)) '2
            v_dtHDR.Columns.Add("NNT", GetType(System.String)) '3
            v_dtHDR.Columns.Add("SKH", GetType(System.String)) '4
            v_dtHDR.Columns.Add("SMA", GetType(System.String)) '5
            v_dtHDR.Columns.Add("STK", GetType(System.String)) '6
            v_dtHDR.Columns.Add("NTK", GetType(System.String)) '7
            v_dtHDR.Columns.Add("XNK", GetType(System.String)) '8
            v_dtHDR.Columns.Add("CQP", GetType(System.String)) '9
            v_dtHDR.Columns.Add("TKH", GetType(System.String)) '10
            Dim v_drRow1 As DataRow = v_dtHDR.NewRow()
            v_dtHDR.Rows.Add(v_drRow1)
            Return v_dtHDR
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '    CTuCommon.WriteLog(ex, "Lỗi build chứng từ HDR", HttpContext.Current.Session.Item("TEN_DN"))
        End Try
        Return v_dtHDR
    End Function
    Public Shared Function TCS_BuildCITAD_DTL_XML(Optional ByVal i_rows As Integer = 0) As DataTable
        Dim v_dtHDR As New DataTable("VSTD")
        Try
            v_dtHDR.Columns.Add("MCH", GetType(System.String)) '0
            v_dtHDR.Columns.Add("NDK", GetType(System.String)) '1
            v_dtHDR.Columns.Add("STN", GetType(System.String)) '1
            v_dtHDR.Columns.Add("NDG", GetType(System.String)) '2
            Dim i_rowID As Integer = 0
            If i_rows = 0 Then
                Dim v_drRow1 As DataRow = v_dtHDR.NewRow()
                v_dtHDR.Rows.Add(v_drRow1)
            Else
                While i_rowID < i_rows
                    Dim v_drRow11 As DataRow = v_dtHDR.NewRow()
                    v_dtHDR.Rows.Add(v_drRow11)
                    i_rowID += 1
                End While
            End If
            Return v_dtHDR
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  CTuCommon.WriteLog(ex, "Lỗi build chứng từ HDR", HttpContext.Current.Session.Item("TEN_DN"))
        End Try
        Return v_dtHDR
    End Function
    Private Shared Function getDSToKhai(ByVal dt As DataTable) As String
        Try
            Dim strOutput As String = ""
            For i As Integer = 0 To dt.Rows.Count - 1
                strOutput = strOutput & dt.Rows(i)("so_tk").ToString() & ";"
            Next
            Return strOutput
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy danh sách tờ khai", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function
#End Region

#Region "UtilFunction"
    ''' <summary>
    ''' Kiem tra tai khoan trung gian co ton tai hay khong(phan nay nen dua vao cho khac)
    ''' </summary>
    ''' <param name="pv_strAccount"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function CheckTKTG(ByVal pv_strAccount As String) As Boolean
        Dim dsTSHT As DataSet
        Dim strSQL As String
        Try
            'strSQL = "SELECT count(1) FROM tcs_dm_tk_tg a where a.so_tk='" & pv_strAccount & "' and ma_dthu=" & TCS_GetMaDT(CType(HttpContext.Current.Session("User"), Integer))
            strSQL = "SELECT count(1) FROM tcs_dm_tk_tg a where a.so_tk='" & pv_strAccount & "'" ' and ma_dthu=" & TCS_GetMaDT(CType(HttpContext.Current.Session("User"), Integer))
            dsTSHT = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If dsTSHT.Tables(0).Rows.Count > 0 Then
                If dsTSHT.Tables(0).Rows(0)(0) > 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi kiểm tra tài khoản trung gian", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Lay so du hien tai cua khach hang(cho cac chung tu chua kiem soat???Co thuc su can thiet?)
    ''' </summary>
    ''' <param name="pv_strSoTKNH"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function GetSoDu_KH_NH(ByVal pv_strSoTKNH As String) As String
        Dim v_strDescTKKH As String = ""
        Dim v_strSoDuNH As String = ""
        Dim v_strRtXML As New StringBuilder

        Dim v_objCoreBank As New clsCoreBankUtil
        Try
            v_strRtXML.Append("")
            'If v_objCoreBank.GetCustomerInfo(pv_strSoTKNH, v_strDescTKKH, v_strSoDuNH) = "0" Then
            '    'v_strRtXML.Append(gc_XML_HEADER_TAG)
            '    'v_strRtXML.Append(gc_XML_START_CTU_HDR_TAG)
            '    'v_strRtXML.Append("<TK_KH_NH>" & pv_strSoTKNH & "</TK_KH_NH>")
            '    'v_strRtXML.Append("<SoDu_KH_NH>" & v_strSoDuNH & "</SoDu_KH_NH>")
            '    'v_strRtXML.Append("<TenTK_KH_NH>" & v_strDescTKKH & "</TenTK_KH_NH>")
            '    'v_strRtXML.Append(gc_XML_END_CTU_HDR_TAG)
            '    'Return v_strRtXML.ToString
            '    Return v_strSoDuNH
            'Else
            '    Return String.Empty
            'End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy số dư tài khoản khách hàng", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            Return String.Empty
        End Try
    End Function

    ''' <summary>
    ''' Lay phan noi dung mo ta cua tieu muc(phan nay co nen dua vao cho khac)
    ''' </summary>
    ''' <param name="MaTMuc"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Get_TenTieuMuc(ByVal MaTMuc As String) As String
        Dim cnTenMTM As New DataAccess
        Dim drTenMTM As IDataReader
        Dim strSql As String
        Dim strResult As String = ""
        Try
            strSql = "Select TEN From TCS_DM_MUC_TMUC Where MA_TMUC='" & MaTMuc & "' and tinh_trang = '1'"

            strResult = cnTenMTM.ExecuteSQLScalar(strSql)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin tên tiểu mục", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        Finally
            If Not drTenMTM Is Nothing Then drTenMTM.Close()
            If Not cnTenMTM Is Nothing Then cnTenMTM.Dispose()
        End Try
        Return strResult

    End Function

    Public Shared Function TCS_CalSoDaThu(ByVal strMa_NNT As String, ByVal strMaQuy As String, ByVal strMaChuong As String, _
                                     ByVal strMaKhoan As String, ByVal strMaMuc As String, ByVal strKyThue As String) As String

        'Neu la tai khoan dac biet khong show so da thu
        If strMa_NNT.Trim = "0000000017" Then
            Return "0"
        Else
            Return ChungTu.buChungTu.CTU_CalSoDaThu(strMa_NNT, strMaQuy, strMaChuong, _
                                        strMaKhoan, strMaMuc, strKyThue)
        End If
    End Function
    Public Shared Function TCS_CalSoDaThu_TKhai(ByVal strMa_NNT As String, ByVal strMaChuong As String, _
                                    ByVal strMaMuc As String, ByVal strToKhai As String, Optional ByVal strNgayKS As String = "") As String

        'Neu la tai khoan dac biet khong show so da thu
        If strMa_NNT.Trim = "0000000017" Then
            Return "0"
        Else
            Return ChungTu.buChungTu.CTU_CalSoDaThu_ToKhai(strMa_NNT, strMaChuong, strMaMuc, strToKhai, strNgayKS)
        End If
    End Function
    Public Shared Function TCS_CalSoDaThu_TheoNgayKS(ByVal strMa_NNT As String, ByVal strMaChuong As String, _
                                      ByVal strMaMuc As String, ByVal strKyThue As String, ByVal strNgay_KS As String) As String

        'Neu la tai khoan dac biet khong show so da thu
        If strMa_NNT.Trim = "0000000017" Then
            Return "0"
        Else
            Return ChungTu.buChungTu.CTU_CalSoDaThu_Ngay_KS(strMa_NNT, strMaChuong, _
                                         strMaMuc, strKyThue, strNgay_KS)
        End If
    End Function
    Public Shared Function CTU_Check_Exist_NNT(ByVal strMa_NNT As String, ByVal strNgay_KS As String) As Integer

        'Neu la tai khoan dac biet khong show so da thu
        If strMa_NNT.Trim = "0000000017" Then
            Return "0"
        Else
            Return ChungTu.buChungTu.CTU_Check_Exist_NNT(strMa_NNT, strNgay_KS)
        End If
    End Function
    Public Shared Function CTU_Check_Exist_NNT_HQ(ByVal strMa_NNT As String, ByVal strSoTokhai As String) As Integer

        'Neu la tai khoan dac biet khong show so da thu
        If strMa_NNT.Trim = "0000000017" Then
            Return "0"
        Else
            Return ChungTu.buChungTu.CTU_Check_Exist_NNT_HQ(strMa_NNT, strSoTokhai)
        End If
    End Function

    Private Shared Function TCS_GetDCNNT(ByVal strMa_NNT As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "select dia_chi from tcs_dm_nnt where ma_nnt='" & strMa_NNT & "'"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi TCS_GetDCNNT", HttpContext.Current.Session.Item("TEN_DN"))
            'Throw ex
            Return ""
        End Try
    End Function

    Public Shared Function Get_TellerID(ByVal pv_intMaNV As Integer) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "select teller_id from tcs_dm_nhanvien where ma_nv=" & pv_intMaNV
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin teller", HttpContext.Current.Session.Item("TEN_DN"))
            'Throw ex
            Return Nothing
        End Try
    End Function

    Public Shared Function GetWorkingDate() As Long
        Dim dsTSHT As DataSet
        Dim strSQL As String
        Try
            strSQL = "SELECT TEN_TS,GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV' AND MA_DTHU='" & TCS_GetMaDT(CType(HttpContext.Current.Session("User"), Integer)) & "'"
            dsTSHT = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            Return ConvertDateToNumber(GetString(dsTSHT.Tables(0).Rows(0)("GIATRI_TS")).Trim())
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin ngày làm việc", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function

    Public Shared Function Get_GL_BranchCode(ByVal strMaNV As String) As String
        Dim strSQL As String
        Try
            strSQL = "select MA_CN from tcs_dm_nhanvien where ma_nv='" & strMaNV & "'"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin mã chi nhánh", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            Return Nothing
        End Try
    End Function
    Public Shared Function TCS_GetTenNV(ByVal strMa_NV As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT TEN FROM TCS_DM_NHANVIEN WHERE UPPER(TEN_DN) = '" & strMa_NV.ToUpper & "'"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin nhân viên", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            Return Nothing
        End Try
    End Function
    Public Shared Function TCS_GetTenDN(ByVal strMa_NV As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT TEN_DN FROM TCS_DM_NHANVIEN where ma_nv='" & strMa_NV & "'"
            Return DataAccess.ExecuteSQLScalar(strSQL)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin nhân viên", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            Return Nothing
        End Try
    End Function

    Public Shared Function GetKyThue(ByVal strNgayLV As String) As String
        Try
            If (strNgayLV.Length <> 8) Then Return ""

            Dim strThang, strNam As String
            strNam = strNgayLV.Substring(0, 4)
            strThang = strNgayLV.Substring(4, 2)

            Return (strThang & "/" & strNam)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin kỳ thuế", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function

    'Public Shared Function ChuanHoa_KyThue(ByVal strKyThue As String) As String
    '    Try
    '        strKyThue = Trim(strKyThue)
    '        If (Valid_KyThue(strKyThue)) Then Return strKyThue

    '        Dim intYear As Integer = Convert.ToInt16(strKyThue)
    '        Return ("01/" & strKyThue)
    '    Catch ex As Exception
    '        '  Return GetKyThue(gdtmNgayLV)
    '    End Try
    'End Function

    Public Shared Function TCS_GetKHKB(ByVal strMa_NV As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "')"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin KHKB", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            Return Nothing
        End Try
    End Function

    Public Shared Function TCS_GetMaDT(ByVal strMa_NV As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='MA_DT' AND MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "')"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin MADT", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            Return Nothing
        End Try
    End Function

    Public Shared Function TCS_GetMaHQ(ByVal strCQT As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT MA_HQ FROM TCS_DM_CQTHU WHERE MA_HQ IS NOT NULL AND MA_CQTHU='" & strCQT & "'"
            Dim ds As New DataSet
            ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
            If ds.Tables(0).Rows.Count > 0 Then
                Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
            End If
            Return ""
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin Mã hải quan", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            Return Nothing
        End Try
    End Function

    ''' ******************************************************'
    ''' Name: TCs the s_ get by CQT.
    ''' Author: ANHLD
    ''' Goals: Lay ma_hq, ma_hq_ph, shkb theo cqt
    ''' Date:
    ''' Event:
    ''' ************************BEGIN******************************'
    ''' <param name="strCQT">The STR CQT.</param>
    ''' <returns>System.String.</returns>
    Public Shared Function TCS_GetByCQT(ByVal strCQT As String, Optional ByVal strMa_HQ As String = "") As String
        Try
            Dim strSQL As String = String.Empty
            If strMa_HQ.Trim.Length = 0 Then
                strSQL = "SELECT (MA_HQ || '|' || SHKB || '|' || TEN) FROM TCS_DM_CQTHU WHERE MA_HQ IS NOT NULL AND MA_CQTHU='" & strCQT & "'"
            Else
                strSQL = "SELECT (MA_HQ || '|' || SHKB || '|' || TEN) FROM TCS_DM_CQTHU WHERE MA_HQ ='" & strMa_HQ & "' AND MA_CQTHU='" & strCQT & "' "
            End If

            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin cơ quan thu", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            Return Nothing
        End Try
    End Function
    ''' ************************END********************************'

    Public Shared Function TCS_GetMaChiNhanh(ByVal strMa_NV As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "'"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin mã chi nhánh", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            Return Nothing
        End Try
    End Function

    Public Shared Function TCS_GetThamSoBDS(ByVal strMaCN As String, ByRef strHostName As String, _
                                            ByRef strDBName As String, ByRef strDBUser As String, _
                                            ByRef strDBPassword As String, Optional ByVal strNguonDL As String = "1") As Boolean
        Try
            Dim v_dt As New DataTable
            Dim strSQL As String = String.Empty
            strSQL = "SELECT A.MA_CN, A.URLBDS, A.URLBDSVB, A.SERVERNAME, A.USER_NAME,A.PASSWORD, A.DBNAME ,A.BDSVB_SERVERNAME ,A.URLBDSVB,A.BDSVB_USER_NAME,A.BDSVB_PASSWORD FROM TCS_BANK_THAMSO A WHERE A.MA_CN='" & strMaCN & "'"
            v_dt = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0)
            If (v_dt.Rows.Count > 0) Then
                If (strNguonDL.Equals("1")) Then
                    strHostName = v_dt.Rows(0)("SERVERNAME").ToString
                    strDBName = v_dt.Rows(0)("URLBDS").ToString
                    strDBUser = v_dt.Rows(0)("USER_NAME").ToString
                    strDBPassword = v_dt.Rows(0)("PASSWORD").ToString
                Else
                    strHostName = v_dt.Rows(0)("BDSVB_SERVERNAME").ToString
                    strDBName = v_dt.Rows(0)("URLBDSVB").ToString
                    strDBUser = v_dt.Rows(0)("BDSVB_USER_NAME").ToString
                    strDBPassword = v_dt.Rows(0)("BDSVB_PASSWORD").ToString
                End If
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin tham số BDS", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            Return False
        End Try
    End Function

    Public Shared Function TCS_GetMaDT_KB(ByVal strSHKB As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT MA_DTHU FROM TCS_THAMSO WHERE TEN_TS='SHKB' AND GIATRI_TS='" & strSHKB & "'"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin mã điểm thu kho bạc", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            Return Nothing
        End Try
    End Function
    'manhnv-2012/10/11
    'Them ma_cn, loai_gd: 01: thue noi dia; 04: thue hai quan; 05: bao lanh hai quan
    Public Shared Function TCS_GetKHCT(ByVal strSHKB As String, Optional ByVal strMaNV As String = "", Optional ByVal strLoaiGD As String = "01") As String
        Try
            Dim strSQL As String = String.Empty
            Dim strResult As String = String.Empty


            If strLoaiGD = "01" Then
                strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT'"
                Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
            Else
                Try
                    Dim strGetCN As String = "select ma_cn from tcs_dm_nhanvien where ma_nv = '" & strMaNV & "'"
                    Dim strMaCN As String = DataAccess.ExecuteSQLScalar(strGetCN)
                    'strMaCN = strMaCN.Substring(0, 3)
                    If strLoaiGD = "04" Then
                        strSQL = "select substr(max(kyhieu_ct),0,3) || LPAD(TO_NUMBER(substr(max(kyhieu_ct),4,7))+1,7,0) KHCT from tcs_ctu_hdr " & _
                                 "where ma_nv in (select ma_nv from tcs_dm_nhanvien where ma_cn like '" & strMaCN & "%') and ma_lthue = '04'"
                    ElseIf strLoaiGD = "05" Then
                        strSQL = "select substr(max(kyhieu_ct),0,3) || LPAD(TO_NUMBER(substr(max(kyhieu_ct),4,7))+1,7,0) KHCT from tcs_baolanh_hdr where ma_nv in (select ma_nv from tcs_dm_nhanvien where ma_cn like '" & strMaCN & "%') "
                    End If
                    Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSQL)
                    If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                        strResult = dt.Rows(0)(0).ToString()
                        If strResult = "" Then
                            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT'"
                            Dim strKHCT As String = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
                            strResult = strKHCT.Substring(0, 3) & "0000001"
                        End If
                    Else
                        strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT' "
                        Dim strKHCT As String = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
                        strResult = strKHCT.Substring(0, 3) & "0000001"
                    End If
                Catch
                    strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='KHCT' "
                    Dim strKHCT As String = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
                    strResult = strKHCT.Substring(0, 3) & "0000001"
                End Try
                Return strResult
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin KHCT", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            Return Nothing
        End Try
    End Function

    Public Shared Function Get_SoBT(ByVal iNgayLV As Integer, ByVal iMaNV As Integer) As Integer
        Dim iResult As Integer = 0
        Try
            Dim strSql As String = "Select MAX(So_BT) AS So_BT From TCS_BAOLANH_HDR Where NGAY_KB=" & iNgayLV & " AND Ma_NV=" & iMaNV
            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(strSql)
            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                iResult = CInt(dt.Rows(0)(0).ToString()) + 1
            End If
        Catch ex As Exception
            ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin số bút toán", HttpContext.Current.Session.Item("TEN_DN"))
            iResult = 1
        End Try
        Return iResult
    End Function


    Public Shared Function TCS_GetDBThu(ByVal strMa_NV As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='DB_THU' AND MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "')"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin địa bàn thu", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            Return Nothing
        End Try
    End Function

    Public Shared Function TCS_GetMaNHKB(ByVal strMa_NV As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='MA_NHKB' AND MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "')"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin mã ngân hàng kho bạc", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            Return Nothing
        End Try
    End Function

    Public Shared Function TCS_GetMatKhau(ByVal strMa_NV As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT MAT_KHAU FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "'"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin mật khẩu", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function

    Public Shared Function TCS_GetParams(ByVal strMa_NV As String) As DataTable
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT TEN_TS,GIATRI_TS FROM TCS_THAMSO WHERE MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "')"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin tham số", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function

    Public Shared Function TCS_GetMaDBHC(ByVal strMa_NV As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='MA_DBHC' AND MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "')"
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin mã địa bàn hành chính", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function

    Public Shared Function TCS_GetNgayLV(ByVal strMa_NV As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV' AND MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "')"
            Return ConvertDateToNumber(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi lấy thông tin ngày làm việc", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'Return Nothing

        End Try
    End Function
    Public Shared Function TCS_GetNgayLV() As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV' AND MA_DTHU='03'"
            Return ConvertDateToNumber(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin ngày làm việc", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'Return Nothing

        End Try
    End Function

    Public Shared Function TCS_GetNgayKH_NH(ByVal strMa_NV As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT (CASE WHEN TO_DATE(GIATRI_TS,'DD/MM/YYYY') >TRUNC(SYSDATE) THEN TO_CHAR(SYSDATE,'DD/MM/YYYY') ELSE GIATRI_TS END) GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV' AND MA_DTHU=(SELECT BAN_LV FROM TCS_DM_NHANVIEN WHERE MA_NV='" & strMa_NV & "')"
            Return ConvertDateToNumber(DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin ngày giá trị", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function
    Public Shared Function TCS_GetNgayBDS() As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO_HT WHERE TEN_TS='NGAY_BDS' "
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy ngày BDS", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function
    Public Shared Function TCS_TTCUTOFFTIME() As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO_HT WHERE TEN_TS='TT_CUTOFFTIME' "
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy trạng thái cutofftime", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function
    Public Shared Function TCS_GETTS(ByVal p_strTENTS As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO_HT WHERE TEN_TS='" & p_strTENTS & "' "
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) 'clsCommon.WriteLog(ex, "Lỗi lấy giá trị tham số", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function
    Public Shared Function TCS_GetMaCN_NH(ByVal strMa_PGD As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT a.id, a.name, a.branch_code FROM tcs_dm_chinhanh a where a.id='" & strMa_PGD & "' "
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(2).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy mã chi nhánh ngân hàng", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function


    ''' **************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			TCs the s_ get ten CNNH.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		09/28/2012 00:00:00
    '''  Mục đích:	    Lấy thông tin tên chi nhánh ngân hàng
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    ''' <param name="p_strMaCN">The P_STR ma CN.</param>
    ''' <returns>System.String.</returns>
    Public Shared Function TCS_GetTenCNNH(ByVal p_strMaCN As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT a.id, a.name, a.branch_id FROM tcs_dm_chinhanh a where a.branch_id='" & p_strMaCN & "' "
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(1).ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin tên chi nhánh ngân hàng", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function

    ''' ******************************************************'
    ''' Name: TCs the s_ get truy van HQ.
    ''' Author: ANHLD
    ''' Goals: Order ket qua theo tieu muc
    ''' Date: 25.09.2012
    ''' Event: Modify
    ''' ************************BEGIN******************************'
    ''' <param name="strMaNNT">The STR ma NNT.</param>
    ''' <param name="strMaCQT">The STR ma CQT.</param>
    ''' <param name="strToKhaiSo">The STR to khai so.</param>
    ''' <param name="strLHXNK">The STR LHXNK.</param>
    ''' <param name="strMaLT">The STR ma LT.</param>
    ''' <returns>DataTable.</returns>
    Public Shared Function TCS_GetTruyVanHQ(ByVal strMaNNT As String, ByVal strMaCQT As String, ByVal strToKhaiSo As String, ByVal strLHXNK As String, Optional ByVal strMaLT As String = "") As DataTable
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT ma_nnt, ten_nnt, dia_chi, lh_xnk, ma_cqthu, ma_hq, so_phainop, so_tk, to_char(ngay_tk, 'dd/MM/yyyy') ngay_tk, tk_thu_ns, ma_chuong, ma_khoan, ma_tmuc, so_phainop, ma_hq_ph, ten_hq_ph, ten_hq,ma_kb " & _
            " FROM tcs_ds_tokhai WHERE ma_nnt='" & strMaNNT & "'" & _
                                    " And so_tk ='" & strToKhaiSo & "'"
            If Not strMaLT = "" Then
                strSQL = strSQL + " And ma_lt = '" + strMaLT + "'"
            End If
            strSQL = strSQL + " order by ma_lt, ma_tmuc asc "
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi truy vấn hải quan", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function

    Public Shared Function TCS_SetupParams(ByVal mv_strMaNV As Integer, ByRef mv_strSHKB As String, ByRef mv_strKyHieuCT As String, _
                                           ByRef mv_strMaDiemThu As String, ByRef mv_strTenDiemThu As String, _
                                           ByRef mv_strDBThu As String, ByRef mv_strMaNHKB As String, _
                                           ByRef mv_strTenKB As String) As Boolean
        Try
            Dim v_dt As DataTable = TCS_GetParams(mv_strMaNV)
            If Not v_dt Is Nothing Then
                For Each v_dr As DataRow In v_dt.Rows
                    Select Case v_dr("TEN_TS").ToString
                        Case "SHKB"
                            mv_strSHKB = v_dr("GIATRI_TS").ToString
                        Case "KHCT"
                            mv_strKyHieuCT = v_dr("GIATRI_TS").ToString
                        Case "MA_DT"
                            mv_strMaDiemThu = v_dr("GIATRI_TS").ToString
                        Case "TEN_DTHU"
                            mv_strTenDiemThu = v_dr("GIATRI_TS").ToString
                        Case "DB_THU"
                            mv_strDBThu = v_dr("GIATRI_TS").ToString.Replace(";", "")
                        Case "MA_NHKB"
                            mv_strMaNHKB = v_dr("GIATRI_TS").ToString
                        Case "TEN_KB"
                            mv_strTenKB = v_dr("GIATRI_TS").ToString
                    End Select
                Next
            End If
            Return True
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi cấu hình tham số", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source:" & ex.StackTrace & vbNewLine _
            '  & "Error code: System error!" & vbNewLine _
            '  & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try

    End Function

    Public Shared Function GenerateRMRef(ByVal pv_strBranchNo As String, ByRef seq_no As String) As String
        Try
            Dim v_strTempREF As String = String.Empty
            Dim v_strWorkingDate As String = String.Empty
            Dim v_strAutoID As String = String.Empty
            Dim v_strSQL As String = "SELECT GIATRI_TS NGAY_LV FROM TCS_THAMSO_HT WHERE TEN_TS = 'NGAY_BDS'"
            '****************

            Dim dt As DataTable = DatabaseHelp.ExecuteToTable(v_strSQL)
            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                v_strWorkingDate = dt.Rows(0)(0).ToString
            End If

            v_strSQL = "SELECT TCS_REF_NO_SEQ.NEXTVAL FROM DUAL"
            dt = DatabaseHelp.ExecuteToTable(v_strSQL)
            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
                v_strAutoID = dt.Rows(0)(0).ToString
            End If
            seq_no = v_strAutoID
            Dim v_lngTempDate As Long = ConvertDateToNumber2(v_strWorkingDate)
            v_strTempREF = pv_strBranchNo & "2" & v_lngTempDate & "9" & Strings.Right("0000" & CStr(v_strAutoID), Len("0000"))
            Return v_strTempREF
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi GenerateRMRef", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '              & "Error code: System error!" & vbNewLine _
            '              & "Error message: " & ex.Message, EventLogEntryType.Error)
            'Return String.Empty
        End Try
    End Function

#End Region

    ''' <summary>
    ''' </summary>
    ''' <param name="MaTMuc"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Get_TenCQHQThu(ByVal pv_strMaCQThu As String) As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = ""
        Try
            strResult = Get_TenCQThu(pv_strMaCQThu)

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin tên CQHQThu", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
        Return strResult

    End Function
    Public Shared Function GetTT_NH_NHAN(ByVal strMa_NH As String) As String
        Dim strResult As String = "null"
        Dim dt As New DataTable
        Dim strSql As String
        Try
            strSql = "Select  ten_giantiep  From TCS_DM_NH_GIANTIEP_TRUCTIEP" & _
                " Where ma_giantiep='" & strMa_NH & "'"
            strResult = DataAccess.ExecuteSQLScalar(strSql)
            Return strResult
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy trạng thái Ngân hàng nhận", HttpContext.Current.Session("TEN_DN"))
        Finally
            If Not dt Is Nothing Then
                dt.Dispose()
            End If
        End Try
        Return ""
    End Function

    ''' <summary>
    ''' </summary>
    ''' <param name="MaTMuc"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Get_TenMaHQ(ByVal pv_strMaHQ As String) As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = ""
        Try
            strResult = Get_TenHQ(pv_strMaHQ)

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin tên mã hải quan", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '              & "Error code: System error!" & vbNewLine _
            '              & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
        Return strResult

    End Function

    ''' ******************************************************'
    ''' Name: Get_s the ten SHKB.
    ''' Author: ANHLD
    ''' Goals: Lay ten so hieu kho bac
    ''' Date: 24.09.2012
    ''' Event:
    ''' ************************BEGIN******************************'
    ''' <param name="pv_strSHKB">The PV_STR SHKB.</param>
    ''' <returns>System.String.</returns>
    Public Shared Function Get_TenSHKB(ByVal pv_strSHKB As String) As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = ""
        Try
            strResult = Get_TenKH(pv_strSHKB)

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin số hiệu kho bạc", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
        Return strResult

    End Function
    Public Shared Function Get_DBHC_SHKB(ByVal pv_strSHKB As String) As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = ""
        Try
            strResult = Get_DBHC_KB(pv_strSHKB)

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin số hiệu kho bạc", HttpContext.Current.Session.Item("TEN_DN"))

        End Try
        Return strResult

    End Function
    Public Shared Function Get_MaTinh(ByVal strMaXa As String) As String
        Dim cnTenMTM As New DataAccess
        Dim drMT As DataTable
        Dim strResult As String = ""
        Try
            drMT = Get_MaTenTinh(strMaXa)
            If Not drMT Is Nothing Then
                strResult = drMT.Rows(0)("Ma_tinh").ToString()
            End If


        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '    clsCommon.WriteLog(ex, "Lỗi lấy thông tin mã tỉnh", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
        Return strResult

    End Function
    Public Shared Function Get_TenTinh(ByVal strMaXa As String) As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = String.Empty
        Dim drMT As DataTable
        Try
            drMT = Get_MaTenTinh(strMaXa)
            If Not drMT Is Nothing Then
                strResult = drMT.Rows(0)("ma_tinh").ToString() & ";" & drMT.Rows(0)("Ten").ToString()
            End If

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin tên tỉnh", HttpContext.Current.Session.Item("TEN_DN"))
            Return ""
            'Throw ex
        End Try
        Return strResult

    End Function
    Public Shared Function Get_TenTinh_KB_BL(ByVal strMaXa As String) As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = String.Empty
        Dim drMT As DataTable
        Try
            drMT = Get_MaTenTinh(strMaXa)
            If Not drMT Is Nothing Then
                strResult = drMT.Rows(0)("Ten").ToString()
            End If

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin tên tỉnh", HttpContext.Current.Session.Item("TEN_DN"))
            Return ""
            'Throw ex
        End Try
        Return strResult

    End Function
    Public Shared Function GetMaQuanHuyen(ByVal pv_strMaXa As String) As String
        Dim str_result As String = ""
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable

        Dim strSQL As String = " SELECT b.ma_xa, b.ten FROM tcs_dm_xa b WHERE b.ma_cha = '" & pv_strMaXa & "'"
        dt = DataAccess.ExecuteToTable(strSQL)

        If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then
            For i As Integer = 0 To dt.Rows.Count - 1
                str_result &= dt.Rows(i)("ma_xa") & ";" & dt.Rows(i)("TEN") & "|"
            Next
        End If
        Return str_result
    End Function
    Public Shared Function Get_TenQuanHuyen(ByVal strMaXa As String) As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = String.Empty
        Dim drMT As DataTable
        Try
            drMT = Get_TenQuan(strMaXa)
            If Not drMT Is Nothing Then
                strResult = drMT.Rows(0)("Ten").ToString()
            End If

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi lấy thông tin tên tỉnh", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
        Return strResult

    End Function
    Public Shared Function Get_TenTinhTP(ByVal strMaXa As String) As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = String.Empty
        Dim drMT As DataTable
        Try
            drMT = Get_MaTenTinh(strMaXa)
            If Not drMT Is Nothing Then
                strResult = drMT.Rows(0)("ma_xa").ToString() & ";" & drMT.Rows(0)("Ten").ToString()
            End If

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi lấy thông tin tên tỉnh", HttpContext.Current.Session.Item("TEN_DN"))
            strResult = " ; "
            'Throw ex
        End Try
        Return strResult

    End Function
    Public Shared Function Get_TenDBHC(ByVal strMaXa As String) As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = String.Empty
        Dim drMT As DataTable
        Try
            drMT = Get_MaTenTinh(strMaXa)
            If Not drMT Is Nothing Then
                strResult = drMT.Rows(0)("Ten").ToString()
            End If

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin tên tỉnh", HttpContext.Current.Session.Item("TEN_DN"))
            strResult = " ; "
            'Throw ex
        End Try
        Return strResult

    End Function
    ''' ************************END********************************'


    ''' <summary>
    ''' </summary>
    ''' <param name="MaTMuc"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Get_TenLH(ByVal pv_strMaLH As String) As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = ""
        Try
            strResult = Get_TenLHXNK(pv_strMaLH)

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi tên LH", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '              & "Error code: System error!" & vbNewLine _
            '              & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
        Return strResult

    End Function

    Public Shared Function Get_AnHanXNK(ByVal pv_strMaLH As String) As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = ""
        Try
            strResult = "" 'Get_TTAnHanXNK(pv_strMaLH)

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi tên LH", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '              & "Error code: System error!" & vbNewLine _
            '              & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
        Return strResult

    End Function
    ''' <summary>
    ''' </summary>
    ''' <param name="MaTMuc"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Get_TenNNT(ByVal pv_strMaNNT As String) As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = ""
        Try
            strResult = Get_TenNNThue(pv_strMaNNT)

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy tên NNT", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '              & "Error code: System error!" & vbNewLine _
            '              & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
        Return strResult

    End Function

    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			Get_s the cif num.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		10/02/2012 00:00:00
    '''  Mục đích:	    Lay so CIF theo MST
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    ''' <param name="pv_strMaNNT">The PV_STR ma NNT.</param>
    ''' <returns>System.String.</returns>
    Public Shared Function Get_CifNum(ByVal pv_strMaNNT As String) As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = ""
        Try
            strResult = Get_CifNumber(pv_strMaNNT)

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy thông tin số cif", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '              & "Error code: System error!" & vbNewLine _
            '              & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
        Return strResult

    End Function

    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			Get_s the D c_ TS.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		10/02/2012 00:00:00
    '''  Mục đích:	    Lay tham so de chay doi chieu tu dong
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    ''' <returns>System.String.</returns>
    Public Shared Function Get_DC_TS() As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = ""
        Try
            strResult = Get_TS_DC()

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi lấy thông tin tham số đối chiếu", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '              & "Error code: System error!" & vbNewLine _
            '              & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
        Return strResult

    End Function

    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			Get_s the D c_ TS.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		10/12/2012 00:00:00
    '''  Mục đích:	    Lay tham so duong dan webservice hai quan
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    ''' <returns>System.String.</returns>
    Public Shared Function Get_URL_HAIQUAN() As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = ""
        Try
            strResult = GET_URL_HQ()

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi lấy url hải quan", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '              & "Error code: System error!" & vbNewLine _
            '              & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
        Return strResult

    End Function

    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			Get_s the D c_ TS.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		10/12/2012 00:00:00
    '''  Mục đích:	    Lay tham so duong dan webservice hai quan doi chieu
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    ''' <returns>System.String.</returns>
    Public Shared Function Get_URL_HAIQUAN_DC() As String
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = ""
        Try
            strResult = GET_URL_HQ_DC()

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi lấy thông tin url hải quan DC", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '              & "Error code: System error!" & vbNewLine _
            '              & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try
        Return strResult

    End Function

    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			Update_s the D c_ T s_ GUI.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		10/02/2012 00:00:00
    '''  Mục đích:	    Update trang thai sau khi gui doi chieu tu dong
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    ''' <param name="p_bTT">The P_B TT.</param>
    Public Shared Sub Update_DC_TS_GUI(ByVal p_bTT As String)
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = ""
        Try
            strResult = "UPDATE TCS_THAMSO_DC SET GUI_LAN_CUOI=SYSDATE, TT_GUI_CUOI='" & p_bTT & "'"
            'Dim v_daTs As HeThong.daThamso = New HeThong.daThamso
            'v_daTs.UpdateThamso_DC(strResult)
            DataAccess.ExecuteNonQuery(strResult, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi update trạng thái sau khi đối chiếu tự động", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '              & "Error code: System error!" & vbNewLine _
            '              & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try

    End Sub

    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			Update_s the D c_ T s_ NHAN.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		10/02/2012 00:00:00
    '''  Mục đích:	    Cap nhat trang thai sau khi nhan doi chieu
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    ''' <param name="p_bTT">The P_B TT.</param>
    Public Shared Sub Update_DC_TS_NHAN(ByVal p_bTT As String)
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = ""
        Try
            strResult = "UPDATE TCS_THAMSO_DC SET NHAN_LAN_CUOI=SYSDATE, TT_NHAN_CUOI='" & p_bTT & "'"
            DataAccess.ExecuteNonQuery(strResult, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  strResult(clsCommon.WriteLog(ex, "Lỗi update sau khi nhận đối chiếu", HttpContext.Current.Session.Item("TEN_DN")))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '              & "Error code: System error!" & vbNewLine _
            '              & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try

    End Sub
    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			Update_s the D c_ T s_ NHAN.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		10/05/2012 00:00:00
    '''  Mục đích:	    Cap nhat trang thai tu dong doi chieu
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    ''' <param name="p_bTT">The P_B TT.</param>
    Public Shared Sub Update_DC_TS_AUTO_RUN(ByVal p_bTT As String)
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = ""
        Try
            strResult = "UPDATE TCS_THAMSO_DC SET AUTO_RUN='" & p_bTT & "'"
            DataAccess.ExecuteNonQuery(strResult, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi cập nhật trạng thái đối chiếu tự động", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '              & "Error code: System error!" & vbNewLine _
            '              & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try

    End Sub

    ''' ***************BEGIN****************************************'
    ''' ---------------CREATE--------------------------
    '''  Tên:			INSERs the t_ LO g_ AUT o_ DC.
    '''  Người viết:	ANHLD
    '''  Ngày viết:		10/02/2012 00:00:00
    '''  Mục đích:	    Ghi log khi doi chieu tu dong
    ''' ---------------MODIFY--------------------------
    '''  Người sửa:	
    '''  Ngày sửa: 	
    '''  Mục đích:	
    ''' ---------------PARAM--------------------------
    ''' <param name="p_bTT">The P_B TT.</param>
    Public Shared Sub INSERT_LOG_AUTO_DC(ByVal p_bTT As String, ByVal p_strLoaiDC As String, ByVal p_strErrCode As String, ByVal p_strErrMsg As String)
        Dim cnTenMTM As New DataAccess
        Dim strResult As String = ""
        Try
            strResult = "INSERT INTO TCS_LOG_AUTO_DC(STT,NGAY_TH,TRANG_THAI,LOAI_DC,ERROR_CODE,ERROR_MSG) VALUES(SEQ_LOG_AUTO_DC.NEXTVAL, SYSDATE, '" & p_bTT & "','" & p_strLoaiDC & "','" & p_strErrCode & "','" & p_strErrMsg & "')"
            DataAccess.ExecuteNonQuery(strResult, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi khi ghi log đối chiếu", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '              & "Error code: System error!" & vbNewLine _
            '              & "Error message: " & ex.Message, EventLogEntryType.Error)
        End Try

    End Sub
    Public Shared Function checkGioGD(Optional ByVal strLoaiDC As String = "0") As String
        Try
            Dim strSQL As String
            Dim dt As DataTable
            If strLoaiDC = "0" Then
                strSQL = " select * from ( " & _
                         " select '1' from dual where sysdate < to_date(to_char(sysdate,'dd/mm/yyyy')||(select DECODE(LENGTH(gio_bat_dau),2,gio_bat_dau || ':00',gio_bat_dau) from tcs_thamso_dc),'dd/mm/yyyy HH24:mi') " & _
                         " UNION" & _
                         " select '0' from dual where sysdate >= to_date(to_char(sysdate,'dd/mm/yyyy')||(select DECODE(LENGTH(gio_bat_dau),2,gio_bat_dau || ':00',gio_bat_dau) from tcs_thamso_dc),'dd/mm/yyyy HH24:mi'))"
            Else
                strSQL = " select * from ( " & _
                         " select '1' from dual where sysdate < to_date(to_char(sysdate,'dd/mm/yyyy')||(select DECODE(LENGTH(gio_bat_dau),2,gio_bat_dau || ':10',substr(gio_bat_dau,0,3) || (substr(gio_bat_dau,4,2)+10)) from tcs_thamso_dc),'dd/mm/yyyy HH24:mi') " & _
                         " UNION" & _
                         " select '0' from dual where sysdate >= to_date(to_char(sysdate,'dd/mm/yyyy')||(select DECODE(LENGTH(gio_bat_dau),2,gio_bat_dau || ':10',substr(gio_bat_dau,0,3) || (substr(gio_bat_dau,4,2)+10)) from tcs_thamso_dc),'dd/mm/yyyy HH24:mi'))"
            End If
            dt = DataAccess.ExecuteToTable(strSQL)
            If Not dt Is Nothing Then
                Return dt.Rows(0)(0).ToString
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) ' clsCommon.WriteLog(ex, "Lỗi kiểm tra giờ GD", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
            'LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
            '              & "Error code: System error!" & vbNewLine _
            '              & "Error message: " & ex.Message, EventLogEntryType.Error)
            Return "1"
        End Try
    End Function
    Public Shared Sub TCS_UpdateTrangThai2(ByVal pv_strSHKB As String, ByVal pv_strSoCT As String, _
                                ByVal pv_strSoBT As String, ByVal pv_strTrangThai As String, _
                                Optional ByVal pv_strLyDoHuy As String = "", Optional ByVal pv_strNgayKB As String = "", _
                                Optional ByVal pv_strLyDoCTra As String = "", Optional ByVal pv_strMaNv2 As String = "", _
                                Optional ByVal pv_strMaNvHuy As String = "")
        Dim key As New KeyCTu
        Try
            Dim v_strNgayKB As String
            Dim v_strMaNv As String
            Dim v_strClau As String
            Dim v_strSql As String = ""

            If pv_strMaNv2.Trim.Length > 0 Then
                v_strMaNv = pv_strMaNv2
                v_strClau = "ma_ks_2 = '" & v_strMaNv & "', ngay_ks_2 = sysdate,"
            Else
                v_strMaNv = pv_strMaNvHuy
                v_strClau = "ma_nv_huy = '" & v_strMaNv & "', ngay_huy = sysdate, "
            End If

            If pv_strNgayKB.Length <> 0 Then
                v_strNgayKB = pv_strNgayKB
            Else
                v_strNgayKB = TCS_GetNgayLV(v_strMaNv)
            End If

            v_strSql = "update tcs_baolanh_hdr set trang_thai = '" & pv_strTrangThai & "', " & v_strClau & " " & _
                     " ly_do_ctra = '" & pv_strLyDoCTra & "' " & _
                     IIf(pv_strLyDoHuy.Length > 0, ", ly_do_huy = '" & pv_strLyDoHuy & "'", "") & _
                     " Where Ngay_KB = " & v_strNgayKB & _
                     " And So_BT = " & pv_strSoBT & _
                     " And so_ct = '" & pv_strSoCT & "'" & _
                     " And shkb = '" & pv_strSHKB & "'"
            DataAccess.ExecuteNonQuery(v_strSql, CommandType.Text)
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "Lỗi cập nhật trạng thái bảo lãnh", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Sub
    Public Shared Function GET_TK_KH_NH(ByVal pv_strInput As String) As String
        Dim strReturn As String = ""
        Dim charsplit As String = "*"

        Try
            Dim arrParams() As String = pv_strInput.Replace("~", "").Split(charsplit)

            For i_arr As Integer = 0 To arrParams.Length - 1
                If arrParams(i_arr).ToString().Split("=")(0).ToString() = "res_Result_Code" Then
                    If Not arrParams(i_arr).ToString().Split("=")(1).ToString() = "00000" Then
                        Return "<PARAMETER><PARAMS><BANKACCOUNT></BANKACCOUNT><ACY_AVL_BAL></ACY_AVL_BAL><CRR_CY_CODE></CRR_CY_CODE><RETCODE>" & arrParams(i_arr).ToString().Split("=")(1).ToString() & "</RETCODE><ERRCODE>" & arrParams(i_arr).ToString().Split("=")(1).ToString() & "</ERRCODE></PARAMS></PARAMETER>"
                    End If
                End If
            Next

            Dim res_Result_Code As String = arrParams(0).ToString().Split("=")(1).ToString()
            Dim res_Echo_Field As String = arrParams(1).ToString().Split("=")(1).ToString()
            Dim res_Ref_No As String = arrParams(2).ToString().Split("=")(1).ToString()
            Dim res_Acc_No As String = arrParams(3).ToString().Split("=")(1).ToString()
            Dim res_Acc_CCY_Cd As String = arrParams(4).ToString().Split("=")(1).ToString()
            Dim res_Curr_Bal As String = arrParams(5).ToString().Split("=")(1).ToString()
            Dim res_Avail_Bal As String = arrParams(6).ToString().Split("=")(1).ToString()
            Dim res_Fin_Status As String = arrParams(7).ToString().Split("=")(1).ToString()
            Dim res_Legal_Status As String = arrParams(8).ToString().Split("=")(1).ToString()
            Dim res_AcInact_Status As String = arrParams(9).ToString().Split("=")(1).ToString()
            Dim res_Contigent_Flg As String = arrParams(10).ToString().Split("=")(1).ToString()
            Dim res_Blockamt As String = arrParams(11).ToString().Split("=")(1).ToString()
            Dim res_Casa_Acc_Pos_Cd_Desc As String = arrParams(12).ToString().Split("=")(1).ToString()
            Dim res_Acc_CCY As String = arrParams(13).ToString().Split("=")(1).ToString()
            Dim req_Exists_Flg As String = arrParams(14).ToString().Split("=")(1).ToString()
            strReturn = "<PARAMETER><PARAMS><BANKACCOUNT>" & res_Acc_No & "</BANKACCOUNT>" & _
                        "<ACY_AVL_BAL>" & res_Avail_Bal & "</ACY_AVL_BAL><CRR_CY_CODE>" & res_Acc_CCY_Cd & "</CRR_CY_CODE>" & _
                        "<RETCODE>" & res_Result_Code & "</RETCODE><ERRCODE>" & res_Result_Code & "</ERRCODE></PARAMS></PARAMETER>"
        Catch ex As Exception
            Return "<PARAMETER><PARAMS><BANKACCOUNT></BANKACCOUNT><ACY_AVL_BAL></ACY_AVL_BAL><CRR_CY_CODE></CRR_CY_CODE><RETCODE>99</RETCODE><ERRCODE>" & ex.Message.ToString() & "</ERRCODE></PARAMS></PARAMETER>"
        End Try
        Return strReturn
    End Function
    Public Shared Sub PAYMENT_SHB(ByVal pv_strInput As String, ByRef rm_ref_no As String, ByRef so_du As String, _
                                       ByRef err_code As String, ByRef err_msg As String)
        Dim strReturn As String = ""
        Dim charsplit As String = "*"
        Dim hdr_Tran_Id As String = ""
        Dim res_Status As String = ""
        Dim res_Result_Code As String = ""
        Dim res_Ref_No As String = ""
        Dim res_Avail_Bal As String = ""
        Dim res_Curr_Bal As String = ""
        Dim res_Acc_CCY_Cd As String = ""
        Dim res_Lcy_Amt As String = ""
        Try
            Dim arrParams() As String = pv_strInput.Replace("~", "").Split(charsplit)

            For i_arr As Integer = 0 To arrParams.Length - 1
                If arrParams(i_arr).ToString().Split("=")(0).ToString() = "res_Result_Code" Then
                    If Not arrParams(i_arr).ToString().Split("=")(1).ToString() = "00000" Then
                        err_code = arrParams(i_arr).ToString().Split("=")(1).ToString()
                        Exit Sub
                    End If
                End If
            Next

            hdr_Tran_Id = arrParams(0).ToString().Split("=")(1).ToString()
            res_Status = arrParams(1).ToString().Split("=")(1).ToString()
            res_Result_Code = arrParams(2).ToString().Split("=")(1).ToString()
            res_Ref_No = arrParams(3).ToString().Split("=")(1).ToString()
            res_Avail_Bal = arrParams(4).ToString().Split("=")(1).ToString()
            res_Curr_Bal = arrParams(5).ToString().Split("=")(1).ToString()
            res_Acc_CCY_Cd = arrParams(6).ToString().Split("=")(1).ToString()
            res_Lcy_Amt = arrParams(7).ToString().Split("=")(1).ToString()
            rm_ref_no = res_Ref_No
            so_du = res_Curr_Bal
            err_code = res_Result_Code
        Catch ex As Exception
            err_code = res_Result_Code
            err_msg = ex.Message.ToString()
        End Try
    End Sub
    Public Shared Function GetMA_NguyenTe(ByVal strNT As String) As String
        Dim strResult As String = "null"
        Dim dt As New DataTable
        Dim strSql As String
        Try
            strSql = "Select  CODE_TIENTE  From tcs_dm_nguyente" & _
                " Where ma_NT='" & strNT & "'"
            strResult = DataAccess.ExecuteSQLScalar(strSql)
            Return strResult
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Lỗi lấy mã nguyên tệ", HttpContext.Current.Session("TEN_DN"))
        Finally
            If Not dt Is Nothing Then
                dt.Dispose()
            End If
        End Try
        Return ""
    End Function
    Public Shared Function GetMoTa_TrangThai_NNTDT(ByVal strTrang_Thai As String) As String
        Try
            Dim result As String = ""
            Dim sql As String = "select mota from tcs_dm_trangthai_ntdt where trang_thai='" & strTrang_Thai & "' AND GHICHU='NNT'"
            Dim dr As IDataReader = DataAccess.ExecuteDataReader(sql, CommandType.Text)
            If dr.Read Then
                result = dr("MOTA").ToString
            End If
            'Select Case strTrang_Thai
            '    Case "00"
            '        result = "Đăng ký mới"
            '    Case "01"
            '        result = "Chờ kiểm soát"
            '    Case "02"
            '        result = "Hủy - Chờ duyệt"
            '    Case "03"
            '        result = "Thành công"
            '    Case "04"
            '        result = "Hủy"
            '    Case "05"
            '        result = "Điều chỉnh"
            '    Case "06"
            '        result = "Ngừng"
            '    Case "07"
            '        result = "Chuyển trả"
            '    Case Else
            '        result = strTrang_Thai
            'End Select
            Return result
        Catch ex As Exception
            Return strTrang_Thai
        End Try
    End Function
    Public Shared Function GetMoTa_TrangThai_CT_NTDT(ByVal strTrang_Thai As String) As String
        Try
            Dim result As String = ""
            Dim sql As String = "select mota from tcs_dm_trangthai_ntdt where trang_thai='" & strTrang_Thai & "' AND GHICHU='CT'"

            Dim dt As DataTable = New DataTable()
            dt = DataAccess.ExecuteReturnDataSet(sql, CommandType.Text).Tables(0)
            If dt.Rows.Count > 0 Then
                result = dt.Rows(0)("MOTA").ToString()
            End If
            'Dim dr As IDataReader = DataAccess.ExecuteDataReader(sql, CommandType.Text)
            'If dr.Read Then
            '    result = dr("MOTA").ToString
            'End If
            'Select Case strTrang_Thai
            '    Case "00"
            '        result = "Đăng ký mới"
            '    Case "01"
            '        result = "Chờ kiểm soát"
            '    Case "02"
            '        result = "Hủy - Chờ duyệt"
            '    Case "03"
            '        result = "Thành công"
            '    Case "04"
            '        result = "Hủy"
            '    Case "05"
            '        result = "Điều chỉnh"
            '    Case "06"
            '        result = "Ngừng"
            '    Case "07"
            '        result = "Chuyển trả"
            '    Case Else
            '        result = strTrang_Thai
            'End Select
            Return result
        Catch ex As Exception
            Return strTrang_Thai
        End Try
    End Function

    'Public Shared Function GetWorkingDate2() As String
    '    Dim dsTSHT As DataSet
    '    Dim strSQL As String
    '    Try
    '        strSQL = "SELECT TEN_TS,GIATRI_TS FROM TCS_THAMSO WHERE TEN_TS='NGAY_LV' AND MA_DTHU='" & TCS_GetMaDT(CType(HttpContext.Current.Session("User"), Integer)) & "'"
    '        dsTSHT = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text)
    '        Return GetString(dsTSHT.Tables(0).Rows(0)("GIATRI_TS")).Trim()
    '    Catch ex As Exception
    '        clsCommon.WriteLog(ex, "Lỗi lấy thông tin ngày làm việc", HttpContext.Current.Session.Item("TEN_DN"))
    '        Throw ex
    '    End Try
    'End Function

End Class

