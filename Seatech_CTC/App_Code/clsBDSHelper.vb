﻿Imports Microsoft.VisualBasic
Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Math
Imports System

Public Class BDSHelper
    Private pv_objConn As MySqlConnection
    Private pv_strHostName As String
    Private pv_strDatabaseName As String
    Private pv_strUserName As String
    Private pv_strPassword As String
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Public Sub New(ByVal pv_strHostName As String, ByVal pv_strDatabaseName As String, _
                 ByVal pv_strUserName As String, ByVal pv_strPassword As String)
        Try
            pv_strHostName = pv_strHostName
            pv_strDatabaseName = pv_strDatabaseName
            pv_strUserName = pv_strUserName
            pv_strPassword = pv_strPassword
            pv_objConn = New MySqlConnection("server=" & pv_strHostName & "; user id=" & pv_strUserName & "; password=" _
                                & pv_strPassword & "; database=" & pv_strDatabaseName & "; pooling=false;")

        Catch ex As Exception
            Log.Error(ex.Message & "-" & ex.StackTrace) '   clsCommon.WriteLog(ex, "Initializing BDSHelper")
        End Try
    End Sub

    Public Function ConnectDB() As Boolean
        Try
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function IncreaseTotal(ByVal pv_strTotalID As String, ByVal pv_strTellerID As String, ByVal pv_strAmount As String) As Boolean
        Dim v_strSQL As String
        'CongNT comment test
        'Return True
        'CongNT comment test
        Try
            pv_strAmount = CStr(Round(CDbl(pv_strAmount), 0))
            If (CheckRowExits(pv_strTotalID, pv_strTellerID)) Then
                v_strSQL = "UPDATE TOTAL SET AMOUNT=AMOUNT + " + pv_strAmount + ",CNT=CNT + 1 WHERE TOTALID='" + pv_strTotalID + "' AND USERID='" + pv_strTellerID + "' AND CURR='VND'"
                Dim v_objCommand As MySqlCommand = New MySqlCommand("IncreaseTotal", pv_objConn)
                v_objCommand.CommandType = CommandType.Text
                v_objCommand.CommandText = v_strSQL
                v_objCommand.Connection.Open()
                v_objCommand.ExecuteNonQuery()
                v_objCommand.Connection.Close()
            Else
                v_strSQL = "INSERT INTO TOTAL(USERID,CURR,TOTALID,AMOUNT,CNT) VALUES ('" + pv_strTellerID + "','VND','" + pv_strTotalID + "'," + pv_strAmount + ",'1')"
                Dim v_objCommand As MySqlCommand = New MySqlCommand("IncreaseTotal", pv_objConn)
                v_objCommand.CommandType = CommandType.Text
                v_objCommand.CommandText = v_strSQL
                v_objCommand.Connection.Open()
                v_objCommand.ExecuteNonQuery()
                v_objCommand.Connection.Close()
            End If
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '  clsCommon.WriteLog(ex, "UpdateBDS:")
        Finally
            pv_objConn.Close()
        End Try
        Return True
    End Function

    Private Function CheckRowExits(ByVal pv_strTotalID As String, ByVal pv_strTellerID As String) As Boolean
        Dim oDA As MySqlDataAdapter = Nothing
        Dim ds As DataSet = New DataSet
        Dim v_strSQL As String = "SELECT * FROM TOTAL WHERE TOTALID='" + pv_strTotalID + "' AND USERID='" + pv_strTellerID + "' AND CURR='VND'"
        Try
            oDA = New MySqlDataAdapter(v_strSQL, pv_objConn)
            oDA.Fill(ds)
            If ds.Tables(0).Rows.Count > 0 Then
                Return True
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If Not oDA Is Nothing Then oDA.Dispose()
            If Not pv_objConn Is Nothing Then pv_objConn.Dispose()
        End Try
        Return False
    End Function

    Public Function DecreaseTotal(ByVal pv_strTotalID As String, ByVal pv_strTellerID As String, ByVal pv_strAmount As String) As Boolean
        Dim v_strSQL As String
        Try
            If (CheckRowExits(pv_strTotalID, pv_strTellerID)) Then
                v_strSQL = "UPDATE TOTAL SET AMOUNT=AMOUNT - " + pv_strAmount + ",CNT=CNT - 1 WHERE TOTALID='" + pv_strTotalID + "' AND USERID='" + pv_strTellerID + "' AND CURR='VND'"
                Dim v_objCommand As MySqlCommand = New MySqlCommand("IncreaseTotal", pv_objConn)
                v_objCommand.CommandType = CommandType.Text
                v_objCommand.CommandText = v_strSQL
                v_objCommand.Connection.Open()
                v_objCommand.ExecuteNonQuery()
                v_objCommand.Connection.Close()
            End If
        Catch ex As Exception
            Throw ex
        Finally
            pv_objConn.Close()
        End Try
        Return True
    End Function

    Public Function GetCustomerImge(ByVal pv_strAccount As String, ByVal pv_strBranch As String) As DataSet
        Dim oDA As MySqlDataAdapter = Nothing
        Dim arrContent As Byte() = Nothing
        Dim ds As New DataSet
        Dim v_objCommand As MySqlCommand
        Try
            pv_objConn.Open()
            v_objCommand = New MySqlCommand("SELECT IMAGENO,IDTYPE,IDNO,SIGNATURE,ACCNO,'" & pv_strBranch _
                        & "' BRANCH FROM IMAGE WHERE ACCNO='" + pv_strAccount.Replace("-", "") + "'", pv_objConn)
            oDA = New MySqlDataAdapter(v_objCommand)
            oDA.Fill(ds)
            pv_objConn.Close()
        Catch ex As Exception
            Throw ex
        Finally
            pv_objConn.Close()
        End Try
        Return ds
    End Function

    Public Function GetCustomerSignature(ByVal pv_acctno As String, ByVal pv_imageno As String) As Byte()
        Dim oDA As MySqlDataAdapter = Nothing
        Dim arrContent As Byte() = Nothing
        Dim ds As New DataSet
        Dim dr As DataRow
        Dim v_objCommand As MySqlCommand
        Try
            pv_objConn.Open()
            v_objCommand = New MySqlCommand("SELECT SIGNATURE FROM IMAGE WHERE ACCNO='" & pv_acctno & "' and imageno='" + pv_imageno + "'", pv_objConn)
            oDA = New MySqlDataAdapter(v_objCommand)
            oDA.Fill(ds)
            If ds.Tables(0).Rows.Count > 0 Then
                dr = ds.Tables(0).Rows(0)
                arrContent = CType(dr.Item("SIGNATURE"), Byte())
            End If
            pv_objConn.Close()
        Catch ex As Exception
            Throw ex
        Finally
            pv_objConn.Close()
        End Try
        Return arrContent
    End Function
End Class
