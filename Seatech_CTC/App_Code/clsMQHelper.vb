﻿Imports System
Imports System.Data
Imports BIDVCRYPTOLIBLib
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports System.Diagnostics
Imports IBM.WMQ
Imports System.Collections
Imports Microsoft.VisualBasic

Public Class clsMQHelper
    Private mv_strHostIP As String
    Private mv_HostPort As String
    Private mv_strQmgrName As String
    Private mv_strQInputName As String
    Private mv_strQOutputName As String
    Private mv_strChannelName As String

    Private mv_objQMgr As MQQueueManager
    Private mv_objQueueIn As MQQueue
    Private mv_objQueueOut As MQQueue
    Private mv_objGetOptions As MQGetMessageOptions
    Private mv_objPutOptions As MQPutMessageOptions
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Public Sub New()
        InitParam()
    End Sub

    Private Sub InitParam()
        Try
            mv_strHostIP = Configuration.ConfigurationManager.AppSettings("mq.hostIP").ToString
            mv_HostPort = Configuration.ConfigurationManager.AppSettings("mq.hostPort").ToString
            mv_strQmgrName = Configuration.ConfigurationManager.AppSettings("mq.qmgrName").ToString
            mv_strQInputName = Configuration.ConfigurationManager.AppSettings("mq.qlinputName").ToString
            mv_strQOutputName = Configuration.ConfigurationManager.AppSettings("mq.qloutputName").ToString
            mv_strChannelName = Configuration.ConfigurationManager.AppSettings("mq.svrchannelName").ToString
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
        End Try
    End Sub

    Private Function ConnectMQ() As Boolean
        Try
            Dim queueProperties As Hashtable = New Hashtable()
            queueProperties(MQC.HOST_NAME_PROPERTY) = mv_strHostIP
            queueProperties(MQC.PORT_PROPERTY) = CInt(mv_HostPort)
            queueProperties(MQC.CHANNEL_PROPERTY) = mv_strChannelName

            mv_objQMgr = New MQQueueManager(mv_strQmgrName, queueProperties)

            Dim openInputOptions As Integer = MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING
            Dim openOutputOptions As Integer = MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING

            mv_objQueueIn = mv_objQMgr.AccessQueue(mv_strQInputName, openInputOptions)
            mv_objQueueOut = mv_objQMgr.AccessQueue(mv_strQOutputName, openOutputOptions)


            mv_objGetOptions = New MQGetMessageOptions()
            mv_objGetOptions.Options += MQC.MQGMO_WAIT
            mv_objGetOptions.WaitInterval = 60000

            mv_objPutOptions = New MQPutMessageOptions()

        Catch ex As Exception
           log.Error(ex.Message & "-" & ex.StackTrace)'
            Return False
        End Try
        Return True
    End Function

    Private Function DisconnectMQ() As Boolean
        Try
            If Not mv_objQueueOut Is Nothing Then
                mv_objQueueOut.Close()
            End If
            If Not mv_objQueueIn Is Nothing Then
                mv_objQueueIn.Close()
            End If
            If Not mv_objQMgr Is Nothing Then
                mv_objQMgr.Disconnect()
            End If
        Catch ex As Exception
          log.Error(ex.Message & "-" & ex.StackTrace)'
            Return False
        End Try
        Return True
    End Function

    Public Function PutAndGetMQMessage(ByVal pv_strMessage As String) As String
        Dim v_strRequest As String = pv_strMessage
        Dim v_strResponse As String = ""
        Try
            If ConnectMQ() Then
                Dim mqPutMsg As MQMessage = New MQMessage()
                mqPutMsg.WriteUTF(v_strRequest)
                mv_objQueueIn.Put(mqPutMsg, mv_objPutOptions)
                Dim mqGetMsg As MQMessage = New MQMessage()
                mqGetMsg.CorrelationId = mqPutMsg.MessageId
                mv_objQueueOut.Get(mqGetMsg, mv_objGetOptions)
                v_strResponse = mqGetMsg.ReadString(mqGetMsg.MessageLength)
            End If
            DisconnectMQ()
        Catch ex As IBM.WMQ.MQException
            log.Error(ex.Message & "-" & ex.StackTrace) '
            If (ex.Reason = MQC.MQRC_NO_MSG_AVAILABLE) Then
                log.Error("Error source: " & ex.Source & vbNewLine _
                 & "Error code: System error!" & vbNewLine _
                 & "Error message: " & ex.Message)
            Else
                log.Error("Error source: " & ex.Source & vbNewLine _
                 & "Error code: System error!" & vbNewLine _
                 & "Error message: " & ex.Message)
                Return v_strResponse
            End If
        End Try
        Return v_strResponse
    End Function

End Class
