﻿Imports System.Collections.Generic
Imports System.Data
Imports CoreBankSV.EsbServiceReference
Imports System

Public Class ClsUtilities
    Public Shared ReadOnly LOG_ERROR As String = "Error"
    Public Shared ReadOnly LOG_INFO As String = "Info"
    Public Shared ReadOnly LOG_WARN As String = "Warn"

    'Public Function ReturnListOfCaAccount(cifNo As String, teller As String, branchCode As String, loaiHs As String, taiKhoan As String, Optional isAddNew As Boolean = True) As List(Of CAAccount)
    '    Dim chanel As String = String.Empty
    '    Dim transDate As String = DateTime.Now.ToString("ddMMyy")

    '    Const accType = "D"
    '    Dim listAcount = New List(Of String)
    '    Dim listCaAccount = New List(Of CAAccount)
    '    Dim clsCoreBank = New CorebankSV.ClsCoreBank
    '    Dim strSql As String = "SELECT CUR_TAIKHOAN_TH FROM TCS_DM_NTDT_HQ_311 WHERE cifno='" & cifNo & "'AND TRANGTHAI in('10','11','12','13','09','04') "
    '    strSql &= "UNION ALL SELECT CUR_TAIKHOAN_TH FROM TCS_DM_NTDT_HQ WHERE cifno='" & cifNo & "' AND TRANGTHAI in('10','11','12','13','09','04')"
    '    If isAddNew Then
    '        strSql &= "Union all select TAIKHOAN_TH CUR_TAIKHOAN_TH from tcs_dm_ntdt_hq_311 where TAIKHOAN_TH='" + taiKhoan.Trim.PadLeft(14, CType("0", Char)) + "' and loai_hs in ('1','2') and trangthai='04'"
    '    End If


    '    Try
    '        Dim ds As DataSet = VBOracleLib.DataAccess.ExecuteReturnDataSet(strSql, CommandType.Text)
    '        Dim rows As Integer = ds.Tables(0).Rows.Count - 1
    '        For i As Integer = 0 To rows
    '            listAcount.Add(ds.Tables(0).Rows(i)("CUR_TAIKHOAN_TH").ToString)
    '        Next
    '        For Each item As String In listAcount
    '            Dim ca = New CAAccount
    '            Dim result As accountInfoInquiryResponse
    '            Dim accountNo As String = item.Trim
    '            result = clsCoreBank.GetCustomerByAccount(chanel, teller, transDate, accountNo, branchCode, accType, "311", "")
    '            If result.return.respMessage.respCode.Equals("0") Then
    '                ca.accountNumber = result.return.accountInfoInquiryDomain.accountNo
    '                ca.accountName = result.return.accountInfoInquiryDomain.accountName.Replace("&", "-")
    '                ca.acctType = result.return.accountInfoInquiryDomain.accountType
    '                ca.status = "USED"
    '                listCaAccount.Add(ca)
    '                'Else
    '                '    ca = Nothing
    '                '    listCaAccount.Add(ca)
    '            End If
    '        Next

    '    Catch ex As Exception

    '    End Try
    '    Return listCaAccount
    'End Function
    Public Function ReturnListBranch(maNhanVien As String, ByRef phan_cap As String) As DataSet
        Dim dt = New DataSet
        Dim sqlPhanCap = "Select cn.phan_cap phan_cap,cn.id ma_cn from TCS_DM_NhanVien nv,tcs_dm_chinhanh cn where cn.id=nv.ma_cn and ma_nv='" + maNhanVien + "' "

        Dim ds = VBOracleLib.DataAccess.ExecuteReturnDataSet(sqlPhanCap, CommandType.Text)
        Dim strPhanCap = ds.Tables(0).Rows(0)("phan_cap").ToString
        Dim subBranchId = ds.Tables(0).Rows(0)("ma_cn").ToString
        Dim sqlSearchBranchId = "select branch_id  from tcs_dm_chinhanh  where id ='" + subBranchId + "' "
        Dim dsBranchId = VBOracleLib.DataAccess.ExecuteReturnDataSet(sqlSearchBranchId, CommandType.Text)
        Dim branchId = dsBranchId.Tables(0).Rows(0)("branch_id").ToString
        Dim sqlBranch = ""
        Select Case strPhanCap
            Case "1"
                sqlBranch = "SELECT a.branch_id ,a.name branch_name FROM tcs_dm_chinhanh a where a.phan_cap in ('1','2')  order by a.branch_id "
            Case "2"
                sqlBranch = "SELECT a.branch_id ,a.name branch_name FROM tcs_dm_chinhanh a where a.branch_id='" + branchId + "' and a.phan_cap ='2' order by a.branch_id"
                'Case "3"
                '    sqlBranch = "SELECT a.branch_id branch_id,a.id, a.id ||'-'||a.name name FROM tcs_dm_chinhanh a where branch_id='" + branchId + "' and id='" + subBranchId + "' order by a.id"
            Case Else

        End Select
        phan_cap = strPhanCap
        dt = VBOracleLib.DataAccess.ExecuteReturnDataSet(sqlBranch, CommandType.Text)
        Return dt
    End Function
    Public Function UpdateInfomationCustomer(id311 As String, strTenNguoiNopThue As String, strDiaChi As String, strSoGiayTo As String, strHoVaTen As String, strNgaySinh As String, strNguyenQuan As String, strSdt As String, strEmail As String) As Boolean
        Dim flag = True
        Dim ngaysinh = strNgaySinh.Split(CType("/", Char))(2) & strNgaySinh.Split(CType("/", Char))(1) & strNgaySinh.Split(CType("/", Char))(0)
        Dim sqlInsert As String = "UPDATE TCS_DM_NTDT_HQ_311 SET TEN_DV = '" + strTenNguoiNopThue + "', DIACHI = '" + strDiaChi + "', SO_CMT = '" + strSoGiayTo + "',HO_TEN = '" + strHoVaTen + "',NGAYSINH = TO_DATE ('" + ngaysinh + "', 'RRRRMMDDHH24MISS'),NGUYENQUAN = '" + strNguyenQuan + "' where id ='" + id311 + "'"

        Dim i = VBOracleLib.DataAccess.ExecuteNonQuery(sqlInsert, CommandType.Text)
        If i > 0 Then
            flag = True
        Else
            flag = False
        End If

        Return flag
    End Function
End Class
