﻿Imports System
Imports System.Data
Imports BIDVCRYPTOLIBLib
Imports Business.Common.mdlCommon
Imports VBOracleLib
Imports System.Diagnostics
Imports Business_HQ
Imports Business
'Imports CoreBankSV.EsbServiceReference
Imports Microsoft.VisualBasic
Imports System.Text
Imports System.Reflection
Imports System.Web.UI.WebControls.Expressions
'Imports CorebankSV

Public Class clsCoreBankUtilMSB
    Private Const gc_FORMAT_REFNO As String = "00000000"
    Private Const MSG_CODE_BALANCE_INQ As String = "TX001"
    Private Const MSG_CODE_PAYMENT As String = "TX002"
    Private Const MSG_CODE_CANCEL_PAYMENT As String = "TX003"
    Private Const MSG_PAYMENT_REMARK_HDR As String = "THNS-"
    Private mv_strMSBTSBDS As String


    'Private Shared clsCoreBank As CorebankSV.ClsCoreBank
    Private _chanel As String
    Private _telle As String
    Private _branchCode As String
    Private transDate As String = DateTime.Now.ToString("dd/MM/yy")

    Public Property telle As String

        Get
            Return _telle
        End Get

        Set(value As String)
            _telle = value
        End Set

    End Property

    Public Property branchCode As String

        Get
            Return _branchCode
        End Get

        Set(value As String)
            _branchCode = value
        End Set

    End Property

    'Public Function GetCustomerInfoInstance(ByVal accountNo As String, ByRef descAccount As String, ByRef availBalance As String, Optional ByRef v_strErrorCode As String = "", Optional ByRef v_strErrorMessage As String = "", Optional ByVal v_Type As String = "") As infTK_KH_TH
    '    Dim m_infTK_KH_TH As New infTK_KH_TH
    '    Try

    '        If accountNo Is Nothing OrElse accountNo.Length = 0 Then Return m_infTK_KH_TH
    '        Dim v_strRetAccountName As String = String.Empty
    '        Dim v_strCertCode As String = String.Empty
    '        Dim v_strCertType As String = String.Empty
    '        Dim v_strBalance As String = String.Empty
    '        Dim v_strBranchNo As String = String.Empty
    '        Dim v_strCif_no As String = String.Empty
    '        ' Kết nối theo esb mới
    '        '    ''vinhvv
    '        Dim v_objESB As New CorebankSV.ClsCoreBank
    '        Dim accountInfoInquiryResponse1 As accountInfoInquiryResponse = New CoreBankSV.EsbServiceReference.accountInfoInquiryResponse
    '        chanel = ""
    '        Try
    '            accountInfoInquiryResponse1 = clsCoreBank.GetCustomerByAccount(chanel, telle, transDate, accountNo, branchCode, "D", "TTQ", "")
    '            If Not accountInfoInquiryResponse1 Is Nothing Then
    '                If accountInfoInquiryResponse1.return.respMessage.respCode = "0" Then
    '                    v_strRetAccountName = accountInfoInquiryResponse1.return.accountInfoInquiryDomain.accountName.Replace("&", "-")
    '                    'vinhvv
    '                    'chua biet v_strCertCode, v_strCertType la gi?
    '                    v_strCertCode = ""
    '                    v_strCertType = ""
    '                    'vinhvv
    '                    v_strBalance = accountInfoInquiryResponse1.return.accountInfoInquiryDomain.availableBal
    '                    v_strBranchNo = accountInfoInquiryResponse1.return.accountInfoInquiryDomain.branchCode
    '                    v_strCif_no = accountInfoInquiryResponse1.return.accountInfoInquiryDomain.cifNo
    '                    descAccount = v_strBranchNo & "-" & v_strCif_no & "-" & v_strRetAccountName & "-" & v_strCertCode & "-" & accountInfoInquiryResponse1.return.accountInfoInquiryDomain.currency
    '                    If Not v_strBalance Is Nothing OrElse v_strBalance.Length = 0 Then
    '                        availBalance = (CDbl(v_strBalance.Replace(".", "").ToString) / 100).ToString()
    '                    Else
    '                        availBalance = 0
    '                    End If
    '                    v_strBalance = VBOracleLib.Globals.Format_Number(CStr(availBalance), ".")

    '                    m_infTK_KH_TH.RetAccountName = v_strRetAccountName
    '                    m_infTK_KH_TH.CertCode = v_strCertCode
    '                    m_infTK_KH_TH.CertType = v_strCertType
    '                    m_infTK_KH_TH.Balance = v_strBalance
    '                    m_infTK_KH_TH.BranchNo = v_strBranchNo
    '                    m_infTK_KH_TH.Cif_No = v_strCif_no
    '                    m_infTK_KH_TH.DescAccount = descAccount
    '                    m_infTK_KH_TH.ErrorMessage = accountInfoInquiryResponse1.return.respMessage.respDesc
    '                    m_infTK_KH_TH.ErrorCode = "0"
    '                    m_infTK_KH_TH.CurrencyCode = accountInfoInquiryResponse1.return.accountInfoInquiryDomain.currency

    '                Else
    '                    v_strErrorCode = accountInfoInquiryResponse1.return.respMessage.respCode
    '                    v_strErrorMessage = accountInfoInquiryResponse1.return.respMessage.respDesc
    '                    m_infTK_KH_TH.ErrorCode = v_strErrorCode
    '                    m_infTK_KH_TH.ErrorMessage = v_strErrorMessage
    '                    Return m_infTK_KH_TH
    '                End If
    '            Else
    '                v_strErrorCode = "-1"
    '                v_strErrorMessage = "ERR_SYSTEM_ERROR"
    '                Return m_infTK_KH_TH
    '            End If
    '        Catch ex As Exception
    '            LogDebug.WriteLog(ex.Message + ex.StackTrace, EventLogEntryType.Error)
    '            Return m_infTK_KH_TH

    '        End Try
    '    Catch ex As Exception
    '        clsCommon.WriteLog(ex, "GetCustomerInfo")
    '        Return m_infTK_KH_TH
    '    End Try
    '    Return m_infTK_KH_TH
    '    '-----------------------------------------------------------------------------------------------------------------------

    'End Function

    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Public Function GetCustomerInfo(ByVal accountNo As String, ByRef descAccount As String, _
                                    ByRef availBalance As String, Optional ByRef v_strErrorCode As String = "", _
                                    Optional ByRef v_strErrorMessage As String = "") As String

        Dim dtAcc As DataTable
        Dim cnCT As DataAccess
        Dim v_strSQL As String = "SELECT cust_ac_no accountNo, acy_avl_bal availBalance, cust_no cifNo, ac_desc AccountName " & _
                                        "FROM OCEANLIVE.STTM_CUST_ACCOUNT@COLLECTDATA" & _
                                        "WHERE cust_ac_no = " & accountNo & "AND record_stat = 'O' AND auth_stat = 'A'"
        Try

            dtAcc = cnCT.ExecuteToTable(v_strSQL)
            If Not dtAcc Is Nothing And dtAcc.Rows.Count > 0 Then
                Dim v_strRetAccountName As String = String.Empty
                Dim v_strBalance As String = String.Empty
                Dim v_strBranchNo As String = String.Empty
                Dim v_strCif_no As String = String.Empty
                v_strRetAccountName = dtAcc.Rows(0)("AccountName").ToString
                v_strBalance = dtAcc.Rows(0)("availBalance").ToString
                v_strBranchNo = dtAcc.Rows(0)("AccountName").ToString
                v_strCif_no = dtAcc.Rows(0)("cifNo").ToString
                descAccount = v_strBranchNo & "-" & v_strCif_no & "-" & v_strRetAccountName
                availBalance = dtAcc.Rows(0)("availBalance").ToString
                v_strBalance = VBOracleLib.Globals.Format_Number(CStr(availBalance), ".")
                ' descAccount = "33-136930-NGUYEN THI THU THUY-182494049"
            Else
                v_strErrorCode = "-1"
                v_strErrorMessage = "Tài khoản này không tồn tại"
                Return "-1"
            End If
            Return "0"
        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '
            v_strErrorCode = "-2"
            v_strErrorMessage = ex.Message
            Return "-1"
        End Try

    End Function

    Public Function Payment(ByVal cif_no As String, ByVal cert_type As String, ByVal cert_code As String, _
                            ByVal issue_date As String, ByVal issue_place As String, _
                            ByVal teller_branch_no As String, ByVal rollout_acct_no As String, _
                            ByVal rollout_acct_name As String, ByVal ten_kb As String, _
                            ByVal ma_cqthu As String, ByVal ten_cqthu As String, _
                            ByVal ma_nh_gui As String, ByVal ma_nh_nhan As String, ByVal ngay_gui As String, ByVal amt As String, _
                            ByVal fee As String, ByVal vat_charge As String, _
                            ByVal remark As String, ByRef errorCode As String, ByRef errorMessage As String, _
                            ByRef hostExcutionDate As String, ByRef hostREFNo As String, _
                            ByVal teller_id As String, ByVal approver_id As String, _
                            ByVal v_strRM_REF_NO As String, ByVal v_strREF_NO As String, ByVal v_strTranDate As String, _
                            ByVal PTHT_BDS As String, ByVal maGD As String, ByVal str_TKHachToan As String, Optional ByVal ma_nh_tt As String = "") As Boolean
        'CongNT comment test
        'Return True
        'CongNT comment test
        Try
            If ma_nh_tt = "" Then
                ma_nh_tt = ma_nh_nhan
            End If
            Dim v_strRemark As String = String.Empty
            'Dim strXMLString As String = String.Empty
            ''Dim v_objMQHelper As clsMQHelper = New clsMQHelper
            ''Dim v_strRequest As String = BuildMsgPayment(cif_no, cert_code, _
            ''                 teller_branch_no, rollout_acct_no, _
            ''                 rollout_acct_name, ten_kb, ma_cqthu, ten_cqthu, ma_nh_gui, ma_nh_nhan, ngay_gui, _
            ''                 amt, fee, vat_charge, remark, teller_id, approver_id, v_strRM_REF_NO)
            'mv_strMSBTSBDS = Configuration.ConfigurationManager.AppSettings("MSB_UID").ToString
            'If rollout_acct_no.Length < 14 Then
            '    rollout_acct_no = "".PadLeft(14 - rollout_acct_no.Length, "0") & rollout_acct_no
            'End If
            'v_strTranDate = ConvertDateToNumber3(v_strTranDate)
            ''Dim remark2 As String = "RM        C                            OL2                                                                                                                                                                             " _
            ''            & ma_nh_nhan.ToUpper & "       VND" _
            ''            & PTHT_BDS.ToUpper _
            ''            & CStr("C" & rollout_acct_name.ToUpper) & "".PadRight(41 - CStr("C" & rollout_acct_name).Length, " ") _
            ''            & cert_code.ToUpper & "".PadRight(196 - cert_code.Length, " ") _
            ''            & "T0000000000280701018" _
            ''            & ma_cqthu.ToUpper & "".PadRight(20 - ma_cqthu.Length, " ") _
            ''            & ten_cqthu.ToUpper & "".PadRight(40 - ten_cqthu.Length, " ") _
            ''            & "".PadRight(43, " ") _
            ''            & ma_nh_nhan.ToUpper & "".PadRight(40 - ma_nh_nhan.Length, " ") _
            ''            & ma_nh_gui & "".PadRight(113 - ma_nh_gui.Length, " ") _
            ''            & "0000000000000000000      " _
            ''            & "".PadLeft(54 - CStr(amt & "00").Length, "0") & CStr(amt & "00") _
            ''            & "            " _
            ''            & v_strTranDate _
            ''            & "".PadRight(144, " ") & "|"
            'Dim remark2 As String = "RM        " _
            '            & "C                            " _
            '            & "OL2                                                                                                                 " _
            '            & ten_kb.ToUpper & "".PadRight(60 - ten_kb.Length, " ") _
            '            & PTHT_BDS.ToUpper() _
            '            & "VND                                                  " _
            '            & CStr("T" & rollout_acct_name.ToUpper) & "".PadRight(41 - CStr("T" & rollout_acct_name).Length, " ") _
            '            & cert_code.ToUpper & "".PadRight(196 - cert_code.Length, " ") _
            '            & "0000000000" & str_TKHachToan.ToUpper _
            '            & ma_cqthu.ToUpper & "".PadRight(21 - ma_cqthu.Length, " ") _
            '            & ten_cqthu.ToUpper & "".PadRight(40 - ten_cqthu.Length, " ") _
            '            & "".PadRight(43, " ") _
            '            & ma_nh_nhan.ToUpper & "".PadRight(40 - ma_nh_nhan.Length, " ") _
            '            & ma_nh_gui & "".PadRight(113 - ma_nh_gui.Length, " ") _
            '            & "0000000000000000000      " _
            '            & "".PadLeft(54 - CStr(amt & "00").Length, "0") & CStr(amt & "00") _
            '            & v_strTranDate _
            '            & "".PadRight(144, " ") & "|"
            ''& "            131011IP123                                                                                  BENNAMEABC2                                              |" _
            'Dim net_cr_amt As String = CStr(Math.Round(CDbl(vat_charge) + CDbl(fee) + CDbl(amt), 0))

            'Dim v_strBaseCurr As String = "00"
            'Dim v_strMessage As String = ""
            'v_strMessage = "*LINX|10.10.10.10||||213|42|0200||ABCS|*LINX|||||||BBHTLMONEYFNC|||||||" _
            '                       & teller_id & "|kienvt|" & maGD & "||||3843|*MOSA||10.10.10.10|||||T9999|||" _
            '                       & teller_id & "|" & v_strREF_NO & "|N||||TP8277|" & v_strTranDate & "||VD|vt|" & approver_id & "|" _
            '                       & teller_branch_no & "|0|1|N|N|N|N|N|N|N|" _
            '                       & v_strRM_REF_NO & "||" & net_cr_amt & v_strBaseCurr & "|||||0000000000" & str_TKHachToan & "|" & rollout_acct_no & "|||||||000|000|000||" _
            '                       & amt & "00||||00000000||10000000|10000000||10000000||VND|" & cif_no & "|||||||||||VND|VND|VND|VND" _
            '                       & "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" _
            '                       & "||||||||||||||||||||||||||||||||||VND|||||||||||||||||||" & fee & v_strBaseCurr & "|" _
            '                       & vat_charge & v_strBaseCurr & "||||||||" _
            '                       & net_cr_amt & v_strBaseCurr & "|||||||||||||||||||||||||||||||" _
            '                       & remark & "||||||||||||||||||||||" _
            '                       & remark2

            'Dim ws As New MsbEtaxRef.MsbEtaxServiceService
            'Dim msg As MsbEtaxRef.messages = ws.submitWS(mv_strMSBTSBDS, v_strMessage)

            'If Not msg Is Nothing Then
            '    If msg.errCode Is Nothing Then
            '        errorCode = 0
            '        errorMessage = "ERR_SYSTEM_OK"
            '        ' Return True
            '    Else
            '        errorCode = msg.errCode
            '        errorMessage = msg.description
            '        'Gan bien o day
            '        ' Return False
            '    End If
            'Else
            '    Return False
            'End If

            mv_strMSBTSBDS = Configuration.ConfigurationManager.AppSettings("MSB_UID").ToString

            Dim remark2 As String = "RM        " _
                        & "C                            " _
                        & "OL2                                                                                                                 " _
                        & ten_kb.ToUpper & "".PadRight(60 - ten_kb.Length, " ") _
                        & "TAXPAY      " _
                        & "VND                                                  " _
                        & CStr("C" & rollout_acct_name.ToUpper) & "".PadRight(41 - CStr("C" & rollout_acct_name).Length, " ") _
                        & cert_code.ToUpper & "".PadRight(196 - cert_code.Length, " ") _
                        & "T0000000000" & str_TKHachToan _
                        & ma_cqthu.ToUpper & "".PadRight(20 - ma_cqthu.Length, " ") _
                        & ten_cqthu.ToUpper & "".PadRight(40 - ten_cqthu.Length, " ") _
                        & "".PadRight(43, " ") _
                        & ma_nh_nhan.ToUpper & "".PadRight(40 - ma_nh_nhan.Length, " ") _
                        & ma_nh_gui & "".PadRight(113 - ma_nh_gui.Length, " ") _
                        & "0000000000000000000      " _
                        & "".PadLeft(54 - CStr(amt & "00").Length, "0") & CStr(amt & "00") _
                        & "            " _
                        & v_strTranDate _
                        & "".PadRight(144, " ")

            Dim net_cr_amt As String = CStr(Math.Round(CDbl(vat_charge) + CDbl(fee) + CDbl(amt), 0))
            v_strTranDate = ConvertDateToNumber3(v_strTranDate)
            Dim v_strBaseCurr As String = "00"
            Dim v_strMessage As String = "*LINX|10.10.10.10||||213|42|0200||ABCS|*LINX|||||||BBHTLMONEYFNC|||||||" _
                    & teller_id & "|kienvt|TP8277||||3843|*MOSA||10.10.10.10|||||T9999|||" _
                    & teller_id & "|" & v_strREF_NO & "|N||||TP8277|" & v_strTranDate & "||VD|vt|" & approver_id & "|" _
                    & teller_branch_no & "|0|1|N|N|N|N|N|N|N|" _
                    & v_strRM_REF_NO & "||" & net_cr_amt & v_strBaseCurr & "|||||0000000000" & str_TKHachToan & "|" & rollout_acct_no & "|||||||||||" _
                    & amt & "00||||||10000000|10000000||10000000||VND|" & cif_no & "|||||||||||VND|VND|VND|VND" _
                    & "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" _
                    & "||||||||||||||||||||||||||||||||||VND|||||||||||||||||||" & fee & v_strBaseCurr & "|" _
                    & vat_charge & v_strBaseCurr & "||||||||" _
                    & net_cr_amt & v_strBaseCurr & "|||||||||||||||||||||||||||||||" _
                    & remark & "||||||||||||||||||||||" _
                    & remark2

            'Ghi log msg
            log.Info("v_strMessage: " & vbNewLine & v_strMessage)

            'Goi One Connect
            '--------TyNK comment test 18-01-2013--------
            'Dim ws As New MsbEtaxRef.MsbEtaxServiceService
            'Dim msg As MsbEtaxRef.messages = ws.submitWS(mv_strMSBTSBDS, v_strMessage)

            'If Not msg Is Nothing Then
            '    If msg.errCode Is Nothing Then
            '        errorCode = 0
            '        errorMessage = "ERR_SYSTEM_OK"
            '        ' Return True
            '    Else
            '        errorCode = msg.errCode
            '        errorMessage = msg.description
            '        'Gan bien o day
            '        ' Return False
            '    End If
            'Else
            '    Return False
            'End If
            ''Ghi log
            '--------TyNK comment test 18-01-2013--------
            'Đặt giá trị mặc định insert logPayment
            'Dim v_strSQLInsert As String = "INSERT INTO tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE,CIF_NO," _
            '    & " CIF_ACCT_NAME,CERT_CODE,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SELL_RATE," _
            '    & " AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO) " _
            '    & " VALUES('TX002','A','1',SYSDATE,'OL2','" + cif_no + "'," _
            '    & " '" + rollout_acct_name + "','" + cert_code + "','" + teller_branch_no + "','" + rollout_acct_no + "','D','VND','10000000','" + ma_nh_gui + "','" + ma_nh_nhan + "','10000000'," _
            '    & " " + amt + "," + fee + "," + vat_charge + ",'" + remark + "','" + errorCode + "','" + errorMessage + "','','" + hostREFNo + "','" + v_strRM_REF_NO + "')"
            Dim v_strSQLInsert As String = "INSERT INTO tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE,CIF_NO," _
                & " CIF_ACCT_NAME,CERT_CODE,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SELL_RATE," _
                & " AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO) " _
                & " VALUES('TX002','A','1',SYSDATE,'OL2','139630'," _
                & " 'Ngan hang hang hai Viet Nam','182494049','110','03301010002381','D','VND','10000000','01302001','01204018','10000000'," _
                & " 35,10000,1000,'remark','errorcode','errorMessage','',' ',' ')"
            Dim v_intResult As Integer = DatabaseHelp.Execute(v_strSQLInsert)

            If (errorCode = "0") Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            log.Error(ex.Message & "-" & ex.StackTrace) '

            errorCode = "-1"
            errorMessage = "Không thể kết nối đến hệ thống corebank"
            Return False
        End Try
    End Function
    Public Function PaymentGL(ByVal cif_no As String, ByVal cert_type As String, ByVal cert_code As String, _
                           ByVal issue_date As String, ByVal issue_place As String, _
                           ByVal teller_branch_no As String, ByVal rollout_acct_no As String, _
                           ByVal rollout_acct_name As String, ByVal ten_kb As String, _
                           ByVal ma_cqthu As String, ByVal ten_cqthu As String, _
                           ByVal ma_nh_gui As String, ByVal ma_nh_nhan As String, ByVal ngay_gui As String, ByVal amt As String, _
                           ByVal fee As String, ByVal vat_charge As String, _
                           ByVal remark As String, ByRef errorCode As String, ByRef errorMessage As String, _
                           ByRef hostExcutionDate As String, ByRef hostREFNo As String, _
                           ByVal teller_id As String, ByVal approver_id As String, _
                           ByVal v_strRM_REF_NO As String, ByVal v_strREF_NO As String, ByVal v_strTranDate As String, _
                           ByVal PTHT_BDS As String, ByVal maGD As String, ByVal str_TKHachToan As String, Optional ByVal ma_nh_tt As String = "") As Boolean
        'CongNT comment test
        'Return True
        'CongNT comment test
        Try
            If ma_nh_tt = "" Then
                ma_nh_tt = ma_nh_nhan
            End If

            Dim v_strRemark As String = String.Empty

            Dim strXMLString As String = String.Empty
            mv_strMSBTSBDS = Configuration.ConfigurationManager.AppSettings("MSB_UID").ToString
            If rollout_acct_no.Length < 14 Then
                rollout_acct_no = "".PadLeft(14 - rollout_acct_no.Length, "0") & rollout_acct_no
            End If
            v_strTranDate = ConvertDateToNumber3(v_strTranDate)

            Dim remark2 As String = "RM        " _
                        & "C                            " _
                        & "OL2                                                                                                                 " _
                        & ten_kb.ToUpper & "".PadRight(60 - ten_kb.Length, " ") _
                        & PTHT_BDS.ToUpper() _
                        & "VND                                                  " _
                        & CStr("T" & rollout_acct_name.ToUpper) & "".PadRight(41 - CStr("T" & rollout_acct_name).Length, " ") _
                        & cert_code.ToUpper & "".PadRight(196 - cert_code.Length, " ") _
                        & "T0000000000" & str_TKHachToan _
                        & ma_cqthu.ToUpper & "".PadRight(20 - ma_cqthu.Length, " ") _
                        & ten_cqthu.ToUpper & "".PadRight(40 - ten_cqthu.Length, " ") _
                        & "".PadRight(43, " ") _
                        & ma_nh_nhan.ToUpper & "".PadRight(40 - ma_nh_nhan.Length, " ") _
                        & ma_nh_gui & "".PadRight(113 - ma_nh_gui.Length, " ") _
                        & "0000000000000000000      " _
                        & "".PadLeft(54 - CStr(amt & "00").Length, "0") & CStr(amt & "00") _
                        & v_strTranDate _
                        & "".PadRight(156, " ") & "|"
            Dim net_cr_amt As String = CStr(Math.Round(CDbl(vat_charge) + CDbl(fee) + CDbl(amt), 0))

            Dim v_strBaseCurr As String = "00"
            Dim v_strMessage As String = ""
            v_strMessage = "*LINX|10.0.2.9||||213|42|0200||ABCS|*LINX|||||||BBHTLMONEYFNC|||||||" _
                                & teller_id & "|01|TP8278||||3843|*MOSA||10.0.2.9|||||T9999|||" _
                                & teller_id & "|" & v_strREF_NO & "|N||||" & maGD & "|" & v_strTranDate & "||VD|01|" & approver_id & "|" _
                                & teller_branch_no & "|0|1|N|N|N|N|N|N|N|" _
                                & v_strRM_REF_NO & "|||" & net_cr_amt & v_strBaseCurr & "||||0000000000" & str_TKHachToan & "|" & rollout_acct_no & "|||||||000|000|000||" _
                                & amt & "00||||00000000||10000000|10000000|10000000|10000000||VND|" & cif_no & "|||||||||||VND|VND|VND|VND" _
                                & "|||||3||||||||||||5|1|5||2|||||||||||3|2|5|5|5|||4||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" _
                                & "VND|||||||||||||||||||" & fee & v_strBaseCurr & "|" _
                                & vat_charge & v_strBaseCurr & "||||||||" _
                                & net_cr_amt & v_strBaseCurr & "|||||||||||||||||||||||||||||||" _
                                & remark & "||||||||||||||||||||||" _
                                & remark2

            'Ghi log msg
            log.Info("v_strMessage: " & vbNewLine & v_strMessage)

            'Goi One Connect
            '--------TyNK comment test 18-01-2013--------
            'Dim ws As New MsbEtaxRef.MsbEtaxServiceService
            'Dim msg As MsbEtaxRef.messages = ws.submitWS(mv_strMSBTSBDS, v_strMessage)

            'If Not msg Is Nothing Then
            '    If msg.errCode Is Nothing Then
            '        errorCode = 0
            '        errorMessage = "ERR_SYSTEM_OK"
            '        ' Return True
            '    Else
            '        errorCode = msg.errCode
            '        errorMessage = msg.description
            '        'Gan bien o day
            '        ' Return False
            '    End If
            'Else
            '    Return False
            'End If
            ''Ghi log
            '--------TyNK comment test 18-01-2013--------
            'Đặt giá trị mặc định insert logPayment
            'Dim v_strSQLInsert As String = "INSERT INTO tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE,CIF_NO," _
            '    & " CIF_ACCT_NAME,CERT_CODE,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SELL_RATE," _
            '    & " AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO) " _
            '    & " VALUES('TX002','A','1',SYSDATE,'OL2','" + cif_no + "'," _
            '    & " '" + rollout_acct_name + "','" + cert_code + "','" + teller_branch_no + "','" + rollout_acct_no + "','D','VND','10000000','" + ma_nh_gui + "','" + ma_nh_nhan + "','10000000'," _
            '    & " " + amt + "," + fee + "," + vat_charge + ",'" + remark + "','" + errorCode + "','" + errorMessage + "','','" + hostREFNo + "','" + v_strRM_REF_NO + "')"
            Dim v_strSQLInsert As String = "INSERT INTO tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE,CIF_NO," _
                & " CIF_ACCT_NAME,CERT_CODE,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SELL_RATE," _
                & " AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO) " _
                & " VALUES('TX002','A','1',SYSDATE,'OL2','139630'," _
                & " 'Ngan hang hang hai Viet Nam','182494049','110','03301010002381','D','VND','10000000','01302001','01204018','10000000'," _
                & " 35,10000,1000,'remark','errorcode','errorMessage','',' ',' ')"
            Dim v_intResult As Integer = DatabaseHelp.Execute(v_strSQLInsert)

            If (errorCode = "0") Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception

            Dim sbErrMsg As StringBuilder
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append("PaymentGL Error : ")
            sbErrMsg.Append(ex.Source)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.StackTrace)

            log.Error(ex.Message & "-" & ex.StackTrace) '
            errorCode = "-1"
            errorMessage = "Không thể kết nối đến hệ thống corebank"
            Return False
        End Try
    End Function
    'Public Function PaymentGL(ByVal issue_date As String, ByVal issue_place As String, _
    '                        ByVal teller_branch_no As String, ByVal rollout_acct_no As String, _
    '                        ByVal rollout_acct_name As String, ByVal ten_kb As String, _
    '                        ByVal ma_cqthu As String, ByVal ten_cqthu As String, _
    '                        ByVal ma_nh_gui As String, ByVal ma_nh_nhan As String, ByVal ngay_gui As String, ByVal amt As String, _
    '                        ByVal fee As String, ByVal vat_charge As String, _
    '                        ByVal remark As String, ByRef errorCode As String, ByRef errorMessage As String, _
    '                        ByRef hostExcutionDate As String, ByRef hostREFNo As String, _
    '                        ByVal teller_id As String, ByVal approver_id As String, _
    '                        ByVal v_strRM_REF_NO As String, ByVal v_strREF_NO As String, ByVal v_strTranDate As String, _
    '                        ByVal isHighValue As Boolean, ByVal v_strTKCo As String) As Boolean
    '    Try
    '        Dim v_strRemark As String = String.Empty

    '        Dim strXMLString As String = String.Empty
    '        'Dim v_objMQHelper As clsMQHelper = New clsMQHelper
    '        'Dim v_strRequest As String = BuildMsgPayment(cif_no, cert_code, _
    '        '                 teller_branch_no, rollout_acct_no, _
    '        '                 rollout_acct_name, ten_kb, ma_cqthu, ten_cqthu, ma_nh_gui, ma_nh_nhan, ngay_gui, _
    '        '                 amt, fee, vat_charge, remark, teller_id, approver_id, v_strRM_REF_NO)
    '        mv_strMSBTSBDS = Configuration.ConfigurationManager.AppSettings("MSB_UID").ToString
    '        Dim remark2 As String
    '        Dim maGD As String
    '        If isHighValue = True Then
    '            maGD = "TP8217"
    '            remark2 = "RM" & "".PadRight(8, " ") _
    '                   & "CZ" & "".PadRight(27, " ") _
    '                   & "OL8" & "".PadRight(101, " ") _
    '                   & ma_nh_nhan.ToUpper & "".PadRight(72 - ma_nh_nhan.Length, " ") _
    '                   & ma_nh_nhan.ToUpper & "".PadRight(12 - ma_nh_nhan.Length, " ") _
    '                   & "VND" & "".PadRight(50, " ") _
    '                   & "TMARITIMEBANK" & "".PadRight(224, " ") _
    '                    & "T" & "".PadLeft(22 - v_strTKCo.Length, "0") & v_strTKCo & "".PadLeft(32 - ten_cqthu.Length, " ") & ten_cqthu & "".PadRight(221, " ") _
    '                   & "0000000000000000000      " _
    '                   & "".PadLeft(54 - CStr(amt & "00").Length, "0") & CStr(amt & "00") _
    '                   & "            000000" _
    '                   & "".PadRight(144, " ") & "|"
    '        Else
    '            maGD = "TP8268"
    '            ' remark2 = "TT TEST"
    '            remark2 = "RM" & "".PadRight(8, " ") _
    '                                   & "CZ" & "".PadRight(27, " ") _
    '                                   & "OL4" & "".PadRight(101, " ") _
    '                                   & ma_nh_nhan.ToUpper & "".PadRight(72 - ma_nh_nhan.Length, " ") _
    '                                   & ma_nh_nhan.ToUpper & "".PadRight(12 - ma_nh_nhan.Length, " ") _
    '                                   & "VND" & "".PadRight(50, " ") _
    '                                   & "TMSB VN" & "".PadRight(230, " ") _
    '                                   & "T" & "".PadLeft(22 - v_strTKCo.Length, "0") & v_strTKCo & "".PadLeft(32 - ten_cqthu.Length, " ") & ten_cqthu & "".PadRight(221, " ") _
    '                                   & "0000000000000000000      " _
    '                                   & "".PadLeft(54 - CStr(amt & "00").Length, "0") & CStr(amt & "00") _
    '                                   & "            000000" _
    '                                   & "".PadRight(144, " ") & "|"

    '        End If


    '        Dim net_cr_amt As String = CStr(Math.Round(CDbl(vat_charge) + CDbl(fee) + CDbl(amt), 0))
    '        v_strTranDate = ConvertDateToNumber3(v_strTranDate)
    '        Dim v_strBaseCurr As String = "00"

    '        Dim v_strMessage As String = "*LINX|10.0.2.9| | | |213|42|0200| |ABCS|*LINX| | | | | | |BBHTLMONEYFNC| | | | | | |" _
    '         & teller_id & "|01|" & maGD & "| | | |3843|*MOSA| |10.0.2.9| | | | |T9999| ||" _
    '         & teller_id & "|" & v_strREF_NO & "|N||||" & maGD & "|" & v_strTranDate & "| |V3|01|" & approver_id & "|" _
    '         & teller_branch_no & "|0|1|N|N|N|N|N|N|N|" _
    '         & v_strRM_REF_NO & "|||" & amt & v_strBaseCurr & "|||||00000" & rollout_acct_no & "|||||||000|000|000||" _
    '         & amt & v_strBaseCurr & "||||00000000||10000000|10000000|10000000|10000000||VND||||||||||||VND|VND|VND|VND" _
    '         & "|||||3||||||||||||5|1|5||2|||||||||||3|2|5|5|5|||4||||||||||||||||||||||||||||||||||||||||||||||||||||||" _
    '         & "||||||||||||||||||||VND|||||||||||||||||000|000|000|000||||||||" & amt & v_strBaseCurr & "||||4|5|5|5||||||||2||||||||||||||||" _
    '         & remark & "|||||000|000|||||||||1|1||||||" _
    '         & remark2

    '        'strXMLString = v_objMQHelper.PutAndGetMQMessage(v_strRequest)

    '        Dim ws As New MsbEtaxRef.MsbEtaxServiceService
    '        Dim msg As MsbEtaxRef.messages = ws.submitWS(mv_strMSBTSBDS, v_strMessage)

    '        'If strXMLString Is Nothing Then Return False
    '        'If strXMLString.Length = 0 Then Return False

    '        'If strXMLString Is Nothing OrElse strXMLString.Length = 0 Then Return False
    '        'Dim v_xmlDocument As New Xml.XmlDocument
    '        'v_xmlDocument.LoadXml(ReplaceSpecialCharacter(strXMLString))
    '        'Dim v_nodeList As Xml.XmlNodeList
    '        'v_nodeList = v_xmlDocument.SelectNodes("/message/head/field")
    '        'If v_nodeList.Count > 0 Then
    '        '    For i As Integer = 0 To v_nodeList.Count - 1
    '        '        If v_nodeList.Item(i).Attributes.Item(0).Value = "resp_code" Then
    '        '            errorCode = v_nodeList.Item(i).InnerText
    '        '        ElseIf v_nodeList.Item(i).Attributes.Item(0).Value = "resp_msg" Then
    '        '            errorMessage = v_nodeList.Item(i).InnerText
    '        '        ElseIf v_nodeList.Item(i).Attributes.Item(0).Value = "excution_date" Then
    '        '            hostExcutionDate = v_nodeList.Item(i).InnerText
    '        '        ElseIf v_nodeList.Item(i).Attributes.Item(0).Value = "ref_tran_no" Then
    '        '            hostREFNo = v_nodeList.Item(i).InnerText
    '        '        End If
    '        '    Next
    '        'End If
    '        If Not msg Is Nothing Then
    '            If msg.errCode Is Nothing Then
    '                errorCode = 0
    '                errorMessage = "ERR_SYSTEM_OK"
    '                ' Return True
    '            Else
    '                errorCode = msg.errCode
    '                errorMessage = msg.description
    '                'Gan bien o day
    '                ' Return False
    '            End If
    '        Else
    '            Return False
    '        End If




    '        'If strXMLString Is Nothing Then Return False
    '        'If strXMLString.Length = 0 Then Return False

    '        'If strXMLString Is Nothing OrElse strXMLString.Length = 0 Then Return False
    '        'Dim v_xmlDocument As New Xml.XmlDocument
    '        'v_xmlDocument.LoadXml(ReplaceSpecialCharacter(strXMLString))
    '        'Dim v_nodeList As Xml.XmlNodeList
    '        'v_nodeList = v_xmlDocument.SelectNodes("/message/head/field")
    '        'If v_nodeList.Count > 0 Then
    '        '    For i As Integer = 0 To v_nodeList.Count - 1
    '        '        If v_nodeList.Item(i).Attributes.Item(0).Value = "resp_code" Then
    '        '            errorCode = v_nodeList.Item(i).InnerText
    '        '        ElseIf v_nodeList.Item(i).Attributes.Item(0).Value = "resp_msg" Then
    '        '            errorMessage = v_nodeList.Item(i).InnerText
    '        '        ElseIf v_nodeList.Item(i).Attributes.Item(0).Value = "excution_date" Then
    '        '            hostExcutionDate = v_nodeList.Item(i).InnerText
    '        '        ElseIf v_nodeList.Item(i).Attributes.Item(0).Value = "ref_tran_no" Then
    '        '            hostREFNo = v_nodeList.Item(i).InnerText
    '        '        End If
    '        '    Next
    '        'End If


    '        ''Ghi log
    '        Dim v_strSQLInsert As String = "INSERT INTO ctc_owner.tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE," _
    '            & " CIF_ACCT_NAME,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SELL_RATE," _
    '            & " AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO) " _
    '            & " VALUES('TX002','A','1',SYSDATE,'OL2'," _
    '            & " '" + rollout_acct_name + "','" + teller_branch_no + "','" + rollout_acct_no + "','D','VND','10000000','" + ma_nh_gui + "','" + ma_nh_nhan + "','10000000'," _
    '            & " " + amt + "," + fee + "," + vat_charge + ",'" + remark + "','" + errorCode + "','" + errorMessage + "','','" + hostREFNo + "','" + v_strRM_REF_NO + "')"
    '        Dim v_intResult As Integer = DatabaseHelp.Execute(v_strSQLInsert)

    '        If (errorCode = "0") Then
    '            Return True
    '        Else
    '            Return False
    '        End If

    '    Catch ex As Exception
    '        LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
    '              & "Error code: System error!" & vbNewLine _
    '              & "Error message: " & ex.Message, EventLogEntryType.Error)
    '        errorCode = "-1"
    '        errorMessage = "Không thể kết nối đến hệ thống corebank"
    '        Return False
    '    End Try
    '    Return True
    'End Function

    Public Function CancelPayment(ByVal cif_no As String, ByVal cert_type As String, ByVal cert_code As String, _
                          ByVal issue_date As String, ByVal issue_place As String, _
                          ByVal teller_branch_no As String, ByVal rollout_acct_no As String, _
                          ByVal rollout_acct_name As String, ByVal ten_kb As String, _
                          ByVal ma_cqthu As String, ByVal ten_cqthu As String, _
                          ByVal ma_nh_gui As String, ByVal ma_nh_nhan As String, ByVal ngay_gui As String, ByVal amt As String, _
                          ByVal fee As String, ByVal vat_charge As String, _
                          ByVal remark As String, ByRef errorCode As String, ByRef errorMessage As String, _
                          ByRef hostExcutionDate As String, ByRef hostREFNo As String, _
                          ByVal teller_id As String, ByVal approver_id As String, _
                          ByVal v_strRM_REF_NO As String, ByVal v_strREF_NO As String, ByVal v_strTranDate As String) As Boolean
        'CongNT comment test
        'Return True
        'CongNT comment test
        Try
            Dim v_strRemark As String = String.Empty

            Dim strXMLString As String = String.Empty
            Dim v_objMQHelper As clsMQHelper = New clsMQHelper
            Dim v_strRequest As String = BuildMsgCancelPayment(cif_no, cert_code, _
                             teller_branch_no, rollout_acct_no, _
                             rollout_acct_name, ten_kb, ma_cqthu, ten_cqthu, ma_nh_gui, ma_nh_nhan, ngay_gui, _
                             amt, fee, vat_charge, remark, teller_id, approver_id, v_strRM_REF_NO, v_strREF_NO)


            strXMLString = v_objMQHelper.PutAndGetMQMessage(v_strRequest)

            If strXMLString Is Nothing Then Return False
            If strXMLString.Length = 0 Then Return False

            If strXMLString Is Nothing OrElse strXMLString.Length = 0 Then Return False
            Dim v_xmlDocument As New Xml.XmlDocument
            v_xmlDocument.LoadXml(ReplaceSpecialCharacter(strXMLString))
            Dim v_nodeList As Xml.XmlNodeList
            v_nodeList = v_xmlDocument.SelectNodes("/message/head/field")
            If v_nodeList.Count > 0 Then
                For i As Integer = 0 To v_nodeList.Count - 1
                    If v_nodeList.Item(i).Attributes.Item(0).Value = "resp_code" Then
                        errorCode = v_nodeList.Item(i).InnerText
                    ElseIf v_nodeList.Item(i).Attributes.Item(0).Value = "resp_msg" Then
                        errorMessage = v_nodeList.Item(i).InnerText
                    ElseIf v_nodeList.Item(i).Attributes.Item(0).Value = "excution_date" Then
                        hostExcutionDate = v_nodeList.Item(i).InnerText
                    ElseIf v_nodeList.Item(i).Attributes.Item(0).Value = "ref_tran_no" Then
                        hostREFNo = v_nodeList.Item(i).InnerText
                    End If
                Next
            End If


            'Ghi log
            'Dim v_strSQLInsert As String = "INSERT INTO tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE,CIF_NO," _
            '    & " CIF_ACCT_NAME,CERT_CODE,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SELL_RATE," _
            '    & " AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO) " _
            '    & " VALUES('TX002','A','1',SYSDATE,'OL2','" + cif_no + "'," _
            '    & " '" + rollout_acct_name + "','" + cert_code + "','" + teller_branch_no + "','" + rollout_acct_no + "','D','VND','10000000','" + ma_nh_gui + "','" + ma_nh_nhan + "','10000000'," _
            '    & " " + amt + "," + fee + "," + vat_charge + ",'" + remark + "','" + errorCode + "','" + errorMessage + "','','" + hostREFNo + "','" + v_strRM_REF_NO + "')"
            Dim v_strSQLInsert As String = "INSERT INTO tcs_log_payment (TRAN_CODE,TRAN_TYPE,TRAN_SEQ,TRAN_DATE,PRODUCT_CODE,CIF_NO," _
                & " CIF_ACCT_NAME,CERT_CODE,BRANCH_NO,ACCT_NO,ACCT_TYPE,ACCT_CCY,BUY_RATE,BNFC_BANK_ID,BNFC_BANK_NAME,SELL_RATE," _
                & " AMT,FEE,VAT_CHARGE,REMARK_DESC,RESPONSE_CODE,RESPONSE_MSG,SEQ_NO,REF_SEQ_NO,RM_REF_NO) " _
                & " VALUES('TX002','A','1',SYSDATE,'OL2','139630'," _
                & " 'Ngan hang hang hai Viet Nam','182494049','110','03301010002381','D','VND','10000000','01302001','01204018','10000000'," _
                & " 35,10000,1000,'remark','errorcode','errorMessage','',' ',' ')"
            Dim v_intResult = DatabaseHelp.Execute(v_strSQLInsert)

            If (errorCode = "0") Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception

            log.Error(ex.Message & "-" & ex.StackTrace) '
            errorCode = "-1"
            errorMessage = "Không thể kết nối đến hệ thống corebank"
            Return False
        End Try
    End Function

    Private Function BuildMsgGetBalance(ByVal accountNo As String) As String
        Dim v_xmlDocument As New Xml.XmlDocument
        Dim dataElement As Xml.XmlElement
        Dim entryNode As Xml.XmlNode
        v_xmlDocument.LoadXml(BuildMessageHeader(MSG_CODE_BALANCE_INQ))
        dataElement = v_xmlDocument.CreateElement(Xml.XmlNodeType.Element, "body", "")
        Dim v_attrFLDNAME As Xml.XmlAttribute

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "acct_no"
        entryNode.InnerText = accountNo
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "acct_type"
        entryNode.InnerText = "D"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "acct_ccy"
        entryNode.InnerText = "VND"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        Return v_xmlDocument.InnerXml
    End Function

    Private Function BuildMsgPayment(ByVal cif_no As String, ByVal cert_code As String, _
                            ByVal teller_branch_no As String, ByVal rollout_acct_no As String, _
                            ByVal rollout_acct_name As String, ByVal ten_kb As String, _
                            ByVal ma_cqthu As String, ByVal ten_cqthu As String, _
                            ByVal ma_nh_gui As String, ByVal ma_nh_nhan As String, ByVal ngay_gui As String, _
                            ByVal amt As String, ByVal fee As String, ByVal vat_charge As String, _
                            ByVal remark As String, ByVal teller_id As String, ByVal approver_id As String, ByVal v_strRM_REF_NO As String) As String

        Dim v_xmlDocument As New Xml.XmlDocument
        Dim dataElement As Xml.XmlElement
        Dim entryNode As Xml.XmlNode
        v_xmlDocument.LoadXml(BuildMessageHeader(MSG_CODE_PAYMENT))
        dataElement = v_xmlDocument.CreateElement(Xml.XmlNodeType.Element, "body", "")
        Dim v_attrFLDNAME As Xml.XmlAttribute

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "teller_id"
        entryNode.InnerText = teller_id
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "approver_id"
        entryNode.InnerText = approver_id
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "teller_branch_no"
        entryNode.InnerText = teller_branch_no
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "is_outward_transfer"
        entryNode.InnerText = "Y"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "product_code"
        entryNode.InnerText = "OL02"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)


        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "cif_no"
        entryNode.InnerText = cif_no
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "cert_code"
        entryNode.InnerText = cert_code
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "rollout_acct_name"
        entryNode.InnerText = rollout_acct_name
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "rollout_acct_no"
        entryNode.InnerText = rollout_acct_no
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "rollout_acct_type"
        entryNode.InnerText = "D"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "rollout_acct_ccy"
        entryNode.InnerText = "VND"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "rollout_buy_rate"
        entryNode.InnerText = "10000000"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)


        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "ten_kb"
        entryNode.InnerText = ten_kb
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "ma_cqthu"
        entryNode.InnerText = ma_cqthu
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "ten_cqthu"
        entryNode.InnerText = ten_cqthu
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "ma_nh_gui"
        entryNode.InnerText = ma_nh_gui
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "ma_nh_nhan"
        entryNode.InnerText = ma_nh_nhan
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "ngay_gui"
        entryNode.InnerText = ngay_gui
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "amt"
        entryNode.InnerText = amt
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "fee"
        entryNode.InnerText = fee
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "rm_ref_no"
        entryNode.InnerText = v_strRM_REF_NO
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "vat_charge"
        entryNode.InnerText = vat_charge
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "remark"
        entryNode.InnerText = remark
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        Return v_xmlDocument.InnerXml
    End Function

    Private Function BuildMsgCancelPayment(ByVal cif_no As String, ByVal cert_code As String, _
                                ByVal teller_branch_no As String, ByVal rollout_acct_no As String, _
                                ByVal rollout_acct_name As String, ByVal ten_kb As String, _
                                ByVal ma_cqthu As String, ByVal ten_cqthu As String, _
                                ByVal ma_nh_gui As String, ByVal ma_nh_nhan As String, ByVal ngay_gui As String, _
                                ByVal amt As String, ByVal fee As String, ByVal vat_charge As String, _
                                ByVal remark As String, ByVal teller_id As String, ByVal approver_id As String, _
                                ByVal v_strRM_REF_NO As String, ByVal v_strOrgRefNo As String) As String

        Dim v_xmlDocument As New Xml.XmlDocument
        Dim dataElement As Xml.XmlElement
        Dim entryNode As Xml.XmlNode
        v_xmlDocument.LoadXml(BuildMessageHeader(MSG_CODE_CANCEL_PAYMENT))
        dataElement = v_xmlDocument.CreateElement(Xml.XmlNodeType.Element, "body", "")
        Dim v_attrFLDNAME As Xml.XmlAttribute

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "teller_id"
        entryNode.InnerText = teller_id
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "approver_id"
        entryNode.InnerText = approver_id
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "teller_branch_no"
        entryNode.InnerText = teller_branch_no
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "is_outward_transfer"
        entryNode.InnerText = "Y"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "product_code"
        entryNode.InnerText = "OL02"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)


        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "cif_no"
        entryNode.InnerText = cif_no
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "cert_code"
        entryNode.InnerText = cert_code
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "rollout_acct_name"
        entryNode.InnerText = rollout_acct_name
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "rollout_acct_no"
        entryNode.InnerText = rollout_acct_no
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "rollout_acct_type"
        entryNode.InnerText = "D"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "rollout_acct_ccy"
        entryNode.InnerText = "VND"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "rollout_buy_rate"
        entryNode.InnerText = "10000000"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)


        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "ten_kb"
        entryNode.InnerText = ten_kb
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "ma_cqthu"
        entryNode.InnerText = ma_cqthu
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "ten_cqthu"
        entryNode.InnerText = ten_cqthu
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "ma_nh_gui"
        entryNode.InnerText = ma_nh_gui
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "ma_nh_nhan"
        entryNode.InnerText = ma_nh_nhan
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "ngay_gui"
        entryNode.InnerText = ngay_gui
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "amt"
        entryNode.InnerText = amt
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "fee"
        entryNode.InnerText = fee
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "rm_ref_no"
        entryNode.InnerText = v_strRM_REF_NO
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "org_ref_no"
        entryNode.InnerText = v_strOrgRefNo
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)


        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "vat_charge"
        entryNode.InnerText = vat_charge
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "remark"
        entryNode.InnerText = remark
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        Return v_xmlDocument.InnerXml
    End Function

    Private Function BuildMessageHeader(ByVal pv_strMessageCode As String) As String
        Dim v_xmlDocument As New Xml.XmlDocument
        Dim dataElement As Xml.XmlElement
        Dim entryNode As Xml.XmlNode
        v_xmlDocument.LoadXml("<message></message>")
        dataElement = v_xmlDocument.CreateElement(Xml.XmlNodeType.Element, "head", "")
        Dim v_attrFLDNAME As Xml.XmlAttribute

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "version"
        entryNode.InnerText = "100"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "message_type"
        entryNode.InnerText = "0"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "message_sn"
        entryNode.InnerText = "111"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "sender_id"
        entryNode.InnerText = "TAX"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "sender_tran_sn"
        entryNode.InnerText = "111"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "send_date"
        entryNode.InnerText = "99999"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "send_time"
        entryNode.InnerText = "173629"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "tran_code"
        entryNode.InnerText = pv_strMessageCode
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "receiver_id"
        entryNode.InnerText = "CBS"
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "receiver_tran_sn"
        entryNode.InnerText = ""
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "resp_date"
        entryNode.InnerText = ""
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)


        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "resp_time"
        entryNode.InnerText = ""
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "resp_code"
        entryNode.InnerText = ""
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)

        entryNode = v_xmlDocument.CreateNode(Xml.XmlNodeType.Element, "field", "")
        v_attrFLDNAME = v_xmlDocument.CreateAttribute("name")
        v_attrFLDNAME.Value = "resp_msg"
        entryNode.InnerText = ""
        entryNode.Attributes.Append(v_attrFLDNAME)
        dataElement.AppendChild(entryNode)
        v_xmlDocument.DocumentElement.AppendChild(dataElement)
        Return v_xmlDocument.InnerXml
    End Function

    Private Function ReplaceSpecialCharacter(ByVal pv_strInput As String) As String
        If pv_strInput Is Nothing Then
            pv_strInput.Replace("&", "")
        End If
        Return pv_strInput
    End Function

End Class
