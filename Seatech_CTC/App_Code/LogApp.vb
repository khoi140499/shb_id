﻿Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System

Public Class LogApp

    Private Shared log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private Shared log_action As log4net.ILog = log4net.LogManager.GetLogger("LOG_ACTION")
    Private Shared LOG_APP_NAME As String = ConfigurationManager.AppSettings.Get("Log_Name").ToString()

    Public Shared Sub AddDebug(ByVal pv_strAction As String, ByVal pv_strMessage As String)
        Dim v_strUserName As String = System.Web.HttpContext.Current.Session("UserName")
        Dim v_strBranh As String = System.Web.HttpContext.Current.Session("MA_CN_USER_LOGON")
        Dim v_strIp As String = System.Web.HttpContext.Current.Session("IP_LOCAL")

        log.Debug("APP_NAME=""" & LOG_APP_NAME & """ userName=""" & v_strUserName & """ branchId=""" & v_strBranh & """ userIp=""" & v_strIp & """ action=""" & pv_strAction & """ Message=""" & pv_strMessage & """" & Environment.NewLine)

    End Sub

    Public Shared Sub AddAction(ByVal pv_strAction As String, ByVal pv_strMessage As String)
        Dim v_strUserName As String = System.Web.HttpContext.Current.Session("UserName")
        Dim v_strBranh As String = System.Web.HttpContext.Current.Session("MA_CN_USER_LOGON")
        Dim v_strIp As String = System.Web.HttpContext.Current.Session("IP_LOCAL")
        log_action.Info("APP_NAME=""" & LOG_APP_NAME & """ userName=""" & v_strUserName & """ branchId=""" & v_strBranh & """ userIp=""" & v_strIp & """ action=""" & pv_strAction & """ Message=""" & pv_strMessage & """" & Environment.NewLine)


    End Sub
    Public Shared Sub WriteLog(ByVal strAction As String, ByVal ex As Exception, ByVal strDescription As String, Optional ByVal approverID As String = "")


        AddAction(strAction, "ApproverID: " & approverID & vbNewLine _
              & "Error source: " & ex.Source & vbNewLine _
              & "Error code: System error!" & vbNewLine _
              & "Error message: " & ex.Message & vbNewLine _
              & "Error StackTrace: " & ex.StackTrace)

        If Not ex.InnerException Is Nothing Then
            AddAction(strAction, "ApproverID: " & approverID & vbNewLine _
             & "Error source: " & ex.InnerException.Source & vbNewLine _
             & "Error code: System error!" & vbNewLine _
             & "Error message: " & ex.InnerException.Message & vbNewLine _
             & "Error StackTrace: " & ex.InnerException.StackTrace)
        End If

        AddAction(strAction, strDescription & " Error source: " & ex.Source & vbNewLine _
              & "Error code: System error!" & vbNewLine _
              & "Error message: " & ex.Message)
    End Sub
    Public Shared Sub WriteLogAddDebug(ByVal strAction As String, ByVal ex As Exception, ByVal strDescription As String, Optional ByVal approverID As String = "")


        AddDebug(strAction, "ApproverID: " & approverID & vbNewLine _
              & "Error source: " & ex.Source & vbNewLine _
              & "Error code: System error!" & vbNewLine _
              & "Error message: " & ex.Message & vbNewLine _
              & "Error StackTrace: " & ex.StackTrace)

        If Not ex.InnerException Is Nothing Then
            AddDebug(strAction, "ApproverID: " & approverID & vbNewLine _
             & "Error source: " & ex.InnerException.Source & vbNewLine _
             & "Error code: System error!" & vbNewLine _
             & "Error message: " & ex.InnerException.Message & vbNewLine _
             & "Error StackTrace: " & ex.InnerException.StackTrace)
        End If

        AddDebug(strAction, strDescription & " Error source: " & ex.Source & vbNewLine _
              & "Error code: System error!" & vbNewLine _
              & "Error message: " & ex.Message)
    End Sub
    Private Shared Function GetIPv4Address() As String
        GetIPv4Address = String.Empty
        Dim strHostName As String = System.Net.Dns.GetHostName()
        Dim iphe As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(strHostName)

        For Each ipheal As System.Net.IPAddress In iphe.AddressList
            If ipheal.AddressFamily = System.Net.Sockets.AddressFamily.InterNetwork Then
                GetIPv4Address = ipheal.ToString()
            End If
        Next

    End Function


End Class
