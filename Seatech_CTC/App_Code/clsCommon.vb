﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports Business.Common.mdlSystemVariables
Imports System
Imports System.Web.UI.WebControls
Imports System.Diagnostics
Imports VBOracleLib
Imports System.Text
Imports System.Web


Public Class clsCommon
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Public Shared Sub get_NguyenTe(ByVal cboNguyenTe As DropDownList)
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable
        Try
            Dim strSQL As String = "SELECT * from Tcs_dm_nguyente "
            dt = DataAccess.ExecuteToTable(strSQL)

            If Not VBOracleLib.Globals.IsNullOrEmpty(dt) Then

                For i As Integer = 0 To dt.Rows.Count
                    Dim item As ListItem
                    item = New ListItem(dt.Rows(i)("TEN").ToString(), dt.Rows(i)("Ma_NT").ToString())
                    cboNguyenTe.Items.Add(item)
                Next

            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Function checkContansChar(ByVal strOrigin As String, ByVal strValid As String) As Boolean
        Dim result As Boolean = False
        For Each c As Char In strValid.ToCharArray
            If strOrigin.Contains(c) Then
                result = True
                Exit For
            End If
        Next
        Return result
    End Function

    Public Shared Sub ComboBoxWithValue(ByRef cbo As DropDownList, ByVal ds As DataSet, _
                ByVal colDisplay As Integer, ByVal colValue As Integer)
        Try
            cbo.Items.Clear()

            If (ds.Tables.Count = 0) Then Return

            cbo.DataSource = ds.Tables(0)
            cbo.DataTextField = ds.Tables(0).Columns(colDisplay).ColumnName
            cbo.DataValueField = ds.Tables(0).Columns(colValue).ColumnName

            cbo.SelectedIndex = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Shared Sub ComboBoxWithValue(ByRef cbo As DropDownList, ByVal ds As DataSet, _
                ByVal colDisplay As Integer, ByVal colValue As Integer, ByVal strTitle As String)
        Try
            cbo.Items.Clear()

            If (ds.Tables.Count = 0) Then Return

            'Dim r As New DataRow
            ds.Tables(0).Rows.Add(New Object() {"0", ""})
            Dim row As DataRow = ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1)
            ds.Tables(0).Rows.Remove(row)
            ds.Tables(0).Rows.InsertAt(row, 0)

            ds.Tables(0).Rows(0).Item(colDisplay) = strTitle


            cbo.DataSource = ds.Tables(0)
            cbo.DataTextField = ds.Tables(0).Columns(colDisplay).ColumnName
            cbo.DataValueField = ds.Tables(0).Columns(colValue).ColumnName
            cbo.DataBind()
            cbo.SelectedIndex = 0

            ds.Dispose()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Shared Function CompareDate(ByVal dtStr1 As String, ByVal dtStr2 As String) As Boolean
        If dtStr1 = String.Empty OrElse dtStr2 = String.Empty Then
            Return False
        End If
        Dim intDay1 As Integer = Convert.ToInt16(dtStr1.Substring(0, 2))
        Dim intMonth1 As Integer = Convert.ToInt16(dtStr1.Substring(3, 2))
        Dim intYear1 As Integer = Convert.ToInt16(dtStr1.Substring(6, 4))

        Dim intDay2 As Integer = Convert.ToInt16(dtStr2.Substring(0, 2))
        Dim intMonth2 As Integer = Convert.ToInt16(dtStr2.Substring(3, 2))
        Dim intYear2 As Integer = Convert.ToInt16(dtStr2.Substring(6, 4))
        Dim bReturn As Boolean = True
        If intYear1 > intYear2 Then
            bReturn = False
        ElseIf (intYear1 = intYear2) AndAlso (intMonth1 > intMonth2) Then
            bReturn = False
        ElseIf (intMonth1 > intMonth2) AndAlso (intDay1 = intDay2) Then
            bReturn = False
        End If
        Return bReturn
    End Function


    Public Shared Function GetSessionByID(ByVal pv_strManv As String) As Boolean
        Try
            Dim v_strSql As String = "SELECT SESSION_CODE FROM TCS_DM_SESSION WHERE MA_NV= '" & pv_strManv & "'"
            Dim v_strSession As String = DataAccess.ExecuteSQLScalar(v_strSql)

            If v_strSession.Equals(HttpContext.Current.Session.SessionID) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception

        End Try
    End Function

    Public Shared Function Check_Maker_Checker(ByVal Nhom As String) As String
        Dim cnDiemThu As New DataAccess
        Dim dt As DataTable
        Try
            Dim strSQL As String = "select * from tcs_thamso_ht where ten_ts='NHOM_MAKER' and INSTR(giatri_ts,'" & Nhom & "')>0"
            dt = DataAccess.ExecuteToTable(strSQL)
            If dt.Rows.Count > 0 Then
                Return "10"
            Else
                Dim strSQL2 As String = "select * from tcs_thamso_ht where ten_ts='NHOM_CHECKER' and INSTR(giatri_ts,'" & Nhom & "')>0"
                dt = DataAccess.ExecuteToTable(strSQL2)
                If dt.Rows.Count > 0 Then
                    Return "11"
                Else
                    Return "00"
                End If
            End If
        Catch ex As Exception
            Return "00"
        End Try
    End Function

    'sonmt
    Public Shared Function TCS_GETTS(ByVal p_strTENTS As String) As String
        Try
            Dim strSQL As String = String.Empty
            strSQL = "SELECT GIATRI_TS FROM TCS_THAMSO_HT WHERE TEN_TS='" & p_strTENTS & "' "
            Return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables(0).Rows(0)(0).ToString
        Catch ex As Exception
            'clsCommon.WriteLog(ex, "Lỗi lấy giá trị tham số", HttpContext.Current.Session.Item("TEN_DN"))
            Throw ex
        End Try
    End Function

#Region "Ham correct du lieu"
    Public Shared Function checkHighValue(ByVal amount As Double) As Boolean
        Dim numberLimit As Double
        numberLimit = 500000000 ' 500 triệu
        If amount > numberLimit Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Shared Function gf_CorrectString(ByVal data As Object) As String
        If Not data Is Nothing Then
            Return CStr(data)
        Else
            Return String.Empty
        End If
    End Function

    Public Shared Function gf_CorrectNumeric(ByVal data As Object) As String
        If Not data Is Nothing Then
            Return CDbl(data)
        Else
            Return 0
        End If
    End Function

    Public Shared Function gf_DDMMYY_MMDDYY(ByVal data As String) As String
        If data.Length = 0 OrElse data = String.Empty OrElse data Is Nothing Then
            Return Now.Date.ToString(gc_DEFAULT_FORMAT_DATE)
        Else
            If data.Length = 10 Then
                Return data.Substring(3, 2) & "/" & data.Substring(0, 2) & "/" & data.Substring(6, 4)
            Else
                If data.IndexOf("/") = 1 Then 'Neu thang < 10
                    Return data.Substring(2, 2) & "/" & "0" & data.Substring(0, 1) & "/" & data.Substring(5, 4)
                Else 'Neu ngay <10
                    Return "0" & data.Substring(3, 1) & "/" & data.Substring(0, 2) & "/" & data.Substring(5, 4)
                End If
            End If
        End If
    End Function
#End Region

#Region "Cac ham show message box"

    Public Shared Sub OpenNewWindow(ByVal pPage As System.Web.UI.Page, ByVal pURL As String, ByVal pTitle As String)
        Dim strScript As String = "<script language=JavaScript>"
        ' strScript += " var params = 'width='screen.availWidth-100', height='screen.availHeight-10', top='0', left='0', directories='no', location='no', menubar='no', resizable='no', scrollbars='yes', status='no', toolbar='no';"
        'strScript += ("window.open('" & pURL & "','") + pTitle & "','dialogWidth:600px;dialogHeight:500px;help:0;status:0;','_new')"
        strScript += ("window.open('" & pURL & "','") + pTitle & "','','_new')"
        strScript += "</script>"
        If Not pPage.ClientScript.IsStartupScriptRegistered("clientScript") Then
            pPage.ClientScript.RegisterStartupScript(GetType(String), "clientScript", strScript)
        End If

    End Sub

    Public Shared Sub ShowMessageBox(ByVal pPage As System.Web.UI.Page, ByVal pMessage As String)
        Dim strScript As String = "<script language=JavaScript>"
        strScript += "alert('" + pMessage + "');"
        strScript += "</script>"
        If Not pPage.ClientScript.IsStartupScriptRegistered("clientScript") Then
            pPage.ClientScript.RegisterStartupScript(GetType(String), "clientScript", strScript)
        End If
    End Sub
    Public Shared Sub ShowMessageBoxConfirm(ByVal pPage As System.Web.UI.Page, ByVal pMessage As String)
        Dim strScript As String = "<script language=JavaScript>"
        strScript += "alert('" + pMessage + "');"
        strScript += "</script>"
        If Not pPage.ClientScript.IsStartupScriptRegistered("clientScript") Then
            pPage.ClientScript.RegisterStartupScript(GetType(String), "clientScript", strScript)
        End If
    End Sub
    'function CheckForDataSaving() {
    '      var result = window.confirm("'Are you sure you want to navigate away from this tree node or page without saving your data?'");
    '      if (!result) {
    '          return (false);
    '      }
    '  }
    Public Shared Sub addPopUpCalendarToImage(ByVal image As System.Web.UI.WebControls.Image, ByVal textbox As System.Web.UI.WebControls.TextBox, ByVal datetimeFormat As String)
        image.Attributes.Add("onclick", "popUpCalendar(document.forms[0]." + textbox.ClientID + ", document.forms[0]." + textbox.ClientID + ", '" + datetimeFormat + "'); return false;")
    End Sub

#End Region
    Public Shared Sub ShowMessageBoxHQ247(ByVal pPage As System.Web.UI.Page, ByVal pMessage As String, Optional ByVal urlRedirect As String = "")
        Dim strScript As String = "<script language=JavaScript>"
        strScript += "alert('" + pMessage + "');"
        strScript += "if('" + urlRedirect + "' != '')"
        strScript += "{"
        strScript += "  window.location = '" + urlRedirect + "';"
        strScript += "}"
        strScript += "</script>"
        If Not pPage.ClientScript.IsStartupScriptRegistered("clientScript") Then
            pPage.ClientScript.RegisterStartupScript(GetType(String), "clientScript", strScript)
        End If
    End Sub
    Public Shared Sub ShowMessageBoxConfirmHQ247(ByVal pPage As System.Web.UI.Page, ByVal pMessage As String)
        Dim strScript As String = "<script language=JavaScript>"
        strScript += "var result = confirm('" & pMessage & "');"
        strScript += "if (result) {"
        strScript += " document.getElementById('ctl00_MstPg05_MainContent_dieuChinhConfirm').value = 'OK';"
        strScript += "}"
        strScript += "</script>"
        If Not pPage.ClientScript.IsStartupScriptRegistered("clientScript") Then
            pPage.ClientScript.RegisterStartupScript(GetType(String), "clientScript", strScript)
        End If
    End Sub

    Public Shared Sub WriteLog(ByVal ex As Exception, ByVal strDescription As String, Optional ByVal approverID As String = "")

        Dim sbErrMsg As StringBuilder
        sbErrMsg = New StringBuilder()
        sbErrMsg.Append("SessionID : ")
        sbErrMsg.Append(System.Web.HttpContext.Current.Session.SessionID)
        sbErrMsg.Append(vbCrLf)
        If approverID <> "" Then
            sbErrMsg.Append("approverID: " & approverID)
            sbErrMsg.Append(vbCrLf)
        End If
        sbErrMsg.Append(strDescription & " : ")
        sbErrMsg.Append(ex.Source)
        sbErrMsg.Append(vbCrLf)
        sbErrMsg.Append(ex.Message)
        sbErrMsg.Append(vbCrLf)
        sbErrMsg.Append(ex.StackTrace)

        LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)

        If Not ex.InnerException Is Nothing Then
            sbErrMsg = New StringBuilder()
            sbErrMsg.Append(strDescription & " : ")
            sbErrMsg.Append(ex.InnerException.Source)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.InnerException.Message)
            sbErrMsg.Append(vbCrLf)
            sbErrMsg.Append(ex.InnerException.StackTrace)

            LogDebug.WriteLog(sbErrMsg.ToString(), EventLogEntryType.Error)
        End If

        LogDebug.WriteLog("Error source: " & ex.Source & vbNewLine _
              & "Error code: System error!" & vbNewLine _
              & "Error message: " & ex.Message, EventLogEntryType.Error)
    End Sub
End Class
