﻿Imports System.Net
Imports System.Security.Cryptography.X509Certificates
Public Class CustomCertificatePolicy
    Implements ICertificatePolicy

    Public Function CheckValidationResult(ByVal sp As ServicePoint, ByVal cert As X509Certificate, ByVal req As WebRequest, ByVal problem As Integer) As Boolean Implements ICertificatePolicy.CheckValidationResult
        '* Return "true" to force the certificate to be accepted.
        Return True
    End Function
End Class