﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ETAX_GDT;
using System.Configuration;
using System.Collections;
using System.Data.OracleClient;
using VBOracleLib;
using System.IO.Compression;
using System.IO;
using System.Diagnostics;

namespace DChieu_NTDT
{
    public partial class DC_NTDT : Form
    {

        private static string strStatus = "";
        private static string strTimeDelay = ConfigurationSettings.AppSettings.Get("TIME_DC_DELAY").ToString();
        public DC_NTDT()
        {
            InitializeComponent();
        }

        private void DC_NTDT_Load(object sender, EventArgs e)
        {

            timer1.Enabled = true;
        }

        private void  DOICHIEU_NTDT() {
            //int timeDelay = 0;
            //timeDelay=Convert.ToInt32(strTimeDelay);
            //timeDelay = timeDelay * 60 * 1000;
            //DataSet dsCT = null;
            //string strSQL = "";
            //int tong_tc = 0;
            //int tong_tn_tct = 0;
            //int tong_tn_nh = 0;
            //int tong_lech_tct = 0;
            //int tong_lech_nh = 0;
            //string errorCode = "";
            //string errorMSG = "";
            //string strReturn = "";
            //strSQL = "SELECT * FROM TCS_QLY_DC_NTDT WHERE TO_CHAR(NGAY_DC,'dd/MM/rrrr')=:NGAY_DC order by ID_QLDC_NTDT desc";

            //IDbDataParameter[] p = null;
            //p = new IDbDataParameter[1];
            //p[0] = new OracleParameter();
            //p[0].ParameterName = "NGAY_DC";
            //p[0].DbType = DbType.String;
            //p[0].Direction = ParameterDirection.Input;
            //p[0].Value = DateTime.Now.ToString("dd/MM/yyyy");


            //dsCT = Utility.ExecuteReturnDataSet(strSQL, CommandType.Text, p);

            //try
            //{
            //    if (dsCT.Tables[0].Rows.Count > 0)
            //    {
            //        ProcessBaoCaoDoiChieu_NH_TTXL guibaocao = new ProcessBaoCaoDoiChieu_NH_TTXL();
            //        guibaocao.BaoCaoDoiChieu(DateTime.Now.ToString("dd/MM/yyyy"), ref tong_tc, ref tong_tn_tct, ref tong_tn_nh, ref tong_lech_tct, ref tong_lech_nh, ref errorCode, ref errorMSG);
            //        strStatus = "000000";
            //        Application.Exit();
            //    }
            //    else {
            //        strStatus = "999999";
            //        Utility.Log_Action("DCTD", "Error:999999-NO DATA");
            //        timer1.Interval = timeDelay;
            //        timer1.Enabled = true;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Utility.Log_Action("DCTD", "Error: " + ex.Message.ToString());
            //}
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DOICHIEU_NTDT();
        }
    }
}
