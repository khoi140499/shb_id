﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GiaHQsend
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                richTextBox2.Text = "";
                HQ247Service.EtaxServiceHQ svr = new HQ247Service.EtaxServiceHQ();
                richTextBox2.AppendText(svr.WSProcess(richTextBox1.Text));
            }
            catch (Exception ex)
            { richTextBox2.AppendText(ex.Message); }
        }
    }
}
