﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using APITThaiKBNN_TCT.Model;

namespace APITThaiKBNN_TCT
{
    public class DBConnect : DbContext
    {

        public DbSet<TCS_CTU_HDR> TCS_CTU_HDR { get; set; }
        public DbSet<TCS_CTU_NTDT_HDR> TCS_CTU_NTDT_HDR { get; set; }
        public DBConnect(string name) : base(name)
        {

        }
    }
}
