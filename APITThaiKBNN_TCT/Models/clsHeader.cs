﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APITThaiKBNN_TCT.Models
{
    public class clsHeader
    {
        public string sender_code { get; set; }
        public string sender_name { get; set; }
        public string receiver_code { get; set; }
        public string receiver_name { get; set; }
        public string tran_code { get; set; }
        public string msg_id { get; set; }
        public string transactionid { get; set; }
        public string send_date { get; set; }
    }
}