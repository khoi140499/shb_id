﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APITThaiKBNN_TCT.Models
{
    public class clsSendStatusReponse
    {
        public clsHeader Header { get; set; }
        public clsSendStatusRpData Data { get; set; }

        public clsSendStatusReponse()
        {
            this.Header = new clsHeader();
            this.Data = new clsSendStatusRpData();
        }
    }

    public class clsSendStatusRpData
    {
        public string code { get; set; }
        public string desc { get; set; }
        public string transactionrq { get; set; }
        public string total { get; set; }
        public string xData { get; set; }
    }
}