﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APITThaiKBNN_TCT.Models
{
    public class clsSendStatusRequest
    {
        public clsHeader Header { get; set; }
        public clsSendStatusData Data { get; set; }
    }

    public class clsSendStatusData
    {
        public string code { get; set; }
        public string desc { get; set; }
        public string refid { get; set; }
        public string soctetax { get; set; }
        public string typeMsg { get; set; }
        public string so_tchieu { get; set; }
    }
}