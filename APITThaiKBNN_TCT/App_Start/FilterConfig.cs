﻿using System.Web;
using System.Web.Mvc;

namespace APITThaiKBNN_TCT
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
