﻿using ETAX_GDT;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace APITThaiKBNN_TCT.Controllers
{

    public class APIKBNNController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [Route("api/queryTreasury")]
        [HttpPost]
        public object Post([FromBody] object obj)
        {
            log.Info("queryTreasury " + obj.ToString());
            clsSendStatusRequest ob = new clsSendStatusRequest();
            try
            {
                //check header
                if(!string.IsNullOrEmpty(Request.Headers.Authorization.ToString()))
                {
                    if (ProcessStatusKBNN.checkAuthen(Request.Headers.Authorization.ToString()))
                    {
                        //check code gui kb
                        string ck = ProcessStatusKBNN.CheckObjectRequest(obj.ToString(), ref ob);
                        if (!string.IsNullOrEmpty(ck))
                        {
                            return ProcessStatusKBNN.GetResponse("01", "Error - "+ ck,"","", ob);
                        }

                        switch (ob.Header.tran_code)
                        {
                            case "90006":
                                {
                                    return ProcessStatusKBNN.ProcessTranCode9006(ob);
                                }
                            case "90008":
                                {
                                    return ProcessStatusKBNN.ProcessTranCode90008(ob);
                                }
                        }
                    }
                    else
                    {
                        return ProcessStatusKBNN.GetResponse("03", "Your account is not authorized to access the requested resource", "","", ob);
                    }
                }
                else
                {
                    return ProcessStatusKBNN.GetResponse("02", "You are unauthorized to access the requested resource", "","", ob);
                }
            }catch(Exception ex)
            {
                return ProcessStatusKBNN.GetResponse("05", "Format message invalid","","", ob);
            }
            return "";
        }
    }
}
