﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Text;
using APITThaiKBNN_TCT.Models;
using System.Configuration;
using System.Runtime.InteropServices;
using APITThaiKBNN_TCT;
using VBOracleLib;
using System.Data;

namespace APITThaiKBNN_TCT
{

    public static class Process
    {
        private static readonly string ConnectDb = getConfig("CTC_DB_CONN", "");
        public static string getConfig(string name, [Optional] string defaulValue)
        {
            string AppName = "";
            try
            {
                AppName = ConfigurationManager.AppSettings[name].ToString();

            }
            catch (Exception ex)
            {
                AppName = "";
            }

            if (string.IsNullOrEmpty(AppName))
            {
                return defaulValue;
            }
            return AppName;
        }
        public static object GetResponse(string code, string desc, string total, string xData, clsSendStatusRequest objRq)
        {
            clsSendStatusReponse xxx = new clsSendStatusReponse();
            xxx.Header.receiver_code = objRq.Header ==null ? null : objRq.Header.sender_code;
            xxx.Header.receiver_name = objRq.Header == null ? null : objRq.Header.sender_name;
            xxx.Header.sender_code = "Etax";
            xxx.Header.sender_name = "Etax";
            xxx.Header.send_date = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            xxx.Header.transactionid = Guid.NewGuid().ToString();
            xxx.Header.tran_code = "90009";
            xxx.Data.code = code;
            xxx.Data.desc = desc;
            xxx.Data.total = string.IsNullOrEmpty(total) ? null : total;
            xxx.Data.xData = string.IsNullOrEmpty(xData) ? null : xData;
            xxx.Data.transactionrq = objRq.Header == null ? null: objRq.Header.transactionid;
            return xxx;
        }
        public static object ProcessTranCode9006(clsSendStatusRequest objRq)
        {
            string code = objRq.Data.code.Equals("00") ? "7" : "8";
            UpdateStatus(objRq.Data.refid, objRq.Data.soctetax, code);
            if (code.Equals("7"))
            {
                Task.Factory.StartNew(() =>
                {
                    Process.SendNotiTCT(objRq.Data.soctetax, objRq.Data.refid);
                });

            }
            return GetResponse("00", "Success","","",objRq);
        }
        public static object ProcessTranCode90008(clsSendStatusRequest objRq)
        {
            try
            {
                //string kqJson = ProcMegService.ProMsgJson.getMSG90008(objRq.Data.so_tchieu.Trim());

                return GetResponse("00", "Success", "", "", objRq);
            }
            catch(Exception ex)
            {
                return GetResponse("01", "Error ", "", "", objRq); ;
            }
           
        }

        public static void SendNotiTCT(string so_ct, string so_ct_nh)
        {
            try
            {
                
            }
            catch (Exception ex)
            {

            }
        }

        public static bool UpdateStatus(string soThamChieu, string so_ct, string trangThai)
        {
            try
            {
                string strSQL = "";
                //if (phan_he.Trim().Length > 0)
                //{
                //    switch (phan_he)
                //    {
                //        case "1":
                //            //thuế nội địa tại quầy
                //            strSQL = "UPDATE TCS_CTU_HDR SET trangthai_sendkb='" + trang_thai + "',ten_trangthai_sendkb='ten_trang_thai'  WHERE MATHAMCHIEU='" + strSO_THAMCHIEU + "'";
                //            break;
                //        case "2":
                //            //thuế nội địa điện tử
                //            strSQL = "UPDATE TCS_CTU_NTDT_HDR SET trangthai_sendkb='" + trang_thai + "',ten_trangthai_sendkb='ten_trang_thai'  WHERE ma_thamchieu_knop='" + strSO_THAMCHIEU + "'";
                //            break;
                //        case "3":
                //            //thuế etax mobile
                //            strSQL = "UPDATE tcs_ctu_hdr_mobitct SET trangthai_sendkb='" + trang_thai + "',ten_trangthai_sendkb='ten_trang_thai'  WHERE ma_thamchieu_knop='" + strSO_THAMCHIEU + "'";
                //            break;

                //    }
                //    DataAccess.ExecuteNonQuery(strSQL, CommandType.Text);
                //}
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool checkAuthen(string request)
        {
            if (string.IsNullOrEmpty(request))
            {
                return false;
            }
            else
            {
                if (request.Split(' ')[0].Equals("Basic"))
                {
                    Encoding encoding = Encoding.GetEncoding("UTF-8");
                    string token = encoding.GetString(Convert.FromBase64String(request.Split(' ')[1]));
                    string tokenValid = getConfig("UserName") + ":" + getConfig("PassWord");
                    if (token.Equals(tokenValid))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public static string CheckObjectRequest(string request, ref clsSendStatusRequest obj)
        {
            try
            {
                clsSendStatusRequest rq = new clsSendStatusRequest();
                rq = JsonConvert.DeserializeObject<clsSendStatusRequest>(request);
                if (string.IsNullOrEmpty(rq.Header.sender_code))
                {
                    return "Sender_Code is null";
                }
                if (string.IsNullOrEmpty(rq.Header.sender_name))
                {
                    return "Sender_Name is null";
                }
                if (string.IsNullOrEmpty(rq.Header.receiver_code))
                {
                    return "Receiver_code is null";
                }
                if (string.IsNullOrEmpty(rq.Header.receiver_name))
                {
                    return "Receiver_name is null";
                };
                if (string.IsNullOrEmpty(rq.Header.send_date))
                {
                    return "Send_date is null";
                }
                if (string.IsNullOrEmpty(rq.Header.transactionid))
                {
                    return "TransactionId is null";
                }
                if (string.IsNullOrEmpty(rq.Header.tran_code))
                {
                    return "Tran_code is null";
                }
                if (!rq.Header.tran_code.Equals("90008"))
                {
                    return "Tran_code invalid";
                }
                if (!CheckTransactionid(rq.Header.transactionid))
                {
                    return "TransactionId already exist";
                }
                if (string.IsNullOrEmpty(rq.Data.code))
                {
                    return "Code is null";
                }
                if (string.IsNullOrEmpty(rq.Data.desc))
                {
                    return "Desc is null";
                }
                if (string.IsNullOrEmpty(rq.Data.refid))
                {
                    return "Refid is null";
                }
                if (string.IsNullOrEmpty(rq.Data.soctetax))
                {
                    return "Soctetax is null";
                }
                obj = rq;
                return "";
            }
            catch (Exception ex)
            {
                return "Message format invalid";
            }
        }
        private static bool CheckTransactionid(string trans)
        {
            try
            {
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
