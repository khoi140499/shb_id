﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BIDVUtil
{
    public class Common
    {
        //private static readonly string[] VietnameseSigns = new string[] { "aAeEoOuUiIdDyY", "\x00e1\x00e0ạả\x00e3\x00e2ấầậẩẫăắằặẳẵ", "\x00c1\x00c0ẠẢ\x00c3\x00c2ẤẦẬẨẪĂẮẰẶẲẴ", "\x00e9\x00e8ẹẻẽ\x00eaếềệểễ", "\x00c9\x00c8ẸẺẼ\x00caẾỀỆỂỄ", "\x00f3\x00f2ọỏ\x00f5\x00f4á??á??á??á??á??Æ?á??á??á??á?á??", "\x00d3\x00d2á?á?\x00d5\x00d4á??á??á??á??á??Æ?á?á?á??á?á??", "\x00fa\x00f9á??á??Å?Æ?á??á??á??á??á??", "\x00da\x00d9á??á??Å?Æ?á??á?ªá??á??á??", "\x00ed\x00ecá??á??Ä?", "\x00cd\x00ccá?á??Ä?", "Ä?", "Ä?", "\x00fdá??á?µá??á??", "\x00ddá??á??á??á??" };
        private static readonly string[] VietnameseSigns = new string[] { "aAeEoOuUiIdDyY", "\x00e1\x00e0ạả\x00e3\x00e2ấầậẩẫăắằặẳẵ", "\x00c1\x00c0ẠẢ\x00c3\x00c2ẤẦẬẨẪĂẮẰẶẲẴ", "\x00e9\x00e8ẹẻẽ\x00eaếềệểễ", "\x00c9\x00c8ẸẺẼ\x00caẾỀỆỂỄ", "\x00f3\x00f2ọỏ\x00f5\x00f4ốồộổỗơớờợởỡ", "\x00d3\x00d2ỌỎ\x00d5\x00d4ỐỒỘỔỖƠỚỜỢỞỠ", "\x00fa\x00f9ụủũưứừựửữ", "\x00da\x00d9ỤỦŨƯỨỪỰỬỮ", "\x00ed\x00ecịỉĩ", "\x00cd\x00ccỊỈĨ", "đ", "Đ", "\x00fdỳỵỷỹ", "\x00ddỲỴỶỸ" };
        /// <summary>
        /// Removes the sign4 vietnamese string.
        /// </summary>
        /// <param name="str">The STR.</param>
        /// <returns></returns>
        public static string RemoveSign4VietnameseString(string str)
        {
            for (int i = 1; i < VietnameseSigns.Length; i++)
            {
                for (int k = 0; k < VietnameseSigns[i].Length; k++)
                {
                    str = str.Replace(VietnameseSigns[i][k], VietnameseSigns[0][i - 1]);
                }
            }
            string str2 = "";
            for (int j = 0; j < str.Length; j++)
            {
                int num4 = Convert.ToInt32(str[j]);
                if ((num4 >= 0x20) && (num4 <= 0x7f))
                {
                    str2 = str2 + str[j];
                }
            }
            return str2;
        }

    }
}
