﻿using Business.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using VBOracleLib;

namespace APIMOBIINGNT
{
    public class InProcess
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static string objInCTU(string strMaThamChieu, string publickey)
        {
            log.Info("1.objInCTU: Begin");
            try
            {
                string mv_SoCT = "";
                string strMa_Lthue = "";
                string strObjIn = "";
                //kiem tra ma tham chieu co ton tai
                string strSQL = "SELECT MATHAMCHIEU,SO_CT,MALOAIHINHTHUE,HUYENNGUOINOP,TINHNGUOINOP,SOTAIKHOAN_THE,TEN_NH_A FROM SET_CTU_HDR_MOBITCT A WHERE A.MATHAMCHIEU='" + strMaThamChieu + "' AND TRANGTHAI IN ('05','06','09','10') ";
                DataSet dsctu = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (mdlCommon.IsEmptyDataSet(dsctu))
                {
                    log.Info("Khong tim thay, hoac trang thai chung tu [" + strMaThamChieu + "] khong dung ");
                    return "";
                }
                mv_SoCT = dsctu.Tables[0].Rows[0]["SO_CT"].ToString();
                if (mv_SoCT.Length <= 0)
                    return "";

                if (dsctu.Tables[0].Rows[0]["MALOAIHINHTHUE"].ToString().Equals("02"))
                    strMa_Lthue = "03";


                string TEN_HUYEN_NNT = dsctu.Tables[0].Rows[0]["HUYENNGUOINOP"].ToString();
                string TEN_TINH_NNT = dsctu.Tables[0].Rows[0]["TINHNGUOINOP"].ToString();
                string strTK_NH_KH = dsctu.Tables[0].Rows[0]["SOTAIKHOAN_THE"].ToString();
                string strTEN_NH_A = dsctu.Tables[0].Rows[0]["TEN_NH_A"].ToString();
                log.Info("Load chung tu OK");
                Business.NewChungTu.infChungTuHDR hdr = new Business.NewChungTu.infChungTuHDR();
                Business.MobiTCT.MobiTCTCTU objCTU = new Business.MobiTCT.MobiTCTCTU();
                hdr = objCTU.CTU_HeaderIN(mv_SoCT);
                string v_strMa_CN_Uer = TCS_GetMaChiNhanh(hdr.Ma_NV.ToString());

                string v_strBranchCode = TCS_GetMaCN_NH(v_strMa_CN_Uer);
                string v_strNameCN = TCS_GetTenCNNH(v_strMa_CN_Uer);
                DataSet ds = new DataSet();
                if (strMa_Lthue.Equals("03"))
                    ds = objCTU.CTU_IN_TRUOCBA(mv_SoCT);
                else
                    ds = objCTU.CTU_IN_LASER(mv_SoCT, hdr.Ngay_BK);
                string strFileName = "";
                string v_strReportName = "In chứng từ thu ngân sách";

                objInCTU objIn = new objInCTU();
                objIn.HDR = new objHDRInCTU();
                objIn.HDR.TienPhi = Globals.Format_Number(hdr.PHI_GD.ToString(), ".");
                objIn.HDR.TienVAT = Globals.Format_Number(hdr.PHI_VAT.ToString(), ".");
                objIn.HDR.KHCT = hdr.KyHieu_CT;
                objIn.HDR.SoCT = hdr.So_CT;
                objIn.HDR.TK_KN_NH = gf_CorrectString(strTK_NH_KH);
                objIn.HDR.Ten_NNThue = hdr.Ten_NNThue.ToString();
                objIn.HDR.Ma_NNThue = gf_CorrectString(hdr.Ma_NNThue);
                objIn.HDR.DC_NNThue = gf_CorrectString(hdr.DC_NNThue);

                //chỗ này điền mã, không điền tên -> cần lấy ra mã
                if(string.IsNullOrEmpty(TEN_HUYEN_NNT.Trim())){
                    objIn.HDR.Huyen_NNThue = "";
                }
                else{
                    string[] arrhuyen = TEN_HUYEN_NNT.Split('-');
                    string ten_huyen = arrhuyen[1];
                    objIn.HDR.Huyen_NNThue = gf_CorrectString(ten_huyen.Trim());
                }

                //
                if (string.IsNullOrEmpty(TEN_TINH_NNT.Trim()))
                {
                    objIn.HDR.Tinh_NNThue = "";
                }
                else
                {
                    string[] arrtinh = TEN_TINH_NNT.Split('-');
                    string ten_tinh = arrtinh[1];
                    objIn.HDR.Tinh_NNThue = gf_CorrectString(ten_tinh.Trim());
                }
                //
                

                objIn.HDR.Ten_NNTien = gf_CorrectString(hdr.Ten_NNTien);
                objIn.HDR.Ma_NNTien = gf_CorrectString(hdr.Ma_NNTien);
                objIn.HDR.DC_NNTien = gf_CorrectString(hdr.DC_NNTien);
                objIn.HDR.Huyen_NNTien = gf_CorrectString("");
                objIn.HDR.Tinh_NNTien = gf_CorrectString("");
                objIn.HDR.NHA = gf_CorrectString(strTEN_NH_A);
                objIn.HDR.NHB = gf_CorrectString(hdr.Ten_NH_B.ToString());

                objIn.HDR.Ten_KB = mdlCommon.Get_TenKhoBac(hdr.SHKB);
                objIn.HDR.Tinh_KB = gf_CorrectString(hdr.QUAN_HUYEN_NNTIEN);
                objIn.HDR.Ma_CQThu =  gf_CorrectString(hdr.Ma_CQThu);
                objIn.HDR.Ten_CQThu = gf_CorrectString(hdr.Ten_cqthu);

                objIn.HDR.So_TK = hdr.So_TK;
                objIn.HDR.Ngay_TK = "";
                objIn.HDR.Ten_CQThu = gf_CorrectString(hdr.Ten_cqthu);
                objIn.HDR.Ten_CQThu = gf_CorrectString(hdr.Ten_cqthu);

                if (hdr.Ma_NT.Equals("VND"))
                    objIn.HDR.Tien_Bang_Chu = mdlCommon.TCS_Dich_So(hdr.TTien, "đồng");
                else
                    objIn.HDR.Tien_Bang_Chu = mdlCommon.TCS_Dich_So((hdr.TTien * hdr.Ty_Gia), "đồng");

                objIn.HDR.SO_FT = hdr.So_CT_NH;
                objIn.HDR.Ten_KeToan = "";

                if (hdr.PT_TT.Equals("01"))
                     objIn.HDR.Key_MauCT = "1";
                else
                    objIn.HDR.Key_MauCT = "0";

                if (hdr.PT_TT.ToString().Equals("01"))
                    hdr.PT_TT = "01";

                if (hdr.PT_TT.Equals("05"))
                    hdr.PT_TT = "00";

                objIn.HDR.PT_TT = hdr.PT_TT.ToString();
                objIn.HDR.ma_CN = v_strBranchCode;
                objIn.HDR.ten_cn = v_strNameCN;
                objIn.HDR.Ngay_GD = Convert.ToDateTime(hdr.NGAY_KH_NH).ToString("dd/MM/yyyy  HH:mm:ss");
                objIn.HDR.TienTKNO = Globals.Format_Number((Double.Parse(hdr.TTien.ToString()) + Double.Parse(hdr.PHI_GD.ToString()) + Double.Parse(hdr.PHI_VAT.ToString())).ToString(), ".");
                objIn.HDR.TienTKNH = Globals.Format_Number(hdr.TTien.ToString(), ".");
                objIn.HDR.TKNo = gf_CorrectString(strTK_NH_KH);
   
                //if (!hdr.Ma_NT.Equals("VND") && !hdr.Ma_NT.Equals("USD"))
                //    objIn.HDR.Ma_NT = gf_CorrectString(hdr.Ma_NT);
                //else
                //    objIn.HDR.Ma_NT = "";


                objIn.HDR.Ma_NT = gf_CorrectString(hdr.Ma_NT);
                objIn.HDR.LoaiTien = gf_CorrectString(hdr.Ma_NT);

                objIn.HDR.SO_FCC = strMaThamChieu;
                objIn.HDR.TEN_GDV = "";
                objIn.HDR.IsTamThu = hdr.MA_NTK;
                objIn.HDR.Ngay_NH_KH = Convert.ToDateTime(hdr.NGAY_KH_NH).ToString("dd/MM/yyyy  HH:mm:ss");
                

                DateTime dNoptien = Convert.ToDateTime(hdr.NGAY_KH_NH);

                objIn.HDR.Ngay_noptien = "Ngày " + dNoptien.ToString("dd") + " tháng " + dNoptien.ToString("MM") + " năm " + dNoptien.ToString("yyyy");
                objIn.HDR.TKCo = hdr.TK_Co;

                objIn.HDR.CTU = new List<objDTLInCTU>();

                for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    objDTLInCTU objDtl = new objDTLInCTU();
                    objDtl.STT = ds.Tables[0].Rows[i]["STT"].ToString();
                    objDtl.SO_CT = ds.Tables[0].Rows[i]["SO_CT"].ToString();
                    objDtl.NOI_DUNG = ds.Tables[0].Rows[i]["NOI_DUNG"].ToString();
                    objDtl.CC = ds.Tables[0].Rows[i]["CC"].ToString();
                    objDtl.LK = ds.Tables[0].Rows[i]["LK"].ToString();
                    objDtl.MTM = ds.Tables[0].Rows[i]["MTM"].ToString();
                    objDtl.KYTHUE = ds.Tables[0].Rows[i]["KYTHUE"].ToString();
                    objDtl.SOTIEN = ds.Tables[0].Rows[i]["SOTIEN"].ToString();
                    objDtl.SOTIEN_NT = ds.Tables[0].Rows[i]["SOTIEN_NT"].ToString();
                    objDtl.NGAY_TK = ds.Tables[0].Rows[i]["NGAY_TK"].ToString();
                    objDtl.MA_DBHC = ds.Tables[0].Rows[i]["MA_DBHC"].ToString();

                    objIn.HDR.CTU.Add(objDtl);
                }

                strObjIn = Newtonsoft.Json.JsonConvert.SerializeObject(objIn);
                return strObjIn;
            }
            catch (Exception ex)
            {
                log.Info("99.objInCTU: " + ex.Message + ex.StackTrace);
            }

            return "";
        }

        public static string GenerateKey(string strSeq, int intLength = 0)
        {
            string strReSult = "";
            try
            {
                string strSQL = "SELECT LPAD(" + strSeq + ".NEXTVAL," + intLength + ",'0')  FROM DUAL";
                if (intLength == 0)
                    strSQL = "SELECT " + strSeq + ".NEXTVAL  FROM DUAL";
                DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
                if (ds != null)
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            strReSult = ds.Tables[0].Rows[0][0].ToString();
                if (intLength > 0 && strReSult.Length <= intLength)
                    strReSult = strReSult.PadLeft(intLength, '0');
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
            }
            return strReSult;
        }
        public static string TCS_GetMaChiNhanh(string strMa_NV)
        {
            try
            {
                string strSQL = string.Empty;
                strSQL = "SELECT MA_CN FROM TCS_DM_NHANVIEN WHERE MA_NV='" + strMa_NV + "'";
                return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                throw ex;

            }
            // return "";
        }
        public static string TCS_GetMaCN_NH(string strMa_PGD)
        {
            try
            {
                string strSQL = string.Empty;
                strSQL = "SELECT a.id, a.name, a.branch_code FROM tcs_dm_chinhanh a where a.id='" + strMa_PGD + "' ";
                return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                throw ex;
            }
        }
        public static string TCS_GetTenCNNH(string p_strMaCN)
        {
            try
            {
                string strSQL = string.Empty;
                strSQL = "SELECT a.id, a.name, a.branch_id FROM tcs_dm_chinhanh a where a.branch_id='" + p_strMaCN + "' ";
                return DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text).Tables[0].Rows[0][1].ToString();
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + " " + ex.StackTrace);
                throw ex;
            }
        }
        public static string gf_CorrectString(object data)
        {
            if (data != null)
            {
                return data.ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        public static string getLogID()
        {
            string strResult = DateTime.Now.ToString("yyyyMMdd");
            string strSQL = "SELECT LPAD( SEQ_LOG_API_TCT_MOBI.NEXTVAL,8,'0') FROM DUAL";
            DataSet ds = DataAccess.ExecuteReturnDataSet(strSQL, CommandType.Text);
            if (ds != null)
                if (ds.Tables.Count > 0)
                    if (ds.Tables[0].Rows.Count > 0)
                        strResult = strResult + ds.Tables[0].Rows[0][0].ToString();
            return strResult;
        }
        public static string InsertLog(string strId, string strMSG, string strMSGType)
        {
            try
            {
                string strSQL = "INSERT INTO SET_LOG_API_TCT_MOBI(ID, REQ_TYPE, REQ_MSG, REQ_TIME)";
                strSQL += "VALUES(:ID, :REQ_TYPE, :REQ_MSG, SYSDATE)";

                List<IDbDataParameter> lst = new List<IDbDataParameter>();
                lst.Add(DataAccess.NewDBParameter("ID", ParameterDirection.Input, strId, DbType.String));
                lst.Add(DataAccess.NewDBParameter("REQ_TYPE", ParameterDirection.Input, strMSGType, DbType.String));
                lst.Add(DataAccess.NewDBParameter("REQ_MSG", ParameterDirection.Input, strMSG, DbType.String));
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, lst.ToArray());
                return "";

            }
            catch (Exception ex)
            {
                log.Info("InsertLog :" + strMSG);
                log.Error(ex.Message + "-" + ex.StackTrace);
                return "";
            }
            return "";

        }
        public static string UpdateLog(string strId, string strRESMSG, string strErrCode, string strErrMSG, string strMST = "")
        {
            try
            {
                string strSQL = "UPDATE SET_LOG_API_TCT_MOBI SET RES_TIME=sysdate, RES_MSG=:RES_MSG, ERRCODE=:ERRCODE, ERRMSG=:ERRMSG,MST='" + strMST + "' where id='" + strId + "'";


                List<IDbDataParameter> lst = new List<IDbDataParameter>();

                lst.Add(DataAccess.NewDBParameter("RES_MSG", ParameterDirection.Input, strRESMSG, DbType.String));
                lst.Add(DataAccess.NewDBParameter("ERRCODE", ParameterDirection.Input, strErrCode, DbType.String));
                lst.Add(DataAccess.NewDBParameter("ERRMSG", ParameterDirection.Input, strErrMSG, DbType.String));
                DataAccess.ExecuteNonQuery(strSQL, CommandType.Text, lst.ToArray());
                return "";

            }
            catch (Exception ex)
            {
                log.Info("Update Log:" + strRESMSG);
                log.Error(ex.Message + "-" + ex.StackTrace);
                return "";
            }
            return "";

        }
        public static string verifyCKS(string content)
        {

            try
            {
                //string SIGNATUREURL = ConfigurationManager.AppSettings["DIGITAL.SIGNATURE.URL"];
                string SIGNATUREURL = Business.Common.mdlCommon.TCS_GETTS("DIGITAL.SIGNATURE.URL");
                string certPath = ConfigurationManager.AppSettings["CertPathTCT"];

                log.Info("2.ValidSign_IN - SIGNATUREURL:" + SIGNATUREURL + "------- certPath: " + certPath);
                string valid = "false";
                string rs = "";

                byte[] bufferCertPath = Encoding.UTF8.GetBytes(certPath);
                string maskedCertPath = Convert.ToBase64String(bufferCertPath);

                rs = MessageRequest(SIGNATUREURL + "/VerifyTCT/" + maskedCertPath, content);

                if (rs.Equals("true"))
                {
                    valid = "true";
                }
                else
                {
                    log.Info("Error source: DigitalSignature | ValidSign_IN()\\n" + "Error code: System error!" + "\\n" + "Error message: " + rs);
                    valid = "false";
                }
                log.Info("3.getVerifyMobile - rs:" + rs);
                //output = rs;
                return valid;
            }
            catch (Exception ex)
            {
                log.Info("Error source: " + ex.Source + " | ValidSign_IN()\\n" + "Error code: System error!" + "\\n" + "Error message: " + ex.Message);
                return "false";
            }
        }
        public static string MessageRequest(string url, string content)
        {
            string strResponse = "";
            try
            {
                string strURL = url;
                byte[] dataByte = Encoding.UTF8.GetBytes(content);

                HttpWebRequest POSTRequest = (HttpWebRequest)WebRequest.Create(strURL);
                //Method type
                POSTRequest.Method = "POST";
                // Data type - message body coming in xml
                POSTRequest.ContentType = "text/xml";
                POSTRequest.KeepAlive = true;
                //POSTRequest.Timeout = WS_TIMEPOUT '5000
                //Content length of message body
                POSTRequest.ContentLength = dataByte.Length;

                // Get the request stream
                Stream POSTstream = POSTRequest.GetRequestStream();
                // Write the data bytes in the request stream
                POSTstream.Write(dataByte, 0, dataByte.Length);

                //Get response from server
                HttpWebResponse POSTResponse = (HttpWebResponse)POSTRequest.GetResponse();

                StreamReader reader = new StreamReader(POSTResponse.GetResponseStream(), Encoding.UTF8);
                strResponse = reader.ReadToEnd().ToString();


            }
            catch (Exception ex)
            {
                log.Info("Error source: " + ex.Source + " | MessageRequest_MOBILEI_IN()\\n" + "Error code: System error!" + "\\n" + "Error message: " + ex.Message);
            }

            return strResponse;
        }
    }
}