﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace APIMOBIINGNT
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class InGNTService : System.Web.Services.WebService
    {

        [WebMethod]
        public string WSProcess(string msg)
        {
            string strPublicKey = "xxxxxxxxxxx";
            try
            {
                if (!Directory.Exists(Server.MapPath("\\" + DateTime.Now.ToString("yyyyMMdd"))))
                {
                    Directory.CreateDirectory(Server.MapPath("\\" + DateTime.Now.ToString("yyyyMMdd")));
                }
                File.WriteAllText(Server.MapPath("\\" + DateTime.Now.ToString("yyyyMMdd")) + "\\msg_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".txt", msg);
            }
            catch (Exception ex)
            { }
            return APIMOBIINGNT.InProcess.objInCTU(msg, strPublicKey);

        }

        [WebMethod]
        public string GetID(string msg)
        {
            string strPublicKey = "xxxxxxxxxxx";
            try
            {
                if (!Directory.Exists(Server.MapPath("\\" + DateTime.Now.ToString("yyyyMMdd"))))
                {
                    Directory.CreateDirectory(Server.MapPath("\\" + DateTime.Now.ToString("yyyyMMdd")));
                }
                //File.WriteAllText(Server.MapPath("\\" + DateTime.Now.ToString("yyyyMMdd")) + "\\msg_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".txt", msg);
            }
            catch (Exception ex)
            { }
            return APIMOBIINGNT.InProcess.getLogID();

        }
        [WebMethod]
        public string InsertLog(string strId, string strMSG, string strMSGType)
        {
            string strPublicKey = "xxxxxxxxxxx";
            try
            {
                if (!Directory.Exists(Server.MapPath("\\" + DateTime.Now.ToString("yyyyMMdd"))))
                {
                    Directory.CreateDirectory(Server.MapPath("\\" + DateTime.Now.ToString("yyyyMMdd")));
                }
                //File.WriteAllText(Server.MapPath("\\" + DateTime.Now.ToString("yyyyMMdd")) + "\\msg_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".txt", msg);
            }
            catch (Exception ex)
            { }
            return APIMOBIINGNT.InProcess.InsertLog(strId, strMSG, strMSGType);

        }
        [WebMethod]
        public string UpdateLog(string strId, string strRESMSG, string strErrCode, string strErrMSG, string strMST)
        {
            string strPublicKey = "xxxxxxxxxxx";
            try
            {
                if (!Directory.Exists(Server.MapPath("\\" + DateTime.Now.ToString("yyyyMMdd"))))
                {
                    Directory.CreateDirectory(Server.MapPath("\\" + DateTime.Now.ToString("yyyyMMdd")));
                }
               // File.WriteAllText(Server.MapPath("\\" + DateTime.Now.ToString("yyyyMMdd")) + "\\msg_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".txt", msg);
            }
            catch (Exception ex)
            { }
            return APIMOBIINGNT.InProcess.UpdateLog(strId, strRESMSG, strErrCode, strErrMSG, strMST);

        }
        [WebMethod]
        public string VerifyCKS(string msg)
        {
            string strPublicKey = "xxxxxxxxxxx";
            return APIMOBIINGNT.InProcess.verifyCKS(msg);

        }

    }
}