﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.IO;
using System.Data;

namespace OpenXMLExcel.SLExcelUtility
{
    public class SLExcelReader
    {
        private string GetColumnName(string cellReference)
        {
            var regex = new Regex("[A-Za-z]+");
            var match = regex.Match(cellReference);

            return match.Value;
        }

        private int ConvertColumnNameToNumber(string columnName)
        {
            var alpha = new Regex("^[A-Z]+$");
            if (!alpha.IsMatch(columnName)) throw new ArgumentException();

            char[] colLetters = columnName.ToCharArray();
            Array.Reverse(colLetters);

            var convertedValue = 0;
            for (int i = 0; i < colLetters.Length; i++)
            {
                char letter = colLetters[i];
                int current = i == 0 ? letter - 65 : letter - 64; // ASCII 'A' = 65
                convertedValue += current * (int)Math.Pow(26, i);
            }

            return convertedValue;
        }

        private IEnumerator<Cell> GetExcelCellEnumerator(Row row)
        {
            int currentCount = 0;
            foreach (Cell cell in row.Descendants<Cell>())
            {
                string columnName = GetColumnName(cell.CellReference);

                int currentColumnIndex = ConvertColumnNameToNumber(columnName);

                for (; currentCount < currentColumnIndex; currentCount++)
                {
                    var emptycell = new Cell() { DataType = null, CellValue = new CellValue(string.Empty) };
                    yield return emptycell;
                }

                yield return cell;
                currentCount++;
            }
        }

        private string ReadExcelCell(Cell cell, WorkbookPart workbookPart)
        {
            var cellValue = cell.CellValue;
            var text = (cellValue == null) ? cell.InnerText : cellValue.Text;
            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString))
            {
                text = workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(
                        Convert.ToInt32(cell.CellValue.Text)).InnerText;
            }

            return (text ?? string.Empty).Trim();
        }

        public SLExcelData ReadExcel(string file)
        {
            var data = new SLExcelData();

            // Open the excel document
            WorkbookPart workbookPart; List<Row> rows;
            SpreadsheetDocument document = null;
            try
            {
                try
                {
                    document = SpreadsheetDocument.Open(file, false);
                    workbookPart = document.WorkbookPart;

                    var sheets = workbookPart.Workbook.Descendants<Sheet>();
                    var sheet = sheets.First();
                    data.SheetName = sheet.Name;

                    var workSheet = ((WorksheetPart)workbookPart.GetPartById(sheet.Id)).Worksheet;
                    var columns = workSheet.Descendants<Columns>().FirstOrDefault();
                    data.ColumnConfigurations = columns;

                    var sheetData = workSheet.Elements<SheetData>().First();
                    rows = sheetData.Elements<Row>().ToList();
                    //document.Close();
                }
                catch (Exception e)
                {
                    data.Status.Message = "Unable to open the file";
                    return data;
                }

                // Read the header
                if (rows.Count > 0)
                {
                    var row = rows[0];
                    var cellEnumerator = GetExcelCellEnumerator(row);
                    while (cellEnumerator.MoveNext())
                    {
                        var cell = cellEnumerator.Current;
                        var text = ReadExcelCell(cell, workbookPart).Trim();
                        data.Headers.Add(text);
                    }
                }

                // Read the sheet data
                if (rows.Count > 1)
                {
                    for (var i = 1; i < rows.Count; i++)
                    {
                        var dataRow = new List<string>();
                        data.DataRows.Add(dataRow);
                        var row = rows[i];
                        var cellEnumerator = GetExcelCellEnumerator(row);
                        while (cellEnumerator.MoveNext())
                        {
                            var cell = cellEnumerator.Current;
                            var text = ReadExcelCell(cell, workbookPart).Trim();
                            dataRow.Add(text);
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
            finally
            {
                if (document != null)
                    document.Close();
            }

            return data;
        }
        public DataTable ReadExcelFileByEEp(string fileName)
        {
            DataTable dt = new DataTable();

            //using (ExcelPackage package = new ExcelPackage(new FileInfo(fileName)))
            //{
            //    if (package.Workbook.Worksheets.Count < 1)
            //    { // Log - Không có sheet nào tồn tại trong file excel của bạn return null; 
            //    }
            //    // Lấy Sheet đầu tiện trong file Excel để truy vấn // Truyền vào name của Sheet để
            //    //lấy ra sheet cần, nếu name = null thì lấy sheet đầu tiên 
            //    ExcelWorksheet workSheet = package.Workbook.Worksheets.FirstOrDefault();
            //    // Đọc tất cả data bắt đầu từ row thứ 2
            //    for (var rowNumber = 2; rowNumber <= workSheet.Dimension.End.Row; rowNumber++)
            //    {
            //        // Lấy 1 row trong excel để truy vấn
            //        var row = workSheet.Cells[rowNumber, 1, rowNumber, workSheet.Dimension.End.Column];
            //        // tạo 1 row trong data table
            //        var newRow = dt.NewRow();
            //        foreach (var cell in row)
            //        {
            //            newRow[cell.Start.Column - 1] = cell.Text;
            //        }
            //        dt.Rows.Add(newRow);
            //    }
            //}
           
            return dt;
        }
    }
}