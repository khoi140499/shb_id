﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlexCel.Core;
using FlexCel.Render;
using FlexCel.XlsAdapter;
using System.Data;
using System.Configuration;
using System.IO;
using System.Windows.Media.Imaging;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.Util;
using log4net;
namespace OpenXMLExcel.SLExcelUtility
{
    public class ExportFileDownloadActionResult
    {
       // private static   CoreBankSV_MSB.clsLogCoreBank _clsLog = new CoreBankSV_MSB.clsLogCoreBank();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static DataTable AddRowNumber(DataTable dataTable)
        {
            dataTable.Columns.Add("NUMBER", typeof(int));
            for (var i = 0; i < dataTable.Rows.Count; i++) { dataTable.Rows[i]["NUMBER"] = i + 1; }
            return dataTable;
        }
        public static string CreateFileReportDefault(string pathRpt, string pathSave, DataTable dt, string tableName, IEnumerable<ExcelTitleAlias> lsAliases)
        {
            string strResultPath = string.Empty;
            try
            {
                ExcelFile xlsSource = CreateReportExcel(pathRpt, dt, tableName, lsAliases);
                strResultPath = ExcelProcess(xlsSource, dt, tableName, pathSave);

            }
            catch (Exception ex)
            { log.Error(ex.Message + ex.StackTrace); }
            return strResultPath;
        }
        public static string ExcelProcess(ExcelFile xlsSource, DataTable dataTable, string tableName, string strFileName)
        {
            string strResultPath = string.Empty;
            try
            {
                strResultPath = strFileName;
                dataTable.TableName = tableName;
                xlsSource.Save(strResultPath);

                return strResultPath;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                throw ex;

            }
            return strResultPath;
        }
        public static ExcelFile CreateReportExcel(string dataPath, DataTable dt, string tableName, IEnumerable<ExcelTitleAlias> lsAliases)
        {
            try
            {
                var result = new XlsFile(true);
                result.Open(dataPath);
                var flcReport = new FlexCel.Report.FlexCelReport();
                dt.TableName = tableName;
                flcReport.AddTable(tableName, dt);
                foreach (var item in lsAliases)
                {
                    flcReport.SetValue(item.AliasName, item.AliasValue);
                }
                flcReport.Run(result);
                return result;
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);

                return new XlsFile(true);
            }
        }
        public static string CreateFileReport(string pathRpt, string pathSave, DataTable dt, string tableName, IEnumerable<ExcelTitleAlias> lsAliases)
        {
            var strResultPath = string.Empty;
            try
            {
                //    dt.Rows.RemoveAt(0);
                //    dt.AcceptChanges();
                // khởi tạo wb rỗng

                FileStream fss = new FileStream(pathRpt, FileMode.Open);

                var wb = new HSSFWorkbook(fss);

                ISheet sheet = wb.GetSheetAt(0);

                //var row0 = sheet.CreateRow(0);

                //// Merge lại row đầu 4 cột
                //row0.CreateCell(0); // tạo ra cell trc khi merge
                //CellRangeAddress cellMerge = new CellRangeAddress(0, 0, 0, 4);
                //sheet.AddMergedRegion(cellMerge);
                //row0.GetCell(0).SetCellValue("DANH SÁCH LỖI IMPORT");
                //row0.RowStyle.Alignment = HorizontalAlignment.Center;
                //row0.RowStyle.VerticalAlignment = VerticalAlignment.Center;


                //var title = sheet.CreateRow(1);
                //title.CreateCell(0).SetCellValue("STT");
                //title.CreateCell(1).SetCellValue("MST");
                //title.CreateCell(2).SetCellValue("CD_NO");
                //title.CreateCell(3).SetCellValue("CD_DATE");
                //title.CreateCell(4).SetCellValue("NOTE");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // tao row mới
                    var newRow = sheet.CreateRow(i + 2);

                    // set giá trị
                    newRow.CreateCell(0).SetCellValue(dt.Rows[i]["STT"].ToString());
                    newRow.CreateCell(1).SetCellValue(dt.Rows[i]["MST"].ToString());
                    newRow.CreateCell(2).SetCellValue(dt.Rows[i]["CD_NO"].ToString());
                    newRow.CreateCell(3).SetCellValue(dt.Rows[i]["CD_DATE"].ToString());
                    newRow.CreateCell(4).SetCellValue(dt.Rows[i]["NOTE"].ToString());

                }

                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    var row = sheet.GetRow(i + 2);

                //    row.GetCell(0).CellStyle.BorderLeft = BorderStyle.Thin;
                //    row.GetCell(0).CellStyle.BorderRight = BorderStyle.Thin;
                //    row.GetCell(0).CellStyle.BorderTop = BorderStyle.Thin;
                //    row.GetCell(0).CellStyle.BorderBottom = BorderStyle.Thin;
                //    //
                //    row.GetCell(1).CellStyle.BorderLeft = BorderStyle.Thin;
                //    row.GetCell(1).CellStyle.BorderRight = BorderStyle.Thin;
                //    row.GetCell(1).CellStyle.BorderTop = BorderStyle.Thin;
                //    row.GetCell(1).CellStyle.BorderBottom = BorderStyle.Thin;
                //    //
                //    row.GetCell(2).CellStyle.BorderLeft = BorderStyle.Thin;
                //    row.GetCell(2).CellStyle.BorderRight = BorderStyle.Thin;
                //    row.GetCell(2).CellStyle.BorderTop = BorderStyle.Thin;
                //    row.GetCell(2).CellStyle.BorderBottom = BorderStyle.Thin;
                //    //
                //    row.GetCell(3).CellStyle.BorderLeft = BorderStyle.Thin;
                //    row.GetCell(3).CellStyle.BorderRight = BorderStyle.Thin;
                //    row.GetCell(3).CellStyle.BorderTop = BorderStyle.Thin;
                //    row.GetCell(3).CellStyle.BorderBottom = BorderStyle.Thin;
                //    //
                //    row.GetCell(4).CellStyle.BorderLeft = BorderStyle.Thin;
                //    row.GetCell(4).CellStyle.BorderRight = BorderStyle.Thin;
                //    row.GetCell(4).CellStyle.BorderTop = BorderStyle.Thin;
                //    row.GetCell(4).CellStyle.BorderBottom = BorderStyle.Thin;

                //}

                FileStream fs = new FileStream(pathSave, FileMode.CreateNew);
                wb.Write(fs);
                strResultPath = "ok";
                fs.Close();
                fs.Dispose();
            }
            catch (Exception ex)
            {
                strResultPath = "";
                log.Error(ex.Message);
                throw;
            }
            return strResultPath;
        }

        public static string CreateReportMSB(string pathRpt, string pathSave, DataTable dt)
        {
            var strResultPath = string.Empty;
            try
            {
                //    dt.Rows.RemoveAt(0);
                //    dt.AcceptChanges();
                // khởi tạo wb rỗng

                FileStream fss = new FileStream(pathRpt, FileMode.Open);

                var wb = new HSSFWorkbook(fss);

                ISheet sheet = wb.GetSheetAt(0);


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // tao row mới
                    var newRow = sheet.CreateRow(i + 2);
                    // set giá trị
                    newRow.CreateCell(0).SetCellValue(i+1);
                    newRow.CreateCell(1).SetCellValue(dt.Rows[i]["so_to_khai"].ToString());
                    newRow.CreateCell(2).SetCellValue(dt.Rows[i]["ngay_dk"].ToString());
                    newRow.CreateCell(3).SetCellValue(dt.Rows[i]["mst"].ToString());
                    newRow.CreateCell(4).SetCellValue(dt.Rows[i]["ma_xn"].ToString());
                    newRow.CreateCell(5).SetCellValue(dt.Rows[i]["ten_nnk"].ToString());
                    newRow.CreateCell(6).SetCellValue(dt.Rows[i]["ngay_thong_quan"].ToString());
                    newRow.CreateCell(7).SetCellValue(dt.Rows[i]["tong_tri_gia_hoa_don"].ToString());
                    newRow.CreateCell(8).SetCellValue(dt.Rows[i]["nguyen_te_tong_tri_gia_hoa_don"].ToString());
                    newRow.CreateCell(9).SetCellValue(dt.Rows[i]["so_hoa_don"].ToString());
                    newRow.CreateCell(10).SetCellValue(dt.Rows[i]["ngay_phat_hanh"].ToString());
                    newRow.CreateCell(11).SetCellValue(dt.Rows[i]["phuong_thuc_thanh_toan"].ToString());
                    newRow.CreateCell(12).SetCellValue(dt.Rows[i]["ten_nxk"].ToString());
                    newRow.CreateCell(13).SetCellValue(dt.Rows[i]["dia_chi_1"].ToString());
                    newRow.CreateCell(14).SetCellValue(dt.Rows[i]["dia_chi_2"].ToString());
                    newRow.CreateCell(15).SetCellValue(dt.Rows[i]["dia_chi_3"].ToString());
                    newRow.CreateCell(16).SetCellValue(dt.Rows[i]["trang_thai"].ToString());
                }

                FileStream fs = new FileStream(pathSave, FileMode.CreateNew);
                wb.Write(fs);
                strResultPath = "ok";
                fs.Close();
                fs.Dispose();
            }
            catch (Exception ex)
            {
                strResultPath = "";
                log.Error("CreateReportEIB :" + ex.Message);
                throw;
            }
            return strResultPath;
        }

        public static string CreateFileExcelToPdf(string pathRpt, string pathSave, DataTable dt)
        {
            return "";
        }

        //Lấy ngày - tháng - năm
        public static string GetCurrDate()
        {
            try
            {
                DateTime _dt = DateTime.Now;
                string _ngay = _dt.Day.ToString();
                string _thang = _dt.Month.ToString();
                string _nam = _dt.Year.ToString();
                return "Ngày " + _ngay + " tháng " + _thang + " năm " + _nam;
            }
            catch (Exception ex)
            {

                return "";
            }
        }
    }
    public class ExcelTitle
    {
        public string TitleText { get; set; }
        public string Color { get; set; }
        public string BackgroundColor { get; set; }
        public string Align { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string DataType { get; set; }

        //Align
        public static string AlignCenter = "Center";
        public static string AlignRight = "Right";
        public static string AlignLeft = "Left";

        //DataType
        public static string DataTypeString = "string";
        public static string DataTypeInt = "int";
        public static string DataTypeDecimal = "decimal";
        public static string DataTypeDateTime = "datetime";
    }

    public class ExcelTitleAlias
    {
        public ExcelTitleAlias()
        { 
        
        }
        public ExcelTitleAlias(string sAliasName, string sAliasValue)
        {
            AliasName = sAliasName;
            AliasValue = sAliasValue;
        }
        public string AliasName { get; set; }
        public string AliasValue { get; set; }
    }
}
