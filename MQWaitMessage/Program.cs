/** 
 * Author: Mr.Fuangwith S.
 * Date: 21/Sep/2008
 * Todo: Application start at here.
 **/

using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;
using System.IO;
using System.Configuration;
using System.Threading;
using ETAX_GDT;

namespace MQWaitMessage
{
    class Program
    {
        public static string logFile = ConfigurationSettings.AppSettings.Get("logfile").ToString();

        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            Console.WriteLine("==START==");
            String message = null;
            do
            {
                Console.WriteLine("Waiting...");
                try
                {
                    MQMessage mqMessage = MQMessageListener.Listen();
                    message = mqMessage.ReadString(mqMessage.MessageLength);
                    ETAX_GDT.ProcessMSG procmsg = new ETAX_GDT.ProcessMSG();
                    string tran_code = "";
                    string ms_id = "";
                    procmsg.process_Msg(message, ref tran_code, ref ms_id);
                    Console.WriteLine("Received Message: "+ DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") +" # tran_code=" + tran_code + " # msg_id=" + ms_id);

                    //background job citad 
                    if (ProcessCitad.getConfig("ProcessCitad.ON").Equals("1"))
                    {
                        ProcessCitad.BackgroundJobCitad();
                    }
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " # " + e.Message);
                    writeLog(e.Message);
                    Thread.Sleep(10000);
                }

            } while (message != "BYE");
            Console.WriteLine("==END==");
        }

        //Ghi log
        public static void writeLog(string msg)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(logFile, true);
                sw.WriteLine(DateTime.Now.ToString() + " " + msg);
                sw.Flush();
                sw.Close();
            }
            catch (Exception ex)
            {
            }
        }
    }
}
