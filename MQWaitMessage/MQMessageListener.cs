/** Author: Mr.Fuangwith S.
 *  Date: 21/Sep/2008
 *  Todo: This class waits for suitable message to arrive.
 **/

//add reference from C:\Program Files\IBM\WebSphere MQ\bin\amqmdnet.dll
using IBM.WMQ;
using System.Configuration;
using System.Collections;
using System;

namespace MQWaitMessage
{
    public class MQMessageListener
    {
       
        public static string mv_strHostIP = ConfigurationManager.AppSettings.Get("mq.hostIP").ToString();
        public static string mv_HostPort = ConfigurationManager.AppSettings.Get("mq.hostPort").ToString();
        public static string mv_strQmgrName = ConfigurationManager.AppSettings.Get("mq.qmgrName").ToString();
        public static string mv_strQInputName = ConfigurationManager.AppSettings.Get("mq.qlinputName").ToString();
        public static string mv_strQOutputName = ConfigurationManager.AppSettings.Get("mq.qloutputName").ToString();
        public static string mv_strChannelName = ConfigurationManager.AppSettings.Get("mq.svrchannelName").ToString();

        public static MQMessage Listen()
        {

            Hashtable queueProperties = new Hashtable();
            queueProperties.Add(MQC.HOST_NAME_PROPERTY, mv_strHostIP);
            queueProperties.Add(MQC.PORT_PROPERTY, Convert.ToInt32(mv_HostPort));
            queueProperties.Add(MQC.CHANNEL_PROPERTY, mv_strChannelName);
            queueProperties.Add(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES_CLIENT);

            /** MQOO_INPUT_AS_Q_DEF -- open queue to get message using queue-define default.
             *  MQOO_FAIL_IF_QUIESCING -- access fail if queue manange is quiescing. **/
            int openOptions = MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_FAIL_IF_QUIESCING;
            MQQueueManager mqManager = new MQQueueManager(mv_strQmgrName, queueProperties);
            MQQueue queue = mqManager.AccessQueue(mv_strQOutputName, openOptions);

            /** MQGMO_FAIL_IF_QUIESCING -- get message fail if queue manager is quiescing.
             *  MQGMO_WAIT -- waits for suitable message to arrive.
             *  MQWI_UNLIMITED -- unlimited wait interval. **/
            MQGetMessageOptions gmo = new MQGetMessageOptions();
            gmo.Options = MQC.MQGMO_FAIL_IF_QUIESCING | MQC.MQGMO_WAIT;
            gmo.WaitInterval = MQC.MQWI_UNLIMITED;
            MQMessage message = new MQMessage();
            //wait for message
            queue.Get(message, gmo);
            queue.Close();

            //release resource.
            mqManager = null;
            queue = null;
            gmo = null;
            queueProperties = null;
            System.GC.Collect();

            return message;
        }

    }
}
